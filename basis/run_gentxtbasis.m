allfiles = ls;
nn       = length(allfiles);
for ii = 1:nn
    filename = allfiles(ii,:);
    if contains(filename,'mat')
        Element_symbol = strtok(filename,'_');
        out_file_name  = strcat(Element_symbol,'.bas');
        gentxtbasis(filename,out_file_name);
    end
end