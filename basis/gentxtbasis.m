function gentxtbasis(a,b)

load(a,'-mat');
filename = b;

fid = fopen(filename,'wt');

%% Atom information
fprintf(fid,'%s\n',data.atom.symbol);
fprintf(fid,'%8.10f\n',data.atom.Z);
fprintf(fid,'%s\n',data.atom.shell);
fprintf(fid,'%8.10f\n',data.atom.N);
fprintf(fid,'%8.10f\n',data.atom.mass);
fprintf(fid,'%s\n',data.atom.name);

%% summery of potential and orbital set
fprintf(fid,'%s\n', ...
'################### Summery of potential and orbitalset ###################');

fprintf(fid,'%s\n','Vlocal, structure data, size as follows:');
fprintf(fid,'%8.10f\n',length(data.Vlocal));
fprintf(fid,'%s\n','rrData    drData    vvData');
fprintf(fid,'%g\t%g\t%g\n',length(data.Vlocal.rrData),length(data.Vlocal.drData),length(data.Vlocal.vvData));

fprintf(fid,'%s\n','Rlocal, structure data, size as follows:');
fprintf(fid,'%8.10f\n',length(data.Rlocal));
fprintf(fid,'%s\n','rrData    drData    rhoData');
fprintf(fid,'%g\t%g\t%g\n',length(data.Rlocal.rrData),length(data.Rlocal.drData),length(data.Rlocal.rhoData));

fprintf(fid,'%s\n','Vnl, structure data, size as follows:');
fprintf(fid,'%8.10f\n',length(data.Vnl));
fprintf(fid,'%s\n','rrData    drData    vvData    qqdata    fqData    qwData');
fprintf(fid,'%g\t%g\t%g\t%g\t%g\t%g\n',length(data.Vnl(1).rrData),length(data.Vnl(1).drData),length(data.Vnl(1).vvData),length(data.Vnl(1).qqData),length(data.Vnl(1).fqData),length(data.Vnl(1).qwData));

fprintf(fid,'%s\n','Vna, structure data, size as follows:');
fprintf(fid,'%8.10f\n',length(data.Vna));
fprintf(fid,'%s\n','rrData    drData    vvData');
fprintf(fid,'%g\t%g\t%g\n',length(data.Vna.rrData),length(data.Vna.drData),length(data.Vna.vvData));

fprintf(fid,'%s\n','Rna, structure data, size as follows:');
fprintf(fid,'%8.10f\n',length(data.Rna));
fprintf(fid,'%s\n','rrData    drData    rhoData');
fprintf(fid,'%g\t%g\t%g\n',length(data.Rna.rrData),length(data.Rna.drData),length(data.Rna.rhoData));

fprintf(fid,'%s\n','RelPseudoP, structure data, size as follows:');
fprintf(fid,'%8.10f\n',length(data.RelPseudoP));
fprintf(fid,'%s\n','rrData    drData    vvData_screened    vvdata_unscreened');
fprintf(fid,'%g\t%g\t%g\t%g\n',length(data.RelPseudoP(1).rrData),length(data.RelPseudoP(1).drData),length(data.RelPseudoP(1).vvData_screened),length(data.RelPseudoP(1).vvData_unscreened));

fprintf(fid,'%s\n','OrbitalSet, structure data, size as follows:');
fprintf(fid,'%8.10f\n',length(data.OrbitalSet));
fprintf(fid,'%s\n','rrData    drData    frData    qqdata    fqData    qwData');
fprintf(fid,'%g\t%g\t%g\t%g\t%g\t%g\n',length(data.OrbitalSet(1).rrData),length(data.OrbitalSet(1).drData),length(data.OrbitalSet(1).frData),length(data.OrbitalSet(1).qqData),length(data.OrbitalSet(1).fqData),length(data.OrbitalSet(1).qwData));
fprintf(fid,'\n');

%% Vlocal
fprintf(fid,'%s\n','Part: Vlocal');
fprintf(fid,'%s\n','Rcore,    Ncore');
fprintf(fid,'%8.10f\t%8.10f\n',data.Vlocal.Parameter.Rcore,data.Vlocal.Parameter.Ncore);

fprintf(fid,'%s\n','Part: Vlocal; SubPart: rrData');
nn = length(data.Vlocal.rrData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Vlocal.rrData(i));
    else
        fprintf(fid,'%8.10f\t',data.Vlocal.rrData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

fprintf(fid,'%s\n','Part: Vlocal; SubPart: drData');
nn = length(data.Vlocal.drData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Vlocal.drData(i));
    else
        fprintf(fid,'%8.10f\t',data.Vlocal.drData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

fprintf(fid,'%s\n','Part: Vlocal; SubPart: vvData');
nn = length(data.Vlocal.vvData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Vlocal.vvData(i));
    else
        fprintf(fid,'%8.10f\t',data.Vlocal.vvData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

%% Rlocal
fprintf(fid,'%s\n','Part: Rlocal');
fprintf(fid,'%s\n','Part: Rlocal; SubPart: rrData');
nn = length(data.Rlocal.rrData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Rlocal.rrData(i));
    else
        fprintf(fid,'%8.10f\t',data.Rlocal.rrData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

fprintf(fid,'%s\n','Part: Rlocal; SubPart: drData');
nn = length(data.Rlocal.drData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Rlocal.drData(i));
    else
        fprintf(fid,'%8.10f\t',data.Rlocal.drData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

fprintf(fid,'%s\n','Part: Rlocal; SubPart: rhoData');
nn = length(data.Rlocal.rhoData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Rlocal.rhoData(i));
    else
        fprintf(fid,'%8.10f\t',data.Rlocal.rhoData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

%% Vnl
mm = length(data.Vnl);
fprintf(fid,'%s\n','Part: Vnl');
fprintf(fid,'%8.10f\n',mm);
for jj = 1:mm
    fprintf(fid,'%s','Vnl, part: ');
    fprintf(fid,'%8.10f\n',jj);
    fprintf(fid,'%s\t','L              ');
    fprintf(fid,'%s\t','KBenergy       ');
    fprintf(fid,'%s\t','KBcosine       ');
    fprintf(fid,'%s\t','E              ');
    fprintf(fid,'%s\n','isGhost        ');
    fprintf(fid,'%8.10f\t%8.10f\t%8.10f\t%8.10f\t%8.10f\n',data.Vnl(jj).Parameter.L,data.Vnl(jj).Parameter.KBenergy,data.Vnl(jj).Parameter.KBcosine,data.Vnl(jj).Parameter.E,data.Vnl(jj).Parameter.isGhost);
    
    fprintf(fid,'%s%g%s\n','Part: Vnl(',jj,'); SubPart: rrData');
    nn = length(data.Vnl(jj).rrData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.Vnl(jj).rrData(i));
        else
            fprintf(fid,'%8.10f\t',data.Vnl(jj).rrData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end

    fprintf(fid,'%s%g%s\n','Part: Vnl(',jj,'); SubPart: drData');
    nn = length(data.Vnl(jj).drData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.Vnl(jj).drData(i));
        else
            fprintf(fid,'%8.10f\t',data.Vnl(jj).drData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end

    fprintf(fid,'%s%g%s\n','Part: Vnl(',jj,'); SubPart: vvData');
    nn = length(data.Vnl(jj).vvData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.Vnl(jj).vvData(i));
        else
            fprintf(fid,'%8.10f\t',data.Vnl(jj).vvData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
    
    fprintf(fid,'%s%g%s\n','Part: Vnl(',jj,'); SubPart: qqData');
    nn = length(data.Vnl(jj).qqData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.Vnl(jj).qqData(i));
        else
            fprintf(fid,'%8.10f\t',data.Vnl(jj).qqData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
    
    fprintf(fid,'%s%g%s\n','Part: Vnl(',jj,'); SubPart: fqData');
    nn = length(data.Vnl(jj).fqData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.Vnl(jj).fqData(i));
        else
            fprintf(fid,'%8.10f\t',data.Vnl(jj).fqData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
    
    fprintf(fid,'%s%g%s\n','Part: Vnl(',jj,'); SubPart: qwData');
    nn = length(data.Vnl(jj).qwData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.Vnl(jj).qwData(i));
        else
            fprintf(fid,'%8.10f\t',data.Vnl(jj).qwData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
end

%% Vna
fprintf(fid,'%s\n','Part: Vna');
fprintf(fid,'%s\n','Part: Vna; SubPart: rrData');
nn = length(data.Vna.rrData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Vna.rrData(i));
    else
        fprintf(fid,'%8.10f\t',data.Vna.rrData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

fprintf(fid,'%s\n','Part: Vna; SubPart: drData');
nn = length(data.Vna.drData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Vna.drData(i));
    else
        fprintf(fid,'%8.10f\t',data.Vna.drData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

fprintf(fid,'%s\n','Part: Vna; SubPart: vvData');
nn = length(data.Vna.vvData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Vna.vvData(i));
    else
        fprintf(fid,'%8.10f\t',data.Vna.vvData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

%% Rna
fprintf(fid,'%s\n','Part: Rna');
fprintf(fid,'%s\n','Part: Rna; SubPart: rrData');
nn = length(data.Rna.rrData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Rna.rrData(i));
    else
        fprintf(fid,'%8.10f\t',data.Rna.rrData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

fprintf(fid,'%s\n','Part: Rna; SubPart: drData');
nn = length(data.Rna.drData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Rna.drData(i));
    else
        fprintf(fid,'%8.10f\t',data.Rna.drData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

fprintf(fid,'%s\n','Part: Rna; SubPart: rhoData');
nn = length(data.Rna.rhoData);
for i = 1:nn
    if mod(i,10) == 0
        fprintf(fid,'%8.10f\n',data.Rna.rhoData(i));
    else
        fprintf(fid,'%8.10f\t',data.Rna.rhoData(i));
    end
end
if mod(nn,10)~=0
    fprintf(fid,'\n');
end

%% RelPseudoPotential
mm = length(data.RelPseudoP);
fprintf(fid,'%s\n','Part: RelPseudoP');
fprintf(fid,'%8.10f\n',mm);
for jj = 1:mm
    fprintf(fid,'%s','RelPseudoP, part: ');
    fprintf(fid,'%8.10f\n',jj);
    fprintf(fid,'%s\t','N              ');
    fprintf(fid,'%s\t','L              ');
    fprintf(fid,'%s\t','J              ');
    fprintf(fid,'%s\t','Rc             ');
    fprintf(fid,'%8.10f\t%8.10f\t%8.10f\t%8.10f\n', ...
                 data.RelPseudoP(jj).N, ...
                         data.RelPseudoP(jj).L, ...
                                 data.RelPseudoP(jj).J, ...
                                         data.RelPseudoP(jj).Rc);
    
    fprintf(fid,'%s%g%s\n','Part: RelPseudoP(',jj,'); SubPart: rrData');
    nn = length(data.RelPseudoP(jj).rrData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.RelPseudoP(jj).rrData(i));
        else
            fprintf(fid,'%8.10f\t',data.RelPseudoP(jj).rrData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end

    fprintf(fid,'%s%g%s\n','Part: RelPseudoP(',jj,'); SubPart: drData');
    nn = length(data.RelPseudoP(jj).drData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.RelPseudoP(jj).drData(i));
        else
            fprintf(fid,'%8.10f\t',data.RelPseudoP(jj).drData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end

    fprintf(fid,'%s%g%s\n','Part: RelPseudoP(',jj,'); SubPart: vvData_screened');
    nn = length(data.RelPseudoP(jj).vvData_screened);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.RelPseudoP(jj).vvData_screened(i));
        else
            fprintf(fid,'%8.10f\t',data.RelPseudoP(jj).vvData_screened(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
    
    fprintf(fid,'%s%g%s\n','Part: RelPseudoP(',jj,'); SubPart: vvData_unscreened');
    nn = length(data.RelPseudoP(jj).vvData_unscreened);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.RelPseudoP(jj).vvData_unscreened(i));
        else
            fprintf(fid,'%8.10f\t',data.RelPseudoP(jj).vvData_unscreened(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
end


%% OrbitalSet
mm = length(data.OrbitalSet);
fprintf(fid,'%s\n','Part: OrbitalSet');
fprintf(fid,'%8.10f\n',mm);
for jj = 1:mm
    fprintf(fid,'%s','OrbitalSet, part: ');
    fprintf(fid,'%8.10f\n',jj);
    fprintf(fid,'%s\t','N              ');
    fprintf(fid,'%s\t','L              ');
    fprintf(fid,'%s\t','type           ');
    fprintf(fid,'%s\t','E              ');
    fprintf(fid,'%s\t','Population     ');
    fprintf(fid,'%s\t','CoulombEnergyU ');
    fprintf(fid,'%s\n','ExchangeEnergyJ');
    fprintf(fid,'%8.10f\t%8.10f\t%s\t%8.10f\t%8.10f\t%8.10f\t%8.10f\n', ...
                 data.OrbitalSet(jj).Parameter.N, ...
                         data.OrbitalSet(jj).Parameter.L, ...
                                 data.OrbitalSet(jj).Parameter.type, ...
                                     data.OrbitalSet(jj).Parameter.E, ...
                                             data.OrbitalSet(jj).Parameter.Population, ...
                                                     data.OrbitalSet(jj).Parameter.CoulombEnergyU, ...
                                                             data.OrbitalSet(jj).Parameter.ExchangeEnergyJ);
    
    fprintf(fid,'%s%g%s\n','Part: OrbitalSet(',jj,'); SubPart: rrData');
    nn = length(data.OrbitalSet(jj).rrData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.OrbitalSet(jj).rrData(i));
        else
            fprintf(fid,'%8.10f\t',data.OrbitalSet(jj).rrData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end

    fprintf(fid,'%s%g%s\n','Part: OrbitalSet(',jj,'); SubPart: drData');
    nn = length(data.OrbitalSet(jj).drData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.OrbitalSet(jj).drData(i));
        else
            fprintf(fid,'%8.10f\t',data.OrbitalSet(jj).drData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end

    fprintf(fid,'%s%g%s\n','Part: OrbitalSet(',jj,'); SubPart: frData');
    nn = length(data.OrbitalSet(jj).frData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.OrbitalSet(jj).frData(i));
        else
            fprintf(fid,'%8.10f\t',data.OrbitalSet(jj).frData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
    
    fprintf(fid,'%s%g%s\n','Part: OrbitalSet(',jj,'); SubPart: qqData');
    nn = length(data.OrbitalSet(jj).qqData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.OrbitalSet(jj).qqData(i));
        else
            fprintf(fid,'%8.10f\t',data.OrbitalSet(jj).qqData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
    
    fprintf(fid,'%s%g%s\n','Part: OrbitalSet(',jj,'); SubPart: fqData');
    nn = length(data.OrbitalSet(jj).fqData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.OrbitalSet(jj).fqData(i));
        else
            fprintf(fid,'%8.10f\t',data.OrbitalSet(jj).fqData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
    
    fprintf(fid,'%s%g%s\n','Part: OrbitalSet(',jj,'); SubPart: qwData');
    nn = length(data.OrbitalSet(jj).qwData);
    for i = 1:nn
        if mod(i,10) == 0
            fprintf(fid,'%8.10f\n',data.OrbitalSet(jj).qwData(i));
        else
            fprintf(fid,'%8.10f\t',data.OrbitalSet(jj).qwData(i));
        end
    end
    if mod(nn,10)~=0
        fprintf(fid,'\n');
    end
end

fclose(fid);