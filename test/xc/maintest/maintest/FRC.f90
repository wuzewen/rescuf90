! FRC.f90
    
    module FORTRAN_RESCU_CALCULATION_TYPE
    
    use mpiType_module
    use spinType_module
    use domainType_module
    use functionalType_module
    use symmetryType_module
    use infoType_module
    use energyType_module
    use potentialType_module
    use mixingType_module
    use FRC_RHO_TYPE
    use smiType_module
    
    type :: FORTRAN_RESCU_CALCULATION
        integer              :: scloop
        type(mpiType)        :: mpi
        type(spinType)       :: spin
        type(domainType)     :: domain
        type(functionalType) :: functional
        type(symmetryType)   :: symmetry
        type(infoType)       :: info
        type(energyType)     :: energy
        type(potentialType)  :: potential
        type(mixingType)     :: mixing
        type(FRCrhoType)     :: rho
        type(smiType)        :: smi
    end type FORTRAN_RESCU_CALCULATION
    
    end module FORTRAN_RESCU_CALCULATION_TYPE