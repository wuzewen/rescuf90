!  maintest.f90 
!
!  FUNCTIONS:
!  maintest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: maintest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program maintest

    use FORTRAN_RESCU_CALCULATION_TYPE
    use xcMain_module
    
    implicit none
    
    type(FORTRAN_RESCU_CALCULATION) :: FRC

    ! Variables
    integer :: ii, jj, kk

    ! Body of maintest
    print *, 'Hello World'
    
    !*********************************************  MPI
    FRC%mpi%status  = .FALSE.
    FRC%mpi%rank    =  0
    FRC%mpi%mpisize =  1
    
    !*********************************************  Spin
    FRC%spin%ispin  =  1
    
    !*********************************************  domain
    FRC%domain%fgridn = 13
    
    !*********************************************  functional
    allocate(FRC%functional%list(2))
    FRC%functional%libxc   = .FALSE.
    FRC%functional%list(1) = "XC_LDA_X"
    FRC%functional%list(2) = "XC_LDA_C_PW"
    
    !*********************************************  scf loop
    FRC%scloop = 1
    
    !*********************************************  mixing
    FRC%mixing%mixingType = "Potential"
    
    !*********************************************  rho
    allocate(FRC%rho%output(1))
    allocate(FRC%rho%output(1)%vata(2197,1))
    allocate(FRC%rho%pc%vata(2197,1))
    FRC%rho%output(1)%m      = 2197
    FRC%rho%output(1)%n      = 1
    FRC%rho%output(1)%mblock = 1
    FRC%rho%output(1)%nblock = 1
    FRC%rho%output(1)%mproc  = 1
    FRC%rho%output(1)%nproc  = 1
    open(unit=10,file='rhoout.txt',status='old',access='sequential',action='read')
    do ii = 1,2197,1
        read(10,*) FRC%rho%output(1)%vata(ii,1)
    end do
    close(10)
    FRC%rho%pc%m      = 2197
    FRC%rho%pc%n      = 1
    FRC%rho%pc%mblock = 2197
    FRC%rho%pc%nblock = 1
    FRC%rho%pc%mproc  = 1
    FRC%rho%pc%nproc  = 1
    open(unit=10,file='rhopc.txt',status='old',access='sequential',action='read')
    do ii = 1,2197,1
        read(10,*) FRC%rho%pc%vata(ii,1)
    end do
    close(10)
    
    !*********************************************  symmetry
    FRC%symmetry%pointsymmetry = .TRUE.
    allocate(FRC%symmetry%sym_rec(3,3,48))
    open(unit=10,file='sym_rec.txt',status='old',access='sequential',action='read')
    do ii = 1,3,1
        do jj = 1,3,1
            do kk = 1,48,1
                read(10,*) FRC%symmetry%sym_rec(ii,jj,kk)
            end do
        end do
    end do
    close(10)
    allocate(FRC%symmetry%sym_t(48,3))
    open(unit=10,file='sym_t.txt',status='old',access='sequential',action='read')
    do ii = 1,48,1
        read(10,*) FRC%symmetry%sym_t(ii,:)
    end do
    close(10)
    
    call xcMain(FRC)

    end program maintest

