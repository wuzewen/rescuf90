!  mixer.f90 
!
!  FUNCTIONS:
!  mixer - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: mixer
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program mixerMainTest

    use FORTRAN_RESCU_CALCULATION_TYPE
    use mixer_module
    
    implicit none

    ! Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer :: ii

    ! Body of mixer
    print *, 'Hello World'
    
    ! ********************************* info
    FRC%info%calculationType = "SCF"
    
    ! ********************************* mpi
    FRC%mpi%status = .FALSE.
    FRC%mpi%rank   = 0
    FRC%scloop     = 1
    
    ! ********************************* mixing
    FRC%mixing%mixingType = "Density"
    FRC%mixing%method     = "Linear"
    FRC%mixing%beta       = 0.25
    
    ! ********************************* rho
    allocate(FRC%rho%input(2))
    allocate(FRC%rho%input(1)%vata(2197,1))
    allocate(FRC%rho%input(2)%vata(2197,1))
    open(unit=10,file='rhoin.txt',status='old',access='sequential',action='read')
    do ii = 1,2197,1
        read(10,*) FRC%rho%input(1)%vata(ii,1)
    end do
    close(10)
    
    allocate(FRC%rho%output(2))
    allocate(FRC%rho%output(1)%vata(2197,1))
    open(unit=10,file='rhoout.txt',status='old',access='sequential',action='read')
    do ii = 1,2197,1
        read(10,*) FRC%rho%output(1)%vata(ii,1)
    end do
    close(10)
    
    call mixer(FRC)

    end program mixerMainTest

