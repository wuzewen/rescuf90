!  maintest.f90 
!
!  FUNCTIONS:
!  maintest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: maintest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program maintest
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ElementDataType_module
    use GetHartreePotential_module
    use VataMN_1D_module

    implicit none

    ! Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_1D)                 :: rho, VHout
    type(ElementDataType)           :: ElementData
    integer :: ii, bc(3), jj, kk
    logical :: symmetry

    ! Body of maintest
    print *, 'Hello World'
    FRC%mpi%status  = .FALSE.
    FRC%mpi%rank    = 0
    FRC%mpi%mpisize = 1
    
    FRC%domain%fgridn(1) = 13
    FRC%domain%fgridn(2) = 13
    FRC%domain%fgridn(3) = 13
    
    FRC%domain%latvec(:,:) = 5.131267834761913
    FRC%domain%latvec(1,1) = 0
    FRC%domain%latvec(2,2) = 0
    FRC%domain%latvec(3,3) = 0
    
    allocate(FRC%atom%element(2))
    FRC%atom%element(1) = 1
    FRC%atom%element(2) = 1
    FRC%Atom%numberOfElement = 1
    FRC%Atom%numberOfAtom    = 2
    
    
    !FRC%potential%fourierinit  = .FALSE.
    !FRC%potential%initParaReal = .FALSE.
    
    allocate(FRC%atom%XYZ(2,3))
    FRC%atom%XYZ(1,1) = 1.282816958690478
    FRC%atom%XYZ(1,2) = 1.282816958690478
    FRC%atom%XYZ(1,3) = 1.282816958690478
    FRC%atom%XYZ(2,1) = 3.848450876071436
    FRC%atom%XYZ(2,2) = 3.848450876071436
    FRC%atom%XYZ(2,3) = 3.848450876071436
    
    FRC%spin%ispin    = 1
    
    
    allocate(FRC%SOB(FRC%Atom%numberOfElement))
    FRC%SOB(1)%SizeOfVnl = 4
    allocate(FRC%element(1))
    allocate(FRC%element(1)%vnllnu(4))
    allocate(FRC%element(1)%vnlnnu(4))
    allocate(FRC%element(1)%vnlrnu(4))
    allocate(FRC%element(1)%vnlenu(4))
    forall(ii=1:4)
        FRC%element(1)%vnllnu(ii) = ii-1
        !FRC%element(1)%vnlenu(ii) = 
        FRC%element(1)%vnlnnu(ii) = 1
        FRC%element(1)%vnlrnu(ii) = 1.865161693277237 
    end forall
    FRC%element(1)%vnlenu(1)  =  2.577499550897633
    FRC%element(1)%vnlenu(2)  =  0.867640491169417 
    FRC%element(1)%vnlenu(3)  = -1.229641167609682
    FRC%element(1)%vnlenu(4)  = -0.378759557601471
    FRC%element(1)%vnlLcutoff =  3
    FRC%element(1)%vnlNcutoff =  2
    FRC%symmetry%pointsymmetry = .TRUE.
    allocate(FRC%symmetry%sym_rec(3,3,48))
    open(unit=10,file='sym_rec.txt',status='old',access='sequential',action='read')
    do ii = 1,3,1
        do jj = 1,3,1
            do kk = 1,48,1
                read(10,*) FRC%symmetry%sym_rec(ii,jj,kk)
            end do
        end do
    end do
    close(10)
    allocate(FRC%symmetry%sym_t(48,3))
    open(unit=10,file='sym_t.txt',status='old',access='sequential',action='read')
    do ii = 1,48,1
        read(10,*) FRC%symmetry%sym_t(ii,:)
    end do
    close(10)
    symmetry = .TRUE.
    bc         = 1
    allocate(rho%vata(2197))
    rho%vata   = 0
    rho%m      = 2197
    rho%n      = 1
    rho%mblock = 2197
    rho%nblock = 1
    rho%mproc  = 1
    rho%nproc  = 1
    call GetHartreePotential(FRC,rho,bc,symmetry,VHout)

    end program maintest

