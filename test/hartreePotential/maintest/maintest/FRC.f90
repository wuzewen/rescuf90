! FRC.f90
    
!**********************************
    !
    !
    !
!**********************************
    
    module FORTRAN_RESCU_CALCULATION_TYPE
    
    use mpitype_module
    use spinType_module
    use symmetryType_module
    use atomType_module
    use SummeryOfBas_module
    use domainType_module
    use ElementDataType_module
    use elementType_module
    use smiType_module
    
    type :: FORTRAN_RESCU_CALCULATION
        type(atomType)                     :: atom
        type(mpiType)                      :: mpi
        type(spinType)                     :: spin
        type(symmetryType)                 :: symmetry
        type(SummeryOfBas), allocatable    :: SOB(:)
        type(ElementDataType), allocatable :: ElementData
        type(domainType)                   :: domain
        type(elementType), allocatable     :: element(:)
        type(smiType)                      :: smi
    end type FORTRAN_RESCU_CALCULATION
    
    end module FORTRAN_RESCU_CALCULATION_TYPE