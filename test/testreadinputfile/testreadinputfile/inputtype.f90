!
    
    module inputtype
    !contains
    
    type :: inputFromFileType
        character(len=50) :: name, value
        !real*8            :: value
    end type inputFromFileType
    !type(inputFromFileType), allocatable :: inputFromFile(:)
    
    end module inputtype