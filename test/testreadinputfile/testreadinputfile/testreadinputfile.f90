!  testreadinputfile.f90 
!
!  FUNCTIONS:
!  testreadinputfile - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: testreadinputfile
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program testreadinputfile

    use readinputfile_module
    use inputtype
    
    implicit none

    ! Variables
    integer :: nline, nvalue
    !type :: inputFromFileType
    !    character(len=50) :: name, value
    !    !real*8            :: value
    !end type inputFromFileType
    type(inputFromFileType), allocatable :: inputFromFile(:)
    integer :: ii

    ! Body of testreadinputfile
    print *, 'Hello World'
    call readNumberOfInput(nline,nvalue)
    write(*,*) nline
    write(*,*) nvalue
    allocate(inputFromFile(nvalue))
    call readinputfile(nline,inputFromFile)
    do ii = 1,nvalue,1
        write(*,*) inputFromFile(ii)%name
        write(*,*) inputFromFile(ii)%value
    end do
    

    end program testreadinputfile

