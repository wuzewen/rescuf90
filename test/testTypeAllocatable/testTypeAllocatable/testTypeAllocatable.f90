!  testTypeAllocatable.f90 
!
!  FUNCTIONS:
!  testTypeAllocatable - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: testTypeAllocatable
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program testTypeAllocatable

    implicit none

    ! Variables
    type :: Atom_type
        integer, allocatable :: element(:)
    end type
    type(Atom_type),allocatable :: Atom(:)

    
    ! Body of testTypeAllocatable
    allocate(Atom(2))
    allocate(Atom(1)%element(3))
    allocate(Atom(2)%element(0))
    Atom(1)%element(:) = 1
    Atom(2)%element(:) = 2
    
    write(*,*) Atom(1)%element
    write(*,*) Atom(2)%element

    end program testTypeAllocatable

