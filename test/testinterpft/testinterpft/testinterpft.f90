!  testinterpft.f90 
!
!  FUNCTIONS:
!  testinterpft - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: testinterpft
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program testinterpft
    use interpft_module
    implicit none

    ! Variables
    integer             :: cn, fn
    complex,allocatable :: eyem(:,:)
    complex,allocatable :: MatrixC(:,:)
    

    ! Body of testinterpft
    allocate(eyem(cn,cn))
    call interpft(eyem,fn,MatrixC)

    end program testinterpft

