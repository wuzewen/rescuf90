!  arraytest.f90 
!
!  FUNCTIONS:
!  arraytest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: arraytest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program arraytest

    implicit none

    ! Variables
    !integer :: a(3,3), b(1,3), ii, jj

    ! Body of arraytest
    !print *, 'Hello World'
    !forall(ii=1:3,jj=1:3)
    !    a(ii,jj) = ii*2 + jj*3
    !end forall
    !write(*,*) sum(a**2,2)**0.5
    !b = a(1,*)
    !write(*,*) b
    
    
    !integer, allocatable :: a(:,:)
    !integer              :: ii
    !do ii = 1,5,1
    !    allocate(a(ii,ii))
    !    a = ii
    !    write(*,*) a
    !    deallocate(a)
    !end do

    
    integer :: a(5,5), ii, b(2,2)
    b          = 1
    a(1:2,1:2) = b
    a(3:5,3:5) = 2
    do ii = 1,5,1
        write(*,*) a(ii,:)
    end do
    
    end program arraytest

