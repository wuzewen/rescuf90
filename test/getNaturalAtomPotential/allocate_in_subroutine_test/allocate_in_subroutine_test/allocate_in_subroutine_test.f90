!  allocate_in_subroutine_test.f90 
!
!  FUNCTIONS:
!  allocate_in_subroutine_test - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: allocate_in_subroutine_test
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program allocate_in_subroutine_test

    use test_sub_module
    
    implicit none

    ! Variables
    integer, allocatable :: A(:,:) 

    ! Body of allocate_in_subroutine_test
    print *, 'Hello World'
    call test_sub(A)
    write(*,*) A

    end program allocate_in_subroutine_test

