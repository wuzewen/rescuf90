! test_sub.f90
    
    module test_sub_module
    contains
    subroutine test_sub(A)
    
    implicit none
    
    integer, allocatable :: A(:,:)
    
    allocate(A(2,2))
    A = 2
    
    return
    end subroutine test_sub
    end module test_sub_module