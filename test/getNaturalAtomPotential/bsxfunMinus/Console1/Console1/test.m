nA = 3;
mA = 1;
nB = 1;
mB = 3;
A = zeros(nA,mA);
B = zeros(nB,mB);
for ii = 1:nA
    for jj = 1:mA
        A(ii,jj) = ii^2 - jj*2;
    end
end

for ii = 1:nB
    for jj = 1:mB
        B(ii,jj) = ii*3 - jj/2;
    end
end

C = bsxfun(@rdivide,A,B)