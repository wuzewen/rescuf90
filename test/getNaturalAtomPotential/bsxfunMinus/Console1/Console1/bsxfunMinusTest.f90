!  Console1.f90 
!
!  FUNCTIONS:
!  Console1 - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: Console1
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program bsxfunMinusTest

    use bsxfunMinus_module
    use bsxfunRdivide_module
    
    implicit none

    ! Variables
    real*8, allocatable :: A(:,:), B(:,:), C(:,:)
    integer             :: nA, mA, nB, mB, ii, jj

    ! Body of Console1
    print *, 'Hello World'
    
    nA = 3
    mA = 1
    nB = 1
    mB = 3
    allocate(A(nA,mA),B(nB,mB))
    forall(ii=1:nA,jj=1:mA)
        A(ii,jj) = real(ii)**2 - real(jj)*2
    end forall
    forall(ii=1:nB,jj=1:mB)
        B(ii,jj) = real(ii)*3 - real(jj)/2
    end forall
    
    allocate(C(max(nA,nB),max(mA,mB)))
    call bsxfunRdivide(A,B,C)
    write(*,*) "A"
    write(*,*)  A
    write(*,*) "B"
    write(*,*)  B
    write(*,*) "C"
    write(*,*)  C

    end program bsxfunMinusTest

