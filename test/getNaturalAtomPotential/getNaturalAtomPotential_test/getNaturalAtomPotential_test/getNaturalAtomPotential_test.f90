!  getNaturalAtomPotential_test.f90 
!
!  FUNCTIONS:
!  getNaturalAtomPotential_test - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: getNaturalAtomPotential_test
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program getNeutralAtomPotential_test
    
    use dataType_module
    use readSummeryOfBasis_module
    use readPseudoPotential_module
    use genNeutralAtomPotential_module

    implicit none

    ! Variables
    type(SummeryOfBas)    :: SOB
    type(ElementDataType) :: ElementData
    type(FRC_type)        :: FRC
    character(len=4)      :: ElementName
    character(len=20)     :: nonsense
    integer               :: ii, tmp

    ! Body of getNaturalAtomPotential_test
    
    print *, 'Hello World'
    !fileName = 'Si.bas'
    open(unit=10,file='Si.bas',status='old',access='sequential',action='read')
    do ii = 1,32,1
        read(10,*) nonsense
    end do
    read(10,*) tmp
    allocate(SOB%orbitalsize(tmp))
    close(10)
    
    ElementName = "Si"
    call readSummeryOfBasis(ElementName,SOB)
    
    !allocate(ElementData(Vlocal(SizeOfVlocal)))
    allocate(ElementData%Vlocal%rrData(SOB%SizeOfVlocalrrData))
    allocate(ElementData%Vlocal%drData(SOB%SizeOfVlocaldrData))
    allocate(ElementData%Vlocal%vvData(SOB%SizeOfVlocalvvData))
    
    !allocate(ElementData(Rlocal(SizeOfRlocal)))
    allocate(ElementData%Rlocal%rrData(SOB%SizeOfRlocalrrData))
    allocate(ElementData%Rlocal%drData(SOB%SizeOfRlocaldrData))
    allocate(ElementData%Rlocal%rhoData(SOB%SizeOfRlocalrhoData))
    
    allocate(ElementData%Vnl(SOB%SizeOfVnl))
    do ii = 1,SOB%SizeOfVnl,1
        allocate(ElementData%Vnl(ii)%rrData(SOB%SizeOfVnlrrData))
        allocate(ElementData%Vnl(ii)%drData(SOB%SizeOfVnldrData))
        allocate(ElementData%Vnl(ii)%vvData(SOB%SizeOfVnlvvData))
        allocate(ElementData%Vnl(ii)%qqData(SOB%SizeOfVnlqqData))
        allocate(ElementData%Vnl(ii)%fqData(SOB%SizeOfVnlfqData))
        allocate(ElementData%Vnl(ii)%qwData(SOB%SizeOfVnlqwData))
    end do
    
    !allocate(ElementData(Vna(SizeOfVlocal)))
    allocate(ElementData%Vna%rrData(SOB%SizeOfVnarrData))
    allocate(ElementData%Vna%drData(SOB%SizeOfVnadrData))
    allocate(ElementData%Vna%vvData(SOB%SizeOfVnavvData))
    
    !allocate(ElementData(Rna(SizeOfRlocal)))
    allocate(ElementData%Rna%rrData(SOB%SizeOfRnarrData))
    allocate(ElementData%Rna%drData(SOB%SizeOfRnadrData))
    allocate(ElementData%Rna%rhoData(SOB%SizeOfRnarhoData))
    
    allocate(ElementData%RelPseudoP(SOB%SizeOfRelPseudoP))
    do ii = 1,SOB%SizeOfRelPseudoP,1
        allocate(ElementData%RelPseudoP(ii)%rrData(SOB%SizeOfRelPseudoPrrData))
        allocate(ElementData%RelPseudoP(ii)%drData(SOB%SizeOfRelPseudoPdrData))
        allocate(ElementData%RelPseudoP(ii)%vvData_screened(SOB%SizeOfRelPseudoPvvData_screened))
        allocate(ElementData%RelPseudoP(ii)%vvData_unscreened(SOB%SizeOfRelPseudoPvvData_unscreened))
    end do
    
    allocate(ElementData%OrbitalSet(SOB%SizeOfOrbitalSet))
    do ii = 1,SOB%SizeOfOrbitalSet,1
        allocate(ElementData%OrbitalSet(ii)%rrData(SOB%orbitalsize(ii)%SizeOfOrbitalSetrrData))
        allocate(ElementData%OrbitalSet(ii)%drData(SOB%orbitalsize(ii)%SizeOfOrbitalSetdrData))
        allocate(ElementData%OrbitalSet(ii)%frData(SOB%orbitalsize(ii)%SizeOfOrbitalSetfrData))
        allocate(ElementData%OrbitalSet(ii)%qqData(SOB%orbitalsize(ii)%SizeOfOrbitalSetqqData))
        allocate(ElementData%OrbitalSet(ii)%fqData(SOB%orbitalsize(ii)%SizeOfOrbitalSetfqData))
        allocate(ElementData%OrbitalSet(ii)%qwData(SOB%orbitalsize(ii)%SizeOfOrbitalSetqwData))
    end do
    
    call readPseudoPotential(SOB,ElementData)
    allocate(FRC%SOB(1))
    allocate(FRC%ElementData(1))
    FRC%ElementData(1)        = ElementData
    FRC%mpi%status            = .FALSE.
    FRC%mpi%rank              = 0
    FRC%mpi%mpisize           = 1
    FRC%smi%status            = .FALSE.
    FRC%domain%fgridn(1)      = 4
    FRC%domain%fgridn(2)      = 4
    FRC%domain%fgridn(3)      = 4
    FRC%domain%latvec         = 0
    FRC%domain%latvec(1,1)    = 4
    FRC%domain%latvec(2,2)    = 4
    FRC%domain%latvec(3,3)    = 4
    FRC%atom%element          = 1
    FRC%atom%xyz              = 0
    FRC%atom%xyz(2,1)         = 2
    FRC%atom%xyz(2,2)         = 2
    FRC%atom%xyz(3,1)         = 2
    FRC%atom%xyz(3,3)         = 2
    FRC%atom%xyz(4,2)         = 2
    FRC%atom%xyz(4,3)         = 2
    FRC%potential%fourierinit = .TRUE.
    FRC%option%initParaReal   = .TRUE.
    FRC%SOB(1)                = SOB
    FRC%atom%numberOfElement  = 1
    FRC%atom%numberOfAtom     = 4
    !write(*,*) "Lattice Vectors"
    !write(*,*)  FRC%domain%latvec
    
    !write(*,*) "Atom positions"
    !write(*,*)  FRC%atom%xyz
    
    !write(*,*) SOB%SizeOfVnl
    
    
    call genNeutralAtomPotential(FRC)
    write(*,*) "Potential"
    write(*,*) FRC%potential%vna%dataA
    
    tmp = size(FRC%potential%vna%dataA)
    open(unit=11,file='potentialData.txt',status='replace',access='sequential',action='write')
    do ii = 1,tmp,1
        write(11,*) FRC%potential%vna%dataA(ii)
    end do
    

    end program getNeutralAtomPotential_test

