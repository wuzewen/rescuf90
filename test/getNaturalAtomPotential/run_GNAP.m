mrc.mpi.status    = false;
mrc.mpi.rank      = 0;
mrc.mpi.size      = 1;

mrc.smi.status    = true;

mrc.domain.fgridn = [4,4,4];
mrc.domain.latvec = [4,0,0;0,4,0;0,0,4];

%mrc.element       = [1,1,1,1];
mrc.atom.element  = [1,1,1,1];
mrc.atom.xyz      = [[0 0 0]; ...
                     [2 2 0]; ...
                     [2 0 2]; ...
                     [0 2 2]];%4*rand(4,3);

mrc.potential.fourierinit = false;

mrc.option.initParaReal   = true;

data = load('Si_SZP.mat');
mrc.data = data.data;
mrc = GetNeutralAtomPotential(mrc);