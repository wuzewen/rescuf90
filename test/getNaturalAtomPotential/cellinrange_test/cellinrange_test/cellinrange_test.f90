!  cellinrange_test.f90 
!
!  FUNCTIONS:
!  cellinrange_test - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: cellinrange_test
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program cellinrange_test

    use cellInRange_module
    
    implicit none

    ! Variables
    real*8, allocatable :: avec(:,:)
    real*8              :: cartxyz(1,3), rad
    real*8, allocatable :: bound(:,:)
    logical             :: issym
    integer             :: ii, jj

    ! Body of cellinrange_test
    print *, 'Hello World'
    issym = .FALSE.
    allocate(avec(3,3))
    forall(ii=1:3, jj=1:3)
        avec(ii,jj) = ii*2+jj**3
    end forall
    rad = 0.3
    issym = .FALSE.
    !allocate(bound(5,5))
        
    call cellInRange(avec,cartxyz,rad,issym,bound)

    end program cellinrange_test

