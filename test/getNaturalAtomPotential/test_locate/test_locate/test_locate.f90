!  test_locate.f90 
!
!  FUNCTIONS:
!  test_locate - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: test_locate
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program test_locate

    implicit none

    ! Variables
    integer :: a(5),ii
    integer, allocatable :: b(:)

    ! Body of test_locate
    print *, 'Hello World'
    forall(ii=1:5)
        a(ii) = ii
    end forall
    allocate(b(1))
    b = minloc(a)
    write(*,*) b !minloc(a)
    write(*,*) size(b)

    end program test_locate

