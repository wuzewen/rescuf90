!  GetGlobalInd_test.f90 
!
!  FUNCTIONS:
!  GetGlobalInd_test - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: GetGlobalInd_test
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program GetGlobalInd_test

    use GetGlobalInd_module
    
    implicit none

    ! Variables

    ! Body of GetGlobalInd_test
    print *, 'Hello World'

    end program GetGlobalInd_test

