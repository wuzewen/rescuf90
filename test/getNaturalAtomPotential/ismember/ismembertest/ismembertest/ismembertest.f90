!  ismembertest.f90 
!
!  FUNCTIONS:
!  ismembertest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: ismembertest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program ismembertest

    use ismember_module
    
    implicit none

    ! Variables
    real*8, allocatable :: A(:,:), B(:,:)
    integer :: mA, nA, BB, ii, jj
    logical, allocatable :: isLoc(:,:)

    ! Body of ismembertest
    print *, 'Hello World'
    mA = 4
    nA = 4
    BB = 6
    
    allocate(B(BB,1))
    forall(ii=1:BB)
        B(ii,1) = ii
    end forall
    
    allocate(A(mA,nA))
    forall(ii=1:mA,jj=1:nA)
        A(ii,jj) = ii*2-jj
    end forall
    
    allocate(isLoc(mA,nA))
    call ismember(A,B,isLoc)

    write(*,*) "A"
    write(*,*)  A
    write(*,*) "B"
    write(*,*)  B
    write(*,*) "isLoc"
    write(*,*)  isLoc
    
    end program ismembertest

