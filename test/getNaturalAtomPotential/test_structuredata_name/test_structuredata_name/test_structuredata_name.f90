!  test_structuredata_name.f90 
!
!  FUNCTIONS:
!  test_structuredata_name - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: test_structuredata_name
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program test_structuredata_name

    implicit none

    ! Variables
    type :: tulu_type
        integer :: vvdata
        integer :: rrdata
    end type
    type(tulu_type) :: tulu
    character(len=20) :: name

    ! Body of test_structuredata_name
    print *, 'Hello World'
    name = "vvdata"
    tulu%name = 1

    end program test_structuredata_name

