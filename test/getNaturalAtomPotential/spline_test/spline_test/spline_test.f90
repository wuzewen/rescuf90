!  spline_test.f90 
!
!  FUNCTIONS:
!  spline_test - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: spline_test
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program spline_test

    use spline_module
    
    implicit none

    ! Variables
    real*8, allocatable :: X(:), Y(:), Xin(:), Yout(:)
    real*8  :: ddY0, ddYe
    integer :: n, m, ii

    ! Body of spline_test
    print *, 'Hello World'
    n = 10
    m = 4
    allocate(X(n),Y(n))
    allocate(Xin(m),Yout(m))
    
    forall(ii=1:n)
        X(ii) = real(ii)
        Y(ii) = real(ii)**3
    end forall
    
    Xin(1) = 1.8
    Xin(2) = 3.7
    Xin(3) = 6.3
    Xin(4) = 8.9
    
    ddY0 = 6
    ddYe = 60
    write(*,*) "Yout"
    !write(*,*)  X
    
    call spline(X,Y,ddY0,ddYe,Xin,Yout)
    
    !write(*,*) "Yout"
    write(*,*)  Yout
    

    end program spline_test

