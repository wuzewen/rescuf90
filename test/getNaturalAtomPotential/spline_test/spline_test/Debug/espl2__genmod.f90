        !COMPILER-GENERATED INTERFACE MODULE: Fri Feb 09 12:01:42 2018
        MODULE ESPL2__genmod
          INTERFACE 
            SUBROUTINE ESPL2(X,Y,N,DY1,DYN,XX,M,DY,DDY,S,DS,DDS,T,H)
              INTEGER(KIND=4) :: M
              INTEGER(KIND=4) :: N
              REAL(KIND=8) :: X(N)
              REAL(KIND=8) :: Y(N)
              REAL(KIND=8) :: DY1
              REAL(KIND=8) :: DYN
              REAL(KIND=8) :: XX(M)
              REAL(KIND=8) :: DY(N)
              REAL(KIND=8) :: DDY(N)
              REAL(KIND=8) :: S(M)
              REAL(KIND=8) :: DS(M)
              REAL(KIND=8) :: DDS(M)
              REAL(KIND=8) :: T
              REAL(KIND=8) :: H(N)
            END SUBROUTINE ESPL2
          END INTERFACE 
        END MODULE ESPL2__genmod
