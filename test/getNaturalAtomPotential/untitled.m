! GetGlobalInd.f90
    
!**********************************
    !
    !
    !
!**********************************
    
    module GetGlobalInd_module
    contains
    subroutine GetGlobalInd(mpirank,A,rowind,colind)
    
    use A_module
    
    implicit none
    
    ! input variables
    integer :: mpirank
    type(A_type) :: A
    
    
    ! temporary variables
    integer              :: m, n, mb, nb, nprow, npcol, myrow, mycol, ii, nbrow, nbcol, ntmp, jj
    integer, allocatable :: tmp(:,:), tmp1(:,:), tmp2(:,:), tmp3(:,:), tmp4(:,:), tmptmp(:,:)
    integer, allocatable :: rowindtmp(:,:), colindtmp(:,:)
    
    ! output variables
    integer, allocatable :: rowind(:,:), colind(:,:)
    
    ! body of this fnction
    m     = A%m
    n     = A%n
    mb    = A%mblock
    nb    = A%nblock
    nprow = A%mproc
    npcol = A%nproc
    myrow = mpirank/npcol
    mycol = mod(mpirank,npcol)
    
    allocate(rowindtmp(mb,1))
    forall(ii=1:mb)
        rowindtmp(ii,1) = ii-1
    end forall
    rowindtmp = rowindtmp+myrow*mb
    
    nbrow  = m/mb/nprow
    allocate(tmp1(mb,1))
    allocate(tmp(mb,1))
    tmp1   = rowindtmp
    tmp    = rowindtmp
    allocate(tmp2(1,nbrow))
    forall(ii=1:nbrow)
        tmp2(1,ii) = ii-1
    end forall
    tmp2 = tmp2*mb*nprow
    
    deallocate(rowindtmp)
    allocate(rowindtmp(mb,nbrow))
    call bsxfinPlus(tmp1,tmp2,rowindtmp)
    
    tmp = tmp+nbrow*mb*nprow
    !tmp = tmp(tmp<m)
    ntmp = 0
    do ii = 1,mb,1
        if (tmp(ii,1)<m) then
            ntmp = ntmp+1
        end if
    end do
    allocate(tmptmp(ntmp,1))
    ntmp = 0
    do ii = 1,mb,1
        if (tmp(ii,1)<m) then
            ntmp           = ntmp+1
            tmptmp(ntmp,1) = tmp(ii,1)
        end if
    end do
    
    allocate(rowind(1,ntmp+mb*nbrow))
    do ii = 1,mb,1
        do jj = 1,nbrow,1
            rowind(1,(jj-1)*mb+ii) = rowindtmp(ii,jj)+1
        end do
    end do
    forall(ii=mb*nbrow+1:ntmp+mb*nbrow)
        rowind(1,ii) = tmptmp(ii-mb*nbrow,1)
    end forall
    
    !rowind(:,2) = tmp+1
    
    forall(ii=1:nb)
        colindtmp(ii) = ii-1
    end forall
    
    colindtmp = colindtmp+mycol*nb
    nbcol     = n/nb/npcol
    tmp3      = rowind
    tmp       = rowind
    forall(ii=1:nbcol)
        tmp4(1,ii) = ii-1
    end forall
    tmp4 = tmp4*mb*npcol
    
    call bsxfinPlus(tmp3,tmp4,colindtmp)
    tmp = tmp+nbcol*nb*npcol
    !tmp = tmp(tmp<n)
    colind(:,1) = colindtmp+1
    colind(:,2) = tmp+1
    
    return
    end subroutine GetGlobalInd
    end module GetGlobalInd_module