MatrixA = zeros(1,4);
MatrixB = zeros(3,1);
for ii = 1:3
    for jj = 1:4
        MatrixA(jj) = jj;
        MatrixB(ii) = ii;
    end
end
MatrixC = bsxfun(@times,MatrixA,MatrixB);
%save bsxfunEQ_3_4_testM.txt -ascii MatrixC
fid = fopen('bsxfunTimes_14_31_testM.txt','wt');
for ii = 1:3
    fprintf(fid,'%g %g %g %g\n',MatrixC(ii,:));
end
fclose(fid);