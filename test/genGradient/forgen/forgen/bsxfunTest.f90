!  forgen.f90 
!
!  FUNCTIONS:
!  forgen - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: forgen
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program forgen
    
    use bsxfunEQ_module
    use bsxfunPlus_module
    use bsxfunPower_module
    use bsxfunTimes_module

    implicit none

    ! Variables
    real*8, allocatable  :: MatrixA(:,:), MatrixB(:,:), MatrixC(:,:)
    !logical, allocatable :: MatrixC(:,:)
    integer              :: ii,jj

    ! Body of forgen
    print *, 'Hello World'
    allocate(MatrixA(1,4),MatrixB(3,1),MatrixC(3,4))
    
    forall(ii=1:3,jj=1:4)
        MatrixA(1,jj) = jj
        MatrixB(ii,1) = ii
    end forall
    
    call bsxfunTimes(MatrixA,MatrixB,MatrixC)
    
    open(unit=10,file='bsxfunTimes_14_31_test.txt',form='formatted',status='replace')
    
    do ii = 1,3,1
        write(10,*) MatrixC(ii,:)
    end do
    

    end program forgen

