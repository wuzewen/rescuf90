!  stencilTest.f90 
!
!  FUNCTIONS:
!  stencilTest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: stencilTest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program stencilTest

    use stencil_module
    
    implicit none

    ! Variables
    integer             :: order, acc
    real*8, allocatable :: coeff(:)

    ! Body of stencilTest
    print *, 'Hello World'
    order = 2;
    acc   = 10;
    allocate(coeff(acc+1))
    call stencil(order,acc,coeff)
    write(*,*) coeff

    end program stencilTest

