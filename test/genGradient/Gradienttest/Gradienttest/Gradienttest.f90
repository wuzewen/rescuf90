!  Gradienttest.f90 
!
!  FUNCTIONS:
!  Gradienttest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: Gradienttest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program Gradienttest
    
    use genGradient_module

    implicit none

    ! Variables
    integer :: acc, bc(3)
    real*8, allocatable :: Lu(:,:),  Lv(:,:),  Lw(:,:)
    real*8, allocatable :: Du(:,:),  Dv(:,:),  Dw(:,:), &
                          fDu(:,:), fDv(:,:), fDw(:,:)
    character(len=20)   :: method
    integer             :: cgridn(3),fgridn(3)

    ! Body of genLaplacian
    print *, 'Hello World'
    acc       = 16
    bc(1)     = 1
    bc(2)     = 1
    bc(3)     = 1
    method    = "fd"
    cgridn(1) = 12
    cgridn(2) = 13
    cgridn(3) = 14
    fgridn(1) = 12
    fgridn(2) = 13
    fgridn(3) = 14
    !allocate(Lu(cgridn(1)))
    !allocate(Lv(cgridn(2)))
    !allocate(Lw(cgridn(3)))
    allocate(Du(cgridn(1),cgridn(1)))
    allocate(Dv(cgridn(2),cgridn(2)))
    allocate(Dw(cgridn(3),cgridn(3)))
    allocate(fDu(fgridn(1),fgridn(1)))
    allocate(fDv(fgridn(2),fgridn(2)))
    allocate(fDw(fgridn(3),fgridn(3)))
    
    call genGradient(cgridn, fgridn, method, acc, bc, Du,Dv,Dw,fDu,fDv,fDw)
    open(unit=10,file='duuFor.txt')
    !write(10,*) "Du"
    Du = transpose(Du)
    write(10,*) Du
    close(10)
    
    open(unit=10,file='dvvFor.txt')
    !write(10,*) "Dv"
    write(10,*) Dv
    close(10)
    !write(*,*) Dv
    
    open(unit=10,file='dwwFor.txt')
    !write(10,*) "Dw"
    write(10,*) Dw
    close(10)
    
    open(unit=10,file='fduuFor.txt')
    !write(10,*) "fDu"
    write(10,*) fDu
    close(10)
    
    open(unit=10,file='fdvvFor.txt')
    !write(10,*) "fDv"
    write(10,*) fDv
    close(10)
    
    open(unit=10,file='fdwwFor.txt')
    !write(10,*) "fDw"
    write(10,*) fDw
    close(10)

    end program Gradienttest

