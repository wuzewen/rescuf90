! FRC.f90
    
    module FORTRAN_RESCU_CALCULATION_TYPE
    
    use mpiType_module
    use spinType_module
    use domainType_module
    use functionalType_module
    use symmetryType_module
    use infoType_module
    use energyType_module
    use potentialType_module
    use mixingType_module
    use FRC_RHO_TYPE
    use smiType_module
    use optionType_module
    use LCAOtype_module
    use initType_module
    use eigenSolverType_module
    use interpolationType_module
    use diffopType_module
    use AtomType_module
    use elementType_module
    use SummeryOfBas_module
    use ElementDataType_module
    use kpointType_module
    use forceType_module
    
    type :: FORTRAN_RESCU_CALCULATION
        integer                 :: scloop
        type(mpiType)           :: mpi
        type(spinType)          :: spin
        type(domainType)        :: domain
        type(functionalType)    :: functional
        type(symmetryType)      :: symmetry
        type(infoType)          :: info
        type(energyType)        :: energy
        type(potentialType)     :: potential
        type(mixingType)        :: mixing
        type(FRCrhoType)        :: rho
        type(smiType)           :: smi
        type(optionType)        :: option
        type(LCAOType)          :: LCAO
        type(initType)          :: init
        type(eigenSolverType)   :: eigenSolver
        type(interpolationType) :: interpolation
        type(diffopType)        :: diffop
        type(Atomtype)          :: Atom
        type(kpointType)        :: kpoint
        type(forceType)         :: force
        type(elementType),     allocatable :: element(:)
        type(SummeryOfBas),    allocatable :: SOB(:)
        type(ElementDataType), allocatable :: ElementData(:)
    end type FORTRAN_RESCU_CALCULATION
    
    type :: inteType
        integer, allocatable :: vata(:)
    end type inteType
    
    type :: realType
        real*8,  allocatable :: vata(:)
    end type realType
    
    end module FORTRAN_RESCU_CALCULATION_TYPE