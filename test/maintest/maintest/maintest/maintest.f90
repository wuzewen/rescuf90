!  maintest.f90 
!
!  FUNCTIONS:
!  maintest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: maintest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program maintest

    implicit none

    ! Variables

    ! Body of maintest
    print *, 'Hello World'

    end program maintest

