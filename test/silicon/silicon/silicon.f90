!  silicon.f90 
!
!  FUNCTIONS:
!  silicon - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: silicon
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program silicon

    implicit none

    ! Variables

    ! Body of silicon
    print *, 'Hello World'

    end program silicon

