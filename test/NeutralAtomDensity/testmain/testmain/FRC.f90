! FRC.f90
    
!******************************************
    !
    !
    !
!******************************************
    
    module FORTRAN_RESCU_CALCULATION_TYPE
    
    use ElementDataType_module
    use SummeryOfBas_module
    use Acell_type
    use smiType_module
    use eigensolverType_module
    use spinType_module
    use RHO_type
    use FRC_RHO_TYPE
    use AtomType_module
    use elementType_module
    
    
    type :: Potvnltype
        integer              :: kbsparse
        integer, allocatable :: evec(:), Lorb(:), Morb(:), Norb(:), Oorb(:), Sorb(:)
        real*8 , allocatable :: Rorb(:), Eorb(:), kbvec(:,:), Aorb(:), KBEnergy(:)
        type(AcellType), allocatable :: kbcell(:) 
    end type Potvnltype
    
    type :: potentialType
        type(Potvnltype) :: vnl
        logical          :: fourierinit, initParaReal
    end type potentialType
    
    type :: SuOfBa
        integer :: SizeOfvnl
    end type SuOfBa
    
    type :: mpitype
        integer :: mpisize, rank
        logical :: status
    end type mpitype
    
    type :: domainType
        integer :: fgridn(3)
        real*8  :: latvec(3,3)
    end type domainType
    
    type :: kpointType
        real*8, allocatable :: kdirect(:,:)
    end type kpointType
    
    type :: infoType
        character(len=20) :: calculationType
    end type infoType
    
    type :: optionType
        logical :: initParaReal
    end type optionType
    
    type :: FORTRAN_RESCU_CALCULATION
        type(elementtype), allocatable    :: element(:)
        type(potentialType)               :: potential
        type(AtomType)                    :: Atom
        type(SummeryOfBas), allocatable   :: SOB(:)
        type(mpitype)                     :: mpi
        type(domainType)                  :: domain
        type(kpointType)                  :: kpoint
        type(infoType)                    :: info
        type(optionType)                  :: option
        type(ElementDataType),allocatable :: ElementData(:)
        type(smiType)                     :: smi
        type(eigensolverType)             :: eigensolver
        type(spinType)                    :: spin
        type(RHOtype)                     :: Ratom
        type(FRCrhoType)                  :: rho
    end type FORTRAN_RESCU_CALCULATION
    
    type :: inteType
        integer, allocatable :: vata(:)
    end type inteType
     
    type :: realType
        real*8,  allocatable :: vata(:)
    end type realType
    
    end module FORTRAN_RESCU_CALCULATION_TYPE