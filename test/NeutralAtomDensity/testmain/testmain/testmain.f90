!  testmain.f90 
!
!  FUNCTIONS:
!  testmain - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: testmain
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program testmain

    use FORTRAN_RESCU_CALCULATION_TYPE
    !use GetNonLocalPotentialK_module
    use readSummeryOfBasis_module
    use readPseudoPotential_module
    use ElementDataType_module
    use GetNeutralAtomDensity_module

    implicit none

    ! Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer :: ii, tmp, m, n ,jj
    character(len=20) :: nonsense
    type(SummeryOfBas)    :: SOB
    type(ElementDataType) :: ElementData
    character(len=4)      :: ElementName

    ! Body of NonlocalPotential
    ! print *, 'Hello World'
    FRC%mpi%status  = .FALSE.
    FRC%mpi%rank    = 0
    FRC%mpi%mpisize = 1
    
    FRC%domain%fgridn(1) = 13
    FRC%domain%fgridn(2) = 13
    FRC%domain%fgridn(3) = 13
    
    FRC%domain%latvec(:,:) = 5.131267834761913
    FRC%domain%latvec(1,1) = 0
    FRC%domain%latvec(2,2) = 0
    FRC%domain%latvec(3,3) = 0
    
    allocate(FRC%atom%element(2))
    FRC%atom%element(1) = 1
    FRC%atom%element(2) = 1
    FRC%Atom%numberOfElement = 1
    FRC%Atom%numberOfAtom    = 2
    
    FRC%spin%ispin  = 1
    
    
    FRC%option%initParaReal = .FALSE.
    
    FRC%eigensolver%Nvalence = 8
    
    allocate(FRC%element(1))
    FRC%element(1)%Z = 4
    
    
    !FRC%potential%fourierinit  = .FALSE.
    !FRC%potential%initParaReal = .FALSE.
    
    allocate(FRC%atom%XYZ(2,3))
    FRC%atom%XYZ(1,1) = 1.282816958690478
    FRC%atom%XYZ(1,2) = 1.282816958690478
    FRC%atom%XYZ(1,3) = 1.282816958690478
    FRC%atom%XYZ(2,1) = 3.848450876071436
    FRC%atom%XYZ(2,2) = 3.848450876071436
    FRC%atom%XYZ(2,3) = 3.848450876071436
    
    allocate(FRC%SOB(FRC%Atom%numberOfElement))
    FRC%SOB(1)%SizeOfVnl = 4
    !allocate(FRC%element(1))
    
    
    
    !FRC%info%calculationType  = "dfpt"
    
    
    
    
!**********************************************************************************
!************************************************************** basis start
    open(unit=10,file='Si.bas',status='old',access='sequential',action='read')
    do ii = 1,32,1
        read(10,*) nonsense
    end do
    read(10,*) tmp
    allocate(SOB%orbitalsize(tmp))
    close(10)
    
    ElementName = "Si"
    call readSummeryOfBasis(ElementName,SOB)
    
    !allocate(ElementData(Vlocal(SizeOfVlocal)))
    allocate(ElementData%Vlocal%rrData(SOB%SizeOfVlocalrrData))
    allocate(ElementData%Vlocal%drData(SOB%SizeOfVlocaldrData))
    allocate(ElementData%Vlocal%vvData(SOB%SizeOfVlocalvvData))
    
    !allocate(ElementData(Rlocal(SizeOfRlocal)))
    allocate(ElementData%Rlocal%rrData(SOB%SizeOfRlocalrrData))
    allocate(ElementData%Rlocal%drData(SOB%SizeOfRlocaldrData))
    allocate(ElementData%Rlocal%rhoData(SOB%SizeOfRlocalrhoData))
    
    allocate(ElementData%Vnl(SOB%SizeOfVnl))
    do ii = 1,SOB%SizeOfVnl,1
        allocate(ElementData%Vnl(ii)%rrData(SOB%SizeOfVnlrrData))
        allocate(ElementData%Vnl(ii)%drData(SOB%SizeOfVnldrData))
        allocate(ElementData%Vnl(ii)%vvData(SOB%SizeOfVnlvvData))
        allocate(ElementData%Vnl(ii)%qqData(SOB%SizeOfVnlqqData))
        allocate(ElementData%Vnl(ii)%fqData(SOB%SizeOfVnlfqData))
        allocate(ElementData%Vnl(ii)%qwData(SOB%SizeOfVnlqwData))
    end do
    
    !allocate(ElementData(Vna(SizeOfVlocal)))
    allocate(ElementData%Vna%rrData(SOB%SizeOfVnarrData))
    allocate(ElementData%Vna%drData(SOB%SizeOfVnadrData))
    allocate(ElementData%Vna%vvData(SOB%SizeOfVnavvData))
    
    !allocate(ElementData(Rna(SizeOfRlocal)))
    allocate(ElementData%Rna%rrData(SOB%SizeOfRnarrData))
    allocate(ElementData%Rna%drData(SOB%SizeOfRnadrData))
    allocate(ElementData%Rna%rhoData(SOB%SizeOfRnarhoData))
    
    allocate(ElementData%RelPseudoP(SOB%SizeOfRelPseudoP))
    do ii = 1,SOB%SizeOfRelPseudoP,1
        allocate(ElementData%RelPseudoP(ii)%rrData(SOB%SizeOfRelPseudoPrrData))
        allocate(ElementData%RelPseudoP(ii)%drData(SOB%SizeOfRelPseudoPdrData))
        allocate(ElementData%RelPseudoP(ii)%vvData_screened(SOB%SizeOfRelPseudoPvvData_screened))
        allocate(ElementData%RelPseudoP(ii)%vvData_unscreened(SOB%SizeOfRelPseudoPvvData_unscreened))
    end do
    
    allocate(ElementData%OrbitalSet(SOB%SizeOfOrbitalSet))
    do ii = 1,SOB%SizeOfOrbitalSet,1
        allocate(ElementData%OrbitalSet(ii)%rrData(SOB%orbitalsize(ii)%SizeOfOrbitalSetrrData))
        allocate(ElementData%OrbitalSet(ii)%drData(SOB%orbitalsize(ii)%SizeOfOrbitalSetdrData))
        allocate(ElementData%OrbitalSet(ii)%frData(SOB%orbitalsize(ii)%SizeOfOrbitalSetfrData))
        allocate(ElementData%OrbitalSet(ii)%qqData(SOB%orbitalsize(ii)%SizeOfOrbitalSetqqData))
        allocate(ElementData%OrbitalSet(ii)%fqData(SOB%orbitalsize(ii)%SizeOfOrbitalSetfqData))
        allocate(ElementData%OrbitalSet(ii)%qwData(SOB%orbitalsize(ii)%SizeOfOrbitalSetqwData))
    end do
    
    call readPseudoPotential(SOB,ElementData)
    !allocate(FRC%SOB(1))
    allocate(FRC%ElementData(1))
    FRC%ElementData(1)        = ElementData
    FRC%SOB(1)                = SOB
!****************************************************************   basis end
!******************************************************************************
    
    
    call GetNeutralAtomDensity(FRC)
    
    
    
    
    
    !open(unit=11,file='vnl_Arob_for.txt',status='replace',access='sequential',action='write')
    !tmp = size(FRC%potential%vnl%Aorb)
    !do ii = 1,tmp,1
    !    write(11,*) FRC%potential%vnl%Aorb(ii)
    !end do
    !close(11)
    
    !open(unit=11,file='vnl_KBE_for.txt',status='replace',access='sequential',action='write')
    !tmp = size(FRC%potential%vnl%KBEnergy)
    !do ii = 1,tmp,1
    !    write(11,*) FRC%potential%vnl%KBEnergy(ii)
    !end do
    !close(11)
    
    !open(unit=11,file='kbcell_8_col_for.txt',status='replace',access='sequential',action='write')
    !open(unit=12,file='kbcell_8_row_for.txt',status='replace',access='sequential',action='write')
    !open(unit=13,file='kbcell_8_val_for.txt',status='replace',access='sequential',action='write')
    !tmp = 8
    !m = size(FRC%potential%vnl%kbcell(tmp)%vata,1)
    !n = size(FRC%potential%vnl%kbcell(tmp)%vata,2)
    !do ii = 1,n,1
    !    do jj = 1,m,1
    !        if (FRC%potential%vnl%kbcell(tmp)%vata(jj,ii) /= 0) then
    !            write(11,*) jj
    !            write(12,*) ii
    !            write(13,*) FRC%potential%vnl%kbcell(tmp)%vata(jj,ii)
    !        end if
    !    end do
    !end do
    !close(11)
    !close(12)
    !close(13)

    end program testmain

