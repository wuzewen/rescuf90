!  type_test.f90 
!
!  FUNCTIONS:
!  type_test - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: type_test
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program type_test

    implicit none

    ! Variables

    ! Body of type_test
    print *, 'Hello World'

    end program type_test

