!  GetKBOrbitalInfo.f90 
!
!  FUNCTIONS:
!  GetKBOrbitalInfo - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: GetKBOrbitalInfo
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program GetKBOrbitalInfo_test
    
    use GetKBOrbitalInfo_module
    use FORTRAN_RESCU_CALCULATION_TYPE

    implicit none

    ! Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer :: ii
    

    ! Body of GetKBOrbitalInfo
    print *, 'Hello World'
    FRC%Atom%numberOfElement = 1
    FRC%atom%numberOfAtom    = 2
    allocate(FRC%element(FRC%Atom%numberOfElement))
    allocate(FRC%SOB(1))
    FRC%SOB(1)%SizeOfVnl     = 4
    !allocate(FRC%element(1)%vnlrnu(4))
    !allocate(FRC%element(1)%vnlenu(4))
    !allocate(FRC%element(1)%vnllnu(4))
    !allocate(FRC%element(1)%vnlnnu(4))
    
    forall(ii=1:4)
        FRC%element(1)%vnllnu(ii) = ii-1 
        FRC%element(1)%vnlnnu(ii) = 1
    end forall
    
    FRC%element(1)%vnlrnu(1)  = 1.865161693277237
    FRC%element(1)%vnlrnu(2)  = 1.865161693277237   
    FRC%element(1)%vnlrnu(3)  = 1.865161693277237 
    FRC%element(1)%vnlrnu(4)  = 1.865161693277237
    
    FRC%element(1)%vnlenu(1)  = 3.167134177963655   
    FRC%element(1)%vnlenu(2)  = 1.265352423464108  
    FRC%element(1)%vnlenu(3)  = -0.873213959025991  
    FRC%element(1)%vnlenu(4)  = -0.248405873879871
    
    FRC%element(1)%vnlLcutoff = 3
    FRC%element(1)%vnlNcutoff = 2
    
    allocate(FRC%atom%element(2))
    FRC%atom%element(:) = 1
    
    call GetKBOrbitalInfo(FRC)
    
    
    write(*,*) FRC%potential%vnl%evec
    write(*,*) FRC%potential%vnl%Eorb
    write(*,*) FRC%potential%vnl%Lorb
    write(*,*) FRC%potential%vnl%Morb
    write(*,*) FRC%potential%vnl%Norb
    write(*,*) FRC%potential%vnl%Oorb
    write(*,*) FRC%potential%vnl%Rorb
    write(*,*) FRC%potential%vnl%Sorb
    

    end program GetKBOrbitalInfo_test

