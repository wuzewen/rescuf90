! cellfunnnz.f90

!***************************************************************************
    !
    ! This function is used to count the number non-zero element in 
    ! AcellType data (now, only in the type of data). If the type is 
    !changed, this function should be fixed.
    !
!***************************************************************************
    
    module cellfunnnz2_module
    contains
    subroutine cellfunnnz2(Acell,spnnz)
    
    use Acell_type
    
    implicit none
    
    ! input variables
    type(AcellType), allocatable :: Acell(:,:)
    
    ! temporary variables
    integer :: m, n, ii, jj
    
    ! output variables
    integer :: spnnz
    
    ! body of this function
    n     = size(Acell,1)
    m     = size(Acell,2)
    spnnz = 0
    do ii = 1,n,1
        do jj = 1,m,1
            spnnz = spnnz + count(Acell(ii,jj)%vata /= 0)
        end do
    end do
    
    return
    end subroutine cellfunnnz2
    end module cellfunnnz2_module