mrc = load('FRC.mat');
nkb = size(mrc.FRC.potential.vnl.kbcell);
for ii = 1:nkb(1)
    vata    = mrc.FRC.potential.vnl.kbcell{nkb(1)}.data;
    [a,b,c] = find(vata);
    name = strcat('vnl_kbcell_',num2str(ii),'_col_mat.txt');
    save(name,a);
    %name    = ['vnl_kbcell_',name1,'_row_mat.txt'];
    %save(name,b,'-ascii')
    %name    = ['vnl_kbcell_',name1,'_val_mat.txt'];
    %save(name,c,'-ascii')
end
