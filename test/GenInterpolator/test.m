mrc.interpolation.method = 'lagrange';
mrc.interpolation.order  = 6;
mrc.domain.boundary      = [1,1,1];
mrc.domain.cgridn        = [12,13,14];
mrc.domain.fgridn        = [12,13,14];

[intx,inty,intz]         = GenInterpolator(mrc);