! readinputfile.f90
!*************************************
    !
!*************************************
    
    module readinputfile_module
    contains
    subroutine readinputfile(nline,inputFromFile)
    
    use inputtype
    
    implicit none
    
    ! input variables
    integer              :: nline
    
    ! temporary variables
    character(len=50)    :: nonsense
    integer              :: ii
    
    ! output variables
    type(inputFromFileType), allocatable :: inputFromFile(:)
    
    ! body of this function
    open(unit=10,file='control.input',status='old',access='sequential',action='read')
    do ii = 1,nline,1
        read(10,*) inputFromFile(ii)%name,nonsense,inputFromFile(ii)%value
    end do
    close(10)
    
    return
    end subroutine readinputfile
    end module readinputfile_module