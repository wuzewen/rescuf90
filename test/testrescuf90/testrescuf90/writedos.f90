!
    module writedos_module
    contains
    subroutine writedos(FRC)
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    implicit none
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    open(unit=10,file='hello.txt')
    write(10,*) "hello from writedos"
    close(10)
    write(*,*) "hello from writedos"
    return
    end subroutine writedos
    end module writedos_module