!
    module rescu_relax_module
    contains
    subroutine rescu_relax(FRC)
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    implicit none
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    open(unit=10,file='hello.txt')
    write(10,*) "hello from rescu_relax"
    close(10)
    write(*,*) "hello from rescu_relax"
    return
    end subroutine rescu_relax
    end module rescu_relax_module