!
    module init_structure_module
    contains
    subroutine init_structure(FRC)
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    implicit none
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    open(unit=10,file='hello.txt')
    write(10,*) "hello from init_structure"
    close(10)
    write(*,*) "hello from init_structure"
    return
    end subroutine init_structure
    end module init_structure_module