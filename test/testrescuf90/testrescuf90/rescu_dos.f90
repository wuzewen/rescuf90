!
    module rescu_dos_module
    contains
    subroutine rescu_dos(FRC)
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    implicit none
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    open(unit=10,file='hello.txt')
    write(10,*) "hello from rescu_dos"
    close(10)
    write(*,*) "hello from rescu_dos"
    return
    end subroutine rescu_dos
    end module rescu_dos_module