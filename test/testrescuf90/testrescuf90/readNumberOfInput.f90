! readNumberOfInput.f90
!*****************************************************************
    
    ! This is a function which can read the number of lines in
    ! the input file, control.input.
    
!*****************************************************************
    
    subroutine readNumberOfInput(nline,nvalue)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    integer            :: val
    character(len=100) :: nonsense
    
    ! output variables
    integer            :: nline, nvalue
    
    ! body of this function
    open(unit=10,file='control.input',status='old',access='sequential',action='read')
    val    = 0
    nline  = 0
    nvalue = 0
    do while(val==0)
        read(10,fmt="(A79)",iostat=val) nonsense
        if (val==0) then
            nline = nline+1
            if (len_trim(nonsense)/=0) then
                nvalue = nvalue+1
            end if
        else if (val>0) then
            write(*,*) "Wrong Input File"
        else
            exit
        end if
    end do
    close(10)
    
    return
    end subroutine readNumberOfInput