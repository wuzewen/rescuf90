!
    module rescu_force_module
    contains
    subroutine rescu_force(FRC)
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    implicit none
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    open(unit=10,file='hello.txt')
    write(10,*) "hello from rescu_force"
    close(10)
    write(*,*) "hello from rescu_force"
    return
    end subroutine rescu_force
    end module rescu_force_module