!
    module rescu_dftp_module
    contains
    subroutine rescu_dftp(FRC)
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    implicit none
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    open(unit=10,file='hello.txt')
    write(10,*) "hello from rescu_dftp"
    close(10)
    write(*,*) "hello from rescu_dftp"
    return
    end subroutine rescu_dftp
    end module rescu_dftp_module