!  testReadNumberOfInput.f90 
!
!  FUNCTIONS:
!  testReadNumberOfInput - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: testReadNumberOfInput
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program testReadNumberOfInput

    implicit none

    ! Variables
    integer :: nline, nvalue

    ! Body of testReadNumberOfInput
    call readNumberOfInput(nline,nvalue)
    write(*,*) nline
    write(*,*) nvalue

    end program testReadNumberOfInput

