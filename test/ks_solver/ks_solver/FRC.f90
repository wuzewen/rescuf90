! FRC.f90
    
!***************************************************
    !
    !
    !
!***************************************************
    
    module FORTRAN_RESCU_CALCULATION_TYPE
    
    use domainType_module
    use kpointType_module
    use eigenSolverType_module
    use spinType_module
    use mpiType_module
    use mixingType_module
    use potentialType_module
    use optionType_module
    use VataMN_2D_module
    use energyType_module
    use smiType_module
    use interpolationType_module
    use diffopType_module
    
    type :: FORTRAN_RESCU_CALCULATION
        integer                 :: scloop
        type(domainType)        :: domain
        type(kpointType)        :: kpoint
        type(eigensolverType)   :: eigenSolver
        type(spinType)          :: spin
        type(mpiType)           :: mpi
        type(mixingType)        :: mixing
        type(potentialType)     :: potential
        type(optionType)        :: option
        type(energyType)        :: energy
        type(smiType)           :: smi
        type(interpolationType) :: interpolation
        type(diffopType)        :: diffop
        type(VataMN_2D), allocatable :: psi(:,:)
    end type FORTRAN_RESCU_CALCULATION
    
    end module FORTRAN_RESCU_CALCULATION_TYPE