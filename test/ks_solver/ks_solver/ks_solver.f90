!  ks_solver.f90 
!
!  FUNCTIONS:
!  ks_solver - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: ks_solver
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program ks_solver

    use FORTRAN_RESCU_CALCULATION_TYPE
    use KS_main_real_scf_module
    
    implicit none

    ! Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer                         :: ii

    ! Body of ks_solver
    print *, 'Hello World'
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    FRC%scloop = 1
    
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Domain
    FRC%domain%cgridn      = 13
    FRC%domain%fgridn      = 13
    FRC%domain%latvec(:,:) = 5.131267834761913
    FRC%domain%latvec(1,1) = 0
    FRC%domain%latvec(2,2) = 0
    FRC%domain%latvec(3,3) = 0
    
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Kpoint
    allocate(FRC%kpoint%ikdirect(10,3))
    open(unit=10,file='kdir.txt',status='old',access='sequential',action='read')
    do ii = 1,10,1
        read(10,*) FRC%kpoint%ikdirect(ii,:)
    end do
    close(10)

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% eigensolver
    FRC%eigensolver%nband      = 18
    FRC%eigensolver%maxit      = 15
    FRC%eigensolver%tol        = 0.01
    FRC%eigensolver%maxRestart = 1
    FRC%eigensolver%precond    = 4
    FRC%eigensolver%nsym       = 18
    FRC%eigensolver%algo       = "lobpcg"
    
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% spin
    FRC%spin%nspin = 1
    FRC%spin%ispin = 1
    
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% mpi
    FRC%mpi%status  = .FALSE.
    FRC%mpi%mpisize = 1
    
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% potential
    allocate(FRC%potential%vps%vata(2197))
    open(unit=10,file='potentialVPS.txt',status='old',access='sequential',action='read')
    do ii = 1,2197,1
        read(10,*) FRC%potential%vps%vata(ii)
    end do
    close(10)
    FRC%potential%vps%m      = 2197
    FRC%potential%vps%n      = 1
    FRC%potential%vps%mblock = 2197
    FRC%potential%vps%nblock = 1
    FRC%potential%vps%mproc  = 2197
    FRC%potential%vps%nproc  = 1
    allocate(FRC%potential%veffin(2))
    allocate(FRC%potential%veffin(1)%vata(2197))
    open(unit=10,file='veffin1.txt',status='old',access='sequential',action='read')
    do ii = 1,2197,1
        read(10,*) FRC%potential%veffin(1)%vata(ii)
    end do
    close(10)
    FRC%potential%veffin(1)%m      = 2197
    FRC%potential%veffin(1)%n      = 1
    FRC%potential%veffin(1)%mblock = 2197
    FRC%potential%veffin(1)%nblock = 1
    FRC%potential%veffin(1)%mproc  = 2197
    FRC%potential%veffin(1)%nproc  = 1
    
    
    call KS_main_real_scf(FRC)
    
    end program ks_solver

