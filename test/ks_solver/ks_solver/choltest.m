A = [4,12,-16;12,37,-43;-16,-43,98];

[m,n] = size(A);
if m ~= n
    exit;
end

L = zeros(m,n);

for ii = 1:m
    for jj = ii:n
        Atmp = sum(L(ii,:).*L(jj,:));
        if ii == jj
            L(jj,ii) = sqrt(A(ii,jj)-Atmp);
        else
            L(jj,ii) = (A(ii,jj)-Atmp)/L(ii,ii);
        end
    end
end