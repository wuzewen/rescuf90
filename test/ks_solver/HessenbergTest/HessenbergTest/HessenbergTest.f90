!  HessenbergTest.f90 
!
!  FUNCTIONS:
!  HessenbergTest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: HessenbergTest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program HessenbergTest

    use general2HessenbergMatrix_module
    implicit none

    ! Variables
    integer :: N
    real*8, allocatable :: A(:,:), B(:,:)

    ! Body of HessenbergTest
    print *, 'Hello World'
    N = 5
    allocate(A(N,N),B(N,N))
    A(1,1) =  1.0
    A(1,2) =  6.0
    A(1,3) = -3.0
    A(1,4) = -1.0
    A(1,5) =  7.0
    
    A(2,1) =  8.0
    A(2,2) = -15.0
    A(2,3) =  18.0
    A(2,4) =  5.0
    A(2,5) =  4.0
    
    A(3,1) = -2.0
    A(3,2) =  11.0
    A(3,3) =  9.0
    A(3,4) =  15.0
    A(3,5) =  20.0
    
    A(4,1) = -13.0
    A(4,2) =  2.0
    A(4,3) =  21.0
    A(4,4) =  30.0
    A(4,5) = -6.0
    
    A(5,1) =  17.0
    A(5,2) =  22.0
    A(5,3) = -5.0
    A(5,4) =  3.0
    A(5,5) =  6.0
    
    call general2HessenbergMatrix(A,B)
    
    write(*,*) B

    end program HessenbergTest

