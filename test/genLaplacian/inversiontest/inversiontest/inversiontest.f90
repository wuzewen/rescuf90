!  inversiontest.f90 
!
!  FUNCTIONS:
!  inversiontest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: inversiontest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program inversiontest

    use inversion_module
    
    implicit none

    ! Variables
    integer             :: n, i
    real*8, allocatable :: A(:,:), B(:,:)

    ! Body of inversiontest
    print *, 'Hello World'
    n = 4
    allocate(A(n,n),B(n,n))
    
    forall(i=1:n)
        A(i,i) = i*i
    end forall
    
    call inversion(A,B)
    open(unit=10,file='inversiontest.txt')
    do i=1,n,1
        write(10,*) B(i,:)
    end do
    close(10)

    end program inversiontest

