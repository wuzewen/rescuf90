!  stenciltest.f90 
!
!  FUNCTIONS:
!  stenciltest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: stenciltest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program stenciltest

    use stencil_module
    
    implicit none

    ! Variables
    integer :: order, acc
    real*8, allocatable :: coeff(:)
    

    ! Body of stenciltest
    print *, 'Hello World'
    order = 2
    acc   = 10
    allocate(coeff(acc+1))
    call stencil(order,acc,coeff)
    
    !open(unit=10,file='stenciltest_order2.txt')
    write(*,*) coeff
    !close(10)

    end program stenciltest

