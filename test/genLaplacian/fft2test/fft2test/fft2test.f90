!  fft2test.f90 
!
!  FUNCTIONS:
!  fft2test - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: fft2test
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program fft2test
    
    use fft2_module

    implicit none

    ! Variables
    real*8, allocatable  :: A(:,:)
    integer              :: dim, ii, nu
    complex, allocatable :: B(:,:)

    ! Body of fft2test
    print *, 'Hello World'
    nu  = 12
    dim = 2
    allocate(A(nu,nu),B(nu,nu))
    forall(ii=1:nu)
        A(ii,ii) = ii
    end forall
    
    call fft2(A,dim,B)
    write(*,*) B

    end program fft2test

