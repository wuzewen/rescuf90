!  fftfreq_test.f90 
!
!  FUNCTIONS:
!  fftfreq_test - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: fftfreq_test
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program fftfreq_test

    use fftFreq_module
    
    implicit none

    ! Variables
    integer            :: ii, gridn(3)
    real*8             :: bvec(3,3)
    real*8,allocatable :: ku(:,:,:), kv(:,:,:), kw(:,:,:)

    ! Body of fftfreq_test
    print *, 'Hello World'
    forall(ii=1:3)
        bvec(ii,ii) = 1
    end forall
    gridn(1) = 12
    gridn(2) = 1
    gridn(3) = 1
    
    allocate(ku(gridn(1),1,1))
    allocate(kv(gridn(1),1,1))
    allocate(kw(gridn(1),1,1))
    
    call fftFreq(gridn,bvec,ku,kv,kw)
    write(*,*) ku

    end program fftfreq_test

