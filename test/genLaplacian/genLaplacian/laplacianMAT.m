mrc.diffop.accuracy = 16;
mrc.domain.boundary = [1,1,1];
mrc.diffop.method   = 'fd';
mrc.domain.cgridn   = [12,13,14];
mrc.domain.fgridn   = [12,13,14];

mrc = GenLaplacian(mrc);