! leftdivision.f90

!******************************************
    !
    !
    !
!******************************************
    
    module leftdivision_module
    contains
    subroutine leftdivision(A,B,C)
    
    use inversion_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    real*8, allocatable :: INVA(:,:)
    integer             :: n
    
    ! output variables
    real*8, allocatable :: C(:,:)
    
    ! body of this function
    n = size(A,1)
    allocate(INVA(n,n))
    
    call inversion(A,INVA)
    C = matmul(INVA,B)
    
    return
    end subroutine leftdivision
    end module leftdivision_module