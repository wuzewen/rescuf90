!  leftdivisiontest.f90 
!
!  FUNCTIONS:
!  leftdivisiontest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: leftdivisiontest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program leftdivisiontest

    use leftdivision_module
    
    implicit none

    ! Variables
    integer :: n, i
    real*8, allocatable :: A(:,:), B(:,:), C(:,:)

    ! Body of leftdivisiontest
    print *, 'Hello World'

    n = 4
    allocate(A(n,n),B(n,1),C(n,1))
    
    forall(i=1:n)
        A(i,i) = i*i
        B(i,1) = i*2
    end forall
    
    call leftdivision(A,B,C)
    open(unit=10,file='leftdivisiontest.txt')
    do i=1,n,1
        write(10,*) C(i,:)
    end do
    close(10)
    
    end program leftdivisiontest

