        !COMPILER-GENERATED INTERFACE MODULE: Sat Oct 28 20:07:02 2017
        MODULE NDGRID__genmod
          INTERFACE 
            FUNCTION NDGRID(KU,KV,KW,DIM)
              REAL(KIND=4) ,ALLOCATABLE :: KU(:)
              REAL(KIND=4) ,ALLOCATABLE :: KV(:)
              REAL(KIND=4) ,ALLOCATABLE :: KW(:)
              INTEGER(KIND=4) :: DIM
              REAL(KIND=4) ,ALLOCATABLE :: NDGRID(:,:,:)
            END FUNCTION NDGRID
          END INTERFACE 
        END MODULE NDGRID__genmod
