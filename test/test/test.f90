!  test.f90 
!
!  FUNCTIONS:
!  test - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: test
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program test
    use fftFreq_module
    implicit none

    ! Variables
    integer          :: gridn(3), i, j, k, dd(9,9), a(4)
    real             :: bvec(3,3)
    real,allocatable :: ku(:,:,:), kv(:,:,:), kw(:,:,:)

    ! Body of test
    gridn(1) = 3
    gridn(2) = 4
    gridn(3) = 2
    bvec(:,1) = 1
    bvec(:,2) = 2
    bvec(:,3) = 3
    allocate(ku(gridn(1),gridn(2),gridn(3)),kv(gridn(1),gridn(2),gridn(3)),kw(gridn(1),gridn(2),gridn(3)))
    call fftFreq(gridn,bvec,ku,kv,kw)
    open(unit=10,file="ku.txt",status='replace',action='write')
    open(unit=11,file="kv.txt",status='replace',action='write')
    open(unit=12,file="kw.txt",status='replace',action='write')
    do i = 1,gridn(3)
        do j = 1,gridn(2)
            do k = 1,gridn(1)
                write(10,*) ku(k,j,i)
                write(11,*) kv(k,j,i)
                write(12,*) kw(k,j,i)
            end do
        end do
    end do

    !forall (i=2:5,j=4:7) 
    !    dd(i,j) = 1
    !end forall
    
    a(1) = 3
    a(2) = 5
    a(3) = 7
    a(4) = 9
    do a(i)
        dd(i,i) = i
    end do
    
    do i=1,9,1
        write(*,*) dd(i,:)
    end do
    
    end program test

