! FRC.f90
    
!**************************
    !
    !
    !
!**************************
    
    module FORTRAN_RESCU_CALCULATION_TYPE
    
    use AtomType_module
    use DomainType_module
    use mpitype_module
    use functionalType_module
    use forceType_module
    use optionType_module
    use FRC_RHO_TYPE
    use smiType_module
    
    type :: FORTRAN_RESCU_CALCULATION
        type(AtomType)       :: Atom
        type(DomainType)     :: domain
        type(mpiType)        :: mpi
        type(functionalType) :: functional
        type(forceType)      :: force
        type(optionType)     :: option
        type(FRCrhoType)     :: rho
        type(smiType)        :: smi
    end type FORTRAN_RESCU_CALCULATION
    
    end module FORTRAN_RESCU_CALCULATION_TYPE