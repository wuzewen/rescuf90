!  maintest.f90 
!
!  FUNCTIONS:
!  maintest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: maintest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program maintest
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use getPartialCoreDensity_module

    implicit none

    ! Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC

    ! Body of maintest
    print *, 'Hello World'
    
    FRC%mpi%status  = .FALSE.
    FRC%mpi%rank    = 0
    FRC%mpi%mpisize = 1
    
    FRC%atom%numberOfAtom = 2
    
    FRC%domain%fgridn(1) = 13
    FRC%domain%fgridn(2) = 13
    FRC%domain%fgridn(3) = 13
    
    FRC%domain%latvec(:,:) = 5.131267834761913
    FRC%domain%latvec(1,1) = 0
    FRC%domain%latvec(2,2) = 0
    FRC%domain%latvec(3,3) = 0
    
    FRC%force%status        = .TRUE.
    FRC%option%initParaReal = .FALSE.
    allocate(FRC%functional%list(2))
    FRC%functional%list(1)  = 'XC_LDA_X'
    FRC%functional%list(2)  = 'XC_LDA_C_PW'
    
    call getPartialCoreDensity(FRC)

    end program maintest

