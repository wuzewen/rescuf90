!  characterTest.f90 
!
!  FUNCTIONS:
!  characterTest - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: characterTest
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program characterTest

    use parseFuncName_module
    
    implicit none

    ! Variables
    character(len=20), allocatable :: a(:), faprx(:)

    ! Body of characterTest
    print *, 'Hello World'
    allocate(a(2))
    a(1) = 'xc_lda_x'
    a(2) = 'xc_lda_c'
    
    call parseFuncName(a,faprx)
    
    write(*,*) faprx(1), faprx(2)

    end program characterTest

