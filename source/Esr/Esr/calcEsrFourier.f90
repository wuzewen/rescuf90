!  Esr.f90 
!
!  FUNCTIONS:
!  Esr - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: Esr
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module calcEsrFourier_module
    contains
    subroutine calcEsrFourier(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical :: mpistat
    integer :: mpirank, buff, nelem, natom, smimb, sminb, smimp, sminp
    real*8  :: avec(3,3)
    real*8,  allocatable :: xyz(:,:)
    integer, allocatable :: velem(:)
    
    ! output variables
    
    ! Body of calEsrFourier
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    buff    = FRC%option%bufferSize
    
    if (mpirank == 0) then
        write(*,*) "output information"
    end if
    
    avec  = FRC%domain%latvec
    natom = FRC%atom%numberOfAtom
    xyz   = FRC%atom%xyz
    velem = FRC%atom%element
    nelem = FRC%atom%numberOfElement
    smimb = FRC%SMI%mb
    sminb = FRC%SMI%nb
    smimp = FRC%SMI%mp
    sminp = FRC%SMI%np
    smimb = min(smimb,ceiling(natom/smimp))
    sminb = min(sminb,ceiling(natom/sminp))
    call gengauntmatrix(0,gauntmat)
    
    q = FRC%elementDATA(1)%OrbitalSet(1)%qqData
    w = FRC%elementDATA(1)%OrbitalSet(1)%qwData
    allocate(Rorb(nelem),Zorb(nelem))
    Rorb(:) = 0
    Zorb(:) = 0
    
    allocate(fqmat1(FRC%SOB%RnarrDataSize,NumSpecies,NumSpecies))
    do ii = 1,NumSpecies
        tmp  = FRC%elementDATA(ii)%Rna
        call radialFT(tmp%rrData,4*pi*tmp%rhoData,tmp%drData,q,fqii)
        Rorb(ii) = max(tmp%rrData)
        call trapz(tmp%rrData,4*pi*tmp%rrData*tmp%rrData*tmp%rhoData,Zorb(ii))
        Zorb(ii) = 4*pi*Zorb(ii)
        
        do jj = 1,NumSpecies
            tmp = FRC%elementDATA(jj)%Rna
            call radialFT(tmp%rrData,4*pi*tmp%rhoData,tmp%drData,q,fqjj)
            fqmat1(:,ii,jj) = fqii*fqjj
        end do
    end do
    
    szfq(1) = size(q)
    szfq(2) = NumSpecies
    szfq(3) = NumSpecies
    
    allocate(fqmat2(NumSpecies*NumSpecies,szfq(1)))
    fqmat2 = transpose(reshape(fqmat1,(/szfq(1),NumSpecies*NumSpecies/)))
    
    rad = 2*maxval(Rorb)
    call cellInRange(avec,xyz,rad,1,txyz)
    nt = size(txyz,1)
    
    call InitDistArray(natom,natom,smimb,sminb,smimp,sminp,sboole)
    call GetGlobalInd(mpirank,sboole,iloc,jloc)
    
    maxrad = 2*maxval(Rorb)*maxval(q)*1.05
    call linspace(0,maxrad,ceiling(maxrad/0.05),juvrad)
    call calcSphericalBessel(0,juvrad,juvval)
    call radialGradient(juvrad,juvval,djuvval)
    
    allocate(Fsr(natom,3))
    Esr      = 0
    Fsr(:,:) = 0
    ni       = size(iloc)
    nj       = size(jloc)
    
    if (ni*nj>0) then
        Zorbi = Zorb(iloc)
        Zorbj = Zorb(jloc)
        call spdiags(Zorbi,0,ni,ni,Zmati)
    end if
    
    do jj = 1,nt,1
        xyz1 = xyz(iloc,:)
        call bsxfunPlus(xyz(jloc,:),matmul(txyz(ii,:),avec),xyz2)
        call calcLocalBooleanOverlap(xyz1,xyz2,iRorb,jRorb,slog)
        if (any(slog/=0)) then
            call findNoneZero(slog,fslogi,fslogj)
            blks   = ceiling(buff/size(q)/32)
            nn     = size(fslogi)
            forall(ii=1:nn)
                colvec(ii) = ii
            end forall
            colvec = int((colvec-1E-12)/blks)+1
            do ii = 1,maxval(colvec)
                call findNumber(colvec,ii,nn)
                allocate(tmp(nn))
                call locate(colvec,ii,tmp)
                allocate(rijmat(nn,3))
                do kk = 1,nn,1
                    slogi(kk)    = fslogi(tmp(kk))
                    slogj(kk)    = fslogj(tmp(kk))
                    rijmat(kk,:) = xyz1(slogi(kk),:)-xyz2(slogj(kk),:)
                end do
                tmp(:) = 0
                call getsphharm(0,tmp,rijmat,gaunt)
                gaunt  = gaunt*gauntmat(1)
                
                dijmat = sqrt(sum(rijmat*rijmat,2))
                
                call bsxfunTimes(dijmat,q,tmp)
                call interp1(juvrad,juvval,tmp,"spline",0,juv)
                szfqtmp(1) = NumSpecies
                szfqtmp(2) = NumSpecies
                allocate(velemi(ni),velemj(nj))
                do kk =1,ni,1
                    velemi(kk) = velem(iloc(slogi(kk)))
                    velemj(kk) = velem(jloc(slogj(kk)))
                end do
                allocate(Rq12(nnnnn))
                call sub2ind(szqtmp,velemi,velemj,Rq12)
                allocate(temp(nnnnn,NumSpecies*NumSpecies))
                do kk = 1,nnnnn,1
                    temp(kk,:) = fqmat2(Rq12(kk),:)
                end do
                temp = temp*juv
                temp = 4*pi*gaunt*matmul(temp,transpose(q*w))
                
                tmpx = temp*rijmat(:,1)/dijmat
                do kk = 1,ni,1
                    Fsr(iloc(kk),1) = Fsr(iloc(kk),1)+sum(tmp(kk,:))
                end do
                
                tmpx = temp*rijmat(:,2)/dijmat
                do kk = 1,ni,1
                    Fsr(iloc(kk),2) = Fsr(iloc(kk),2)+sum(tmp(kk,:))
                end do
                
                tmpx = temp*rijmat(:,3)/dijmat
                do kk = 1,ni,1
                    Fsr(iloc(kk),3) = Fsr(iloc(kk),3)+sum(tmp(kk,:))
                end do
            end do
        end if
        
        if (.not. any(txyz(jj,:))) then
            allocate(tmp(ntmp))
            call bsxfunEq(iloc,transpose(jloc),tmp)
            do ii = 1,ntmp,1
                slog(tmp(ii)) = .FALSE.
            end do
        end if
    
        if (.not. all(slog == 0)) then
            call findNoneZerosNumber(slog,nx)
            allocate(slogi(nx),slogj(nx))
            call findNoneZeros(slog,slogi,slogj)
            allocate(dist(nx,3))
            do ii = 1,nx,1
                dist(ii,:) = xyz1(slogi(ii),:)
                dist(ii,:) = dist(ii,:)-xyz2(slogj(ii),:)
            end do
            
            allocate(distx(nx),disty(nx),distz(nx))
            distx = dist(:,1)
            disty = dist(:,2)
            distz = dist(:,3)
            dist  = sqrt(sum(dist*dist,2))
            call  sparse(slogi,slogj,1/dist,ni,nj,tmps)
            Esr   = Esr + matmul(matmul(transpose(Zorbi),tmps),Zorbj)
            
            call sparse(slogi,slogj,distx/(dist*dist*dist),ni,nj,tmps)
            Fsr(iloc,1) = Fsr(iloc,1)+matmul(Zmati,matmul(tmps,Zorbj))
            Fsr(iloc,2) = Fsr(iloc,2)+matmul(Zmati,matmul(tmps,Zorbj))
            Fsr(iloc,3) = Fsr(iloc,3)+matmul(Zmati,matmul(tmps,Zorbj))
        end if
    end do
    
    if (mpistat) then
        call MPI_Allreduce_sum(Esr)
        call MPI_Allreduce_sum(Fsr)
    end if
    
    FRC%energy%Esr = 0.5*Esr
    FRC%force%Fsr  = Fsr
    
    if (mpirank == 0) then
        write(*,*) "infomation"
    end if        

    return
    end subroutine calcEsrFourier
    end module calcEsrFourier_module

