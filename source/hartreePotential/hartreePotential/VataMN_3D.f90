! VataMN_3D.f90
    
!*********************************************
    !
    ! A data type used in initDistArray.f90
    !
!*********************************************
    
    module VataMN_3D_module
    
    type :: VataMN_3D
        integer :: m
        integer :: n
        integer :: mblock
        integer :: nblock
        integer :: mproc
        integer :: nproc
        real*8, allocatable :: vata(:,:,:)
    end type VataMN_3D
    
    end module VataMN_3D_module