! idct.f90
    
!***********************************************
    !
    !
    !
!***********************************************
    
    module idct_module
    contains
    subroutine idct(x,dim,y)
    
    use dct_module
    
    implicit none
    
    ! input variables
    complex, allocatable :: x(:,:,:)
    integer             :: dim
    
    ! temporary variables
    integer             :: M
    
    ! output varibales
    complex, allocatable :: y(:,:,:)
    
    ! body of this function
    M = size(x,dim)
    call dct(x,dim,y)
    y = y*2/(M-1)
    
    return
    end subroutine idct
    end module idct_module