!  GetHartreePotential.f90 
!
    module GetHartreePotential_module
    contains
    subroutine GetHartreePotential(FRC,rho,bc,symmetry,VHout)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    use distmat_zeroslike_1D_module
    use distmat_gather_module
    use ModBCDist_1D_module
    use mixedPoissonSolver_module
    use symmetrize_1D_module
    !use initDistArray_1D_module

    implicit none

    ! input ariables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    logical                         :: symmetry
    type(VataMN_1D)                 :: rho
    integer                         :: bc(3)
    
    ! temporery valuables
    integer             :: fnx, fny, fnz, mpirank, mpisize, ispin, tmp(2), fn
    logical             :: mpistat, rhonum
    type(VataMN_1D)     :: rhotmp, VH, VH1, VH2
    real*8, allocatable :: rhotmp2(:), rhotmp3(:,:,:), sym_t(:,:), sym_rec(:,:,:)
    
    ! output valuables
    type(VataMN_1D)     :: VHout

    ! Body of hartreePotential
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    !rhonum  = .TRUE.
    !rhotmp  = rho
    
    !fnx = size(rho%vata)
    !fny = size(rho%vata,2)
    !fnz = size(rho%vata,3)
    fnx = FRC%domain%fgridn(1)
    fny = FRC%domain%fgridn(2)
    fnz = FRC%domain%fgridn(3)
    
    tmp(1) = rho%m
    tmp(2) = 1
    
    call distmat_zeroslike_1D(FRC,rho,tmp,rhotmp)
    
    if (ispin == 1) then
        rhotmp%vata = rho%vata
    else if (ispin == 2) then
        
        write(*,*) "Error in GetHartreePotential.f90. Collinear spin is not available now."
        stop
        ! rhotmp%dataA = sum(rho%dataA,2)
        
    else if (ispin == 4) then
        
        write(*,*) "Error in GetHartreePotential.f90. General spin is not available now."
        stop
        ! rhotmp%dataA = rho%dataA(:,1)
        
    end if
    allocate(rhotmp2(size(rhotmp%vata)))
    call distmat_gather(FRC,rhotmp,rhotmp2)
    
    if (mpirank == 0) then
        allocate(rhotmp3(fnx,fny,fnz))
        rhotmp3 = reshape(rhotmp2,(/fnx,fny,fnz/))
        call MixedPoissonSolver(FRC,rhotmp3,bc,VH1%vata)
        
        if (FRC%symmetry%pointsymmetry .and. symmetry) then
            allocate(sym_rec(size(FRC%symmetry%sym_rec,1),size(FRC%symmetry%sym_rec,2),size(FRC%symmetry%sym_rec,3)))
            sym_rec = FRC%symmetry%sym_rec
            allocate(sym_t(size(FRC%symmetry%sym_t,1),size(FRC%symmetry%sym_t,2)))
            sym_t   = FRC%symmetry%sym_t
            allocate(VH2%vata(size(VH1%vata)))
            call symmetrize_1D(VH1%vata,(/fnx,fny,fnz/),sym_rec,sym_t,VH2%vata)
        end if
    end if
    
    if (.not. rhonum) then
        fn = size(VH2%vata)
        if (mpistat) then
            
            write(*,*) "Error in GetHartreePotential.f90. mpi is not available now."
            stop
            
            !call MPI_Bcast_variables(fn,0)
        end if
        call initDistArray_1D(fn,1,fn,1,1,mpisize,VH2)
        call ModBCDist_1D(FRC,VH2,rho%mblock,rho%nblock,rho%mproc,rho%nproc,.FALSE.,.FALSE.,VHout)
        !allocate(VHout%vata(fn))
    end if
    
    return
    end subroutine GetHartreePotential
    end module GetHartreePotential_module
