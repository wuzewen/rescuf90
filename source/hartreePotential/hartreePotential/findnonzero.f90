! findnonzero.f90
    
!***********************************************
    !
    !
    !
!***********************************************
    
    module findnonzero_module
    contains
    subroutine findnonzero(A,iA,jA,valA)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    
    ! temporary variables
    integer :: ii, jj, n1, n2, ntmp
    
    ! output variables
    integer, allocatable :: iA(:), jA(:)
    real*8, allocatable  :: valA(:)
    
    ! body of this function
    n1 = size(A,1)
    n2 = size(A,2)
    
    ntmp = 0
    do ii = 1,n1,1
        do jj = 1,n2,1
            if (A(ii,jj) /= 0) then
                ntmp = ntmp+1
            end if
        end do
    end do
    
    allocate(iA(ntmp),jA(ntmp),valA(ntmp))
    
    ntmp = 0
    do ii = 1,n1,1
        do jj = 1,n2,1
            if (A(ii,jj) /= 0) then
                ntmp       = ntmp+1
                iA(ntmp)   = ii
                jA(ntmp)   = jj
                valA(ntmp) = A(ii,jj)
            end if
        end do
    end do
    
    return
    end subroutine findnonzero
    end module findnonzero_module