! upGradeGrid.f90
    
    module upGradeGrid_module
    contains
    subroutine upGradeGrid(nv, symop, upG)
    
    implicit none
    
    ! input valuables
    integer              :: nv(3)
    real*8, allocatable  :: symop(:,:,:)
    
    ! temporery valuables
    
    ! output valuables
    integer              :: upG(3)
    
    ! body of this function
    if ((.not.(all(symop(1,2,:)==0))) .or. (.not.(all(symop(2,1,:)==0)))) then
        upG(1) = max(nv(1),nv(2))
        upG(2) = max(nv(1),nv(2))
    end if
    
    if ((.not.(all(symop(2,3,:)==0))) .or. (.not.(all(symop(3,2,:)==0)))) then
        upG(2) = max(nv(2),nv(3))
        upG(3) = max(nv(2),nv(3))
    end if
    
    if ((.not.(all(symop(1,3,:)==0))) .or. (.not.(all(symop(3,1,:)==0)))) then
        upG(1) = max(nv(1),nv(3))
        upG(3) = max(nv(1),nv(3))
    end if
    
    if ((.not.(all(symop(2,3,:)==0))) .or. (.not.(all(symop(3,2,:)==0)))) then
        upG(2) = max(nv(2),nv(3))
        upG(3) = max(nv(2),nv(3))
    end if
    
    if ((.not.(all(symop(1,2,:)==0))) .or. (.not.(all(symop(2,1,:)==0)))) then
        upG(1) = max(nv(1),nv(2))
        upG(2) = max(nv(1),nv(2))
    end if
    
    
    return
    end subroutine upGradeGrid
    end module upGradeGrid_module
    