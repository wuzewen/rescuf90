! fftFreq
    
    module fftFreqA_module
    contains
    subroutine fftFreqA(gridn,bvec,kuvw)
    use ndgrid_module
    implicit none
    
    ! imput valuables
    integer            :: gridn(3)
    real*8             :: bvec(3,3)
    
    ! temporery valuables
    integer            :: nn, i, j, k
    real*8,allocatable :: kuu(:), kvv(:), kww(:), kuvw(:,:)
    real*8,allocatable :: ku1(:), kv1(:), kw1(:)
    
    ! output valuables
    real*8,allocatable :: ku(:,:,:), kv(:,:,:), kw(:,:,:)
    
    ! body of this function
    nn = gridn(1)*gridn(2)*gridn(3)
    
    allocate(kuu(gridn(1)), kvv(gridn(2)), kww(gridn(3)))
    do i = 1,gridn(1),1
        if (i<=(gridn(1)/2+1)) then
            kuu(i) = i-1
        else
            kuu(i) = i-gridn(1)-1
        end if
    end do
    
    do i = 1,gridn(2),1
        if (i<=(gridn(2)/2+1)) then
            kvv(i) = i-1
        else
            kvv(i) = i-gridn(2)-1
        end if
    end do
    
    do i = 1,gridn(3),1
        if (i<=(gridn(3)/2+1)) then
            kww(i) = i-1
        else
            kww(i) = i-gridn(3)-1
        end if
    end do
    allocate(ku(gridn(1),gridn(2),gridn(3)))
    allocate(kv(gridn(1),gridn(2),gridn(3)))
    allocate(kw(gridn(1),gridn(2),gridn(3)))
    call ndgrid(kuu,kvv,kww,ku,kv,kw)
    
    allocate(ku1(nn),kv1(nn),kw1(nn))
    ku1 = reshape(ku,(/nn/))
    kv1 = reshape(kv,(/nn/))
    kw1 = reshape(kw,(/nn/))
    
    allocate(kuvw(nn,3))
    kuvw(:,1) = ku1(:)
    kuvw(:,2) = kv1(:)
    kuvw(:,3) = kw1(:)
    
    kuvw = matmul(kuvw,bvec)
      
    return
    end subroutine fftFreqA
    end module fftFreqA_module