! symmetrize_1D.f90
    
    module symmetrize_1D_module
    contains
    subroutine symmetrize_1D(vector,nvec,symrec,symt,symv)
    
    use upGradeGrid_module
    use fftFreqA_module
    use NDgrid_module
    use bsxfunMod_module
    use fft3_module
    use ifft3_module
    use bsxfunTimes_module
    use bsxfunTimes_CMPLX_module
    
    implicit none
    
    ! input valuables
    integer              :: nvec(3)
    real*8 , allocatable :: vector(:)
    !real*8 , allocatable :: hartreePotential(:,:,:)
    real*8 , allocatable :: symrec(:,:,:), symt(:,:)
    
    ! temporery valuables
    integer              :: ii, jj, kk
    integer              :: ngrid, nblk, nsym, nxtmp1, nytmp1, nztmp1, nxtmp2, nytmp2, nztmp2
    integer              :: nv2(3), nv(3), cnx, cny, cnz, fnx, fny, fnz, nxtmp, nytmp, nztmp
    integer              :: ng2, nva, tmp(3,1)
    real*8               :: eyem(3,3), pi
    integer, allocatable :: ndx(:), ndy(:), ndz(:)
    integer, allocatable :: gd(:,:), gd1(:,:), symop(:,:,:)
    integer, allocatable :: inde(:,:)
    real*8 , allocatable :: v_rec(:,:,:), vrec(:,:,:)
    real*8 , allocatable :: fftfreq(:,:), t(:,:), ttmp(:,:), symtmp(:)
    real*8 , allocatable :: ndxr(:), ndyr(:), ndzr(:), gdt(:,:,:), dex(:,:,:), phase(:,:,:)
    real*8 , allocatable :: gd_tmp(:,:), gd_tmp2(:,:), nvr(:,:)
    complex              :: imnu
    complex, allocatable :: vrec_c(:,:,:), v_rec_c(:,:,:), symtmpt(:,:,:), symvtr(:,:,:), v_rectm(:)
    complex, allocatable :: phase0(:,:), v_rectm2(:,:), tmpt(:,:), symvtmp(:), symvt(:,:,:)
    ! output valuables
    real*8, allocatable  :: symv(:)
    
    ! body of this function
    pi    = 3.14159265354
    ngrid = size(vector)
    !nblk  = size(vector,2)
    nsym  = size(symrec,3)
    imnu  = (0,1)
    
    call upGradeGrid(nvec,symrec,nv2)
    ng2 = product(nv2)
    
    !call eye(3,eyem)
    forall(ii=1:3)
        eyem(ii,ii) = 1
    end forall
    
    call fftFreqA(nv2,eyem,fftfreq)
    
    allocate(ndx(nv2(1)),ndy(nv2(2)),ndz(nv2(3)))
    forall(ii=1:nv2(1))
        ndx(ii) = ii-1
    end forall
    forall(ii=1:nv2(2))
        ndy(ii) = ii-1
    end forall
    forall(ii=1:nv2(3))
        ndz(ii) = ii-1
    end forall
    
    !call arrayN(0,nv2(1)-1,ndx)
    !call arrayN(0,nv2(2)-1,ndy)
    !call arrayN(0,nv2(3)-1,ndz)
    allocate(ndxr(nv2(1)),ndyr(nv2(2)),ndzr(nv2(3)))
    ndxr = real(ndx)
    ndyr = real(ndy)
    ndzr = real(ndz)
    allocate(gdt(nv2(1),nv2(2),nv2(3)),dex(nv2(1),nv2(2),nv2(3)),phase(nv2(1),nv2(2),nv2(3)))
    call ndgrid(ndxr,ndyr,ndzr,gdt,dex,phase)
    nva = nv2(1)*nv2(2)*nv2(3)
    allocate(gd(nva,3))
    gd(:,1) = reshape(gdt,(/nva/))
    gd(:,2) = reshape(dex,(/nva/))
    gd(:,3) = reshape(phase,(/nva/))
    
    allocate(vrec(nvec(1),nvec(2),nvec(3)))
    vrec = reshape(vector,(/nvec(1),nvec(2),nvec(3)/))
    vrec = cshift(vrec,-1,1)
    vrec = cshift(vrec,-1,2)
    vrec = cshift(vrec,-1,3)
    allocate(vrec_c(nvec(1),nvec(2),nvec(3)))
    allocate(v_rec_c(nvec(1),nvec(2),nvec(3)))
    vrec_c  = cmplx(vrec)
    !v_rec_c = cmplx(v_rec)
    call fft3(vrec_c,3,v_rec_c)
    call fft3(v_rec_c,2,vrec_c)
    call fft3(vrec_c,1,v_rec_c)
    
    if (.not. all(nv2 == nvec)) then
        write(*,*) "Error in symmetrize_1D.f90. nv2 should equal to nvec."
        stop
        !cnx    = nvec(1)
        !cny    = nvec(2)
        !cnz    = nvec(3)
        !fnx    = nv2(1)
        !fny    = nv2(2)
        !fnz    = nv2(3)
        !allocate(symv(nv2(1),nv2(2),nv2(3),nblk))
        !symv   = 0
        !nxtmp1 = cnx/2+1
        !nytmp1 = cny/2+1
        !nztmp1 = cnz/2+1
        !forall(ii=1:nxtmp,jj=1:nytmp,kk=1:nztmp)
        !    symv(ii) = v_rec(ii,jj,kk,:)
        !end forall
        !nxtmp2 = fnx-(cnx-cnx/2-1)
        !nytmp2 = fny-(cny-cny/2-1)
        !nztmp2 = fnz-(cnz-cnz/2-1)
        !forall(ii=nxtmp2:fnx,jj=nytmp2:fny,kk=nztmp2:fnz)
        !    symv(ii,jj,kk,:) = v_rec(ii-nxtmp1,jj-nytmp1,kk-nztmp1,:)
        !end forall
        
        !symvtmp = reshape(symv,(/ng2,nblk/))
        !v_rectm = symvtmp
        !symv    = 0
    else
        allocate(v_rectm(nva))
        v_rectm = reshape(v_rec_c,(/ng2/))
        allocate(symvtmp(ng2))
        symvtmp = (0,0)
    end if
    
    allocate(gd1(size(symrec,1),size(symrec,2)))
    allocate(gd_tmp(size(gd,1),size(gd1,2)))
    allocate(nvr(1,3))
    nvr(1,:) = real(nv2(:))
    allocate(gd_tmp2(size(gd,1),size(gd1,2)))
    allocate(phase0(nva,1))
    allocate(ttmp(3,1))
    allocate(v_rectm2(size(v_rectm),1))
    allocate(tmpt(nva,1))
    allocate(inde(nva,1))
    do ii = 1,nsym,1
        gd1(:,:) = symrec(:,:,ii)
        gd_tmp   = matmul(gd,gd1)
        call bsxfunMod(gd_tmp,nvr,gd_tmp2)
        tmp(1,1) = 1
        tmp(2,1) = nv2(1)
        tmp(3,1) = nv2(1)*nv2(2)
        inde = matmul(gd_tmp2,tmp)+1
        
        if (any(symt(ii,:) /= 0)) then
            ttmp(:,1) = symt(ii,:)
            phase0 = exp(cmplx(2.0)*imnu*cmplx(pi)*cmplx(matmul(fftfreq,ttmp)))
            v_rectm2(:,1) = v_rectm(:)
            call bsxfunTimes_CMPLX(v_rectm2,phase0,tmpt)
            do jj = 1,size(inde,1)
                symvtmp(jj)  = symvtmp(inde(jj,1)) + tmpt(inde(jj,1),1)
            end do
        else
            do jj = 1,size(inde,1)
                symvtmp(jj)  = symvtmp(inde(jj,1)) + v_rectm(inde(jj,1))
            end do
        end if
    end do
    symvtmp  = symvtmp/nsym
    allocate(symvt(nv2(1),nv2(2),nv2(3)))
    symvt = reshape(symvtmp,nv2)
    
    if (.not. all(nv2 == nvec)) then
        !allocate(symvt(nv2(1),nv2(2),nv2(3)))
        !symvt  = 0
        !nxtmp1 = cnx/2+1
        !nytmp1 = cny/2+1
        !nztmp1 = cnz/2+1
        !forall(ii=1:nxtmp,jj=1:nytmp,kk=1:nztmp)
        !    symvt(ii,jj,kk) = v_rec(ii,jj,kk)
        !end forall
        !nxtmp2 = fnx-(cnx-cnx/2-1)
        !nytmp2 = fny-(cny-cny/2-1)
        !nztmp2 = fnz-(cnz-cnz/2-1)
        !forall(ii=nxtmp:fnx,jj=nytmp:fny,kk=nztmp:fnz)
        !    symvt(ii,jj,kk) = v_rec(ii-nxtmp1,jj-nytmp1,kk-nztmp1)
        !end forall
    end if
    allocate(symvtr(nv2(1),nv2(2),nv2(3)))
    !allocate(symtmpt(nv2(1),nv2(2),nv2(3)))
    !symvtr = cmplx(symvt)
    call ifft3(symvt,3,symvtr)
    call ifft3(symvtr,2,symvt)
    call ifft3(symvt,1,symvtr)
    symvtr = cshift(symvtr,-1,1)
    symvtr = cshift(symvtr,-1,2)
    symvtr = cshift(symvtr,-1,3)
    
    symv = real(reshape(symvtr,(/ngrid/)))
    
    return
    end subroutine symmetrize_1D
    end module symmetrize_1D_module