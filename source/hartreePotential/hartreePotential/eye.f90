! eye.f90
    
    module eye_module
    contains
    subroutine eye(dim,eyem)
    
    implicit none
    
    ! input valuables
    integer             :: dim
    
    ! temporery valuables
    integer             :: i, j
    ! output valuables
    integer,allocatable :: eyem(:,:)
    
    ! body of this function
    do i = 1,dim,1
        do j = 1,dim,1
            if (i==j) then
                eyem(i,j) = 1
            else
                eyem(i,j) = 0
            end if
        end do
    end do
    
    return
    end subroutine eye
    end module eye_module