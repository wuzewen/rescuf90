! dct.f90

!*********************************************************
    !
    !
    !
!*********************************************************
    
    module dct_module
    contains
    subroutine dct(x,dim,y)
    
    use fft3_module
    
    implicit none
    
    ! input variables
    complex, allocatable :: x(:,:,:)
    integer             :: dim
    
    ! temporary variables
    integer             :: L, M, N, ii
    complex, allocatable :: pad(:,:,:), padtmp(:,:,:), ytmp(:,:,:), ytmp2(:,:,:)
    
    ! output variables
    complex, allocatable :: y(:,:,:)
    
    ! body of this function
    L = size(x,1)
    M = size(x,2)
    N = size(x,3)
    
    if (dim == 1) then
        allocate(pad(L-2,M,N))
        pad(:,:,:) = x(2:L-1,:,:)
        allocate(padtmp(L-2,M,N))
        forall(ii=1:L-2)
            padtmp(ii,:,:) = pad(L-ii-1,:,:)
        end forall
        allocate(ytmp(2*L-2,M,N))
        forall(ii=1:L)
            ytmp(ii,:,:) = x(ii,:,:)
        end forall
        forall(ii=L+1:2*L-2)
            ytmp(ii,:,:) = padtmp(ii-L,:,:)
        end forall
        call fft3(ytmp,1,ytmp2)
        allocate(y(L,M,N))
        y(:,:,:) = ytmp2(1:L,:,:)
    end if
    
    if (dim == 2) then
        allocate(pad(L,M-2,N))
        pad(:,:,:) = x(:,2:M-1,:)
        allocate(padtmp(L,M-2,N))
        forall(ii=1:M-2)
            padtmp(:,ii,:) = pad(:,M-ii-1,:)
        end forall
        allocate(ytmp(L,2*M-2,N))
        forall(ii=1:M)
            ytmp(:,ii,:) = x(:,ii,:)
        end forall
        forall(ii=M+1:2*M-2)
            ytmp(:,ii,:) = padtmp(:,ii-M,:)
        end forall
        call fft3(ytmp,2,ytmp2)
        allocate(y(L,M,N))
        y(:,:,:) = ytmp2(:,1:M,:)
    end if
    
    if (dim == 3) then
        allocate(pad(L,M,N-2))
        pad(:,:,:) = x(:,:,2:N-1)
        allocate(padtmp(L,M,N-2))
        forall(ii=1:N-2)
            padtmp(:,:,ii) = pad(:,:,N-ii-1)
        end forall
        allocate(ytmp(L,M,2*N-2))
        forall(ii=1:N)
            ytmp(:,:,ii) = x(:,:,ii)
        end forall
        forall(ii=N+1:2*N-2)
            ytmp(:,:,ii) = padtmp(:,:,ii-N)
        end forall
        call fft3(ytmp,3,ytmp2)
        allocate(y(L,M,N))
        y(:,:,:) = ytmp2(:,:,1:N)
    end if
    
    return
    end subroutine dct
    end module dct_module