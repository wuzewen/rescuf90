! arrayN.f90
    
    module arrayN_module
    contains
    subroutine arrayN(nmin,nmax,arra)
    
    implicit none
    
    ! input valuables
    integer              :: nmin, nmax
    
    ! temporery valuables
    integer              :: i
    
    ! output valuables
    integer, allocatable :: arra(:)
    
    do i = 1,(nmax-nmin+1),1
        arra(i) = i+nmin-1
    end do
    
    end subroutine arrayN
    end module arrayN_module