! rnuType.f90
    
!**************************************************************
    !
    ! this is to define a type to contain rnu information.
    !
!**************************************************************
    
    module rnuType_module
    
    type :: rnuType
        real*8, allocatable  :: vata(:)
    end type rnuType
    
    end module rnuType_module
    