! optionType.f90
    
!****************************
    !
    !
    !
!****************************
    
    module optionType_module
    
    type :: optionType
        logical              :: pwmode
        integer              :: bufferSize
        integer              :: centerMolecule
        logical              :: initParaReal
        integer              :: maxSCFiteration
        integer              :: plorder
        integer              :: replicateCell(3)
        logical              :: shortRange
        logical              :: saveDensity
        logical              :: saveDFPT
        logical              :: saveEigensolver
        logical              :: saveInterPolation
        logical              :: saveLCAO
        logical              :: saveMisc
        logical              :: saveMixing
        logical              :: savePotential
        logical              :: saveTau
        logical              :: saveWaveFunction
    end type optionType
    
    end module optionType_module