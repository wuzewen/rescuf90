! mixingType.f90
    
!****************************************************************
    !
    ! This is to define a type to contain mixing information.
    !
!****************************************************************
    
    module mixingType_module
    
    type :: mixingType
        real*8               :: alpha
        real*8               :: beta
        real*8               :: betalin
        integer              :: initlin
        real*8               :: lambda
        integer              :: maxHistory
        character(len=20)    :: method
        real*8               :: tol(2)
        real*8               :: dRho
        character(len=20)    :: MixingType
    end type mixingType
    
    end module mixingType_module