! potentialType.f90
    
!********************************************************************
    !
    ! This is to define a type to contain potential information.
    !
!********************************************************************
    
    module potentialType_module
    
    use VataMN_2D_module
    use VataMN_1D_module
    use Acell_type
    
    type :: Potvnltype
        logical              :: kbsparse
        logical              :: separable
        integer, allocatable :: evec(:), Lorb(:), Morb(:), Norb(:), Oorb(:), Sorb(:)
        real*8 , allocatable :: Rorb(:), Eorb(:), kbvec(:,:), Aorb(:), KBEnergy(:), KBorb(:,:)
        real*8               :: kpt(3)
        type(AcellType), allocatable :: kbcell(:) 
        integer, allocatable         :: vnlsize(:)
    end type Potvnltype
    
    type :: potentialType
        logical                      :: fourierinit
        real*8, allocatable          :: vloc(:,:)
        real*8, allocatable          :: vloc1(:)
        type(VataMN_2D)              :: vtau
        real*8, allocatable          :: vtauloc(:,:)
        type(VataMN_2D), allocatable :: VXCout(:)
        type(VataMN_1D)              :: vna
        type(VataMN_1D)              :: vps
        type(VataMN_1D), allocatable :: VH(:)
        type(VataMN_1D), allocatable :: VHout(:)
        type(VataMN_1D), allocatable :: veffin(:)
        type(VataMN_1D), allocatable :: veffout(:)
        type(VataMN_1D), allocatable :: deltain(:)
        type(VataMN_1D), allocatable :: deltaout(:)
        type(Potvnltype)             :: vnl
    end type potentialType
    
    end module potentialType_module