! readNumberOfElement.f90
!***************************************
    !
!***************************************
    
    subroutine readNumberOfElement(A)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    integer :: A
    
    ! body of this function
    open(unit=10,file='SystemStrcture.input',status='old',access='sequential',action='read')
    read(10,*) A
    close(10)
    
    return
    end subroutine readNumberOfElement