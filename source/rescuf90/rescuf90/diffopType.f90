! diffopType.f90
    
!****************************************************************
    !
    ! this is to define a type to contain diffop information.
    !
!****************************************************************
    
    module diffopType_module
    
    type :: diffopType
        integer              :: accuracy
        character(len=20)    :: method
        real*8, allocatable  :: Du(:,:)
        real*8, allocatable  :: Dv(:,:)
        real*8, allocatable  :: Dw(:,:)
        real*8, allocatable  :: fDu(:,:)
        real*8, allocatable  :: fDv(:,:)
        real*8, allocatable  :: fDw(:,:)
        real*8, allocatable  :: Duu(:,:)
        real*8, allocatable  :: Dvv(:,:)
        real*8, allocatable  :: Dww(:,:)
        real*8, allocatable  :: fDuu(:,:)
        real*8, allocatable  :: fDvv(:,:)
        real*8, allocatable  :: fDww(:,:)
    end type diffopType
    
    end module diffopType_module