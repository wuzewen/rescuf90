! AtomType.f90
    
!***********************************************************
    !
    ! this is to define a type to contain Atom infomation
    !
!***********************************************************
    
    module AtomType_module
    
    type :: AtomType
        character(len=99)    :: systemName
        character(len=20)    :: coordinateType
        integer              :: NumberOfElement
        integer              :: NumberOfAtom
        integer, allocatable :: element(:)
        real*8,  allocatable :: XYZ(:,:)
        real*8,  allocatable :: FracXYZ(:,:)
        real*8,  allocatable :: magmom(:,:) 
        real*8,  allocatable :: magmomD(:,:) 
        real*8,  allocatable :: magmomcart(:,:)
    end type AtomType
    
    end module AtomType_module