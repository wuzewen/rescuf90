! phType.f90
    
!**********************************
    !
    !
    !
!***********************************
    
    module phType_module
    
    type :: phType
        real*8               :: klist
        integer              :: nk
        character(len=20)    :: labels
        integer, allocatable :: mass(:)
        integer, allocatable :: unitCellIndex(:)
        integer, allocatable :: unitCellMass(:)
        integer              :: superCellDimension
        character(len=20)    :: mode
        integer              :: ASR
        real                 :: dr
        integer              :: plot
        integer              :: dispStartNumber
        character(len=20)    :: supercellXyzFile
        real                 :: tol
        character(len=20)    :: wUnit
        integer              :: isMinus
    end type phType
    
    end module phType_module