! SummeryOfBas.f90
    
!***********
    !
    !
    !
!************
    
    module SummeryOfBas_module
    
    type :: orbitalsize_Type
        integer  :: SizeOfOrbitalSetrrData,SizeOfOrbitalSetdrData,SizeOfOrbitalSetfrData,         SizeOfOrbitalSetqqData,           SizeOfOrbitalSetfqData,SizeOfOrbitalSetqwData
    end type orbitalsize_Type
    
    type :: SummeryOfBas
        integer  :: SizeOfVlocal,    SizeOfVlocalrrData,    SizeOfVlocaldrData,    SizeOfVlocalvvData
        integer  :: SizeOfRlocal,    SizeOfRlocalrrData,    SizeOfRlocaldrData,    SizeOfRlocalrhoData
        integer  :: SizeOfVnl,       SizeOfVnlrrData,       SizeOfVnldrData,       SizeOfVnlvvData,                SizeOfVnlqqData,                  SizeOfVnlfqData,       SizeOfVnlqwData
        integer  :: SizeOfVna,       SizeOfVnarrData,       SizeOfVnadrData,       SizeOfVnavvData
        integer  :: SizeOfRna,       SizeOfRnarrData,       SizeOfRnadrData,       SizeOfRnarhoData
        integer  :: SizeOfRelPseudoP,SizeOfRelPseudoPrrData,SizeOfRelPseudoPdrData,SizeOfRelPseudoPvvData_screened,SizeOfRelPseudoPvvData_unscreened
        integer  :: SizeOfOrbitalSet
        type(orbitalsize_Type), allocatable :: orbitalsize(:)
    end type SummeryOfBas
    
    end module SummeryOfBas_module
    