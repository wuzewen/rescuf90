! optsType.f90
    
!*******************************
    !
    !
    !
!************************
    
    module optsType_module
    
    type :: optsType
        logical :: issym
        logical :: isreal
        real*8  :: tol
        integer :: maxit
    end type optsType
    
    end module optsType_module