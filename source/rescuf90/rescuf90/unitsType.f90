! unitsType.f90
    
!*****
    !
    !
    !
!****************
    
    module unitsType_module
    
    type :: unitDOS
        character(len=20)    :: DOSrange
        character(len=20)    :: resolution
    end type unitDOS
    
    type :: unitsType
        character(len=20)    :: xyz
        character(len=20)    :: latvec
        character(len=20)    :: sigma
        type(unitDOS)        :: dos
    end type unitsType
    
    end module unitsType_module