! SRtype.f90
    
!************************************************************
    !
    ! This is to define a type to contain SR information.
    !
!************************************************************
    
    module SRtype_module
    
    type :: SRtype
        integer              :: initStep
        integer              :: maxStep
        integer              :: maxit
        integer              :: maxLineIt
        character(len=20)    :: method
        character(len=20)    :: linemethod
        integer              :: moveableAtomList
        character(len=99)    :: relaxedstructure
        character(len=99)    :: savePath
        real*8               :: tol
    end type SRtype
    
    end module SRtype_module