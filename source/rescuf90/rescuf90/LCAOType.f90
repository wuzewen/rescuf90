! LCAOType.f90
    
!**************************************************************
    !
    ! This is to define a type to contain LCAO information.
    !
!**************************************************************
    
    module LCAOType_module
    
    type :: LCAOType
        logical              :: cfsi
        integer              :: dynSubRed
        logical              :: mulliken
        real*8               :: sprs
        logical              :: status
        integer              :: VeffRed
    end type LCAOType
    
    end module LCAOType_module