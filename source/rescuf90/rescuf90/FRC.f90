! FRC.f90
!*******************************************************************
    !
    ! This is the structure data used in rescu FORTRAN version
    !
!*******************************************************************
    
    module FORTRAN_RESCU_CALCULATION_TYPE
    
    use A_module
    use AtomType_module
    use diffopType_module
    use DomainType_module
    use DOStype_module
    use ElementDataType_module
    use eigensolverType_module
    use elementType_module
    use GPUtype_module
    use infoType_module
    use initType_module
    use interpolationType_module
    use kpointType_module
    use LCAOType_module
    use LDOStype_module
    use mpitype_module
    use mixingType_module
    use optionType_module
    use optsType_module
    use phType_module
    use potentialType_module
    use partialRRtype_module
    use rnuType_module
    use smearingType_module
    use smiType_module
    use spinType_module
    use SRtype_module
    use SummeryOfBas_module
    use symmetryType_module
    use unitsType_module
    
    type :: FORTRAN_RESCU_CALCULATION
        type(AtomType)          :: Atom
        type(DomainType)        :: Domain
        type(diffopType)        :: diffop
        type(elementType), allocatable      :: element
        type(eigensolverType)   :: eigensolver
        type(interpolationType) :: interpolation
        type(kpointType)        :: kpoint
        type(LCAOType)          :: LCAO
        type(mixingType)        :: mixing
        type(potentialType)     :: potential
        type(smearingType)      :: smearing
        type(spinType)          :: spin
        type(SRtype)            :: SR
        type(symmetryType)      :: symmetry
        type(unitsType)         :: units
        type(smiType)           :: smi
        type(infoType)          :: info
        type(DOStype)           :: DOS
        type(optionType)        :: option
        type(GPUtype)           :: GPU
        type(phType)            :: ph
        type(mpiType)           :: mpi
        type(initType)          :: init
        type(SummeryOfBas),      allocatable :: SOB(:)
        type(ElementDataType),   allocatable :: ElementData(:)
        integer                              :: scloop
        type(partialRRtype)                  :: partialRR
    end type FORTRAN_RESCU_CALCULATION
    
    end module FORTRAN_RESCU_CALCULATION_TYPE