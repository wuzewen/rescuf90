! MPI_type.f90
    
!*************************************************************
    !
    ! This is to define a type to contain MPI imformation
    !
!*************************************************************
    
    module mpitype_module
    
    type :: mpiType
        logical              :: status
        integer              :: rank
        integer              :: mpisize
        logical              :: paraK
    end type mpiType
    
    end module mpitype_module