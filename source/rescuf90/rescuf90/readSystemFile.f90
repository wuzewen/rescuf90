! readSystemFile.f90
    
!****************************************************************
    !
    !
    !
!****************************************************************
    
    module readSystemFile_module
    contains
    subroutine readSystemFile(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputtype
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    integer :: ii, nline, status
    logical :: VariableIsFind
    real*8  :: LengthScale
    character(len=3), allocatable :: speciesName(:)
    integer, allocatable          :: speciesNumber(:)
    
    ! output variables
    
    ! body of this function
    nline = inputFromFile%NumberOfInput
    ii = 0
    do while (ii < nline)
        ii     = ii+1
        if (trim(inputFromFile%NAV(ii)%name) == trim("NumberOfElement")) then
            VariableIsFind = .TRUE.
            exit
        end if
    end do
    if (VariableIsFind) then
        read(inputFromFile%NAV(ii)%value,"(i2)") status
        FRC%atom%NumberOfElement = status
    else
        write(*,*) "Error in rescu. Please give the number of elements in the system."
        stop
    end if
    
    ii = 0
    do while (ii < nline)
        ii     = ii+1
        if (trim(inputFromFile%NAV(ii)%name) == trim("NumberOfAtom")) then
            VariableIsFind = .TRUE.
            exit
        end if
    end do
    if (VariableIsFind) then
        read(inputFromFile%NAV(ii)%value,"(i2)") status
        FRC%atom%NumberOfAtom = status
    else
        write(*,*) "Error in rescu. Please give the number of atoms in the system."
        stop
    end if
    
    allocate(FRC%atom%XYZ(FRC%atom%NumberOfAtom,3))
    allocate(FRC%atom%FracXYZ(FRC%atom%NumberOfAtom,3))
    allocate(FRC%element(FRC%atom%NumberOfElement))
    allocate(speciesName(FRC%atom%NumberOfElement))
    allocate(speciesNumber(FRC%atom%NumberOfElement))
    
    open(unit=10,file='system.input',status='old',access='sequential',action='read')
    read(10,*) FRC%atom%systemName
    read(10,*) LengthScale
    read(10,*) FRC%units%latvec
    read(10,*) FRC%domain%latvec(1,:)
    read(10,*) FRC%domain%latvec(2,:)
    read(10,*) FRC%domain%latvec(3,:)
    read(10,*) speciesName(:)
    read(10,*) speciesNumber(:)
    read(10,*) FRC%units%xyz
    read(10,*) FRC%atom%coordinateType
    if (trim(FRC%atom%coordinateType) == trim("Cartesian")) then
        do ii = 1,FRC%atom%NumberOfAtom,1
            read(10,*) FRC%atom%XYZ(ii,:)
        end do
        deallocate(FRC%atom%FracXYZ)
    else
        do ii = 1,FRC%atom%NumberOfAtom,1
            read(10,*) FRC%atom%FracXYZ(ii,:)
        end do
        deallocate(FRC%atom%XYZ)
    end if
    close(10)
    
    do ii = 1,FRC%atom%NumberOfElement,1
        FRC%element(ii)%species      = speciesName(ii)
        FRC%element(ii)%NumberOfAtom = speciesNumber(ii)
    end do
    
    return
    end subroutine readSystemFile
    end module readSystemFile_module