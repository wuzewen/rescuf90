! AtomSymbolType.f90
!**************************************************************
    !
    ! A data type to accept the number and species of Atoms
    !
!**************************************************************
    
    module AtomSymbolType_module
    
    type :: AtomSymbolType
        character(len=4) :: species
        integer          :: NumberOfAtoms
    end type AtomSymbolType
    
    end module AtomSymbolType_module