! energyType.f90
    
!****************************************************************
    !
    ! This module contains Energy data during the calculation.
    !
!****************************************************************
    
    module energyType_module
    
    use VataMN_1D_module
    use Cell_3D_real
    
    type :: energyType
        real*8  :: Efermi
        logical :: ewaldE
        type(VataMN_1D),   allocatable :: XCdens(:)
        type(Cell_3D_rea), allocatable :: ksnrg(:)
        real*8, allocatable            :: Eks(:)
        real*8, allocatable            :: Etot(:)
        real*8, allocatable            :: EdH(:)
        real*8, allocatable            :: nocc(:,:,:)
        real*8, allocatable            :: entropy(:,:)
    end type energyType
    
    end module energyType_module