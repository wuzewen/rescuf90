! fermiDirac_3D.f90
    
!********************************************************************************
    !
    !
    !
!********************************************************************************
    
    module fermiDirac_3D_module
    contains
    subroutine fermiDirac_3D(W,EF,x,y)
    
    implicit none
    
    ! input variables
    real*8              :: W, EF
    real*8, allocatable :: x(:,:,:)
    
    ! temporary variables
    integer :: l, m, n, ii, jj, kk
    real*8  :: xtmp
    
    ! output variables
    real*8, allocatable :: y(:,:,:)
    
    ! body of this function
    l = size(x,1)
    m = size(x,2)
    n = size(x,3)
    
    allocate(y(l,m,n))
    do ii = 1,l,1
        do jj = 1,m,1
            do kk = 1,n,1
                xtmp        = (x(ii,jj,kk)-EF)/W
                y(ii,jj,kk) = 1.0/(exp(xtmp)+1.0)
            end do
        end do
    end do
    
    return
    end subroutine fermiDirac_3D
    end module fermiDirac_3D_module