!  updateOccupancy.f90 
!****************************************************************************
!
!  PROGRAM: updateOccupancy
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module updateOccupancy_module
    contains
    subroutine updateOccupancy(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use calcoccupancy_module
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    integer             :: iter
    real*8              :: mu
    real*8, allocatable :: energy(:,:,:), nocc(:,:,:), eks(:,:), entropy(:,:), ener2(:,:), nocc2(:,:)
    
    ! output variables

    ! Body of updateOccupancy
    iter   = FRC%scloop
    energy = FRC%energy%ksnrg(iter)%vata
    call calcoccupancy(FRC,ener2,mu,nocc,entropy)
    FRC%energy%EFermi    = mu
    FRC%energy%nocc      = nocc
    FRC%energy%entropy   = entropy
    eks                  = ener2*nocc2
    FRC%energy%Eks(iter) = sum(eks)

    return
    end subroutine updateOccupancy
    end module updateOccupancy_module

