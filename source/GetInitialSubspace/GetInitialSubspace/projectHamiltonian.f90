! projectHamiltonian.f90
    
!******************************************************************
    !
    ! 
    !
!******************************************************************
    
    module projectHamiltonian_module
    contains
    subroutine projectHamiltonian(FRC,kpt,vs,XHX,XSX)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use distmat_isreal_module
    use modBCDist_2D_module
    use getGlobalInd_2D_module
    use triu_module
    use GenHamFun_module
    
    implicit none
    
    ! input variables
    real*8                          :: kpt(3)
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_2D)                 :: vs
    real*8, allocatable             :: oper(:,:)
    
    ! temporary variables
    integer              :: mpirank, mpisize, cn, ii, jj, norb, nblock, maxind, buff, sizeof
    integer              :: nx, mbpsi, nbpsi, blks, ntmp, crealIN, spin
    logical              :: smistat, mpistat, gpustat, creal
    real*8               :: sep, dr, avec(3,3)
    logical, allocatable :: colind(:)
    integer, allocatable :: jb(:), ia(:,:), ja(:,:)
    real*8 , allocatable :: tmp1(:,:), tmp2(:,:), XHXtmp(:,:), XSXtmp(:,:), nonsense1(:,:), nonsense2(:,:)
    type(VataMN_2D)      :: HX
    
    ! output variables
    type(VataMN_2D)      :: XHX, XSX
    
    ! body of this function
    spin    = 1
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    smistat = FRC%smi%status
    gpustat = .FALSE.
    sep     = 1.0*10**(-14)
    cn      = product(FRC%domain%cgridn)
    avec    = FRC%domain%latvec
    dr      = avec(1,1)*avec(2,2)*avec(3,3)+avec(1,2)*avec(2,3)*avec(3,1)+avec(1,3)*avec(2,1)*avec(3,2) &
             -avec(1,3)*avec(2,2)*avec(3,1)-avec(1,1)*avec(2,3)*avec(3,2)-avec(1,2)*avec(2,1)*avec(3,3)
    dr      = dr/real(cn)
    norb    = vs%n
    buff    = FRC%option%bufferSize
    call distmat_isreal(FRC,vs,creal)
    if (creal) then
        crealIN = .TRUE.
    else
        crealIN = .FALSE.
    end if
    sizeof  = 16-8*0!creal
    mbpsi   = vs%mblock
    nbpsi   = vs%nblock
    blks    = ceiling(real(buff)/real(sizeof)/real(cn))*mpisize
    ntmp    = ceiling(real(norb)/768.0)
    ntmp    = ceiling(real(norb)/real(ntmp))
    blks    = min(blks,ntmp)
    allocate(jb(norb))
    forall(ii=1:norb)
        jb(ii) = ceiling((real(ii)-sep)/real(blks))
    end forall
    
    nblock  = jb(norb)
    call initDistArray_2D(norb,norb,norb,blks,1,mpisize,XHX)
    call GetGlobalInd_2D(mpirank,XHX,ia,ja)
    allocate(XHX%vata(size(ia),size(ja)))
    
    XSX = XHX
    
    allocate(colind(size(jb)))
    do ii = 1,nblock,1
        do jj = 1,size(jb),1
            if (jb(jj) == ii) then
                colind(jj) = .TRUE.
                maxind     = jj
            else
                colind(jj) = .FALSE.
            end if
        end do
        nX = count(colind == .TRUE.)
        
        if (nX > 0) then
            call initDistArray_2D(cn,nX,vs%mblock,1,mpisize,1,HX)
            allocate(HX%vata(size(vs%vata,1),nX))
            ntmp = 0
            do jj = 1,size(jb),1
                if (colind(jj)) then
                    ntmp = ntmp+1
                    HX%vata(:,ntmp) = vs%vata(:,jj)
                end if
            end do
            call modBCDist_2D(FRC,HX,1,1,1,mpisize,.FALSE.,.FALSE.,HX)
            if (size(HX%vata,2) > 0) then
                call GenHamFun(FRC,kpt,spin,gpustat,HX%vata,HX%vata,nonsense1,nonsense2)
                !call oper(HX%vata)
            end if
            
            call ModBCDist_2D(FRC,HX,vs%mblock,1,mpisize,1,.FALSE.,.FALSE.,HX)
            if (gpustat) then
                write(*,*) "Error in projectHamiltonian.f90. GPU is not available now."
                stop
            else
                allocate(tmp1(maxind,size(vs%vata,1)))
                forall (jj=1:maxind)
                    tmp1(jj,:) = vs%vata(:,jj)
                end forall
                allocate(XHXtmp(size(tmp1,1),size(HX%vata,2)))
                XHXtmp = matmul(tmp1,HX%vata)
                allocate(tmp2(size(vs%vata,1),nX))
                ntmp = 0
                do jj=1,size(jb),1
                    if (colind(jj)) then
                        ntmp = ntmp+1
                        tmp2(:,ntmp) = vs%vata(:,jj)
                    end if
                end do
                allocate(XSXtmp(size(tmp1,1),size(tmp2,2)))
                XSXtmp = matmul(tmp1,tmp2)
            end if
            
            if (mpistat) then
                write(*,*) "Error in projectHamiltonian.f90. MPI is not available now."
                stop
            end if
            
            if (mpirank == mod(ii-1,mpisize)) then
                !forall(jj=1:maxind)
                    !XHX%vata(jj,colind(ja(:,1))) = XHXtmp(jj,:)
                    !XSX%vata(jj,colind(ja(:,1))) = XSXtmp(jj,:)
                !end forall
            end if
            deallocate(HX%vata,tmp1,XHXtmp,tmp2,XSXtmp)
        end if
    end do
    
    if (smistat) then
        write(*,*) "Error in projectHamiltonian.f90. SMI is not available now."
        stop
    else
        call ModBCDist_2D(FRC,XHX,XHX%m,XHX%n,mpisize,1,.FALSE.,.FALSE.,XHX)
        call modBCDist_2D(FRC,XSX,XSX%m,XSX%n,mpisize,1,.FALSE.,.FALSE.,XSX)
        if (mpirank == 0) then
            allocate(tmp1(size(XHX%vata,1),size(XHX%vata,2)))
            allocate(tmp2(size(XHX%vata,1),size(XHX%vata,2)))
            call triu(XHX%vata,0,tmp1)
            call triu(XHX%vata,1,tmp2)
            XHX%vata = tmp1+transpose(tmp2)
            
            call triu(XSX%vata,0,tmp1)
            call triu(XSX%vata,1,tmp2)
            XSX%vata = tmp1+transpose(tmp2)
        end if
        XHX%vata = XHX%vata*dr
        XSX%vata = XSX%vata*dr
    end if
    
    return
    end subroutine projectHamiltonian
    end module projectHamiltonian_module