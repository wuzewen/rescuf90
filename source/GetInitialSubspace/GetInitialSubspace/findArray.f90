! findArray.f90
!***************************************
    !
!***************************************
    
    module findArray_module
    contains
    subroutine findArray(ArrayA,siz,valu,ArrayB)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: ArrayA(:)
    real*8              :: valu
    integer             :: siz
    
    ! temporery variables
    integer             :: i, j
    
    ! output variables
    real*8, allocatable :: ArrayB(:)
    
    ! body of this function
    j = 0
    do i = 1,siz,1
        if (ArrayA(i)==valu) then
            j = j+1
            ArrayB(j) = i
        end if
    end do
    
    return
    end subroutine findArray
    end module findArray_module