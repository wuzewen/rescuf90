! InitFromSin.f90
    
!**********************************************************************************
    !
    ! 
    !
!**********************************************************************************
    
    module InitFromSin_module
    contains
    subroutine InitFromSin(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ndgrid_module
    use sort_1D_module
    use modBCDist_2D_module
    use GetGlobalInd_2D_module
    use distmat_allgather_2D_module
    use projectHamiltonian_module
    use getKBorb_module
    use bsxfunTimes_CMPLX_module
    use updateVeff_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    real*8               :: pi, omgx, omgy, omgz, phix, phiy, phiz, kpt(3)
    integer              :: mpirank, mpisize, ispin, nspin, nvec(3), nx, ny, nz, nband, cn, ntmp, ntmp2, nkpt
    integer              :: ii, jj, kk, nn, ss, intmp
    logical              :: mpistat, rrfind
    real*8,  allocatable :: kdir(:,:), avec(:,:), xtmp(:), ytmp(:), ztmp(:), nrg(:,:,:), vtd(:,:), ham(:,:), tmp(:,:)
    real*8,  allocatable :: vt(:), itmp(:,:,:), jtmp(:,:,:), ktmp(:,:,:), ijk2(:,:,:), xt(:,:,:), yt(:,:,:), zt(:,:,:)
    real*8,  allocatable :: ijk2tmp(:), ijktmp2(:), itmptmp(:), jtmptmp(:), ktmptmp(:), xtt(:), ytt(:), ztt(:)
    integer, allocatable :: ind2(:), iitmp(:), jjtmp(:), kktmp(:)
    integer, allocatable :: iA(:,:), jA(:,:)
    complex              :: imnu
    complex, allocatable :: phase(:,:), vatatmp(:,:)
    type(VataMN_2D)      :: vsNew, veff, vs, vstmp, veffin
    type(VataMN_1D)      :: veff_1D
    type(VataMN_2D)      :: XHX, XSX
    
    ! output variables
    
    ! body of this function
    imnu    = (0.0,1.0)
    pi      = 3.14159265354
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    nkpt    = size(FRC%kpoint%ikdirect,1)
    allocate(kdir(nkpt,3))
    kdir    = FRC%kpoint%ikdirect
    allocate(avec(3,3))
    avec    = FRC%domain%latvec
    !allocate(bvec(3,3))
    !call inversion(avec,bvec)
    !bvec    = transpose(bvec)
    !bvec    = 2.0*pi*bvec
    nvec    = FRC%domain%cgridn
    nx      = nvec(1)
    ny      = nvec(2)
    nz      = nvec(3)
    nband   = FRC%eigensolver%nband
    cn      = product(nvec)
    ntmp    = ceiling(real(nband)**(1.0/3.0))
    ntmp2   = ceiling(real(ntmp)/2.0)
    allocate(vt(2*ntmp2+1))
    forall(ii=1:2*ntmp2+1)
        vt(ii) = ii-ntmp2-1
    end forall
    allocate(itmp(2*ntmp2+1,2*ntmp2+1,2*ntmp2+1),jtmp(2*ntmp2+1,2*ntmp2+1,2*ntmp2+1),ktmp(2*ntmp2+1,2*ntmp2+1,2*ntmp2+1))
    call ndgrid(vt,vt,vt,itmp,jtmp,ktmp)
    allocate(ijk2(2*ntmp2+1,2*ntmp2+1,2*ntmp2+1))
    ijk2 = itmp**2+jtmp**2+ktmp**2
    intmp   = size(ijk2,1)*size(ijk2,2)*size(ijk2,3)
    allocate(ijk2tmp(intmp))
    ijk2tmp = reshape(ijk2,(/intmp/))
    call sort_1D(ijk2tmp,ijktmp2,ind2)
    allocate(itmptmp(intmp),jtmptmp(intmp),ktmptmp(intmp))
    itmptmp = reshape(itmp,(/intmp/))
    jtmptmp = reshape(jtmp,(/intmp/))
    ktmptmp = reshape(ktmp,(/intmp/))
    allocate(iitmp(intmp),jjtmp(intmp),kktmp(intmp))
    forall(ii=1:intmp)
        iitmp(ii) = itmptmp(ind2(ii))
        jjtmp(ii) = jtmptmp(ind2(ii))
        kktmp(ii) = ktmptmp(ind2(ii))
    end forall
    !deallocate(ijk2tmp)
    !allocate(ijk2tmp(2*ntmp2+1))
    ijk2tmp = iitmp**2+jjtmp**2+kktmp**2
    
    ntmp = count(ijk2tmp == 0)
    ntmp = intmp-ntmp
    deallocate(itmptmp,jtmptmp,ktmptmp)
    allocate(itmptmp(ntmp),jtmptmp(ntmp),ktmptmp(ntmp))
    ntmp = 0
    do ii = 1,intmp,1
        if (ijk2tmp(ii) /= 0) then
            ntmp = ntmp+1
            itmptmp(ntmp) = iitmp(ii)
            jtmptmp(ntmp) = jjtmp(ii)
            ktmptmp(ntmp) = kktmp(ii)
        end if
    end do
    
    call initDistArray_2D(cn,nband,cn/mpisize,1,mpisize,1,vsnew)
    call GetGlobalInd_2D(mpirank,vsnew,ia,ja)
    allocate(vsnew%vata(size(ia),size(ja)))
    
    allocate(xtmp(nx))
    allocate(ytmp(ny))
    allocate(ztmp(nz))
    forall(ii=1:nx)
        xtmp(ii) = real(ii)/real(nx)
    end forall
    forall(ii=1:ny)
        ytmp(ii) = real(ii)/real(ny)
    end forall
    forall(ii=1:nz)
        ztmp(ii) = real(ii)/real(nz)
    end forall
    allocate(xt(nx,ny,nz),yt(nx,ny,nz),zt(nx,ny,nz))
    call ndgrid(xtmp,ytmp,ztmp,xt,yt,zt)
    
    deallocate(xtmp,ytmp,ztmp)
    allocate(xtmp(size(ia)),ytmp(size(ia)),ztmp(size(ia)))
    allocate(xtt(size(ia)),ytt(size(ia)),ztt(size(ia)))
    xtt = reshape(xt,(/size(ia)/))
    ytt = reshape(yt,(/size(ia)/))
    ztt = reshape(zt,(/size(ia)/))
    forall(ii=1:size(ia))
        xtmp(ii) = xtt(ia(1,ii))
        ytmp(ii) = ytt(ia(1,ii))
        ztmp(ii) = ztt(ia(1,ii))
    end forall
    !allocate(FRC%potential%veffin(1))
    allocate(veffin%Vata(size(FRC%potential%veffin(1)%vata),1))
    veffin%Vata(:,1) = FRC%potential%veffin(1)%vata
    veffin%m         = FRC%potential%veffin(1)%m
    veffin%n         = FRC%potential%veffin(1)%n
    veffin%mblock    = FRC%potential%veffin(1)%mblock
    veffin%nblock    = FRC%potential%veffin(1)%nblock
    veffin%mproc     = FRC%potential%veffin(1)%mproc
    veffin%nproc     = FRC%potential%veffin(1)%nproc
    call UpdateVeff(FRC,veffin,FRC%potential%vps,.FALSE.,veff)
    call distmat_allgather_2D(FRC,veff,FRC%potential%vloc)
    allocate(FRC%psi(nkpt,nspin))
    do kk = 1,nkpt,1
        kpt = kdir(kk,:)
        deallocate(FRC%potential%vnl%KBorb)
        call GetKBorb(FRC,kpt)
        do ss = 1,nspin,1
            vs = vsnew
            do nn = 1,nband,1
                omgx = 2.0*pi*real(itmptmp(nn))
                omgy = 2.0*pi*real(jtmptmp(nn))
                omgz = 2.0*pi*real(ktmptmp(nn))
                if (omgx < 0) then
                    phix = (-1.0)*pi/4.0+pi/4.0
                else if (omgx == 0) then
                    phix = 0
                else
                    phix = pi/4.0+pi/4.0
                end if
                
                if (omgy < 0) then
                    phiy = (-1.0)*pi/4.0+pi/4.0
                else if (omgy == 0) then
                    phiy = 0
                else
                    phiy = pi/4.0+pi/4.0
                end if
                
                if (omgz < 0) then
                    phiz = (-1.0)*pi/4.0+pi/4.0
                else if (omgz == 0) then
                    phiz = 0
                else
                    phiz = pi/4.0+pi/4.0
                end if
                !phiy = sign(omgy)*pi/4.0+pi/4.0
                !phiz = sign(omgz)*pi/4.0+pi/4.0
                forall(ii=1:size(ia))
                    vs%vata(ii,nn) = cos(omgx*xtmp(ii)+phix)*cos(omgy*ytmp(ii)+phiy)*cos(omgz*ztmp(ii)+phiz)
                end forall
            end do
            
            if (any(kpt /= 0)) then
                allocate(phase(size(ia),1))
                forall(ii=1:size(ia))
                    phase(ii,1) = exp(-imnu*(xtmp(ii)*kpt(1)+ytmp(ii)*kpt(2)+ztmp(ii)*kpt(3)))
                end forall
                allocate(vatatmp(size(vs%vata,1),size(vs%vata,2)))
                vatatmp = cmplx(vs%vata)
                call bsxfunTimes_CMPLX(vatatmp,phase,vatatmp)
                vs%vata = real(vatatmp)
                deallocate(phase,vatatmp)
            end if
                
            if (ispin == 1 .or. ispin == 2) then
                call projectHamiltonian(FRC,kpt,vs,XHX,XSX)
            else if (ispin == 4) then
                write(*,*) "Error in InitFromSin.f90. Non-collinear spin is not available now."
                stop
            end if
                
            rrfind = .FALSE.
            if (rrfind) then
                write(*,*) "Error in InitFromSin.f90. rr is not available now."
                stop
            else
                call ModBCDist_2D(FRC,vs,1,1,1,mpisize,.false.,.false.,vstmp)
                deallocate(ia,ja)
                call GetGlobalInd_2D(mpirank,vstmp,ia,ja)
                allocate(nrg(nband,1,1))
                nrg = 0
                do jj = 1,size(vstmp%vata,2),1
                    vtd(1,:) = vstmp%vata(:,nn)
                    !call GenHamFun(FRC,kpt,ss,ham)
                    tmp         = matmul(vtd,ham)
                    !nrg(jj,:,:) = tmp(:,:)
                end do
                    
                if (mpistat) then
                    write(*,*) "Error in InitFromSin.f90. MPI is not available now."
                    stop
                    !
                end if
                    
                if (ispin == 1 .or. ispin == 2) then
                    FRC%psi(kk,ss) = vs
                else if (ispin == 4) then
                    write(*,*) "Error in InitFromSin.f90. Non-collinear spin is not available."
                    stop
                end if
                deallocate(XHX%vata,XSX%vata,nrg)
                !FRC%energy%ksnrg(1)%vata(:,kk,ss) = nrg(:,1,1)
            end if
        end do
    end do
        
    return
    end subroutine InitFromSin
    end module InitFromSin_module