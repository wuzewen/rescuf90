! GetAtomicOrbitalSubspace.f90
!****************************************
    !
!****************************************
    
    module GetAtomicOrbitalSubspace_module
    contains
    subroutine GetAtomicOrbitalSubspace(FRC,printswitch)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    logical                         :: printswitch
    
    ! temporery variables
    logical              :: mpistat, usecell, tmp(3), 
    logical, allocatable :: isLoc(:)
    integer              :: mpirank,mpisize,gridn(3),nn,nproj,natom,nkpt,ntv,npoint, ntmp, mtmp, itmp, jj, ttmp
    integer              :: kk, tvec(3)
    integer, allocatable :: inde(:)
    real*8               :: avec(3,3),evec,oorb,rad,rsph,ones(3),center(3,3), pos(3), lnu, lcut, znu, zncut, mo, LL
    real*8               :: spnnz, piarBoole, nzLoc
    real*8, allocatable  :: kdir(:,:), rorb(:,:), xyz(:,:), tvec(:,:), aoind(:)
    character(len=100)   :: scrinfo
    type(element)        :: elem
    type(OrbitalSetType) :: dataElem
    
    ! output variables
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%size
    
    call GetAtomOrbitalInfo(FRC)
    
    if (FRC%interpolation%vloc) then
        gridn = FRC%domain%fgridn
    else
        gridn = FRC%domain%cgridn
    end if
    
    nn      = product(gridn)
    avec    = FRC%domain%latvec
    evec    = FRC%LCAO%parameters%evec
    rorb    = FRC%LCAO%parameters%Rorb
    oorb    = FRC%LCAO%parameters%Oorb
    nproj   = size(evec,1)
    natom   = FRC%atom%numberOfAtoms
    kdir    = FRC%kpoint%ikdirect
    nkpt    = size(kdir,1)
    useCell = any(KpointKDirect)
    
    if (.not. useCell) then
        FRC%LCAO%dynSubRed = .FALSE.
        FRC%LCAO%veffRed   = .FALSE.
        scrinfo            = "Gamma point calculation"
    end
    
    rad  = maxval(rorb)
    xyz  = FRC%Atom%XYZ
    call cellInRange(avec,xyz,rad,tvec)
    call flipud(tvec)
    ntv  = size(tvec,1)
    !tvec = -tvec(ntv:-1:1,:)
    call GetGridPointCoord(FRC,gridn,GridCoord)
    npoint = size(GridCoord,1)
    
    if (FRC%option%initParaReal .and. useCell) then
        call circum_sphere(avec,rsph)
        allocate(ones(1,3))
        ones(:) = 1
        center  = 0.5*matmul(ones,avec)
        call InitDistArray(N,npproj,ceiling(N/mpisize),1,mpisize,1,aosub%aodata)
        call repmat(aosub,ntv,1,aocell)
        do tt = 1,ntv,1
            aotmp = aosub
            do ii = 1,natom,1
                pos  = FRC%atom%XYZ(ii,:)+matmul(tvec(tt,:),avec)
                elem = FRC%atom%element(ii)
                lnu  = FRC%element(elem)%aolnu
                lcut = FRC%element(elem)%aoLcutoff
                znu  = FRC%element(elem)%aoznu
                zcut = FRC%element(elem)%aoZcutoff
                
                ntmp = 0
                mtmp = size(lnu)
                do itmp = 1,mtmp,1
                    if (lnu(itmp)<=lcut .and. znu(itmp)<=zcut) then
                        ntmp = ntmp+1
                    end if
                end do
                allocate(inde(ntmp))
                ntmp = 0
                do itmp = 1,mtmp,1
                    if (lnu(itmp)<=lcut .and. znu(itmp)<=zcut) then
                        ntmp       = ntmp+1
                        inde(ntmp) = itmp
                    end if
                end do
                
                dataElem = FRC%elementData(elem)%OrbitalSet
                do jj = inde
                    ntmp = size(dataElem%rrData)
                    call norm(pos-center,2,mo)
                    if (mo<rsph+dataElem(jj)%rrdata(FRC%SOB%sizeOfOrbitalSetrrdata)) then
                        LL = dataElem(jj)%L
                        ttmp = 0
                        do kk = 1,nproj,1
                            if (evec(kk)==ii .and. oorb(kk)==jj) then
                                ttmp = ttmp+1
                            end if
                        end do
                        allocate(aoind(ttmp))
                        ttmp = 0
                        do kk = 1,nproj,1
                            if (evec(kk)==ii .and. oorb(kk)==jj) then
                                ttmp = ttmp+1
                                aoind(ttmp) = kk
                            end if
                        end do
                        tmp(:) = .FALSE.
                        forall(kk=aoind)
                            call InterpSph2Cart(Odata(jj),"rrData","frData",pos,ll,GridCoord,avec,tmp,aotmp%aodata(:,aoind))
                        end forall
                    end if
                end do
            end do
            call adjustSparsity(aotmp%apdata,0.2)
            aocell(tt) = aotmp
        end do
        !remove empty cells
        if (.not. useCell) then
            call cellfun(nnz(dataElem,aocell,tmp))
            spnnz = sum(tmp)
            tvec  = (/0,0,0/)
            call adjustSparsity(aocell(1)%dataElem,0.2,aocell(1)%dataElem)
        end if
        
    else if (mpistat .and. (.not. useCell)) then
        tvec(:) = 0
        call repmat(ntv,1,aodata)
        kk = 1
        do ii = 1,natom,1
            pos  = FRC%Atom%XYZ(ii,:)+matmul(tvec(1,:),avec)
            elem = FRC%Atom%Element(ii)
            lnu  = FRC%Element(elem)%AOlnu
            lcut = FRC%Element(elem)%AOLcutoff
            znu  = FRC%Element(elem)%AOznu
            zcut = FRC%Element(elem)%AOZcutoff
            ntmp = 0
            mtmp = size(lnu)
            do itmp = 1,mtmp,1
                if (lnu(itmp)<=lcut .and. znu(itmp)<=zcut) then
                    ntmp = ntmp+1
                end if
             end do
            allocate(inde(ntmp))
            ntmp = 0
            do itmp = 1,mtmp,1
                if (lnu(itmp)<=lcut .and. znu(itmp)<=zcut) then
                    ntmp       = ntmp+1
                    inde(ntmp) = itmp
                end if
            end do
            dataElem = FRC%elementdata(elem)%OrbitalSet
            do jj = inde
                LL = dataElem(jj)%L
                call InterpSph2Cart(dataElem(jj),"rrdata","frdata",pos,LL,GridCoord,avec,(/.TRUE.,.TRUE.,.TRUE./))
                kk = kk+1
            end do
        end do
        call cat(2,aodata,aodata)
        call adjustSparsity(aodata,0.2,aodata)
        call InitDistArray(N,nproj,ceiling(N/mpisize),1,mpisize,1,aodata)
        scrinfo = "Gamma point calculation"
    else
        call repmat(ntv,1,aocell)
        kk = 1
        do ii = 1,natom,1
            pos  = FRC%Atom%XYZ(ii,:)+matmul(tvec(1,:),avec)
            elem = FRC%Atom%Element(ii)
            lnu  = FRC%Element(elem)%AOlnu
            lcut = FRC%Element(elem)%AOLcutoff
            znu  = FRC%Element(elem)%AOznu
            zcut = FRC%Element(elem)%AOZcutoff
            ntmp = 0
            mtmp = size(lnu)
            do itmp = 1,mtmp,1
                if (lnu(itmp)<=lcut .and. znu(itmp)<=zcut) then
                    ntmp = ntmp+1
                end if
            end do
            allocate(inde(ntmp))
            ntmp = 0
            do itmp = 1,mtmp,1
                if (lnu(itmp)<=lcut .and. znu(itmp)<=zcut) then
                    ntmp       = ntmp+1
                    inde(ntmp) = itmp
                end if
            end do
            dataElem = FRC%elementData(elem)%OrbitalSet
            do jj = inde
                LL = dataElem(jj)%L
                allocate(isLoc(ntv))
                isLoc(:) = .FALSE.
                if (mod(ii-1,mpisize) == mpirank) then
                    call InterRdist2Cart(dataElem(jj),"rrData","frData",pos,LL,avec,gridn,.TRUE.,aotmp,tvtmp)
                    call ismenber(tvec,tvtmp,"row",isLoc)
                    call ismenber(tvtmp,tvec,"row",indLoc)
                    ntnp = count(indLoc==0)
                    allocate(rmind(ntnp))
                    call findArray(inLoc,ntnp,0,rmind)
                    if (any(isLoc)) then
                        aocell(indLoc,kk) = aotmp
                    end if
                end if
                
                    
                !if (any(isLoc)) then
                !    aoCell(indLoc,kk) = aotmp
                !end if
                allocate(tmp(N,2*LL+1))
                tmp                    = 0
                aocell(.not. isLoc,kk) = tmp
                    
                kk = kk+1
            end do
        end do
        
        ntnp = size(aoCell,1)
        do ii = 1,ntnp,1
            call cat(2,aocell(ii,:),0.2,tmp)
            call adjustSparsity(tmp,0.2,aocell(ii,1))
            call InitDistArray(N,nproj,ceiling(N/mpisize),1,mpisize,1,aocell(ii,1),aocell(ii,1))
            forall (jj=2:kk-1)
                aocell(ii,jj) = 0
            end forall
        end do
            
        call sprucecell(FRC,aocell(:,1),idel)
        aocell(idel) = 0
            
        if (.not. useCell) then
            call cellfun(dataElem,aocell,tmp)
            spnnz = sum(tmp)
            tvec  = (/0,0,0/)
            call adjustSparsity(aocell(1)%dataElem,0.2,aocell(1)%dataElem)
        end if
            
        if (mpistat) then
            allocate(tmp(npoint,nproj))
            call InitDistArray(N,nproj,ceiling(N/mpisize,1,mpisize,1,tmp,aosub))
            call repmat(aosub,size(aocell),1,aotmp)
            call mpiTransposeCell(aocell,evec,aotmp,aotmp)
            aoCell = aotmp
        end if
    end if
    
    call reduceOper(aocell,tvec,(/0,0,0/),.TRUE.,aotmp)
    call adjustSparsity(aotmp%dataElem,0.2,aotmp%dataElem,aosparse)
    nnzLoc = nnz(aotmp%dataElem)
    
    call findCellPair(FRC,aocell,pairBoole)
    call findPairVec(tvec,pairBoole,pairVec,vecInd)
    call distmat_nnz(FRC,aotmp,nnzTot)
    buff = FRC%Option%BufferSize
    
    if (nkpt > 15) then
        FRC%LCAO%veffRed = .TRUE.
    else
        FRC%LCAO%veffRed = .FALSE.
    end if
    
    if (nkpt*nnzTot/mpisize>buff/16) then
        FRC%LCAO%dynSubRed = .TRUE.
        scrinfo = "The atomic subspace memory requirement is large"
    else
        FRC%LCAO%dynSubRed = .FALSE.
        if (nkpt<100) then
            FRC%LCAO%veffRed = .FALSE.
        end if
        scrinfo = "The atomic subspace memory requirement is small"
    else if (useCell)
        scrinfo = 'User defined'
    end if
    
    if (trim(FRC%info%calculationType) == trim("band-structure") .or. trim(FRC%info%calculationType) == trim("dos")) then
        FRC%LCAO%dynSubRed = .TRUE.
        FRC%LCAO%veffRed   = .TRUE.
        if (trim(FRC%info%calculationType) == trim("band-structure")) then
            scrinfo = "Band structure calculation"
        else if (trim(FRC%info%calculationType) == trim("dos")) then
            scrinfo = "Density of states calculation"
        end if
    end if
    
    if (FRC%LCAO%veffRed) then
        FRC%LCAO%pairBoole = pairBoole
        FRC%LCAO%pairVec   = pairVec
        FRC%LCAO%vecInd    = vecInd
        FRC%LCAO%aoCell    = aoCell
    end if
    
    if (FRC%LCAO%dynSubRed) then
        FRC%LCAO%aoCell    = aoCell
    else if (useCell) then
        do kk = 1,nkpt,1
            kpt = kdir(kk,:)
            call reduceOper(aocell,tvec,kpt,aosparse,FRC%LCAO%aokcell(kk))
        end do
    else if (.not. useCell) then
        FRC%LCAO%aoKcell    = aoCell
    end if
    
    FRC%LCAO%aovec = tvec
    if (aosparse) then
        FRC%LCAO%aosparse = nnzLoc
    else
        FRC%LCAO%aosparse = -1
    end if
    
    return 
    end subroutine GetAtomicOrbitalSubspace
    end module GetAtomicOrbitalSubspace_module
        
            
    module mpiTransposeCell_module
    contains
    subroutine mpiTransposeCell(Acell,jloc,Bcell)
    
    implicit none
    
    ! input variables
    type :: cellType
        real*8, allocatable     :: dataA(:,:)
    end type cellType
    type(cellType), allocatable :: Acell(:),Bcell(:)
    integer, allocatable        :: jloc(:)
    
    ! temporery variables
    logical :: 
    integer :: mpisize, mpirank, nn, mm, jj, kk, ii, indn, indm, Iind, tt
    real*8, allocatable :: indmat(:,:), tmp(:), tmp2(:,:), tmp3(:,:), tmp4(:,:), 
    
    ! output variables
    
    ! body of this function
    
    mpisize = MPI_Comm_size
    mpirank = MPI_Comm_rank
    
    ! find cmmunication pairs
    allocate(tmp(mpisize))
    forall(ii=1:mpisize)
        tmp(ii) = ii
    end forall
    
    call bsxfunMinus(transpose(tmp),tmp,indmat)
    indmat = -indmat
    call flipud(indmat,indmat)
    allocate(tmp2(mpisize,mpisize))
    forall(ii=1:mpisize)
        tmp2(ii,ii) = mpisize
    end forall
    allocate(tmp3(mpisize,mpisize))
    call fliplr(tmp2,tmp3)
    call triu(indmat-tmp3)
    call diag(indmat,tmp4)
    call diag(tmp4,tmp4)
    indmat = indmat - tmp4
    nn = size(indmat,1)
    mm = size(indmat,2)
    do jj = 1,nn,1
        do kk = 1,mm,1
            if (indmat(nn,mm) \= 0) then
                indmat(nn,mm) = mpisize-mod(-indmat(nn,mm),mpisize)
            end if
        end do
    end do
    
    ! get local data, no spatial disstribution
    forall (ii=1:Bcell(1)%m)
        i1(ii) = ii
    end forall
    
    call findArray(mod(jloc-1,mpisize),mpirank,j1)
    call GetGlobalInd(mpirank,Bcell(1),i2)
    call ismember(i1,i2,iloc1)
    call ismember(i2,i1,iloc2)
    
    if (any(iloc1) .and. any(j1) .and. any(iloc2)) then
        do ii = 1,size(Acell),1
            Bcell(ii)%dataA(iloc2,j1) = Acell(ii)%dataA(iloca1,j1)
        end do
    end if
    
    do ii = 1,mpisize,1
        call findMatrix(indmat,ii,indm,indn)
        Iind = (indm == mpirank+1) .or. (indn == mpirank+1)
        indm = indm(Iind)-1
        indn = indn(Iind)-1
        if (any(indm)) then
            do tt = 1,size(Acell),1
                if (mpirank==indn) then
                    call findArray(mod(jloc-1,mpisize,indn,j1))
                    call GetGlobalInd(indm,Bcell(tt),i2)
                    call ismember(i1,i2,iloc1)
                    if (any(isloc1) .and. any(j1)) then
                        call MPI_send_variables(Acell(tt)%dataA(iloc1,j1),indm,0)
                    end if
                    call findArray(mod(jloc-1,mpisize,indm,j1))
                    call GetGlobalInd(indn,Bcell(tt),i2)
                    call ismember(i2,i1,iloc2)
                    if (any(iloc2) .and. any(j1)) then
                        call MPI_recv_variable(indm,0,Bcell(tt)%dataA(iloc2,j1))
                    end if
                end if
                
                if (mpirank==indm) then
                    call findArray(mod(jloc-1,mpisize),indn,j1)
                    call GetGlobalInd(indm,Bcell(tt),i2)
                    call ismember(i2,i1,iloc2)
                    if (any(iloc2) .and. any(j1)) then
                        call MPI_recv_variables(indn,0,Bcell(tt)%dataA(iloc2,j1))
                    end if
                    call findArray(mod(jloc-1,mpisize),indm,j1)
                    call GetGlobalInd(indn,Bcell(tt),i2)
                    call ismember(i1,i2,iloc1)
                    if (any(iloc1) .and. any(j1)) then
                        call MPI_send_variables(Acell(tt)%dataA(iloc1,j1),indn,0)
                    end if
                end if
            end do
        end if
    end do
    
    return
    end subroutine mpiTransposeCell
    end module mpiTransposeCell_module
    
    
    module findPairVec_module
    contains
    subroutine findPairVec(aovec,aopair,pairVec,pairInd,vecInd)
    
    implicit none
    
    ! input variables
    
    ! temporery variables
    
    ! output variables
    
    ! body of this function
    call triu(aopair,tmp)
    call findMatrix(tmp,ia,ja)
    pairVec = aoVec(ja,:)-aoVec(ia,:)
    call unique(pairVec,"rows",pairVec,pairInd,vecInd)
    
    return
    end subroutine findPairVec
    end module findPairVec_module
    
    
    module findCellPair_module
    contains
    subroutine findCellPair(FRC,aocell,pair,pairLoc)
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type :: aocellType
        real*8, allocatable :: dataA(:,:)
    end type aocellType
    type(aocellType), allocatable :: aocell(:)
    
    ! temporery variables
    integer :: ncell
    logical :: gridBoole, ptmp
    real*8, allocatable :: pair(:,:), 
    logical, allocatable :: pairLoc(:,:)
    
    ! output variables
    
    ! body of this function
    mpistatus = FRC%mpi%status
    ncell     = size(aoCell,1)
    call repmat(ncell,1,gridBoole)
    
    do ii = 1,ncell,1
        gridBoole(ii) = any(aoCell(ii)%dataA == 2)
    end do
    
    allocate(pair(ncell,ncell),pairLoc(ncell,ncell))
    pair(:,:)    = 0
    pairLoc(:,:) = 0
    
    do ii = 1,ncell,1
        do jj = 1,ncell,1
            pairLoc(ii,jj) = any(gridBoole(ii) .and. gridBoole(jj))
            ptmp = pairLoc(ii,jj)
            if (mpistat) then
                call double(ptmp,ptmp)
                call MPI_allReduce_sum(ptmp)
                ptmp = logical(ptmp)
            end
            pair(ii,jj) = ptmp
        end do
    end do
    
    return
    end subroutine findCellPair
    end module findCellPair_module
