! norm.f90
!*****************************************
    !
!*****************************************
    
    module norm_module
    contains
    subroutine norm(ArrayA,p,mo)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: ArrayA(:)
    integer             :: p
    
    ! temporery variables
    real*8, allocatable :: ArrayT(:)
    
    ! output variables
    real*8              :: mo
    
    ! body of this function
    allocate(ArrayT(size(ArrayA)))
    ArrayT = abs(ArrayA)
    mo     = sum(ArrayT**p)**real(1/p)
    
    return
    end subroutine norm
    end module norm_module