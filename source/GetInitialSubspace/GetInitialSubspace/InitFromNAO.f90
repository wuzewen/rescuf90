! InitFromNAO
!*******************************************
    !
!*******************************************
    
    module InitFromNAO_module
    contains
    subroutine InitFromNAO(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    integer             :: mpirank, ispin, nspin, nkpt, cnn, i, j
    real*8, allocatable :: kdir(:,:)
    character(len=20)   :: flist(4)
    
    ! output variables
    
    ! body of this function
    mpirank = FRC%mpi%rank
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    
    call GetAtomicOrbitalSubspace(FRC)
    kdir = FRC%kpoint%ikdirect
    nkpt = size(kdir,1)
    call inittime(FRC)
    call KS_main_lcao_scf(FRC)
    
    cnn = cgridn(1)*cgridn(2)*cgridn(3)
    
    if (FRC%LCAO%dynSubRed) then
        if (cnn /= FRC%LCAO%aocell(1)%M) then
            call GetAtomicOrbitalSubspace(FRC,FRC%domain%cgridn,.FALSE.)
        end if
        aocell   = FRC%LCAO%aocell
        aov      = FRC%LCAO%aoev
        aosparse = FRC%LCAO%aosparse
    else
        if (cnn /= FRC%LCAO%aocell(1)%M) then
            call GetAtomicOrbitalSubspace(FRC,FRC%domain%cgridn,.FALSE.)
        end if
        aocell   = FRC%LCAO%aocell
    end if
    
    do i = 1,nkpt,1
        kpt = kdir(i,:)
        if (FRC%LCAO%dynSubRed) then
            call reduceOper(aocell,aov,kpt,apsparse,aosub)
        else
            aosub = aocell(i)
        end if
        
        do j = 1,nspin,1
            if (ispin == 1 .or. ispin == 2) then
                call calcev_lcao(FRC,aosub,FRC%psi(i,j))
            else if (ispin == 4) then
                call calcev_lcao(FRC,aosub,FRC%psi(i,1))
                call calcev_lcao(FRC,aosub,FRC%psi(i,2))
            end if
        end do
    end do
    
    flist(1) = "XSX"
    flist(2) = "XHX"
    flist(3) = "XVX"
    flist(4) = "aosubspace"
    
    return
    end subroutine InitFromNAO
    end module InitFromNAO_module