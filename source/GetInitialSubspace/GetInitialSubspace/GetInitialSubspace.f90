!  GetInitialSubspace.f90 
!****************************************************************************
!
!  PROGRAM: GetInitialSubspace
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module GetInitialSubspace_module
    contains
    subroutine GetInitialSubspace(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use initFromSin_module

    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    integer :: mpisize, mpirank
    
    ! output variables
    

    ! Body of GetInitialSubspace
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    
    !if ((.not. psiStatus) .and. randStatus .and. FRC%mpi%paraK) then
    !    call InitFromRandParaK(FRC)
    !else if ((.not. psiStatus) .and. FRC%LCAO%status)
    !    call InitFromNAO(FRC)
    !    if (FRC%mpi%paraK) then
    !        call distWaveFunctionKpoint(FRC)
    !    end if
    !else if ((.not. psiStatus) .and. any(trim(FRC%eigensolver%init) == trim("sin/cos"))
    call InitFromSin(FRC)
    !else if (psiStatus) then         ! unfinished
    !    if (psiIsChar) then
    !        nkpt = size(KpointIKDrection,1)
    !        call fileParts()
    !    end if
    !else
    !    write(*,*) 'error'
    !end if
    

    return
    end subroutine GetInitialSubspace
    end module

