! InitFromRandParaK.f90
!************************************
    !
    !
    !
!************************************
    
    module InitFromRandParaK_module
    contains
    subroutine InitFromRandParaK(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables 
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical             :: mpistat
    integer             :: mpirank, mpisize, nvec(3), ispin, nspin, nkpt, nband, cn
    integer             :: ii, jj, mm, kk, ss
    real*8              :: vol
    real*8, allocatable :: kdir(:,:), vs(:,:), tmp(:,:), XHX(:,:), XSX(:,:), nrg(:,:,:), ZZ(:,:)
    
    ! output variables
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%size
    nvec    = FRC%domain%cgridn
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    kdir    = FRC%kpoint%ikdirect
    nkpt    = size(kdir,1)
    nband   = FRC%eigensolver%nband
    cn      = product(nvec)
    call det(FRC%domain%latvec,vol)
    vol     = abs(vol)
    dr      = vol/cn
    allocate(nrg(nband,nkpt,nspin))
    call repmat(nkpt,nspin,FRC%psi)
    
    call UpdateVeff(FRC,FRC%potential%veffin(1),FRC%potential%vps,veff)
    call distmat_allgather(FRC,veff,FRC%potential%vloc)
    call distmat_allgether(FRC,FRC%potential%vtau,FRC%potential%vtauloc)
    
    do ii = mpirank+1,nkpt*nspin,mpisize
        call ind2sub((/nkpt,npin/),ii,kk,ss)
        call GetKBorb(FRC,kpt)
        if (ispin == 1 .or. ispin == 2) then
            allocate(vs(cn,kpt))
            forall(jj=1:cn,mm=1:kpt)
                vs(jj,mm) = random_number
                call GenHamFun(FRC,kpt,ss)
            end forall
        else if (ispin == 4) then
            allocate(vs(2*cn,kpt))
            call GenHamFun(FRC,kpt,ss,hamu,hamd)
            !
        end if
        
        if (trim(FRC%eigensolver%init) == trim("rr") .or. trim(FRC%eigensolver%init) == trim("sin/cos/rr")) then
            call ham(vs,tmp)
            XHX = matmul(transpose(vs),tmp)*dr
            XSX = matmul(transpose(vs),vs)*dr
            call eig(XHX,XSX,ZZ,nrg(:,kk,ss))
            vs = matmul(vs,ZZ)
        else
            do nn = 1,nband,1
                call ham(vs(:,nn),tmp)
                nrg(nn,kk,ss) = matmul(transpose(vs(:,nn)),tmp)*dr
            end do
        end if
        
        if (ispin == 1 .or. ispin == 2) then
            FRC%psi(kk,ss) = vs
        else
            FRC%psi(kk,1) = vs(1:cn,:)
            FRC%psi(kk,2) = vs(cn+1:2*cn,:)
        end if
    end do
    
    if (mpisat) then
        call MPI_Allreduce_sum(nrg)
    end
    
    FRC%energy%ksnrg(1) = nrg
    
    return
    end subroutine InitFromRandParaK
    end module InitFromRandParaK_module
                
    