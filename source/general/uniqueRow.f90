! uniqueRow.f90
    
!******************************************************************************
    !
    !
    !
!******************************************************************************
    
    module uniqueRow_module
    contains
    subroutine uniqueRow(Ain,Aout)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: Ain(:,:)
    
    ! temporary variables
    real*8, allocatable :: Atmp(:,:)
    integer :: n, m, kk, ii, jj, ntmp
    logical :: issame
    
    ! output variables
    real*8, allocatable :: Aout(:,:)
    
    ! body of this function
    n   = size(Ain,1)
    m   = size(Ain,2)
    
    allocate(Aout(1,m))
    Aout(1,:) = Ain(1,:)
    
    kk = 1
    do ii = 2,n,1
        ntmp = size(Aout,1)
        issame = .FALSE.
        do jj = 1,ntmp,1
            if (all(Ain(ii,:) == Aout(jj,:))) then
                issame = .TRUE.
            end if
        end do
        if (.not. issame) then
            kk = kk+1
            allocate(Atmp(ntmp,m))
            Atmp = Aout
            deallocate(Aout)
            allocate(Aout(kk,m))
            Aout(1:kk-1,:) = Atmp
            Aout(kk,:)     = Ain(ii,:)
            deallocate(Atmp)
        end if
    end do
    
    return
    end subroutine uniqueRow
    end module uniqueRow_module