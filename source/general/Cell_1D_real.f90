! Cell_1D_real.f90
    
!******************************************************
    !
    ! This file is to define a data type, in which,
    ! contains one dimentional real Array.
    ! while, this type of data can be defined in
    ! any number of dimentions.
    !
!******************************************************
    
    module Cell_1D_real
    
    type :: Cell_1D_rea
        real*8, allocatable :: vata(:)
    end type Cell_1D_rea
    
    end module Cell_1D_real