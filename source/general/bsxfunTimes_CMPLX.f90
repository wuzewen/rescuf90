! bsxfunTime.f90
!******************************************************************************
    ! This function has the same function with function bsxfun(@times,*,*)
    ! in matlab. While, here a should be two dimentional matrix, and b sh-
    ! ould be an array.
!******************************************************************************
    
    module bsxfunTimes_CMPLX_module
    contains
    subroutine bsxfunTimes_CMPLX(MatrixA,MatrixB,MatrixC)
    
    implicit none
    
    ! input valuables
    complex,allocatable :: MatrixA(:,:), MatrixB(:,:)
    
    ! temporery valuables
    integer            :: mA, nA, mB, nB
    integer            :: ii, jj

    ! output valuables
    complex,allocatable :: MatrixC(:,:)
    
    ! body of this function
    mA = size(MatrixA,1)
    nA = size(MatrixA,2)
    mB = size(MatrixB,1)
    nB = size(MatrixB,2)
    
    if (mA == 1 .and. nB == 1) then
        do ii = 1,nA,1
            do jj = 1,mB,1
                MatrixC(jj,ii) = MatrixA(1,ii)*MatrixB(jj,1)
            end do
        end do
    else if (nA == 1 .and. mB == 1) then
        do ii = 1,mA,1
            do jj = 1,nB,1
                MatrixC(ii,jj) = MatrixA(ii,1)*MatrixB(1,jj)
            end do
        end do
    else if (mA == 1 .and. nA == nB) then
        do ii = 1,nA,1
            do jj = 1,mB,1
                MatrixC(jj,ii) = MatrixA(1,ii)*MatrixB(jj,ii)
            end do
        end do
    else if (nA == 1 .and. mA == mB) then
        do ii = 1,mA,1
            do jj = 1,nB
                MatrixC(ii,jj) = MatrixA(ii,1)*MatrixB(ii,jj)
            end do
        end do
    else if (mB == 1 .and. nA == nB) then
        do ii = 1,nB,1
            do jj = 1,mA,1
                MatrixC(jj,ii) = MatrixB(1,ii)*MatrixA(jj,ii)
            end do
        end do
    else if (nB == 1 .and. mB == mA) then
        do ii = 1,mB,1
            do jj = 1,nA,1
                MatrixC(ii,jj) = MatrixB(ii,1)*MatrixA(ii,jj)
            end do
        end do
    else if (mA == mB .and. nA == nB) then
        MatrixC = MatrixA*MatrixB
    else
        write(*,*) "Error in bsxfunTimes.f90."
        stop
    end if
    
    return
    end subroutine bsxfunTimes_CMPLX
    end module bsxfunTimes_CMPLX_module
        