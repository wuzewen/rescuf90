! factorial.f90
    
!**************************************************************************************
    !
    !
    !
!**************************************************************************************
    
    module factorial_module
    contains
    subroutine factorial(n,nout)
    
    implicit none
    
    ! input variables
    integer :: n
    
    ! temporary variables
    integer :: ii
    
    ! output variables
    integer :: nout
    
    ! body of this function
    nout = 1
    do ii = 1,n,1
        nout = nout*ii
    end do
    
    return
    end subroutine factorial
    end module factorial_module