! bsxfunTimes_3D_2D.f90
    
!*************************************
    !
    !
    !
!*************************************
    
    module bsxfunTimes_3D_2D_module
    contains
    subroutine bsxfunTimes_3D_2D(A,B,C)
    
    use bsxfunTimes_3D_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:,:), B(:,:)
    
    ! temporary variables
    real*8, allocatable :: tmpB(:,:,:)
    
    ! output variables
    real*8, allocatable :: C(:,:,:)
    
    ! body of this function
    allocate(tmpB(size(B,1),size(B,2),1))
    tmpB(:,:,1) = B(:,:)
    call bsxfunTimes_3D(A,tmpB,C)
    
    return
    end subroutine bsxfunTimes_3D_2D
    end module bsxfunTimes_3D_2D_module