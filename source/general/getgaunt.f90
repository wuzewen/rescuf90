! getgaunt.f90
    
!******************************************************************
    !
    !
    !
!******************************************************************
    
    module getgaunt_module
    contains
    subroutine getgaunt(lu,lv,mu,mv,l,output)
    
    implicit none
    
    ! input variables
    integer, allocatable :: lu(:), lv(:), mu(:), mv(:), l(:)
    
    ! temporary variables
    real*8  :: pi
    integer :: mtmp, ntmp, ii
    integer, allocatable :: lutmp(:), lvtmp(:), mutmp(:), mvtmp(:), ltmp(:), prefactor(:), c1(:), c2(:), zr(:)
    
    ! ouput variables
    integer, allocatable :: output(:)
    
    ! body of this function
    ntmp = size(l)
    pi   = 3.14159265354
    allocate(output(ntmp))
    mtmp = 0
    do ii = 1,ntmp,1
        if (2*l(ii)+1 < 0) then
            output(ii) = 0
        else
            mtmp = mtmp+1
        end if
    end do
    
    allocate(lutmp(mtmp),lvtmp(mtmp),mutmp(mtmp),mvtmp(mtmp),ltmp(mtmp))
    
    mtmp = 0
    do ii = 1,ntmp,1
        if (2*l(ii)+1 >= 0) then
            mtmp        = mtmp+1
            lutmp(mtmp) = lu(ii)
            lvtmp(mtmp) = lv(ii)
            mutmp(mtmp) = mu(ii)
            mvtmp(mtmp) = mv(ii)
            ltmp(mtmp)  = l(ii)
        end if
    end do
    
    allocate(prefactor(mtmp))
    do ii = 1,mtmp,1
        prefactor(ii) = ((2*lu(ii)+1)*(2*lv(ii)+1)/(4.0*pi*(2.0*l(ii)+1)))**0.5
        if (mod(mu(ii),2) /= 0) then
            prefactor(ii) = -prefactor(ii)
        end if
    end do
    
    allocate(c1(mtmp))
    call clebschgordan(lutmp,lvtmp,ltmp,-mutmp,mvtmp,-mutmp+mvtmp,c1)
    allocate(zr(mtmp))
    zr = 0
    allocate(c2(mtmp))
    call clebschgordan(lutmp,lvtmp,ltmp,zr,zr,zr,c2)
    
    mtmp = 0
    do ii = 1,ntmp,1
        if (2*l(ii)+1 >= 0) then
            mtmp = mtmp+1
            output(ii) = prefactor(mtmp)*c1(mtmp)*c2(mtmp)
        end if
    end do
    
    return
    end subroutine getgaunt
    end module getgaunt_module
    