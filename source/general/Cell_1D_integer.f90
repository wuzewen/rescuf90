! Cell_1D_integer.f90
    
!******************************************************
    !
    ! This file is to define a data type, in which,
    ! contains one dimentional integer Array.
    ! while, this type of data can be defined in
    ! any number of dimentions.
    !
!******************************************************
    
    module Cell_1D_integer
    
    type :: Cell_1D_int
        integer, allocatable :: vata(:)
    end type Cell_1D_int
    
    end module Cell_1D_integer