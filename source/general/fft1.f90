! fft.f90
    
    module fft1_module
    contains
    subroutine fft1(A,B)
    
    implicit none
    
    ! input valuables
    complex, allocatable  ::  A(:)
    
    ! temporery valuables
    integer               ::  i
    integer               ::  N
    complex, parameter    ::  imnu = (0,1), pi = (3.14159265354,0)
    
    ! output valuables
    complex, allocatable  ::  B(:)
    
    ! body of this function
    
    N = size(A)
    B(:) = 0
    do i = 1,N,1
        do j = 1,N,1
            B(i) = B(i)+A(j)*exp(-imnu*2*pi*cmplx(i-1)*(j-1)/cmplx(N))
        end do
    end do
    
    end subroutine fft1
    end module fft1_module
    