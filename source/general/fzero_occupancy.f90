! fzero_occupancy.f90
    
!***********************************************************************************************
    !
    !
    !
!***********************************************************************************************
    
    module fzero_occupancy_module
    contains
    subroutine fzero_occupancy(sigma,kweight,energy,ntot,mu0,mu)
    
    use fermiDirac_3D_module
    
    implicit none
    
    ! input variables
    real*8              :: sigma, ntot, mu0
    real*8, allocatable :: kweight(:), energy(:,:,:)
    
    ! temporary variables
    real*8, allocatable :: focc(:,:,:), foccT(:)
    real*8              :: init0, init1, init2, init3, tmp1, tmp2, mut, h
    
    ! output variables
    real*8 :: mu
    
    ! body of this function
    call fermiDirac_3D(sigma,mu0,energy,focc)
    foccT = sum(sum(focc,3),1)
    deallocate(focc)
    init0 = sum(foccT*kweight)-ntot
    init1      = init0
    if (init1 > 0) then
        init2      = 1.0
        init3      = 1.0
    else if (init1 == 0) then
        mu = mu0
        return
    else
        init2      = -1.0
        init3      = -1.0
    end if
        
    h          = 0.00000001
    mu         = mu0
    mut        = mu
    tmp1       = init0*init2
    tmp2       = init1*init3
    do while (tmp1 > 0 .and. tmp2 > 0)
        mu = mu+h
        call fermiDirac_3D(sigma,mu,energy,focc)
        foccT = sum(sum(focc,3),1)
        deallocate(focc)
        init2 = sum(foccT*kweight)-ntot
        tmp1  = init0*init2
        if (tmp1 < 0) then
            mu = mu-h/2.0
            exit
        end if
        init0      = init2
        
        mut = mut-h
        call fermiDirac_3D(sigma,mut,energy,focc)
        foccT = sum(sum(focc,3),1)
        deallocate(focc)
        init3 = sum(foccT*kweight)-ntot
        tmp2  = init1*init3
        if (tmp2 < 0) then
            mu = mut+h/2.0
            exit
        end if
        init1      = init3
    end do
    
    return
    end subroutine fzero_occupancy
    end module fzero_occupancy_module