! Cell_3D_cmplx.f90
    
!******************************************************
    !
    ! This file is to define a data type, in which,
    ! contains three dimentional complex Array.
    ! while, this type of data can be defined in
    ! any number of dimentions.
    !
!******************************************************
    
    module Cell_3D_cmplx
    
    type :: Cell_3D_cmpl
        complex, allocatable :: vata(:,:,:)
    end type Cell_3D_cmpl
    
    end module Cell_3D_cmplx