! gengautmatrix.f90
    
!******************************************************************************
    !
    !
    !
!******************************************************************************
    
    module gengautmatrix_module
    contains
    subroutine gengautmatrix(lmax,gauntmat)
    
    implicit none
    
    ! input variables
    integer :: lmax
    
    ! temporary variables
    integer :: kk, lu, lv, mu, mv, Luv, ktmp, ii
    integer, allocatable :: gau(:,:,:,:,:), l1(:), l2(:), l3(:), m1(:), m2(:), ind(:,:), gauntmatmp(:)
    
    ! output variables
    integer, allocatable :: gauntmat(:)
    
    ! body of this function
    allocate(gau(lmax+1,lmax+1,2*lmax+1,2*lmax+1,2*lmax+1))
    
    kk = 1
    do lu = 0,lmax,1
        do lv = 0,lmax,1
            do mu = -lu,lu,1
                do mv = -lv,lv,1
                    do Luv = abs(lu-lv),(lu+lv),1
                        kk = kk+1
                    end do
                end do
            end do
        end do
    end do
    
    allocate(l1(kk),l2(kk),l3(kk),m1(kk),m2(kk))
    allocate(ind(kk,5))
    
    kk = 1
    do lu = 0,lmax,1
        do lv = 0,lmax,1
            do mu = -lu,lu,1
                do mv = -lv,lv,1
                    do Luv = abs(lu-lv),(lu+lv),1
                        l1(kk) = lu
                        l2(kk) = lv
                        l3(kk) = Luv
                        m1(kk) = mu
                        m2(kk) = mv
                        ind(kk,1) = lu+1
                        ind(kk,2) = lv+1
                        ind(kk,3) = mu+lu+1
                        ind(kk,4) = mv+lv+1
                        ind(kk,5) = Luv-abs(lu-lv)+1
                        kk = kk+1
                    end do
                end do
            end do
        end do
    end do
    
    call getgaunt(l1,l2,m1,m2,l3,gauntmatmp)
    
    ktmp = 1
    do lu = 1,kk,1
        do lv = 1,kk,1
            do mu = 1,kk,1
                do mv = 1,kk,1
                    do Luv = 1,kk,1
                        ktmp = ktmp+1
                        gauntmat(ktmp) = gauntmatmp(ii)
                    end do
                end do
            end do
        end do
    end do
    
    return
    end subroutine gengautmatrix
    end module gengautmatrix_module