! cellfunnnz.f90

!***************************************************************************
    !
    ! This function is used to count the number non-zero element in 
    ! AcellType data (now, only in the type of data). If the type is 
    !changed, this function should be fixed.
    !
!***************************************************************************
    
    module cellfunnnz_module
    contains
    subroutine cellfunnnz(Acell,spnnz)
    
    use Acell_type
    
    implicit none
    
    ! input variables
    type(AcellType), allocatable :: Acell(:)
    
    ! temporary variables
    integer :: n, ii
    
    ! output variables
    integer :: spnnz
    
    ! body of this function
    n     = size(Acell)
    spnnz = 0
    do ii = 1,n,1
        spnnz = spnnz + count(Acell(ii)%vata /= 0)
    end do
    
    return
    end subroutine cellfunnnz
    end module cellfunnnz_module