! Cell_2D_integer.f90
    
!******************************************************
    !
    ! This file is to define a data type, in which,
    ! contains two dimentional integer Array.
    ! while, this type of data can be defined in
    ! any number of dimentions.
    !
!******************************************************
    
    module Cell_2D_integer
    
    type :: Cell_2D_int
        integer, allocatable :: vata(:,:)
    end type Cell_2D_int
    
    end module Cell_2D_integer