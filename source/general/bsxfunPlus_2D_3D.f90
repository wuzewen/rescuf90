! bsxfunPlus_3D_2D.f90
    
!*************************************
    !
    !
    !
!*************************************
    
    module bsxfunPlus_2D_3D_module
    contains
    subroutine bsxfunPlus_2D_3D(A,B,C)
    
    use bsxfunPlus_3D_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:,:)
    
    ! temporary variables
    real*8, allocatable :: tmpA(:,:,:)
    
    ! output variables
    real*8, allocatable :: C(:,:,:)
    
    ! body of this function
    allocate(tmpA(size(A,1),size(A,2),1))
    tmpA(:,:,1) = A(:,:)
    call bsxfunPlus_3D(tmpA,B,C)
    
    return
    end subroutine bsxfunPlus_2D_3D
    end module bsxfunPlus_2D_3D_module