! bsxfunTimes_2D_3D.f90
    
!******************************************************
    !
    !
    !
!******************************************************
    
    module bsxfunTimes_2D_3D_module
    contains
    subroutine bsxfunTimes_2D_3D(A,B,C)
    
    use bsxfunTimes_3D_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:,:)
    
    ! temporary variables
    real*8, allocatable :: tmpA(:,:,:)
    
    ! output variables
    real*8, allocatable :: C(:,:,:)
    
    ! body of this function
    allocate(tmpA(size(A,1),size(A,2),1))
    tmpA(:,:,1) = A(:,:)
    call bsxfunTimes_3D(tmpA,B,C)
    
    return
    end subroutine bsxfunTimes_2D_3D
    end module bsxfunTimes_2D_3D_module
    