! intersectRow.f90
    
!************************************************************************
    !
    !
    !
!************************************************************************
    
    module intersectRow_module
    contains
    subroutine intersectRow(A,B,samerow)
    
    use uniqueRow_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:)
    
    ! temporary varibales
    real*8, allocatable :: Atmp(:,:), Btmp(:,:)
    integer :: mA, nA, mB, nB, ntmp, ii, jj
    
    ! output varibales
    real*8, allocatable :: samerow(:,:)
    
    ! body of this function
    mA = size(A,1)
    nA = size(A,2)
    mB = size(B,1)
    nB = size(B,2)
    
    if (nA /= nB) then
        write(*,*) "Error in intersectRow.f90. Sizes of the inputs are invalid."
        stop
    end if
    
    call uniqueRow(A,Atmp)
    call uniqueRow(B,Btmp)
    mA = size(Atmp,1)
    nA = size(Atmp,2)
    mB = size(Btmp,1)
    nB = size(Btmp,2)
    
    ntmp = 0
    do ii = 1,mA,1
        do jj = 1,mB,1
            if (all(Atmp(ii,:) == Btmp(jj,:))) then
                ntmp = ntmp+1
                !allocate(outm(ntmp,nA))
            end if
        end do
    end do
    allocate(samerow(ntmp,nA))
     ntmp = 0
    do ii = 1,mA,1
        do jj = 1,mB,1
            if (all(Atmp(ii,:) == Btmp(jj,:))) then
                ntmp            = ntmp+1
                samerow(ntmp,:) = Atmp(ii,:)
            end if
        end do
    end do
    
    return
    end subroutine intersectRow
    end module intersectRow_module