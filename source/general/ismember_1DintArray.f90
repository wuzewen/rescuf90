! ismember_1DintArray.f90
    
!***************************************************************************
    !
    !
    !
!***************************************************************************
    
    module ismember_1DintArray_module
    contains
    subroutine ismember_1DintArray(A,B,isLoc)
    
    implicit none
    
    ! input variables
    integer, allocatable :: A(:), B(:)
    
    ! temporary variables
    integer :: nA, nB, ii
    
    ! output variables
    logical, allocatable :: isLoc(:)
    
    ! body of this function
    nA = size(A)
    allocate(isLoc(nA))
    
    do ii = 1,nA,1
        isLoc(ii) = any(B == A(ii))
    end do
    
    return
    end subroutine ismember_1DintArray
    end module ismember_1DintArray_module