! Cell_3D_real.f90
    
!******************************************************
    !
    ! This file is to define a data type, in which,
    ! contains three dimentional integer Array.
    ! while, this type of data can be defined in
    ! any number of dimentions.
    !
!******************************************************
    
    module Cell_3D_real
    
    type :: Cell_3D_rea
        real*8, allocatable :: vata(:,:,:)
    end type Cell_3D_rea
    
    end module Cell_3D_real