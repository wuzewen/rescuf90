! fft.f90
    
    module fft2_module
    contains
    subroutine fft2(A,dim,B)
    
    implicit none
    
    ! input valuables
    integer               ::  dim
    real*8, allocatable   ::  A(:,:)
    
    ! temporery valuables
    integer               ::  i, j
    integer               ::  N
    complex, parameter    ::  imnu = (0,1), pi = (3.14159265354,0)
    
    ! output valuables
    complex, allocatable  ::  B(:,:)
    
    ! body of this function
    
    N = size(A,dim)
    B(:,:) = 0
    do i = 1,N,1
        do j = 1,N,1
            if (dim==1) then
                B(i,:) = B(i,:)+A(j,:)*exp(-imnu*2*pi*cmplx(i-1)*(j-1)/cmplx(N))
            else if (dim==2) then
                B(:,i) = B(:,i)+A(:,j)*exp(-imnu*2*pi*cmplx(i-1)*(j-1)/cmplx(N))
            end if
        end do
    end do
    
    end subroutine fft2
    end module fft2_module
    