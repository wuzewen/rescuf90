! VataMN_3D.f90
    
!**************************************************************
    !
    ! This module will contain a kind of data type including
    ! three dimentional real array vata, and integers m, n, 
    ! mblock, nblock, mproc, nproc.
    !
!**************************************************************
    
    module VataMN_3D_module
    
    type :: VataMN_3D
        real*8, allocatable :: vata(:,:,:)
        integer             :: m
        integer             :: n
        integer             :: mblock
        integer             :: nblock
        integer             :: mproc
        integer             :: nproc
    end type VataMN_3D
    
    end module VataMN_3D_module