! GenGmask.f90
!*********************************
    !
!*********************************
    
    module GenGmask_module
    contains
    subroutine GenGmask(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary valuables
    
    ! output variables
    
    ! body of this function
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%size
    nkpt    = size(FRC%Kpoint%kdirect,1)
    avec    = FRC%domain%latvec
    bvec    = FRC%domain%recvec
    
    call det(avec,vol)
    Exx%vol = abs(vol)
    
    nu = domain%fgridn(1)
    nv = domain%fgridn(2)
    nw = domain%fgridn(3)
    
    forall(i=1:nu/2+1)
        GCGridX = i-1
    end forall
    forall(i=nu/2+2:nu)
        GcGridX = i-nu-1
    end forall
    
    forall(i=1:nv/2+1)
        GCGridY = i-1
    end forall
    forall(i=nv/2+2:nv)
        GcGridY = i-nv-1
    end forall
    
    forall(i=1:nw/2+1)
        GCGridZ = i-1
    end forall
    forall(i=nw/2+2:nw)
        GcGridZ = i-nw-1
    end forall
    
    call ndgrid(GCGridX,GCGridY,GCGridZ,GGX,GGY,GGZ)
    nn = size(GGX)
    allocate(GGI(nn,3))
    GGI(:,1) = reshape(GGX,(/nn,1/))
    GGI(:,2) = reshape(GGY,(/nn,1/))
    GGI(:,3) = reshape(GGZ,(/nn,1/))
    
    Gcutoff  = FRC%Exx%Gcutoff
    Gvector  = matmul(GGI,bvec)
    GV2      = sum(Gvector**2,2)*0.5
    call selectLess(GV2,Gcutoff,GCGridBoolen)
    nn       = size(GCGridBoolen)
    GCB      = reshape(GCGridBoolen,(/nn/))
    call findequal(GCB,iu)
    call ind2sub((/nu,nv,nw/),iu,iu,iv,iw)
    limu = 
    allocate(masktmp(nu))
    masktmp(:) = .FALSE.
    forall(ii=1:limu(1))
        masktmp(ii) = .TRUE.
    end forall
    forall(ii=limu(2):nu)
        masktmp(ii) = .TRUE.
    end forall
    FRC%EXX%Gmasku = masktmp
    
    limv = 
    allocate(masktmp(nv))
    masktmp(:) = .FALSE.
    forall(ii=1:limv(1))
        masktmp(ii) = .TRUE.
    end forall
    forall(ii=limv(2):nv)
        masktmp(ii) = .TRUE.
    end forall
    FRC%EXX%Gmaskv = masktmp
    
    limw = 
    allocate(masktmp(nw))
    masktmp(:) = .FALSE.
    forall(ii=1:limw(1))
        masktmp(ii) = .TRUE.
    end forall
    forall(ii=limw(2):nw)
        masktmp(ii) = .TRUE.
    end forall
    FRC%EXX%Gmaskw = masktmp
    
    GV2t = reshape(GV2,(/nu,nv,nw/))
    
    allocate(GgridBoole(nu,nv,nw))
    do ii = 1,nu,1
        do jj = 1,nv,1
            do kk = 1,nw,1
                if (GV2t(FRC%EXX%Gmasku(ii),FRC%EXX%Gmasku(jj),FRC%EXX%Gmasku(kk))<Gcutoff) then
                    GgridBoole(ii,jj,kk) = .TRUE.
                else
                    GgridBoole(ii,jj,kk) = .FALSE.
                end if
            end do
        end do
    end do
    
    allocate(Ggridx2(limu(1)+limu(2)-nu+1))
    allocate(Ggridy2(limv(1)+limv(2)-nv+1))
    allocate(Ggridz2(limw(1)+limw(2)-nw+1))
    
    forall(ii=1:limu(1))
        Ggridx2(ii) = ii-1
    end forall
    forall(ii=limu(1)+1:limu(1)+limu(2)-nu+1)
        Ggridx2(ii) = ii-(limu(1)+limu(2)-nu+1)-1
    end forall
    
    forall(ii=1:limv(1))
        Ggridy2(ii) = ii-1
    end forall
    forall(ii=limv(1)+1:limv(1)+limv(2)-nv+1)
        Ggridy2(ii) = ii-(limv(1)+limv(2)-nv+1)-1
    end forall
    
    forall(ii=1:limw(1))
        Ggridz2(ii) = ii-1
    end forall
    forall(ii=limw(1)+1:limw(1)+limw(2)-nw+1)
        Ggridz2(ii) = ii-(limw(1)+limw(2)-nw+1)-1
    end forall
    
    call ndgrid(Ggridx2,Ggridy2,Ggridz2,GgridInd)
    
    if (all(GgridBoole)) then
        if (mpirank == 0) then
            Gmax = floor(max(GVector))
        end if
    end if
    
    if (nkpt == 1) then
        do ii = 1,nu*nv*nw,1
            if (GgridInd(ii,3)<0) then
                GgridBoole(ii) = .FALSE.
            end if
            if (GgridInd(ii,2)<0 .and. GgridInd(ii,3) == 0) then
                GgridBoole(ii) = .FALSE.
            end if
            if (GgridInd(ii,1)<0 .and. GgridInd(ii,2) == 0 .and. GgridInd(ii,3) == 0) then
                GgridBoole(ii) = .FALSE.
            end if
        end do
    end if
    
    FRC%EXX%GgridBoole = GgridBoole
    nG                 = sum(GgridBoole)
    call InitDistArray(nG,1,ceiling(nG/mpisize),1,mpisize,1,temp)
    call GetGlobalInd(mpirank,temp,ia)
    temp = GgridInd(GgridBoole,:)
    FRC%EXX%GgridInd = temp(ia,:)
    FRC%EXX%nG       = nG
    FRC%EXX%loc_nG   = size(FRC%EXX%GgridInd,1)
    
    return
    end subroutine GenGmask
    end module GenGmask_module
    