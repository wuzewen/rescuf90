! EwaldSum.f90
!********************************************
    !
!********************************************
    
    module EwaldSum_module
    contains
    subroutine EwaldSum(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    
    ! output variables
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    avec    = FRC%domain%latvec
    call inversion(avec,bvec)
    bvec = transpose(bvec)*pi*2
    xyz  = FRC%Atom%XYZ
    call det(avec,vol)
    vol = abs(vol)
    natom = FRC%atom%numberOfAtom
    nelem = FRC%atom%numberOfElement
    allocate(Z(nelem))
    Z(:) = 0
    do ii = 1,nelem,1
        Z(ii) = FRC%Element(ii)%Z
    end do
    
    smimp = FRC%smi%mp
    sminp = FRC%smi%np
    smimb = ceiling(real(natom)/real(smimp))
    sminb = ceiling(real(natom)/real(sminp))
    
    call InitDistArray(natom,natom,smimb,sminb,smimp,sminp)
    call GetGlobalInd(mpirank,dummy,ia,ja)
    
    qmax = sqrt(sum(Z**2)/natom)
    eta  = sqrt(pi)/vol**(1/3)
    tol  = eps
    terr = pi*natom**2*qmax**2/vol/eta**2
    gerr = natom**2*qmax**2/sqrt(pi)*eta
    tcut = sqrt(-log(tol)+log(terr))/eta
    gcut = 2*eta*sqrt(-log(tol)+log(gerr))
    
    call genvecmat(xyz(ia,:),xyz(ja,:),distvec,distsca)
    
    onetmp(:) = 0.5
    center = matmul(onetmp,bvec)
    call norm(center,rad)
    call cellInRange(bvec,center,gcut+rad,1,gvec)
    gvec = matmul(gvec,bvec)
    gsqr = sqrt(sum(gvec**2,2))
    
    allocate(inde(3))
    do ii = 1,3,1
        if (gsqr(ii)>gcut(ii) .or. gsqr == 0) then
            inde(ii) = .TRUE.
        else
            inde(ii) = .FALSE.
        end if
    end do
    
    ng   = size(gsqr)
    call permute(gvec,(/1,3,2/),gvec)
    
    Ewald1 = 0
    if (size(distca)) then
        m = size(distvec,1)
        n = size(distvec,2)
        allocate(Fwald1(m,n))
        do ii = 1,ng,1
            call��bsxfunTimes(distvec,gvec(ii,:,:),Etmp)
            Ftmp = sin(sum(Etmp,3))
            Etmp = cos(sum(Etmp,3))
            Etmp = 1/gsqr(ii)*Etmp*exp(-gsqr(ii)/4/eta**2)
            Ewald1 = Ewald1+Etmp
            call bsxfunTimes(Ftmp,gvec(ii,:,:),Ftmp)
            Fwald1 = Fwald1+Ftmp
        end do
        Ewald1 = 2*pi/vol*matmul(transpose(Z(ia)),matmul(Ewald1,Z(ja)))
        call bsxfunTimes(Fwald1,transpose(Z(ja)),Fwald1)
        call bsxfunTimes(Z(ia),Fwald1,Fwald1)
        Fwald1 = sum(Fwald1,2)
    else
        allocate(Fwald1(size(ia),3))
        Fwald1(:,:) = 0
    end if
    
    Ftmp = 4*pi/vol*Fwald1
    allocate(Fwald1(natom,3))
    Fwald1(ia,:) = Ftmp
    if (mpistat) then
        call MPI_Allreduce_sum(Ewald1,Ewald1)
        call MPI_Allreduce_sum(Ewald1,Ewald1)
    end if
    
    Ewald2 = -pi/2/vol/eta**2*sum(Z)**2
    Ewald3 = -eta/sqrt(pi)*sum(Z)**2
    
    center = matmul(onetmp,avec)
    call norm(center,rad)
    call cellInRange(avec,center,tcut+rad,1,tvec)
    tvec = matmul(tvec,avec)
    tsqr = sqrt(sum(tvec**2,2))
    
    inde
    
    tsqr = tsqr**2
    nt   = size(tsqr)
    
    Ewald4 = 0
    if (size(distca)) then
        Ewald4 = erfc(eta*distsca)/dsitsca
        call bsxfunEQ(transpose(ia),ja,tmp)
        Ewald4(tmp) = 0
        Fwald4 = erfc(eta*distsca)/distsca**3 + 2*eta/sqrt(pi)*exp(-(eta*distsca)**2)/distsca**2
        call bsxfunEQ(transpose(ia),ja,tmp)
        Fwald4(tmp) = 0
        call bsxfunTimes(Fwalsd4,distvec,Fwald4)
        do i = 1,nt,1
            call bsxfunPlus(xyz(ja,:),tvec(ii,:),Etmp)
            call genvecmat(xyz(ia,:),Etmp,distvec,distsca)
            Etmp   = erfc(eta*distsca)/distsca
            Ewald4 = Ewald4+Etmp
            Ftmp   = erfc(eta*distsca)/distsca**3 + 2*eta/sqrt(pi)*exp(-(eta*distsca)**2)/distsca**2
            call bsxfunTimes
            Fwald4 = Fwald4+Ftmp
        end do
        Ewald4 = 1/2*matmul(transpose(Z(ia)),matmul(Ewald4,Z(ja)))
        call bsxfunTimes(Fwald4,transpose(Z(ja)),Fwald4)
        call bsxfunTimes(Z(ia),Fwald4,Fwald4)
    else
        allocate(Fwald4(size(ia),3))
        Fwald4(:,:) = 0
    end if
    
    Ftmp   = reshape(Fwald4,(/:,3/))
    allocate(Fwald4(natom,3))
    Fwald4(:,:)  = 0
    Fwald4(ia,:) = Ftmp
    
    if (mpistat) then
        call MPI_Allreduce_sum(Ewald4,Ewald4)
        call MPI_Allreduce_sum(Fwald4,Fwald4)
    end if
    
    Ewald = Ewald1+Ewald2+Ewald3+Ewald4
    Fwald = Fwald1+Fwald4
    
    return
    end subroutine EwaldSum
    end module EwaldSum_module
            