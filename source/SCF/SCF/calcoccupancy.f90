! calcoccupancy.f90
!********************************
    !
!********************************
    
    module calcoccupancy_module
    contains
    subroutine calcoccupancy(FRC,energy,mu,nocc,entropy)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use fermiDirac_module
    use bsxfunTimes_module
    use fzero_occupancy_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable             :: energy(:,:)
    
    ! temporary variables
    logical           :: mpistat
    character(len=20) :: method
    integer           :: nband, nkpt, ispin, nspin, xeig, nval, ii
    real*8            :: mu0, sigma, xt, ntot
    
    real*8, allocatable :: kweight(:), energyT(:,:,:), mu1(:,:,:), x(:), nfun(:), focc(:), ent(:,:), energyTmp(:,:,:)
    real*8, allocatable :: enerT(:), focc2(:), nocc2(:,:), kweight2(:,:)
    
    ! output variables
    real*8              :: mu
    real*8, allocatable :: nocc(:,:,:), entropy(:,:)
    
    ! body of this function
    mpistat = FRC%mpi%status
    method  = FRC%kpoint%sampling
    nband   = size(energy,1)
    nkpt    = size(energy,2)
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    xeig    = FRC%eigensolver%emptyBand
    Nval    = FRC%eigensolver%Nvalence
    kweight = FRC%kpoint%weight
    energyT = reshape(energy,(/nband,nkpt,nspin/))
    
    if (ispin == 1) then
        ntot = Nval/2
    else
        ntot = Nval
    end if
    
    !if (FRC%energy%EFermi > 20) then
    !    mu0 = FRC%energy%EFermi
    !else
    !    mu1 = energyT(nband-xeig:nband-xeig+1,:,:)
    !    mu0 = sum(mu1)/size(mu1)
    !end if
    mu0 = 0
    
    if (trim(method) == trim("fermi-dirac")) then
        sigma = FRC%smearing%sigma
        if (sigma /= 0.0) then
            !call fermiDirac(sigma,xt,enerT,focc)
            !call nfunsum(focc,kweight,nfun)
            call fzero_occupancy(sigma,kweight,energyT,ntot,mu0,mu)
            call fermiDirac(sigma,mu,enerT,focc2)
            allocate(entropy(size(nocc,1),size(nocc,2)))
            entropy = sigma*sum(ent)
            call bsxfunTimes(nocc2,kweight2,nocc2)
        else
            allocate(nocc(size(energyT,1),size(energyT,2),size(energyT,3)))
            forall(ii=1:int(ntot)/nspin)
                nocc(ii,:,:) = 1
            end forall
            allocate(energytmp(2,size(energyT,2),size(energyT,3)))
            mu = sum(energytmp)/real(size(energyT,2))/real(size(energyT,3))/2.0
            call bsxfunTimes(nocc2,kweight2,nocc2)
            entropy = 0
        end if
    else if (trim(method) == trim("tetrahedron")) then
        write(*,*) "Error in calcoccupancy.f90. FRC%kpoint%sampling = tetrahedron is not available now."
        stop
        !nvec = kpoint%gridn
        !allocate(avec(3,3))
        !avec = FRC%domain%latvec
        !call inversion(avec,bvec)
        !bvec = transpose(bvec)
        !bvec = 2.0*pi*bvec
        !call tetra_tri(nvec,bvec,ttri)
        !ttri = FRC%kpoint%ired(ttri)
        !ttri = abs(ttri)
        !call fzero(ttri,energy,ntot,-1E-8,mu0,mum)
        !call fzero(ttri,energy,ntot,1E-8,mu0,mup)
        !mu   = (mum+mup)/2.0
        !if (trim(method) == trim("tetrahedron")) then
        !    call tetra_weight(ttri,energy,mu,.FALSE.,nocc)
        !else
        !    call tetra_weight(ttri,energy,mu,.TRUE.,nocc)
        !end if
        !entropy = 0
    else if (trim(method) == trim("gauss")) then
        write(*,*) "Error in calcoccupancy.f90. FRC%kpoint%sampling = gauss is not available now."
        stop
        !sigma   = FRC%smearing%sigma
        !sorder  = FRC%smearing%order
        !call methpaxton(sorder,sigma,energy,x,1,focc)
        !nfun    = matmul(focc,kweight)
        !call fzero(focc,kweight)
        !call hermiteExp2(sorder,sigma,mu,energy,entropy)
        !entropy    = 0.5*sigma*entropy
        !entropyTmp = sum(sum(entropy,3),1)
        !entropym   = matmul(entropyTmp,kweight)
    else
        write(*,*) "Invalid value method to calculate occupancies."
        stop
    end if
    
    if (ispin == 1) then
        nocc = nocc*2.0
    end if
    
    if (mpistat) then
        write(*,*) "MPI is not available now."
        stop
    end if
    
    return
    end subroutine calcoccupancy
    end module calcoccupancy_module
        