! IonIonInt.f90
!*************************************
    !
!*************************************
    
    module IonIonInt_module
    contains
    subroutine IonIonInt(FRC)
    
    USE FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    real*8              :: Ewald
    real*8, allocatable :: Fwald(:)
    
    ! output variables
    
    ! body of this function
    allocate(Fwald(FRC%atom%numberOfAtom))
    
    if (any(FRC%domain%boundary)) then
        call ewaldsum(FRC,Ewald,Fwald)
    else
        call directsum(FRC,Ewald,Fwald)
    end if
    
    FRC%Energy%Ionion = Ewald
    FRC%force%Ionion  = Fwald
    
    return
    end subroutine IonIonInt
    end module IonIonInt_module