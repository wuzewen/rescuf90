! directSum.f90
!*************************************
    !
!*************************************
    
    module directSum_module
    contains
    subroutine directSum(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    xyz     = FRC%Atom%xyz
    natom   = FRC%atom%numberOfAtom
    nelem   = FRC%atom%numberOfElement
    allocate(Z(nelem))
    Z(:)    = 0
    do ii = 1,nelem,1
        Z(ii) = FRC%element(ii)%Z
    end do
    
    smimb = FRC%smi%mb
    sminb = FRC%smi%nb
    smimp = FRC%smi%mp
    sminp = FRC%smi%np
    call InitDistArray(natom,natom,smimb,sminb,smimp,sminp,dummy)
    call GetGloballInd(mpirank,dummy,ia,ja)
    
    call genvecMat(xyz(ia,:),xyz(ja,:),distvec,distsca)
    distsca = 1/distsca
    call bsxfunEQ(transpose(ia),ia,tmp)
    distsca(tmp) = 0
    nrg = matmul(transpose(Z(ia)),matmul(distsca,Z(ja)))
    if (mpistat) then
        call MPI_Allreduce_sum(nrg,nrg)
    end
    
    allocate(force(natom,3))
    call bsxfunTimes(Z(ia),distsca**3,distsca)
    call bsxfunTimes(distsca,transpose(Z(ja)),distsca)
    call bsxfunTimes(distvec,distsca,distvec)
    forcetmp = sum(distvec,2)
    call squeeze(forcetmp,forcetmp)
    force(ia,:) = reshape(forcetmp,(/size(ia),3/))
    
    if (mpistat) then
        call MPI_Allreduce_sum(force,force)
    end if 
    
    return
    end subroutine directSum
    end module directSum_module