! distmat_feval_minus_1D.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module distmat_feval_minus_1D_module
    contains
    subroutine distmat_feval_minus_1D(FRC,dA,dB,dC)
    
    use VataMN_1D_module
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ModBCDist_1D_module
    use Distmat_issame_1D_module
    
    implicit none
    
    ! intput vatiables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_1D)                 :: dA, dB
    
    ! temporary variables
    integer :: mpisize
    logical :: issame
    
    ! output variables
    type(VataMN_1D)                 :: dC
    
    ! body of this function
    mpisize = FRC%mpi%mpisize
    call distmat_issame_1D(dA,dB,issame)
    
    if (mpisize == 1 .and. issame) then
        dC = dB
    else
        call ModBCDist_1D(FRC,dB,dA%mblock,dA%nblock,dA%mproc,dA%mproc,.FALSE.,.FALSE.,dC)
    end if
    
    dC%vata = dA%vata-dC%vata
    
    return
    end subroutine distmat_feval_minus_1D
    end module distmat_feval_minus_1D_module