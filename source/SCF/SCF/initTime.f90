! initTime.f90
!************************************
    !
!************************************
    
    module initTime_module
    contains
    subroutine initTime()
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    iter  = SCFLOOP
    fname(1)  = 'CF'
    fname(2)  = 'ExactExchange'
    fname(3)  = 'diag'
    fname(4)  = 'dos'
    fname(5)  = 'energy'
    fname(6)  = 'force'
    fname(7)  = 'mixing'
    fname(8)  = 'poisson'
    fname(9)  = 'projectVeff'
    fname(10) = 'roc'
    fname(11) = 'rotatesubspace'
    fname(12) = 'total'
    fname(13) = 'updateRho'
    fname(14) = 'xcfunc'
    
    do ii = 1,14,1
        time%fname(ii)(SCFLOOP)
    end do
    
    return
    end subroutine initTime
    end module initTime_module