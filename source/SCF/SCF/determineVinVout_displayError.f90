! determineVinVout_displayError.f90
    
!*****************************************************************************
    !
    !
    !
!*****************************************************************************
    
    module determineVinVout_displayError_module
    contains
    subroutine determineVinVout_displayError(FRC,vin,vout,rin,rout)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    integer :: iter
    
    ! output variables
    type(VataMN_2D) :: rin, rout
    type(VataMN_1D) :: vin, vout
    
    ! body of this function
    iter = FRC%scloop
    if (trim(FRC%mixing%mixingType) == trim("density") .and. (trim(FRC%info%calculationType) == trim("self-consistent") .or. trim(FRC%info%calculationType) == trim("relaxation") .or. trim(FRC%info%calculationType) == trim("phonon"))) then
        rin  = FRC%rho%input(iter)
        rout = FRC%rho%output(iter)
        vin  = FRC%potential%veffin(iter)
        vout = FRC%potential%veffin(iter+1)
    else if (trim(FRC%mixing%mixingType) == trim("potential") .and. (trim(FRC%info%calculationType) == trim("self-consistent") .or. trim(FRC%info%calculationType) == trim("relaxation") .or. trim(FRC%info%calculationType) == trim("phonon"))) then
        rin  = FRC%rho%output(max(iter-1,1))
        rout = FRC%rho%output(iter)
        vin  = FRC%potential%veffin(iter)
        vout = FRC%potential%veffout(iter)
    else if (trim(FRC%mixing%mixingType) == trim("density-matrix") .and. (trim(FRC%info%calculationType) == trim("self-consistent") .or. trim(FRC%info%calculationType) == trim("relaxation") .or. trim(FRC%info%calculationType) == trim("phonon"))) then
        rin  = FRC%rho%output(max(iter-1,1))
        rout = FRC%rho%output(iter)
        vin  = FRC%potential%veffin(iter)
        vout = FRC%potential%veffin(iter+1)
    else if (trim(FRC%mixing%mixingType) == trim("density") .and. (trim(FRC%info%calculationType) == trim("dfpt"))) then
        rin  = FRC%rho%deltaIn(iter)
        rout = FRC%rho%deltaOut(iter)
        vin  = FRC%potential%deltaIn(iter)
        vout = FRC%potential%deltaOut(iter+1)
    else if (trim(FRC%mixing%mixingType) == trim("potential") .and. (trim(FRC%info%calculationType) == trim("dfpt"))) then
        rin  = FRC%rho%deltaOut(max(iter-1,1))
        rout = FRC%rho%deltaOut(iter)
        vin  = FRC%potential%deltaIn(iter)
        vout = FRC%potential%deltaOut(iter)
    end if
    
    return
    end subroutine determineVinVout_displayError
    end module determineVinVout_displayError_module