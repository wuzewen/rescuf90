!  SCF.f90 
!****************************************************************************
!
!  PROGRAM: SCF
!
!  PURPOSE: Main function of RESCU in FORTRAN version.
!       Including: SCF, DOS, force, total energy, mulliken populations.
!
!****************************************************************************

    module SCF_module
    contains
    subroutine SCF(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use GenHamiltonian_module
    use GetInitialSubspace_module
    use getHartreePotential_module
    use XCmain_module
    use VataMN_2D_module
    use KS_main_real_scf_module
    use mixer_module
    use updateoccupancy_module
    use UpdateRHO_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    logical                         :: shortrange, mpirank, isConverged
    integer                         :: iter
    type(VataMN_2D)                 :: rho
    real*8                          :: dV, dRho, dE
    
    ! output variables
    

    ! Body of SCF
    shortrange = FRC%option%shortRange
    mpirank    = FRC%mpi%rank
    !outfile    = FRC%info%outfile
    
!*********************************************  001 start
! generating Hamiltonian, neutral atom and prtial core density
    allocate(FRC%potential%VH(FRC%option%maxSCfIteration))
    allocate(FRC%potential%VHout(FRC%option%maxSCfIteration))
    allocate(FRC%potential%veffin(FRC%option%maxSCfIteration))
    allocate(FRC%energy%ksnrg(FRC%option%maxSCfIteration))
    allocate(FRC%energy%XCdens(FRC%option%maxSCfIteration))
    allocate(FRC%potential%VXCout(FRC%option%maxSCfIteration))
    call GenHamiltonian(FRC)
    
!*********************************************  001 end
    
    !if (shortRange) then
        !call calcEsrFourier(FRC)
    !else
    !    write(*,*) "Error in SCF.f90. ShortRange must be true now."
    !    stop
    !    if (FRC%energy%ewaldE) then
    !        call IonIonInt(FRC)
    !        call IonAlphaFactor(FRC)
    !    else
    !        FRC%energy%ionion = 0
    !        FRC%energy%alphaZ = 0
    !    end if
    !end if
    
!********************************************  002 start
! generating subspace
    
    !if (FRC%LCAO%status) then
    !    call GetAtomicOrbitalSubspace(FRC)
    !else
    call GetInitialSubspace(FRC)
    !end if
    
!********************************************  002 end
    
    !if (FRC%functional%hybrid) then
    !    call GenGmask(FRC)
    !    call GenDMxx0(FRC)
    !    call DetYcondition(FRC)
    !    
    !    if (FRC%Exx%UseSMI) then
    !        call calcYmatrixSMI(FRC)
    !    else
    !        call calcYmatrix(FRC)
    !    end if
    !end if
    
    !call initTime(FRC)
   ! 
    !FRC%scloop     = 0
    isConverged    = .FALSE.
   ! 
!****************************************************
!****************************************************
! scf loop start
    
    FRC%scloop = 0
    do while(FRC%scloop < FRC%option%maxSCfIteration)
        FRC%scloop = FRC%scloop+1
        iter       = FRC%scloop
        write(*,*) "Self-consistent loop", iter
    !    call initTime(FRC)
    !    
    !!**************************************** solve Kohn-Sham problem
    !    if (FRC%LCAO%status) then
    !        call ks_main_lcao_scf(FRC)
    !    else
    !        if (FRC%mpi%paraK) then
    !            call ks_main_real_scf_paraK(FRC)
    !        else
                call ks_main_real_scf(FRC)
    !        end if
    !    end if
    ! 
    !**************************************** update occupation numbers
        call UpdateOccupancy(FRC)
    !    
    !**************************************** update electronic density
    !    if (FRC%LCAO%status .and. trim(FRC%mixing%mixingType) == trim("density-matrix") then
    !        call calcDensityMatrix(FRC)
    !        call Mixer(FRC)
    !        call updateRHO_LCAO_DM(FRC)
    !    else if (FRC%LCAO%status) then
    !        call updateRHO_LCAO(FRC)
    !    else
            call updateRHO(FRC,FRC%energy%nocc,FRC%rho%output(iter))
    !    end if
        
    !    if (trim(FRC%mixing%mixingType) == trim("density")) then
            call Mixer(FRC)
    !    end
        
        if (trim(FRC%mixing%mixingType) == trim("density")) then
            rho = FRC%rho%input(iter+1)
        else
            rho = FRC%rho%input(iter)
        end if
    !    
    !**************************************** generate VH
    !    if (shortRange) then
    !        if (FRC%spin%ispin == 1) then
    !            call distmat_bsxfunMinus(FRC)
    !        else if (FRC%spin%ispin == 2) then
    !            call distmat_bsxfun2(FRC)
    !        else if (FRC%spin%ispin == 4) then
    !            call distmat_bsxfunMinus(FRC)
    !        end if
    !    end if
        
        !call GetHartreePotential(FRC,rho,FRC%domain%boundary,.TRUE.,FRC%potential%VHout(iter))  !PotentialVHout
    !    
    !******************************************* generate xc
        call xcMain(FRC)
        
    !******************************************* Veff in/out
    !    if (trim(FRC%mixing%mixingType) == trim("potential")) then
    !        call updateVeff(FRC)
    !    else
    !        call updateVeff(FRC)
    !    end if
    !    
    !******************************************* total energy
    !    if (shortRange) then
    !        call totalEnergy2(FRC)
    !    else
    !        call totalEnergy1(FRC)
    !    end if
        
    !    if (trim(FRC%mixing%mixingType) == trim("potential")) then
    !        call Mixer(FRC)
    !    end if
    !    
    !    call InterMediarySave(FRC)
    !    
    !    call displayError(FRC,dV,dRho,dE)
    !    
    !    call cleanStruct(FRC)
    !    
        if (iter>1 .and. dRHO<FRC%mixing%tol(2) .and. dE<FRC%mixing%tol(1)) then
            isConverged = .TRUE.
            exit
        end if
    end do
    
    !if (FRC%mpi%paraK .and. (FRC%option%saveWaveFunction .or. FRC%force%status .or. FRC%DOS%status .or. FRC%LCAO%mulliken)) then
    !    call distributeWaveFunction(FRC)
    !end if
    
!*********************************** force
    !if (FRC%force%ftatus) then
    !    call force_main(FRC)
    !end if
    
!*********************************** dos
    !if (FRC%DOS%status) then
    !    call dos_main(FRC)
    !end if
    
!*********************************** mulliken
    !if (FRC%LCAO%mulliken) then
    !    call mulliken_main(FRC)
    !end if
    
    !call saveMatRcal(FRC)

    return
    end subroutine SCF
    end module SCF_module

