! displayError.f90
    
!**********************************************************************
    !
    !
    !
!**********************************************************************
    
    module displayError_module
    contains
    subroutine displayError(FRC,dV,dRho,dE)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use determineVinVout_displayError_module
    use distmat_feval_plus_1D_module
    use distmat_feval_minus_1D_module
    use distmat_feval_minus_2D_module
    use VataMN_2D_module
    use VataMN_1D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical         :: mpistat
    integer         :: mpirank, iter, Nval
    real*8          :: avec(3,3), vol, dr
    type(VataMN_2D) :: rin, rout, Rhod
    type(VataMN_1D) :: vin, vout, meanV, diffV
    real*8, allocatable :: Vmean(:), dRhod(:), Vdiff(:)
    
    ! output variables
    real*8 :: dV, dRho, dE
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    iter    = FRC%scloop
    Nval    = FRC%eigensolver%Nvalence
    avec    = FRC%domain%latvec
    vol     = avec(1,1)*avec(2,2)*avec(3,3)  &
             +avec(1,2)*avec(2,3)*avec(3,1)  &
             +avec(1,3)*avec(2,1)*avec(3,2)  &
             -avec(1,1)*avec(2,3)*avec(3,2)  &
             -avec(1,2)*avec(2,1)*avec(3,3)  &
             -avec(1,3)*avec(2,2)*avec(3,1)
    vol     = abs(vol)
    dr      = vol/real(product(FRC%domain%cgridn))
    call determineVinVout_displayError(FRC,vin,vout,rin,rout)
    call distmat_feval_minus_1D(FRC,vin,vout,diffV)
    Vdiff = sum(abs(diffV%vata),1)
    call distmat_feval_plus_1D(FRC,vin,vout,meanV)
    Vmean = sum(abs(meanV%vata))/2.0
    call distmat_feval_minus_2D(FRC,rin,rout,Rhod)
    dRhod = sum(abs(Rhod%vata),1)
    
    if (mpistat) then
        write(*,*) "Error in displayError.f90. MPI is not available now."
        stop
    end if
    
    dV   = sum(Vdiff/Vmean)
    dRho = sum(dRhod)*dr/real(Nval)
    
    !if (trim(FRC%info%calculationType) == trim("self-consistent") .or. trim(FRC%info%calculationType) == trim("relaxation") .or. trim(FRC%info%calculationType) == trim("phonon")) then
    !    Etot = FRC%energy%Etot(iter)
    !    if (iter > 1) then
    !        dE = abs(FRC%energy%Etot(iter)-FRC%energy%Etot(iter-1))/Nval
    !    else
    !        dE = 1.0
    !    end if
    !    if (mpistat) then
    !        write(*,*) "Error in displayError.f90. MPI is not available now."
    !        stop
    !    end if
    !    
    !else
    !    Etot = FRC%energy%Etot(1)
    !    dE   = 0.0
    !end if
    
    return
    end subroutine displayError
    end module displayError_module