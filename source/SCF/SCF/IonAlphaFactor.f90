! IonAlphaFactor.f90
!*********************************
    !
!*********************************
    
    module IonAlphaFactor_module
    contains
    subroutine IonAlphaFactor(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)    :: FRC
    
    ! temporary variables
    type(dataElementType), allocatable :: DataElem(:)
    type(elementType)                  :: Element
    real*8                             :: avec(3,3), vol, alphaZ, pi, rmax, alphaZtmp
    real*8, allocatable                :: rrdata(:), rhodata(:), integrand(:,:)
    integer                            :: Z, numofatoms
    
    ! output variables
    
    ! body of this function
    mpirank = FRC%mpi%rank
    pi      = 3.14159265354
    if (any(FRC%domain%boundary)) then
        nspecies    = FRC%atom%numberOfElement
        allocate(DataElem(nspecies))
        DataElem(:) = FRC%dataElement(:)
        Element     = FRC%atom%element
        avec        = FRC%atom%latvec
        call det(avec,vol)
        vol = abs(vol)
        
        alphaZ = 0
        do ii = 1,nspecies
            numofatoms = sum(Element==ii)
            rrdata     = DataElem(ii)%Vlocal%rrData
            rhodata    = DataElem(ii)%Vlocal%vvData
            rmax       = maxval(rrData)
            Z          = FRC%element(ii)%Z
            call interpl(rrdata,rhodata,r1,"spline",0,integrand)
            integrand = (r1**2)*integrand+Z*r1
            call integral(integrand,0,rmax,alphaZtmp)
            alphaZ = alphaZ+alphaZtmp*4*pi*numofatoms
        end do
        FRC%Energy%alphaZ = 1/vol*alphaZ*FRC%eigensolver%Nvalence
    else
        FRC%Energy%alphaZ = 0
    end if
    
    return
    end subroutine IonAlphaFactor
    end module IonAlphaFactor_module
    