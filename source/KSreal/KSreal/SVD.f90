! SVD.f90
    
!**************************************************************
    !
    !
    !
!**************************************************************
    
    module SVD_module
    contains
    subroutine SVD(A,eps,U,V,s,L,E,work)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    real*8              :: eps
    
    ! temporary variables
    integer :: M, N, KA, IT, K, LL, kk, ii, jj, mm, m1, nn
    real*8  :: D, dd, ks, sm, sm1, em1, sk, ek, b, c, shh, f, g, cs, sn
    
    ! output variables
    real*8, allocatable :: U(:,:), V(:,:), s(:), E(:), work(:)
    integer             :: L
    
    ! body of this function
    M  = size(A,1)
    N  = size(A,2)
    KA = 1+max(M,N)
    
    IT = 60
    K  = N
    if (M-1 < N) then
        K = M-1
    end if
    L  = M
    if (N-2 < M) then
        L = N-2
    end if
    if (L < 0) then
        L = 0
    end if
    LL = K
    if (L > K) then
        LL = L
    end if
    
    if (LL > 1) then
        do kk = 1,LL,1
            if (kk <= k) then
                D = 0.0
                do ii = kk,M,1
                    D = D+A(ii,kk)*A(ii,kk)
                end do
                s(kk) = sqrt(D)
                if (s(kk) /= 0.0) then
                    if (A(kk,kk) /= 0.0) then
                        s(kk) = sign(s(kk),A(kk,kk))
                    end if
                    do ii = kk,M,1
                        A(ii,kk) = A(ii,kk)/s(kk)
                    end do
                    A(kk,kk) = 1.0+A(kk,kk)
                end if
                s(kk) = -s(kk)
            end if
            
            if (N >= kk+1) then
                do jj = kk+1,N,1
                    if ((kk <= k) .and. (s(kk) /= 0.0)) then
                        D = 0.0
                        do ii = kk,M,1
                            D = D+A(ii,kk)*A(ii,jj)
                        end do
                        D = -D/A(kk,kk)
                        do ii = kk,M,1
                            A(ii,jj) = A(ii,jj)+D*A(ii,kk)
                        end do
                    end if
                    E(jj) = A(kk,jj)
                end do
            end if
            
            if (kk <= K) then
                do ii = kk,M,1
                    U(ii,kk) = A(ii,kk)
                end do
            end if
            
            if (kk <= L) then
                D = 0.0
                do jj = kk+1,N,1
                    D = D+E(ii)*E(ii)
                end do
                E(kk) = sqrt(D)
                if (E(kk) /= 0.0) then
                    if (E(kk+1) /= 0.0) then
                        E(kk) = sign(E(kk),E(kk+1))
                    end if
                    do ii = kk+1,N,1
                        E(ii) = E(ii)/E(kk)
                    end do
                    E(kk+1) = 1.0+E(kk+1)
                end if
                E(kk) = -E(kk)
                if ((kk+1 <= M) .and. (E(kk) /= 0.0)) then
                    do ii = kk+1,M,1
                        work(ii) = 0.0
                    end do
                    do jj = kk+1,N,1
                        do ii = kk+1,M,1
                            work(ii) = work(ii)+E(jj)*A(ii,jj)
                        end do
                    end do
                    do jj = kk+1,N,1
                        do ii = kk+1,M,1
                            A(ii,jj) = A(ii,jj)-work(ii)*E(jj)/E(kk+1)
                        end do
                    end do
                end if
                
                do ii = kk+1,N,1
                    V(ii,kk) = E(ii)
                end do
            end if
        end do
    end if
    
    mm = N
    if (M+1 < N) then
        mm = M+1
    end if
    if (K < N) then
        S(K+1) = A(K+1,K+1)
    end if
    if (M < mm) then
        S(mm) = 0.0
    end if
    if (L+1 < mm) then
        E(L+1) = A(L+1,mm)
    end if
    E(mm) = 0.0
    
    nn = M
    if (M > N) then
        nn = N
    end if
    if (nn >= K+1) then
        do jj = K+1,nn,1
            do ii = 1,M,1
                U(ii,jj) = 0.0
            end do
            U(jj,jj) = 1.0
        end do
    end if
    
    if (K >= 1) then
        do ll = 1,K,1
            kk = k -ll+1
            if (S(kk) /= 0.0) then
                if (nn >= kk+1) then
                    do jj = kk+1,nn
                        D = 0.0
                        do ii = kk,M,1
                            D = D+U(ii,kk)*U(ii,jj)/U(kk,kk)
                        end do
                        D = -D
                        do ii = kk,M,1
                            U(ii,jj) = U(ii,jj)+D*U(ii,kk)
                        end do
                    end do
                end if
                do ii = kk,M,1
                    U(ii,kk) = -U(ii,kk)
                end do
                U(kk,kk) = 1+U(kk,kk)
                if (kk-1 >= 1) then
                    do ii = 1,kk-1,1
                        U(ii,kk) = 0.0
                    end do
                end if
            else
                do ii = 1,M,1
                    U(ii,kk) = 0.0
                end do
                U(kk,kk) = 1.0
            end if
        end do
    end if
    
    do ll = 1,N,1
        kk = N-ll+1
        if ((kk <= L) .and. (E(kk) /= 0.0)) then
            do jj = kk+1,N,1
                D = 0.0
                do ii = kk+1,N,1
                    D = D+V(ii,kk)*V(ii,jj)/V(kk+1,kk)
                end do
                D = -D
                do ii = kk+1,N,1
                    V(ii,jj) = V(ii,jj)+D*V(ii,kk)
                end do
            end do
        end if
        do ii = 1,N,1
            V(ii,kk) = 0.0
        end do
        V(kk,kk) = 1.0
    end do
    
    do ii = 1,M,1
        do jj = 1,M,1
            A(ii,jj) = 0.0
        end do
    end do
    
    m1 = mm
    IT = 60
2   if (mm == 0) then
        L = 0
        if (M >= N) then
            ii = N
        else
            ii = M
        end if
        
        do jj = 1,ii-1,1
            A(jj,jj)   = S(jj)
            A(jj,jj+1) = E(jj)
        end do
        A(ii,ii) = S(ii)
        
        if (M < N) then
            A(ii,ii+1) = E(ii)
        end if
        do ii = 1,N-1,1
            do jj = ii+1,N,1
                D        = V(ii,jj)
                V(ii,jj) = V(jj,ii)
                V(jj,ii) = D
            end do
        end do
        
        return
        
    end if
    
    if (IT == 0) then
        L = mm
        if (M >= N) then
            ii = N
        else
            ii = M
        end if
        
        do jj = 1,ii-1,1
            A(jj,jj)   = S(jj)
            A(jj,jj+1) = E(jj)
        end do
        
        A(ii,ii) = S(ii)
        if (M < N) then
            A(ii,ii+1) = E(ii)
        end if
        
        do ii = 1,N-1,1
            do jj = ii+1,N,1
                D        = V(ii,jj)
                V(ii,jj) = V(jj,ii)
                V(jj,ii) = 0
            end do
        end do
        
        return
    end if
    
    kk = kk-1
    if (kk /= 0) then
        D  = abs(S(kk))+abs(S(kk+1))
        dd = abs(E(kk))
    else
        D  = 0.0
        dd = 0.0
    end if
    
    do while (dd > eps*D)
        kk = kk-1
        if (kk == 0) then
            exit
        end if
        D  = abs(S(kk))+abs(S(kk+1))
        dd = abs(E(kk))
    end do
    kk = kk+1
    if (kk /= 0) then
        E(kk) = 0.0
    end if
    
    if (kk == mm-1) then
        kk = kk+1
        if (s(kk) < 0.0) then
            s(kk) = -s(kk)
            do ii = 1,N,1
                V(ii,kk) = -V(ii,kk)
            end do
        end if
1       if (kk /= m1) then
            if (s(kk) < s(kk+1)) then
                D = s(kk)
                s(kk) = s(kk+1)
                s(kk+1) = D
                if (kk < N) then
                    do ii = 1,N,1
                        D = V(ii,kk)
                        V(ii,kk) = V(ii,kk+1)
                        V(ii,kk+1) = D
                    end do
                end if
                if (kk < M) then
                    do ii = 1,M,1
                        D = U(ii,kk)
                        U(ii,kk) = U(ii,kk+1)
                        U(ii,kk+1) = D
                    end do
                end if
                kk = kk+1
                goto 1
            end if
        end if
        IT = 60
        mm = mm -1
        goto 2
    end if
    
    ks = mm+1
3   ks = ks-1
    if (ks > kk) then
        D = 0.0
        if (ks /= mm) then
            D = D+abs(E(ks))
        end if
        if (ks/=kk+1) then
            D = D+abs(E(ks-1))
        end if
        dd = abs(s(ks))
        if (dd > eps*d) then
            goto 3
        end if
        s(ks) = 0.0
    end if
    
    if (ks == kk) then
        kk = kk+1
        D  = abs(s(mm))
        if (abs(s(mm-1)) > D) then
            D = abs(s(mm-1))
        end if
        if (abs(E(mm-1)) > D) then
            D = abs(s(kk))
        end if 
        if (abs(s(kk)) > D) then
            D = abs(s(kk))
        end if
        if (abs(E(kk)) > D) then
            D = abs(E(kk))
        end if
        sm  = s(mm)/D
        sm1 = s(mm-1)/D
        em1 = E(mm-1)/D
        sk  = s(kk)/D
        ek  = E(kk)/D
        b   = ((sm1+sm)*(sm1-sm)+em1*em1)/2.0
        c   = sm*em1
        c   = c*c
        shh = 0.0
        
        if ((b /= 0.0) .or. (c /= 0.0)) then
            shh = sqrt(b*b+c)
            if (b < 0.0) then
                shh = -shh
            end if
            shh = c/(b+shh)
        end if
        f = (sk+sm)*(sk-sm)-shh
        g = sk*sk
        do ii = kk,mm-1,1
            call sss(f,g,cs,sn)
            if (ii /= kk) then
                E(ii-1) = f
            end if
            f = cs*s(ii)+sn*s(ii)
            E(ii) = cs*E(ii)-sn*s(ii)
            g = sn*s(ii+1)
            s(ii+1) = cs*s(ii+1)
            
            if ((cs /= 1.0) .or. (sn /= 0.0)) then
                do jj = 1,N,1
                    D = cs*V(jj,ii)+sn*V(jj,ii+1)
                    V(jj,ii+1) = -sn*V(jj,ii)+cs*V(jj,ii+1)
                    V(jj,ii)   = D
                end do
            end if
            
            call sss(f,g,cs,sn)
            s(ii) = f
            f     = cs*E(ii)+sn*S(ii+1)
            s(ii+1) = -sn*E(ii)+cs*s(ii+1)
            g = sn*E(ii+1)
            E(ii+1) = cs*E(ii+1)
            if (ii < m) then
                if ((cs /= 1.0) .or. (sn /= 0.0)) then
                    do jj = 1,M,1
                        D = cs*U(jj,ii)+sn*U(jj,ii+1)
                        U(jj,ii+1) = -sn*U(jj,ii)+cs*U(jj,ii+1)
                        U(jj,ii)   = D
                    end do
                end if
            end if
        end do
        E(mm-1) = f
        IT = IT-1
        goto 2
    end if
    
    if (ks == mm) then
        kk = kk+1
        f  = E(mm-1)
        E(mm-1) = 0.0
        do ll = kk,mm-1,1
            ii = mm+kk-ll-1
            g = s(ii)
            call sss(g,f,cs,sn)
            s(ii) = g
            if (ii /= kk) then
                f = -sn*E(ii-1)
                E(ii-1) = cs*E(ii-1)
            end if
            
            if ((cs /= 1.0) .or. (sn /= 0.0)) then
                do jj = 1,N,1
                    D = cs*V(jj,ii)+sn*V(jj,mm)
                    V(jj,mm) = -sn*V(jj,ii)+cs*V(jj,mm)
                    V(jj,ii) = D
                end do
            end if
        end do
        goto 2
    end if
    
    kk = ks+1
    f  = E(kk-1)
    E(kk-1) = 0.0
    do ii = kk,mm,1
        g = s(ii)
        call sss(g,f,cs,sn)
        s(ii) = g
        f = -sn*E(ii)
        E(ii) = cs*E(ii)
        
        if ((cs /= 1.0) .or. (sn /= 0.0)) then
            do jj = 1,M,1
                D = cs*U(jj,ii)+sn*U(jj,kk-1)
                U(jj,kk-1) = -sn*U(jj,ii)+cs*U(jj,kk-1)
                U(jj,ii) = D
            end do
        end if
    end do
    goto 2
    
    end subroutine SVD
    end module SVD_module