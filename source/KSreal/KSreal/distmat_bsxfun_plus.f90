! distmat_bsxfun_plus.f90
    
!****************************************************************
    !
    !
    !
!****************************************************************
    
    module distmat_bsxfun_plus_module
    contains
    subroutine distmat_bsxfun_plus(FRC,dA,dB,dC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    !use bsxfunPlus_module
    use VataMN_1D_module
    use distmat_issame_1D_module
    use ModBCDist_1D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_1D) :: dA, dB
    
    ! temporary variables
    integer :: mpisize, tmp1, tmp2
    logical :: issame
    
    ! output variables
    type(VataMN_1D) :: dC
    
    ! body of this function
    mpisize = FRC%mpi%mpisize
    tmp1    = dB%m*dB%n
    tmp2    = dA%m*dA%n
    call distmat_issame_1D(dA,dB,issame)
    if (mpisize == 1 .or. issame) then
        dC      = dB
        dC%vata = dA%vata + dB%vata
    else if (tmp1<tmp2) then
        call ModBCDist_1D(FRC,dB,dA%mblock,dA%nblock,dA%mproc,dA%nproc,.FALSE.,.FALSE.,dC)
        dC%vata = dA%vata + dB%vata
    else
        call ModBCDist_1D(FRC,dA,dB%mblock,dB%nblock,dB%mproc,dB%nproc,.FALSE.,.FALSE.,dC)
        dC%vata = dA%vata + dB%vata
    end if
    dC%m = max(dA%m,dB%m)
    dC%n = max(dA%n,dB%n)
    
    return
    end subroutine distmat_bsxfun_plus
    end module distmat_bsxfun_plus_module
    