! spdiags_2in.f90
    
!*************************************************************
    !
    !
    !
!*************************************************************
    
    module spdiags_2in_module
    contains
    subroutine spdiags_2in(A,d,B)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), d(:)
    
    ! temporary variables
    integer :: Nx, Ny, Nn, Nd, ii, jj
    
    ! output variables
    real*8, allocatable :: B(:,:)
    
    ! body of this function
    Nx = size(A,1)
    Ny = size(A,2)
    Nn = min(Nx,Ny)
    Nd = size(d)
    
    allocate(B(Nn,Nd))
    
    do ii = 1,Nd,1
        if (ii < 0) then
            forall(jj=1:Nn+ii)
                B(ii,jj) = A(Nn+ii+jj-1,jj)
            end forall
        else if (ii >= 0) then
            forall(jj=1:Nn-ii)
                B(ii,jj) = A(jj,Nn-ii-jj-1)
            end forall
        end if
    end do
    
    return
    end subroutine spdiags_2in
    end module spdiags_2in_module
            