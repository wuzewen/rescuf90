!  KS_main_real_scf.f90 
!****************************************************************************
!
!  PROGRAM: KSreal
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module KS_main_real_scf_module
    contains
    subroutine KS_main_real_scf(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use lobpcgMain_module
    
    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    integer :: iter, cn, nkpt, nband, nspin, maxRestart
    real*8  :: vol, dr, toltmp(2), latvec(3,3), tol
    real*8, allocatable :: kdir(:,:)
    
    
    ! output variables
    

    ! Body of KSreal
    iter   = FRC%scloop
    cn     = product(FRC%domain%cgridn)
    latvec = FRC%domain%latvec
    vol    = latvec(1,1)*latvec(2,2)*latvec(3,3)  &
            +latvec(1,2)*latvec(2,3)*latvec(3,1)  &
            +latvec(1,3)*latvec(2,1)*latvec(3,2)  &
            -latvec(1,1)*latvec(2,3)*latvec(3,2)  &
            -latvec(1,2)*latvec(2,1)*latvec(3,3)  &
            -latvec(1,3)*latvec(2,2)*latvec(3,1)
    dr         = vol/cn
    allocate(kdir(size(FRC%kpoint%ikdirect,1),3))
    kdir       = FRC%kpoint%ikdirect
    nkpt       = size(kdir,1)
    nband      = FRC%eigensolver%nband
    nspin      = FRC%spin%nspin
    toltmp     = FRC%eigensolver%tol
    tol        = minval(toltmp)
    maxRestart = FRC%eigensolver%maxRestart
    
    if (trim(FRC%eigensolver%algo) == trim("CFSI")) then
        write(*,*) "Error in KS_main_real_scf.f90. CFSI is not available now."
        stop
        !call calcCFBound(FRC,LB,UB)
        !restartIT = 1;
        !do while(restartIT < maxRestart)
        !    call CFSI(FRC,LB,UB)
        !    if (maxRestart>1 .and. restartIT<maxRestart) then
        !        tol = min(FRC%mixing%dRHO/dr/nband/10,tol)
        !        call compute_cfsi_residuals(mrc,tol,.false.,failureFlag)
        !    else
        !        failureFlag = .FALSE.
        !    end if
        !    if (.not. failureFlag) then
        !        exit
        !    else
        !        restartIT = restartIT+1
        !    end if
        !end do
    else if (trim(FRC%eigensolver%algo) == trim("lobpcg")) then
        call lobpcgMain(FRC)
    else
        write(*,*) "Error in KS_main_real_scf.f90. Invalid method for eigensolver algo."
    end if
    
    return
    end subroutine KS_main_real_scf
    end module KS_main_real_scf_module

