! diag_1D.f90
    
!****************************************************************
    !
    !
    !
!****************************************************************
    
    module diag_1D_module
    contains
    subroutine diag_1D(A,B)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:)
    
    ! temporary variables
    integer :: n, ii
    
    ! output variables
    real*8, allocatable :: B(:,:)
    
    ! body of this function
    n = size(A)
    allocate(B(n,n))
    B = 0
    forall(ii=1:n)
        B(ii,ii) = A(ii)
    end forall
    
    return
    end subroutine diag_1D
    end module diag_1D_module
    