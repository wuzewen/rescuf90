! permute.f90
    
!****************************************************************
    !
    ! This function has the same function as function permute
    ! in MATLAB.
    !
!****************************************************************
    
    module permute5_cmplx_module
    contains
    subroutine permute5_cmplx(A,b,C)
    
    ! input variables
    complex, allocatable :: A(:,:,:,:,:)
    integer              :: b(5)
    
    ! temporary variables
    integer :: n(5), ny, nz, sA(5), sC(5), ss(5), sx(5), ii, jj, kk, ll, mm
    
    ! output variables
    complex, allocatable :: C(:,:,:,:,:)
    
    ! body of this function
    n(1) = b(1)
    n(2) = b(2)
    n(3) = b(3)
    n(4) = b(4)
    n(5) = b(5)
    
    sA(1) = size(A,1)
    sA(2) = size(A,2)
    sA(3) = size(A,3)
    sA(4) = size(A,4)
    sA(5) = size(A,5)
    
    sC(1) = sA(n(1))
    sC(2) = sA(n(2))
    sC(3) = sA(n(3))
    sC(4) = sA(n(4))
    sC(5) = sA(n(5))
    
    allocate(C(sC(1),sC(2),sC(3),sC(4),sC(5)))
    !forall(ii=1:sC(1),jj=1:sC(2),kk=1:sC(3))
    !    C(ii,jj,kk) = A()
    do ii = 1,sA(1),1
        do jj = 1,sA(2),1
            do kk = 1,sA(3),1
                do ll = 1,sA(4),1
                    do mm = 1,sA(5),1
                        ss(1) = ii
                        ss(2) = jj
                        ss(3) = kk
                        ss(4) = ll
                        ss(5) = mm
                        sx(1) = ss(n(1))
                        sx(2) = ss(n(2))
                        sx(3) = ss(n(3))
                        sx(4) = ss(n(4))
                        sx(5) = ss(n(5))
                        C(sx(1),sx(2),sx(3),sx(4),sx(5)) = A(ii,jj,kk,ll,mm)
                    end do
                end do
            end do
        end do
    end do
    
    return
    end subroutine permute5_cmplx
    end module permute5_cmplx_module