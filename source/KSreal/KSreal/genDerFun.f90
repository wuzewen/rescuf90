! genDerFun.f90
    
!******************************************************************
    !
    !
    !
!******************************************************************
    
    module genDerFun_module
    contains
    subroutine genDerFun(flag,FRC,avec,ng,kpt,gpu,X,derFun,tauKin)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_2D_module
    use inversion_module
    use fftFreq_module
    use DerFftGrad_module
    use fdGradMat_module
    use ftGradMat_module
    use rkronprod_module
    use lkronprod_module
    use rlkronprod_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable :: avec(:,:), X(:,:)
    logical             :: gpu
    integer             :: ng(3), flag
    real*8              :: kpt(3)
    
    ! temporary variables
    real*8  :: pi
    real*8, allocatable :: J1(:,:), bvec(:,:), ku(:,:,:), kv(:,:,:), kw(:,:,:), Du(:,:), Dv(:,:), Dw(:,:)
    real*8, allocatable :: Dux(:,:), Dvx(:,:), Dwx(:,:)
    integer :: acc, bc(3)
    complex :: imnu
    complex, allocatable :: Xcmplx(:,:), Kcmplx(:,:,:), Vcmplx(:,:)
    
    ! output variables
    type(VataMN_2D) :: derFun(3)
    real*8, allocatable :: tauKin(:,:)
    
    ! body of this function
    imnu = (0,1)
    call inversion(avec,J1)
    pi   = 3.14159265354
    J1   = transpose(J1)
    bvec = 2*pi*J1
    kpt  = matmul(kpt,bvec)
    
    if (trim(FRC%diffop%method) == trim("fft")) then
        call fftfreq(ng,bvec,ku,kv,kw)
        if (gpu) then
            write(*,*) "Error in GenGradFun.f90. GPU is not available now."
            stop
        end if
        Xcmplx = cmplx(X)
        Vcmplx = cmplx(derFun(1)%vata)
        !Kcmplx = cmplx(ku)
        call DerfftGrad(Xcmplx,ng,ku,kpt(1),Vcmplx)
        Vcmplx = cmplx(derFun(2)%vata)
        !Kcmplx = cmplx(kv)
        call DerfftGrad(Xcmplx,ng,kv,kpt(2),Vcmplx)
        Vcmplx = cmplx(derFun(3)%vata)
        !Kcmplx = cmplx(kw)
        call DerfftGrad(Xcmplx,ng,kw,kpt(3),Vcmplx)
    else
        if (all(ng == FRC%domain%cgridn)) then
            Du  = FRC%diffop%Du
            Dv  = FRC%diffop%Dv
            Dw  = FRC%diffop%Dw
        else if (all(ng == FRC%domain%fgridn)) then
            Du  = FRC%diffop%fDu
            Dv  = FRC%diffop%fDv
            Dw  = FRC%diffop%fDw
        else if (trim(FRC%diffop%method) == trim("fd")) then
            acc = FRC%diffop%accuracy
            bc  = FRC%domain%boundary
            call FdGradMat(ng,acc,bc,Du,Dv,Dw)
        else if (trim(FRC%diffop%method) == trim("ft")) then
            call FtGradMat(ng,Du,Dv,Dw)
        end if
        
        if (gpu) then
            write(*,*) "Error in GenGradFun.f90. GPU is not available now."
            stop
        end if
        
        call rkronProd(Du,x,Dux)
        call rlkronProd(Dv,x,ng(1),Dvx)
        call lkronProd(Dw,x,Dwx)
        derFun(1)%vata = Dux*J1(1,1)+Dvx*J1(2,1)+Dwx*J1(3,1)+imnu*kpt(1)*x
        derFun(2)%vata = Dux*J1(1,2)+Dvx*J1(2,2)+Dwx*J1(3,2)+imnu*kpt(2)*x
        derFun(3)%vata = Dux*J1(1,3)+Dvx*J1(2,3)+Dwx*J1(3,3)+imnu*kpt(3)*x
        
    end if
    
    return
    end subroutine genDerFun
    end module genDerFun_module
    