! matGrad.f90
    
!*************************************************
    !
    !
    !
!*************************************************
    
    module matGrad_module
    contains
    subroutine matGrad(x,Dcell,J1,kpt,y)
    
    use rkronProd_module
    use lkronProd_module
    use rlkronProd_module
    use Cell_2D_real
    use permute_module
    
    implicit none
    
    ! input variables
    complex              :: imnu
    real*8, allocatable  :: X(:,:)
    type(cell_2D_rea)    :: Dcell(3)
    real*8               :: kpt(3), j1(3,3)
    
    ! temporary variables
    integer              :: n(3), nn, n2
    real*8, allocatable  :: Dux(:,:), Dvx(:,:), Dwx(:,:), xtmp(:,:,:)
    
    ! output variables
    complex, allocatable :: Y(:,:)
    
    ! body of this function
    imnu = (0,1)
    n(1) = size(Dcell(1)%vata,1)
    n(2) = size(Dcell(2)%vata,1)
    n(3) = size(Dcell(3)%vata,1)
    nn   = product(n)
    
    call rkronProd(Dcell(1)%vata,X,Dux)
    call rlkronProd(Dcell(2)%vata,X,n(1),dvx)
    call lkronProd(Dcell(3)%vata,X,Dwx)
    
    xtmp(:,:,1) = Dux*J1(1,1)+Dvx*J1(2,1)+Dwx*J1(3,1)+imnu*kpt(1)*x
    xtmp(:,:,2) = Dux*J1(1,2)+Dvx*J1(2,2)+Dwx*J1(3,2)+imnu*kpt(2)*x
    xtmp(:,:,3) = Dux*J1(1,3)+Dvx*J1(2,3)+Dwx*J1(3,3)+imnu*kpt(3)*x
    
    call permute(xtmp,(/1,3,2/),xtmp)
    y = reshape(xtmp,(/nn,n2/))
    
    return
    end subroutine matGrad
    end module matGrad_module