! cond.f90
    
!*****************************************************
    !
    !
    !
!*****************************************************
    
    module cond_module
    contains
    subroutine cond(A,p,val)
    
    use SVD_module
    use norm_2D_module
    use inversion_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    integer             :: p
    
    ! temporary variables
    integer :: Nx, Ny, L
    real*8, allocatable :: s(:), Atmp(:,:), E(:), work(:), U(:,:), V(:,:)
    real*8  :: c, eps
    
    ! output variables
    real*8 :: val
    
    ! body of this function
    Nx = size(A,1)
    Ny = size(A,2)
    
    if (p /= 2 .and. Nx /= Ny) then
        write(*,*) "Error in cond.f90. The input matrix is not a square Matrix or norm is not 2."
    end if
    
    if (p == 2) then
        call svd(A,eps,U,V,s,L,E,work)
        if (any(s == 0)) then
            c = 1.0/0.0
        else
            c = maxval(s)/minval(s)
        end if
    else
        call norm_2D(A,p,c)
        call inversion(A,Atmp)
        call norm_2D(Atmp,p,val)
        val = c*val
    end if
    
    return
    end subroutine cond
    end module cond_module