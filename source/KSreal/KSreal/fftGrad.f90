! fftGrad.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module fftGrad_module
    contains
    subroutine fftGrad(X,n,k,kpt,Y)
    
    use Cell_3D_real
    use fft4_module
    use permute5_cmplx_module
    use bsxfunTimes_cmplx_module
    
    implicit none
    
    ! input variables
    complex, allocatable :: X(:,:)
    integer              :: n(3)
    type(Cell_3D_rea)    :: k(3)
    real*8               :: kpt(3)
    
    ! temporary variables
    complex :: imnu
    integer :: nx, n2
    complex, allocatable :: xtmp(:,:,:,:), xtmp1(:,:,:,:), outmp(:,:,:,:), yt(:,:,:,:,:), intmp(:,:,:)
    
    ! output variables
    complex, allocatable :: Y(:,:)
    
    ! body of this function
    imnu  = (0,1)
    nx    = size(x,2)
    xtmp  = reshape(x,(/n(1),n(2),n(3),nx/))
    call fft4(xtmp,1,xtmp1)
    call fft4(xtmp1,2,xtmp)
    call fft4(xtmp,3,xtmp1)
    intmp = imnu*(k(1)%vata+kpt(1))
    call bsxfunTimes_CMPLX(x,intmp,outmp)
    yt(:,:,:,:,1) = outmp
    intmp = imnu*(k(2)%vata+kpt(2))
    call bsxfunTimes_CMPLX(x,intmp,outmp)
    yt(:,:,:,:,2) = outmp
    intmp = imnu*(k(3)%vata+kpt(3))
    call bsxfunTimes_CMPLX(x,intmp,outmp)
    yt(:,:,:,:,3) = outmp
    call permute5_cmplx(yt,(/1,2,3,5,4/),yt)
    nx = product(n)
    Y = reshape(yt,(/nx,n2/))
    
    return
    end subroutine fftGrad
    end module fftGrad_module