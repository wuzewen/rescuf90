! GenGradFun.f90
    
!***********************************************************
    !
    !
    !
!***********************************************************
    
    module GenGradFun_module
    contains
    subroutine GenGradFun(FRC,kpt,gpu,X,cGfun,fGfun)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use Cell_3D_real
    use Cell_2D_real
    use inversion_module
    use fftFreq_module
    use fftGrad_module
    use MatGrad_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8                          :: kpt(3)
    real*8, allocatable             :: X(:,:)
    logical                         :: gpu
    
    ! temporary variables
    real*8               :: pi
    real*8, allocatable  :: avec(:,:), J1(:,:), bvec(:,:), Du(:,:,:), Dv(:,:,:), Dw(:,:,:)
    real*8, allocatable  :: fDu(:,:), fDv(:,:), fDw(:,:)
    integer              :: n(3)
    type(Cell_3D_rea)    :: uvw(3)
    type(Cell_2D_rea)    :: uvw2(3)
    complex, allocatable :: Xcmplx(:,:)
    
    ! output variables
    complex, allocatable :: cGfun(:,:), fGfun(:,:)
    
    ! body of this function
    pi = 3.14159265354
    allocate(avec(3,3))
    avec = FRC%domain%latvec
    call inversion(avec,J1)
    J1   = transpose(J1)
    bvec = 2*pi*J1
    kpt  = matmul(kpt,bvec)
    
    if (trim(FRC%diffop%method) == trim("fft")) then
        n = FRC%domain%cgridn
        call fftfreq(n,bvec,uvw(1)%vata,uvw(2)%vata,uvw(3)%vata)
        if (gpu) then
            write(*,*) "Error in GenGradFun.f90. GPU is not available now."
            stop
        end if
        Xcmplx = cmplx(X)
        call fftGrad(Xcmplx,n,uvw,kpt,cGfun)
        n = FRC%domain%fgridn
        call fftfreq(n,bvec,uvw(1)%vata,uvw(2)%vata,uvw(3)%vata)
        if (gpu) then
            write(*,*) "Error in GenGradFun.f90. GPU is not available now."
            stop
        end if
        call fftGrad(Xcmplx,n,uvw,kpt,fGfun)
    else
        uvw(1)%vata = Du
        uvw(2)%vata = Dv
        uvw(3)%vata = Dw
        if (gpu) then
            write(*,*) "Error in GenGradFun.f90. GPU is not available now."
            stop
        end if
        call matGrad(X,uvw2,J1,kpt,cGfun)
        uvw2(1)%vata = fDu
        uvw2(2)%vata = fDv
        uvw2(3)%vata = fDw
        if (gpu) then
            write(*,*) "Error in GenGradFun.f90. GPU is not available now."
            stop
        end if
        call matGrad(X,uvw2,J1,kpt,fGfun)
    end if
    
    return
    end subroutine GenGradFun
    end module GenGradFun_module