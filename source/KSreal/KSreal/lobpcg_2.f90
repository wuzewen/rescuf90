! lobpcg_2.f90
    
!*******************************************************
    !
    !
    !
!*******************************************************
    
    module lobpcg_2_module
    contains
    subroutine lobpcg_2(FRC,blkX,tol,maxit,mpistat,sizeY,nrg,lambda,failureflag,out2,out3)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use gramProcess_module
    use distArrayMtimes_module
    use inversion_module
    use eig_module
    use diag_module
    use cond_module
    use spdiags_4in_module
    use operTimesDistArray_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable :: blkX(:,:)
    real*8              :: tol
    integer             :: maxit, sizeY
    logical             :: mpistat
    
    ! temporary variables
    integer              :: n_loc, blkSize, verbosityLevel, n, mpirank, cholFlag, iterNum, ii, ntmp, activeRSize
    integer              :: activePSize, restart, currentBlockSize, cond_try, nn, JT, cn
    logical              :: explicitGramFlag
    real*8               :: eps, ntmpR, condestG, condestGmean
    logical, allocatable :: activeMask(:,:)
    real*8,  allocatable :: resNormsHistory(:,:), lambdaHistory(:,:), condestGhistory(:,:), blkAX(:,:), blkR(:,:)
    real*8,  allocatable :: blkAR(:,:), blkP(:,:), blkAP(:,:), nonsense(:,:), gramXAX(:,:), coordX(:,:), gram(:,:)
    real*8,  allocatable :: opA(:,:), resNorms(:), blkRtmp(:,:), eyetmp(:,:), gramPBP(:,:), condestGram(:,:)
    real*8,  allocatable :: tmp(:,:), PBPtmp(:,:), gramXAR(:,:), blkPtmp(:,:), blkAPtmp(:,:)
    real*8,  allocatable :: blkARtmp(:,:), gramRAR(:,:), gramXBX(:,:), gramRBR(:,:), gramXBR(:,:), gramXAP(:,:)
    real*8,  allocatable :: gramRAP(:,:), gramPAP(:,:), gramA(:,:), gramB(:,:), lambdatmp(:,:), gramXBP(:,:)
    real*8,  allocatable :: gramRBP(:,:), gramA1(:,:), gramB1(:,:), gramB2(:,:), gramXBXtmp(:,:)
    real*8,  allocatable :: noneig(:), X(:,:)
    integer, allocatable :: tmp0(:)
    character(len=20)    :: ham, opT, pre
    
    ! output variables
    real*8, allocatable :: nrg(:,:), lambda(:,:), out2(:,:), out3(:,:)
    logical             :: failureFlag, out1
    
    ! body of this function
    n_loc          = size(blkX,1)
    blksize        = size(blkX,2)
    failureFlag    = .TRUE.
    verbosityLevel = 0
    if (mpistat) then
        write(*,*) "Error in lobpcg_2.f90. MPI is not available now."
        stop
        !call MPI_Allreduce_sum(n_loc,n)
        !mpirank = MPI_Comm_rank
    else
        n       = n_loc
        mpirank = 0
    end if
    
    if (n-sizeY < 5*blkSize) then
        write(*,*) "Error in lobpcg_2.f90. The problem size is too small, Try using eig() or eigs() instead."
        stop
    end if
    
    call gramProcess(blkX,mpistat,nonsense,cholFlag)
    
    if (cholFlag /= 0) then
        write(*,*) "Error in lobpcg_2.f90. The initial approximation after constraints is not full rank."
        stop
    end if
    
    allocate(resNormsHistory(blksize,maxit))
    allocate(lambdaHistory(blkSize,maxit+1))
    allocate(condestGhistory(1,maxit+1))
    allocate(activeMask(blkSize,1))
    allocate(blkAX(n_loc,blkSize))
    allocate(blkR(n_loc,blkSize))
    allocate(blkAR(n_loc,blkSize))
    allocate(blkP(n_loc,blkSize))
    allocate(blkAP(n_loc,blkSize))
    
    resNormsHistory = 0
    lambdaHistory   = 0
    condestGhistory = 0
    activeMask      = .TRUE.
    blkAX           = 0
    blkR            = 0
    blkAR           = 0
    blkP            = 0
    blkAP           = 0
    
    call operTimesDistArray(FRC,ham,X,cn,cn,blkAX)
    !call feval(opA,blkX,blkAX)
    call distArrayMtimes(blkX,blkAX,mpistat,gramXAX)
    if (mpirank == 0) then
        gramXAX = (gramXAX+transpose(gramXAX))*0.5
        !call eig(gramXAX,eps,coordX,noneig,gram,JT)
        call diag(gram,lambda)
    else
        allocate(coordX(0,0),lambda(0,0))
    end if
    
    if (mpistat) then
        write(*,*) "Error in lobpcg_2.f90. MPI is not available now."
        stop
        !call MPI_Bcast_variable(coordX,0)
        !call MPI_Bcast_variable(lambda,0)
    end if
    
    blkX  = matmul(blkX,coordX)
    blkAX = matmul(blkAX,coordX)
    
    condestGhistory(1,1)         = -log10(eps)/2
    lambdaHistory(1:blksize,1)   = lambda(:,1)
    
    do iterNum = 1,maxit,1
        if (blkSize > 1) then
            allocate(tmp0(1))
            tmp0 = 0
            call spdiags_4in(lambda,tmp0,blksize,blksize,blkRtmp)
            blkR = blkAX-matmul(blkX,blkRtmp)
        else
            blkR = blkAX-matmul(blkX,lambda)
        end if
        resNorms = sum(blkR*blkR,1)
        if (mpistat) then
            write(*,*) "Error in lobpcg_2.f90. MPI is not available now."
            stop
            !call MPI_Allreduce_sum(resNorms)
        end if
        resNorms                           = resNorms**0.5
        resNormsHistory(1:blkSize,iterNum) = resNorms
        
        do ii = 1,blkSize,1
            if (resNorms(ii)>tol .and. activeMask(ii,1)) then
                activeMask(ii,1) = .TRUE.
            end if
        end do
        
        if (all(activeMask == .FALSE.)) then
            failureFlag = .FALSE.
            exit
        end if
        
        if (trim(opT) /= trim("none")) then
            !call operTimesDistArray(FRC,pre,blkR(:,activeMask),cn,cn,blkR(:,activeMask))
        end if
        
        !if (trim(opY) /= trim("none")) then
        !    call ???
        !end if
        
        !***********************************************
        !***********************************************
        ntmp = 0
        do ii = 1,blkSize,1
            if (activeMask(ii,1)) then
                ntmp = ntmp+1
                blkRtmp(:,ntmp) = blkR(:,ii)
            end if
        end do
        call distArrayMtimes(blkX,blkRtmp,mpistat,tmp)
        blkRtmp = blkRtmp-matmul(blkX,tmp)
        call gramProcess(blkRtmp,mpistat,nonsense,cholFlag)
        if (cholFlag) then
            write(*,*) "Error in lobpcg_2.f90. The residual is not full rank."
            exit
        end if
        
        !call feval
        !call operTimesDistArray(FRC,ham,blkR(:,activeMask),cn,cn,blkAR(:,activeMask))
        
        if (iterNum > 1) then
            ntmp = 0
            do ii = 1,blkSize,1
                if (activeMask(ii,1)) then
                    ntmp = ntmp+1
                    blkPtmp(:,ntmp) = blkP(:,ii)
                end if
            end do
            call gramProcess(blkptmp,mpistat,nonsense,cholFlag)
            if (cholFlag == 0) then
                allocate(eyetmp(size(gramPBP),size(gramPBP)))
                eyetmp = 1
                call inversion(gramPBP,PBPtmp)
                ntmp = 0
                do ii = 1,blkSize,1
                    if (activeMask(ii,1)) then
                        ntmp = ntmp+1
                        blkAPtmp(:,ntmp) = blkAP(:,ii)
                    end if
                end do
                blkAPtmp = matmul(matmul(blkAPtmp,eyetmp),PBPtmp)
            else
                write(*,*) "Error in lobpcg_2.f90. The direction matrix is not full rank."
                exit
            end if
        end if
        
        ntmpR        = log(real(currentBlockSize))
        ntmp         = iterNum - 10 - int(ntmpR)
        ntmp         = max(1,ntmp)
        condestGmean = sum(condestGhistory(ntmp:iterNum,1))/(iterNum-ntmp+1)
        
        ntmpR = eps**0.6
        if (all(resNorms > ntmpR)) then
            explicitGramFlag = .FALSE.
        else
            explicitGramFlag = .TRUE.
        end if
        
        activeRSize = size(blkRtmp,2)
        
        if (iterNum == 1) then
            activePSize = 0
            restart     = 1
        else
            activePSize = size(blkptmp,2)
            restart     = 0
        end if
        
        call distArrayMtimes(blkAX,blkRtmp,mpistat,gramXAR)
        ntmp = 0
        do ii = 1,blkSize,1
            if (activeMask(ii,1)) then
                ntmp = ntmp+1
                blkARtmp(:,ntmp) = blkAR(:,ii)
            end if
        end do
        call distArrayMtimes(blkARtmp,blkRtmp,mpistat,gramRAR)
        gramRAR = 0.5*(transpose(gramRAR)+gramRAR)
        
        if (explicitGramFlag) then
            call distArrayMtimes(blkAX,blkX,mpistat,gramXAX)
            gramXAX = 0.5*(gramXAX+transpose(gramXAX))
            call distArrayMtimes(blkX,blkX,mpistat,gramXBX)
            call distArrayMtimes(blkRtmp,blkRtmp,mpistat,gramRBR)
            call distArrayMtimes(blkX,blkRtmp,mpistat,gramXBR)
            gramXBX = 0.5*(gramXBX+transpose(gramXBX))
            gramRAR = 0.5*(gramRBR+transpose(gramRBR))
        end if
        
        do cond_try = 1,2,1
            if (restart == 0) then
                call distArrayMtimes(blkAX,blkptmp,mpistat,gramXAP)
                call distArrayMtimes(blkARtmp,blkptmp,mpistat,gramRAP)
                call distArrayMtimes(blkAPtmp,blkRtmp,mpistat,gramPAP)
                gramPAP = 0.5*(gramPAP+transpose(gramPAP))
                
                if (explicitGramFlag) then
                    gramA(1:n,1:n)             = gramXAX
                    gramA(1:n,n+1:2*n)         = gramXAR
                    gramA(1:n,2*n+1:3*n)       = gramXAP
                    gramA(n+1:2*n,1:n)         = transpose(gramXAR)
                    gramA(n+1:2*n,n+1:2*n)     = gramRAR
                    gramA(n+1:2*n,2*n+1:3*n)   = gramRAP
                    gramA(2*n+1:3*n,1:n)       = transpose(gramXAP)
                    gramA(2*n+1:3*n,n+1:2*n)   = transpose(gramRAP)
                    gramA(2*n+1:3*n,2*n+1:3*n) = gramPAP
                else
                    call diag(lambda,lambdatmp)
                    gramA(1:n,1:n)             = lambdatmp
                    gramA(1:n,n+1:2*n)         = gramXAR
                    gramA(1:n,2*n+1:3*n)       = gramXAP
                    gramA(n+1:2*n,1:n)         = transpose(gramXAR)
                    gramA(n+1:2*n,n+1:2*n)     = gramRAR
                    gramA(n+1:2*n,2*n+1:3*n)   = gramRAP
                    gramA(2*n+1:3*n,1:n)       = transpose(gramXAP)
                    gramA(2*n+1:3*n,n+1:2*n)   = transpose(gramRAP)
                    gramA(2*n+1:3*n,2*n+1:3*n) = gramPAP
                end if
                
                call distArrayMtimes(blkX,blkptmp,mpistat,gramXBP)
                call distArrayMtimes(blkRtmp,blkptmp,mpistat,gramRBP)
                
                if (explicitGramFlag) then
                    call distArrayMtimes(blkptmp,blkptmp,mpistat,gramPBP)
                    gramPBP = 0.5*(gramPBP+transpose(gramPBP))
                    gramB(1:n,1:n)             = gramXBX
                    gramB(1:n,n+1:2*n)         = gramXBR
                    gramB(1:n,2*n+1:3*n)       = gramXBP
                    gramB(n+1:2*n,1:n)         = transpose(gramXBR)
                    gramB(n+1:2*n,n+1:2*n)     = gramRBR
                    gramB(n+1:2*n,2*n+1:3*n)   = gramRBP
                    gramB(2*n+1:3*n,1:n)       = transpose(gramXBP)
                    gramB(2*n+1:3*n,n+1:2*n)   = transpose(gramRBP)
                    gramB(2*n+1:3*n,2*n+1:3*n) = gramPBP
                else
                    !gramB(1:n,1:n)             = gramXBX
                    gramB(1:n,n+1:2*n)         = 0
                    gramB(1:n,2*n+1:3*n)       = gramXBP
                    gramB(n+1:2*n,1:n)         = 0
                    !gramB(n+1:2*n,n+1:2*n)     = gramRBR
                    gramB(n+1:2*n,2*n+1:3*n)   = gramRBP
                    gramB(2*n+1:3*n,1:n)       = transpose(gramXBP)
                    gramB(2*n+1:3*n,n+1:2*n)   = transpose(gramRBP)
                    !gramB(2*n+1:3*n,2*n+1:3*n) = gramPBP
                    forall(ii=1:nn)
                        gramB(ii,ii) = 1
                    end forall
                end if
            else
                if (explicitGramFlag) then
                    gramA(1:n,1:n)             = gramXAX
                    gramA(1:n,n+1:2*n)         = gramXAR
                    gramA(n+1:2*n,1:n)         = transpose(gramXAR)
                    gramA(n+1:2*n,n+1:2*n)     = gramRAR
                    
                    gramB(1:n,1:n)             = gramXBX
                    gramB(1:n,n+1:2*n)         = gramXBR
                    gramB(n+1:2*n,1:n)         = transpose(gramXBR)
                    !gramB(n+1:2*n,n+1:2*n)     = gramRBR
                    forall(ii=n+1:2*n)
                        gramB(ii,ii) = 1
                    end forall
                else
                    call diag(lambda,lambdatmp)
                    gramA(1:n,1:n)             = lambdatmp
                    gramA(1:n,n+1:2*n)         = gramXAR
                    gramA(n+1:2*n,1:n)         = transpose(gramXAR)
                    gramA(n+1:2*n,n+1:2*n)     = gramRAR
                    
                    forall(ii=1:blkSize+activeRSize)
                        gramB(ii,ii) = 1
                    end forall
                end if
                
                call cond(gramB,2,condestG)
                condestG = log10(condestG)+1
                
                if ((condestG/condestGmean > 2) .and. (condestG > 2) .or. (condestG > 8)) then
                    if (verbosityLevel) then
                        write(*,*) "Restart on step", iterNum, "as condestG", condestG
                    end if
                    
                    if (cond_try == 1 .and. restart == 0) then
                        restart = 1
                    else
                        write(*,*) "Warning. Gram matrix ill-conditioned: results unpredictable."
                    end if
                else
                    exit
                end if
            end if
        end do
        
        if (mpirank == 0) then
            !call eig(gramA,eps,gramB,noneig,gramA1,JT)!gramB1)
            gramB2 = gramB1(1:blkSize,1:blkSize)
            call diag(gramB2,lambda)
            coordX = gramA1(:,1:blkSize)
        else
            deallocate(lambda)
        end if
            
        if (mpistat) then
            write(*,*) "Error in lobpcg_2.f90. MPI is not available now."
            stop
            !call MPI_Bcast_variable(coordX,0,coordX)
            !call MPI_Bcast_variable(lambda,0,lambda)
        end if
            
        if (restart == 0) then
            blkP  = matmul(blkRtmp,coordX(blkSize+1:blkSize+activeRSize,:)) + matmul(blkptmp,coordX(blkSize+activeRSize+1:blkSize,:))
            blkAP = matmul(blkARtmp,coordX(blkSize+1:blkSize+activeRSize,:)) + matmul(blkAPtmp,coordX(blkSize+activeRSize+1:blkSize,:))
        else
            blkP  = matmul(blkRtmp,coordX(blkSize+1:blkSize+activeRSize,:))
            blkAP = matmul(blkARtmp,coordX(blkSize+1:blkSize+activeRSize,:))
        end if
            
        blkX  = matmul(blkX,coordX(1:blkSize,:)) + blkP
        blkAX = matmul(blkAX,coordX(1:blkSize,:))+ blkAX
            
        lambdaHistory(1:blkSize,1:iterNum+1) = lambda
        condestGhistory(iterNum+1,1)       = condestG
            
        if (verbosityLevel) then
            write(*,*) "Iteration", iterNum, "current block size", currentBlockSize
            write(*,*) "Eigenvalues lambda", lambda
            write(*,*) "Residual Norms", resNorms
        end if
    end do
        
    call distArrayMtimes(blkX,blkX,mpistat,gramXBX)
    gramXBX = 0.5*(gramXBX+transpose(gramXBX))
    !call fevalOPA(blkX,blkAX)
    call distArrayMtimes(blkX,blkAX,mpistat,gramXAX)
    gramXAX = 0.5*(gramXAX+transpose(gramXAX))
    
    if (mpirank == 0) then
        !call eig(gramXAX,eps,gramXBX,coordX,gramXBXtmp,JT)
        call diag(gramXBXtmp,lambda)
    else
        deallocate(coordX)
    end if
    
    if (mpistat) then
        write(*,*) "Error in lobpcg_2.f90. MPI is not available now."
        stop
        !call MPI_Bcast_variable(coordX,0,coordX)
        !call MPI_Bcast_variable(lambda,0,lambda)
    end if
    
    blkX  = matmul(blkX,coordX)
    blkAX = matmul(blkAX,coordX)
    
    if (blkSize > 1) then
        call spdiags_4in(lambda,tmp0,blkSize,blkSize,blkR)
        blkR = blkAX - matmul(blkX,blkR)
    else
        blkR = blkAX - matmul(blkX,lambda)
    end if
    
    resNorms = sum(blkR*blkR,1)
    
    if (mpistat) then
        !call MPI_Allreduce_sum(resNorms,resNorms)
        write(*,*) "Error in lobpcg_2.f90. MPI is not available now."
        stop
    end if
    
    resNorms = resNorms**0.5
    resNormsHistory(1:blkSize,iterNum) = resNorms
    
    if (verbosityLevel /= 0) then
        write(*,*) "Final Eigenvalues lambda", lambda
        write(*,*) "Final Residual Norms", resNorms
    end if
    
    if (verbosityLevel == 2) then
        write(*,*) "Error in lobpcg_2.f90. verbosityLevel could not be 2 now. please use other values."
        stop
    end if
    
    out1 = failureFlag
    out2 = lambdaHistory(1:blkSize,1:iterNum)
    out2 = resNormsHistory(1:blkSize,1:iterNum-1)
    
    return
    end subroutine lobpcg_2
    end module lobpcg_2_module
    