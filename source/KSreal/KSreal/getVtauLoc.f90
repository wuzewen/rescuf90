! getVtauloc.f90
    
!*******************************************************
    !
    !
    !
!*******************************************************
    
    module getVtauloc_module
    contains
    subroutine getVtauloc(FRC,spin,vtauloc)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_2D_module
    use VataMN_1D_module
    use distmat_zeroslike_2D_module
    use distmat_allgather_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    logical                         :: spin
    
    ! temporary variables
    type(VataMN_1D) :: vtautmp1
    type(VataMN_2D) :: vtautmp2
    integer :: cn
    
    ! output variables
    real*8, allocatable             :: vtauloc(:)
    
    ! body of this function
    if (size(FRC%potential%vtauloc) /= 0) then
        vtauloc = FRC%potential%vtauloc(:,spin)
    else if (size(FRC%potential%vtau%vata) /= 0) then
        cn = FRC%potential%vtau%m
        call distmat_zeroslike_2D(FRC,FRC%potential%vtau,(/cn,1/),vtautmp2)
        vtautmp2%vata(:,1) = FRC%potential%vtau%vata(:,spin)
        call distmat_allgather(FRC,vtautmp1,vtauloc)
    end if
    
    return
    end subroutine getVtauloc
    end module getVtauloc_module