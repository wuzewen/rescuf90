! calcCFBound.f90
!**********************************************
    !
!**********************************************
    
    module calcCFBound_module
    contains
    subroutine calcCFBound(FRC,LB,UB)
    
    implicit none
    
    ! input variables
    
    ! temporery variables
    logical :: mpistat
    integer :: mpirank, mpisize, iter, ispin, nspin, cn, nkpt, gridn, LB, nham, 
    real*8, allocatable :: kdir(:,:), veff(:,:,:), UB(:,:)
    
    ! output variables
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%size
    iter    = FRC%scloop
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    cn      = product(FRC%domain%cgridn)
    kdir    = FRC%kpoint%ikdirect
    nkpt    = size(kdir,1)
    
    call updateVeff(FRC,FRC%potential%veffin(iter),FRC%potential%vps,veff)
    call distmat_allgether(FRC,veff,FRC%potential%vloc)
    gridn = veff%m
    
    if (vtauStatus) then
        call distmat_allgether(FRC,FRC%potential%vtau,FRC%potential%vtauloc)
    end if
    
    ksnrg = FRC%energy%ksnrg(max(1,iter-1))
    LB    = max(ksnrg,1)
    call permute(LB,(/2,3,1/),LB)
    call squeeze(LB,LB)
    
    allocate(UB(nkpt,nspin))
    niter = FRC%eigensolver%ubmaxit
    do ii = mpirank+1,nkpt*nspin,mpisize
        call ind2sub((/nkpt,nspin/),ii,kk,ss)
        kpt = kdir(kk,:)
        call GetKBorb(FRC,kpt,ss,FRC)
        if (ispin == 1 .or. ispin == 2) then
            call GenHamFun(FRC,kpt,ss,ham)
            nham = cn
        else if (ispin == 4) then
            call GenHamFun(FRC,kpt,ss,hamu,hamd)
            ! ham = xxx
            nham = 2*cn
        end if
        
        if (trim(FRC%eigensolver%ubmethod) == trim("lanczos")) then
            call Lanczos(ham,nham,niter,.TRUE.,UB(kk,ss))
        else
            opts%issym  = 1
            opts%isreal = any(kpt)
            opts%tol    = 0.1
            opts%maxit  = niter
            sigma       = "lr"
            if (.not. any(kpt)) then
                sigma = "la"
            end if
            call eigs(ham,gridn,1,sigma,opts,V,UB(kk,ss),flag)
            if (flag) then
                tmp = ham(V)
                UB(kk,ss) = matmul(transpose(V),tmp)
                call norm(tmp-matmul(V,UB(kk,ss)),tmp2)
                UB(kk,ss) = UB(kk,ss) + tmp2
            else
                UB(kk,ss) = UB(kk,ss)+0.1
            end if
        end if
    end do
    
    if (mpistat) then
        call MPI_Bcast_variable(real(LB),0,LB)
        call MPI_Allreduce_sum(real(UB),UB)
    end if
    
    return
    end subroutine calcCFBound
    end module calcCFBound_module
    
    