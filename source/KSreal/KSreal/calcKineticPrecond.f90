! calcKineticPrecond.f90
    
!***********************************************************************
    !
    !
    !
!***********************************************************************
    
    module calcKineticPrecond_module
    contains
    subroutine calcKineticPrecond(FRC,degree,kpt,precond)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inversion_module
    use fftFreq_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer                         :: degree
    real*8                          :: kpt(3)
    
    ! temporary variables
    integer              :: nvec(3), ntmp, ii, jj
    real*8               :: kptt(1,3), kcart(1,3), pi
    real*8, allocatable  :: avec(:,:), bvec(:,:), ku(:,:,:), kv(:,:,:), kw(:,:,:)
    real*8, allocatable  :: x(:,:,:), precondtmp(:,:,:)
    
    ! output variables
    real*8, allocatable  :: precond(:,:,:)
    
    ! body of this function
    pi        = 3.14159265354
    allocate(avec(3,3),bvec(3,3))
    avec      = FRC%domain%latvec
    call inversion(avec,bvec)
    bvec      = transpose(bvec)
    bvec      = 2*pi*bvec
    kptt(1,:) = kpt(:)
    kcart     = matmul(kptt,bvec)
    nvec      = FRC%domain%cgridn
    ntmp      = product(nvec)
    allocate(ku(nvec(1),nvec(2),nvec(3)))
    allocate(kv(nvec(1),nvec(2),nvec(3)))
    allocate(kw(nvec(1),nvec(2),nvec(3)))
    call fftFreq(nvec,bvec,ku,kv,kw)
    ku        = ku+kcart(1,1)
    kv        = kv+kcart(1,2)
    kw        = kw+kcart(1,3)
    !x         = ku*conjg(ku)+kv*conjg(kv)+kw*conjg(kw)
    allocate(x(nvec(1),nvec(2),nvec(3)))
    x         = ku*ku+kv*kv+kw*kw
    x         = 0.25*x
    
    select case (degree)
    case (-1)
        allocate(precond(0,0,0))
    case (0)
        allocate(precondtmp(nvec(1),nvec(2),nvec(3)))
        allocate(precond(nvec(1),nvec(2),1))
        precondtmp = 1.0/x/2.0
        forall(ii=1:nvec(1),jj=1:nvec(2))
            precond(ii,jj,1) = minval(precondtmp(ii,jj,:))
        end forall
    case (1)
        allocate(precondtmp(nvec(1),nvec(2),nvec(3)))
        allocate(precond(nvec(1),nvec(2),1))
        precondtmp = 1.0/(2*x-2)
        forall(ii=1:ntmp)
            precond(ii,jj,1) = minval(precondtmp(ii,jj,:))
        end forall
    case (2)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 9.0+6.0*x+4.0*x**2.0
        precond = precond/(precond+8.0*x**3)
    !case (3)
    !    precond = 27.0+18.0*x+4.0*x**2.0
    !    precond = precond/(precond+8.0*x**3)
    case (4)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 81.0+54.0*x+36.0*x**2.0+24.0*x**3+16*x**4
        precond = precond/(precond+32.0*x**5)
    case (5)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 243.0+162.0*x+108.0*x**2+72.0*x**3+48.0*x**4+32.0*x**5
        precond = precond/(precond+64.0*x**6)
    case (6)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 729.0+486.0*x+324.0*x**2+216.0*x**3+144.0*x**4+96.0*x**5+64.0*x**6
        precond = precond/(precond+128.0*x**7)
    case (7)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 2187.0+1458.0*x+972.0*x**2+648.0*x**3+432.0*x**4+288.0*x**5+192.0*x**6+128.0*x**7
        precond = precond/(precond+256.0*x**8)
    case default
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 81.0+54.0*x+36.0*x**2+24.0*x**3+16.0*x**4
        precond = precond/(precond+32.0*x**5)
    end select
    
    return
    end subroutine calcKineticPrecond
    end module calcKineticPrecond_module