! operTimesDistArray.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module operTimesDistArray_module
    contains
    subroutine operTimesDistArray(FRC,oper,distData,opN,distN,distDataOut)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_2D_module
    use ModBCDist_2D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    character(len=20)               :: oper
    real*8, allocatable             :: distData(:,:)
    integer                         :: opN, distN
    
    ! temporary variables
    integer             :: mpisize, blkSize, stmp
    type(VataMN_2D)     :: tmpX, X
    real*8, allocatable :: vata(:,:)
    
    
    ! output variables
    real*8, allocatable             :: distDataOut(:,:)
    
    ! body of this function
    mpisize = FRC%mpi%mpisize
    stmp    = size(distData,1)
    blkSize = size(distData,2)
    
    call InitDistArray_2D(distN,blkSize,distN/mpisize,1,mpisize,1,tmpX)
    tmpX%vata = distData
    call ModBCDist_2D(FRC,tmpX,1,1,1,mpisize,.FALSE.,.FALSE.,X)
    if (size(X%vata,2) >0 ) then
        deallocate(tmpX%vata)
        allocate(tmpX%vata(opN,stmp*blkSize/opN))
        tmpX%vata = reshape(X%vata,(/opN,stmp*blkSize/opN/))
        if (trim(oper) == trim("ham")) then
            !call feval(oper,tmpX%vata,vata)
        end if
        
        deallocate(tmpX%vata)
        allocate(tmpX%vata(distN,stmp*blkSize/distN))
        tmpX%vata = reshape(vata,(/distN,stmp*blkSize/distN/))
    else
        tmpX = X
    end if
    
    call ModBCDist_2D(FRC,tmpX,distN/mpisize,1,mpisize,1,.FALSE.,.FALSE.,X)
    distDataOut = X%vata
    
    return
    end subroutine operTimesDistArray
    end module operTimesDistArray_module