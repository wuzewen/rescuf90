! precondT.f90
    
!***************************************************************
    !
    !
    !
!***************************************************************
    
    module precondT_module
    contains
    subroutine precondT(FRC,degree,precond)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inversion_module
    use fftFreq_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer :: degree
    
    ! temporary variables
    real*8, allocatable :: avec(:,:), bvec(:,:), ku(:,:,:), kv(:,:,:), kw(:,:,:), k2(:,:,:)
    real*8              :: pi
    integer             :: nvec(3), ii, jj
    
    ! output variables
    real*8, allocatable :: precond(:,:,:)
    
    ! body of this function
    allocate(avec(3,3))
    allocate(bvec(3,3))
    pi   = 3.14159265354
    avec = FRC%domain%latvec
    nvec = FRC%domain%cgridn
    call inversion(avec,bvec)
    bvec = 2.0*pi*transpose(bvec)
    allocate(ku(nvec(1),nvec(2),nvec(3)))
    allocate(kv(nvec(1),nvec(2),nvec(3)))
    allocate(kw(nvec(1),nvec(2),nvec(3)))
    call fftFreq(nvec,bvec,ku,kv,kw)
    allocate(k2(nvec(1),nvec(2),nvec(3)))
    k2   = ku*ku+kv*kv+kw*kw
    k2   = 0.5*k2
    
    select case (degree)
    case (0)
        allocate(precond(0,0,0))
    case (1)
        k2 = 1.0/(2.0*k2-2.0)
        allocate(precond(size(k2,2),size(k2,3),1))
        forall(ii=1:size(k2,2),jj=1:size(k2,3))
            precond(ii,jj,1) = minval(k2(:,ii,jj))
        end forall
    case (2)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 9.0+6.0*k2+4.0*k2**2
        precond = precond/(precond+8.0*k2**3)
    case (4)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 81.0+54.0*k2+36.0*k2**2+24.0*k2**3+16.0*k2**4
        precond = precond/(precond+32.0*k2**5)
    case (5)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 243.0+162.2*k2+108.0*k2**2+72.0*k2**3+48.0*k2**4+32.0*k2**5
        precond = precond/(precond+64.0*k2**6)
    case (6)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 729.0+486.0*k2+324.0*k2**2+216.0*k2**3+144.0*k2**4+96.0*k2**5+64.0*k2**6
        precond = precond/(precond+128.0*k2**7)
    case (7)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 2187.0+1458.0*k2+972.0*k2**2+648.0*k2**3+432.0*k2**4+288.0*k2**5+192.0*k2**6+128.0*k2**7
        precond = precond/(precond+256.0*k2**8)
    case default
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 81.0+54.0*k2+36.0*k2**2+24.0*k2**3+16.0*k2**4
        precond = precond/(precond+32.0*k2**5)
    end select
    
    return
    end subroutine precondT
    end module precondT_module