! genTauKinOp.f90
    
!*********************************************************
    !
    !
    !
!*********************************************************
    
    module genTauKinOp_module
    contains
    subroutine genTauKinOp(FRC,kpt,gpu,X,vtau,tauKin)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use genDerFun_module
    use genGradFun_module
    use bsxfunTimes_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable             :: X(:,:)
    logical                         :: gpu
    real*8                          :: kpt(3)
    
    ! temporary variables
    integer :: nx, cgridn(3), ii
    real*8, allocatable  :: intmp(:,:), tauKin1(:,:), avec(:,:), tautmpt(:,:)
    type(VataMN_2D)      :: tautmp(3)
    complex, allocatable :: tmp(:,:), fGfun(:,:)
    
    ! output variables
    real*8, allocatable             :: vtau(:,:), tauKin(:,:)
    
    ! body of this function
    call GenGradFun(FRC,kpt,gpu,X,tmp,fGfun)
    nx    = size(tmp,2)
    forall(ii=1:3:nx-2)
        intmp(:,ii) = tmp(:,ii)
    end forall
    call bsxfunTimes(vtau,intmp,tautmpt)
    call GenDerFun(1,FRC,avec,cgridn,kpt,gpu,X,tautmp,tauKin)
    forall(ii=2:3:nx-1)
        intmp(:,ii) = tmp(:,ii)
    end forall
    call bsxfunTimes(vtau,intmp,tautmpt)
    call GenDerFun(2,FRC,avec,cgridn,kpt,gpu,X,tautmp,tauKin1)
    tauKin = tauKin + tauKin1
    forall(ii=3:3:nx)
        intmp(:,ii) = tmp(:,ii)
    end forall
    call bsxfunTimes(vtau,intmp,tautmpt)
    call GenDerFun(3,FRC,avec,cgridn,kpt,gpu,X,tautmp,tauKin1)
    tauKin = tauKin + tauKin1
    
    return
    end subroutine genTauKinOp
    end module genTauKinOp_module