! norm.f90
!*****************************************
    !
!*****************************************
    
    module norm_2D_module
    contains
    subroutine norm_2D(ArrayA,p,mo)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: ArrayA(:,:)
    integer             :: p
    
    ! temporery variables
    real*8, allocatable :: ArrayT(:,:)
    
    ! output variables
    real*8              :: mo
    
    ! body of this function
    allocate(ArrayT(size(ArrayA,1),size(ArrayA,2)))
    ArrayT = abs(ArrayA)
    mo     = sum(ArrayT**p)**real(1/p)
    
    return
    end subroutine norm_2D
    end module norm_2D_module