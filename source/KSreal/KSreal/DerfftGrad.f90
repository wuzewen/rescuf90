! DerfftGrad.f90
    
!*************************************************************************
    !
    !
    !
!*************************************************************************
    
    module DerfftGrad_module
    contains
    subroutine DerfftGrad(x,n,k,kpt,Xout)
    
    use fft4_module
    
    implicit none
    
    ! input variables
    complex, allocatable :: X(:,:)
    integer              :: n(3)
    real*8               :: kpt(3)
    real*8,allocatable   :: k(:,:,:)
    
    ! temporary variables
    complex, allocatable :: xtmp(:,:,:,:), ytmp(:,:,:,:)
    complex, allocatable :: ktmp(:,:,:)
    integer              :: ntmp, nx
    complex              :: imnu
    
    ! output variables
    complex, allocatable :: Xout(:,:)
    
    ! body of this function
    imnu = (0.0,1.0)
    nx   = size(X,2)
    xtmp = reshape(X,(/n(1),n(2),n(3),nx/))
    ntmp = product(n)
    call fft4(xtmp,1,ytmp)
    call fft4(ytmp,2,xtmp)
    call fft4(xtmp,3,ytmp)
    !ktmp = imnu*(k+kpt)
    call bsxfunTimes_3D_CMPLX(ytmp,ktmp,xtmp)
    x    = reshape(xtmp,(/ntmp,nx/))
    
    return
    end subroutine DerfftGrad
    end module DerfftGrad_module