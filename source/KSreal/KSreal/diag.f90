! diag.f90
    
!***********************************************************************
    !
    !
    !
!***********************************************************************
    
    module diag_module
    contains
    subroutine diag(A,B)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    
    ! temporary variables
    integer :: Nx, Ny, ii, ntmp
    
    ! output variables
    real*8, allocatable :: B(:,:)
    
    ! body of this function
    Nx = size(A,1)
    Ny = size(A,2)
    
    if (Nx == 1) then
        allocate(B(Ny,Ny))
        B = 0
        forall(ii=1:Ny)
            B(ii,ii) = A(1,ii)
        end forall
    else if (Ny == 1) then
        allocate(A(Nx,Nx))
        B = 0
        forall(ii=1:Nx)
            B(ii,ii) = A(ii,1)
        end forall
    else if (Nx == Ny) then
        allocate(B(Nx,Ny))
        B = 0
        forall(ii=1:Nx)
            B(ii,ii)=A(ii,ii)
        end forall
    else
        ntmp = min(Nx,Ny)
        allocate(B(ntmp,ntmp))
        B = 0
        forall(ii=1:Ntmp)
            B(ii,ii)=A(ii,ii)
        end forall
    end if
    
    return
    end subroutine diag
    end module diag_module