! sss.f90
    
!****************************************************************
    !
    !
    !
!****************************************************************
    
    !module sss_module
    !contains
    subroutine sss(f,g,cs,sn)
    
    implicit none
    
    ! input variables
    real*8 :: f, g, cs, sn, d, r
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    if ((abs(f)+abs(g)) == 0.0) then
        cs = 1.0
        sn = 0.0
        d  = 0.0
    else
        d  = sqrt(f*f+g*g)
        if (abs(f) > abs(g)) then
            d = sign(d,f)
        end if
        if (abs(g) > abs(f)) then
            d = sign(d,g)
        end if
        cs = f/d
        sn = g/d
    end if
    
    r = 1.0
    if (abs(f) > abs(g)) then
        r = sn
    else
        if (cs /= 0.0) then
            r = 1.0/cs
        end if
    end if
    
    f = d
    g = r
    
    return
    end subroutine sss