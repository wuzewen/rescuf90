! genVnlOp.f90
    
!*******************************************************************
    !
    !
    !
!*******************************************************************
    
    module genVnlOp_module
    contains
    subroutine genVnlOp(FRC,gpu,X,vnl)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use bsxfunTimes_module
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    logical :: gpu
    real*8, allocatable :: X(:,:)
    
    ! temporary variables
    real*8 :: avec(3,3)
    real*8, allocatable :: chilm(:,:), chilmtmp(:,:), kbedr(:,:), vnltmp(:,:)
    
    ! output variabels
    real*8, allocatable :: vnl(:,:)
    
    ! body of this function
    !call det(FRC%domain%latvec,dr)
    avec = FRC%domain%latvec
    dr   = avec(1,1)*avec(2,2)*avec(3,3)+avec(1,2)*avec(2,3)*avec(3,1)+avec(1,3)*avec(2,1)*avec(3,2)-avec(1,3)*avec(2,2)*avec(3,1)-avec(1,1)*avec(2,3)*avec(3,2)+avec(1,2)*avec(2,1)*avec(3,3)
    if (FRC%interpolation%vnl) then
        dr = dr/product(FRC%domain%fgridn)
    else
        dr = dr/product(FRC%domain%cgridn)
    end if
    allocate(kbedr(size(FRC%potential%vnl%KBEnergy),1))
    kbedr(:,1) = FRC%potential%vnl%KBEnergy(:)*dr
    allocate(chilm(size(FRC%potential%vnl%KBorb,1),size(FRC%potential%vnl%KBorb,2)))
    chilm      = FRC%potential%vnl%KBorb
    
    if (gpu) then
        !call gpuArray(kbedr)
        !call gpuArray(chilm)
        !chilmtmp = matmul(transpose(chilm),X)
        !call bsxfunTimes(chilmtmp,kbedr,vnl)
        !vnl = matmul(chilm,vnl)
        !if (FRC%interpolation%vnl) then
            !call initFun(vnl,X,vnltmp)
            !call antFun(vnltmp,X,vnl)
        ! end if
    else
        allocate(chilmtmp(size(chilm,2),size(X,2)))
        chilmtmp = matmul(transpose(chilm),X)
        allocate(vnltmp(size(chilmtmp,1),size(chilmtmp,2)))
        call bsxfunTimes(chilmtmp,kbedr,vnltmp)
        allocate(vnl(size(chilm,1),size(vnltmp,2)))
        vnl = matmul(chilm,vnltmp)
        if (FRC%interpolation%vnl) then
            write(*,*) "Error in genVnlOp.f90. FRC%interpolation%vnl should be false now."
            stop
            !call initFun(vnl,X,vnltmp)
            !call antFun(vnltmp,X,vnl)
        end if
    end if
    
    return
    end subroutine genVnlOp
    end module genVnlOp_module