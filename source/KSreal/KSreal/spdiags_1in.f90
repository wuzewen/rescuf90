! spdiags_1in.f90
    
!*********************************************************
    !
    !
    !
!*********************************************************
    
    module spdiags_1in_module
    contains
    subroutine spdiags_1in(A,B)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    
    ! temporary variables
    integer :: Nx, Ny, Nn, ntmp, ii, jj
    real*8, allocatable :: tmp(:)
    
    ! output variables
    real*8, allocatable :: B(:,:)
    
    ! body of this function
    Nx   = size(A,1)
    Ny   = size(A,2)
    Nn   = min(Nx,Ny)
    ntmp = 0
    do ii = -Nx+1,Ny-1,1
        if (ii < 0) then
            allocate(tmp(Nn+ii))
            forall(jj=1:Nn+ii)
                tmp(jj) = A(Nn+ii+jj-1,jj)
            end forall
        else if (ii>=0) then
            allocate(tmp(Nn-ii))
            forall(jj=1:Nn-ii)
                tmp(jj) = A(jj,Nn-ii+jj-1)
            end forall
        end if
        if (any(tmp /= 0)) then
            ntmp = ntmp+1
        end if
        deallocate(tmp)
    end do
    allocate(B(Nn,ntmp))
    B = 0
    do ii = -Nx+1,Ny-1,1
        if (ii < 0) then
            allocate(tmp(Nn+ii))
            forall(jj=1:Nn+ii)
                tmp(jj) = A(Nn+ii+jj-1,jj)
            end forall
            if (any(tmp /= 0)) then
                ntmp = ntmp+1
                forall(jj=1:Nn+ii)
                    B(jj,ntmp) = tmp(jj)
                end forall
            end if
        else if (ii>=0) then
            allocate(tmp(Nn-ii))
            forall(jj=1:Nn-ii)
                tmp(jj) = A(jj,Nn-ii+jj-1)
            end forall
            if (any(tmp /= 0)) then
                ntmp = ntmp+1
                forall(jj=1:Nn+ii)
                    B(Nn-ntmp+jj,ntmp) = tmp(jj)
                end forall
            end if
        end if
        deallocate(tmp)
    end do
    
    return
    end subroutine spdiags_1in
    end module spdiags_1in_module