! rkronprod.f90
    
!*****************************************************************
    !
    !
    !
!*****************************************************************
    
    module rkronprod_module
    contains
    subroutine rkronprod(A,B,KA)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    integer :: p, q, n, m
    real*8, allocatable :: T(:,:), Atmp(:,:)
    
    ! output variables
    real*8, allocatable :: KA(:,:)
    
    ! body of this function
    p = size(A,1)
    q = size(A,2)
    n = size(B,1)/q
    m = size(B,2)
    
    allocate(T(q,m*n))
    T = reshape(B,(/q,m*n/))
    Atmp = matmul(A,T)
    allocate(KA(p*n,m))
    KA = reshape(Atmp,(/p*n,m/))
    
    return
    end subroutine rkronprod
    end module rkronprod_module