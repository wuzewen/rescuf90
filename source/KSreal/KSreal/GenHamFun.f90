! GenHamFun.f90
    
!***************************************************
    !
    !
    !
!***************************************************
    
    module GenHamFun_module
    contains
    subroutine GenHamFun(FRC,kpt,spin,gpu,X,ham,hamu,hamd)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use getVtauloc_module
    use genVnlop_module
    use genVlocop_module
    use genLaplFun_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8                          :: kpt(3)
    logical                         :: gpu
    integer                         :: spin
    real*8, allocatable             :: X(:,:)
    
    ! temporary variables
    integer             :: ispin, ntmp, mtmp, cgridn(3)
    real*8, allocatable :: vloc(:,:), vtauloc(:), vnlPvloc(:,:), cLkfun(:,:), vnl(:,:), vnld(:,:), vnlu(:,:)
    real*8, allocatable :: xu(:,:), xd(:,:), vnlju(:,:), vnljd(:,:), vlocu(:,:), vlocd(:,:), cLkfunu(:,:), cLkfund(:,:)
    real*8, allocatable :: vlocfun(:,:), out2(:,:), fLkfun(:,:)
    real*8              :: avec(3,3)
    
    ! output variables
    real*8, allocatable             :: ham(:,:), hamu(:,:), hamd(:,:)
    
    ! body of this function
    ispin  = FRC%spin%ispin
    ntmp   = size(FRC%potential%vloc,1)
    mtmp   = size(FRC%potential%vloc,1)
    if (ispin == 1 .or. ispin == 2) then
        allocate(vloc(ntmp,1))
        vloc(:,1) = FRC%potential%vloc(:,spin)
    else
        write(*,*) "Error in GenHamFun.f90. Ispin should not be greater than 2."
        stop
        !allocate(vloc(ntmp,mtmp))
        !vloc    = FRC%potential%vloc
    end if
    
    if (size(FRC%potential%vtau%vata) /= 0) then
        !call getVtauloc(FRC,spin,vtauloc)
        !avec    = FRC%domain%latvec
        !cgridn  = FRC%domain%cgridn
        !vtauloc = vtauloc+1
    else
        !call GenLaplFun(FRC,kpt,gpu,cLkfun,nonsense)
    end if
    
    if (FRC%interpolation%vloc) then
        write(*,*) "Error in GenHamFun.f90. FRC%interpolation%vloc should be flase now."
        stop
        !if (ispin == 1 .or. ispin == 2) then
        !    call genVnlVloc(FRC,vloc,gpu,X,vnlPvloc)
        !    if (size(FRC%potential%vtau%vata) /= 0) then
        !        call genTauKinOp(FRC,avec,cgridn,kpt,gpu,clkfun)
        !    else
        !        call GenLaplFun(FRC,kpt,gpu,X,clkFun)
        !    end if
        !    ham = -0.5*cLkfun+vnlPvloc
        !end if
    else
        if (ispin == 1 .or. ispin == 2) then
            call genVnlOp(FRC,gpu,X,vnl)
            call genVlocOp(FRC,vloc,gpu,X,Xu,Xd,vlocfun,out2)
            if (size(FRC%potential%vtau%vata) /= 0) then
                write(*,*) "Error in GenHamFun.f90. FRC%potential%vtau should not exist for now."
                stop
                !call genTauKinOp(FRC,avec,cgridn,kpt,gpu,clkfun)
            else
                call GenLaplFun(FRC,kpt,gpu,X,clkFun,fLkfun)
            end if
            
            ham = -0.5*cLkFun+vnl+vlocfun
        else if (ispin == 4) then
            write(*,*) "Error in GenHamFun.f90. Non-collinear spin is not available now."
            stop
            !call genVnlOp(FRC,gpu,X,vnlu)
            !call genVnlOp(FRC,gpu,X,vnld)
            !call genVlocOp(FRC,vloc,gpu,xu,xd,vlocu,vlocd)
            !if (FRC%spin%SOI) then
            !    call genVnlSO(FRC,xu,xd,vnlju,vnljd)
            !    vlocu = vnlju+vlocu
            !    vlocd = vnljd+vlocd
            !end if
            !if (size(FRC%potential%vtau%vata) /= 0) then
            !    call genTauKinOp(FRC,avec,cgridn,kpt,gpu,clkfunu)
            !    call genTauKinOp(FRC,avec,cgridn,kpt,gpu,clkfund)
            !else
            !    call GenLaplFun(FRC,kpt,gpu,X,clkFunu)
            !    call GenLaplFun(FRC,kpt,gpu,X,clkFund)
            !end if
            !hamu = -0.5*clkFunu+vnlu+vlocu
            !hamd = -0.5*clkfund+vnld+vlocd
        end if
    end if
    
    return
    end subroutine GenHamFun
    end module GenHamFun_module
            