! lobpcgMain.f90
    
!*****************************************************
    !
    !
    !
!*****************************************************
    
    module lobpcgMain_module
    contains
    subroutine lobpcgMain(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    use VataMN_2D_module
    use UpdateVeff_module
    use distmat_allgather_module
    use GetKBorb_module
    use calcKineticPrecond_module
    use lobpcg_2_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical :: mpistat, failureFlag
    integer :: mpisize, iter, cn, nkpt, nband, nspin, maxRestart, precondId, nsym
    integer :: kk, ss, ii, ntmp, ll, ncoltmp1, ncol, ncoltmp, ncoltmp2, ispin
    integer :: ptmp, btmp, restartIt, maxit, sizeY
    real*8  :: latvec(3,3), vol, dr, tolt(2), tol, kpt(3)
    integer, allocatable :: colvec(:)
    real*8,  allocatable :: kdir(:,:), prekin(:,:,:), psitmp(:,:), blockY(:,:), psitmp2(:,:)
    real*8,  allocatable :: nrgtmp(:,:), nonsense1(:,:), nonsense2(:,:), nrg(:,:,:), lambda(:,:)
    type(VataMN_1D) :: veff
    type(VataMN_2D) :: veffin, veff2D
    
    
    ! output variables
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpisize = FRC%mpi%mpisize
    iter    = FRC%scloop
    cn      = product(FRC%domain%cgridn)
    latvec  = FRC%domain%latvec
    vol     = latvec(1,1)*latvec(2,2)*latvec(3,3)  &
             +latvec(1,2)*latvec(2,3)*latvec(3,1)  &
             +latvec(1,3)*latvec(2,1)*latvec(3,2)  &
             -latvec(1,1)*latvec(2,3)*latvec(3,2)  &
             -latvec(1,2)*latvec(2,1)*latvec(3,3)  &
             -latvec(1,3)*latvec(2,2)*latvec(3,1)
    dr         = vol/cn
    allocate(kdir(size(FRC%kpoint%ikdirect,1),3))
    kdir       = FRC%kpoint%ikdirect
    nkpt       = size(kdir,1)
    nband      = FRC%eigensolver%nband
    ispin      = FRC%spin%ispin
    nspin      = FRC%spin%nspin
    maxit      = FRC%eigenSolver%maxit
    tolt       = FRC%eigensolver%tol
    allocate(nrg(nband,nkpt,nspin))
    nrg        = 0
    maxRestart = FRC%eigensolver%maxRestart
    precondId  = FRC%eigensolver%precond
    nsym       = FRC%eigensolver%nsym
    tol        = min(tolt(1),tolt(2))!,FRC%mixing%dRho/real(nband)/2.0)
    
    allocate(veffin%Vata(size(FRC%potential%veffin(iter)%vata),1))
    veffin%Vata(:,1) = FRC%potential%veffin(1)%vata
    veffin%m         = FRC%potential%veffin(1)%m
    veffin%n         = FRC%potential%veffin(1)%n
    veffin%m         = FRC%potential%veffin(1)%mblock
    veffin%nblock    = FRC%potential%veffin(1)%nblock
    veffin%mproc     = FRC%potential%veffin(1)%mproc
    veffin%nproc     = FRC%potential%veffin(1)%nproc
    call updateVeff(FRC,veffin,FRC%potential%vps,.FALSE.,veff2D)
    call distmat_allgather(FRC,veff,FRC%potential%vloc1)
    
    do kk = 1,nkpt,1
        kpt(:) = kdir(kk,:)
        call GetKBorb(FRC,kpt)
        call calcKineticPrecond(FRC,precondId,kpt,prekin)
        
        do ss = 1,nspin,1
            if (FRC%option%pwmode) then
                write(*,*) "Error in lobpcgMain.f90. FRC%option%pwmode should be false now."
                stop
                !psitmp = FRC%psi(kk,ss)%vata
                !psi1   = reshape(psitmp,(/FRC%domain%cgridn(1),FRC%domain%cgridn(2),FRC%domain%cgridn(3),nband/))
                !call fft4(psi1,1,psi2)
                !call fft4(psi2,2,psi1)
                !call fft4(psi1,3,psi2)
                !psitmp    = reshape(psi2,(/cn,nband/))
                !restartIt = 1
                !do while (restartIt <= maxRestart)
                !    call lobpcg_2(psitmp,tol,maxit,tmp,0,mpistat,psitmp2,nrg(:,kk,ss),failureFlag,nonsense1,nonsense2)
                !    if (failureFlag) then
                !        return
                !    else
                !        restartIt = restartIt + 1
                !    end if
                !end do
                !psi1   = reshape(psitmp2,(/FRC%domain%cgridn(1),FRC%domain%cgridn(2),FRC%domain%cgridn(3),nband/))
                !call ifft4(psi1,1,psi2)
                !call ifft4(psi2,2,psi1)
                !call ifft4(psi1,3,psi2)
                !psitmp = reshape(psi2,(/cn,nband/))
                !psitmp = psitmp/((vol/(real(cn)**2))**0.5)
                !FRC%psi(kk,ss)%vata = psitmp
            else
                allocate(colvec(nband))
                forall(ii=1:nband)
                    colvec(ii) = floor((real(ii)-1E-12)/real(nsym))+1
                end forall
                ntmp = maxval(colvec)
                do ll = 1,ntmp,1
                    ncoltmp1 = count(colvec==ll)
                    ncol     = ll*ncoltmp
                    ncoltmp2 = count(colvec<ll)
                    if (ispin == 1 .or. ispin == 2) then
                        allocate(psitmp(cn,ncoltmp1))
                        allocate(blockY(cn,ncoltmp2))
                        ptmp = 0
                        btmp = 0
                        do ii = 1,nband,1
                            if (colvec(ii) == ll) then
                                ptmp           = ptmp+1
                                psitmp(:,ptmp) = FRC%psi(kk,ss)%vata(:,ii)*(dr**0.5)
                            else if (colvec(ii) < ll) then
                                btmp           = btmp+1
                                blockY(:,btmp) = FRC%psi(kk,ss)%vata(:,ii)*(dr**0.5)
                            end if
                        end do
                    else
                        write(*,*) "Error in lobpcgMain.f90. General spin is not available now."
                        stop
                        !allocate(psi1%vata(cn,nband))
                        !psi1 = FRC%psi(kk,1)
                        !deallocate(psi1%vata)
                        !allocate(psi2%vata(cn,ncoltmp1))
                        !allocate(psi2%vata(cn,nband))
                        !psi2 = FRC%psi(kk,2)
                        !deallocate(psi2%vata)
                        !allocate(psi2%vata(cn,ncoltmp1))
                        !ptmp = 0
                        !do ii = 1,nband,1
                        !    if (colvec(ii) == ll) then
                        !        ptmp              = ptmp+1
                        !        psi1%vata(:,ptmp) = FRC%psi(kk,1)%vata(:,ii)
                        !        psi2%vata(:,ptmp) = FRC%psi(kk,2)%vata(:,ii)
                        !    end if
                        !end do
                        !psi1%n = ncol
                        !psi2%n = ncol
                        !call ModBCDist_2D(FRC,psi1,1,1,1,mpisize,.FALSE.,.FALSE.,psi1)
                        !call ModBCDist_2D(FRC,psi2,1,1,1,mpisize,.FALSE.,.FALSE.,psi2)
                        !call distmat_cat(FRC,psi1,psi2,1,psitmpMN)
                        !call ModBCDist_2D(FRC,psitmpMN,psitmpMN%m/mpisize,1,mpisize,1,.FALSE.,.FALSE.,psitmpMN)
                        !psitmp = psitmpMN%vata*(dr**0.5)
                    end if
                    
                    restartIt = 1
                    do while (restartIt<=maxRestart)
                        call lobpcg_2(FRC,psitmp,tol,maxit,mpistat,sizeY,nrgtmp,lambda,failureFlag,nonsense1,nonsense2)
                        if (failureFlag) then
                            restartIt = restartit+1
                        else
                            stop
                        end if
                    end do
                    psitmp = psitmp2/(dr**0.5)
                    
                    if (ispin == 1 .or. ispin == 2) then
                        ptmp = 0
                        do ii=1,ncoltmp1
                            if (colvec(ii) == ll) then
                                ptmp = ptmp + 1
                                FRC%psi(kk,ss)%vata(:,ptmp) = psitmp(:,ptmp)
                            end if
                        end do
                    else
                        write(*,*) "Error in lobpcgMain.f90. General spin is not available now."
                        stop
                        !psitmp2 = psitmp*(dr**0.5)
                        !call cat(2,blockY,psitmp2,blockY)
                    end if
                    if (ispin == 4) then
                        write(*,*) "Error in lobpcgMain.f90. General spin is not available now."
                        stop
                        !call initDistArray_2D(2*cn,nband,2*cn/mpisize,1,mpisize,1,psitmpMN)
                        !psitmpMN%vata = blockY/(dr**0.5)
                        !call ModBCDist_2D(FRC,psitmpMN,1,1,1,mpisize,psitmpMN)
                        !call initDistArray_2D(cn,ncol,cn/mpisize,1,1,mpisize,psi1)
                        !psi1%vata = psitmpMN%vata(1:cn,:)
                        !call initDistArray_2D(cn,ncol,cn/mpisize,1,1,mpisize,psi2)
                        !psi2%vata = psitmpMN%vata(cn+1:2*cn,:)
                        !call ModBCDist_2D(FRC,psi1,cn/mpisie,1,mpisize,1,FRC%psi(kk,1))
                        !FRC%psi(kk,1)%vata = psi1%vata
                        !call ModBCDist_2D(FRC,psi2,cn/mpisie,1,mpisize,1,FRC%psi(kk,2))
                        !FRC%psi(kk,2)%vata = psi2%vata
                    end if
                end do
            end if
        end do
    end do
    if (mpistat) then
        write(*,*) "Error in lobpcgMain.f90. mpi is not available now."
        stop
        !call MPI_Bcast_variable(nrg,0,nrg)
    end if
    allocate(FRC%energy%ksnrg(iter)%vata(nband,nkpt,nspin))
    FRC%energy%ksnrg(iter)%vata = nrg
    
    return
    end subroutine lobpcgMain
    end module lobpcgMain_module