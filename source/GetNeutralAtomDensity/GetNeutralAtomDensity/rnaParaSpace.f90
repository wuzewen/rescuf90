! rnaParaSpace.f90
!*******************************************************
    !
!*******************************************************
    
    module rnaParaSpace_module
    contains
    subroutine rnaParaSpace(fn,)
    
    implicit none
    
    ! input variables
    
    ! temporery variables
    
    ! output variables
    
    
    ! body of this function
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    fn      = product(FRC%domain%fgridn)
    natom   = FRC%atom%numberOfAtom
    lvec    = FRC%domain%latvec
    npoint  = ??????
    allocate(GridCoord(npoint))
    call GetGridPointCoord(FRC,GridCoord)
    
    if (ispin == 4) then
        magmom = FRC%atom%magmom
        lmind  = FRC%atom%element
        call initDistArray(fn,4,ceiling(fn/mpisize),1,mpisize,1,rhodm)
        allocate(rhodm%datav(npoint,4))
        rhodm%datav = 0
    else
        write(*,*) "nothing happened."
    end if
    
    call calcFilling(FRC,fillFrac)
    
    call initDistArray(fn,1,ceiling(fn/mpisize),1,mpisize,1,rho)
    
    allocate(rho%datav(npoint,1))
    rho%datav = 0
    
    if (ispin == 1) then
        call initDistArray(fn,1,ceiling(fn/mpisize),1,mpisize,1,rhodm)
        allocate(rhodm%datav(npoint,1))
        rhodm%datav = 0
    else if (ispin == 2) then
        magmom = FRC%atom%magmom
        lmind  = FRC%atom%element
        call initDistArray(fn,2,ceiling(fn/mpisize),1,mpisize,1,rhodm)
        allocate(rho%datav(npoint,2))
        rho%datav = 0
    else if (ispin == 4) then
        magmom = FRC%atom%magmom
        lmind  = FRC%atom%element
        call initDistArray(fn,4,ceiling(fn/mpisize),1,mpisize,1,rhodm)
        allocate(rho%datav(npoint,4))
        rho%datav = 0
    end if
    
    do ii = 1,natom,1
        typeE = FRC%atom%element(ii)
        pos   = FRC%atom%xyz(ii,:)
        call interpSph2Cart(FRC%elementData(typeE)%Rna,"rrData","rhoData",pos,-1,GridCoord,lvec,rhotmp)
        rho%datav = rho%datav + rhotmp
        
        if (ispin == 1) then
            rhodm%datav = rhodm%datav+matmul(rhotmp,fillFrac)
        else if (ispin == 2) then
            mgm = magmom(ii)
            rhodm%datav(:,1) = rhodm%datav(:,1) + 0.5*(1+mgm)*matmul(rhotmp,fillFrac)
            rhodm%datav(:,2) = rhodm%datav(:,2) + 0.5*(1-mgm)*matmul(rhotmp,fillFrac)
        else if (ispin == 4) then
            mgm              = magmom(ii,1)
            theta            = magmom(ii,2)
            phi              = magmom(ii,3)
            val              = FRC%element(lmind(ii))%valence
            rhodm%datav(:,1) = rhodm%datav(:,1) + matmul(rhotmp,fillFrac)
            rhotmp           = mgm/val*matmul(rhotmp,fillFrac)
            rhodm%datav(:,2) = rhodm%datav(:,2) + sin(theta)*rhotmp*cos(phi)
            rhodm%datav(:,3) = rhodm%datav(:,3) + sin(theta)*rhotmp*sin(phi)
            rhodm%datav(:,4) = rhodm%datav(:,4) + cos(theta)*rhotmp
        end if
    end do
    
    FRC%rho%atom = rho
    
    if (ispin == 4) then
        rhodm%datav(:,1) = rho%datav
        FRC%rho%input(1) = rhodm
    end if
    
    return
    end subroutine rnaParaSpace
    end module rnaParaSpace_module
    
    