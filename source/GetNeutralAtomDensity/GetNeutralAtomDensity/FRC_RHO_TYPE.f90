! FRC_RHO_TYPE.f90
    
!********************************************************************************
    !
    ! This type will be used in FRC structure data to contain rho infomation
    !
!********************************************************************************
    
    module FRC_RHO_TYPE
    
    use RHO_type
    use RHOdm_type
    use VataMN_1D_module
    use VataMN_2D_module
    use A_module
    
    type :: FRCrhoType
        character(len=20)            :: inputfile
        type(VataMN_2D), allocatable :: input(:)
        type(VataMN_2D), allocatable :: output(:)
        type(VataMN_2D), allocatable :: deltain(:)
        type(VataMN_2D), allocatable :: deltaout(:)
        type(VataMN_2D)              :: atom
        type(VataMN_2D)              :: gradRpc
        type(VataMN_2D)              :: pc
    end type FRCrhoType
    
    end module FRC_RHO_TYPE