! ModBCDist.f90
    
!**************************************
    !
    !
    !
!**************************************
    
    module ModBCDist_rho_module
    contains
    recursive subroutine ModBCDist_rho(FRC,A,mb2,nb2,mp2,np2,iscomplex,issp,B)
    
    use rho_type
    use FORTRAN_RESCU_CALCULATION_type
    !use dataType_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)       :: FRC
    type(rhoType)        :: A
    integer              :: mb2, nb2, mp2, np2, mb1, nb1, mp1, np1, m, n
    logical              :: iscomplex, issp, isrl
    
    ! temporary variables
    logical              :: mpistat, smistat
    integer              :: mpirank, mpisize, ii, jj, kk, ntmp, ntmptmp
    type(rhoType)        :: psilin
    integer, allocatable :: ia(:), ja(:), i1(:,:), i2(:,:), j1(:,:), j2(:,:), iloc1(:,:), jloc1(:,:), iloc2(:,:), jloc2(:,:)
    integer, allocatable :: indn(:), Iind(:), indm(:)
    real*8, allocatable  :: tmp1(:), tmp2(:), tmpMPI1(:,:), tmpMPI2(:,:), indmat(:,:), indmat1(:,:), indmat2(:,:)
    real*8, allocatable  :: indmtmp(:), indntmp(:), BdataAtmp(:,:)
    
    ! output variables
    type(rhoType)        :: B
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    smistat = FRC%smi%status
    
    if (.not. mpistat) then
        B = A
        B%mblock = mb2
        B%nblock = nb2
        B%mproc  = mp2
        B%nproc  = np2
        return
        !exit
    end if
    
    !mb1 = A%mblock
    !nb1 = A%nblock
    !mp1 = A%mproc
    !np1 = A%nproc
    
    !if (mb1 == mb2 .and. nb1 == nb2 .and. mp1 == mp2 .and. np1 == np2) then
    !    B = A
    !    !exit
    !    return
    !end if
    
    !if (issp == -1) then
    !    call ModBCDist(FRC,A,mb2,nb2,mp2,np2,.false.,.FALSE.,B) 
    !    !exit
    !    return
    !end if
    
    !m = A%m
    !n = A%n
    !call initDistArray(m,n,mb2,nb2,mp2,np2,B)
    !call distmat_isreal(FRC,A,isrl)
   ! 
    !if (smistat .and. (.not. issp) .and. (.not. isrl)) then
    !    psilin            =  A
    !    !psilin%dataA(:,1) =  real(A%dataA)
    !    !psilin%dataA(:,2) =  imag(A%dataA)
    !    call ModBCDist(FRC,psilin,mb2,nb2,mp2,np2,.TRUE.,.FALSE.,B)
    !    call GetGlobalInd(mpirank,B,ia,ja)
    !    !forall(ii = 1:2:)
    !    tmp1 = B%dataA(1::2)
    !    tmp2 = B%dataA(2::2)
    !    tmp1 = tmp1 + tmp2
    !    allocate(BdataAtmp(size(ia),size(ja)))
    !    BdataAtmp = reshape(tmp1,(/size(ia),size(ja)/))
    !else if (smistat .and. (.not. issp) .and. iscomplex) then
    !    call GetGlobalInd(mpirank,B)
    !    B%dataA = 0
    !    !call MPI_Barrier
    !    !call smiZGEMR2D(A,B)
    !    !call MPI_Barrier
    !    !exit
    !    return
    !else if (smistat .and. issp) then
    !    call GetGlobalInd(mpirank,B,ia,ja)
    !    B%dataA = 0
    !    !call MPI_Barrier
    !    !call smiZGEMR2D(A,B)
    !    !call MPI_Barrier
    !    !exit
    !    return
    !else
    !    call GetGlobalInd(mpirank,B,ia,ja)
    !    
    !    allocate(tmpMPI1(mpisize,1),tmpMPI2(1,mpisize))
    !    forall (ii = 1:mpisize)
    !        tmpMPI1(ii,1) = ii
    !        tmpMPI1(1,ii) = ii
    !    end forall
    !    
    !    call bsxfunMinus(tmpMPI1,tmpMPI2,indmat)
    !    call flipud(indmat,indmat1)
    !    allocate(indmat2(mpisize,mpisize))
    !    forall(ii = 1:mpisize)
    !        indmat2(ii,ii) = mpisize
    !    end forall
    !    call fliplr(indmat2,indmat2)
    !    call triu(indmat1-indmat2,indmat)
    !    do ii = 1,mpisize,1
    !        do jj = 1,mpisize,1
    !            if (indmat(ii,jj) /= 0) then
    !                indmat(ii,jj) = mpisize - mod(-indmat(ii,jj),real(mpisize))
    !            end if
    !        end do
    !    end do
    !    
    !    call GetGlobalInd(mpirank,A,i1,j1)
    !    call GetGlobalInd(mpirank,B,i2,j2)
    !    call ismember(i1,i2,iloc1)
    !    call ismember(j1,j2,jloc1)
    !    call ismember(i2,i1,iloc2)
    !    call ismember(j2,j1,iloc2)
    !    
    !    if (any(iloc1 == .TRUE.) .and. any(jloc1 == .TRUE.) .and. any(iloc2 == .TRUE.) .and. any(jloc2 == .TRUE.)) then
    !        BdataAtmp(iloc2,jloc2) = AdataAtmp(iloc1,jloc1)
    !    end if
    !    
    !    do ii = 1,mpisize,1
    !        ntmp = 0
    !        do jj = 1,mpisize,1
    !            do kk = 1,mpisize,1
    !                if (indmat(jj,kk) == ii) then
    !                    ntmp = ntmp + 1
    !                end if
    !            end do
    !        end do
    !        allocate(indmtmp(ntmp),indntmp(ntmptmp))
    !        ntmp = 0
    !        do jj = 1,mpisize,1
    !            do kk = 1,mpisize,1
    !                if (indmat(jj,kk) == ii) then
    !                    ntmp          = ntmp + 1
    !                    indmtmp(ntmp) = ii
    !                    indntmp(ntmp) = jj
    !                end if
    !            end do
    !        end do
    !        
    !        ntmp = 0
    !        do jj = 1,ntmp,1
    !            if (indmtmp(jj) == mpirank+1 .or. indntmp(jj) == mpirank+1) then
    !                ntmp = ntmp+1
    !            end if
    !        end do
    !        allocate(Iind(ntmp))
    !        ntmp = 0
    !        do jj = 1,ntmp,1
    !            if (indmtmp(jj) == mpirank+1 .or. indntmp(jj) == mpirank+1) then
    !                ntmp       = ntmp+1
    !                Iind(ntmp) = jj
    !            end if
    !        end do
    !        
    !        allocate(indm(ntmp),indn(ntmp))
    !        forall(jj=1:ntmp)
    !            indm(jj) = indmtmp(Iind(jj))-1
    !            indn(jj) = indntmp(Iind(jj))-1
    !        end forall
    !        
    !        if (ntmp /= 0) then
    !            if (mpirank == indn) then
    !                call GetGlobalInd(indn,A,i1,j1)
    !                call GetGlobalInd(indm,B,i2,j2)
    !                call ismember(i1,i2,iloc1)
    !                call ismember(j1,j2,jloc1)
    !                call GetGlobalInd(indm,A,i1,j1)
    !                call GetGlobalInd(indn,B,i2,j2)
    !                call ismember(i2,i1,iloc2)
    !                call ismember(j2,j1,jloc2)
    !                if (any(iloc1 == .TRUE.) .and. any(jloc1 == .TRUE.)) then
    !                    call MPI_Send_variable(A%dataA(iloc1,jloc1),indm,0)
    !                end if
    !                if (any(iloc1) .and. any(jloc1)) then
    !                    call MPI_Recv_variable(indm,0,B%dataA(iloc2,jloc2))
    !                end if
    !            end if
    !            
    !            if (mpirank == indm) then
    !                call GetGlobalInd(indm,A,i1,j1)
    !                call GetGlobalInd(indn,B,i2,j2)
    !                call ismember(i1,i2,iloc1)
    !                call ismember(j1,j2,jloc1)
    !                call GetGlobalInd(indn,A,i1,j1)
    !                call GetGlobalInd(indm,B,i2,j2)
    !                call ismember(i2,i1,iloc2)
    !                call ismember(j2,j1,jloc2)
    !                if (any(iloc1) .and. any(jloc1)) then
    !                    B%dataA(iloc2,jloc2) = MPI_Recv_variable(indm,0)
    !                end if
    !                if (any(iloc1) .and. any(jloc1)) then
    !                    call MPI_Send_variable(A%dataA(iloc1,jloc1),indm,0)
    !                end if
    !            end if
    !        end if
    !    end do
    !end if
    
    return
    end subroutine ModBCDist_rho
    end module ModBCDist_rho_module