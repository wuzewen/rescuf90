! RHODM_type.f90
    
!***********************************************
    !
    ! This is a data type to store RHODM data
    !
!***********************************************
    
    module RHODM_type
    
    type :: RHODMtype
        real*8, allocatable :: vata(:,:)
        integer             :: m
        integer             :: n
        integer             :: mblock
        integer             :: nblock
        integer             :: mproc
        integer             :: nproc
    end type RHODMtype
    
    end module RHODM_type