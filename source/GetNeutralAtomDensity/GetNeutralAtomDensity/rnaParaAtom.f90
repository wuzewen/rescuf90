! rnaParaAtom.f90
    
!******************************************************
    !
    !
    !
!******************************************************
    
    module rnaParaAtom_module
    contains
    subroutine rnaParaAtom(FRC,rho,rhodm)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use RHO_type
    use calcFilling_module
    use InterpRdist2Cart_module
    use A_module
    use ModBCDist_module
    use ModBCDist_2D_module
    use Acell_type
    use VataMN_2D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical :: mpistat
    integer :: mpirank, mpisize, ispin, fgridn(3), fn, natom, typeE
    integer :: ii, lqn
    real*8  :: lvec(3,3), pos(1,3), fillFrac, mgm, theta, phi, val
    
    real*8,  allocatable :: magmom(:,:), rrdata(:), rhoData(:)
    integer, allocatable :: lmind(:)
    real*8,  allocatable :: rhodmtmp(:,:), rhoVata(:,:), rhovatatmp(:), nonesne(:,:)
    type(VataMN_2D)         :: rhott, rhodmtt
    type(AcellType), allocatable :: rhotmp(:,:)
    
    ! output variables
    type(VataMN_2D) :: rho, rhodm
    
    !type(RHODMtype) :: rhodm
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    fgridn  = FRC%domain%fgridn
    fn      = product(fgridn)
    natom   = FRC%atom%numberOfAtom
    lvec    = FRC%domain%latvec
    
    call calcFilling(FRC,fillFrac)
    
    allocate(rhoVata(fn,1))
    !allocate(rhotmp(1,1)%vata(fn,1))
    !rhotmp(1,1)%vata(:,1) = 0
    rhoVata               = 0
    
    if (ispin == 1) then
        allocate(rhodmtmp(fn,1))
        rhodmtmp = 0
        rhoVata  = 0
    else if (ispin == 2) then
        magmom   = FRC%atom%magmom
        lmind    = FRC%atom%element
        allocate(rhodmtmp(fn,2))
        rhodmtmp  = 0
        rhoVata   = 0
    else if (ispin == 4) then
        magmom = FRC%atom%magmom
        lmind  = FRC%atom%element
        allocate(rhodmtmp(fn,4))
        rhodmtmp  = 0
        rhoVata   = 0
    end if
    
    do ii = mpirank+1,natom,mpisize
        typeE    = FRC%atom%element(ii)
        pos(1,:) = FRC%atom%XYZ(ii,:)
        allocate(rrData(FRC%SOB(typeE)%SizeOfRnarrData))
        allocate(rhoData(FRC%SOB(typeE)%SizeOfRnarhoData))
        rrData   = FRC%ElementData(typeE)%Rna%rrData
        rhoData  = FRC%ElementData(typeE)%Rna%rhoData
        lqn      = -1
        call InterpRdist2Cart(rrData,rhoData,pos,lqn,lvec,fgridn,.FALSE.,rhotmp,nonesne,rhovatatmp)
        rhoVata(:,1)  = rhoVata(:,1) + rhovatatmp   !rhotmp(1,1)%vata
        
        if (ispin == 1) then
            rhodmtmp(:,1) = rhodmtmp(:,1) + rhovatatmp*fillFrac
        else if (ispin == 2) then
            mgm           = magmom(ii,1)
            rhodmtmp(:,1) = rhodmtmp(:,1) + 0.5*(1+mgm)*rhovatatmp*fillFrac
            rhodmtmp(:,2) = rhodmtmp(:,2) + 0.5*(1-mgm)*rhovatatmp*fillFrac
        else if (ispin == 4) then
            mgm              = magmom(ii,1)
            theta            = magmom(ii,2)
            phi              = magmom(ii,3)
            val              = FRC%element(lmind(ii))%valence
            rhodmtmp(:,1)    = rhodmtmp(:,1) + rhovatatmp*fillFrac
            rhotmp(1,1)%vata = mgm*rhotmp(1,1)%vata*fillFrac/val
            rhodmtmp(:,2)    = rhodmtmp(:,2) + sin(theta)*rhovatatmp*cos(phi)
            rhodmtmp(:,3)    = rhodmtmp(:,3) + sin(theta)*rhovatatmp*sin(phi)
            rhodmtmp(:,4)    = rhodmtmp(:,4) + cos(theta)*rhovatatmp
        end if
        deallocate(rrData,rhoData,rhovatatmp,rhotmp,nonesne)
    end do
    if (mpistat) then
        
        write(*,*) "Error in RnaParaAtom.f90. mpi is not available now."
        stop
        
        !call MPI_Allreduce_sum(rho)
    end if 
    
    call InitDistArray_2D(fn,1,fn,1,1,mpisize,rho)
    allocate(rho%vata(size(rhoVata,1),size(rhoVata,2)),rhott%vata(size(rhovata,1),size(rhoVata,2)))
    rho%vata = rhoVata
    rhott     = rho
    !rho         = rhott
    call ModBCDist_2D(FRC,rhott,fn/mpisize,1,mpisize,1,.FALSE.,.FALSE.,rho)
    
    if (mpistat) then
        
        write (*,*) "Error In RnaParaAtom.f90. mpi is not available now."
        !call MPI_Allreduce_sum(rhodm,rhodm)
    end if
    
    if (ispin == 1) then
        call initDistArray_2D(fn,1,fn,1,1,mpisize,rhodm)
        allocate(rhodm%vata(size(rhodmtmp,1),size(rhodmtmp,2)),rhodmtt%vata(size(rhodmtmp,1),size(rhodmtmp,2)))
        rhodm%vata  = rhodmtmp
        rhodmtt     = rhodm
        call ModBCDist_2D(FRC,rhodmtt,fn/mpisize,1,mpisize,1,.FALSE.,.FALSE.,rhodm)
    else if (ispin == 2) then
        
        write(*,*) "Error in rnaParaAtom. spin system is not available now."
        stop
        
        !call initDistArray(fn,2,fn,2,1,mpisize,rhodm,rhodmtmp)
        !call ModBCDist(FRC,rhodmtmp,fn/mpisize,1,mpisize,1,rhodm)
    else if (ispin == 4) then
        
        write(*,*) "Error in rnaParaAtom. spin system is not available now."
        stop
        
        !call initDistArray(fn,4,fn,4,1,mpisize,rhodm,rhodmtmp)
        !call ModBCDist(FRC,rhodmtmp,fn/mpisize,1,mpisize,1,rhodm)
    end if
    
    return
    end subroutine rnaParaAtom
    end module rnaParaAtom_module