!  totalEnergy.f90 
!****************************************************************************
!
!  PROGRAM: totalEnergy
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module totalEnergy_module
    contains
    subroutine totalEnergy()

    implicit none

    ! input variables
    
    ! temporary variables
    
    ! output variables
    

    ! Body of totalEnergy
    fn  = product(fgirdn)
    call det(latvec,vol)
    vol = abs(vol)
    dr  = vol/fn
    
    if (mixDensity) then
        rho = rho%input(iter+1)
    else
        rho = rho%input(iter)
    end if
    
    call ModBCDist()
    
    if (ispin==1 .or. ispin==2) then
        rhotot = sum(rho,2)
    else if (ispin==4) then
        rhotot = rho(:,1)
        call eyem(2,sig1)
        sig2(1,2) = 1
        sig2(2,1) = 1
        sig3(1,2) = (0,-1)
        sig3(2,1) = (0,1)
        sig4(1,1) = 1
        sig4(2,2) = -1
        rhodm     = rho
        call bsxfunTimes()
        rho       = rho+rhotmp
        call bsxfunTimes()
        rho       = rho+rhotmp
        call bsxfunTimes()
        rho       = rho+rhotmp
        rho       = rho(:,(/1,3,2,4/))/2
    end if
    
    call ModBCDist()
    rna = 
    call ModBCDist()
    rpc = 
    call ModBSDist()
    nxc = 
    call ModBCDist()
    vdh = 
    call ModBCDist()
    vxc = 
    
    Ebs = Energy%Eks(iter)
    
    if (size(rhotot)) then
        Edh = -0.5*dr*(transpose(vdh)*(rhotot+rna))
        Edx = dr*matmul(transpose(nxc),rho+rna)
        do ii = 1,ispin,1
            Edx = Edx-matmul(transpose(vxc(:,ii)),rho(:,ii))*dr
        end do
    else
        Edh = 0
        Edx = 0
    end if
    
    if (mpistat) then
        call MPI_Allreduce_sum(Edh,Edh)
        call MPI_Allreduce_sum(Edx,Edx)
    end if
    
    Esr = Energy%Esr
    Energy%Etot(iter) = Ebs+Edh+Edx+Esr
    Energy%Ftot(iter) = Energy%Etot(iter)-Energy%entroy
    Energy%Exc(iter)  = Edx
    Energy%Edh(iter)  = Edh

    return
    end subroutine totalEnergy
    end module totalEnergy_module

