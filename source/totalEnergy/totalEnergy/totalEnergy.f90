! totalEnergy.f90
!********************************
    !
!********************************
    
    module totalEnergy_module
    contains
    subroutine totalEnergy()
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    if (mixDensity) then
        rho = rho%input(iter+1)
    else
        rho = rho%output(iter)
    end
    
    fgridn = domain%fgridn
    fn     = product(fgridn)
    call det(domain%latvec,vol)
    vol    = abs(vol)
    dr     = vol/fn
    
    call ModBCDist()
    call ModBCDist()
    call ModBCDist()
    call ModBCDist()
    call ModBCDist()
    
    rho = rho%dataRHO
    
    if (ispin==1 .or. ispin==2) then
        rhotot = sum(rho,2)
    else if (ispin==4) then
        rhotot = rho(:,1)
        sig1(1,1) =  1
        sig1(2,2) =  1
        sig2(1,2) =  1
        sig2(2,1) =  1
        sig3(1,2) = -(0,1)
        sig3(2,1) =  (0,1)
        sig4(1,1) =  1
        sig4(2,2) = -1
        rhodm     =  rho
        call bsxfunTimes(rhodm(:,1),sig1,rho)
        call bsxfunTimes(rhodm(:,2),sig2,rhotmp)
        rho = rho+rhotmp
        call bsxfunTimes(rhodm(:,3),sig3,rhotmp)
        rho = rho+rhotmp
        call bsxfunTimes(rhodm(:,4),sig4,rhotmp)
        rho = rho+rhotmp
        rho = rho(:,(/1,3,2,4/))/2
    end if
    
    rpc = rpc%dataRPC
    Exc = Exc%dataEXC
    VH  = VH%dataVH
    Vxc = Vxc%dataVXC
    
    if (size(rhotot)) then
        Edh = -0.5*matmul(transpose(VH),rhotot)*dr
        Exc = matmul(transpose(Exc),(rhotot+rpc))*dr
        do ii = 1,ispin,1
            Exc = Exc-matmul(transpose(Vxc(:,ii)),rho(:,ii))*dr
        end do
    else
        Edh = 0
        Exc = 0
    end
    
    if (mpistat) then
        call MPI_Allreduce_sum(Edh,Edh)
        call MPI_Allreduce_sum(Exc,Exc)
    end if
    
    Eks    = Energy%Eks(iter)
    IonIon = Energy%ionion
    alphaZ = Energy%alphaZ
    
    Energy%Etot(iter) = Eks+Edh+Exc+IonIon+alphaZ
    Energy%Ftot(iter) = Energy%Etot(iter)-Energy%entroy
    Energy%Eks(iter)  = Eks
    Energy%Exc(iter)  = Exc
    Energy%Edh(iter)  = Edh

    return
    end subroutine totalEnergy
    end module totalEnergy_module
