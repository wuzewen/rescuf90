!  genNeutralAtomPotential.f90 
!****************************************************************************
!
!  PROGRAM: genNeutralAtomPotential
!
!  PURPOSE:  Get Neutral Atom Potential from .bas file 
!
!****************************************************************************

    module genNeutralAtomPotential_module
    contains
    !subroutine genNeutralAtomPotential(fgridn,element,latvec,)
    subroutine genNeutralAtomPotential(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use A_module
    use getGridPointCoord_module
    use interpSph2Cart_module
    use VataMN_1D_module
    !use InterpRdist2Cart_module
    !use MPI_reduce_sum_module
    use ModBCDist_module
    !use MPI_allreduce_sum
    
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION)       :: FRC
    
    ! temporery variables
    integer              :: mpisize, mpirank, fgridn(3), ntmp, ii, jj
    integer              :: fn, natom, Etype, nelem, bc(3)
    integer, allocatable :: inde(:), itmp(:), indep(:,:)
    logical              :: mpistat
    
    real*8               :: lvec(3,3), pos(1,3), bvec(3,3), pi
    real*8, allocatable  :: GridCoord(:,:), rrData(:), vvData(:), ku(:), kv(:), kw(:), vnaDatatmp(:)
    real*8, allocatable  :: kuvw(:,:), tmp(:,:,:), pptmp(:,:,:), vnatt(:)
    type(VataMN_1D)      :: vna, vnatmp
    complex              :: im
    ! output variables
    

    ! Body of genNeutralAtomPotential
    pi      = 3.14159265354
    im      = (0,1)
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    fgridn  = FRC%domain%fgridn
    natom   = FRC%atom%numberOfAtom
    lvec    = FRC%domain%latvec
    fn      = product(fgridn)
    !natom   = size(element,1)
    if (FRC%potential%fourierinit .or. FRC%option%initParaReal) then
        allocate(GridCoord(fn,3))
        ntmp = fn/mpisize
        call GetGridPointCoord(FRC,fgridn,ntmp,GridCoord,indep)
        
        !write(*,*) "ntmp"
        !write(*,*)  ntmp
        !write(*,*) "fgridn"
        !write(*,*)  fgridn
        !write(*,*) "GridCoord in getNeutralAtomPotential case one"
        !do ii = 1,fn,1
        !    write(*,*)  GridCoord(ii,:)
        !end do
        
        !write(*,*) "Indep in getNeutralAtomPotential case one"
        !write(*,*)  indep
        
        call initDistArray_1D(fn,1,ntmp,1,mpisize,1,vna)
        
        !write(*,*) "vna after initDistArray."
        !write(*,*)  vna%dataA
        !write(*,*)  vna%m
        !write(*,*)  vna%n
        !write(*,*)  vna%mblock
        !write(*,*)  vna%nblock
        !write(*,*)  vna%mproc
        !write(*,*)  vna%nproc
        
        
    end if
    
    allocate(vna%vata(fn))

    !write(*,*) "natom"
    !write(*,*)  natom
    
    
    if (FRC%option%initParaReal) then
        vna%vata = 0
        do ii = 1,natom,1
            Etype    = FRC%atom%element(ii)
            pos(1,:) = FRC%atom%XYZ(ii,:)
            
            !write(*,*) "FRC%SOB(Etype)%SizeOfVnarrData"
            !write(*,*)  FRC%SOB(Etype)%SizeOfVnarrData
            
            allocate(rrData(FRC%SOB(Etype)%SizeOfVnarrData))
            allocate(vvData(FRC%SOB(Etype)%SizeOfVnavvData))
            rrData  = FRC%ElementData(Etype)%Vna%rrData
            vvData  = FRC%ElementData(Etype)%Vna%vvData
            
            !write(*,*) rrdata
            !write(*,*) vvdata
            
            bc      = 1
            allocate(vnaDatatmp(fn))
            
            !write(*,*) ii, "loop"
            
            call InterpSph2Cart(rrData,vvData,pos,-1,GridCoord,lvec,bc,vnaDatatmp)
            !ntmp = size(vnaDatatmp,1)
            !allocate(vnatt(fn))
            !forall(jj=1:ntmp)
            !    vnatt(jj) = vnaDatatmp(jj)
            !end forall
            
            
            !write(*,*) "vnatt in genNeutralAtomPotential.f90"
            !write(*,*)  vnatt
            
            vna%vata = vna%vata + vnaDatatmp !vnatt
            deallocate(rrData,vvData,vnaDatatmp)
            !deallocate(vnaDatatmp,vnatt)
        end do
    else if (.not. FRC%potential%fourierinit) then
        
        write(*,*) "FRC%potential%fourierinit = .FALSE. is not avalable. Please use FRC%option%initParaReal = .TRUE."
        stop
        
        !vna%DataA = 0
        !do ii = mpirank+1,natom,mpisize
        !    Etype = FRC%atom%element(ii)
        !    pos(1,:) = FRC%atom%XYZ(ii,:)
        !    allocate(rrData(FRC%SOB(ii)%SizeOfVnarrData))
        !    allocate(vvData(FRC%SOB(ii)%SizeOfVnavvData))
        !    rrData  = FRC%ElementData(Etype)%Vna%rrData
        !    vvData  = FRC%ElementData(Etype)%Vna%vvData
        !    call InterpRdist2Cart(rrData,vvData,pos,-1,lvec,fgridn,vnaDatatmp)
        !    vna%DataA = vna%DataA + vnaDatatmp
        !    deallocate(rrData,vvData)
        !end do
        
        !if (mpistat) then
        !    call MPI_Reduce_sum(vna%DataA,vna%DataA)
        !end if
        
        !if (mpirank /=0) then
        !    vna%dataA = 0
        !end if
        
        !call InitDistArray(fn,1,fn,1,1,mpisize,vna)
        !call ModBCDist(FRC,fn/mpisize,1,mpisize,1,vna)
    else
        !####################################
        !
        ! This part will never be used.
        !
        !####################################
        
        write(*,*) "Error in genNeutralAtomPotential. FRC%potential%fourierinit should be true. This part may never be avalable."
        stop
        
        !fgridn = FRC%domain%fgridn
        !call inversion(lvec,bvec)
        !bvec = 2*pi*transpose(bvec)
        !call fftFreq(fgridn,bvec,ku,kv,kw)
        !allocate(inde())
        !call GetGlobalInd(mpirank,vna,inde)
        !ntmp = size(inde)
        !allocate(kuvw(ntmp,3))
        !forall(ii=1:ntmp)
        !    kuvw(ii,1)  = ku(inde(ii))
        !    kuvw(ii,2)  = kv(inde(ii))
        !    kuvw(ii,3)  = kw(inde(ii))
        !end forall
        
        !nelem      = FRC%atom%numberOfElement
        !vna%DataA  = 0
        !vnatmp     = vna
        
        !do ii = 1,nelem,1
        !    allocate(rrData(FRC%SOB(ii)%SizeOfVnarrData))
        !    allocate(vvData(FRC%SOB(ii)%SizeOfVnavvData))
        !    rrData = FRC%ElementData(ii)%Vna%rrData
        !    vvData = FRC%ElementData(ii)%Vna%vvData
        !    call InterpSph2Cart(rrData,vvData,(/0,0,0/),-1,GridCoord,lvec,vnaDatatmp)
        !    if (mpistat) then
        !        call ModBCDist(FRC,vnatmp,fn,1,mpisize,1,vnatmp)
        !    end if
            
        !    if (mpirank == 0) then
        !        allocate(tmp(fgridn(1),fgridn(2),fgridn(3)))
        !        tmp = reshape(vnatmp%dataA,(/fgridn(1),fgridn(2),fgridn(3)/))
        !        call fftn(tmp,vnatmp%dataA)
        !    end if
            
        !    if (mpistat) then
        !        call ModBCDist(FRC,vnatmp,fn/mpisize,1,mpisize,1,vnatmp)
        !    end if
        !    
        !    ntmp = 0
        !    do jj = 1,nelem,1
        !        if (FRC%atom%element(jj) == ii) then
        !            ntmp = ntmp+1
        !        end if
        !    end do
        !    allocate(itmp(ntmp))
        !    ntmp = 0
        !    do jj = 1,nelem,1
        !        if (FRC%atom%element(jj) == ii) then
        !            ntmp = ntmp+1
        !            itmp(ntmp) = jj
        !        end if
        !    end do
        !    
        !    do jj = 1,ntmp,1
        !        pos(1,:)  = FRC%atom%XYZ(itmp(jj),:)
        !        vna%dataA = vna%dataA + exp(-im*(matmul(kuvw,transpose(pos))))*vnatmp%dataA
        !    end do
        !    deallocate(rrData,vvData)
        !end do
        
        !call ModBCDist(FRC,vna,fn,1,1,mpisize)
        
        !if (mpirank == 0) then
        !    vna%dataA(1) = 0
        !    allocate(pptmp(fgridn(1),fgridn(2),fgridn(3)))
        !    call ifftn(reshape(vna%dataA,(/fgridn(1),fgridn(2),fgridn(3)/)),pptmp)
        !    vna%dataA    = real(pptmp)
        !end if
        
        !call ModBCDist(FRC,vna,fn/mpisize,1,mpisize,1,vna)
    end if
    FRC%potential%vna = vna
                
    return
    end subroutine genNeutralAtomPotential
    end module genNeutralAtomPotential_module

