! getTrans.f90
    
!************************************************
    !
    !
    !
!************************************************
    
    module getTrans_module
    contains
    subroutine getTrans(nvec,tx,ty,tz,txout,tyout,tzout)
    
    implicit none
    
    ! input variables
    integer :: nvec(3)
    real*8, allocatable :: tx(:), ty(:), tz(:)
    
    ! temporary variables
    integer :: n, ii
    
    ! output variables
    real*8, allocatable :: txout(:), tyout(:), tzout(:)
    
    ! body of this function
    n = size(tx)
    allocate(txout(n),tyout(n),tzout(n))
    do ii = 1,n,1
        txout(ii) = floor(real(tx(ii))/real(nvec(1))-0.5/real(nvec(1)))
        tyout(ii) = floor(real(ty(ii))/real(nvec(2))-0.5/real(nvec(2)))
        tzout(ii) = floor(real(tz(ii))/real(nvec(3))-0.5/real(nvec(3)))
    end do
    
    return
    end subroutine getTrans
    end module getTrans_module