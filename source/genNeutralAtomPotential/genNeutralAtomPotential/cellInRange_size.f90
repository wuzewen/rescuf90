! cellInRange_size.f90
    
!**********************************************************
    !
    ! The function is used to determine the size of the
    ! output in function cellInRange.
    !
!**********************************************************
    
    module cellInRange_module
    contains
    subroutine cellInRange(avec,cartxyz,rad,issym,n)
    
    implicit none
    
    ! input variables
    real*8  :: avec(3,3), cartxyz(1,3), rad
    logical :: issym 
    
    ! temporary variables
    real*8  :: avecinv(3,3), redxyz(1,3), bvec(3,3), nrbv(3,1), proj(3,1), projtmp(3,1)
    real*8  :: tmp1(1,3), tmp2(1,3), boundtmp(2,3), 
    integer :: nn, mm, boundm(2,3), boundn
    integer :: nx, ny, nz
    
    ! output variables
    integer :: n
    
    ! body of this function
    call inversion(avec,avecinv)
    redxyz = matmul(cartxyz,avecinv)
    call inversion(transpose(avec),bvec)
    nrbv = sqrt(sum(bvec**2,2))
    call bsxfunRdivide(bvec,nrbv,bvec)
    proj = sum(bvec*avec,2)
    proj = abs(projtmp)
    proj = rad/projtmp
    
    nn   = size(redxyz,1)
    mm   = size(redxyz,2)
    forall(ii=1:mm)
        tmp1(1,ii) = minval(redxyz(:,ii))
        tmp2(1,ii) = maxval(redxyz(:,ii))
    end forall
    
    boundtmp(1,:) = tmp1-proj
    bouddtmp(2,:) = tmp2+proj
    boundm        = floor(boundtmp)
    
    if (issy,) then
        boundn      = maxval(abs(boundm))
        boundm(1,:) = -boundn
        boundm(2,:) =  boundn
    end if
    
    nx = boundm(2,1)-boundm(1,1)+1
    ny = boundm(2,2)-boundm(1,2)+1
    nz = boundm(2,3)-boundm(1,3)+1
    n  = nx*ny*nz
    
    return
    end subroutine cellInRange
    end module cellInRange_module