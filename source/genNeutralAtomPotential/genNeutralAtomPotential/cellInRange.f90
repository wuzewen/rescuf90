! cellInRange.f90
    
!**********************************************************
    !
    ! The input variable issym should be given before 
    ! this function be called. while in matlab version,
    ! it can generate issym inside the function. issym's
    ! defalt value is .false.. 
    ! Notice that, the ouput bound's size is determined
    ! inside this function. Not outside.
    !
!**********************************************************
    
    module cellInRange_module
    contains
    subroutine cellInRange(avec,cartxyz,rad,issym,bound)
    
    use inversion_module
    use bsxfunRdivide_module
    use ndgridA_module
    
    implicit none
    
    ! input variables
    !real*8               :: avec(3,3), cartxyz(1,3), rad
    real*8, allocatable  :: avec(:,:), cartxyz(:,:)
    real*8               :: rad
    logical              :: issym 
    
    ! temporary variables
    real*8               :: nptm(3), proj(1,3), projtmp(1,3) ! avecinv(3,3), bvec(3,3), 
    real*8               :: tmp1(1,3), tmp2(1,3), boundtmp(2,3)
    integer              :: nn, mm, boundm(2,3), boundn, ii
    real*8, allocatable  :: tx(:), ty(:), tz(:), redxyz(:,:)
    real*8, allocatable  :: avecinv(:,:), bvec(:,:), nrbv(:,:), bvecm(:,:)
    
    ! output variables
    real*8, allocatable  :: bound(:,:)
    
    ! body of this function
    allocate(avecinv(3,3),bvec(3,3))
    call inversion(avec,avecinv)
    mm = size(cartxyz,1)
    nn = size(cartxyz,2)
    allocate(redxyz(mm,nn))
    redxyz = matmul(cartxyz,avecinv)
    !call inversion(avec,bvec)
    bvec = transpose(avecinv)
    
    !write(*,*) "bvec in cellInRange"
    !write(*,*)  bvec
    
    !write(*,*) size((sum(bvec**2,2))**0.5)
    !write(*,*) size((sum(bvec**2,2))**0.5,2)
    
    
    nptm = (sum(bvec**2,2))**0.5
    
    
    
    allocate(nrbv(1,3))
    forall(ii=1:3)
        nrbv(1,ii) = nptm(ii)
    end forall
    
    !write(*,*) "nrbv in CellInRange"
    !write(*,*)  nrbv
    
    allocate(bvecm(3,3))
    call bsxfunRdivide(bvec,nrbv,bvecm)
    
    !write(*,*) "bvecm in cellInRange"
    !write(*,*)  bvecm
    
    nptm = sum(bvecm*avec,2)
    
    !write(*,*) "nptm in cellInRange"
    !write(*,*)  nptm
    
    forall(ii=1:3)
        proj(1,ii) = nptm(ii)
    end forall
    proj = abs(proj)
    proj = rad/proj
    
    !write(*,*) "proj in cellinrange"
    !write(*,*)  proj
    
   ! 
    nn   = size(redxyz,1)
    mm   = size(redxyz,2)
    forall(ii=1:mm)
        tmp1(1,ii) = minval(redxyz(:,ii))
        tmp2(1,ii) = maxval(redxyz(:,ii))
    end forall
    
    forall(ii=1:3)
        boundtmp(1,ii) = tmp1(1,ii)-proj(1,ii)
        boundtmp(2,ii) = tmp2(1,ii)+proj(1,ii)
    end forall
    
    !write(*,*) "boundtmp in cellinrange"
    !write(*,*)  boundtmp
   ! 
    boundm        = floor(boundtmp)
    
    !write(*,*) "boundm in cellinrange"
    !write(*,*)  boundm
    
    if (issym) then
        boundn      = maxval(abs(boundm))
        boundm(1,:) = -boundn
        boundm(2,:) =  boundn
    end if
    
    allocate(tx(boundm(2,1)-boundm(1,1)+1))
    allocate(ty(boundm(2,2)-boundm(1,2)+1))
    allocate(tz(boundm(2,3)-boundm(1,3)+1))
    forall(ii=boundm(1,1):boundm(2,1))
        tx(ii-boundm(1,1)+1) = ii
    end forall
    
    !write(*,*) size(tx)
    
    forall(ii=boundm(1,2):boundm(2,2))
        ty(ii-boundm(1,2)+1) = ii
    end forall
    forall(ii=boundm(1,3):boundm(2,3))
        tz(ii-boundm(1,3)+1) = ii
    end forall
    nn = (boundm(2,1)-boundm(1,1)+1)*(boundm(2,2)-boundm(1,2)+1)*(boundm(2,3)-boundm(1,3)+1)
    allocate(bound(nn,3))
    
    !write(*,*) "tx ty tz in CellInRange"
    !write(*,*)  tx
    !write(*,*)  ty
    !write(*,*)  tz
    call ndgridA(tx,ty,tz,bound)
    !deallocate(bound)
    
    return
    end subroutine cellInRange
    end module cellInRange_module