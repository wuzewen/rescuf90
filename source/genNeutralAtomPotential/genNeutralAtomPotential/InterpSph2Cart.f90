! InterpSph2Cart.f90
    
!******************************************************************************************
    !
    ! In this function, the input rrdata and frdata are numbers, not characters. Because
    ! A variable's content can't be turned to another variable's name in fortran. 
    ! rrdata and frdata should be assigned before this function be called.
    ! While, in matlab version, it is rfield and ffield instead of rrdata and frdata as 
    ! input. And they are characters.
    !
!******************************************************************************************
    
    module InterpSph2Cart_module
    contains
    subroutine InterpSph2Cart(rrdata,frdata,pos,lqn,gridcoord,avec,bc,ffgrid)
    
    use cellInRange_module
    use flipud_module
    use bsxfunPlus_module
    use interp1_module
    use genrealSH_module
    use bsxfunTimes_module
    
    implicit none
    
    ! input variables
    integer              :: bc(3), lqn
    real*8               :: pos(1,3), avec(3,3)
    real*8, allocatable  :: gridcoord(:,:)
    real*8, allocatable  :: rrdata(:), frdata(:)
    
    ! temporary variables
    character(len=20)    :: method
    integer              :: n, inde, ii, jj, nf, nr, nn, ntvec, ntmp, nnn
    real*8               :: eps, rmax
    logical              :: issym
    logical, allocatable :: nonzero(:)
    real*8, allocatable  :: txyz(:,:), txyzout(:,:), ffgrid(:), xtmp(:,:), dist(:), frgrid(:), disttmp(:)
    real*8, allocatable  :: Ylm(:,:), ffgridtmp(:,:), avectmp(:,:), tmp(:,:), xtmp1(:), xtmp2(:), xtmp3(:)
    real*8, allocatable  :: ftgrid(:,:), post(:,:)
    
    ! output variables
    
    ! body of this function
    eps     = 2.2204E-16
    n       = size(gridcoord,1)
    !call getDataFromChar_vna(rfield,dataA,rrdata)
    !call getDataFromChar_vna(ffield,dataA,frdata)
    !rrdata = dataA%(rfield)
    !frdata = dataA%(ffield)
    nr      = size(rrdata)
    nf      = size(frdata)
    allocate(avectmp(3,3))
    allocate(tmp(1,3))
    avectmp = avec
    
    !write(*,*) "nr, nf, n in InterpSph2Cart.f90"
    !write(*,*)  nr, nf, n, eps
    
    
    inde = 0
    
    
    !write(*,*) "ii in loop in InterpSph2Cart.f90"
    
    do ii = 1,nf,1
        if (abs(frdata(ii))>eps) then ! .and. abs(frdata(ii)) == 1) then
            inde = ii
        end if
        
        !write(*,*)  ii, frdata(ii)
        
    end do
    
    inde = inde + 1
    
    !write(*,*)  frdata(1799),frdata(1800),frdata(1801),frdata(1802),frdata(1803),frdata(1804),frdata(1805),frdata(1806)
    !write(*,*) "inde in InterpSph2Cart.f90"
    !write(*,*)  inde, nf, ii
    
    
    if (inde < nr) then
        frdata(inde) = 0
        !rrdata
        forall(ii=inde+1:nf)
            rrdata(ii) = 0
            frdata(ii) = 0
        end forall
        
        !write(*,*) "inde < nr"
        
        !frdata
    end if
    rmax = maxval(rrdata)
    
    if (any(bc>0)) then
        issym = .FALSE.
        !call cellInRange_size(avec,pos,rmax,issym,nn)
        !allocate(txyz(nn,3))
        
        !write(*,*) "avectmp in InterpSph2Cart.f90 before CellInRange"
        !write(*,*)  avectmp
        !write(*,*) "pos in InterpSph2Cart.f90 before CellInRange"
        !write(*,*)  pos
        !write(*,*) "rmax in InterpSph2Cart.f90 before CellInRange"
        !write(*,*)  rmax
        !write(*,*) "issym in InterpSph2Cart.f90 before CellInRange"
        !write(*,*)  issym
        
        !write(*,*) "nn"
        !write(*,*)  nn 
        allocate(post(1,3))
        post = pos
        call cellInRange(avectmp,post,rmax,issym,txyz)
        
        !write(*,*) "txyz in interpSph2Cart.f90"
        !write(*,*)  txyz
        nnn = size(txyzout,1)
        !allocate(txyzout(nnn,3))
        call flipud(txyz,txyzout)
        
        
        !write(*,*) "txyz in interpSph2Cart.f90"
        !nnn = size(txyzout,1)
        !do ii = 1,nnn,1
        !    write(*,*)  txyzout(ii,:)
        !end do
        
        
        txyz = matmul(-txyzout,avec)
        
        !write(*,*) "txyz in interpSph2Cart.f90"
        !nnn = size(txyz,1)
        !do ii = 1,nnn,1
        !    write(*,*)  txyz(ii,:)
        !end do
        
        
    else
        allocate(txyz(1,3))
        txyz = 0
    end if
    
    ntvec = size(txyz,1)
    
    ! this part should be outside of this function
    if (lqn < 0) then
        allocate(ftgrid(n,1))
        ftgrid = 0
    else
        allocate(ftgrid(n,2*lqn+1))
        ftgrid = 0
    end if
    
    allocate(xtmp(n,3))
    allocate(dist(n))
    do ii = 1,ntvec,1
        forall(jj=1:3)
            tmp(1,jj)  = -pos(1,jj)-txyz(ii,jj)
        end forall
        
        !write(*,*) "txyz(ii,jj)"
        !write(*,*)  txyz(ii,:)
        
        call bsxfunPlus(gridcoord,tmp,xtmp)
        
        !write(*,*) "xtmp"
        !nnn = size(xtmp,1)
        !do jj = 1,nnn,1
        !    write(*,*)  xtmp(jj,:)
        !end do
        !write(*,*)  xtmp
        
        
        forall(jj=1:n)
            dist(jj) = (xtmp(jj,1)**2+xtmp(jj,2)**2+xtmp(jj,3)**2)**0.5
            !dist = dist + xtmp(:,2)**2
            !dist = dist + xtmp(:,3)**2
            !
        end forall
        !dist = sqrt(dist)
        !write(*,*) "dist"
        !write(*,*)  dist
        
        if (minval(dist) <= rmax) then
            !do jj = 1,n,1
                !if (dist(jj) <= rmax)
            !allocate(nonzero(n))
            ntmp = 0
            do jj = 1,n,1
                if (dist(jj) <= rmax) then
                    ntmp = ntmp + 1
                end if
            end do
            allocate(disttmp(ntmp))
            ntmp = 0
            do jj = 1,n,1
                if (dist(jj) <= rmax) then
                    ntmp          = ntmp + 1
                    disttmp(ntmp) = dist(jj)
                end if
            end do
            !nonzero = dist <= rmax
            
            !write(*,*) "nonzero in interS"
            !write(*,*)  nonzero
            
            !allocate(frgrid(n,3))
            method  = "spline"
            !allocate(disttmp(n))
            !disttmp = dist(nonzero)
            allocate(frgrid(ntmp))
            
            !write(*,*) "loop in interpSph2Cart.f90"
            !write(*,*)  ii
            !write(*,*) "rrdata"
            !write(*,*)  minval(rrdata)
            !write(*,*)  maxval(rrdata)
            !write(*,*)  minval(disttmp)
            !write(*,*)  maxval(disttmp)
            
            call interp1(rrdata,frdata,disttmp,method,0,frgrid)
            deallocate(disttmp)
            if (lqn < 0) then
                ntmp = 0
                do jj = 1,n,1
                    if (dist(jj) <= rmax) then
                        ntmp = ntmp+1
                        ftgrid(jj,1) = ftgrid(jj,1)+frgrid(ntmp)
                    end if
                end do
                forall(jj=1:n)
                    ffgrid(jj) = ftgrid(jj,1)
                end forall
            else
                if (lqn == 1) then
                    allocate(Ylm(3,1))
                else if (lqn == 1) then
                    allocate(Ylm(3,3))
                else if (lqn == 2) then
                    allocate(Ylm(3,5))
                else if (lqn == 3) then
                    allocate(Ylm(3,7))
                end if
                allocate(xtmp1(n),xtmp2(n),xtmp3(n))
                forall(jj=1:n)
                    xtmp1(jj) = xtmp(nonzero(jj),1)
                    xtmp2(jj) = xtmp(nonzero(jj),2)
                    xtmp3(jj) = xtmp(nonzero(jj),3)
                end forall
                
                call GenRealSH(xtmp1,xtmp2,xtmp3,lqn,1,Ylm)
                !call bsxfunTimes(frgrid,Ylm,ffgridtmp)
                !ftgrid(nonzero,:) = ftgrid(nonzero,:)+ffgridtmp
            end if
            deallocate(frgrid)
        end if
        
    end do
    deallocate(txyz)
    
    return
    end subroutine InterpSph2Cart
    end module InterpSph2Cart_module
        