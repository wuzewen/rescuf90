! GenRealSH.f90
    
!*********************************
    !
    !
    !
!*********************************
    
    module GenRealSH_module
    contains
    subroutine GenRealSH(x,y,z,ll,mm,Ylm)
    
    use bsxfunRdivide_module
    
    implicit none
    
    ! input variables
    real*8, allocatable  :: x(:), y(:), z(:)
    integer              :: ll, mm
    
    ! temporary variables
    real*8               :: pi, tmpR, tmpR1, tmpR2, tmpR3
    integer              :: n, ntmp, itmp, ii
    real*8, allocatable  :: r(:), tmp(:,:), rtmp(:,:), rrtmp(:,:)
    integer, allocatable :: inde(:)
    
    ! output variables
    real*8, allocatable  :: Ylm(:,:)
    
    ! body of this function
    pi   = 3.14159265354
    n    = size(x)
    allocate(r(n))
    r    = sqrt(x**2+y**2+z**2)
    !allocate(inde(n))
    allocate(rtmp(n,1))
    rtmp(:,1) = r(:)
    
    ntmp = 0
    do ii = 1,n,1
        if (r(ii) == 0) then
            ntmp = ntmp+1
        end if
    end do
    allocate(inde(ntmp))
    ntmp = 0
    do ii = 1,n,1
        if (r(ii) == 0) then
            ntmp = ntmp+1
            inde(ntmp) = ii
        end if
    end do
    !inde = r == 0
    
    !write(*,*) "ll in genrealsh"
    !write(*,*)  ll
    
    select case (ll)
    case (0)
        
        !write(*,*) "case 0 in genrealsh"
        
        allocate(Ylm(n,1))
        forall(ii=1:n)
            Ylm(ii,1) = (sqrt(pi)**-1)/2
        end forall
        
    case (1)
        
        !write(*,*) "case 1 in genrealsh"
        
        allocate(tmp(n,3))
        allocate(Ylm(n,3))
        tmpR     = (3*(4*pi)**-1)**0.5
        tmp(:,1) = y*tmpR
        tmp(:,2) = z*tmpR
        tmp(:,3) = x*tmpR
        !tmp      = (3/4/pi)**0.5*tmp
        call bsxfunRdivide(tmp,rtmp,Ylm)
        do ii = 1,ntmp,1
            itmp        = inde(ii)
            Ylm(itmp,:) = 0
        end do
    case(2)
        
        !write(*,*) "case 2 in genrealsh"
        
        allocate(tmp(n,5))
        allocate(Ylm(n,5))
        tmp(:,1) = ((15/pi)**0.5)*x*y/2
        tmp(:,2) = ((15/pi)**0.5)*y*z/2
        tmp(:,3) = ((05/pi)**0.5)*(2*z**2-x**2-y**2)/4
        tmp(:,4) = ((15/pi)**0.5)*x*z/4
        tmp(:,5) = ((15/pi)**0.5)*(x**2-y**2)/4
        allocate(rrtmp(n,1))
        rrtmp = rtmp**2
        call bsxfunRdivide(tmp,rrtmp,Ylm)
        do ii = 1,ntmp,1
            itmp        = inde(ii)
            Ylm(itmp,:) = 0
        end do
    case(3)
        
        !write(*,*) "case 3 in genrealsh"
        
        allocate(tmp(n,7))
        allocate(Ylm(n,7))
        tmp(:,1) = ((35/2/pi)**0.5)*(3*x**2-y**2)*y/4
        tmp(:,2) = ((105/pi)**0.5)*(x*y*z)/2
        tmp(:,3) = ((21/2/pi)**0.5)*y*(4*z**2-x**2-y**2)/4
        tmp(:,4) = ((7/pi)**0.5)*z*(2*z**2-3*x**2-3*y**2)/4
        tmp(:,5) = ((21/2/pi)**0.5)*x*(4*z**2-x**2-y**2)/4
        tmp(:,6) = ((105/pi)**0.5)*(x**2-y**2)*z/4
        tmp(:,7) = ((35/2/pi)**0.5)*(x**2-3*y**2)*x/4
        allocate(rrtmp(n,1))
        rrtmp = rtmp**3
        call bsxfunRdivide(tmp,rrtmp,Ylm)
        do ii = 1,ntmp,1
            itmp        = inde(ii)
            Ylm(itmp,:) = 0
        end do
    end select
    
    return
    end subroutine GenRealSH
    end module GenRealSH_module