! unique.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    module unique_module
    contains
    subroutine unique(tx,ut)
    
    implicit none
    
    ! input variables
    integer, allocatable :: tx(:)
    
    ! temporary variables
    integer              :: n, ii, jj, ntmp, maxv
    integer, allocatable :: txtmp(:), uttmp(:), loca(:)
    
    ! output variables
    integer, allocatable :: ut(:)
    
    ! body of this function
    n = size(tx)
    if (n == 1) then
        allocate(ut(1))
        ut = tx
        return
    end if
    
    ntmp = 1
    do ii = 2,n,1
        allocate(txtmp(ii-1))
        forall(jj=1:ii-1)
            txtmp(jj) = tx(jj)
        end forall
        if (any(txtmp == tx(ii))) then
            ntmp = ntmp+0
        else
            ntmp = ntmp+1
        end if
    end do
    allocate(ut(ntmp),uttmp(ntmp))
    
    ntmp  = 1
    ut(1) = tx(1)
    do ii = 2,n,1
        allocate(txtmp(ii-1))
        forall(jj=1:ii-1)
            txtmp(jj) = tx(jj)
        end forall
        if (any(txtmp == tx(ii))) then
            ntmp = ntmp+0
        else
            ntmp     = ntmp+1
            ut(ntmp) = tx(ii)
        end if
    end do
    
    uttmp = ut
    
    maxv     = maxval(uttmp)
    ut(ntmp) = maxv
    allocate(loca(1))
    do ii = 1,ntmp-1,1
        loca        = minloc(uttmp)
        ut(ii)      = minval(uttmp)
        uttmp(loca) = uttmp(loca)+maxv
    end do
    
    return
    end subroutine unique
    end module unique_module