! distmat_isreal.f90

!*************************************
    !
    !
    !
!*************************************
    
    module distmat_isreal_module
    contains
    subroutine distmat_isreal(FRC,dA,isrl)
    
    use VataMN_2D_module
    use dataType_module
    use MPI_Allreduce_sum_module
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    logical        :: mpistat, isrl
    type(VataMN_2D)   :: dA
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    mpistat = FRC%mpi%status
    
    if (mpistat) then
        call MPI_Allreduce_sum(isrl)
    end if
    
    isrl = .not. isrl
    
    return
    end subroutine distmat_isreal
    end module distmat_isreal_module