! GetGlobalInd_1D.f90
    
!**********************************
    !
    !
    !
!**********************************
    
    module GetGlobalInd_1D_module
    contains
    subroutine GetGlobalInd_1D(mpirank,A,rowind,colind)
    
    use VataMN_1D_module
    use bsxfunPlus_module
    
    implicit none
    
    ! input variables
    integer :: mpirank
    type(VataMN_1D) :: A
    
    
    ! temporary variables
    integer              :: m, n, mb, nb, nprow, npcol, myrow, mycol, ii, nbrow, nbcol, ntmp, jj
    real*8, allocatable  :: tmp(:,:), tmp1(:,:), tmp2(:,:), tmp3(:,:), tmp4(:,:), tmptmp(:,:)
    real*8, allocatable  :: rowindtmp(:,:), colindtmp(:,:)
    
    ! output variables
    integer, allocatable :: rowind(:,:), colind(:,:)
    
    ! body of this fnction
    m     = A%m
    n     = A%n
    mb    = A%mblock
    nb    = A%nblock
    nprow = A%mproc
    npcol = A%nproc
    myrow = mpirank/npcol
    mycol = mod(mpirank,npcol)
    
    allocate(rowindtmp(mb,1))
    forall(ii=1:mb)
        rowindtmp(ii,1) = ii-1
    end forall
    rowindtmp = rowindtmp+myrow*mb
    
    nbrow  = m/mb/nprow
    allocate(tmp1(mb,1))
    allocate(tmp(mb,1))
    tmp1   = rowindtmp
    tmp    = rowindtmp
    allocate(tmp2(1,nbrow))
    forall(ii=1:nbrow)
        tmp2(1,ii) = ii-1
    end forall
    tmp2 = tmp2*mb*nprow
    
    deallocate(rowindtmp)
    allocate(rowindtmp(mb,nbrow))
    call bsxfunPlus(tmp1,tmp2,rowindtmp)
    
    tmp = tmp+nbrow*mb*nprow
    !tmp = tmp(tmp<m)
    ntmp = 0
    do ii = 1,mb,1
        if (tmp(ii,1)<m) then
            ntmp = ntmp+1
        end if
    end do
    allocate(tmptmp(ntmp,1))
    ntmp = 0
    do ii = 1,mb,1
        if (tmp(ii,1)<m) then
            ntmp           = ntmp+1
            tmptmp(ntmp,1) = tmp(ii,1)
        end if
    end do
    
    allocate(rowind(1,ntmp+mb*nbrow))
    do ii = 1,mb,1
        do jj = 1,nbrow,1
            rowind(1,(jj-1)*mb+ii) = rowindtmp(ii,jj)+1
        end do
    end do
    forall(ii=mb*nbrow+1:ntmp+mb*nbrow)
        rowind(1,ii) = tmptmp(ii-mb*nbrow,1)
    end forall
    
    !rowind(:,2) = tmp+1
    allocate(colindtmp(nb,1))
    forall(ii=1:nb)
        colindtmp(ii,1) = ii-1
    end forall
    colindtmp = colindtmp+mycol*nb
    nbcol     = n/nb/npcol
    
    allocate(tmp3(nb,1))
    deallocate(tmp)
    allocate(tmp(nb,1))
    tmp3 = rowindtmp
    tmp  = rowindtmp
    allocate(tmp4(1,nbcol))
    forall(ii=1:nbcol)
        tmp4(1,ii) = ii-1
    end forall
    tmp4 = tmp4*mb*npcol
    
    !allocate(colindtmp(nb,nbrow))
    call bsxfunPlus(tmp3,tmp4,colindtmp)
    
    tmp = tmp+nbcol*nb*npcol
    !tmp = tmp(tmp<n)
    ntmp = 0
    do ii = 1,nb,1
        if (tmp(ii,1)<n) then
            ntmp = ntmp+1
        end if
    end do
    deallocate(tmptmp)
    !allocate(tmp(ntmp,1))
    ntmp = 0
    do ii = 1,nb,1
        if (tmp(ii,1)<n) then
            ntmp           = ntmp+1
            tmptmp(ntmp,1) = tmp(ii,1)
        end if
    end do
    allocate(colind(1,ntmp+nb*nbcol))
    do ii = 1,nb,1
        do jj = 1,nbcol,1
            colind(1,(jj-1)*nb+ii) = colindtmp(ii,jj)+1
        end do
    end do
    forall(ii=nb*nbcol+1:ntmp+nb*nbcol)
        colind(1,ii) = tmptmp(ii-nb*nbcol,1)
    end forall
    
    !colind(:,1) = colindtmp+1
    !colind(:,2) = tmp+1
    
    return
    end subroutine GetGlobalInd_1D
    end module GetGlobalInd_1D_module