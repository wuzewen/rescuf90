! sub2ind.f90
    
!***************************************************
    !
    ! this function has the same function with
    ! the function sub2ind.m in matlab
    !
!***************************************************
    
    module sub2ind_module
    contains
    subroutine sub2ind(nvec,tx,ty,tz,inde)
    
    implicit none
    
    ! input variables
    integer              :: nvec(3)
    integer, allocatable :: tx(:,:,:), ty(:,:,:), tz(:,:,:)
    
    ! temporary variables
    integer              :: n1, n2, n3, ii, jj, kk, BIG
    
    ! output variables
    integer, allocatable :: inde(:,:,:)
    
    ! body of this function
    n1 = size(tx,1)
    n2 = size(tx,2)
    n3 = size(tx,3)
    allocate(inde(n1,n2,n3))
    
    forall(ii=1:n1,jj=1:n2,kk=1:n3)
        inde(ii,jj,kk) = (tz(ii,jj,kk)-1)*nvec(2)*nvec(1) + (ty(ii,jj,kk)-1)*nvec(1) + tx(ii,jj,kk)
    end forall
    
    BIG = product(nvec)
    
    !if (any(inde > BIG)) then
    !    write(*,*) "Error in sub2ind. Index beyond limit."
    !    stop
    !end if
    
    return
    end subroutine sub2ind
    end module sub2ind_module