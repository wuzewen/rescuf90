! interp1.f90
    
!********************************************************************
    !
    !
    !
!********************************************************************
    
    module interp1_module
    contains
    subroutine interp1(X,Y,dist,method,lqn,frgrid)
    
    use reshapeAndSortXandV_module
    use spline_module
    
    implicit none
    
    ! input variables
    real*8, allocatable  :: X(:), Y(:),dist(:)
    character(len=20)    :: method
    integer              :: lqn
    
    ! temporary variables
    integer              :: ii, m, nr, nf, nmin
    real*8               :: dy0, dye
    real*8, allocatable  :: Xtmp(:), Ytmp(:)
    
    ! output variables
    real*8, allocatable  :: frgrid(:)
    
    ! body of this function
    !write(*,*) "interp1"
    m  = size(dist)
    nr = size(X)
    nf = size(Y)
    allocate(Xtmp(nr),Ytmp(nf))
    call reshapeAndSortXandV(X,Y,Xtmp,Ytmp)
    !allocate(dist(m))
    if (any(dist<X(1)) .or. any(dist>X(nr))) then
        write(*,*) "There are points outside the range. In Function interp1."
        stop
    end if
    
    !call griddedInterpolant(X,V,method,F)
    
    if (trim(method) == "spline") then
        dy0 = 0
        dye = 0
        call spline(X,Y,dy0,dye,dist,frgrid)
    else! if (trim(method) == "linear") then
        do ii = 1,m,1
            nmin       = count(X<=dist(ii))
            frgrid(ii) = X(nmin)+(dist(ii)-X(nmin))*(Y(nmin+1)-Y(nmin))/(X(nmin+1)-X(nmin))
        end do
    end if
    
        
    
    !allocate(frgrid(m))
    !do ii = 1,m,1
    !    frgrid(ii) = F(dist(ii))
    !end do
    
    return
    end subroutine interp1
    end module interp1_module