! getDataFromChar.f90
    
!**************************************************************
    !
    ! Since the content of a variable can't be changed to a
    ! variable's name, there is this series of functions.
    !
!**************************************************************
    
    module getDataFromChar_module
    contains
    subroutine getDataFromChar(dataName,dataA,dataOut)
    
    use A_module
    use dataType_module
    
    implicit none
    
    ! input variables
    character(len=20) :: dataName
    type(Vna_type)    :: dataA
    
    ! temporary variables
    
    ! output variables
    real*8, allocatable :: dataOut
    
    ! body of this function
    
    select case(trim(dataName))
    case("rrdata")
        dataOut = dataA%rrdata
    case("drdata")
        dataOut = dataA%drdata
    case("vvdata")
        dataOut = dataA%vvdata
    end select
    
    return
    end subroutine getDataFromChar
    end module getDataFromChar_module