! InitDistArray.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    subroutine InitDistArray(M,N,MB,NB,MP,NP,A)
    
    use A_module
    
    implicit none
    
    ! input variables
    integer      :: M, N, MB, NB, MP, NP
    
    ! ouput variables
    type(A_type) :: A
    
    ! body of this function
    A%m      = M
    A%n      = n
    A%mblock = MB
    A%nblock = NB
    A%mproc  = MP
    A%nproc  = NP
    
    return
    end subroutine InitDistArray