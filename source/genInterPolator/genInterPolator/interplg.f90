! interplg.f90
!**************************************
    !
!**************************************
    
    module interplg_module
    contains
    subroutine interplg(x,y,xi,c,yi)
    
    implicit none
    
    ! input variables
    integer             :: c
    real*8,allocatable  :: x(:), xi(:), y(:)
    
    ! temporery variables
    
    
    ! output variables
    real*8,allocatable  :: yi(:)
    
    ! body of this function
    m = size(xi)
    n = size(x)
    if (n<c) then 
        write(*,*) 'error'
    end if
    
    
    allocate(yi(m),kklo(m),kkhi(m))
    forall (i=1:m)
        yi(i)   = 0
        kklo(i) = 1
        kkhi(i) = n
    end forall
    
    do while (any(kkhi-kklo>1))
        k = floor((kkhi+kklo)/2)
        do i = 1,m,1
            if (x(k(i))>xi(i)) then
                kkhi(i) = k(i)
            else 
                kklo(i) = k(i)
            end if
        end do
    end do
    
    cc = c/2
    
    do i = 1,m,1
        if (mode(c,2)) then
            if (kklo(i)<cc+1) then
                kklo(i) = cc+1
            end if
        else
            if (kklo(i)<cc) then
                kklo(i) = cc
            end if
        end if
        if (kklo(i)>n-cc) then
            kklo(i) = n-cc
        end if
    end do
    
    allocate(tmp(m))
    if (mod(c,2)) then
        forall(i=1:m)
            tmp(i) = i-cc-1
        end forall
        k = kklo+tmp
    else
        forall(i=1:m)
            tmp(i) = i-cc
        end forall
        k = kklo+tmp
    end if
    
    forall(i=1:m)
        yy(i) = y(k(i))
    end forall
    aa = count(yy/=0)
    bb = 0
    do i = 1,m,1
        if (yy(i)/=0) then
            bb = bb+1
            aaa(bb) = i
        end if
    end do
    
    forall(i=1:m)
        cp(i) = k(y(k(i)))
    end forall
    
    do i = 1,aa,1
        klo = kklo(aaa(i))
        if (mod(c,2)) then
            forall(j=1:tt)
                mi(tt) = klo+i-1
            end forall
        else
            forall(j=1:tt)
                mi(tt) = klo+i
            end forall
        end if
        jj = aaa(i)
        kk = cp(i)
        yi(jj) = 1
        do j = 1,tt,1
            if (mi(j)/=kk) then
                yi(jj) = yi(jj)*(xi(jj)-x(mi(j)))/(x(kk)-x(mi(j)))
            end if
        end do
    end do
    
    
    return
    end subroutine interplg
    end module interplg_module
        
