! fftIntMat.f90
!*****************************************
    !
!*****************************************
    
    module fftIntMat_module
    contains
    subroutine fftIntMat(cn,fn,intmat)
    
    implicit none
    
    ! input valuables
    integer :: cn, fn
    
    ! temporery valuables
    integer, allocatable :: eyem(:,:)
    
    ! output valuables
    integer, allocatable :: intmat(:,:)
    
    ! body of this function
    allocate(eyem(cn,cn))
    !call eye(cn,eyem)
    eyem = 0
    forall(ii=1:cn)
        eyem(ii) = 1
    end forall
    
    allocate(intmat(fn,cn))
    call interpft(eyem,fn,intmat)
    intmat = cshift(intmat,1,1)
    intmat = cshift(intmat,1,2)
    
    return
    end subroutine fftIntMat
    end module fftIntMat_module
    