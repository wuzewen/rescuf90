! lagIntMat.f90
!******************************************************************
    !
!******************************************************************
    
    module lagIntMat_module
    contains
    subroutine LagIntMat(cn,fn,bc,order,intmatout)
    
    implicit none
    
    ! input valuables
    integer :: cn, fn
    logical :: bc
    real    :: order
    
    ! temporery valuables
    integer              :: ii
    !integer              :: ci(:), fi(:)
    real, allocatable    :: cx(:), fx(:)
    real, allocatable    :: w(:)
    integer, allocatable :: delta(:,:),intmat(:,:)
    
    ! output valuables
    integer, allocatable :: intmatout(:,:)
    
    ! body of this function
    allocate(cx(cn),fx(cn))
    !call arrayN(1,cn,ci)
    !call arrayN(1,fn,fi)
    forall (ii=1:cn)
        cx = real(ii)/real(cn)
    end forall
    forall (ii=1:fn)
        fx = real(ii)/real(fn)
    end forall
    
    allocate(w(3*cn))
    forall(ii=1:cn)
        w(ii) = cx(i)-1
    end forall
    forall(ii=cn+1:2*cn)
        w(ii) = cx(i-cn)
    end forall
    forall(ii=2*cn+1:3*cn)
        w(ii) = cx(i-2*cn)+1
    end forall
    
    allocate(delta(3*cn,3*cn))
    !call eye(3*cn,delta)
    forall (ii=1:3*cn)
        delta(ii,ii) = 1
    end forall
    
    allocate(intmat(fn,3*cn))
    do ii = 1,3*cn,1
        call interplg(w,delta(:,ii),fx,order,intmat(:,ii))  
    end do
    
    allocate(intmatout(fn,cn))
    if (bc) then
        forall(i=1:cn)
            intmatout(:,i) = intmat(:,i)+intmat(:,cn+i)+intmat(:,2*cn+i)
        end forall
    else
        forall(i=1:cn)
            intmatout(:,i) = intmat(:,cn+i)
        end forall
    end if
    
    return
    end subroutine lagIntMat
    end module lagIntMat_module