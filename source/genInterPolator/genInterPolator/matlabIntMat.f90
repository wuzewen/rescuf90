! matlabIntMat.f90
!**************************************************
    !
!**************************************************
    
    module matlabIntMat_module
    contains
    subroutine matlabIntMat(cn,fn,bc,order,intmat)
    
    implicit none
    
    ! input valuables
    integer :: cn, fn, bc, order
    
    ! temporery valuables
    integer              :: i
    integer, allocatable :: ci(:), fi(:), eyem(:,:)
    real, allocatable    :: cx(:), fx(:), w(:), intmattemp(:,:)
    
    ! output valuables
    integer, allocatable :: intmat(:,:)
    
    ! body of this function
    allocate(ci(cn),fi(fn),cx(cn),fx(fn))
    call arrayN(cn,ci)
    call arrayN(fn,fi)
    cx = real(ci)/real(cn)
    fx = real(fi)/real(fn)
    allocate(w(3*cn))
    forall(i=1:cn)
        w(i) = cx(i)-1
    end forall
    forall(i=cn+1:2*cn)
        w(i) = cx(i-cn)
    end forall
    forall(i=2*cn+1:3*cn)
        w(i) = cx(i-2*cn)+1
    end forall
    
    allocate(intmattemp(fn,3*cn),eyem(3*cn,3*cn))
    call eye(3*cn,eyem)
    call interpl(w,eyem,fx,order,intmattemp) ! unfinished
    if (bc) then
        forall(i=1:cn)
            intmat(i) = intmattemp(i)+intmattemp(i+cn)+intmattemp(i+2*cn)
        end forall
    else
        forall(i=1:cn)
            intmat(i) = intmattemp(i+cn)
        end forall
    end if
    
    return
    end subroutine matlabIntMat
    end module matlabIntMat_module
    