! interpft.f90
    
!**************************************************************
    ! This function has the same function with the function 
    ! interpft in matlab. MatrixA should be a two dimentional
    ! matrix, fn should be an integer, MatrixB is the output.
!**************************************************************
    
    module interpft_module
    contains
    subroutine interpft(MatrixA,fn,MatrixB)
    use fft2_module
    implicit none
    
    ! input valuables
    complex, allocatable :: MatrixA(:,:)
    integer              :: fn
    
    ! temporery valuables
    integer              :: m, n, mm
    integer              :: i
    integer              :: incr, nyqst
    complex, allocatable :: MatrixC(:,:), MatrixD(:,:), MatrixE(:,:)
    
    ! output valuables
    complex, allocatable :: MatrixB(:,:)
    
    ! body of this function
    m = size(MatrixA,1)
    n = size(MatrixA,2)
    
    if (fn>m) then
        incr = 1
    else if (fn/=0) then
        incr = m/fn+1
        fn   = incr*fn
    end if
    
    allocate(MatrixC(m,n))
    call fft2(MatrixA,1,MatrixC)
    nyqst = ceiling(real(m+1)/2)
    
    allocate(MatrixD(m,n))
    forall(i=1:nyqst)
        MatrixD(i,:) = MatrixC(i,:)
    end forall
    forall(i=1:fn-m)
        MatrixD(i,:) = 0
    end forall
    forall(i=fn-m+1:m)
        MatrixD(i,:) = MatrixC(i-fn+m+nyqst+1,:)
    end forall
    
    if (mod(m,2)==0) then
        MatrixD(nyqst,:)      = MatrixD(nyqst,:)/2
        MatrixD(nyqst+fn-m,:) = MatrixD(nyqst,:)
    end if
    
    allocate(MatrixE(m,n))
    call fft2(MatrixD,1,MatrixE)
    MatrixE = MatrixE*fn/m
    
    mm = fn/incr
    allocate(MatrixB(mm,n))
    forall(i=1:mm)
        MatrixB(i,:) = MatrixE((i-1)*incr,:)
    end forall
    
    end subroutine interpft
    end module interpft_module
    