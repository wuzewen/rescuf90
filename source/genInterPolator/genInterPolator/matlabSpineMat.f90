! matlabSpineMat.f90
!***************************************************************
    !
!***************************************************************
    
    module matlabSpineMat_module
    contains
    subroutine matlabSpineMat(cn,fn,bc,order,intmat)
    
    implicit none
    
    ! input valuables
    integer :: cn, fn, bc, order
    
    ! temporery valuables
    integer              :: i
    integer, allocatable :: ci(:), fi(:)
    integer, allocatable :: delta(:,:), intmattemp(:,:)
    real, allocatable    :: cx(:), fx(:), w(:)
    
    
    ! output valuables
    integer, allocatable :: intmat(:,:)
    
    ! body of this function
    if (mod(order,2)) then
        order = order+1
    end
    allocate(ci(cn),fi(fn),cx(cn),fx(cn))
    call arrayN(1,cn,ci)
    call arrayN(1,fn,fi)
    cx = real(ci)/real(cn)
    fx = real(fi)/real(fn)
    
    allocate(w(3*cn))
    forall(i=1:cn)
        w(i) = cx(i)-1
    end forall
    forall(i=cn+1:2*cn)
        w(i) = cx(i-cn)
    end forall
    forall(i=2*cn+1:3*cn)
        w(i) = cx(i-2*cn)+1
    end forall
    allocate(delta(3*cn,3*cn))
    call eye(3*cn,delta)
    
    allocate(intmattemp(fn,3*cn))
    intmattemp(:) = 0
    do i = order/2,3*cn-order/2,1
        call findMatrix(fx,w(i),w(i+1),inde)
        if (any(inde)) then
            forall(jj=i-prec/2:i+prec/2)
                wind(jj) = jj
            end forall
            call interpl(w(wind),delta(wind,wind),fx(inde),"spline",intmat(inde,wind))
        end if
    end do
    
    allocate(intmat(fn,cn))
    if (bc) then
        forall(i=1:cn)
            intmat(:,i) = intmattemp(:,i)+intmattemp(:,i+cn)+intmattemp(:,i+2*cn)
        end forall
    else
        forall(i=1:cn)
            intmat(:,i) = intmattemp(:,i+cn)
        end forall
    end if
    
    return
    end subroutine matlabSpineMat
    end module matlabSpineMat_module