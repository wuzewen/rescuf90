!  genInterPolator.f90 
!****************************************************************************
!
!  PROGRAM: genInterPolator
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module genInterPolator_module
    contains
    !subroutine genInterPolator(order,cn,fn,bc,method,intx,inty,intz)
    subroutine genInterPolator(FRC,cn,fn,method,intx,iny,intz)

    implicit none

    ! input Variables
    integer :: cn(3), fn(3), bc(3)
    real    :: order
    character(len=20) :: method
    
    ! temporery valuables
    
    
    ! output valuables
    real*8, allocatable :: intx(:), inty(:), intz(:)

    ! Body of genInterPolator
    order = FRC%interpolation%order
    do ii = 1,3,1
        if (FRC%domain%boundary(ii) == 1) then
            bc(ii) = .TRUE.
        else
            bc(ii) = .FALSE.
        end if
    end do
    
    if (trim(method) == trim("fourier") .or. trim(method) == trim("interpft") .or. trim(method) == trim("fft")) then    ! fourer, interpft, fft method
        
        write(*,*) "Error in genInterPolator.f90. fourier, interpft or fft is not avalable, please use lagrange."
        
        !call fftIntMat(cn(1),fn(1),intx)
        !call fftIntMat(cn(2),fn(2),inty)
        !call fftIntMat(cn(3),fn(3),intz)
    else if (trim(method) == trim("lanrange")) then                             ! lanrange method
        call lagIntMat(cn(1),fn(1),bc(1),order,intx)
        call lagIntMat(cn(2),fn(2),bc(2),order,inty)
        call lagIntMat(cn(3),fn(3),bc(3),order,intz)
    else if (trim(method) == trim("spline")) then                             ! spline method
        
        write(*,*) "Error in genInterPolator.f90. spline is not avalable, please use lagrange."
        
        !call matlabSplineMat(cn(1),fn(1),bc(1),order,intx)
        !call matlabSplineMat(cn(2),fn(2),bc(2),order,inty)
        !call matlabSplineMat(cn(3),fn(3),bc(3),order,intz)
    else
        
        write(*,*) "Error in genInterPolator.f90. Other method is not avalable, please use lagrange."
        
        !call matlabIntMat(cn(1),fn(1),bc(1),intx)
        !call matlabIntMat(cn(2),fn(2),bc(2),inty)
        !call matlabIntMat(cn(3),fn(3),bc(3),intz)
    end if
    

    end subroutine genInterPolator
    end module genInterPolator_module

