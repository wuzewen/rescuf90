! repMat.f90
!**************************************************************************
    ! This function has the same function with the function repmat in 
    ! matlab. While, ArrayA must be an array in one dimention.
!**************************************************************************
    
    module repMat2_module
    contains
    subroutine repMat2(ArrayA,dim1,dim2,MatrixB)
    
    implicit none
    
    ! input valuables
    real*8, allocatable  :: ArrayA(:,:)
    integer              :: dim1, dim2
    
    ! temporery valuables
    integer              :: N, M
    integer              :: i, j, k, l
    
    ! output valuables
    real*8, allocatable  :: MatrixB(:,:)
    
    ! body of this function
    M = size(ArrayA,1)
    N = size(ArrayA,2)
    do i = 1,dim1,1
        do j = 1,dim2,1
            forall(k=((i-1)*M+1):(i*M),l=((j-1)*N+1):(j*N))
                MatrixB(k,l) = ArrayA(k-(i-1)*M,l-(j-1)*N)
            end forall
        end do
    end do
    
    return
    end subroutine repMat2
    end module repMat2_module