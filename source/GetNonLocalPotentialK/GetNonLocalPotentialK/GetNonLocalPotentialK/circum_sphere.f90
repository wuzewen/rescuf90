! circum_sphere.f90
    
!*******************************************************
    !
    !
    !
!*******************************************************
    
    subroutine circum_sphere(avec,rad)
    
    use ndgridA_module
    
    implicit none
    
    ! input variables
    real*8  :: avec(3,3)
    
    ! temporary variables
    real*8, allocatable  :: xyz(:,:), rat(:), x(:)
    integer :: ii
    
    ! output variables
    real*8  :: rad
    
    ! body of this function
    allocate(x(2),xyz(8,3),rat(8))
    x(1) = -0.5
    x(2) =  0.5
    call ndgridA(x,x,x,xyz)
    xyz = matmul(xyz,avec)
    xyz = xyz*xyz
    forall(ii=1:8)
        rat(ii) = (sum(xyz(ii,:)))**0.5
    end forall
    rad = maxval(rat)
    
    return
    end subroutine circum_sphere