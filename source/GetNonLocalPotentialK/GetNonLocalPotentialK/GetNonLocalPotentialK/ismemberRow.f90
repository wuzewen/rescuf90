! ismemberRow.f90
    
!********************************************************
    !
    ! This is an additional function of ismember.f90.
    ! if A's ii row is a row in B, isLoc(ii) is True,
    ! otherwise false, and indLoc(ii) is the location
    ! of this row in B, otherwise 0.
    !
!********************************************************
    
    module ismemberRow_module
    contains
    subroutine ismemberRow(A,B,isLoc,indLoc)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    integer :: mA, nA, mB, nB, ii, jj, kk, ntmp
    
    ! output variables
    logical, allocatable :: isLoc(:)
    integer, allocatable :: indLoc(:)
    
    ! body of this function
    mA = size(A,1)
    nA = size(A,2)
    mB = size(B,1)
    nB = size(B,2)
    allocate(isLoc(mA),indLoc(mA))
    
    if (nA /= nB) then
        write(*,*) "Error in ismemberRow. number of A and B's cloumn should be the same."
        stop
    end if
    
    !allocate(isLoc(mA),indLoc(mA))
    isLoc  = .FALSE.
    indLoc = 0
    
    do ii = 1,mA,1
        do jj = 1,mB,1
            ntmp = 0
            do kk = 1,nA,1
                if (A(ii,kk) == B(jj,kk)) then
                    ntmp = ntmp+1
                end if
            end do
            if (ntmp == nA) then
                isLoc(ii)  = .TRUE.
                indLoc(ii) = jj
            end if
        end do
    end do
    
    return
    end subroutine ismemberRow
    end module ismemberRow_module