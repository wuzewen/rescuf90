! getrsphase.f90
    
!****************************************************
    !
    !
    !
!****************************************************
    
    module getrsphase_module
    contains
    subroutine getrsphase(FRC,kk,grid,eikr)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ndGridA_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8                          :: kk(3)
    integer                         :: grid(3)
    
    ! temporary variables
    integer              :: nx, ny, nz, ii
    real*8, allocatable  :: x(:), y(:), z(:), xyz(:,:)
    real*8               :: pi
    complex              :: im
    
    ! output variables
    complex, allocatable :: eikr(:,:)
    
    ! body of this function
    im = (0,1)
    pi = 3.14159265354
    nx = grid(1)
    ny = grid(2)
    nz = grid(3)
    
    allocate(x(nx),y(ny),z(nz))
    forall(ii=1:nx)
        x(ii) = real(ii)/real(nx)
    end forall
    forall(ii=1:ny)
        y(ii) = real(ii)/real(ny)
    end forall
    forall(ii=1:nz)
        z(ii) = real(ii)/real(nz)
    end forall
    
    allocate(xyz(nx*ny*nz,3))
    call ndgridA(x,y,z,xyz)
    allocate(eikr(nx*ny*nz,1))
    eikr(:,1) = exp(matmul(-(2,0)*im*cmplx(pi)*cmplx(xyz),cmplx(kk(:))))
    
    return
    end subroutine getrsphase
    end module getrsphase_module