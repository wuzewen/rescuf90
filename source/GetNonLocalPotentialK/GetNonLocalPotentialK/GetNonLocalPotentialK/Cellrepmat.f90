! Cellrepmat
    
!***************************************
    !
    !
    !
!***************************************
    
    module Cellrepmat_module
    contains
    subroutine Cellrepmat(matrix,dim1,dim2,cellout)
    
    use Acell_type
    
    implicit none
    
    ! input variables
    real*8, allocatable :: matrix(:,:)
    integer             :: dim1, dim2
    
    ! temporary variables
    integer :: m,n,ii,jj
    
    ! output variables
    type(AcellType), allocatable :: cellout(:,:)
    
    ! body of this function
    m = size(matrix,1)
    n = size(matrix,2)
    
    allocate(cellout(dim1,dim2))
    do ii = 1,dim1,1
        do jj = 1,dim2,1
            allocate(cellout(ii,jj)%vata(m,n))
            cellout(ii,jj)%vata = matrix
        end do
    end do
    
    return
    end subroutine Cellrepmat
    end module Cellrepmat_module