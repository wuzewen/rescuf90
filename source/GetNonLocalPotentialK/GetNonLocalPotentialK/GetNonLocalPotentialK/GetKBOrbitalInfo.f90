! GetKBOrbitalInfo.f90
    
!***********************************************
    !
    !
    !
!***********************************************
    
    module GetKBOrbitalInfo_module
    contains
    subroutine GetKBOrbitalInfo(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    integer, allocatable :: lnu(:), nnu(:), lnutmp(:), mnutmp(:), nnutmp(:), onutmp(:), evec(:)
    real*8, allocatable  :: enu(:), rnu(:), enutmp(:), rnutmp(:)
    integer              :: ntmp, nlnu, kk, jj, ii, natom, ntmp2, nelem, sizeOflnu, typeE, ntmp1
    integer              :: sizeOfvata
    type(realType),allocatable :: Eorb(:), Rorb(:)
    type(intetype),allocatable :: Morb(:), Norb(:), Lorb(:), Oorb(:)
    
    
    
    
    ! output variables
    
    ! body of this function
    nelem = FRC%Atom%numberOfElement
    
    !write(*,*) "nelem in GetKBOrbitalinfo.f90"
    !write(*,*)  nelem
    
    !allocate(lnu(FRC%SOB))
    allocate(Eorb(nelem),Lorb(nelem),Morb(nelem),Norb(nelem),Oorb(nelem),Rorb(nelem))
    do kk = 1,nelem,1
        sizeOflnu = FRC%SOB(kk)%SizeOfVnl
        allocate(lnu(sizeOflnu))
        allocate(enu(sizeOflnu))
        allocate(nnu(sizeOflnu))
        allocate(rnu(sizeOflnu))
        
        !write(*,*) "size of *nu in GetKBOrbitalinfo.f90"
        !write(*,*) size(lnu), size(enu), size(nnu), size(rnu)
        
        lnu  = FRC%element(kk)%vnllnu
        enu  = FRC%element(kk)%vnlenu
        nnu  = FRC%element(kk)%vnlnnu
        rnu  = FRC%element(kk)%vnlrnu
        
        !write(*,*) "value of *nu in GetKBOrbitalinfo.f90"
        !write(*,*) lnu
        !write(*,*) enu
        !write(*,*) nnu
        !write(*,*) rnu
        
        ntmp = 0
        nlnu = 0
        do jj = 1,sizeOflnu,1
            if (lnu(jj)<=FRC%element(kk)%vnlLcutoff .and. nnu(jj)<=FRC%element(kk)%vnlNcutoff) then
                ntmp = ntmp+1
                nlnu = nlnu + lnu(jj)
            end if
        end do
        
        ntmp = 2*nlnu+ntmp
        
        !write(*,*) "size of *nutmp in GetKBOrbitalinfo.f90"
        !write(*,*) ntmp
        
        allocate(enutmp(ntmp))
        allocate(lnutmp(ntmp))
        allocate(mnutmp(ntmp))
        allocate(nnutmp(ntmp))
        allocate(onutmp(ntmp))
        allocate(rnutmp(ntmp))
        allocate(Eorb(kk)%vata(ntmp))
        allocate(Lorb(kk)%vata(ntmp))
        allocate(Morb(kk)%vata(ntmp))
        allocate(Norb(kk)%vata(ntmp))
        allocate(Oorb(kk)%vata(ntmp))
        allocate(Rorb(kk)%vata(ntmp))
        sizeOfvata = ntmp
        !write(*,*) ntmp
        
        nlnu = 0
        ntmp = 0
        do jj = 1,sizeOflnu,1
            if (lnu(jj)<=FRC%element(kk)%vnlLcutoff .and. nnu(jj)<=FRC%element(kk)%vnlNcutoff) then
                ntmp  = ntmp+1
                ntmp2 = ntmp+2*lnu(jj)
                
                !write(*,*) ntmp, ntmp2
                !write(*,*) ntmp2
                
                forall(ii=ntmp:ntmp2)
                    enutmp(ii) = enu(jj)
                end forall
                forall(ii=ntmp:ntmp2)
                    lnutmp(ii) = lnu(jj)
                end forall
                forall(ii=ntmp:ntmp2)
                    mnutmp(ii) = -lnu(jj)+ii-ntmp
                end forall
                forall(ii=ntmp:ntmp2)
                    nnutmp(ii) = nnu(jj)
                end forall
                forall(ii=ntmp:ntmp2)
                    onutmp(ii) = jj
                end forall
                forall(ii=ntmp:ntmp2)
                    rnutmp(ii) = rnu(jj)
                end forall
                ntmp = ntmp2
            end if
        end do
        
        !write(*,*) rnutmp
        !write(*,*) "*nutmp in GetKBorbitalInfo.f90"
        !write(*,*)  enutmp
        !write(*,*)  lnutmp
        !write(*,*)  mnutmp
        !write(*,*)  nnutmp
        !write(*,*)  onutmp
        !write(*,*)  rnutmp
        
        Eorb(kk)%vata = enutmp
        Lorb(kk)%vata = lnutmp
        Morb(kk)%vata = mnutmp
        Norb(kk)%vata = nnutmp
        Oorb(kk)%vata = onutmp
        Rorb(kk)%vata = rnutmp
        
        deallocate(lnu,enu,nnu,rnu,enutmp,lnutmp,mnutmp,nnutmp,onutmp,rnutmp)
    end do
    
    natom = FRC%atom%numberOfAtom
    
    ntmp = 0
    do kk = 1,natom,1
        typeE = FRC%atom%element(kk)
        ntmp2 = size(Lorb(typeE)%vata)
        ntmp  = ntmp + ntmp2
    end do
    allocate(evec(ntmp))
    ntmp1 = 0
    do kk = 1,natom,1
        typeE = FRC%atom%element(kk)
        ntmp1 = ntmp1+1
        ntmp2 = size(Lorb(typeE)%vata)
        ntmp2 = ntmp1+ntmp2-1
        forall(jj=ntmp1:ntmp2)
            evec(jj) = kk
        end forall
        ntmp1 = ntmp2
    end do
    
    allocate(FRC%potential%vnl%evec(nelem*ntmp))
    allocate(FRC%potential%vnl%Eorb(nelem*ntmp))
    allocate(FRC%potential%vnl%Lorb(nelem*ntmp))
    allocate(FRC%potential%vnl%Morb(nelem*ntmp))
    allocate(FRC%potential%vnl%Norb(nelem*ntmp))
    allocate(FRC%potential%vnl%Oorb(nelem*ntmp))
    allocate(FRC%potential%vnl%Rorb(nelem*ntmp))
    allocate(FRC%potential%vnl%Sorb(nelem*ntmp))
    
    FRC%potential%vnl%evec = evec
    
    do jj = 1,natom,1
        do ii = 1,sizeOfvata
            typeE = FRC%atom%element(jj)
            FRC%potential%vnl%Eorb((jj-1)*sizeOfvata+ii) = Eorb(typeE)%vata(ii)
            FRC%potential%vnl%Lorb((jj-1)*sizeOfvata+ii) = Lorb(typeE)%vata(ii)
            FRC%potential%vnl%Morb((jj-1)*sizeOfvata+ii) = Morb(typeE)%vata(ii)
            FRC%potential%vnl%Norb((jj-1)*sizeOfvata+ii) = Norb(typeE)%vata(ii)
            FRC%potential%vnl%Oorb((jj-1)*sizeOfvata+ii) = Oorb(typeE)%vata(ii)
            FRC%potential%vnl%Rorb((jj-1)*sizeOfvata+ii) = Rorb(typeE)%vata(ii)
        end do
    end do
    do jj = 1,nelem*ntmp,1
        FRC%potential%vnl%Sorb(jj) = FRC%atom%element(evec(jj))
    end do
    
    !write(*,*) "Potential vnl evec"
    !write(*,*)  FRC%potential%vnl%evec
    !write(*,*) "Potential vnl eorb"
    !write(*,*)  FRC%potential%vnl%Eorb
    !write(*,*) "Potential vnl Lorb"
    !write(*,*)  FRC%potential%vnl%Lorb
    !write(*,*) "Potential vnl Morb"
    !write(*,*)  FRC%potential%vnl%Morb
    !write(*,*) "Potential vnl Norb"
    !write(*,*)  FRC%potential%vnl%Norb
    !write(*,*) "Potential vnl Oorb"
    !write(*,*)  FRC%potential%vnl%Oorb
    !write(*,*) "Potential vnl Rorb"
    !write(*,*)  FRC%potential%vnl%Rorb
    !write(*,*) "Potential vnl Sorb"
    !write(*,*)  FRC%potential%vnl%Sorb
    
    return
    end subroutine GetKBOrbitalInfo
    end module GetKBOrbitalInfo_module
        