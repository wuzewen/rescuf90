!  GetNonLocalPotentialK.f90 
!
!  FUNCTIONS:
!  GetNonLocalPotentialK - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: GetNonLocalPotentialK
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module GetNonLocalPotentialK_module
    contains
    subroutine GetNonLocalPotentialK(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use Acell_type
    use getKBOrbitalinfo_module
    use cellInRange_module
    use flipud_module
    use interprDist2Cart_module
    use ismember_module
    use ismemberRow_module
    use cellfunnnz_module
    use cellfunnnz2_module
    use cat_module
    use reduceoper_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical :: mpistat, useCell, kbsparse
    integer :: mpirank, mpisize, fgridn(3), fn, natom, nproj, ntv, ntv2, kk, elem
    integer :: lcut, ncut, ntmp, nlnu, ii, jj, itmp, ll, nindtmp, pp, nevec, ntmpxxx
    integer :: nnz, spnnz, typeE, nkbcell, nnzLoc, qq, ntmp1, ntmp2, ntmp3, ntmp4, ntmp5
    real*8  :: avec(3,3), pos(1,3), rad
    real*8, allocatable   :: evec(:), rorb(:), oorb(:), xyz(:,:), tvectmp(:,:)
    real*8, allocatable   :: KBEnergy(:), rrData(:), vvData(:), tvtmp(:,:)
    real*8, allocatable   :: avectmp(:,:), vatatmp1(:,:), vatatmp2(:,:), tvec(:,:), out3(:)
    integer, allocatable  :: lnu(:), nnu(:), inde(:), indtmp(:), indLoc(:)
    integer, allocatable  :: indLoctmp(:), tvecp(:,:)
    logical, allocatable  :: isLoc(:), isLoctmp(:)
    type(Acelltype), allocatable :: kbcell(:,:), kbcelltmp(:)
    type(Acelltype)              :: kbcell2, kbcell3
    !real*8, allocatable :: kbtmp(:,:)
    type(VnlType), allocatable   :: dataVnl(:)
    type(AcellType), allocatable :: kbtmp(:,:)
    
    ! output variables

    ! Body of GetNonLocalPotentialK
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    
    !write(*,*) "mpi info in GetNonLocalPotentialK.f90"
    !write(*,*)  mpistat, mpirank, mpisize
    
    call GetKBOrbitalInfo(FRC)
    
    fgridn  = FRC%domain%fgridn
    avec    = FRC%domain%latvec
    nproj   = size(FRC%potential%vnl%evec)
    allocate(evec(nproj),rorb(nproj),oorb(nproj))
    evec    = FRC%potential%vnl%evec
    nevec   = size(evec)
    rorb    = FRC%potential%vnl%Rorb
    oorb    = FRC%potential%vnl%Oorb
    natom   = FRC%atom%numberOfAtom
    fn      = product(fgridn)
    
    
    
    allocate(KBEnergy(nproj))
    KBEnergy = 0
    
    if (any(FRC%kpoint%kdirect /= 0) .or. trim(FRC%info%calculationType) == trim("dfpt")) then
        useCell = .TRUE.
    else
        useCell = .FALSE.
    end if
    
    !write(*,*) "useCell in GetNonLocalPotentialK.f90"
    !write(*,*)  useCell
    
    rad  = maxval(rorb)
    allocate(xyz(natom,3))
    xyz  = FRC%atom%XYZ
    !allocate(tvec(ntv))
    allocate(avectmp(3,3))
    avectmp = avec
    call cellInRange(avectmp,xyz,rad,.FALSE.,tvec)
    
    !write(*,*) "tvec in GetNonLocalPotentialK.f90"
    !write(*,*)  tvec
    
    
    ntv  = size(tvec,1)
    ntv2 = size(tvec,2)
    !allocate(tvectmp(ntv,ntv2))
    call flipud(tvec,tvectmp)
    tvec = -tvectmp
    
    !write(*,*) "tvec after flipud in GetNonLocalPotentialK.f90"
    !write(*,*)  tvec
    FRC%option%initParaReal = .FALSE.
    if (FRC%option%initParaReal) then
        
        write(*,*) "Error in GetNonLocalPotential.f90. FRC%option%initParaReal = FALSE."
        stop
        
        !call circum_sphere(avec,rsph)
        !center = 0.5*matmul((/1,1,1/),avec)
        !npoint = 
        !allocate(GridCoord(npoint))
        !call GetGridPointCoord(FRC,GridCoord)
        !call InitDistArray(fn,nproj,ceiling(fn/mpisize),1,mpisize,1,kbsub)
        !call repmat(kbsub,ntv,1,kbcell)
        
        !do tt = 1,ntv,1
        !    kbtmp = kbsub
        !    do ii = 1,natom,1
        !        pos  = FRC%atom%XYZ(ii,:) + matmul(tvec(tt,:),avec)
        !        elem = FRC%atom%element(ii)
        !        lnu  = FRC%element(elem)%vnllnu
        !        lcut = FRC%element(elem)%vnlLcutoff
        !        nnu  = FRC%element(elem)%vnlnnu
        !        ncut = FRC%element(elem)%vnlNcutoff
        !        
        !        ntmp = 0
        !        do jj = 1,nlnu,1
        !            if (lnu <= lcut .and. nnu <= ncut) then
        !                ntmp = ntmp + 1
        !            end if
        !        end do
        !        
        !        allocate(inde(ntmp))
        !        ntmp = 0
        !        do jj = 1,nlnu,1
        !            if (lnu <= lcut .and. nnu <= ncut) then
        !                ntmp       = ntmp + 1
        !                inde(ntmp) = jj
        !            end if
        !        end do
        !        
        !        dataV = FRC%ElementData(elem)%Vnl
        !        
        !        do jj = 1,nlnu,1
        !            itmp = inde(jj)
        !            if (evev == ii .and. oorb == jj) then
        !                kbind = .TRUE.
        !            else
        !                kbind = .FALSE.
        !            end if
        !            
        !            call norm(pos-center,tmp)
        !            nnn = FRC%SOB(itmp)%rrdatasize
        !            if (tmp < rsph+dataV(itmp)%rrData(nnn)) then
        !                ll = dataV(jj)%L
        !                call InterpSph2Cart(dataV(itmp),"rrData","vvData",pos,ll,GridCoord,avec,(/.FALSE.,.FALSE.,.FALSE./),kbtmp%dataV(:,kbind))
        !            end if
        !            KBEnergy(kbind) = dataV(itmp)%KBEnergy
        !        end do
        !    end do
        !    
        !    call adjustSparsity(kbtmp%dataV,0.2,kbtmp%dataV)
        !    kbcell(tt) = kbtmp
        !end do
        
        !if (.not. useCell) then
        !    call cellFun(nnz())
        !    
        !    
        !    call adjustSparsity(kbcell(1)%dataV,0.2,kbcell(1)%dataV)
        !end if
        !
        !if (mpistat) then
        !    
        !    write(*,*) "Error in GetNonLocalPotentialK.f90. MPI is not avalable now."
        !    stop
        !    
        !    !do tt = 1,NumKbcell,1
        !    !    call distmat_allgather(FRC,kbcell(tt),kbcell(tt)%dataV)
        !    !    call adjustSparsity(kbcell(tt)%dataV,0.2,kbcell(tt)%dataV)
        !    !end do
        !end if
        !
    else
        
        !write(*,*) "Error in GetNonLocalPotentialK.f90, Please use FRC%option%initParaReal = .TRUE.."
        !stop
        !call repmat(ntv,1,kbcell)
        ntmp = 0
        do ii = 1,natom,1
            typeE = FRC%atom%element(ii)
            do jj = 1,FRC%SOB(typeE)%SizeOfVnl
                ntmp = ntmp +1
            end do
        end do
        allocate(kbcell(ntv,ntmp))
        nkbcell = ntv
        
        !write(*,*) "size of kbcell in GetNonLocalPotentialK.f90."
        !write(*,*)  ntv, ntmp
        
        kk = 1
        do ii = 1,natom,1
            pos(1,:)  = FRC%atom%XYZ(ii,:)
            elem      = FRC%atom%element(ii)
            nlnu      = size(FRC%element(elem)%vnllnu)
            allocate(lnu(nlnu),nnu(nlnu))
            lnu       = FRC%element(elem)%vnllnu
            lcut      = FRC%element(elem)%vnlLcutoff
            nnu       = FRC%element(elem)%vnlnnu
            ncut      = FRC%element(elem)%vnlNcutoff
            
            !write(*,*) "Number of loop ii in getNON"
            !write(*,*)  ii
            !write(*,*) "pos, elem, lcut, ncut"
            !write(*,*)  pos, elem, lcut, ncut
            !write(*,*) "lnu"
            !write(*,*)  lnu
            !write(*,*) "nnu"
            !write(*,*)  nnu
            
            ntmp = 0
            do jj = 1,nlnu,1
                if (lnu(jj)<=lcut .and. nnu(jj)<=ncut) then
                    ntmp = ntmp+1
                end if
            end do
            allocate(inde(ntmp))
            ntmp = 0
            do jj = 1,nlnu,1
                if (lnu(jj)<=lcut .and. nnu(jj)<=ncut) then
                    ntmp       = ntmp+1
                    inde(ntmp) = jj
                end if
            end do
            
            !write(*,*) "inde"
            !write(*,*)  inde
            
            allocate(dataVnl(FRC%SOB(elem)%SizeOfVnl))
            dataVnl(:) = FRC%ElementData(elem)%Vnl(:)
            
            !write(*,*) "KBenergy In dataVnl"
            !write(*,*)  dataVnl(1)%KBEnergy, dataVnl(2)%KBEnergy, dataVnl(3)%KBEnergy, dataVnl(4)%KBEnergy
            
            do jj = 1,ntmp,1
                itmp    = inde(jj)
                ll      = dataVnl(itmp)%L
                nindtmp = 0
                do pp = 1,nevec,1
                    if (evec(pp) == ii .and. oorb(pp) == itmp) then
                        nindtmp = nindtmp + 1
                    end if
                end do
                
                !write(*,*) "nindtmp, nevec"
                !write(*,*)  nindtmp, nevec
                
                
                allocate(indtmp(nindtmp))
                nindtmp = 0
                do pp = 1,nevec,1
                    if (evec(pp) == ii .and. oorb(pp) == itmp) then
                        nindtmp         = nindtmp + 1
                        indtmp(nindtmp) = pp
                    end if
                end do
                
                !write(*,*) "indtmp"
                !write(*,*)  indtmp
                
                do pp = 1,nindtmp
                    ntmp           = indtmp(pp)
                    KBenergy(ntmp) = dataVnl(itmp)%KBenergy
                end do
                
                !write(*,*) "KBEnergy"
                !write(*,*)  KBEnergy
                    
                !allocate(isLoc(ntv))
                !forall(pp=1:ntv)
                !    isLoc(pp) = .FALSE.
                !end forall
                
                if (mod(ii-1,mpisize) == mpirank) then
                    allocate(rrData(FRC%SOB(elem)%sizeOfVnlrrData))
                    allocate(vvData(FRC%SOB(elem)%sizeOfVnlvvData))
                    rrData = dataVnl(jj)%rrData
                    vvData = dataVnl(jj)%vvData
                    
                    !write(*,*) "size of rrdata in GETNON"
                    !write(*,*)  size(rrdata), FRC%SOB(elem)%sizeOfVnlrrData, elem
                    
                    call InterpRdist2Cart(rrData,vvData,pos,ll,avec,fgridn,.TRUE.,kbtmp,tvtmp,out3)
                    
                    !write(*,*) "tvtmp in GetNonLocal"
                    !write(*,*)  tvtmp
                    !write(*,*) "kbtmp in GetNonLocal"
                    !write(*,*)  maxval(kbtmp(3,1)%vata), minval(kbtmp(3,1)%vata)
                    !write(*,*)  kbtmp(3,1)%vata(2067,1), kbtmp(3,1)%vata(2093,1)
                    
                    call ismemberRow(tvec,tvtmp,isLoc,indLoctmp)
                    
                    !write(*,*) "isLoc in getNonLocal"
                    !write(*,*)  isLoc
                    !write(*,*)  indLoctmp
                    
                    call ismemberRow(tvtmp,tvec,isLoctmp,indLoc)
                    
                    !write(*,*) "isLoctmp in getNonLocal"
                    !write(*,*)  isLoctmp
                    !write(*,*)  indLoc
                    
                    ntmpxxx = size(indLoc)
                    if (any(isLoc == .TRUE.)) then
                        do pp = 1,ntmpxxx,1
                            qq = indLoc(pp)
                            allocate(kbcell(pp,kk)%vata(size(kbtmp(qq,1)%vata,1),size(kbtmp(qq,1)%vata,2)))
                            kbcell(pp,kk)%vata = kbtmp(qq,1)%vata
                        end do
                    end if
                    deallocate(rrData,vvdata)
                end if
                kk = kk +1
                deallocate(indtmp,kbtmp,tvtmp,isLoc,indLoctmp,isLoctmp,indLoc,out3)
            end do
            deallocate(lnu,nnu,inde,dataVnl)
        end do
        
        
        allocate(kbcelltmp(nkbcell))
        
        !write(*,*) "nkbcell in getNonLocalPotential.f90."
        !write(*,*)  nkbcell, qq, kk
        !write(*,*) "kbenergy in getNonLocalPotential.f90."
        !write(*,*)  kbenergy
        
        do ii = 1,nkbcell,1
            ntmp1 = size(kbcell(ii,1)%vata,1)
            ntmp2 = size(kbcell(ii,1)%vata,2)
            allocate(vatatmp1(ntmp1,ntmp2))
            vatatmp1 = kbcell(ii,1)%vata
            !write(*,*) "kk in getNonLocal"
            !write(*,*)  kk
            
            do jj = 1,kk-2,1
                ntmp1 = size(kbcell(ii,jj)%vata,1)
                ntmp2 = size(kbcell(ii,jj)%vata,2)
                ntmp3 = size(kbcell(ii,jj+1)%vata,1)
                ntmp4 = size(kbcell(ii,jj+1)%vata,2)
                ntmp5 = size(vatatmp1,2)
                
                allocate(vatatmp2(ntmp1,ntmp5+ntmp4))
                call cat(2,vatatmp1,kbcell(ii,jj+1)%vata,vatatmp2)
                deallocate(vatatmp1)
                allocate(vatatmp1(ntmp1,ntmp5+ntmp4))
                vatatmp1 = vatatmp2
                deallocate(vatatmp2)
            end do
            ntmp1 = size(vatatmp1,1)
            ntmp2 = size(vatatmp1,2)
            allocate(kbcelltmp(ii)%vata(ntmp1,ntmp2))
            kbcelltmp(ii)%vata = vatatmp1
            deallocate(vatatmp1)
            !do jj = 1,kk-2,1
            !    kbcelltmp(ii)%vata(ii,:) = kbcell(ii,jj)%vata
            !end do
            !call cat(2,kbcell(ii,jj)%vata,kbcell(ii,jj+1)%vata,kbcelltmp)    
            !call adjustSparsity(kbcelltmp,0.2,kbcell(ii,1))
        end do
                !kbcell = kbcell(:,1)
       !
        ntmp1 = size(tvec,1)
        ntmp2 = size(tvec,2)
        tvecp = int(tvec)
        allocate(tvecp(ntmp1,ntmp2))
        if (.not. useCell) then
            call cellfunnnz(kbcelltmp,spnnz)
            !call cellfunnnz(Acell,spnnz)
            
            !write(*,*) "size of kbcelltmp in get NonLocalPotential."
            !write(*,*)  size(kbcelltmp)
            
            call reduceOper(kbcelltmp,tvecp,(/0,0,0/),spnnz,kbcell2)
            FRC%potential%vnl%kbcell = kbcell2
        else
            ntmp1 = size(kbcelltmp)
            allocate(FRC%potential%vnl%kbcell(ntmp1))
            do ii = 1,ntmp1,1
                ntmp2 = size(kbcelltmp(ii)%vata,1)
                ntmp3 = size(kbcelltmp(ii)%vata,2)
                allocate(FRC%potential%vnl%kbcell(ii)%vata(ntmp2,ntmp3))
            end do
            
            FRC%potential%vnl%kbcell = kbcelltmp
            !tvec = 0
            !call adjustSparsity(kbcell(1,1)%vata,0.2,kbcell(1,1))
        end if
        
        if (mpistat) then
            
            write(*,*) "Error in GetNonLocalPotentialK.f90. MPI is not avalable now."
            stop
            
            !do tt = 1,nkbcell
            !    do ii = 0,mpisize-1,1
            !        nindtmp = 0
            !        do pp = 1,nevec,1
            !           if (mod(evec(pp)-1,mpisize) == ii) then
            !                nindtmp = nindtmp + 1
            !            end if
            !        end do
            !        allocate(indtmp(nindtmp))
            !        nindtmp = 0
            !        do pp = 1,nevec,1
            !            if (mod(evec(pp)-1,mpisize) == ii) then
            !                nindtmp = nindtmp + 1
            !                indtmp(nindtmp) = pp
            !            end if
            !        end do
            !        
            !        if (sum(indtmp) /= 0) then
            !            call adjustSparsity(kbcell(tt)%dataV(:,indtmp),0.2,kbdata)
            !            call MPI_Bcast_variable(kbdata,ii,kbdata)
            !        end if
            !    end do
            !    call adjustSparsity(kbcell(tt)%dataV,0.2,kbcell(tt)%dataV)
            !end do
        end if
    end if
    
    !call cellfunnnz(kbcelltmp,spnnz)
    !call reduceOper(kbcelltmp,tvecp,(/0,0,0/),spnnz,kbcell3)
    !call adjustSparsity(kbtmp%dataV,0.2,kbsparse) !! sparse matrix not used.
    !nnzLoc = nnz(kbtmp%vata)
    !call cellfunnnz(kbtmp,spnnz)
    
    !call PickInterpVnl(FRC)
    !allocate()
    !FRC%potential%vnl%kbcell = kbcell
    allocate(FRC%potential%vnl%kbvec(size(tvec,1),size(tvec,2)))
    FRC%potential%vnl%kbvec  = tvec
    
    if (kbsparse) then
        FRC%potential%vnl%kbsparse = nnzLoc
    else
        FRC%potential%vnl%kbsparse = -1
    end if
    allocate(FRC%potential%vnl%Aorb(size(evec)))
    FRC%potential%vnl%Aorb     = evec
    allocate(FRC%potential%vnl%KBEnergy(size(KBEnergy)))
    FRC%potential%vnl%KBEnergy = KBEnergy
    
    return
    end subroutine GetNonLocalPotentialK
    end module GetNonLocalPotentialK_module

