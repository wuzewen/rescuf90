! reduceOper.f90
    
!*************************************************************************
    !
    ! In this function, kpt = [0 0 0] is used in GetNonLocalPotentialK.
    ! While if kpt is not [0 0 0], this function should be fixed, with
    ! complex numbers and arrays. When it is done, renew this notes.
    ! Another important thing is, sparse matrix is not used here.
    !
!*************************************************************************
    
    module reduceOper_module
    contains
    subroutine reduceOper(Acell,Av,kpt,spnnz,oper)
    
    use Acell_type
    
    implicit none
    
    ! input variables
    type(AcellType), allocatable :: Acell(:)
    integer                      :: kpt(1,3), spnnz, ii
    integer, allocatable         :: Av(:,:)
    
    ! temporary variables
    real*8  :: pi
    integer :: datatmp(3,1), m, n, ncell, tmp
    complex :: im
    
    ! output variables
    type(AcellType)              :: oper
    
    ! body of this function
    im   = (0,1)
    pi   = 3.14159265354
    m    = size(Acell(1)%vata,1)
    n    = size(Acell(1)%vata,2)
    
    allocate(oper%vata(m,n))
    oper%vata = 0
    
    !if (spnnz >= 0) then
    ncell = size(Acell)
    
    do ii = 1,ncell,1
        datatmp(1,:) = Av(ii,:)
        tmp          = kpt(1,1)*datatmp(1,1)+kpt(1,2)*datatmp(2,1)+kpt(1,3)*datatmp(3,1) !matmul(kpt,datatmp) !
        oper%vata    = oper%vata + Acell(ii)%vata*exp(2*real(im)*pi*real(tmp))
    end do
    
    return
    end subroutine reduceOper
    end module reduceOper_module