! pickInterpVnl.f90
    
!***************************
    !
    !
    !
!***************************
    
    module pickInterpVnl_module
    contains
    subroutine pickInterpVnl(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! inputvariables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    buff = FRC%option%bufferSize
    
    if (FRC%interpolation%vloc) then
        interVnl = .TRUE.
        exit
    end if
    
    cgn = FRC%doamin%cgridn
    if (buff > product(cgn)) then
        interVnl = .FALSE.
        exit
    end if
    
    fgn     = FRC%doamin%fgridn
    avec    = FRC%domain%latvec
    call det(avec,tmp)
    cdr     = abs(tmp)/product(cgn)
    fdr     = abs(tmp)/product(fgn)
    order   = FRC%interpolation%order
    dxtmp   = sqrt(sum(avec*avec,2))/cgn
    dx      = sum(order*dx/2)/3
    nel     = FRC%atom%numberOfElement
    nnzvol  = 0
    innzvol = 0
    
    do ee = 1,nel,1
        lnu  = FRC%element(ee)%vnllnu
        lcut = FRC%element(ee)%vnlLcutoff
        nnu  = FRC%element(ee)%vnlnnu
        ncut = FRC%element(ee)%vnlBcutoff
        
        ntmp = 0
        do ii = 1,nlnu,1
            if (lnu(ii) <= lcut .and. nnu(ii) <= ncut) then
                ntmp = ntmp + 1
            end if
        end do
        allocate(inde(ntmp))
        ntmp = 0
        do ii = 1,nlnu,1
            if (lnu(ii) <= lcut .and. nnu(ii) <= ncut) then
                ntmp       = ntmp + 1
                inde(ntmp) = ii
            end if
        end do
        
        ntmp = 0
        do ii = 1,nel,1
            if (FRC%atom%element(ii) == ee) then
                ntmp = ntmp + 1
            end if
        end do
        allocate(nat(ntmp))
        ntmp = 0
        do ii = 1,nel,1
            if (FRC%atom%element(ii) == ee) then
                ntmp      = ntmp + 1
                nat(ntmp) = ii
            end if
        end do
        
        do ii = 1,nlnu,1
            ntmp    = inde(ii)
            rr      = maxval(FRC%elementData(ee)%Vnl(ntmp)%rrData)
            ll      = FRC%elementData(ee)%Vnl(ntmp)%L
            tpvol   = 4*pi/3*(rr**3)
            nnzvol  = nnzvol + tpvol*(2*ll+1)*nat
            tpvol   = 4*pi/3*((rr+dx)**3)
            innzvol = innzvol + tpvol*(2*ll+1)*nat
        end do
    end do
        
    icnnz = ceiling(innzvol/cdr)
    fnnz  = ceiling(nnzvol/fdr)
        
    if (trim(FRC%interpolation%method) == trim("fft") .or. trim(FRC%interpolation%method) == trim("interpft") .or. trim(FRC%interpolation%method) == trim("fourier")) then
        if (fnnz > product(cgn)) then
            interpVnl = .FALSE.
        else
            interpVnl = .TRUE.
        end if
    else
        if (fnnz > icnnz) then
            interpVnl = .FALSE.
        else
            interpVnl = .TRUE.
        end if
    end if
        
    return
    end subroutine pickInterpVnl
    end module pickInterpVnl_module