! mat2cell.f90
    
!***********************************
    !
    !
    !
!***********************************
    
    module mat2cell4D_module
    contains
    subroutine mat2cell4D(dagr,nx,ny,nz,tlpl,cellout)
    
    use Cell_4D_real
    
    implicit none
    
    ! input variables
    integer              :: tlpl
    real*8,  allocatable :: dagr(:,:,:,:)
    integer, allocatable :: nx(:), ny(:), nz(:)
    
    ! temporary variables
    integer :: n1dagr, n2dagr, n3dagr, nnx, nny, nnz, ntmp, ii, jj, kk, oo, pp, qq, rr
    integer :: tx, ty, tz, xt, yt, zt
    
    ! output variables
    type(Cell_4D_rea), allocatable :: cellout(:,:,:)
    
    ! body of this function
    n1dagr = size(dagr,1)
    n2dagr = size(dagr,2)
    n3dagr = size(dagr,3)
    nnx    = size(nx)
    nny    = size(ny)
    nnz    = size(nz)
    ntmp   = sum(nx)
    if (ntmp /= n1dagr) then
        write(*,*) "Error in mat2cell.f90. sum(nx) should equal to dagr's size in dimention 1."
        stop
    end if
    ntmp   = sum(ny)
    if (ntmp /= n2dagr) then
        write(*,*) "Error in mat2cell.f90. sum(ny) should equal to dagr's size in dimention 2."
        stop
    end if
    ntmp   = sum(nz)
    if (ntmp /= n3dagr) then
        write(*,*) "Error in mat2cell.f90. sum(nz) should equal to dagr's size in dimention 3."
        stop
    end if
    
    allocate(cellout(nnx,nny,nnz))
    do ii = 1,nnx,1
        do jj = 1,nny,1
            do kk = 1,nnz,1
                tx = nx(ii)
                ty = ny(jj)
                tz = nz(kk)
                xt = 0
                do oo = 1,ii,1
                    xt = xt + nx(oo)
                end do
                yt = 0
                do pp = 1,jj,1
                    yt = yt + ny(pp)
                end do
                zt = 0
                do qq = 1,kk,1
                    zt = zt + nz(qq)
                end do
                
                allocate(cellout(ii,jj,kk)%vata(tx,ty,tz,tlpl))
                forall (oo=1:tx,pp=1:ty,qq=1:tz,rr=1:tlpl)
                    cellout(ii,jj,kk)%vata(oo,pp,qq,rr) = dagr(xt-nx(ii)+oo,yt-ny(jj)+pp,zt-nz(kk)+qq,rr)
                end forall
            end do
        end do
    end do
    
    return
    end subroutine mat2cell4D
    end module mat2cell4D_module
    