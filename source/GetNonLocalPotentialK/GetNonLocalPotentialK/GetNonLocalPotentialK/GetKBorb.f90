! GetKBorb.f90
    
!********************************************
    !
    !
    !
!********************************************
    
    module GetKBorb_module
    contains
    subroutine GetKBorb(FRC,kpt)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    fgridn    = FRC%domain%fgridn
    fn        = product(fgridn)
    anterpvnl = (.not. FRC%interpolation%vnl)
    call  getrsphase(FRC,kpt,fgridn,eikr)
    
    if (FRC%spin%SOI) then
        
        write(*,*) "Error in GetKBorb. SOI is not avalable now."
        stop
        
        !kbcell    = FRC%potential%vnlSO%kbcell
        !kbvec     = FRC%potential%vnlSO%kvec
        !kbsparse  = FRC%potential%vnlSO%kbsparse
        !jorb      = FRC%potential%vnlSO%Jorb
        !lorb      = FRC%potential%vnlSO%Lorb
        !morb      = FRC%potential%vnlSO%Morb
        !oorb      = FRC%potential%vnlSO%Oorb
        !eorb      = FRC%potential%vnlSO%Eorb
        !evec      = FRC%potential%vnlSO%evec
        !spinnorb  = jorb - lorb
        !do ss = 1,2,1
        !   call reduceOper(kbcell(ss,:),kbvec,kpt,kbsparse,kborb)
        !    morbtmp = morb(spinorb == ss-1.5)
        !    call sph2real(morbtmp,U)
        !    kborb%dataA = matmul(kborb%dataA,transpose(U))
        !    if (kbsparse) then
        !        call spdiags(eikr,0,fn,fn,kborb)
        !        kborbA = matmul(kborb,kborb%dataA)
        !    else
        !        call bsxfunTimes(eikr,kborb%dataA,kborbA)
        !    end if
        !    if (anterpvnl) then
        !        kborbA = FRC%interpolation%AntFun(kborb)
        !    end if
        !    FRC%potential%vnl%KBorbSO(ss) = kborbA
        !end do
        !call calcKBSO(eorbmjorb,lorb,morb,oorb,evec,FRC%potential%vnl%KBESO)
    end if
    
    kbcell    = FRC%potential%vnl%kbcell
    kbvec     = FRC%potential%vnl%kbvec
    kbsparse  = FRC%potential%vnlSO%kbsparse
    call reduceOper(kbcell(ss,:),kbvec,kpt,kbsparse,kborbA)
    
    if (kbsparse) then
        call spdiags(eikr,0,fn,fn,kborbA)
        kborbA = matmul(kborb,kborb%dataA)
    else
        call bsxfunTimes(eikr,kborb%dataA,kborbA)
    end if
    if (anterpvnl) then
        kborbA = FRC%interpolation%AntFun(kborbA)
    end if
    
    FRC%potential%vnl%KBorb = kborbA
    FRC%potential%vnl%kpt   = kpt
    
    return
    end subroutine GetKBorb
    end module GetKBorb_module