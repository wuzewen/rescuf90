! distmat_allgather.f90
    
!*******************************************
    !
    !
    !
!*******************************************
    
    module distmat_allgather_module
    contains
    subroutine distmat_allgather(FRC,dA,A)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    integer :: mpirank, mpisize, n1, n2, n3
    
    ! ouput variables
    
    ! body of this function
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    n1      = size(dA%dataA,1)
    n2      = size(dA%dataA,2)
    n3      = size(dA%dataA,3)
    
    if (mpisize == 1) then
        A = dA%dataA
    else
        
        write(*,*) "Error in Distmat_allgether.f90, MPI is not avalable now."
        stop
        
        !if (mpirank == 0) then
        !    call GetGlobalInd(mpirank,dA,iloc,jloc)
        !    ntmp = 0
        !    do ii = 1,n1,1
        !        do jj = 1,n2,1
        !            if (dA%dataA(ii,jj) /= 0) then
        !                ntmp = ntmp+1
        !            end if
        !        end do
        !    end do
        !    allocate(iA(ntmp),jA(ntmp),valA(ntmp))
        !    ntmp = 0
        !    do ii = 1,n1,1
        !        do jj = 1,n2,1
        !            if (dA%dataA(ii,jj) /= 0) then
        !                ntmp = ntmp+1
        !                iA(ntmp)   = ii
        !                jA(ntmp)   = jj
        !                valA(ntmp) = dA%dataA(ii,jj)
        !            end if
        !        end do
        !    end do
        !    allocate(Atmp(1,1)%dataA(ntmp,3))
        !    Atmp(1,1)%dataA(:,1) = iA(:)
        !    Atmp(1,1)%dataA(:,2) = jA(:)
        !    Atmp(1,1)%dataA(:,3) = valA(:)
        !    deallocate(iA,jA,valA)
        !end if
        
        !do ii = 0,mpisize-1,1
        !    if (mpirank == ii) then
        !        call GetGlobalInd(mpirank,dA,iloc,jloc)
        !        ntmp = 0
        !        do ii = 1,n1,1
        !            do jj = 1,n2,1
        !                if (dA%dataA(ii,jj) /= 0) then
        !                    ntmp = ntmp+1
        !                end if
        !            end do
        !        end do
        !        allocate(iA(ntmp),jA(ntmp),valA(ntmp))
        !        ntmp = 0
        !        do ii = 1,n1,1
        !            do jj = 1,n2,1
        !                if (dA%dataA(ii,jj) /= 0) then
        !                    ntmp = ntmp+1
        !                    iA(ntmp)   = ii
        !                    jA(ntmp)   = jj
        !                    valA(ntmp) = dA%dataA(ii,jj)
        !                end if
        !            end do
        !        end do
        !        allocate(AtmpT(ntmp,3))
        !        AtmpT(:,1) = iA(:)
        !        AtmpT(:,2) = jA(:)
        !        AtmpT(:,3) = valA(:)
        !        deallocate(iA,jA,valA)
        !        !MPI_Send_variable(AtmpT,0,0)
        !    else if (mpirank == 0) then
        !        Atmp(ii+1,1) = MPI_Recv_variable(ii,0)
        !    end if
        !end do
        !if (mpirank == 0) then
        !    ! unfinished
        !end if
        !Atmp = MPI_Bcast_variable(Atmp,0)
        !A=***
    end if
    
    return
    end subroutine distmat_allgather
    end module distmat_allgather_module