! getFrequency.f90
    ! a function
    
    module getFrequency_module
    contains
    subroutine getFrequency(n,bc,k)
    
    implicit none
    
    ! input valuables
    integer               :: n, n1, bc, i
    
    ! temporery valuables
    real*8                :: pi
    integer               :: ii, ntmp
    integer, allocatable  :: arrn(:)
    complex               :: imnu
    
    ! output valuables
    complex, allocatable  :: k(:)
    
    imnu = (0,1)
    n1 = n/2+2
    allocate(arrn(n))
    !do i = 1,n,1
    !    arrn1(i) = i
    !    if (i<n1) then
    !        arrn2(i) = i-1
    !    else
    !        arrn2(i) = i-n-1
    !    end if
    !end
    !arrn2 = 
    !arrn3 = arrn1-1
    if (bc==0) then
        
        write(*,*) "Error in getFrequency.f90. bc = 0 is not available now."
        stop
        
        !forall(ii=1:n)
        !    arrn(ii) = ii
        !end forall
        !k = cmplx(n)*sqrt(2*cos(cmplx(pi)*cmplx(arrn)/cmplx(n+1))-2)
    else if (bc == 1) then
        ntmp = n/2
        forall(ii=1:ntmp)
            arrn(ii) = ii
        end forall
        forall(ii=ntmp+1:n)
            arrn(ii) = ii-n-1
        end forall
        k = 2*imnu*cmplx(pi)*cmplx(arrn)
    else if (bc == 2) then
        
        write(*,*) "Error in getFrequency.f90. bc = 2 is not available now."
        stop
        
        !forall(ii=1:n)
        !    arrn(ii) = ii-1
        !end forall
        
        !k = cmplx(n)*sqrt(2*cos(cmplx(pi)*cmplx(arrn)/cmplx(n))-2)
    end if
    
    return
    end subroutine getFrequency
    end module getFrequency_module
