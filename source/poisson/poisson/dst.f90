! dst.f90
! 
    
    module dst_module
    contains
    subroutine dst(x,dim,dstout)
    
    implicit none
    
    ! input valuables
    integer              :: dim
    complex, allocatable :: x(:,:,:)
    
    ! temporery valuables
    integer              :: L, M, N
    integer              :: i
    real*8, allocatable  :: pad(:,:,:), flip(:,:,:)
    real*8, allocatable  :: dsttmp(:,:,:)
    
    ! output valuables
    real*8, allocatable  :: dstout(:,:,:)
    
    ! body of this function
    L = size(x,1)
    M = size(x,2)
    N = size(x,3)
    
    allocate(flip(L,M,N))
    if (dim==1) then
        allocate(pad(1,M,N))
        allocate(dsttmp(2*L+2,M,N))
        call flipdim(x,dim,flip)
        do i = 1,2*L+2,1
            if (i==1) then
                dsttmp(i,:,:) = pad(1,:,:)
            else if (i>1 .and. i<=L+1) then
                dsttmp(i,:,:) = x(i-1,:,:)
            else if (i==L+2) then
                dsttmp(i,:,:) = pad(1,:,:)
            else if (i>L+2 .and. i<=2*L+2) then
                dsttmp(i,:,:) = -flip(i-L-2,:,:)
            end if
        end do
        call fft(dsttmp,dim,dstout)
    else if (dim==2) then
        allocate(pad(L,1,N))
        allocate(dsttmp(L,2*M+2,N))
        call flipdim(x,dim,flip)
        do i = 1,2*M+2,1
            if (i==1) then
                dsttmp(:,i,:) = pad(:,1,:)
            else if (i>1 .and. i<=M+1) then
                dsttmp(:,i,:) = x(:,i-1,:)
            else if (i==M+2) then
                dsttmp(:,i,:) = pad(:,1,:)
            else if (i>M+2 .and. i<=2*M+2) then
                dsttmp(:,i,:) = -flip(:,i-M-2,:)
            end if
        end do
        call fft(dsttmp,dim,dstout)
    else if (dim==3) then
        allocate(pad(L,M,1))
        allocate(dsttmp(L,M,2*N+2))
        call flipdim(x,dim,flip)
        do i = 1,2*N+2,1
            if (i==1) then
                dsttmp(:,:,i) = pad(:,:,1)
            else if (i>1 .and. i<=N+1) then
                dsttmp(:,:,i) = x(:,:,i-1)
            else if (i==N+2) then
                dsttmp(:,:,i) = pad(:,:,1)
            else if (i>N+2 .and. i<=2*N+2) then
                dsttmp(:,i,:) = -flip(:,:,i-N-2)
            end if
        end do
        call fft(dsttmp,dim,dstout)
    end if
    
    end subroutine dst
    end module dst_module