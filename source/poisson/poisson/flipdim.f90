! flipdim.f90
    
    module flipdim_module
    contains
    function flipdim(A,dim)
    
    implicit none
    
    ! input valuables
    integer             :: dim
    real*8, allocatable :: A(:,:,:)
    
    ! temporery valuables
    integer             :: L, M, N
    integer             :: i, j, k
    ! output valuables
    real*8, allocatable :: flipdim(:,:,:)
    
    L = size(A,1)
    M = size(A,2)
    N = size(A,3)
    
    allocate(flipdim(L,M,N))
    
    if (dim==1) then
        do i = 1,L,1
            flipdim(L+1-i,:,:) = A(i,:,:)
        end do
    else if (dim==2) then
        do i = 1,M,1
            flipdim(:,M+1-i,:) = A(:,i,:)
        end do
    else if (dim==3) then
        do i = 1,N,1
            flipdim(:,:,N+1-i) = A(:,:,i)
        end do
    end if
    
    return
    end function flipdim
    end module flipdim_module