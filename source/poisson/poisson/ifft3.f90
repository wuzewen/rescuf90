! ifft3.f90
    
    module ifft3_module
    contains
    subroutine ifft3(A,dim,ifftout)
    
    implicit none
    
    ! input valuables
    integer               ::  dim
    complex, allocatable  ::  A(:,:,:)
    
    ! temporery valuables
    integer               ::  i, j
    integer               ::  N
    complex, parameter    ::  imnu = (0,1), pi = (3.14159265354,0)
    
    ! output valuables
    complex, allocatable  ::  ifftout(:,:,:)
    
    ! body of this function
    N    = size(A,dim)
    ifftout = 0
    do i = 1,N,1
        do j = 1,N,1
        if (dim == 1) then
            ifftout(i,:,:) = ifftout(i,:,:)+A(j,:,:)*exp(imnu*2*pi*cmplx(i-1)*(j-1)/cmplx(N))
        else if (dim == 2) then
            ifftout(:,i,:) = ifftout(:,i,:)+A(:,j,:)*exp(imnu*2*pi*cmplx(i-1)*(j-1)/cmplx(N))
        else if (dim == 3) then
            ifftout(:,:,i) = ifftout(:,:,i)+A(:,:,j)*exp(imnu*2*pi*cmplx(i-1)*(j-1)/cmplx(N))
        end if
        end do
    end do
    ifftout = ifftout/cmplx(N)
    
    end subroutine ifft3
    end module ifft3_module
    