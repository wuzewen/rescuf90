! idst.f90
    
    module idst_module
    contains
    subroutine idst(x,dim,idstout)
    
    use dst_module
    
    implicit none
    
    ! input valuables
    complex, allocatable :: x(:,:,:)
    integer           :: dim
    
    ! temporery valuables
    integer           :: M
    
    ! output valuables
    real*8, allocatable :: idstout(:,:,:)
    
    ! body of this function
    
    M = size(x,dim)
    call dst(x,dim,idstout)
    idstout = (2/real(M)+1)*idstout
    
    return
    end subroutine idst
    end module idst_module