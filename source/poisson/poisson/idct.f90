! idct.f90
    
    module idct_module
    contains
    subroutine idct(x,dim,idctout)
    
    implicit none
    
    ! input valuables
    integer             :: dim
    real*8, allocatable :: x(:,:,:)
    
    ! temporery valuables
    integer             :: M
    
    ! output valuables
    real*8, allocatable :: idctout(:,:,:)
    
    ! body of this function
    M       = size(x,dim)
    call dct(x,dim,idctout)
    idctout = (2/real(M-1))*idctout
    
    return
    end subroutine idct
    end module idct_module