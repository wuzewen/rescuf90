!  Element.f90 
!****************************************************************************
!
!  PROGRAM: Element
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module Basis

    implicit none

    ! Variables

    ! Body of Element
    type :: RelPseudoP
        integer             :: N, L
        real*8              :: J, Rc
        real*8, allocatable :: rrData(:), drData(:), vvData_screened(:), vvData_unscreend(:)
    end type RelPseudoP
    
    type :: Vlocal
        real*8, allocatable :: rrData(:), drData(:), vvData(:)
        real*8              :: Rcore, Ncore
    end type Vlocal
    
    type :: Rlocal
        real*8, allocatable :: rrData(:), drData(:), rhoData(:)
    end type Rlocal
    
    type :: OrbitalSet
        real*8, allocatable :: rrData(:), drData(:), frData(:), qqData(:), fqData(:), qwData(:)
        integer           :: N, L
        real*8            :: E, Population, CoulombEnergyU, ExchangeEnergyJ
        character(len=20) :: OrbitalType
    end type OrbitalSet
    
    type :: Atom
        integer           :: Z, N
        character(len=4)  :: Symbol
        character(len=20) :: shell
        character(len=20) :: name
        real*8            :: mass
    end type Atom
    
    type :: Vnlocal
        real*8, allocatable :: rrData(:), drData(:), vvData(:), qqData(:), fqData(:), qwData(:)
        integer :: L
        real*8  :: KBenergy, KBcosine, E
        logical :: isGhost
    end type Vnlocal
    
    type :: Vna
        real*8, allocatable :: rrData(:), drData(:), vvData(:)
    end type Vna
    
    type :: Rna
        real*8, allocatable :: rrData(:), drData(:), rhoData(:)
    end type Rna
    
    type :: VnlocalSO
        real*8, allocatable  :: vsoData(:),rrData(:), drData(:), vvData(:), qqData(:), fqData(:)
    end type VnlSO
    
    type :: Element
        type(RelPseudoP),allocatable :: REPP(:)
        type(OrbitalSet),allocatable :: OBTS(:)
        type(Vnlocal),   allocatable :: VnL(:)
        type(VnlocalSO), allocatable :: VnlSO(:)
    end type Element
    
    end module Basis

