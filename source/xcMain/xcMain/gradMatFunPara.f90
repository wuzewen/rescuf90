! gradMatFunPara.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    module gradMatFunPara_module
    contains
    subroutine gradMatFunPara(FRC,A,D,kpt,avec,gA)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    call inversion(avec,j1)
    j1      = transpose(j1)
    mA      = A%m
    nA      = A%n
    call cellFunSize(D,ngrid)
    
    call ModBCDist(FRC,A,ngrid(1)*ngrid(2),1,mpisize,1,A)
    call InitDistArray(FRC,A,ngrid(1)*ngrid(2),1,mpisize,1,gA)
    call GetGlobalInd(mpirank,gA,iloc,jloc)
    allocate(gA%vata(size(iloc),size(jloc)))
    gA%vata = 0
    
    if (any(kpt /= 0)) then
        kpt = 2*imnu*pi*kpt*j1
        do ii = 1,nA,1
            call bsxfunTimes(A%vata(:,ii),kpt,tmp)
            gA%vata(:,3*ii-2:3*ii) = tmp
        end do
    end if
    
    call GetGlobalInd(mpirank,A,iloc,jloc)
    m_loc = size(iloc)
    A%vata = reshape(A%vata,ngrid(1)*ngrid(2),?,A%vata)
    call rkronprod(D(1),A%vata,tmp)
    gtmp(:,?:?) = reshape(tmp,(/m_loc,?/))
    call rkronprod(D(2),A%vata,tmp)
    gtmp(:,?:?) = reshape(tmp,(/m_loc,?/))
    A%vata = reshape(A%vata,(/?,nA/))
    A%vata reshape(transpose(A%vata),(/nA*ngrid(1)*ngrid(2),?/))
    A%m = nA*ngrid(1)*ngrid(2)
    A%n = ngrid(3)
    A%mblock = 1
    A%nblock = 1
    A%mproc  = 1
    A%nproc  = mpisize
    
    call ModBCDist(FRC,A,1,1,mpisize,1,A)
    A%vata = matmul(A%vata,transpose(D(3)))
    call ModBCDist(FRC,A,1,1,1,mpisize)
    A%vata = transpose(reshape(A%vata,(/nA,?/)))
    gtmp(:,?:?) = reshape(A%vata,(/m_loc,?/))
    
    if (size(gtmp)>1) then
        do ii = 1,nA,1
            gA%vata(:,3*ii-2:3*ii) = gA%vata(:,3*ii-2:3*ii)+gtmp(:,ii:nA:3*nA)*j1
        end do
    end if
    
    return
    end subroutine gradMatFunPara
    end module gradMatFunPara_module