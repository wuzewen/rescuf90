! genGradFunPara.f90
    
!*****************************************************
    !
    !
    !
!*****************************************************
    
    module genGradFunPara_module
    contains
    subroutine genGradFunPara(FRC,ng,kpt,gradFun)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    avec = FRC%domain%latvec
    
    if (trim(FRC%diffop%method) == "fft") then
        call fftFreq(ng,D)
        call gradFFTfunPara(FRC,x,D,kpt,avec,gradFun)
    else
        if (ng == FRC%domain%cgridn) then
            D(1) = FRC%diffop%Du
            D(2) = FRC%diffop%Dv
            D(3) = FRC%diffop%Dw
        else if (all(ng == FRC%domain%cgridn)) then
            D(1) = FRC%diffop%fDu
            D(2) = FRC%diffop%fDv
            D(3) = FRC%diffop%fDw
        else
            if (trim(FRC%diffop%method) == "fd") then
                acc = FRC%diffop%accuracy
                bc  = FRC%domain%boundary
                call FdGradMat(ng,acc,bc,Gu,Gv,Gw)
            else
                call FtGradMat(ng,Gu,Gv,Gw)
            end if
            D(1) = Gu
            D(2) = Gv
            D(3) = Gw
        end if
        call GradMatFunPara(FRC,x,D,kpt,avec,gradFun)
    end if
    
    return
    end subroutine genGradFunPara
    end module genGradFunPara_module
        