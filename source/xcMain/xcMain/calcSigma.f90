! calcSigma.f90
    
!*******************************************************
    !
    !
    !
!*******************************************************
    
    module calcSigma_module
    contains
    subroutine calcSigma(FRC,rho,pcc,sigma,gRho)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    mpisize = FRC%mpi%mpisize
    fgridn  = FRC%domain%fgridn
    fn      = rho%m
    lxcstat = FRC%functional%libxc
    
    call genGradFunPara(FRC,fgridn,gfun)
    gRho = gFun(rho)
    call ModBCDist(FRC,gRho,fn/mpisize,1,mpisize,1,gRho)
    
    if (size(FRC%rho%gradRpc) /= 0) then
        gRho%vata(:,1:3) = gRho%vata(:,1:3) + pcc*FRC%rho%gradRpc%vata
        gRho%vata(:,4:6) = gRho%vata(:,4:6) + pcc*FRC%rho%gradRpc%vata
    end if
    
    sigma = gRho
    
    if (lxcstat) then
        sigma%vata(:,1) = sum(sigma%vata(:,1:3)**2,2)
        sigma%vata(:,2) = sum(sigma%vata(:,1:3)*sigma%vata(:,4:6),2)
        sigma%vata(:,3) = sum(sigma%vata(:,4:6)**2,2)
        sigma%n         = 3
    end if
    
    return
    end subroutine calcSigma
    end module calcSigma_module