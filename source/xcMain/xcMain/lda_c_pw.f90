! lda_c_pw.f90
    
!****************************************************
    !
    !
    !
!****************************************************
    
    module lda_c_pw_module
    contains
    subroutine lda_c_pw(rho,order,zk,vrho,v2rho2)
    
    use greatGfunction_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: rho(:,:)
    integer             :: order
    
    ! temporary variables
    real*8  :: tol, fz20, pi
    integer :: ii, jj, ntmp
    real*8, allocatable :: dens(:), rs(:,:), zeta(:), fz(:), z4(:), ecp(:),vcp(:), ecf(:), vcf(:)
    real*8, allocatable :: alpha(:), dalpha(:), d2alpha(:), z3(:), dfz(:), dedrs(:), dedz(:)
    real*8, allocatable :: drs(:), vrhot(:), nonsense(:)
    
    ! output variables
    real*8, allocatable :: zk(:), vrho(:,:), v2rho2(:,:)
    
    ! body of this function
    pi  = 3.14159265354
    tol = 10**-15
    do ii = 1,size(rho,1),1
        do jj = 1,size(rho,2),1
            if (rho(ii,jj) < tol) then
                rho(ii,jj) = tol
            end if
        end do
    end do
    fz20 = 1.709921
    ntmp = size(rho,1)
    allocate(dens(ntmp))
    dens = sum(rho,2)
    allocate(rs(ntmp,2))
    rs(:,1) = (3.000/4.000/pi/dens(:))**(1.000/6.000)
    rs(:,2) = (3.000/4.000/pi/dens(:))**(1.000/3.000)
    allocate(zeta(ntmp))
    zeta(:) = (rho(:,1)-rho(:,2))/dens
    allocate(fz(ntmp))
    fz      = (1+zeta)**(4.00/3.00)+(1-zeta)**(4.00/3.00)-2.00
    fz      =  fz/(2.00**(4.00/3.00)-2.00)
    allocate(z4(ntmp))
    z4      =  zeta**4
    
    if (order == 0) then
        write(*,*) "Error in lda_c_pw.f90. order should not be 0 now, please use order = 1."
        stop
        !call greatGfunction(order,0,rs,ecp)
        !call greatGfunction(order,1,rs,ecf)
        !call greatGfunction(order,2,rs,alpha)
    else if (order == 1) then
        call greatGfunction(order,0,rs,ecp,vcp,nonsense)
        call greatGfunction(order,1,rs,ecf,vcf,nonsense)
        call greatGfunction(order,2,rs,alpha,dalpha,nonsense)
    else if (order == 2) then
        write(*,*) "Error in lda_c_pw.f90. order should not be 2 now, please use order = 1."
        stop
        !call greatGfunction(order,0,rs,ecp,vcp,fcp)
        !call greatGfunction(order,1,rs,ecf,vcf,fcf)
        !call greatGfunction(order,2,rs,alpha,dalpha,d2alpha)
    end if
    
    alpha = -alpha
    !allocate(zk(ntmp))
    zk    =  ecp+z4*fz*(ecf-ecp-alpha/fz20)+fz*alpha/fz20
    
    if (order<1) then
        return
    end if
    
    dalpha    = -dalpha
    allocate(z3(ntmp))
    z3        =  zeta**3.00
    allocate(dfz(ntmp))
    dfz       = (1.00+zeta)**(1.00/3.00)-(1-zeta)**(1.00/3.00)
    dfz       =  4.00/3.00*dfz/(2.00**(4.00/3.00)-2.00)
    allocate(dedrs(ntmp))
    dedrs     =  vcp+z4*fz*(vcf-vcp-dalpha/fz20)+fz*dalpha/fz20
    allocate(dedz(ntmp))
    dedz      = (4.0*z3*fz+z4*dfz)*(ecf-ecp-alpha/fz20)+dfz*alpha/fz20
    allocate(drs(ntmp))
    drs       = -1.00/3.00*rs(:,2.00)/dens
    allocate(vrhot(ntmp))
    vrhot     =  zk+dens*dedrs*drs
    !allocate(vrho(ntmp,2))
    vrho(:,1) =  vrhot-(zeta-1.00)*dedz
    vrho(:,2) =  vrhot-(zeta+1.00)*dedz
    
    if (order < 2) then
        return
    end if
    
    write(*,*) "Error in lda_c_pw.f90. order should not be 2 now, please use order=1."
    !d2alpha = -d2alpha
    !z2      =  zeta**2
    !d2fz    = (1+zeta)**(-2/3)+(1-zeta)**(-2/3)
    !d2fz    =  4/9*d2fz/(2**(4/3)-2)
    !d2edrs2 =  fcp+z4*fz*(fcf-fcp-d2alpha/fz20)+fz*d2alpha/fz20
    !d2edrsz = (4*z3*fz+z4*dfz)*(vcf-vcp-dalpha/fz20)+dfz*dalpha/fz20
    !d2edz2  = (12*z2*fz+8*z3*dfz+z4*d2fz)*(ecf-ecp-alpha/fz20)+d2fz*alpha/fz20
    !d2rs    = -4/3*drs/dens
    
    !v2rho(1,:)  = dedrs*(2*drs+dens*d2rs)+dens*d2edrs2*drs*drs
    !sigmat(1,:) = -1
    !sigmat(3,:) =  1
    !sigmat(2,1) =  1
    !sigmat(2,2) =  1
    
    !do ii = 3,1,-1
    !    v2rho2(ii,:) = v2rho2(1,:) - d2edrsz*(2.0*zeta + sigmat(ii,1) + sigmat(ii,2))*drs + (zeta + sigmat(ii,1))*(zeta + sigmat(ii,2))*d2edz2/dens
    !end do
    
    return
    end subroutine lda_c_pw
    end module lda_c_pw_module