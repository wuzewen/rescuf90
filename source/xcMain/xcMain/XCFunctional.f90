! XCFunctional.f90
    
!***************************************************
    !
    !
    !
!***************************************************
    
    module XCFunctional_module
    contains
    subroutine XCFunctional(outflag,functional,rho,sig,lapl,tau,exc,vxc,vsig,vlapl,vtau,fxc)
    
    use lda_c_pw_module
    use lda_x_module
    
    implicit none
    
    ! input variables
    integer             :: outflag
    character(len=20)   :: functional
    real*8, allocatable :: rho(:,:), sig(:,:), lapl(:,:), tau(:,:)
    
    ! temporary variables
    integer              :: BUFFERSIZE, npoint, ii, jj, ntmp
    integer, allocatable :: inde(:)
    real*8, allocatable  :: vxc(:,:), fxc(:,:), exc(:), vsig(:,:), vlapl(:,:)
    real*8, allocatable  :: vtau(:,:), nonsense1(:), nonsense2(:,:), exct(:)
    
    ! output variables
    
    ! body of this function
    BUFFERSIZE = 2**17
    npoint     = size(rho,2)
    
    if (trim(functional) == "XC_LDA_X" .or. trim(functional) == "XC_LDA_C_PW") then
        if (outflag == 3) then
            !allocate(vxc(2,npoint))
            !allocate(fxc(3,npoint))
        else
            !allocate(exc(npoint))
            !allocate(vxc(2,npoint))
        end if
    else if (trim(functional) == "XC_GGA_X_PBE" .or. trim(functional) == "XC_GGA_C_PBE") then
        write(*,*) "Error in XCFunctional.f90. GGA is not available now."
        stop
        !allocate(exc(npoint))
        !allocate(vxc(2,npoint))
        !allocate(vsig(6,npoint))
    else if (trim(functional) == "XC_MGGA_X_TB09") then
        write(*,*) "Error in XCFunctional.f90. mGGA is not available now."
        !allocate(vxc(2,npoint))
    else
        write(*,*) "WARNING in XCFunctional.f90. Something wrong with XC functional."
    end if
    
    do ii = 1,npoint,BUFFERSIZE
        ntmp = min(npoint,ii+BUFFERSIZE-1)
        allocate(inde(ntmp))
        forall(jj=ii:ntmp)
            inde(jj) = jj
        end forall
        
        select case(functional)
        case("XC_LDA_X")
            if (outflag == 3) then
                call lda_x(rho,2,nonsense1,vxc,fxc)
            else
                !exct(:) = exc(1,:)
                call lda_x(rho,1,exc,vxc,nonsense2)
            end if
        case("XC_LDA_C_PW")
            if (outflag == 3) then
                call lda_c_pw(rho,2,nonsense1,vxc,fxc)
            else
                !exct(:) = exc(1,:)
                call lda_c_pw(rho,1,exc,vxc,nonsense2)
            end if
        case("XC_GGA_X_PBE")
            write(*,*) "XC_GGA_X_PBE is not available now, please use XC_LDA_X."
            stop
            !call xPBE(rho(:,inde),sig(:,inde),exc(:,inde),DExDP,DExDN,DExDGP,DExDGN)
            !vxc(?:?,inde)  = DExDP
            !vxc(?:?,inde)  = DExDN
            !vsig(?:?,inde) = DExDGP
            !vsig(?:?,inde) = DExDGN
        case("XC_GGA_C_PBE")
            write(*,*) "XC_GGA_C_PBE is not available now, please use XC_LDA_C_PW."
            stop
            !call cPBE(rho(:,inde),sig(:,inde),exc(:,inde),DExDP,DExDN,DExDGP,DExDGN)
            !vxc(?:?,inde)  = DExDP
            !vxc(?:?,inde)  = DExDN
            !vsig(?:?,inde) = DExDGP
            !vsig(?:?,inde) = DExDGN
        case("XC_MGGA_X_TB09")
            write(*,*) "XC_MGGA_X_TB09 is not available now, please use XC_LDA_X."
            stop
            !call MBJX(rho(:,inde),sig(:,inde),lapl(:,inde),tau(:,inde),vxc(:,inde))
        end select
        
    end do
    
    !vxc   = reshape(vxc,2,ntmp)
    !vsig  = reshape(vsig,6,ntmp)
    !vlpal = reshape(vlapl,2,ntmp)
    !vtau  = reshape(vtau,2,ntmp)
    
    return
    end subroutine XCFunctional
    end module XCFunctional_module