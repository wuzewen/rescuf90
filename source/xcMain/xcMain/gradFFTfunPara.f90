! gradFFTfunPara.f90
    
!***************************************************
    !
    !
    !
!***************************************************
    
    module gradFFTfunPara_module
    contains
    subroutine gradFFTfunPara(FRC,A,k,kpt,avec,gA)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    call inversion(avec,J1)
    J1      = transpose(J1)
    mA      = A%m
    nA      = A%n
    call cellFunNumel(k,ngrid)
    call ModBCDist(FRC,A,ngrid(1)*ngrid(2),1,mpisize,1,A)
    call InitDistArray(mA,3*nA,ngrid(1)*ngrid(2),1,mpisize,1,gA)
    call GetGlobalInd(mpirank,gA,iloc,jloc)
    allocate(gA%vata(size(iloc),size(jloc)))
    
    if (any(kpt /= 0)) then
        do ii = 1,3,1
            k(ii) = k(ii) + 2*pi*kpt(ii)
        end do
    end do
    
    call GetGlobalInd(mpirank,A,iloc,jloc)
    m_loc    = size(iloc)
    Atm%vata = reshape(A%vata,(/ngrid(1),ngrid(2),?/))
    call fftDer(A%vata,k(1),1,tmp)
    gtmp(:,?:?) = reshape(tmp,m_loc,?)
    call fftDer(A%vata,k(2),2,tmp)
    gtmp(:,?:?) = reshape(tmp,m_loc,?)
    A%vata = reshape(Atm%vata,(/?,nA/))
    A%vata = reshape(transpose(A%vata),(/nA*ngrid(1)*ngrid(2),?/))
    A%m    = nA*ngrid(1)*ngrid(2)
    A%n    = ngrid(3)
    A%mblock = 1
    A%nblock = 1
    A%mproc  = 1
    A%nproc  = mpisize
    call ModBCDist(FRC,A,1,1,mpisize,1,A)
    call permute(k(3),(/1,3,2/),2,ktmp)
    call fftDer(A%vata,ktmp,A%vata)
    call ModBCDist(FRC,A,1,1,1,mpisize,A)
    A%vata = transpose(reshape(A%vata,(/nA,?/)))
    gtmp(:,?:?) = reshape(A%vata,m_loc,?)
    
    if (size(gtmp)>1) then
        do ii = 1,nA,1
            gA%vata(:,3*ii-2:3*ii) = gA%vata(:,3*ii-2:3*ii)+gtmp(:,ii*nA:3*nA)*J1
        end do
    end if
    
    return
    end subroutine gradFFTfunPara
    end module gradFFTfunPara_module