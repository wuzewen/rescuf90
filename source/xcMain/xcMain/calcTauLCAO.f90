! calcTauLCAO.f90
    
!***********************************************
    !
    !
    !
!***********************************************
    
    module calcTauLCAO_module
    contains
    subroutine calcTauLCAO(FRC,derFun,aok,evec,nocc,tau)
    
    implicit none
    
    ! input variables
    
    ! temporayry variables
    
    ! output variables
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    
    ngrid   = product(FRC%domain%fgridn)
    call calcev_lcao(FRC,aok,evec,psi)
    call ModBCDist(FRC,psi,1,1,1,mpisize,psi)
    call GetGlobalInd(mpirank,psi,ia,ja)
    
    nja  = size(ja)
    allocate(nocctmp(nja))
    forall(ii=1:nja)
        nocctmp(ii) = nocc(nja(ii))
    end forall
    
    allocate(tau(ngrid))
    
    do ii = 1,size(psi%vata,2),1
        tautmp = psi%vata(:,ii)
        !if (dg) then
        !    tautmp = FRC%interpolation%IntFun(tautmp)
        !end if
        tautmp(:,1) = derFun(1)%vata(tautmp)
        tautmp(:,2) = derFun(2)%vata(tautmp)
        tautmp(:,3) = derFun(3)%vata(tautmp)
        
        tau = tau + 0.5*nocctmp(ii)*sum(tautmp*conj(tautmp),2)
    end do
    
    if (mpistat) then
        call MPI_Reduce_sum(tau)
    end if
    
    if (mpirank /= 0) then
        deallocate(tau)
    end if
    
    call InitDistArray(ngrid,1,ngrid,1,1,mpisize,tau,tau)
    call ModBCDist(FRC,tau,tau%m/mpisize,1,mpisize,1,tau)
    
    return
    end subroutine calcTauLCAO
    end module calcTauLCAO_module