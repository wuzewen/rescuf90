! fftGrad.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module fftGrad_module
    contains
    subroutine fftGrad(x,n,k,kpt,xtmp)
    
    if (kpt == 0) then
        isr = .TRUE.
    end if
    
    nx = size(x,2)
    
    xtmp0 = reshape(x,(/n,nx/))
    call fft(xtmp0,?,1,xtmp1)
    call fft(xtmp1,?,2,xtmp0)
    call fft(xtmp0,?,3,xtmp1)
    
    call bsxfunTimes(xtmp1,imnu*(k+kpt),xtmp0)
    call ifft(xtmp0,?,1,xtmp1)
    call ifft(xtmp1,?,2,xtmp0)
    call ifft(xtmp0,?,3,xtmp1)
    
    xtmp = reshape(xtmp1,(/product(n),nx/))
    
    return
    end subroutine fftGrad
    end module fftGrad_module