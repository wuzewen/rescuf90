! getXC.f90
    
!***************************************
    !
    !
    !
!***************************************
    
    module getXC_module
    contains
    subroutine getXC(FRC,funcname,rho,sig,lapl,tau,erho,vrho,vsig,vlapl,vtau)
    
    ! data type module
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    use xcFunctional_module
    
    ! function module
    
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    character(len=20)               :: funcname
    type(VataMN_2D)                 :: rho, sig, lapl, tau
    
    ! temporary variables
    logical :: mpistat, lxcstat
    integer :: fgridn(3), fn
    
    ! output variables
    !type(VataMN_1D)   :: erho, vrho, 
    type(VataMN_2D)     :: vsig, vlapl, vtau
    real*8, allocatable :: erho(:), vrho(:,:), fxc(:,:)
    
    ! body of this function
    mpistat = FRC%mpi%status
    fgridn  = FRC%domain%fgridn
    fn      = product(fgridn)
    lxcstat = FRC%functional%libxc
    
    if (trim(funcname) == "XC_MGGA_X_TB09") then
        
        write(*,*) "Error in getXC.f90. mGGA is not available now."
        stop
        
        !mbjLim = FRC%functional%mbjLim
       ! 
        !if (size(FRC%functional%mbjc) /= 0) then
        !    c = FRC%functional%mbjc
        !else
        !    alpha = -0.012
        !    beta  =  1.023
        !    vol   =  abs(det(FRC%domain%latvec))
        !    dr    = vol/fn
        !    if (lxcstat) then
        !        sig2 = sqrt(matmul(sig%vata,(/1,2,1/)))
        !    else
        !        sig2 = sqrt()
        !    end if
        !    c = sig2/sum(rho%vata,2)
        !    
        !    nc      = size(c)
        !    sum_rho = 0
        !    do ii = 1,nc,1
        !        if (sum(rho%vata(ii,:))<mbjLim(ii)) then
        !            c(ii) = 0
        !        else
        !            sum_rho = sum_rho + sum(rho%vata(ii,:))
        !        end if
        !    end do
        !    c = c/sum_rho*size(sum(rho%vata,2))
        !    
        !    if (mpistat) then
        !        
        !        write(*,*) "Error in getXC.f90. mpi is not available."
        !        !call MPI_Allreduce_sum(c)
        !    end if
        !    
        !    c = alpha+beta*(c/col)**0.5
        !end if
        
        !if (lxcstat) then
        !    call LibXCFunctional(1,"XC_MGGA_X_TB09",rho%vata,sig%vata,lapl%vata,tau%vata,c,nonsense,vrho,vsig)
        !    call LibXCFunctional(2,"XC_LDA_X",rho%vata,sig%vata,lapl%vata,tau%vata,ctmp,,erho,vtmp,nonsense)
        !else
        !    call LibXCFunctional(1,"XC_MGGA_X_TB09",rho%vata,sig%vata,lapl%vata,tau%vata,c,nonsense,vrho,nonsense2)
        !    call LibXCFunctional(2,"XC_LDA_X",rho%vata,sig%vata,lapl%vata,tau%vata,ctmp,,erho,vtmp,nonsense)
        !end if
        !
        !vrho = 
        !
        !if (trim(FRC%info%calculationType) == "self-consistent" .and. size(FRC%psi) == 0) then
        !    vrho = vtmp
        !end if
        !
    else if (lxcstat) then
        
        write(*,*) "Error in getXC.f90. libxc is not available now."
        stop
        
        !call LibXCFunctional(2,funcname,rho%vata,sig%vata,lapl%vata,tau%vata,erho,vrho,vsig,vlapl,vtau)
    else
        call XCFunctional(2,funcname,rho%vata,sig%vata,lapl%vata,tau%vata,erho,vrho,vsig%vata,vlapl%vata,vtau%vata,fxc)
    end if
    
    return
    end subroutine getXC
    end module getXC_module