! fftDer.f90
    
!********************************************************
    !
    !
    !
!********************************************************
    
    module fftDer_module
    contains
    subroutine fftDer(A,k,dim,kA)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    call fft(A,?,dim,kA)
    call bsxfunTimes(imnu*k,kA,kAt)
    call ifft(kAt,?,dim,kA)
    
    return
    end subroutine fftDer
    end module fftDer_module