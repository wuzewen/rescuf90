! distmat_issame_1D.f90
    
!***************************************************
    !
    !
    !
!***************************************************
    
    module distmat_issame_1D_module
    contains
    subroutine distmat_issame_1D(dA,dB,issame)
    
    use VataMN_1D_module
    
    implicit none
    
    ! input variables
    type(VataMN_1D) :: dA, dB
    
    ! temporary variables
    
    ! output variables
    logical      :: issame
    
    ! body of this function
    issame = .FALSE.
    if (dA%m == dB%m .and. dA%n == dB%n .and. dA%mblock == dB%mblock .and. dA%nblock == dB%nblock .and. dA%mproc  == dB%mproc  .and. dA%nproc == dB%nproc) then
        issame = .TRUE.
    end if
    
    return
    end subroutine distmat_issame_1D
    end module distmat_issame_1D_module
        