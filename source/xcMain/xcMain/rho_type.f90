! rho_type.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module rho_type_module
    
    type :: rho_type
        real*8, allocatable :: vata(:,:)
        integer             :: m
        integer             :: n
        integer             :: mblock
        integer             :: nblock
        integer             :: mproc
        integer             :: nproc
    end type rho_type
    
    end module rho_type_module