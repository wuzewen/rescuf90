! distmat_feval.f90
    
!***************************************************
    !
    !
    !
!***************************************************
    
    module distmat_feval_module
    contains
    subroutine distmat_feval(FRC,dA,dB,fun,dC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    mpisize = FRC%mpi%mpisize
    call distmat_issame(dA,dB,issame)
    if (mpisize == 1 .or. issame) then
        dC = dB
    else
        call ModBCDist(FRC,dB,dA%mblock,dA%nblock,dA%mproc,dA%nproc,dC)
    end if
    
    if (size(dA%vata)>0) then
        call feval(fun,dA%vata,dC%vata,dC%vata)
    end if
    
    return
    end subroutine distmat_feval
    end module distmat_feval_module
    