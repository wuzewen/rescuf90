! calcXCinput.f90
    
!*************************************************
    !
    !
    !
!*************************************************
    
    module calcXCinput_module
    contains
    subroutine calcXCinput(FRC,pcc,rhoout,sig,Grho,lapl,tau,U)
    
    ! data type module
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    use VataMN_2D_module
    use ModBCDist_1D_module
    use ModBCDist_2D_module
    use Distmat_bsxfun_plushalf_module
    
    ! function module
    use parseFuncName_module
    use ModBCDist_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8                          :: pcc
    
    ! temporary variables
    integer :: mpirank, mpisize, ispin, fgridn(3), fn, iter, nlist, ii, nt
    logical :: fg
    real*8  :: avec(3,3)
    character(len=20), allocatable :: flist(:), faprx(:)
    logical, allocatable           :: isgga(:), ismgga(:), ishyb(:)
    type(VataMN_1D)                :: rhotmp, rhov
    type(VataMN_2D)                :: rho
    real*8, allocatable            :: Grho(:,:), psi(:), kdir(:,:)
    integer, allocatable           :: ia(:), ja(:)
    
    ! output variables
    real*8, allocatable            :: U(:,:), gradRho(:,:)
    !type(VataMN_1D)                :: 
    type(VataMN_2D)                :: rhoout, tau, lapl, sig
    
    ! body of this function
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    fgridn  = FRC%domain%fgridn
    fn      = product(fgridn)
    iter    = FRC%scloop
    nlist   = size(FRC%functional%list)
    allocate(flist(nlist))
    flist   = FRC%functional%list
    allocate(isgga(nlist),ismgga(nlist),ishyb(nlist))
    isgga   = .FALSE.
    ismgga  = .FALSE.
    ishyb   = .FALSE.
    call parseFuncName(flist,faprx)
    
    nlist = size(faprx)
    do ii = 1,nlist,1
        if (trim(faprx(ii)) == "GGA") then
            isgga(ii) = .TRUE.
        end if
    
        if (trim(faprx(ii)) == "MGGA") then
            ismgga(ii) = .TRUE.
        end if
    
        if (trim(faprx(ii)) == "HYB") then
            ishyb(ii) = .TRUE.
        end if
    end do
    
    allocate(rho%vata(size(FRC%rho%input(1)%vata,1),1))
    if (trim(FRC%mixing%mixingType) == 'density') then
        rho = FRC%rho%input(iter+1)
    else
        rho = FRC%rho%output(iter)
    end if
    
    if (ispin == 1 .or. ispin == 2) then
        allocate(rhov%vata(size(rho%vata,1)))
        rhov%vata(:) = rho%vata(:,1)
        rhov%m       = rho%m
        rhov%n       = rho%n
        rhov%mblock  = rho%mblock
        rhov%nblock  = rho%nblock
        rhov%mproc   = rho%mproc
        rhov%nproc   = rho%nproc
        !allocate(rhotmp%vata)
        call ModBCDist_1D(FRC,rhov,fn/mpisize,1,mpisize,1,.FALSE.,.FALSE.,rhotmp)
        !allocate(rhotmp%vata(size(rho%vata,1)))
        rhotmp%vata  = rhov%vata
        if (ispin == 1) then
            deallocate(rho%vata)
            allocate(rho%vata(size(rhotmp%vata),2))
            rho%vata(:,1) = rhotmp%vata(:)/2
            rho%vata(:,2) = rhotmp%vata(:)/2
            rho%n         = 2
        end if
    else if (ispin == 4) then
        
        write(*,*) "Error in calcXCinput.f90. General(non-collinear) Spin is not available now."
        stop
        
        ! call diagRhoDM(FRC,rho,U,rhotmp)
    end if
    
    call InitDistArray_2D(fn,6,fn/mpisize,1,mpisize,1,sig)
    call initDistArray_2D(fn,2,fn/mpisize,1,mpisize,1,lapl)
    call initDistArray_2D(fn,2,fn/mpisize,2,mpisize,1,tau)
    
    if (any(isgga == .TRUE.) .or. any(ismgga == .TRUE.) .or. any(ishyb == .TRUE.)) then
        
        write(*,*) "Error in calcXCinput.f90. GGA, mGGA or Hybrid functional is not available now."
        stop
        
        !call calcSima(FRC,rho,pcc,sig,Grho)
    else
        !deallocate(Grho)
    end if
    
    if (any(ismgga == .TRUE.)) then
        !call ModBCDist(FRC,rho,fn,2,1,mpisize,lapl)
        !if (mpirank == 0) then
        !    call GenLaplFun(FRC,fLfun)
        !    lapl%vata = fLfun(lapl%vata)
        !end if
        !call ModBCDist(FRC,lapl,fn/mpisize, 2,mpisize,1,lapl)
        !if (size(FRC%rho%laplRpc) /= 0) then
        !    call distmat_bsxfun_plushalf(FRC,lapl,FRC%rho%laplRpc,lapl)
        !end if
        
        !if (trim(FRC%info%calculationType) == "band-structure" .or. trim(FRC%info%calculationType) == "dos") then
        !    call ModBCDist(FRC,FRC%energy%tau,tau%mblock,1,mpisize,1,tau)
        !else
        !    call GetGlobalInd(mpirank,tau,ia,ja)
        !    allocate(tau%vata(size(ia),size(ja)))
        !    tau%vata = 0
        !    
        !    if (size(psi) /= 0) then
        !        avec  = FRC%domain%latvec
        !        kdir  = FRC%kpoint%kdirect
        !        nkpt  = size(kdir,1)
        !        nocc  = FRC%energy%nocc
        !        ispin = FRC%spin%ispin
        !        if (FRC%LCAO%status .and. FRC%LCAO%dynSubRed) then
        !            aocell   = FRC%LCAO%aocell
        !            aov      = FRC%LCAO%aovec
        !            aosparse = FRC%LACO%aosparse
        !        else if (FRC%LCAO%status) then
        !            aocell   = FRC%LCAO%aokcell
        !        end if
        !        
        !        do kk = 1,nkpt,1
        !            kpt = kdir(kk,:)
        !            if (FRC%LCAO%status .and. FRC%LCAO%dynSubRed) then
        !                call reduceOper(aocell,aov,kpt,aosparse,aok)
        !            else if (FRC%LCAO%status .and. (.not. FRC%LCAO%veffRed)) then
        !                aok = aocell(kk)
        !            end if
        !            
        !            call GenDerFun(FRC,avec,fgridn,kpt,derFun)
        !            
        !            do ii = 1,ispin,1
        !                if (size(FRC%psi(kk,ii) /= 0) .and. (.not. FRC%LCAO%status)) then
        !                    call calcTau(FRC,derFun,FRC%psi(kk,ii),nocc(:,kk,ii),tautmp)
        !                    tau%vata(:,ii) = tau%vata(:,ii)+tautmp%vata
        !                else if ((.not. FRC%psi(kk,ii)) .and. FRC%LCAO%status) then
        !                    call calcTauLCAO(FRC,derFun,aok,FRC%psi(kk,ii),nocc(:,kk,ii))
        !                    tau%vata(:,ii) = tau%vata(:,ii)+tautmp%vata
        !                end if
        !            end do
        !        end do
        !        
        !        if (ispin == 1) then
        !            tauTmp%vata(:,1) = tau%vata(:,1)/2
        !            tauTmp%vata(:,2) = tau%vata(:,1)/2
        !        end if
        !        
        !        if (FRC%symmetry%pointsymmetry) then
        !            call ModBCDist(FRC,tau,tau%m,1,mpisize,1,tausym)
        !            sym_rec = FRC%symmetry%sym_rec
        !            sym_t   = FRC%symmetry%sym_t
        !            nvec    = FRC%domain%cgridn
        !            if (mpirank == 0) then
        !                call symmetrize(tausym%vata,nvec,sym_rec,sym_t,tausym%vata)
        !            end if
        !            call ModBCDist(FRC,tausym,tau%mblock,1,mpisize,1,tau)
        !        end if
        !        
        !        do ii = 1,size(tau),1
        !            if (tau%vata(ii) < 0) then
        !                tau%vata(ii) = 0
        !            end if
        !        end if
        !        
        !    end if
        !end if
    end if
    
    call distmat_bsxfun_plushalf(FRC,rho,FRC%rho%pc,pcc,rhoout)
    
    return
    end subroutine calcXCinput
    end module calcXCinput_module