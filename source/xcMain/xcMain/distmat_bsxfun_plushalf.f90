! distmat_bsxfun_plushalf.f90
    
!***********************************************************
    !
    !
    !
!***********************************************************
    
    module distmat_bsxfun_plushalf_module
    contains
    subroutine distmat_bsxfun_plushalf(FRC,dA,dB,pcc,dC)
    
    ! data type module
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_2D_module
    use ModBCDist_2D_module
    use distmat_issame_2D_module
    
    ! functions module
    !use distmat_issame_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_2D)                 :: dA, dB
    real*8                          :: pcc
    
    ! temporary variables
    integer :: mpisize
    logical :: issame
    
    ! output variables
    type(VataMN_2D) :: dC
    
    ! body of this function
    mpisize = FRC%mpi%mpisize
    
    call distmat_issame_2D(dA,dB,issame)
    if (mpisize == 1 .or. issame) then
        dC%m      = dB%m
        dC%n      = dB%n
        dC%mblock = dB%mblock
        dC%nblock = dB%nblock
        dC%mproc  = dB%mproc
        dC%nproc  = dB%nproc
        allocate(dC%vata(size(dA%vata,1),size(dA%vata,2)))
        dC%vata(:,1) = dA%vata(:,1) + pcc*dB%vata(:,1)
        dC%vata(:,2) = dA%vata(:,2) + pcc*dB%vata(:,1)
    else if (dB%m*dB%n < dA%m*dA%n) then
        call ModBCDist_2D(FRC,dB,dA%mblock,dA%nblock,dA%mproc,dA%nproc,.FALSE.,.FALSE.,dC)
        dC%vata = dA%vata+pcc*dC%vata
    else
        call ModBCDist_2D(FRC,dA,dB%mblock,dB%nblock,dB%mproc,dB%nproc,.FALSE.,.FALSE.,dC)
        dC%vata = dC%vata+pcc*dB%vata
    end if
    
    dC%m = max(dA%m,dB%m)
    dC%n = max(dA%n,dB%n)
    
    return
    end subroutine distmat_bsxfun_plushalf
    end module distmat_bsxfun_plushalf_module