! calcTau.f90
    
!****************************************************
    !
    !
    !
!****************************************************
    
    module calcTau_module
    contains
    subroutine calcTau(FRC,derFun,evec,nocc,tau)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ngrid   = product(FRC%domain%fgridn)
    
    if (.not. all(FRC%domain%fgridn == FRC%domain%cgridn)) then
        dg = .TRUE.
    else
        dg = .FALSE.
    end if
    
    call ModBCDist(FRC,evec,1,1,1,mpisize,psi)
    call GetGlobalInd(mpirank,psi,ia,ja)
    
    nja  = size(ja)
    allocate(nocctmp(nja))
    forall(ii=1:nja)
        nocctmp(ii) = nocc(nja(ii))
    end forall
    
    allocate(tau(ngrid))
    
    do ii = 1,size(psi%vata,2),1
        tautmp = psi%vata(:,ii)
        if (dg) then
            tautmp = FRC%interpolation%IntFun(tautmp)
        end if
        tautmp(:,1) = derFun(1)%vata(tautmp)
        tautmp(:,2) = derFun(2)%vata(tautmp)
        tautmp(:,3) = derFun(3)%vata(tautmp)
        
        tau = tau + 0.5*nocctmp(ii)*sum(tautmp*conj(tautmp),2)
    end do
    
    if (mpistat) then
        call MPI_Reduce_sum(tau)
    end if
    
    if (mpirank /= 0) then
        deallocate(tau)
    end if
    
    call InitDistArray(ngrid,1,ngrid,1,1,mpisize,tau,tau)
    call ModBCDist(FRC,tau,tau%m/mpisize,1,mpisize,1,tau)
    
    return
    end subroutine calcTau
    end module calcTau_module
    