! GenLaplFun.f90
    
!*****************************************************
    !
    !
    !
!*****************************************************
    
    module GenLaplFun_module
    contains
    subroutine GenLaplFun(FRC,kpt,gpu,X,cLkfun,fLkfun)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use Cell_2D_real
    use inversion_module
    use fftFreq_module
    use fft_lapl_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8  :: kpt(3)
    logical :: gpu
    real*8, allocatable :: X(:,:)
    
    ! temporary variables
    real*8, allocatable :: avec(:,:), bvec(:,:), j1(:,:), j2(:,:), ku(:,:,:), kv(:,:,:), kw(:,:,:), k2(:,:,:)
    real*8, allocatable :: Du(:,:), Dv(:,:), Dw(:,:), Duu(:,:), Dvv(:,:), Dww(:,:)
    real*8              :: pi
    integer             :: n(3)
    type(Cell_2D_rea)   :: tmp1(3), tmp2(3)
    
    ! output variables
    real*8, allocatable :: cLkfun(:,:), fLkfun(:,:)
    
    ! body of this function
    pi   = 3.14159265354
    allocate(avec(3,3),bvec(3,3),j1(3,3),j2(3,3))
    avec = FRC%domain%latvec
    call inversion(avec,bvec)
    j1   = transpose(bvec)
    bvec = 2*pi*j1
    kpt  = matmul(kpt,bvec)
    j2   = matmul(j1,transpose(j1))
    
    if (trim(FRC%diffop%method) == "fft") then
        n = FRC%domain%cgridn
        allocate(ku(n(1),n(2),n(3)))
        allocate(kv(n(1),n(2),n(3)))
        allocate(kw(n(1),n(2),n(3)))
        call fftfreq(n,bvec,ku,kv,kw)
        ku = ku+kpt(1)
        kv = kv+kpt(2)
        kw = kw+kpt(3)
        allocate(k2(n(1),n(2),n(3)))
        !k2 = -ku*conjg(ku)-kv*conjg(kv)-kw*conjg(kw)
        k2 = -ku*ku-kv*kv-kw*kw
        if (gpu) then
            write(*,*) "Error in GenLaplFun.f90. GPU is not avalaible now."
            stop
            !call gpuArray(k2)
        end if
        call fft_lapl(x,n,k2,cLkfun)
        n  = FRC%domain%fgridn
        call fftfreq(n,bvec,ku,kv,kw)
        ku = ku+kpt(1)
        kv = kv+kpt(2)
        kw = kw+kpt(3)
        !k2 = -ku*conjg(ku)-kv*conjg(kv)-kw*conjg(kw)
        k2 = -ku*ku-kv*kv-kw*kw
        if (gpu) then
            write(*,*) "Error in GenLaplFun.f90. GPU is not avalaible now."
            stop
            !call gpuArray(k2)
        end if
        call fft_lapl(x,n,k2,fLkfun)
    else
        write(*,*) "Error in GenLaplFun.f90. FRC%diffop%method should be fft."
        stop
        !n   = FRC%domain%cgridn
        !Du  = FRC%diffop%Du
        !Dv  = FRC%diffop%Dv
        !Dw  = FRC%diffop%Dw
        !Duu = FRC%diffop%Duu
        !Dvv = FRC%diffop%Dvv
        !Dww = FRC%diffop%Dww
        !if (gpu) then
        !    write(*,*) "Error in GenLaplFun.f90. GPU is not avalaible now."
        !    stop
            !call gpuArray(n)
            !call gpuArray(j1)
            !call gpuArray(j2)
            !call gpuArray(kpt)
            !call gpuArray(Du)
            !call gpuArray(Dv)
            !call gpuArray(Dw)
            !call gpuArray(Duu)
            !call gpuArray(Dvv)
            !call gpuArray(Dww)
        !end if
        !tmp1(1)%vata = Du
        !tmp1(2)%vata = Dv
        !tmp1(3)%vata = Dw
        !tmp2(1)%vata = Duu
        !tmp2(2)%vata = Dvv
        !tmp2(3)%vata = Dww
        !call mat_lapl(x,n,tmp1,tmp2,j2,kpt,cLkfun)
        !n   = FRC%domain%fgridn
        !Du  = FRC%diffop%fDu
        !Dv  = FRC%diffop%fDv
        !Dw  = FRC%diffop%fDw
        !Duu = FRC%diffop%fDuu
        !Dvv = FRC%diffop%fDvv
        !Dww = FRC%diffop%fDww
        !if (gpu) then
        !    write(*,*) "Error in GenLaplFun.f90. GPU is not avalaible now."
        !    stop
            !call gpuArray(n)
            !call gpuArray(j1)
            !call gpuArray(j2)
            !call gpuArray(kpt)
            !call gpuArray(Du)
            !call gpuArray(Dv)
            !call gpuArray(Dw)
            !call gpuArray(Duu)
            !call gpuArray(Dvv)
            !call gpuArray(Dww)
        !end if
        !tmp1(1)%vata = Du
        !tmp1(2)%vata = Dv
        !tmp1(3)%vata = Dw
        !tmp2(1)%vata = Duu
        !tmp2(2)%vata = Dvv
        !tmp2(3)%vata = Dww
        !call mat_lapl(x,n,tmp1,tmp2,j2,kpt,fLkfun)
    end if
    
    return
    end subroutine GenLaplFun
    end module GenLaplFun_module