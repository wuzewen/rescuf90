! fft_lapl.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    module fft_lapl_module
    contains
    subroutine fft_lapl(x,n,k2,Xout)
    
    use fft4_module
    use ifft4_module
    use bsxfunTimes_4D_cmplx_module
    
    implicit none
    
    ! input variables
    real*8, allocatable  :: X(:,:), k2(:,:,:)
    integer              :: n(3)
    complex, allocatable :: Xcmplx(:,:)
    
    ! temporary variables
    complex, allocatable :: tmp1(:,:,:,:), tmp2(:,:,:,:), k2tmp(:,:,:,:)
    
    ! output variables
    real*8, allocatable :: Xout(:,:)
    
    ! body of this function
    allocate(Xcmplx(size(X,1),size(X,2)))
    Xcmplx = cmplx(X)
    allocate(tmp1(n(1),n(2),n(3),size(x,2)))
    tmp1 = reshape(Xcmplx,(/n(1),n(2),n(3),size(x,2)/))
    allocate(tmp2(n(1),n(2),n(3),size(x,2)))
    call fft4(tmp1,1,tmp2)
    call fft4(tmp2,2,tmp1)
    call fft4(tmp1,3,tmp2)
    
    allocate(k2tmp(size(k2,1),size(k2,2),size(k2,3),1))
    k2tmp(:,:,:,1) = cmplx(k2)
    call bsxfunTimes_4D_cmplx(tmp2,k2tmp,tmp1)
    call ifft4(tmp1,3,tmp2)
    call ifft4(tmp2,2,tmp1)
    call ifft4(tmp1,1,tmp2)
    
    allocate(xout(product(n),size(tmp1,4)))
    xout = real(reshape(tmp2,(/product(n),size(tmp1,4)/)))
    
    return
    end subroutine fft_lapl
    end module fft_lapl_module