! InitDistArray_1D.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    subroutine InitDistArray_1D(M,N,MB,NB,MP,NP,A)
    
    use VataMN_1D_module
    
    implicit none
    
    ! input variables
    integer      :: M, N, MB, NB, MP, NP
    
    ! ouput variables
    type(VataMN_1D) :: A
    
    ! body of this function
    A%m      = M
    A%n      = n
    A%mblock = MB
    A%nblock = NB
    A%mproc  = MP
    A%nproc  = NP
    
    return
    end subroutine InitDistArray_1D