! applyUnitary.f90
    
!*********************************************
    !
    ! 
    !
!*********************************************
    
    module applyUnitary_module
    contains
    subroutine applyUnitary(FRC,U,vxcd,vxc)
    
    implicit none
    
    ! input variables
    
    ! temporay variables
    
    ! output variables
    
    ! body of this function
    call distmat_zeroslike(FRC,vxcd,(/vxcd%m,4/),vxc)
    Utmp    = U%vata
    vxcdtmp = vxcd%vata
    
    vxc%vata(:,1) = 0.5*sum(vxcdtmp,2)
    vxc%vata(:,4) = vxc%vata(:,1)   !0.5*sum(vxcdtmp,2)
    
    vxcdtmp       = 0.5*(vxcd(:,1)-vxcd(:,2))
    vxc%vata(:,1) = vxc%vata(:,1)+Utmp(:,1)*vxcdtmp
    vxc%vata(:,2) = U(:,2)*vxcdtmp
    vxc%vata(:,3) = conjg(vxc%vata(:,2))
    vxc%vata(:,4) = vxc%vata(:,4)-U(:,1)*vxcdtmp
    
    return
    end subroutine applyUnitary
    end module applyUnitary_module