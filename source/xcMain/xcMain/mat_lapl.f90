! mat_lapl.f90
    
!***********************************************
    !
    !
    !
!***********************************************
    
    module mat_lapl_module
    contains
    subroutine mat_lapl(x,n,G,j1,L,j2,kpt,Lx)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    k2   = norm(kpt,2)**2
    kpt2 = j1*reshape(kpt,?)
    call rkronprod(L(1),x,Lx)
    Lx = Lx*j2(1,1)
    call rkronprod(L(2),x,n(1),Lxtmp)
    Lx = Lx + Lxtmp*j2(2,2)
    call rkronprod(L(3),x,Lxtmp)
    Lx = Lx + Lxtmp*j2(3,3)
    
    if (j2(1,2) .or. j2(1,3) .or. kpt(1)) then
        call rkronprod(G(1),x,xtmp)
        if (kpt(1)) then
            Lx = Lx+2*imnu*kpt(1)*xtmp
        end if
        
        if (j2(1,2)) then
            call rlkronprod(G(2),xtmp,n(1),Lxtmp)
            Lx = Lx+2*J2(1,2)*Lxtmp
        end if
        
        if (j2(1,3)) then
            call lkronprod(G(3),xtmp,Lxtmp)
            Lx = Lx+2*j2(1,3)*Lxtmp
        end if
        
    end if
    
    if (j2(2,3) .or. kpt(2)) then
        call rlkronprod(G(2),x,n(1),xtmp)
        
        if (kpt(2)) then
            Lx = Lx+2*imnu*kpt(2)*xtmp
        end if
        
        if (j2(2,3)) then
            call lkronprod(G(3),xtmp,Lxtmp)
            Lx = Lx+2*j2(2,3)*Lxtmp
        end if
    end if
    
    if (kpt(3)) then
        call lkronprod(G(3),xtmp,Lxtmp)
        Lx = Lx+2*j2(2,3)*Lxtmp
    end if
    
    if (any(k2 /= 0)) then
        Lx = Lx-k2*x
    end if
    
    return
    end subroutine mat_lapl
    end module mat_lapl_module
    