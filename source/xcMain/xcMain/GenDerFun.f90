! GenDerFun.f90
    
!*****************************************************
    !
    !
    !
!*****************************************************
    
    module GenDerFun_module
    contains
    subroutine GenDerFun(FRC,avec,ng,kpt,gpu,derFun)
    
    implicit none
    
    call inversion(avec,j1)
    pi   = 3.14159265354
    j1   = transpose(j1)
    bvec = 2*pi*j1
    kpt  = matmul(kpt,bvec)
    
    if (trim(FRC%diffop%method) == "fft") then
        call fft_freq(ng,bvec,ku,kv,kw)
        if (gpu) then
            call gpuArray(ng)
            call gpuArray(kpt)
            call gpuArray(ku)
            call gpuArray(kv)
            call gpuArray(kw)
        end if
        
        call fftGrad(x,ng,ku,kpt(1),derFun(1))
        call fftGrad(x,ng,kv,kpt(2),derFun(2))
        call fftGrad(x,ng,kw,kpt(3),derFun(3))
        
    else
        
        if (all(ng == FRC%domain%cgridn)) then
            Du = FRC%diffop%Du
            Dv = FRC%diffop%Dv
            Dw = FRC%diffop%Dw
        else if (all(ng == FRC%domain%fgridn)) then
            Du = FRC%diffop%fDu
            Dv = FRC%diffop%fDv
            Dw = FRC%diffop%fDw
        else if (trim(FRC%diffop%method) == "fd") then
            acc = FRC%diffop%accuracy
            bc  = FRC%domain%boundary
            call FdGradMat(ng,acc,bc,Du,Dv,Dw)
        else if (trim(FRC%diffop%method) == "ft") then
            call FdGradMat(ng,Du,Dv,Dw)
        end if
        
        if (gpu) then
            call gpuArray(ng)
            call gpuArray(kpt)
            call gpuArray(j1)
            call gpuArray(ku)
            call gpuArray(kv)
            call gpuArray(kw)
        end if
        
        call rkronprod(Du,x,tmp1)
        call rlkronprod(Dv,x,ng(1),tmp2)
        call lkronprod(Dw,x,tmp3)
        derFun(1) = tmp1*j1(1,1)+tmp2*j1(2,1)+tmp3*j1(3,1)+imnu*kpt(1)*x
        derFun(2) = tmp1*j1(1,2)+tmp2*j1(2,2)+tmp3*j1(3,2)+imnu*kpt(2)*x
        derFun(3) = tmp1*j1(1,3)+tmp2*j1(2,3)+tmp3*j1(3,3)+imnu*kpt(3)*x
        
    end if
    
    return
    end subroutine GenDerFun
    end module GenDerFun_module