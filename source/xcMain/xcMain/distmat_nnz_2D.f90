! distmat_nnz_2D.f90
    
!*********************************************************
    !
    !
    !
!*********************************************************
    
    module distmat_nnz_2D_module
    contains
    subroutine distmat_nnz_2D(FRC,dA,nonzero)
    
    use VataMN_2D_module
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_2D)                 :: dA
    
    ! temporary variables
    logical :: mpistat
    
    ! output variables
    integer :: nonzero
    
    ! body of this function
    mpistat = FRC%mpi%status
    nonzero = count(dA%vata /= 0)
    if (mpistat) then
        write(*,*) "Error in diatmat_nnz.f90. mpi is not available now."
        stop
        !call MPI_Allreduce_sum(nonzero)
    end if
    
    return
    end subroutine distmat_nnz_2D
    end module distmat_nnz_2D_module
    