! diagRhoDM.f90
    
!****************************************
    !
    !
    !
!****************************************
    
    module diagRhoDM_module
    contains
    subroutine diagRhoDM(FRC,rhodm,U,rhod)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    imnu = (0,1)
    call distmat_zeroslike(FRC,rhodm,(/rhodm%m,2/),rhod)
    call distmat_zeroslike(FRC,rhod,U)
    
    sig1    =  0
    sig1(1) =  1
    sig1(4) =  1
    
    sig2    =  0
    sig2(2) =  1
    sig2(3) =  1
    
    sig3    =  0
    sig3(2) =  imnu
    sig3(3) = -imnu
    
    sig4    =  0
    sig4(1) =  1
    sig4(4) = -1
    
    call bsxfunTimes(rhodm%vata(:,1),sig1,rho)
    call bsxfunTimes(rhodm%vata(:,2),sig2,rhotmp)
    rho     =  rho + rhotmp
    call bsxfunTimes(rhodm%vata(:,3),sig3,rhotmp)
    rho     =  rho + rhotmp
    call bsxfunTimes(rhodm%vata(:,4),sig4,rhotmp)
    rho     =  rho + rhotmp
    rho     =  rho/2
    phi     = -atan2(imag(rho(:,3)),real(rho(:,3)))
    tnum    =  real(rho(:,3))*cos(phi)-imag(rho(:,3))*sin(phi)
    theta   = atan2(2*tnum,rho(:,1)-rho(:,4))
    rhoup   = 0.5*(rho(:,1)+rho(:,4)) + 0.5*(rho(:,1)-rho(:,4))*cos(theta) + tnum*sin(theta)
    rhodown = 0.5*(rho(:,1)+rho(:,4)) + 0.5*(rho(:,1)-rho(:,4))*cos(theta) - tnum*sin(theta)
    
    rho%vata(:,1) = real(rhoup)
    rho%vata(:,2) = real(rhodown)
    
    U%vata(:,1) = cos(theta)
    U%vata(:,2) = exp(imnu*phi)*sin(theta)
    
    return
    end subroutine diagRhoDM
    end module diagRhoDM_module