!  VariablesInType.f90 
!****************************************************************************
!
!  PROGRAM: VariablesInType
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module VariablesInType

    implicit none

    ! Variables
    type :: Input_Variables
        
        type :: Atom                                    ! Atom information
            integer, allocatable :: element(:)          ! An array which define every atom's element
            real*8,  allocatable :: XYZ(:,:)            ! cartesian coodinate of every atom, [INF,3]
            real*8,  allocatable :: FracXYZ(:,:)        ! direct coordinate of every atom, [INF,3]
            real*8,  allocatable :: magmom(:)           ! Initial magnetic moment of every atom.
            real*8,  allocatable :: magmomD(:,:)        ! Initial magnetic moment of every atom.
            real*8,  allocatable :: magmomcart(:,:)     ! Initial magnetic moment of every atom.
        end type Atom
        
        type :: Domain                                  ! Cell information and dividing grids
            character(len=10)    :: bravaisLattice      ! Crystal structure type, BCC FCC ...
            integer              :: boundary(3)         ! bounfary condition
            real*8, allocatable  :: bvalx(:)            ! determines the boundary values along the x-direction, It is meaningful only if Dirichlet boundary conditions are used
            real*8, allocatable  :: bvaly(:)            ! 
            real*8, allocatable  :: bvalz(:)            ! 
            integer              :: cgridn(3)           !
            integer              :: fgridn(3)           !
            real*8               :: lowres              !
            real*8               :: higres              !
            real*8               :: latvec(3,3)         !
        end type Domain
        
        type :: diffop
            integer              :: accuracy
            character(len=10)    :: method
        end type diffop
        
        type :: element
            integer              :: aoLcutoff
            integer              :: aoZcutoff
            character(len=99)    :: path
            character(len=03)    :: species
            integer              :: valance
            integer              :: vnlNcutoff
            integer              :: vnlLcutoff
        end type element
        
        type :: eigensolver
            character(len=10)    :: algo
            character(len=10)    :: algoproj
            integer              :: adapCFD
            integer              :: bandi(2)
            integer              :: emptyBand
            character(len=10)    :: init
            integer              :: maxIT
            integer              :: maxRestart
            integer              :: nsym
            integer              :: orthogonalize
            integer              :: precond
            integer              :: pwmode
            real*8               :: tol
            integer              :: UBmaxit
        end type eigensolver
        
        type :: interpolation
            integer              :: interpRho
            character(len=10)    :: method
            integer              :: order
            integer              :: Vnl
            integer              :: Vloc
        end type interpolation
        
        type :: kpoint
            integer              :: gridn(3)
            real*8, allocatable  :: kcartesian(:,3)
            real*8, allocatable  :: kdirect
            character(len=20)    :: sampling
            real*8               :: shift(3)
            character            :: sympoints
            character(len=20)    :: Ksamlingtype
            integer, allocatable :: weight
        end type kpoint
        
        type :: LCAO
            integer              :: cfsi
            integer              :: dynSubRed
            integer              :: mulliken
            real*8               :: spres
            integer              :: status
            integer              :: VeffRed
        end type LCAO
        
        type :: mixing
            real*8               :: alpha
            real*8               :: beta
            real*8               :: betalin
            integer              :: initlin
            real*8               :: lambda
            integer              :: maxHistory
            integer              :: method
            real*8               :: tol(2)
            character(len=20)    :: MixingType
        end type mixing
        
        type :: potential
            integer              :: fourierinit
        end type potential
        
        type :: smearing
            integer              :: order
            real*8               :: sigma
        end type smearing
        
        type :: spin
            integer              :: SOI
            character(len=20)    :: spinType
        end type spin
        
        type :: SR
            integer              :: initStep
            integer              :: maxStep
            integer              :: maxit
            integer              :: maxLineIt
            character(len=20)    :: method
            character(len=20)    :: linemethod
            integer              :: moveableAtomList
            character(len=99)    :: relaxedstructure
            character(len=99)    :: savePath
            real*8               :: tol
        end type SR
        
        type :: symmetry
            integer              :: spacesymmetry
            integer              :: pointymmetry
            integer              :: timereversal
        end type symmetry
        
        type :: units
            character(len=20)    :: xyz
            character(len=20)    :: latvec
            character(len=20)    :: sigma
            character(len=20)    :: dos
        end type units
        
        type :: smi
            real*8               :: abstol
            real*8               :: lwork
            integer              :: mb
            integer              :: mp
            integer              :: nb
            integer              :: np
            real*8               :: orfac
            integer              :: status
        end type
        
        type :: info
            character(len=20)    :: calculationType
            character(len=20)    :: mpiPath
            character(len=20)    :: outfile
            character(len=20)    :: rescuPath
            character(len=20)    :: savePath
            character(len=20)    :: saveDir
            character(len=20)    :: savePreFix
            character(len=20)    :: smiPath
        end type
        
        type :: DOS
            
            type :: LDOSType
                integer              :: status
                real*8               :: energy
                real*8, allocatable  :: energyI
                real*8, allocatable  :: energyK
            end type LDOSType
            
            type(LDOSType) :: LDOS
            
            integer              :: projL
            integer              :: projM
            integer              :: projZ
            integer              :: projXYZ
            integer              :: projAtom
            integer              :: projSpecies
            real*8               :: range
            real*8               :: resolution
            integer              :: status
        end type DOS
        
        type :: option
            integer              :: bufferSize
            integer              :: centerMolecule
            integer              :: initParaReal
            integer              :: maxSCFiteration
            integer              :: plorder
            integer              :: replicateCell(3)
            integer              :: shortRange
            integer              :: saveDensity
            integer              :: saveDFPT
            integer              :: saveEigensolver
            integer              :: saveInterPolation
            integer              :: saveLCAO
            integer              :: saveMisc
            integer              :: saveMixing
            integer              :: savePotential
            integer              :: saveTau
            integer              :: saveWaveFunction
        end type option
        
        type GPU
            integer              :: status
            integer              :: GPUperNode
            integer              :: ProcPerNode
        end type
        
        type :: ph
            real*8               :: klist
            integer              :: nk
            character(len=20)    :: labels
            integer, allocatable :: mass(:)
            integer, allocatable :: unitCellIndex(:)
            integer, allocatable :: unitCellMass(:)
            integer              :: superCellDimension
            character(len=20)    :: mode
            integer              :: ASR
            real                 :: dr
            integer              :: plot
            integer              :: dispStartNumber
            character(len=20)    :: supercellXyzFile
            real                 :: tol
            character(len=20)    :: wUnit
            integer              :: isMinus
        end type ph
        
    end type Input_Variables
    
        
        

    ! Body of VariablesInType
    

    end program VariablesInType

