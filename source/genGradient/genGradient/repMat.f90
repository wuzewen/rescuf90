! repMat.f90
!**************************************************************************
    ! This function has the same function with the function repmat in 
    ! matlab. While, ArrayA must be an array in one dimention.
!**************************************************************************
    
    module repMat_module
    contains
    subroutine repMat(ArrayA,dim1,dim2,MatrixB)
    
    implicit none
    
    ! input valuables
    real*8, allocatable  :: ArrayA(:)
    integer              :: dim1, dim2
    
    ! temporery valuables
    integer              :: N
    integer              :: i, j, k
    
    ! output valuables
    real*8, allocatable  :: MatrixB(:,:)
    
    ! body of this function
    N = size(ArrayA)
    do i = 1,dim1,1
        do j = 1,dim2,1
            forall(k=((j-1)*N+1):(j*N))
                MatrixB(i,k) = ArrayA(k-(j-1)*N)
            end forall
        end do
    end do
    
    return
    end subroutine repMat
    end module repMat_module