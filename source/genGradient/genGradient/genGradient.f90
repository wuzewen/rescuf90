!  genGradient.f90 
!
    module genGradient_module
    contains
    !subroutine genGradient(cgridn, fgridn, method, acc, bc, Du, Dv, Dw, fDu, fDv, fDw)
    subroutine genGradient(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use fdGradMat_module
    use ftGradMat_module
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    character(len=20)   :: method
    integer             :: cgridn(3), fgridn(3), acc, bc(3)
    
    ! temporery valuables
    !real*8, allocatable :: Gu(:,:), Gv(:,:), Gw(:,:)
    !logical             :: bctmp(3)
    !integer             :: ii
    
    
    ! output valuables
    
    real*8, allocatable ::  Du(:,:),  Dv(:,:),  Dw(:,:)
    real*8, allocatable :: fDu(:,:), fDv(:,:), fDw(:,:)

    ! Body of genGradient
    method = FRC%diffop%method
    acc    = FRC%diffop%accuracy
    bc     = FRC%domain%boundary
    cgridn = FRC%domain%cgridn
    fgridn = FRC%domain%fgridn
    if (trim(method) == trim("fd")) then
    !if (trim(FRC%diffop%method) == trim("fd")) then
        !do ii = 1,3,1
        !    if (bc(ii) == 1) then
        !        bctmp(ii) = .TRUE.
        !    else
        !        bctmp(ii) = .FALSE.
        !    end if
        !end do
        
        call fdGradMat(cgridn,acc,bc,Du,Dv,Dw)
        FRC%diffop%Du = Du
        FRC%diffop%Dv = Dv
        FRC%diffop%Dw = Dw
        call fdGradMat(fgridn,acc,bc,fDu,fDv,fDw)
        FRC%diffop%fDu = fDu
        FRC%diffop%fDv = fDv
        FRC%diffop%fDw = fDw
    !else if (trim(FRC%diffop%method) == trim("ft")) then
    else if (trim(method) == trim("ft")) then
        call ftGradMat(cgridn,Du,Dv,Dw)
        FRC%diffop%Du = Du
        FRC%diffop%Dv = Dv
        FRC%diffop%Dw = Dw
        call ftGradMat(fgridn,fDu,fDv,fDw)
        FRC%diffop%fDu = fDu
        FRC%diffop%fDv = fDv
        FRC%diffop%fDw = fDw
    end if
    
    return
    end subroutine genGradient
    end module genGradient_module
