! det.f90
!*************************************************
    ! B = |A|
!*************************************************
    
    module det_module
    contains
    subroutine det(A,B)
    
    implicit none
    
    integer              :: N, i,j,k,is,js
    real*8, allocatable  :: A(:,:)
    real*8               :: B
    real*8               :: F, D, Q
    
    F = 1.0
    B = 1.0
    N = size(A,1)
    
    do k = 1,N-1,1
        
        Q = 0.0

        do i = k,N,1
            do j = k,N,1
                if (abs(A(i,j)) > Q) then
                    Q = abs(A(i,j))
                    is = i
                    js = j
                end if
            end do
        end do
        
        if (Q == 0) then
            B = 0
            return
        end if
        
        if (is /= k) then
            F = -F
            do j = k,N,1
                D = A(k,j)
                A(k,j) = A(is,j)
                A(is,j) = D
            end do
        end if
        
        if (js /= k) then
            F = -F
            do i = k,n,1
                D = A(i,js)
                A(i,js) = A(i,k)
                A(i,k) = D
            end do
        end if
        
        B = B*A(k,k)
        
        do i = k+1,N,1
            D = A(i,k)/A(k,k)
            do j = k+1,N,1
                A(i,j) = A(i,j)-D*A(k,j)
            end do
        end do
    end do
    
    B = F*B*A(N,N)
    
    return
    end subroutine det
    end module det_module