! ftGradMat.f90
!**************************************************************************
    ! This function
!**************************************************************************
    
    module ftGradMat_module
    contains
    subroutine ftGradMat(n,Gu,Gv,Gw)
    
    use fftfreq_module
    use fft2_module
    
    implicit none
    
    ! input valuables
    integer :: n(3)
    
    ! temporery valuables
    integer              :: i
    complex              :: imnu, pi
    integer              :: ntmp(3)
    real*8               :: bvec(3,3)
    real*8, allocatable  :: aa(:,:), bb(:,:), cc(:,:)
    real*8, allocatable  :: ku1(:,:,:), kv1(:,:,:), kw1(:,:,:), &
                            ku2(:,:,:), kv2(:,:,:), kw2(:,:,:), &
                            ku3(:,:,:), kv3(:,:,:), kw3(:,:,:)
    
    ! output valuables
    real*8, allocatable  :: Gu(:,:), Gv(:,:), Gw(:,:)
    complex, allocatable :: Gutmp(:,:), Gvtmp(:,:), Gwtmp(:,:)
    
    ! body of this function
    imnu    = (0,1)
    pi      = (3.14159265354,0)
    ntmp(2) = 1
    ntmp(3) = 1
    
    forall(i=1:3)
        bvec(i,i) = 1
    end forall
    
    !call eye(3,bvec)
    
    ntmp(1) = n(1)
    call fftFreq(ntmp,bvec,ku1,kv1,kw1)
    allocate(Gu(n(1),n(1)))
    !call eye(n(1),Gu)
    forall(i=1:n(1))
        Gu(i,i) = 1
    end forall
    
    call fft2(Gu,2,Gutmp)
    allocate(aa(n(1),n(1)))
    aa(:,:) = 0
    forall(i=1:n(1))
        aa(i,i) = 2*imnu*pi*cmplx(ku1(i,1,1))/n(1)
    end forall
    Gu = real(matmul(transpose(Gutmp),matmul(aa,Gutmp)))
    
    ntmp(1) = n(2)
    call fftFreq(ntmp,bvec,ku2,kv2,kw2)
    allocate(Gv(n(2),n(2)))
    !call eye(n(2),Gv)
    forall(i=1:n(2))
        Gv(i,i) = 1
    end forall
    
    call fft2(Gv,2,Gvtmp)
    allocate(bb(n(2),n(2)))
    bb(:,:) = 0
    forall(i=1:n(2))
        bb(i,i) = 2*imnu*pi*cmplx(ku2(i,1,1))/n(2)
    end forall
    Gv = real(matmul(transpose(Gvtmp),matmul(bb,Gvtmp)))
    
    ntmp(1) = n(3)
    call fftFreq(ntmp,bvec,ku3,kv3,kw3)
    allocate(Gw(n(3),n(3)))
    !call eye(n(3),Gw)
    forall(i=1:n(3))
        Gw(i,i) = 1
    end forall
    
    call fft2(Gw,2,Gwtmp)
    allocate(cc(n(3),n(3)))
    cc(:,:) = 0
    forall(i=1:n(3))
        cc(i,i) = 2*imnu*pi*cmplx(ku3(i,1,1))/n(3)
    end forall
    Gw = real(matmul(transpose(Gwtmp),matmul(cc,Gwtmp)))
    
    return
    end subroutine ftGradMat
    end module ftGradMat_module