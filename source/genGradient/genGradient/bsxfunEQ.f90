! bsxfunEQ.f90

!*************************************************
    !
    !
    !
!*************************************************
    
    module bsxfunEQ_module
    contains
    subroutine bsxfunEQ(MatrixA,MatrixB,MatrixC)
    
    implicit none
    
    ! input variables
    real*8, allocatable  :: MatrixA(:,:), MatrixB(:,:)
    
    ! temporary variables
    integer              :: mA, nA, mB, nB
    integer              :: ii, jj
    
    ! output variables
    logical, allocatable :: MatrixC(:,:)
    
    ! body of this function
    mA = size(MatrixA,1)
    nA = size(MatrixA,2)
    mB = size(MatrixB,1)
    nB = size(MatrixB,2)
    
    if (mA == 1 .and. nB == 1) then
        do ii = 1,nA,1
            do jj = 1,mB,1
                if (MatrixA(1,ii) == MatrixB(jj,1)) then
                    MatrixC(jj,ii) = .TRUE.
                else
                    MatrixC(jj,ii) = .FALSE.
                end if
            end do
        end do
    else if (nA == 1 .and. mB == 1) then
        do ii = 1,mA,1
            do jj = 1,nB,1
                if (MatrixA(ii,1) == MatrixB(1,jj)) then
                    MatrixC(ii,jj) = .TRUE.
                else
                    MatrixC(ii,jj) = .FALSE.
                end if
            end do
        end do
    else if (mA == 1 .and. nA == nB) then
        do ii = 1,nA,1
            do jj = 1,mB,1
                if (MatrixA(1,ii) == MatrixB(jj,ii)) then
                    MatrixC(jj,ii) = .TRUE.
                else
                    MatrixC(jj,ii) = .FALSE.
                end if
            end do
        end do
    else if (nA == 1 .and. mA == mB) then
        do ii = 1,mA,1
            do jj = 1,nB
                if (MatrixA(ii,1) == MatrixB(ii,jj)) then
                    MatrixC(ii,jj) = .TRUE.
                else
                    MatrixC(ii,jj) = .FALSE.
                end if
            end do
        end do
    else if (mB == 1 .and. nA == nB) then
        do ii = 1,nB,1
            do jj = 1,mA,1
                if (MatrixB(1,ii) == MatrixA(jj,ii)) then
                    MatrixC(jj,ii) = .TRUE.
                else
                    MatrixC(jj,ii) = .FALSE.
                end if
            end do
        end do
    else if (nB == 1 .and. mB == mA) then
        do ii = 1,mB,1
            do jj = 1,nA,1
                if (MatrixB(ii,1) == MatrixA(ii,jj)) then
                    MatrixC(ii,jj) = .TRUE.
                else
                    MatrixC(ii,jj) = .FALSE.
                end if
            end do
        end do
    else if (mA == mB .and. nA == nB) then
        do ii = 1,mA,1
            do jj = 1,nA,1
                if (MatrixA(ii,jj) == MatrixB(ii,jj)) then
                    MatrixC(ii,jj) = .TRUE.
                else
                    MatrixC(ii,jj) = .FALSE.
                end if
            end do
        end do
    else
        write(*,*) "Error in bsxfunEQ.f90."
        stop
    end if
    
    return
    end subroutine bsxfunEQ
    end module bsxfunEQ_module