! galleryCircul.f90
!**********************************************************************************
    ! This function has the same function with the function gallery('circul',*)
    ! ArrayA must be an array in one dimention. Output matrix MatrixB is a matrix
!**********************************************************************************
    
    module galleryCircul_module
    contains
    subroutine galleryCircul(ArrayA,MatrixB)
    
    implicit none
    
    ! input valuables
    real*8, allocatable :: ArrayA(:)
    
    ! temporery valuables
    integer              :: N
    integer              :: i, j
    
    ! output valuables
    real*8, allocatable :: MatrixB(:,:)
    
    ! body of this function
    N = size(ArrayA)
!    allocate(MatrixB(N,N))
    do i = 1,N,1
        MatrixB(i,:) = cshift(ArrayA,1-i)
    end do
    
    return
    end subroutine galleryCircul
    end module galleryCircul_module