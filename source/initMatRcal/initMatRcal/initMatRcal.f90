!  initMatRcal.f90 
!
!  FUNCTIONS:
!  initMatRcal - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: initMatRcal
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module initMatRcal_module
    contains
    subroutine initMatRcal(inputFromFile,FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType), allocatable :: inputFromFile(:)
    
    ! temporary variables
    
    ! output variables
    

    ! Body of initMatRcal
    if (status%mpi .or. status%smi) then
        call initMPI()
    end if
    
    call initInfo()
    call AddPaths()
    call InitOption()
    call repxyz()
    call initElement()
    call GetAtomInfo()
    call initLCAO()
    call GetDomainInfo()
    call initSpin()
    call initSymmetry()
    call initKpoint()
    call initTempSmear()
    call initDOS()
    call initEigensolver()
    call initPRR()
    call initXC()
    call initPotential()
    call initInterPolation()
    call initDensity()
    call initEnergy()
    call initSMI()
    call initGPU()
    call initMixer()
    call initSR()
    call initForce()
    call initDFPT()
    

    return
    end subroutine initMatRcal
    end module initMatRcal_module

