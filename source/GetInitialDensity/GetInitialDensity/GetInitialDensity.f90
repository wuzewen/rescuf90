!  GetInitialDensity.f90 
!
!  FUNCTIONS:
!  GetInitialDensity - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: GetInitialDensity
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module GetInitialDensity_module
    contains
    subroutine GetInitialDensity(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    
    ! output variables

    ! Body of GetInitialDensity
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%size
    
    rho     = FRC%rho%input(1)
    ! rho is a char
    call fileparts(rho,rhopath,rhoname)
    call readEnergy(FRC)
    if (trim(FRC%info%calculationType) == trim("relaxation") .or. trim(FRC%info%calculationType) == trim("dfpt")) then
        FRC%enengy%ksnrg(1) = nrg%energy%ksnrg(???)
        FRC%energy%nocc     = nrg%energy%nocc
        FRC%energy%Etot     = nrg%energy%Etot(???)
    end if
    if (trim(FRC%info%calculationType) == trim("force")) then
        FRC%enengy%ksnrg(1) = nrg%energy%ksnrg(???)
    end if
    
    fn    = ???
    nspin = ???
    call initDistArray(fn,nspin,ceiling(fn/mpisize),1,mpisize,1,rho)
    call loadDistArray(rhoH5path,"/rho/rhoOut",rho)
    
    if (FRC%spin%ispin == 1 .and. rho%n == 2) then
        if (mpirank == 0) then
            write(*,*) "warning"
        end if
        call ModBCDist(FRC,rho,rho%mblock,rho%nblock,mpisize,1,rho)
        rho%datav = sum(rho%datav,2)
        rho%n     = 1
    else if (FRC%spin%ispin == 2 .and. rho%n = 1)
        if (mpirank == 0) then
            write(*,*) "warning"
        end if
        call distmat_cat(FRC,rho,rho,2,rho)
        polar = FRC%atom%magmom/FRC%eigensolver%Nvalence
        nocc  = 0.5+0.5*polar*(/1/-1/)
        call bsxfunTimes(rho%datav,nocc,rho%datav)
    else if (FRC%spin%ispin == 4 .and. rho%n == 1) then
        n_loc     = size(rho%datav,1)
        rho%datav(:,?) = rho%datav
        rho%datav(:,?) = 0
        rho%n          = 4
    else if (FRC%spin%ispin == 4 .and. rho%n == 2) then
        n_loc = size(rho%datav,1)
        rho%datav(:,?) = sum(rho%datav,2)
        rho%datav(:,?) = 0
        call diff(rho%datav,2,datav(:,??))
        rho%n = 4
    end if
    FRC%rho%input(1) = rho
    
    return
    end subroutine GetInitialDensity
    end module GetInitialDensity_module

