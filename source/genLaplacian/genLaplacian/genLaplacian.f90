!  genLaplacian.f90 
!****************************************************************************
!
!  PROGRAM: genLaplacian
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module genLaplacian_module
    contains
    !subroutine genLaplacian(acc,bc,method,cgridn,fgridn,Du,Dv,Dw,fDu,fDv,fDw)
    subroutine Genlaplacian(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ftLaplMat_module
    use fdLaplMat_module
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer :: acc
    integer :: bc(3)
    character(len=20) :: method
    integer :: cgridn(3), fgridn(3)
    
    ! temporery valuables
    !real*8, allocatable :: Lu(:,:), Lv(:,:), Lw(:,:)
    
    ! output valuables
    real*8, allocatable ::  Du(:,:),  Dv(:,:),  Dw(:,:), &
                           fDu(:,:), fDv(:,:), fDw(:,:)

    ! Body of genLaplacian
    !allocate( Du(n(1),n(1)), Dv(n(2),n(2)), Dw(n(3),n(3)))
    !allocate(fDu(n(1),n(1)),fDv(n(2),n(2)),fDw(n(3),n(3)))
    method = FRC%diffop%method
    acc    = FRC%diffop%accuracy
    bc     = FRC%domain%boundary
    cgridn = FRC%domain%cgridn
    fgridn = FRC%domain%fgridn
    if (trim(method) == trim("fd")) then
        bc(:) = 1
        call fdLaplMat(cgridn,acc,bc,Du,Dv,Dw)
        FRC%diffop%Duu = Du
        FRC%diffop%Dvv = Dv
        FRC%diffop%Dww = Dw
        call fdLaplMat(fgridn,acc,bc,fDu,fDv,fDw)
        FRC%diffop%fDuu = fDu
        FRC%diffop%fDvv = fDv
        FRC%diffop%fDww = fDw
    else if (trim(method) == trim("ft")) then
        call ftLaplMat(cgridn,Du,Dv,Dw)
        FRC%diffop%Duu = Du
        FRC%diffop%Dvv = Dv
        FRC%diffop%Dww = Dw
        call ftLaplMat(fgridn,fDu,fDv,fDw)
        FRC%diffop%fDuu = fDu
        FRC%diffop%fDvv = fDv
        FRC%diffop%fDww = fDw
    end if
    
    
    return
    end subroutine genLaplacian
    end module genLaplacian_module

