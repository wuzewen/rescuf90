! initDOS.f90
!*****************************************************************************
    !
    ! This subroutine generate the parameters controlling DOS calculation.
    !
!*****************************************************************************
    
    module initDOS_module
    contains
    subroutine initDOS(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    use initSymmetry_module
    use initKpoint_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    real*8                               :: HA2EV
    logical                              :: DOSstatusIsFind, DOSresolutionIsFind, DOSrangeMINIsFind, DOSrangeMAXIsFind
    logical                              :: DOSprojLIsFind, DOSprojMIsFind, DOSprojZIsFind, DOSprojAtomIsFind, DOSprojSpeciesIsFind
    logical                              :: LDOSstatusIsFind, LDOSenergyIsFind, LDOSenergyiIsFind, LDOSenergykIsFind
    logical                              :: unitsDOSIsFind, unitsDOSrangeIsFind, unitsDOSresolutionIsFind
    integer                              :: ii, nline, status
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%DOS) then
        
        DOSstatusIsFind          = .FALSE. 
        DOSresolutionIsFind      = .FALSE. 
        DOSrangeMINIsFind        = .FALSE. 
        DOSrangeMAXIsFind        = .FALSE. 
        DOSprojLIsFind           = .FALSE. 
        DOSprojMIsFind           = .FALSE. 
        DOSprojZIsFind           = .FALSE. 
        DOSprojAtomIsFind        = .FALSE. 
        DOSprojSpeciesIsFind     = .FALSE. 
        LDOSstatusIsFind         = .FALSE. 
        LDOSenergyIsFind         = .FALSE. 
        LDOSenergyiIsFind        = .FALSE. 
        LDOSenergykIsFind        = .FALSE. 
        unitsDOSIsFind           = .FALSE. 
        unitsDOSrangeIsFind      = .FALSE. 
        unitsDOSresolutionIsFind = .FALSE. 
        
        
        
        
        HA2EV = 27.21138602D0
        nline = inputFromFile%NumberOfInput
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DOSstatus")) then
                DOSstatusIsFind = .TRUE.
                exit
            end if
        end do
        if (DOSstatusIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%DOS%status = .TRUE.
            else
                FRC%DOS%status = .FALSE.
            end if
        else
            FRC%DOS%status = .FALSE.
        end if
        
        if (trim(FRC%info%calculationType) == trim("DOS")) then
            FRC%DOS%status = .TRUE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DOSresolution")) then
                DOSresolutionIsFind = .TRUE.
                exit
            end if
        end do
        if (DOSresolutionIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%DOS%resolution
        else
            FRC%DOS%resolution  =  1.0D-3
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DOSrangeMIN")) then
                DOSrangeMINIsFind = .TRUE.
                exit
            end if
        end do
        if (DOSrangeMINIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%DOS%DOSrange(1)
        else
            FRC%DOS%DOSrange(1) = -0.35D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DOSrangeMAX")) then
                DOSrangeMAXIsFind = .TRUE.
                exit
            end if
        end do
        if (DOSrangeMAXIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%DOS%DOSrange(2)
        else
            FRC%DOS%DOSrange(2) =  0.35D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DOSprojL")) then
                DOSprojLIsFind = .TRUE.
                exit
            end if
        end do
        if (DOSprojLIsFind) then
            write(*,*) "projected DOS is not available now."
            stop
            !FRC%DOS%projL = inputFromFile%NAV(ii)%value
        else
            !FRC%DOS%projL       =  0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DOSprojM")) then
                DOSprojMIsFind = .TRUE.
                exit
            end if
        end do
        if (DOSprojMIsFind) then
            write(*,*) "projected DOS is not available now."
            stop
            !FRC%DOS%projM = inputFromFile%NAV(ii)%value
        else
            !FRC%DOS%projM =  0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DOSprojZ")) then
                DOSprojZIsFind = .TRUE.
                exit
            end if
        end do
        if (DOSprojZIsFind) then
            write(*,*) "projected DOS is not available now."
            stop
            !FRC%DOS%projZ = inputFromFile%NAV(ii)%value
        else
            !FRC%DOS%projZ       =  0
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DOSprojAtom")) then
                DOSprojAtomIsFind = .TRUE.
                exit
            end if
        end do
        if (DOSprojAtomIsFind) then
            write(*,*) "projected DOS is not available now."
            stop
            !FRC%DOS%projAtom = inputFromFile%NAV(ii)%value
        else
            !FRC%DOS%projAtom = 0
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DOSprojSpecies")) then
                DOSprojSpeciesIsFind = .TRUE.
                exit
            end if
        end do
        if (DOSprojSpeciesIsFind) then
            write(*,*) "projected DOS is not available now."
            stop
            !FRC%DOS%projSpecies = inputFromFile%NAV(ii)%value
        else
            !FRC%DOS%projSpecies =  0
        end if
        
        !if (trim(FRC%info%calculationType) == trim("DOS")) then
        !    FRC%DOS%status = .TRUE.
        !end if
        
        !if (any(FRC%DOS%projL /= 0) .or. any(FRC%DOS%projM /= 0) .or. any(FRC%DOS%projZ /= 0) .or. any(FRC%DOS%projAtom /= 0) .or. any(FRC%DOS%projSpecies /= 0)) then
        !    FRC%DOS%pdos = .TRUE.
        !else
        !    FRC%DOS%pdos = .FALSE.
        !end if
        
        if (FRC%DOS%pdos) then
            FRC%symmetry%spacesymmetry = .FALSE.
            FRC%symmetry%pointsymmetry = .FALSE.
            FRC%init%symmetry          = .FALSE.
            FRC%init%kpoint            = .FALSE.
            call InitSymmetry(inputFromFile,FRC)
            call InitKpoint(inputFromFile,FRC)
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("LDOSstatus")) then
                LDOSstatusIsFind = .TRUE.
                exit
            end if
        end do
        if (LDOSstatusIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%DOS%ldos%status = .TRUE.
            else
                FRC%DOS%ldos%status = .FALSE.
            end if
        else
            FRC%DOS%ldos%status = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("LDOSenergy")) then
                LDOSenergyIsFind = .TRUE.
                exit
            end if
        end do
        if (LDOSenergyIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%DOS%ldos%energy
        else
            !FRC%DOS%ldos%energy = 0.0D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("LDOSenergyi")) then
                LDOSenergyiIsFind = .TRUE.
                exit
            end if
        end do
        if (LDOSenergyiIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%DOS%ldos%energyi
        else
            !FRC%DOS%ldos%energyi = 0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("LDOSenergyk")) then
                LDOSenergykIsFind = .TRUE.
                exit
            end if
        end do
        if (LDOSenergykIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%DOS%ldos%energyk
        else
            !FRC%DOS%ldos%energyK = 0.0D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("unitsDOS")) then
                unitsDOSIsFind = .TRUE.
                exit
            end if
        end do
        if (unitsDOSIsFind) then 
            read(inputFromFile%NAV(ii)%value,*) FRC%units%DOS%DOSrange 
            FRC%units%DOS%resolution = FRC%units%DOS%DOSrange
        else
            FRC%units%DOS%DOSrange      = "Hartree"
            FRC%units%DOS%resolution = "Hartree"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("unitsDOSrange")) then
                unitsDOSrangeIsFind = .TRUE.
                exit
            end if
        end do
        if (unitsDOSrangeIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%units%DOS%DOSrange
        else
            FRC%units%DOS%DOSrange = "Hartree"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("unitsDOSresolution")) then
                unitsDOSresolutionIsFind = .TRUE.
                exit
            end if
        end do
        if (unitsDOSresolutionIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%units%DOS%resolution
        else
            FRC%units%DOS%resolution = "Hartree"
        end if
        
        if (trim(FRC%units%DOS%DOSrange) == trim("ev")) then
            FRC%DOS%DOSrange            = FRC%DOS%DOSrange/HA2EV
            FRC%units%DOS%DOSrange      = "Hartree"
        end if
        
        if (FRC%units%DOS%resolution == "ev") then
            FRC%DOS%resolution       = FRC%DOS%resolution/HA2EV
            FRC%units%DOS%resolution = "Hartree"
        end if
        
        FRC%init%DOS = .TRUE.
    end if
    
    return
    end subroutine initDOS
    end module initDOS_module
    
    
