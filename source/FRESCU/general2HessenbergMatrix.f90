! general2HessenbergMatrix.f90
    
!************************************************************************
    !
    !
    !
!************************************************************************
    
    module general2HessenbergMatrix_module
    contains
    subroutine general2HessenbergMatrix(GeneralMatrix,HessenbergMatrix)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: GeneralMatrix(:,:)
    
    ! temporary variables
    integer :: N, M, ii, jj, kk
    real*8  :: D, T
    
    ! output variables
    real*8, allocatable :: HessenbergMatrix(:,:)
    
    ! body of this function
    allocate(HessenbergMatrix(size(GeneralMatrix,1),size(GeneralMatrix,2)))
    HessenbergMatrix = GeneralMatrix
    M                = size(GeneralMatrix,1)
    N                = size(GeneralMatrix,2)
    if (M /= N) then
        write(*,*) "Error in general2HessenbergMatrix.f90. Input should be a Square matrix."
        stop
    end if
    
    do kk = 2,N-1,1
        D = 0.0D0
        do jj = kk,N,1
            if (abs(HessenbergMatrix(jj,kk-1)) > abs(D)) then
                D  = HessenbergMatrix(jj,kk-1)
                ii = jj
            end if
        end do
        if (abs(D)+1.0D0 /= 1.0D0) then
            if (ii /= kk) then
                do jj = kk-1,N,1
                    T                       = HessenbergMatrix(ii,jj)
                    HessenbergMatrix(ii,jj) = HessenbergMatrix(kk,jj)
                    HessenbergMatrix(kk,jj) = T
                end do
                do jj = 1,N,1
                    T                       = HessenbergMatrix(jj,ii)
                    HessenbergMatrix(jj,ii) = HessenbergMatrix(jj,kk)
                    HessenbergMatrix(jj,kk) = T
                end do
            end if
            do ii = kk+1,N,1
                T = HessenbergMatrix(ii,kk-1)/D
                HessenbergMatrix(ii,kk-1) = 0.0D0
                do jj = kk,N,1
                    HessenbergMatrix(ii,jj) = HessenbergMatrix(ii,jj)-T*HessenbergMatrix(kk,jj)
                end do
                do jj = 1,N,1
                    HessenbergMatrix(jj,kk) = HessenbergMatrix(jj,kk)+T*HessenbergMatrix(jj,ii)
                end do
            end do
        end if
    end do
    
    return
    end subroutine general2HessenbergMatrix
    end module general2HessenbergMatrix_module
