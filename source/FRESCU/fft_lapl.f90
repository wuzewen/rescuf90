! fft_lapl.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    module fft_lapl_module
    contains
    subroutine fft_lapl(FRC,x,n,k2,Xout)
    
    !use ComplexVata_3D_module
    use fft4_module
    use ifft4_module
    use fft3_fftw_module
    use ifft3_fftw_module
    use fft3_fftw_pointer_module
    use ifft3_fftw_pointer_module
    !use bsxfunTimes_4D_cmplx_module
    use bsxfunTimes_4D_3D_cmplx_module
    use FORTRAN_RESCU_CALCULATION_TYPE

    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable, target :: k2(:,:,:)
    real*8, pointer :: k2p(:)
    complex*16, allocatable, target :: X(:,:)
    integer              :: n(3)
    complex*16, pointer :: Arr(:), Brr(:)
    !complex*16, allocatable :: Xcmplx(:,:)
    
    ! temporary variables
    !complex*16, allocatable :: tmp(:,:,:)!, tmp1(:,:,:,:)!, tmp2(:,:,:,:)!, k2tmp(:,:,:,:)
    integer :: ii
    complex*16, allocatable :: k2tmp(:,:,:)
    !type(ComplexVata_3D), allocatable :: CT(:)

    ! output variables
    complex*16, allocatable, target :: Xout(:,:)
    
    ! body of this function
    !write(*,*) "X ="
    !write(*,*)  X
    !write(*,*) "K2 ="
    !write(*,*)  K2
    !write(*,*) "n ="
    !write(*,*)  n
    !allocate(CT(size(x,2)))
    !do ii = 1,size(x,2),1
    !    allocate(CT(ii)%vata(n(1),n(2),n(3)))
    !    CT(ii) 
    !allocate(tmp1(n(1),n(2),n(3),size(x,2)))
    !tmp1 = reshape(X,(/n(1),n(2),n(3),size(x,2)/))
    !allocate(tmp2(n(1),n(2),n(3),size(x,2)))
    !allocate(tmp(n(1),n(2),n(3)))
    !allocate(k2tmp(size(k2,1),size(k2,2),size(k2,3)))
    !k2tmp = dcmplx(k2)
    allocate(xout(size(X,1),size(X,2)))
    !allocate(Arr(n(1)*n(2)*n(3)))
    !allocate(Brr(n(1)*n(2)*n(3)))
    !allocate(k2p(n(1)*n(2)*n(3)))
    !k2p => reshape(k2,(/n(1)*n(2)*n(3)/))
    do ii = 1,size(x,2),1
        !tmp = tmp1(:,:,:,ii)
        !allocate(tmp(n(1),n(2),n(3)))
        FRC%Arr = reshape(X(:,ii),(/n(1),n(2),n(3)/))
        !call fft3_fftw(n(1),n(2),n(3),tmp)
        call fft3_fftw(FRC)
        !call fft3_fftw(FRC,CT(ii)%vata)
        !FRC%Arr = FRC%Arr*k2tmp
        !Arr => X(:,ii)
        !Brr => Xout(:,ii)
        !call fft3_fftw_pointer(FRC%planfft,Arr,Brr)
        FRC%Arr = FRC%Arr*k2
        !Brr = Brr*reshape(k2,(/n(1)*n(2)*n(3)/))
        !tmp1(:,:,:,ii) = tmp
        call ifft3_fftw(FRC)
        !call ifft3_fftw_pointer(FRC%planifft,Brr,Brr)
        Xout(:,ii) = reshape(FRC%Arr,(/size(X,1)/))
    end do
    !deallocate(k2tmp)
    !call fft4(tmp1,1,tmp2)
    !call fft4(tmp2,2,tmp1)
    !call fft4(tmp1,3,tmp2)
    
    !allocate(k2tmp(size(k2,1),size(k2,2),size(k2,3),1))
    !k2tmp(:,:,:,1) = dcmplx(k2)
    !call bsxfunTimes_4D_cmplx(tmp2,k2tmp,tmp1)
    !allocate(k2tmp(size(k2,1),size(k2,2),size(k2,3)))
    !k2tmp = dcmplx(k2)
    !call bsxfunTimes_4D_3D_cmplx(tmp2,k2tmp,tmp1)


    !do ii = 1,size(x,2),1
    !    tmp1(:,:,:,ii) = tmp1(:,:,:,ii)*k2tmp
    !end do

    !do ii = 1,size(x,2),1
    !    call ifft3_fftw(FRC,CT(ii)%vata)
        !tmp = tmp1(:,:,:,ii)
        !call ifft3_fftw(FRC,tmp)
        !call ifft3_fftw(n(1),n(2),n(3),tmp)
        !tmp1(:,:,:,ii) = tmp
    !end do
    !call ifft4(tmp1,3,tmp2)
    !call ifft4(tmp2,2,tmp1)
    !call ifft4(tmp1,1,tmp2)
    
    !allocate(xout(size(X,1),size(X,2)))
    !Xout = reshape(tmp1,(/size(X,1),size(X,2)/))
    !xout = dble(reshape(tmp2,(/product(n),size(tmp1,4)/)))

    !deallocate(tmp1,tmp,k2tmp)
    !write(*,*) "Xout ="
    !write(*,*)  Xout
    !write(*,*) "Stop in fft_lapl."
    !stop
    
    return
    end subroutine fft_lapl
    end module fft_lapl_module
