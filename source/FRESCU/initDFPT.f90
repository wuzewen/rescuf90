! initDFPT.f90
    
!******************************************************************************
    !
    !
    !
!******************************************************************************
    
    module initDFPT_module
    contains
    subroutine initDFPT(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType) :: inputFromFile
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%dfpt) then
        if (trim(FRC%info%calculationType) ==  trim("DFPT")) then
            write(*,*) "Error in initDFPT.f90. DFPT is not available now."
        end if
        
        FRC%init%dfpt = .TRUE.
    end if
    
    return
    end subroutine initDFPT
    end module initDFPT_module