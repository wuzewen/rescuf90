! SummeryOfBas.f90
    
!***********
    !
    !
    !
!************
    
    module SummeryOfBas_module
    
    type :: orbiType
        integer  :: rr, dr, fr, qq, fq, qw
    end type orbiType
    
    type :: SummeryOfBas
        integer  :: Vloc, Vlocrr, Vlocdr, Vlocvv
        integer  :: Rloc, Rlocrr, Rlocdr, Rlocrho
        integer  :: Vnl,  Vnlrr,  Vnldr,  Vnlvv,   Vnlqq, Vnlfq, Vnlqw
        integer  :: Vna,  Vnarr,  Vnadr,  Vnavv
        integer  :: Rna,  Rnarr,  Rnadr,  Rnarho
        integer  :: PseP, PsePrr, PsePdr, PsePvvS, PsePvvU
        integer  :: Orbit
        type(orbiType), allocatable :: orbi(:)
    end type SummeryOfBas
    
    end module SummeryOfBas_module
    
