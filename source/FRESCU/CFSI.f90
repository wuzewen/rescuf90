! CSFI. f90

!***************************************************
      !
      ! This is the subroutine for solving KS equation with CFSI method. 
      ! CFSI will ergodic every K point and every spin.
      ! 
      ! Subroutine cfsiKpt  is used to get new subspace.
      ! Subroutine ProjectedHamiltonian is used to get the projected
      ! Hamiltonian.
      ! Subroutine diag_proj is used to solve the eigen values of the
      ! projected Hamiltonian.
      !
      ! New subspace gotten from cfsiKpt is the new wavefunction, which will be used to
      ! calculate the electronic density.
      ! Eigen values gotten from diag_proj is the eigen energies in
      ! corresponding SCF loop. 
      !
!***************************************************

      module CFSI_module
      contains
      subroutine CFSI(FRC,LB,UB)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use VataMN_CMPLX_2D_module
      use UpdateVeff_module
      use distmat_allgather_1D_module
      use GetKBorb_module
      use GetKBorb_Sparse_module
      use calcKineticPrecond_module
      use cfsikpt_module
      use ProjectHamiltonian_module
      use diag_proj_module
      use calcev_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      real*8, allocatable             :: LB(:,:), UB(:,:)

      ! temporary variables
      logical :: gpustat, iscomplex
      integer :: gpurank, iter, ispin, nspin, nkpt, precond
      integer :: kk, ss
      real*8  :: kpt(3), e, c
      real*8, allocatable   :: kdir(:,:), prekin(:,:,:), vloc(:)
      real*8, allocatable   :: theta(:)
      type(VataMN_CMPLX_2D) :: XHX, XSX
      type(VataMN_1D) :: veff
      type(VataMN_CMPLX_2D) :: ZZ

      character(len=20)    :: flax, numb
      integer :: cou

      ! output variables

      ! body of this function
      !write(*,*) "I'm here in CFSI."
      gpustat  = FRC%gpu%status
      !gpurank  = FRC%gpu%rank
      iter     = FRC%scloop
      ispin    = FRC%spin%ispin
      nspin    = FRC%spin%nspin
      nkpt     = size(FRC%kpoint%ikdirect,1)
      allocate(kdir(nkpt,3))
      kdir     = FRC%kpoint%ikdirect
      precond  = FRC%eigensolver%precond
      call UpdateVeff(FRC,FRC%potential%veffin(1),FRC%potential%vps,.FALSE.,veff)
      !write(*,*) "In CFSI, veff ="
      !write(*,*)  veff%vata
      !allocate(vloc(size(veff%vata)))
      call distmat_allgather_1D(FRC,veff,vloc)
      FRC%potential%vloc(:,1) = vloc

      !write(*,*) "in cfsi, precond ="
      !write(*,*)  precond
      do kk = 1,nkpt,1
          kpt = kdir(kk,:)
          !deallocate(FRC%potential%vnl%KBorb)
          if (FRC%potential%vnlScheme) then
                  if (FRC%potential%vnl%KBorbIsSparse) then
                          call GetKBorb_Sparse(FRC,kpt)
                  else
                          call GetKBorb(FRC,kpt)
                  end if
          end if
          if (precond >= 0) then
                  call calcKineticPrecond(FRC,precond,kpt,prekin)
          else
                  !prekin = 0.0D0
          end if

          do ss = 1,nspin,1
              e = (UB(kk,ss)-LB(kk,ss))/2.0D0
              c = (UB(kk,ss)+LB(kk,ss))/2.0D0

              !if (ispin == 1 .or. ispin == 2) then
 
              !write(*,*) "In cfsi, before cfsiKpt, psi ="
              !write(*,*) "kk=",kk,"ss",ss
              !write(*,*)  FRC%psi(kk,ss)%vata
              !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
              !%%%% Generate new subspace.                                     %%%%!
              !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
              if (gpustat .and. (ispin == 1 .or. ispin == 2)) then
                      write(*,*) "Error CFSI.f90, GPU is not avalaible."
              else if (ispin == 1 .or. ispin == 2) then
                      if (any(kpt /= 0)) then
                              iscomplex = .TRUE.
                      else
                              iscomplex = .FALSE.
                      end if
                      !write(*,*) 'IN CFSI, before cfsiKpt.'
                      !write(*,*) 'size of psi CFSI.',size(FRC%psi(kk,ss)%vata,1), size(FRC%psi(kk,ss)%vata,2)
                      call cfsiKpt(FRC,kpt,ss,c,e,FRC%psi(kk,ss),prekin,iscomplex)
                      !call cfsiKpt(FRC,kpt,ss,c,e,FRC%psi(kk,ss),prekin,iscomplex,FRC%psi(kk,ss))
                      !write(*,*) 'IN CFSI, after cfsiKpt.'
                      !write(*,*) 'size of psi CFSI.',size(FRC%psi(kk,ss)%vata,1), size(FRC%psi(kk,ss)%vata,2)
              else if (ispin == 4) then
                      write(*,*) "Error CFSI. no spin."
              end if

              !write(numb,"(i4.4)") FRC%mpi%rank
              !flax = 'CFSIpsi'//trim(numb)//'.txt'
              !cou  =  FRC%mpi%rank + 10
              !open(unit=cou,file=flax)
              !write(cou,*)  FRC%mpi%rank
              !write(cou,*) 'IN CFSI, after cfsiKpt.'
              !write(cou,*) 'size()',size(FRC%psi(kk,ss)%vata,1),size(FRC%psi(kk,ss)%vata,2)
              !write(cou,*)  FRC%psi(kk,ss)%vata(:,1)
              !close(cou)
              !stop
              !write(*,*) 'IN CFSI, after cfsiKpt.'
              !write(*,*) "In cfsi, psi ="
              !write(*,*) "kk=",kk,"ss",ss
              !write(*,*) 'size()',size(FRC%psi(kk,ss)%vata,1),size(FRC%psi(kk,ss)%vata,2)
              !write(*,*)  FRC%psi(kk,ss)%vata(1:10,1)
              !stop

              !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
              !%%%% Generate projected Hamiltonian.                            %%%%!
              !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
              if (ispin == 1 .or. ispin == 2) then
                      call projectHamiltonian(FRC,kpt,FRC%psi(kk,ss),XHX,XSX)
              else if (ispin == 4) then
                      write(*,*) "Error CFSI. no spin."
              end if

              !write(numb,"(i4.4)") FRC%mpi%rank
              !flax = 'XHX'//trim(numb)//'.txt'
              !cou  =  FRC%mpi%rank + 10
              !open(unit=cou,file=flax)
              !write(cou,*)  FRC%mpi%rank
              !write(cou,*) 'in SFSI, after projH.'
              !write(cou,*) 'size of psi CFSI.',size(FRC%psi(kk,ss)%vata,1), size(FRC%psi(kk,ss)%vata,2)

              !write(cou,*) 'XHX'
              !write(cou,*)  XHX%vata
              !write(cou,*) 'XSX'
              !write(cou,*)  XSX%vata
              !close(cou)
              !stop
              if (any(kpt /= 0.0) .or. ispin == 4) then
                      iscomplex = .TRUE.
              else
                      iscomplex = .FALSE.
              end if
              
              !write(*,*) "In CFSI, XHX ="
              !write(*,*)  XHX%vata
              !write(*,*) "In CFSI, XSX ="
              !write(*,*)  XSX%vata

              !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
              !%%%% Diagnore projected Hamiltonian and get eigen energies.    %%%%!
              !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
              call diag_proj(FRC,XHX,XSX,iscomplex,ZZ,theta)

              !write(*,*) 'in SFSI, after diag_proj.'
              !write(*,*) 'size of psi CFSI.',size(FRC%psi(kk,ss)%vata,1), size(FRC%psi(kk,ss)%vata,2)

              FRC%energy%ksnrg%vata(:,kk,ss) = theta

              !write(*,*) "ZZ ="
              !write(*,*)  ZZ%vata
              !write(*,*) "FRC%energy%ksnrg%vata ="
              !write(*,*)  FRC%energy%ksnrg%vata
              !write(*,*) 'In CFSI, before calcev.'
              !write(*,*) 'size of ZZ CFSI.', size(ZZ%vata,1), size(ZZ%vata,2)

              if (ispin == 1 .or. ispin == 2) then
                      !call calcev(FRC,FRC%psi(kk,ss),ZZ,FRC%psi(kk,ss))
                      call calcev(FRC,FRC%psi(kk,ss),ZZ)
                      deallocate(XHX%vata,XSX%vata)
              else if (ispin == 4) then
                      write(*,*) "Error CFSI. no spin."
              end if


              !write(*,*) "FRC%psi(kk,ss) ="
              !write(*,*)  FRC%psi(kk,ss)%vata

              deallocate(ZZ%vata,theta)
          end do
          if (precond >= 0) then
                  deallocate(prekin)
          end if
      end do

      return
      end subroutine CFSI
      end module CFSI_module



