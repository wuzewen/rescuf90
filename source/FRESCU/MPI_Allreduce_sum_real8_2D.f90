! MPI_Allreduce_sum_real8_2D.f90

!***********************************************************************
      !
      !
      !
!***********************************************************************

      module MPI_Allreduce_sum_real8_2D_module
      contains
      subroutine MPI_Allreduce_sum_real8_2D(vari,ranks,redvar)

      use MEX_ALLREDUCE_SUM_INTEG_module
      use MEX_ALLREDUCE_SUM_REAL8_2D_module

      include 'mpif.h'

      ! input variables
      real*8 , allocatable :: vari(:,:), redvar(:,:)
      integer, allocatable :: ranks(:)

      ! temporary variables
      integer :: mpirank, mpisize, ierr, rankn, ii
      integer, allocatable :: sendinfo(:), recvinfo(:)
      integer :: nvar
      real*8 , allocatable :: redvart(:,:), varitmp(:,:)

      ! output variables

      ! body of this function
      call MPI_COMM_RANK(MPI_COMM_WORLD,mpirank,ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD,mpisize,ierr)

      if (all(ranks /= mpirank)) then
              redvar = 0.0D0
              return
      end if

      rankn = size(ranks)

      allocate(sendinfo(16),recvinfo(16))
      sendinfo    = 0
      sendinfo(1) = 0
      sendinfo(2) = 0

      ! replace the following with mpi functions
      call MEX_ALLREDUCE_SUM_INTEG(sendinfo,recvinfo,16,rankn,ranks)

      nvar = size(vari)

      if (nvar == 1) then
              allocate(redvart(2,1))
              allocate(varitmp(2,1))
              varitmp(1,1) = vari(1,1)
              varitmp(2,1) = 0.0D0
              call MEX_ALLREDUCE_SUM_REAL8_2D(varitmp,redvart,2,rankn,ranks)
              redvar(1,1)  = redvart(1,1)
              deallocate(redvart,varitmp)
      else
              redvar = 0.0D0
              call MEX_ALLREDUCE_SUM_REAL8_2D(vari,redvar,nvar,rankn,ranks)
      end if

      return
      end subroutine MPI_Allreduce_sum_real8_2D
      end module MPI_Allreduce_sum_real8_2D_module
