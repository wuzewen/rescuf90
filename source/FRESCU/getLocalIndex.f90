! getLocalIndex.f90
    
!*****************************************
    !
    !
    !
!*****************************************
    
    module getLocalIndex_module
    contains
    subroutine getLocalIndex(tx,ty,tz,nvec,inde)
    
    use sub2ind_module
    
    implicit none
    
    ! input variables
    integer, allocatable :: tx(:,:,:), ty(:,:,:), tz(:,:,:)
    integer              :: nvec(3)
    
    ! temporary variables
    integer, allocatable :: xt(:,:,:), yt(:,:,:), zt(:,:,:)
    integer              :: n1, n2, n3, ii, jj, kk
    
    ! ouput variables
    integer, allocatable :: inde(:,:,:)
    
    ! body of this function
    n1 = size(tx,1)
    n2 = size(tx,2)
    n3 = size(tz,3)
    allocate(xt(n1,n2,n3),yt(n1,n2,n3),zt(n1,n2,n3))
    
    do ii = 1,n1,1
        do jj = 1,n2,1
            do kk = 1,n3,1
    !forall(ii=1:n1, jj=1:n2, kk=1:n3)
                if (tx(ii,jj,kk)<=0 .or. tx(ii,jj,kk) == nvec(1)) then
                    xt(ii,jj,kk) = nvec(1)-mod(-tx(ii,jj,kk),nvec(1))
                else
                    xt(ii,jj,kk) = -mod(-tx(ii,jj,kk),nvec(1))
                    if (xt(ii,jj,kk) == 0) then
                            xt(ii,jj,kk) = nvec(1)
                    end if
                end if
                
                if (ty(ii,jj,kk)<=0 .or. ty(ii,jj,kk) == nvec(2)) then
                    yt(ii,jj,kk) = nvec(2)-mod(-ty(ii,jj,kk),nvec(2))
                else
                    yt(ii,jj,kk) = -mod(-ty(ii,jj,kk),nvec(2))
                    if (yt(ii,jj,kk) == 0) then
                            yt(ii,jj,kk) = nvec(2)
                    end if
                end if
                
                if (tz(ii,jj,kk)<=0 .or. tz(ii,jj,kk) == nvec(3)) then
                    zt(ii,jj,kk) = nvec(3)-mod(-tz(ii,jj,kk),nvec(3))
                else
                    zt(ii,jj,kk) = -mod(-tz(ii,jj,kk),nvec(3))
                    if (zt(ii,jj,kk) == 0) then
                            zt(ii,jj,kk) = nvec(3)
                    end if
                end if
    !end forall
            end do
        end do
    end do
    
    
    !write(*,*) "xt in getLocalIndex"
    
    !write(*,*)  nvec(1), -tx(9,9,1), mod(-tx(9,9,1),nvec(1))
    !write(*,*)  yt(:,:,5)
    
    !allocate(inde(n1,n2,n3))
    
    
    call sub2ind(nvec,xt,yt,zt,inde)
    
    return
    end subroutine getLocalIndex
    end module getLocalIndex_module
