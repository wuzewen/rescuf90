! RealVata_3D.f90
    
!***********************************************************
    !
    ! This module is used to define a type of data.
    ! The data is 3 dimentional allocatable real array.
    !
!***********************************************************
    
    module RealVata_3D_module
    
    type :: RealVata_3D
        real*8, allocatable :: vata(:,:,:)
    end type RealVata_3D
    
    end module RealVata_3D_module
