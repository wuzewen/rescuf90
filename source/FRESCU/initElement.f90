! initElement.f90
!*********************************
    !
!*********************************
    
    module initElement_module
    contains
    subroutine initElement(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    use readSummeryOfBasis_module
    use readPseudopotential_module
    use parseKBinfo_module
    use parseAOinfo_module
    !use AtomSymbolType_module
    use writeSOB_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType) :: inputFromFile
    !type(AtomSymbolType),    allocatable :: AtomSymbol(:)
    
    ! temperary variables
    integer                              :: mpirank, ii, jj, nspec
    real*8                               :: bohr2ang
    character(len=4)                     :: species
    
    ! output variables
    
    ! body of this function
    mpirank  = FRC%mpi%rank
    bohr2ang = 0.52917721067D0
    
    if (.not. FRC%init%atominfo) then
        nspec = FRC%atom%NumberOfElement
        !write(*,*) "nspec in initElement.", nspec
        allocate(FRC%SOB(nspec))
        allocate(FRC%ElementData(nspec))
        do ii = 1,nspec,1
            species = FRC%element(ii)%species
            call readSummeryOfBasis(species,FRC%SOB(ii))
            !call writeSOB(FRC)
            ! allocate Vlocal
            allocate(FRC%ElementData(ii)%Vlocal%rrData(FRC%SOB(ii)%Vlocrr))
            allocate(FRC%ElementData(ii)%Vlocal%drData(FRC%SOB(ii)%Vlocdr))
            allocate(FRC%ElementData(ii)%Vlocal%vvData(FRC%SOB(ii)%Vlocvv))
            
            ! allocate Rlocal
            allocate(FRC%ElementData(ii)%Rlocal%rrData(FRC%SOB(ii)%Rlocrr))
            allocate(FRC%ElementData(ii)%Rlocal%drData(FRC%SOB(ii)%Rlocdr))
            allocate(FRC%ElementData(ii)%Rlocal%rhoData(FRC%SOB(ii)%Rlocrho))
            
            ! allocate vnl
            allocate(FRC%ElementData(ii)%Vnl(FRC%SOB(ii)%Vnl))
            do jj = 1,FRC%SOB(ii)%Vnl,1
                allocate(FRC%ElementData(ii)%Vnl(jj)%rrData(FRC%SOB(ii)%Vnlrr))
                allocate(FRC%ElementData(ii)%Vnl(jj)%drData(FRC%SOB(ii)%Vnldr))
                allocate(FRC%ElementData(ii)%Vnl(jj)%vvData(FRC%SOB(ii)%Vnlvv))
                allocate(FRC%ElementData(ii)%Vnl(jj)%qqData(FRC%SOB(ii)%Vnlqq))
                allocate(FRC%ElementData(ii)%Vnl(jj)%fqData(FRC%SOB(ii)%Vnlfq))
                allocate(FRC%ElementData(ii)%Vnl(jj)%qwData(FRC%SOB(ii)%Vnlqw))
            end do
            
            ! allocate Vna
            allocate(FRC%ElementData(ii)%Vna%rrData(FRC%SOB(ii)%Vnarr))
            allocate(FRC%ElementData(ii)%Vna%drData(FRC%SOB(ii)%Vnadr))
            allocate(FRC%ElementData(ii)%Vna%vvData(FRC%SOB(ii)%Vnavv))
            
            ! allocate Rna
            allocate(FRC%ElementData(ii)%Rna%rrData(FRC%SOB(ii)%Rnarr))
            allocate(FRC%ElementData(ii)%Rna%drData(FRC%SOB(ii)%Rnadr))
            allocate(FRC%ElementData(ii)%Rna%rhoData(FRC%SOB(ii)%Rnarho))
            
            ! allocate RelpseudoP
            allocate(FRC%ElementData(ii)%RelPseudoP(FRC%SOB(ii)%PseP))
            do jj = 1,FRC%SOB(ii)%PseP,1
                allocate(FRC%ElementData(ii)%RelPseudoP(jj)%rrData(FRC%SOB(ii)%PsePrr))
                allocate(FRC%ElementData(ii)%RelPseudoP(jj)%drData(FRC%SOB(ii)%PsePdr))
                allocate(FRC%ElementData(ii)%RelPseudoP(jj)%vvData_screened(FRC%SOB(ii)%PsePvvS))
                allocate(FRC%ElementData(ii)%RelPseudoP(jj)%vvData_unscreened(FRC%SOB(ii)%PsePvvU))
            end do
            
            ! allocate OrbitalSet
            allocate(FRC%ElementData(ii)%OrbitalSet(FRC%SOB(ii)%Orbit))
            do jj = 1,FRC%SOB(ii)%Orbit,1
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%rrData(FRC%SOB(ii)%orbi(jj)%rr))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%drData(FRC%SOB(ii)%orbi(jj)%dr))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%frData(FRC%SOB(ii)%orbi(jj)%fr))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%qqData(FRC%SOB(ii)%orbi(jj)%qq))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%fqData(FRC%SOB(ii)%orbi(jj)%fq))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%qwData(FRC%SOB(ii)%orbi(jj)%qw))
            end do
            !write(*,*) "I'm here after allocate in initElement."
            call readPseudoPotential(species,FRC%SOB(ii),FRC%ElementData(ii))
            !write(*,*) "I'm here after readbasis"
            if (FRC%spin%SOI) then
                write(*,*) "Error in initElement.f90. SOI is not available now."
                stop
                !call AddSOI(Element)
            end if
            
            !if (FRC%element(ii)%aoprec == "SZ") then
            !    FRC%element(ii)%aoZcutoff =1
            !else if (FRC%element(ii)%aoprec == "DZ") then
            !    FRC%element(ii)%aoZcutoff =2
            !end if
            do jj = 1,FRC%SOB(ii)%Vnl,1
                FRC%ElementData(ii)%Vnl(jj)%N = 1
            end do
        end do
        !write(*,*) "I'm here after loop"
        call parseAOinfo(FRC)
        call parseKBinfo(FRC)
        !write(*,*) "I'm here"
        do ii = 1,nspec,1
            FRC%element(ii)%Z          = sum(FRC%element(ii)%aopnu)
            FRC%element(ii)%valence    = sum(FRC%element(ii)%aopnu)
            FRC%element(ii)%vnlLcutoff = maxval(FRC%element(ii)%aolnu)+1
            FRC%element(ii)%aoLcutoff  = maxval(FRC%element(ii)%aolnu)+1
            FRC%element(ii)%aoZcutoff  = 3
            FRC%element(ii)%vnlNcutoff = 2
        end do
        
        FRC%init%Element = .TRUE.
    end if
    !write(*,*) "I'm here at the end of initElement."
    return
    end subroutine initElement
    end module initElement_module
    
    
