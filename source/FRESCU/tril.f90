! tril.f90
! this is a function which has the same function with tril in matlab
    
    module tril_module
    contains
    subroutine tril(A,k,B)
    
    ! input valuables
    integer             :: k
    real*8, allocatable :: A(:,:)
    
    ! temporery valuables
    integer             :: m, n
    integer             :: i, j
    
    ! output valuables
    real*8, allocatable :: B(:,:)
    
    ! body of the function
    m = size(A,1)
    n = size(A,2)
    B = 0.0D0
    do i = 1, m, 1
        do j = 1, n, 1
            if  (i+k >= j)  then
                B(i,j) = A(i,j)
            end if
        end do
    end do
    return
    end subroutine tril
    end module tril_module
