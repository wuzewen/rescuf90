! ind2sub_3D.f90

!*********************************************************************
      !
      ! The same function as function ind2sub.m in matlab.
      !
      ! siz : input,  one dimentional integer Array, size(siz) = 3
      ! ind : input,  integer
      ! kk  : output, integer
      ! ss  : output, integer
      ! mm  : output, integer
      !
!*********************************************************************

      module ind2sub_3D_module
      contains
      subroutine ind2sub_3D(siz,ind,kk,ss,mm)

      implicit none

      ! input varibales
      integer :: siz(3), ind

      ! temporary variables
      integer :: mtmp, ntmp

      ! output variables
      integer :: kk, ss, mm

      ! body of this function
      ntmp = siz(1)*siz(2)
      mtmp = mod(ind,ntmp)
      if (mtmp <= 0) then
              mtmp = mtmp + ntmp
      end if
      mm   = (ind-mtmp)/ntmp + 1

      kk = mod(mtmp,siz(1))
      if (kk <= 0) then
              kk = kk + siz(1)
      end if

      ss = (mtmp - kk)/siz(1)+1

      if (ss > siz(2)) then
              write(*,*) "Error in ind2sub.f90. Ind out of boundary."
              stop
      end if

      return
      end subroutine ind2sub_3D
      end module ind2sub_3D_module

