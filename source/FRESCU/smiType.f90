! smiType.f90
    
!**************
    !
    !
    !
!*****************
    
    module smiType_module
    
    type :: smiType
        real*8               :: abstol
        real*8               :: lwork
        integer              :: mb
        integer              :: mp
        integer              :: nb
        integer              :: np
        integer              :: mbpsi
        integer              :: nbpsi
        real*8               :: orfac
        logical              :: status
    end type smiType
    
    end module smiType_module