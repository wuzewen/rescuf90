! initTempSmear.f90
    
!***************************************************************************
    !
    !
    !
!***************************************************************************
    
    module initTempSmear_module
    contains
    subroutine initTempSmear(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    integer :: ii, nkpt, nline
    real*8  :: HA2EV, KBEV, KB
    logical :: smearingSigmaIsFind, smearingOrderIsFind, unitsSigmaIsFind
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%smearing) then
        HA2EV               =  27.21138602D0
        KBEV                =  8.6173303D-5
        KB                  =  KBEV/HA2EV
        smearingSigmaIsFind = .FALSE.
        smearingOrderIsFind = .FALSE.
        unitsSigmaIsFind    = .FALSE.
        nline               = inputFromFile%NumberOfInput
        !ii = 0
        !do while(ii<=nline)
        !    ii     = ii+1
        !    if (trim(inputFromFile(ii)%name) == trim("smearing")) then
        !        smearingIsFind = .TRUE.
        !        exit
        !    end if
        !end do
        
        if (.not. (trim(FRC%kpoint%sampling) == trim("fermi-dirac") .or. &
                trim(FRC%kpoint%sampling) == trim("tetrahedron") .or. &
                trim(FRC%kpoint%sampling) == trim("gauss") .or. &
                trim(FRC%kpoint%sampling) == trim("methfessel-paxton") .or. &
                trim(FRC%kpoint%sampling) == trim("tetrahedron+blochl"))) then
            write(*,*) "Error in initTempSmear.f90. Invalid value for FRC%kpoint%sampling."
            stop
        end if
        
        nkpt = size(FRC%kpoint%kdirect,1)
        if (nkpt < 4 .and. (trim(FRC%kpoint%sampling) == trim("tetrahedron") .or. &
                trim(FRC%kpoint%sampling) == trim("tetrahedron+blochl"))) then
            if (FRC%mpi%rank == 0) then
                write(*,*) "Warning. The tetrahedron methods requires at least 4 k-points, Gaussian smearing will be used instead."
                FRC%kpoint%sampling = "gauss"
            end if
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("smearingSigma")) then
                smearingSigmaIsFind = .TRUE.
                exit
            end if
        end do
        if (smearingSigmaIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%smearing%sigma
        else
            FRC%smearing%sigma = 0.001D0
        end if
        
        if (trim(FRC%kpoint%sampling) == trim("tetrahedron") .or. &
                trim(FRC%kpoint%sampling) == trim("tetrahedron+blochl")) then
            if (FRC%smearing%sigma /=0.0) then
                write(*,*) "Waring. Smearing should be set to 0 when using the tetrahedron method."
                FRC%smearing%sigma = 0.0D0
            end if
        end if
        
        !if (.not. smearingSigmaIsFind) then
        !    FRC%units.sigma = "Hartree"
        !end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("smearingOrder")) then
                smearingOrderIsFind = .TRUE.
                exit
            end if
        end do
        if (smearingOrderIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%smearing%order
        else
            FRC%smearing%order = 2
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("unitsSigma")) then
                unitsSigmaIsFind = .TRUE.
                exit
            end if
        end do
        if (.not. unitsSigmaIsFind) then
            FRC%units%sigma = "Hartree"
        else
            read(inputFromFile%NAV(ii)%value,*) FRC%units%sigma
        end if
        
        if (trim(FRC%units%sigma) == trim("ev")) then
            FRC%smearing%sigma = FRC%smearing%sigma/HA2EV
            FRC%units%sigma    = "Hartree"
        end if
        
        FRC%smearing%temperature = FRC%smearing%sigma/KB
        FRC%units%sigma          = "Hartree"
        
        FRC%init%smearing        = .TRUE.
    end if
        
    return
    end subroutine initTempSmear
    end module initTempSmear_module
