! err.f90

!***********************************************************************************
      !
      !
      !
!***********************************************************************************

      module errMod
      contains
      subroutine err(mes)

      implicit none

      character(len=100) :: mes
      integer            :: ii

      ! body of this function
      write(*,*) ' '
      do ii = 1,79,1
          write(*,FMT='(A1)', ADVANCE='NO') '*'
      end do
      write(*,FMT='(A1)',ADVANCE='YES') '*'
      write(*,*) trim(mes)
      do ii = 1,79,1
          write(*,FMT='(A1)',ADVANCE='NO') '*'
      end do
      write(*,FMT='(A1)',ADVANCE='YES') '*'

      return
      end subroutine err
      end module errMod
