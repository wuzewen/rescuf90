! intersectRow_3output.f90
    
!************************************************************************
    !
    !
    !
!************************************************************************
    
    module intersectRow_3output_module
    contains
    subroutine intersectRow_3output(A,B,samerow,iA,iB)
    
    use uniqueRow_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:)
    
    ! temporary varibales
    real*8, allocatable :: Atmp(:,:), Btmp(:,:)
    integer :: mA, nA, mB, nB, ntmp, mtmp1, mtmp2, ii, jj
    real*8  :: tmp(3)
    
    ! output varibales
    real*8,  allocatable :: samerow(:,:)
    integer, allocatable :: iA(:), iB(:)
    
    ! body of this function
    mA = size(A,1)
    nA = size(A,2)
    mB = size(B,1)
    nB = size(B,2)
    
    if (nA /= nB) then
        write(*,*) "Error in intersectRow.f90. Sizes of the inputs are invalid."
        stop
    end if
    !write(*,*) "I'm here before uniqueRow in intersectRow_3output."
    !write(*,*) "A ="
    !write(*,*)  A
    call uniqueRow(A,Atmp)
    !write(*,*) "I'm here after uniqueRow A in intersectRow_3output."
    call uniqueRow(B,Btmp)
    !write(*,*) "I'm here after uniqueRow B in intersectRow_3output."
    mA = size(Atmp,1)
    nA = size(Atmp,2)
    mB = size(Btmp,1)
    nB = size(Btmp,2)
    
    ntmp = 0
    do ii = 1,mA,1
        do jj = 1,mB,1
            if (all(Atmp(ii,:) == Btmp(jj,:))) then
                ntmp = ntmp+1
                !allocate(outm(ntmp,nA))
            end if
        end do
    end do
    allocate(samerow(ntmp,nA),iA(ntmp),iB(ntmp))
    ntmp = 0
    do ii = 1,mA,1
        do jj = 1,mB,1
            if (all(Atmp(ii,:) == Btmp(jj,:))) then
                ntmp            = ntmp+1
                samerow(ntmp,:) = Atmp(ii,:)
                !iA(ntmp)        = ii
                !iB(ntmp)        = jj
            end if
        end do
    end do
    
    !mtmp1 = 0
    !mtmp2 = 0
    !do ii = 1,ntmp,1
    !    do jj = 1,mA,1
    !        if (all(A(jj,:) == samerow(ii,:))) then
    !            mtmp1 = mtmp1+1
    !            !exit
    !        end if
    !    end do
    !    do jj = 1,mB,1
    !        if (all(B(jj,:) == samerow(ii,:))) then
    !            mtmp2 = mtmp2+1
    !            !exit
    !        end if
    !    end do
    !end do
    !allocate(iA(mtmp1),iB(mtmp2))
    mA    = size(A,1)
    mB    = size(B,1)
    mtmp1 = 0
    mtmp2 = 0
    do ii = 1,ntmp,1
        mtmp1 = mtmp1+1
        do jj = 1,mA,1
            tmp(:) = A(jj,:)-samerow(ii,:)
            if (all(tmp == 0.00000)) then
                !mtmp1 = mtmp1+1
                iA(mtmp1) = jj
                exit
            end if
        end do
        do jj = 1,mB,1
            tmp(:) = B(jj,:)-samerow(ii,:)
            if (all(tmp == 0.00000)) then
                !mtmp2 = mtmp2+1
                iB(mtmp1) = jj
                exit
            end if
        end do
    end do
    
    return
    end subroutine intersectRow_3output
    end module intersectRow_3output_module
