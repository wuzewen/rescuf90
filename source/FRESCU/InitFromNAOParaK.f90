! InitFromNAOParaK.f90

!***********************************************
      !
      !
      !
!****************************************************

      module InitFromNAOParaKMod
      contains
      subroutine InitFromNAOParaK(FRC,time)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none


      ! body of this function
      mpirank = FRC%mpi%rank
      ispin   = FRC%spin%ispin
      nspin   = FRC%spin%nspin
      time    = 0

      call GetAtomicOrbitalSubspace(FRC)

      if (mpirank .EQ. 0) then
              write(*,*) ' time'
      end if

      nkpt = size(FRC%kpoint%ikdirect,1)
      allocate(kdir(nkpt,3))
      kdir = FRC%kpoint%ikdirect
      call inittime(FRC)
      call ks_main_lcao_scf(FRC)

      if (FRC%LCAO%dynSubRed) then
          if (product(FRC%domain%cgridn) .NE. FRC%LCAO%aocell(1)%m) then
              call GetAtomicOrbitalSubspace(FRC,FRC%domain%cgridn,.FALSE.)
          end if
          aocell   = FRC%LCAO%aocell
          aov      = FRC%LCAO%aovec
          aosparse = FRC%LCAO%aosparse
      else
          if (product(FRC%domain%cgridn) .NE. FRC%LCAO%aocell(1)%m) then
              call GetAtomicOrbitalSubspace(FRC,FRC%domain%cgridn,.FALSE.)
          end if
          aocell   = FRC%LCAO%aocell
      end if

      do kk = 1,nkpt,1
          kpt = kdir(kk,:)
          if (FRC%LCAO%dynSubRed) then
              call reduceOper(aocell,aov,kpt,aosparse,aosub)
          else
              aosub = aocell(kk)
          end if
          do ii = 1,nspin,1
              if (ispin .EQ. 1 .OR. ispin .EQ. 2) then
                  call calcev_lcao(FRC,aosub,FRC%psi(kk,ii))
              else if (ispin .EQ. 4) then
                  call calcev_lcao(FRC,aosub,FRC%psi(kk,1))
                  call calcev_lcao(FRC,aosub,FRC%psi(kk,2))
              end if
          end do
      end do

      if (mpirank .EQ. 0) then
          write(*,*) 'time'
      end if

      return
      end subroutine InitFromNAOParaK
      end module InitFromNAOParaKMod
