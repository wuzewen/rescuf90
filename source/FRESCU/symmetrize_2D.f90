! symmetrize_2D.f90
    
    module symmetrize_2D_module
    contains
    subroutine symmetrize_2D(FRC,vector,nvec,symrec,symt)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use upGradeGrid_module
    use fftFreqA_module
    use NDgrid_module
    use bsxfunMod_module
    use fft4_module
    use ifft4_module
    use fft3_fftw_module
    use ifft3_fftw_module
    use fft3_fftw_pointer3D_module
    use ifft3_fftw_pointer3D_module
    use bsxfunTimes_module
    use bsxfunTimes_CMPLX_module
    
    implicit none
    
    ! input valuables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer              :: nvec(3)
    real*8 , allocatable :: vector(:,:)
    !real*8 , allocatable :: hartreePotential(:,:,:)
    real*8 , allocatable :: symrec(:,:,:), symt(:,:)
    
    ! temporery valuables
    integer              :: ii, jj, kk, nnt
    integer              :: ngrid, nblk, nsym, nxtmp1, nytmp1, nztmp1, nxtmp2, nytmp2, nztmp2
    integer              :: nv2(3), nv(3), cnx, cny, cnz, fnx, fny, fnz, nxtmp, nytmp, nztmp
    integer              :: ng2, nva, tmp(3,1)
    real*8               :: eyem(3,3), pi
    integer, allocatable :: ndx(:), ndy(:), ndz(:)
    integer, allocatable :: gd(:,:), gd1(:,:), symop(:,:,:)
    integer, allocatable :: inde(:,:)
    real*8 , allocatable :: v_rec(:,:,:,:), vrec(:,:,:,:), t(:,:)
    real*8 , allocatable :: fftfreq(:,:), ttmp(:,:), symtmp(:,:)
    real*8 , allocatable :: ndxr(:), ndyr(:), ndzr(:), gdt(:,:,:), dex(:,:,:), phase(:,:,:)
    real*8 , allocatable :: gd_tmp(:,:), gd_tmp2(:,:), nvr(:,:)
    complex*16              :: imnu
    complex*16, allocatable, target :: v_rec_c(:,:,:,:), vrec_c(:,:,:,:)
    complex*16, allocatable :: symtmpt(:,:,:,:)
    complex*16, allocatable :: symvtr(:,:,:,:)
    complex*16, allocatable :: symvtmp(:,:), v_rectm(:,:), tmpt(:,:)
    complex*16, allocatable, target :: symvt(:,:,:,:)
    complex*16, allocatable :: phase0(:,:), v_rectm2(:,:)
    complex*16, allocatable :: vrec_tmp(:,:,:)
    complex*16, pointer     :: vrecp1(:,:,:), vrecp2(:,:,:)
    ! output valuables
    real*8, allocatable  :: symv(:,:)
    
    ! body of this function
    !write(*,*) "I'm here at the begining of symmetrize_2D."
    pi    = 3.1415926535897932385D0
    ngrid = size(vector,1)
    nblk  = size(vector,2)
    nsym  = size(symrec,3)
    imnu  = (0.0D0,1.0D0)
    !write(*,*) "vector in symmetrized_2D."
    !write(*,*)  vector
    !open(unit=11,file='rhosym.txt')
    !do ii = 1,size(vector,1),1
    !write(11,*) vector(ii,1)
    !end do
    !close(11)
    !write(*,*) "size of symt =", size(symt,1), size(symt,2)
    !open(unit=11,file='symt.txt')
    !do ii = 1,size(symt,1),1
    !write(11,*)  symt(ii,:)
    !end do
    !close(11)
    !write(*,*) "size of symt =", size(symrec,1), size(symrec,2), size(symrec,3)
    !open(unit=11,file='symrec.txt')
    !do kk = 1,size(symrec,3),1
    !do jj = 1,size(symrec,2),1
    !do ii = 1,size(symrec,1),1
    !write(11,*) symrec(ii,jj,kk)
    !end do
    !end do
    !end do
    !close(11)
    !write(*,*) "nsym = ", nsym
    
    call upGradeGrid(nvec,symrec,nv2)
    ng2 = product(nv2)
    
    !call eye(3,eyem)
    forall(ii=1:3)
        eyem(ii,ii) = 1.0D0
    end forall
    
    call fftFreqA(nv2,eyem,fftfreq)
    !write(*,*) "fftfreq in symmetrize_2D."
    !do ii = 1,size(fftfreq,1),1
    !write(*,*) ii, fftfreq(ii,:)
    !end do
    allocate(ndx(nv2(1)),ndy(nv2(2)),ndz(nv2(3)))
    forall(ii=1:nv2(1))
        ndx(ii) = ii-1
    end forall
    forall(ii=1:nv2(2))
        ndy(ii) = ii-1
    end forall
    forall(ii=1:nv2(3))
        ndz(ii) = ii-1
    end forall
    
    !call arrayN(0,nv2(1)-1,ndx)
    !call arrayN(0,nv2(2)-1,ndy)
    !call arrayN(0,nv2(3)-1,ndz)
    allocate(ndxr(nv2(1)),ndyr(nv2(2)),ndzr(nv2(3)))
    allocate(gdt(nv2(1),nv2(2),nv2(3)),dex(nv2(1),nv2(2),nv2(3)),phase(nv2(1),nv2(2),nv2(3)))
    ndxr = dble(ndx)
    ndyr = dble(ndy)
    ndzr = dble(ndz)
    call ndgrid(ndxr,ndyr,ndzr,gdt,dex,phase)
    nva = nv2(1)*nv2(2)*nv2(3)
    allocate(gd(nva,3))
    gd(:,1) = reshape(gdt,(/nva/))
    gd(:,2) = reshape(dex,(/nva/))
    gd(:,3) = reshape(phase,(/nva/))
    !write(*,*) "gd in symmetrize_2D."
    !do ii = 1,size(gd,1),1
    !write(*,*) ii, gd(ii,:)
    !end do
    allocate(vrec(nvec(1),nvec(2),nvec(3),nblk))
    vrec = reshape(vector,(/nvec(1),nvec(2),nvec(3),nblk/))
    !write(*,*) "vrec in symmetrize_2D."
    !do ii = 1,size(vrec,4),1
    !do jj = 1,size(vrec,3),1
    !do kk = 1,size(vrec,1),1
    !write(*,*) vrec(kk,:,jj,ii)
    !end do
    !end do
    !end do
    vrec = cshift(vrec,-1,1)
    vrec = cshift(vrec,-1,2)
    vrec = cshift(vrec,-1,3)
    !write(*,*) "vrec in symmetrize_2D."
    !nnt = 0
    !    do ii = 1,size(vrec,4),1
    !        do jj = 1,size(vrec,3),1
    !        nnt = nnt +1    
    !        do kk = 1,size(vrec,1),1
    !          write(*,*) nnt
    !                write(*,*) vrec(kk,:,jj,ii)
    !                    end do
    !                        end do
    !                            end do
    allocate(vrec_c(nvec(1),nvec(2),nvec(3),nblk))
    allocate(v_rec_c(nvec(1),nvec(2),nvec(3),nblk))
    vrec_c  = dcmplx(vrec)


    !allocate(vrecp1(nvec(1),nvec(2),nvec(3)))
    !allocate(vrecp2(nvec(1),nvec(2),nvec(3)))
    !allocate(vrec_tmp(nvec(1),nvec(2),nvec(3)))
    do ii = 1,nblk,1
        FRC%Arr = vrec_c(:,:,:,ii)
        call fft3_fftw(FRC)
        v_rec_c(:,:,:,ii) = FRC%Arr
        !vrecp1 => vrec_c(:,:,:,ii)
        !vrecp2 => v_rec_c(:,:,:,ii)
        !call fft3_fftw_pointer3D(FRC%planfft,vrecp1,vrecp2)
        !vrec_tmp = vrec_c(:,:,:,ii)
        !call fft3_fftw(FRC,vrec_tmp)
        !call fft3_fftw(nvec(1),nvec(2),nvec(3),vrec_tmp)
        !v_rec_c(:,:,:,ii) = vrec_tmp
    end do


    !v_rec_c = cmplx(v_rec_c)
    !call fft4(vrec_c,3,v_rec_c)
    !call fft4(v_rec_c,2,vrec_c)
    !call fft4(vrec_c,1,v_rec_c)

    
    !write(*,*) "v_rec_c in symmetrize_2D."
    !do ii = 1,size(v_rec_c,4),1
    !    do jj = 1,size(v_rec_c,3),1
    !        do kk = 1,size(v_rec_c,1),1
    !            write(*,*) v_rec_c(kk,:,jj,ii)
    !        end do
    !    end do
    !end do


    if (.not. all(nv2 == nvec)) then
        write(*,*) "Error in symmetrize_1D.f90. nv2 should equal to nvec."
        stop
        !cnx    = nvec(1)
        !cny    = nvec(2)
        !cnz    = nvec(3)
        !fnx    = nv2(1)
        !fny    = nv2(2)
        !fnz    = nv2(3)
        !allocate(symv(nv2(1),nv2(2),nv2(3),nblk))
        !symv   = 0
        !nxtmp1 = cnx/2+1
        !nytmp1 = cny/2+1
        !nztmp1 = cnz/2+1
        !forall(ii=1:nxtmp,jj=1:nytmp,kk=1:nztmp)
        !    symv(ii) = v_rec(ii,jj,kk,:)
        !end forall
        !nxtmp2 = fnx-(cnx-cnx/2-1)
        !nytmp2 = fny-(cny-cny/2-1)
        !nztmp2 = fnz-(cnz-cnz/2-1)
        !forall(ii=nxtmp2:fnx,jj=nytmp2:fny,kk=nztmp2:fnz)
        !    symv(ii,jj,kk,:) = v_rec(ii-nxtmp1,jj-nytmp1,kk-nztmp1,:)
        !end forall
        
        !symvtmp = reshape(symv,(/ng2,nblk/))
        !v_rectm = symvtmp
        !symv    = 0
    else
        allocate(v_rectm(nva,nblk))
        v_rectm = reshape(v_rec_c,(/ng2,nblk/))
        allocate(symvtmp(ng2,nblk))
        symvtmp = 0.0D0
    end if
    
    allocate(gd1(size(symrec,1),size(symrec,2)))
    allocate(gd_tmp(size(gd,1),size(gd1,2)))
    allocate(nvr(1,3))
    nvr(1,:) = dble(nv2(:))
    allocate(gd_tmp2(size(gd,1),size(gd1,2)))
    allocate(phase0(nva,1))
    allocate(ttmp(3,1))
    !allocate(v_rectm2(size(v_rectm),1))
    allocate(tmpt(nva,2))
    allocate(inde(nva,1))
    do ii = 1,nsym,1
        gd1(:,:) = symrec(:,:,ii)    
        gd_tmp   = matmul(gd,gd1)
        call bsxfunMod(gd_tmp,nvr,gd_tmp2)
        tmp(1,1) = 1
        tmp(2,1) = nv2(1)
        tmp(3,1) = nv2(1)*nv2(2)
        inde = matmul(gd_tmp2,tmp)+1
        !write(*,*) "loop ii in symmetrize_2D, ii =", ii
        if (any(symt(ii,:) /= 0)) then
            !write(*,*) "loop ii in symmetrize_2D, ii =", ii, "/=0"
            ttmp(:,1) = symt(ii,:)
            phase0 = exp(dcmplx(2.0D0*dcmplx(pi))*imnu*dcmplx(matmul(fftfreq,ttmp)))
            call bsxfunTimes_CMPLX(v_rectm,phase0,tmpt)
            do jj = 1,size(inde,1)
                symvtmp(jj,:)  = symvtmp(jj,:) + tmpt(inde(jj,1),:)
            end do
        else
            !write(*,*) "loop ii in symmetrize_2D, ii =", ii, "==0"
            do jj = 1,size(inde,1)
                symvtmp(jj,:)  = symvtmp(jj,:) + v_rectm(inde(jj,1),:)
            end do
        end if
    end do
    !write(*,*) "I'm here after the loop in symmetrize_2D."
    !write(*,*) "nv2 = ", nv2
    symvtmp  = symvtmp/dble(nsym)
    !write(*,*) "size of symvtmp"
    !write(*,*)  size(symvtmp,1),size(symvtmp,2), nblk

    allocate(symvt(nv2(1),nv2(2),nv2(3),nblk))
    !write(*,*) "I'm here after allocate symvt in symmetrize_2D."
    symvt = reshape(symvtmp,(/nv2(1),nv2(2),nv2(3),nblk/))
    !write(*,*) "I'm here after reshape symvt in symmetrize_2D."
    
    if (.not. all(nv2 == nvec)) then
        !allocate(symvt(nv2(1),nv2(2),nv2(3),nblk))
        !symvt  = 0
        !nxtmp1 = cnx/2+1
        !nytmp1 = cny/2+1
        !nztmp1 = cnz/2+1
        !forall(ii=1:nxtmp,jj=1:nytmp,kk=1:nztmp)
        !    symvt(ii,jj,kk,:) = v_rec(ii,jj,kk,:)
        !end forall
        !nxtmp2 = fnx-(cnx-cnx/2-1)
        !nytmp2 = fny-(cny-cny/2-1)
        !nztmp2 = fnz-(cnz-cnz/2-1)
        !forall(ii=nxtmp:fnx,jj=nytmp:fny,kk=nztmp:fnz)
        !    symvt(ii,jj,kk,:) = v_rec(ii-nxtmp1,jj-nytmp1,kk-nztmp1,:)
        !end forall
    end if
    !allocate(symvtr(nv2(1),nv2(2),nv2(3),nblk))
    !write(*,*) "I'm here after allocate symvtr."
    !allocate(symtmpt(nv2(1),nv2(2),nv2(3),nblk))
    !symvtr = cmplx(symvt)
    do ii = 1,nblk,1
        FRC%Arr = symvt(:,:,:,ii)
        call ifft3_fftw(FRC)
        symvt(:,:,:,ii) = FRC%arr
        !vrecp1 => symvt(:,:,:,ii)
        !call ifft3_fftw_pointer3D(FRC%planifft,vrecp1,vrecp1)
        !vrec_tmp = symvt(:,:,:,ii)
        !call ifft3_fftw(nvec(1),nvec(2),nvec(3),vrec_tmp)
        !call ifft3_fftw(FRC,vrec_tmp)
        !symvtr(:,:,:,ii) = vrec_tmp
    end do
    !call ifft4(symvt,3,symvtr)
    !call ifft4(symvtr,2,symvt)
    !call ifft4(symvt,1,symvtr)
    !write(*,*) "I'm here after ifft4 symvtr."
    symvt = cshift(symvt,1,1)
    symvt = cshift(symvt,1,2)
    symvt = cshift(symvt,1,3)
    !write(*,*) "I'm here after cshift symvtr."
    
    vector = dble(reshape(symvt,(/ngrid,nblk/)))
    
    return
    end subroutine symmetrize_2D
    end module symmetrize_2D_module
