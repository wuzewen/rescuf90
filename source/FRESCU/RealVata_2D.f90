! RealVata_2D.f90
    
!*****************************************************************
    !
    ! This module is used to define a type of data.
    ! The data is 2 dimentional allocatable real array.
    !
!*****************************************************************
    
    module RealVata_2D_module
    
    type :: RealVata_2D
        real*8, allocatable :: vata(:,:)
    end type
    
    end module RealVata_2D_module
