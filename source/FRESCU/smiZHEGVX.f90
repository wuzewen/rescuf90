! smiZHEGVX.f90

!****************************************************
      !
      !
      !
!**********************************************************

      module smiZHEGVXMOD
      contains 
      subroutine smiZHEGVX(jobz,irange,,UPLO,N,A,B,MB,NB,nprow,npcol,&
                     VL,VU,IL,IU,ABSTOL,ORFAC,LRWORK,W,Z)

      implicit none

      integer :: N, nprocs, MB, NB, nprow, npcol
      character(len=1) :: jobz
      complex*16, allocatable :: W(), Z(), A(), B()

      integer :: IBTYPE, M, NZ, IL, IU, LWORK, LRWORK, LIWORK, INFO
      character(len=1) :: irange, UPLO
      real*8 :: VL, VU, ABSTOL, ORFAC
      integer, allocatable :: IFAIL(), ICLUSTR()
      complex*16, allocatable :: WORK(), RWORK(), GAP()

      integer :: iam, ictxt, myrow, mycol

      IBTYPE =  1
      irange = 'A'
      UPLO   = 'U'
      BASTOL = -1.0D0
      ORFAC  =  0.000001
      LWORK  = -1
      LRWORK = -1
      LIWORK = -1
      iam    =  0
      ictxt  =  0
      myrow  =  0
      mycol  =  0

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,nprow,npcol,myrow,mycol)
      call pdlanch(ictxt,UPLO,ABSTOL)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,B_np)
      call numroc(N,MB,myrow,izero,nprow,Z_np)
      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,info)
      call descinit(descB,N,N,MB,NB,izero,izero,ictxt,B_np,info)
      call descinit(descZ,N,N,MB,NB,izero,izero,ictxt,Z_np,info)

      allocate(WORK(1))
      ALLOCATE(RWORK(1))
      allocate(IWORK(1))
      call PZHEGVX(INTYPE,jobz,irange,UPLO,N,A,ione,ione,descA,&
              B,ione,ione,descB,VL,VU,IL,IU,ABSTOL,M,NZ,W,ORFAC,&
              Z,ione,ione,descZ,WORK,LWORK,RWORK,LRWORK,IWORK,&
              LIWORK,IFAIL,ICLUSTR,GAP,INFO)

      LWORK = WORK(1)
      deallocate(WORK)
      allocate(WORK(LWORK))
      LIWORK = IWORK(1)
      deallocate(IWORK)
      allocate(IWORK(LIWORK))
      LRWORK = 1.5*RWORK(1)*RWORK(1)/(RWORK(1)+1024)+1024
      deallocate(RWORK)
      allocate(RWORK(LRWORK))

      call PZHEGVX(INTYPE,jobz,irange,UPLO,N,A,ione,ione,descA,&
              B,ione,ione,descB,VL,VU,IL,IU,ABSTOL,M,NZ,W,ORFAC,&
              Z,ione,ione,descZ,WORK,LWORK,RWORK,LRWORK,IWORK,&
              LIWORK,IFAIL,ICLUSTR,GAP,INFO)
      if (INFO .NE. 0) then
              write(*,*) 'Error'
      end if
      deallocate(WORK,IWORK,RWORK)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiZHEGVX
      end module smiZHEGVXMOD
