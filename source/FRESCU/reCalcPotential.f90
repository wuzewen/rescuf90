! reCalcPotential.f90

!***************************************************
      !
      !
      !
!*****************************************************

      module reCalcPotentialMod
      contains
      subroutine reCalcPotential(FRC,potlist)

      implicit none

      mpistat    = FRC%mpi%status
      mpirank    = FRC%mpi%rank
      outfile    = FRC%info%outfile
      shortrange = FRC%option%shortrange
      iter       = FRC%scloop
      rho        = FRC%rho%output(2)

      charlist(1)%vata = 'vna'
      charlist(2)%vata = 'vps'
      call ismemberChar(potlist,charlist,logi)
      if (any(logi) .AND. &
             (.not. allocated(FRC%potential%vna%vata))) then
          if (mpirank .EQ. 0) then
                  write(*,*) 'Generating VNA'
          end if
          call GetNeutralAtomPotential(FRC)
          if (.NOT. shortrange) then
              vmean = sum(FRC%potential%vna%vata)
              if (mpistat) then
                  call MPI_Allreduce_sum(vmean)
              end if
          end if
          if (mpirank .EQ. 0) then
              write(*,*) 'time'
          end if
      end if

      charlist(1)%vata = 'vatom'
      charlist(2)%vata = 'vdh'
      charlist(3)%vata = 'vps'
      call ismemberCha(potlist,charlist,logi)
      if (any(logi) .AND. (.NOT. allocated(FRC%potential%vatom%vata))) then
          if (.NOT. allocated(FRC%rho%atom%vata)) then
              if (mpirank .EQ. 0) then
                  write(*,*) 'Generating VATOM'
              end if
              call GetNeutralAtomDensity(FRC,.FALSE.)
              if (mpirank .EQ. 0) then
                  write(*,*) 'time'
              end if
          end if

          if (mpirank .EQ. 0) then
              write(*,*) 'generating VNA'
          end if

          call GetHartreePotential(FRC,FRC%rho%atom,FRC%potential%vatom)
          if (mpirank .EQ. 0) then
              write(*,*) 'time'
          end if
      end if

      charlist(1)%vata = 'vps'
      call ismemberChar(potlist,charlist,logi)
      if (any(logi)) then
          if (mpirank .EQ. 0) then
              write(*,*) 'Generating Vloc'
          end if
          if (shortrange) then
              FRC%potential%vps = FRC%potential%vna
          else
              call distmat_feval_minus(FRC,FRC%potential%vna,FRC%potential%vatom)
          end if
          if (mpirank .EQ. 0) then
              write(*,*) 'time'
          end if
      end if

      charlist(1)%vata = 'vh'
      charlist(2)%vata = 'vdh'
      charlist(3)%vata = 'veff'
      call ismemberChar(potlist,charlist,logi)
      if (any(logi)) then
          if (mpirank .EQ. 0) then
              write(*,*) 'Generating VH'
          end if
          call GetHartreePotential(FRC,rho,FRC%potential%VH)
          if (mpirank .EQ. 0) then
              write(*,*) 'Time'
          end if
      end if

      charlist(1)%vata = 'vdh'
      charlist(2)%vata = 'veff'
      call ismemberChar(potlist,charlist,logi)
      if (any(logi)) then
          if (mpirank .EQ. 0) then
              write(*,*) 'Generating VDH'
          end if
          call distmat_bsxfun_minus(FRC,FRC%potential%vh,potential%vatom)
          if (mpirank .EQ. 0) then
              write(*,*) 'time'
          end if
      end if

      charlist(1)%vata = 'vxc'
      charlist(2)%vata = 'veff'
      call ismemberChar(potlist,charlist,logi)
      if (any(logi)) then
          if (.NOT. allocated(FRC%rho%pc)) then
              if (mpirank .EQ. 0) then
                  write(*,*) 'generating partial core density'
              end if
              call GetParitalCoreDensity(FRC)
              if (mpirank .EQ. 0) then
                  write(*,*) 'time'
              end if
          end if

          if (mpirank .EQ. 0) then
              write(*,*) 'Generating XC'
          end if
          call xcMain(FRC)
          FRC%potential%vxc = FRC%potential%VXCout(2)
          if (mpirank .EQ. 0) then
              write(*,*) 'time'
          end if
      end if

      charlist(1)%vata = 'veff'
      call ismemberChar(potlist,charlist,logi)
      if (angy(logi)) then
          if (mpirank .EQ. 0) then
              write(*,*) 'generating Veff'
          end if
          veff = FRC%potential%vxc
          if (shortrange) then
              call UpdateVeff(FRC,veff,FRC%potential%vdh)
              call UpdateVeff(FRC,veff,FRC%potential%vna)
          else
              call UpdateVeff(FRC,veff,FRC%potential%vh)
              call UpdateVeff(FRC,veff,FRC%potential%vps)
          end if
          FRC%potential%veff = veff
          if (mpirank .EQ. 0) THEN
              write(*,*) 'time'
          end if
      end if

      return
      end subroutine recalcPotential
      end module reCalcPotentialMod
      if ()

