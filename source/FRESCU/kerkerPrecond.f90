! kerkerPrecond.f90

!**************************************
      !
      !
      !
!*************************************

      module kerkerPrecondMod
      contains
      subroutine kerkerPrecond(FRC,rho,bc)

      use 

      implicit none

      avec = FRC%domain%latvec
      J2   = matmul(avec,transpose(avec))
      call triu(J2,J2tmp)
      call tril(J2,-1,J2)
      J2 = j2tmp+transpose(j2)
      szrho = size(rho)
      call isreal(rho,rhrho)
      k0    = FRC%mixing%kerkerNorm
      bmin  = FRC%mixing%betamin

      do ii = 1,3,1
          call GenFreq(szrho(ii),bc(ii),k(ii)%vata)
          call circshift((/1,2,3/),(/0,ii-1/),tk)
          call permute(k(ii)%vata,tk)
      end do
      do ii = 1,3,1
      if (bc(ii) .EQ. 0) then
              call dst(rho,ii)
      else if (bc(ii) .EQ. 1) then
              call fft(rho,ii)
      else if (bc(ii) .EQ. 2) then
              call dct(rho,ii)
      end if

      allocate(k2(size(rho,1),size(rho,2),size(rho,3)))
      do ii = 1,3,1
      do jj = ii,3,1
      if (J2(ii,jj)) then
              call bsxfunTimes(k(jj)%vata,k(ii)%vata,k2tmp)
              call bsxfunMinus(k2,ketmp,k2)
      end if
      end do
      end do

      k2 = k2 / (k2 + k0**2)
      forall(ii=1:,jj=1:,kk=1:)
              k2(ii,jj,kk) = min(k2(ii,jj,kk),bmin)
      end forall
      rho = rho*k2

      do ii = 1,3,1
      if (bc(ii) .EQ. 0) then
              call idst(rho,ii)
      else if (bc(ii) .EQ. 1) then
              call ifft(rho,ii)
      else if (bc(ii) .EQ. 2) then
              call idct(rho,ii)
      end if

      return
      end subroutine kerkerPrecond
      end module kerkerPrecondMod
