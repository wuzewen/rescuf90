!  GetNonLocalPotentialK.f90 
!
!  FUNCTIONS:
!  GetNonLocalPotentialK - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: GetNonLocalPotentialK
!
!  PURPOSE: This function is used to get Non-local Potential.
!
!****************************************************************************

    module GetNonLocalPotentialK_Sparse_module
    contains
    subroutine GetNonLocalPotentialK_Sparse(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use RealVata_2D_module
    use getKBOrbitalinfo_module
    use cellInRange_module
    use flipud_module
    use interprDist2Cart_module
    use interprDist2Cart_Sparse_module
    use InterpSph2Cart_Sparse_module
    use ismember_module
    use ismemberRow_module
    use cellfunnnz_module
    use cellfunnnz2_module
    use cat_module
    use reduceoper_module
    use SparseArray_2D_Real8_module
    use SparseVataMN_2D_Real8_module
    use getgridpointcoord_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical :: mpistat, useCell, kbsparseLogical
    integer :: mpirank, mpisize, fgridn(3), fn, natom, nproj, ntv, ntv2, kk, elem!, gg, bb
    integer :: lcut, ncut, ntmp, nlnu, ii, jj, itmp, ll, nindtmp, pp, nevec, ntmpxxx!, zz, ww
    integer :: spnnz, typeE, nkbcell, nnzLoc, qq, ntmp1, ntmp2!, ntmp3, ntmp4, ntmp5!,nnz
    integer :: mtmp1, mtmp2
    integer :: tt, nnn, bc(3)
    real*8  :: avec(3,3), pos(1,3), center(1,3), rad
    real*8  :: rsph, tmp

    real*8, allocatable   :: evec(:), rorb(:), oorb(:), xyz(:,:), tvectmp(:,:)
    real*8, allocatable   :: KBEnergy(:), tvtmp(:,:) ! rrdata(:), vvdata(:)
    real*8, allocatable   :: avectmp(:,:)!, vatatmp1(:,:), vatatmp2(:,:), 
    real*8, allocatable   :: tvec(:,:)!, out3(:)
    integer, allocatable  :: indep(:,:)
    integer, allocatable  :: lnu(:), nnu(:), inde(:), indtmp(:), indLoc(:)
    integer, allocatable  :: indLoctmp(:), tvecp(:,:)
    logical, allocatable  :: isLoc(:), isLoctmp(:)
    real*8 , allocatable  :: GridCoord(:,:)
    !type(RealVata_2D), allocatable :: kbcell(:,:), kbcelltmp(:)
    !type(RealVata_2D)              :: kbcell2!, kbcell3
    !real*8, allocatable :: kbtmp(:,:)
    !type(VnlType), allocatable   :: dataVnl(:)
    !type(RealVata_2D), allocatable :: kbtmp(:,:)
    
    
    
    
    !type(SparseArray_2D_Real8), allocatable :: ftout(:), kbsparse(:,:)
    type(SparseVataMN_2D_Real8), allocatable :: ftout(:), kbsparse(:,:), kbtmp(:)
    type(SparseVataMN_2D_Real8) :: kbsub
    
    
    ! output variables

    ! Body of GetNonLocalPotentialK
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    
    call GetKBOrbitalInfo(FRC)
    fgridn  = FRC%domain%fgridn
    avec    = FRC%domain%latvec
    nproj   = size(FRC%potential%vnl%evec)
    allocate(evec(nproj),rorb(nproj),oorb(nproj))
    evec    = FRC%potential%vnl%evec
    nevec   = size(evec)
    rorb    = FRC%potential%vnl%Rorb
    oorb    = FRC%potential%vnl%Oorb
    natom   = FRC%atom%numberOfAtom
    fn      = product(fgridn)
    kbsparseLogical = .FALSE.
    
    allocate(KBEnergy(nproj))
    KBEnergy = 0.0D0
    
    if (any(FRC%kpoint%kdirect /= 0) .or. trim(FRC%info%calculationType) == trim("dfpt")) then
        useCell = .TRUE.
    else
        useCell = .FALSE.
    end if
    
    rad  = maxval(rorb)
    allocate(xyz(natom,3))
    xyz  = FRC%atom%XYZ
    !allocate(tvec(ntv))
    allocate(avectmp(3,3))
    avectmp = avec
    call cellInRange(avectmp,xyz,rad,.FALSE.,tvec)
    
    ntv  = size(tvec,1)
    ntv2 = size(tvec,2)
    !allocate(tvectmp(ntv,ntv2))
    call flipud(tvec,tvectmp)
    tvec = -tvectmp
    !FRC%option%initParaReal = .TRUE.
    FRC%option%initParaReal = .TRUE.
    if (FRC%option%initParaReal) then
        !write(*,*) "Getting Nonlocal Potential with ParaReal."
        !write(*,*) "Error in GetNonLocalPotential.f90. FRC%option%initParaReal = FALSE."
        !stop
        
        call circum_sphere(avec,rsph)
        !write(*,*) "rsph =", rsph
        !center = 0.5*matmul((/1,1,1/),avec)
        center(1,1) = 0.5D0*sum(avec(:,1))
        center(1,2) = 0.5D0*sum(avec(:,2))
        center(1,3) = 0.5D0*sum(avec(:,3))
        !write(*,*) "center ="
        !write(*,*)  center
        !allocate(GridCoord(fn,3))
        !%%%%%%%% this is an easy way to get
        mpisize = 1
        !%%%%%%%% end
        ntmp = ceiling(dble(fn)/dble(mpisize))
        FRC%mpi%rank = 0
        call GetGridPointCoord(FRC,fgridn,ntmp,GridCoord,indep)
        FRC%mpi%rank = mpirank
        call InitDistArray_2D_Sparse(fn,nproj,ntmp,1,mpisize,1,kbsub)
        allocate(FRC%potential%vnl%sparsekbcell(ntv))
        allocate(kbsparse(natom,1))
        do tt = 1,ntv,1
            call InitDistArray_2D_Sparse(fn,nproj,ntmp,1,mpisize,1,FRC%potential%vnl%sparsekbcell(tt))
            !allocate(kbsparse(natom,1))
            do ii = 1,natom,1
                pos(1,:)  = tvec(tt,:)
                pos       = matmul(pos,avec)
                pos(1,:)  = FRC%atom%XYZ(ii,:) + pos(1,:)
                !write(*,*) "pos =", pos
                elem      = FRC%atom%element(ii)
                nlnu      = size(FRC%element(elem)%vnllnu)
                allocate(lnu(nlnu),nnu(nlnu))
                lnu       = FRC%element(elem)%vnllnu
                lcut      = FRC%element(elem)%vnlLcutoff
                nnu       = FRC%element(elem)%vnlnnu
                ncut      = FRC%element(elem)%vnlNcutoff
                
                ntmp = 0
                do jj = 1,nlnu,1
                    if (lnu(jj) <= lcut .and. nnu(jj) <= ncut) then
                        ntmp = ntmp + 1
                    end if
                end do
                
                allocate(inde(ntmp))
                ntmp = 0
                do jj = 1,nlnu,1
                    if (lnu(jj) <= lcut .and. nnu(jj) <= ncut) then
                        ntmp       = ntmp + 1
                        inde(ntmp) = jj
                    end if
                end do
                allocate(kbtmp(ntmp))
                do jj = 1,ntmp,1
                    itmp = inde(jj)
                    nindtmp = 0
                    do pp = 1,nevec,1
                        if (evec(pp) == ii .and. oorb(pp) == itmp) then
                                nindtmp = nindtmp + 1
                        end if
                    end do
                    allocate(indtmp(nindtmp))
                    nindtmp = 0
                    do pp = 1,nevec,1
                        if (evec(pp) == ii .and. oorb(pp) == itmp) then
                                nindtmp         = nindtmp + 1
                                indtmp(nindtmp) = pp
                        end if
                    end do
                    do pp = 1,nindtmp
                        ntmp1           = indtmp(pp)
                        KBenergy(ntmp1) = FRC%ElementData(elem)%Vnl(itmp)%KBenergy
                    end do

                    !write(*,*) 'indtmp ='
                    !write(*,*)  indtmp
                    !call norm(pos-center,tmp)
                    tmp = sqrt(sum((pos-center)*(pos-center)))
                    nnn = FRC%SOB(elem)%Vnlrr
                    !write(*,*) "tmp =", tmp
                    !write(*,*) 'nnn =', nnn
                    if (tmp < rsph+FRC%ElementData(elem)%Vnl(jj)%rrData(nnn)) then
                        ll = FRC%ElementData(elem)%Vnl(jj)%L
                        bc = 0
                        call InterpSph2Cart_Sparse(FRC%ElementData(elem)%Vnl(jj)%rrData, &
                                                   FRC%ElementData(elem)%Vnl(jj)%vvData, & 
                                                   pos,ll,GridCoord,avec,bc,indtmp,kbtmp(jj))
                    else
                        allocate(kbtmp(jj)%Index1(0),kbtmp(jj)%Index2(0),kbtmp(jj)%ValueN(0))
                        kbtmp(jj)%N_nonzero = 0
                        kbtmp(jj)%SIZE1     = size(GridCoord,1)
                        kbtmp(jj)%SIZE2     = size(indtmp)
                    end if
                    !write(*,*) "kbtmp ="
                    !write(*,*) "number =", kbtmp(jj)%N_nonzero, kbtmp(jj)%SIZE1, kbtmp(jj)%SIZE2
                    !do pp = 1,kbtmp(jj)%N_nonzero,1
                    !write(*,*) kbtmp(jj)%Index1(pp), kbtmp(jj)%Index2(pp),kbtmp(jj)%ValueN(pp)
                    !end do
                    deallocate(indtmp)
                end do

                ntmp1 = 0
                do jj = 1,ntmp,1
                    ntmp1 = kbtmp(jj)%N_nonzero+ntmp1
                end do
                allocate(kbsparse(ii,1)%Index1(ntmp1))
                allocate(kbsparse(ii,1)%Index2(ntmp1))
                allocate(kbsparse(ii,1)%ValueN(ntmp1))
                kbsparse(ii,1)%N_nonzero = ntmp1
                kbsparse(ii,1)%Index1    = size(GridCoord,1)
                kbsparse(ii,1)%Index1    = size(indtmp)
                mtmp1 = 0
                mtmp2 = 0
                do jj = 1,ntmp,1
                    mtmp1 = mtmp2 + 1
                    ntmp1 = kbtmp(jj)%N_nonzero
                    mtmp2 = mtmp2+ntmp1
                    kbsparse(ii,1)%Index1(mtmp1:mtmp2) = kbtmp(jj)%Index1
                    kbsparse(ii,1)%Index2(mtmp1:mtmp2) = kbtmp(jj)%Index2
                    kbsparse(ii,1)%ValueN(mtmp1:mtmp2) = kbtmp(jj)%ValueN
                    deallocate(kbtmp(jj)%Index1,kbtmp(jj)%Index2,kbtmp(jj)%ValueN)
                end do
                deallocate(kbtmp,lnu,nnu,inde)
            end do

            ntmp1 = 0
            do ii = 1,natom,1
                    ntmp1 = ntmp1 + kbsparse(ii,1)%N_nonzero
            end do
                
            allocate(FRC%potential%vnl%sparsekbcell(tt)%Index1(ntmp1))
            allocate(FRC%potential%vnl%sparsekbcell(tt)%Index2(ntmp1))
            allocate(FRC%potential%vnl%sparsekbcell(tt)%ValueN(ntmp1))

            FRC%potential%vnl%sparsekbcell(tt)%N_nonzero = ntmp1
            FRC%potential%vnl%sparsekbcell(tt)%SIZE1     = size(GridCoord,1)
            FRC%potential%vnl%sparsekbcell(tt)%SIZE2     = nproj

            mtmp1 = 0
            mtmp2 = 0
            do ii = 1,natom,1
                !do jj = 1,ntmp,1
                    mtmp1 = mtmp2 + 1
                    ntmp1 = kbsparse(ii,1)%N_nonzero
                    mtmp2 = mtmp2+ntmp1

                    FRC%potential%vnl%sparsekbcell(tt)%Index1(mtmp1:mtmp2) = kbsparse(ii,1)%Index1
                    FRC%potential%vnl%sparsekbcell(tt)%Index2(mtmp1:mtmp2) = kbsparse(ii,1)%Index2
                    FRC%potential%vnl%sparsekbcell(tt)%ValueN(mtmp1:mtmp2) = kbsparse(ii,1)%ValueN
                !end do
                    deallocate(kbsparse(ii,1)%Index1,kbsparse(ii,1)%Index2,kbsparse(ii,1)%ValueN)
            end do
            !deallocate(kbsparse)
        end do
        !write(*,*) 'sparsekbcell ='
        !do ii = 1,FRC%potential%vnl%sparsekbcell(1)%N_nonzero,1
        !write(*,*)  FRC%potential%vnl%sparsekbcell(1)%Index1(ii), FRC%potential%vnl%sparsekbcell(1)%Index2(ii), FRC%potential%vnl%sparsekbcell(1)%ValueN(ii)
        !end do


        !            end if
        !            KBEnergy(kbind) = dataV(itmp)%KBEnergy
        !        end do
        !    end do
        !    
        !    call adjustSparsity(kbtmp%dataV,0.2,kbtmp%dataV)
        !    kbcell(tt) = kbtmp
        !end do
        
        !if (.not. useCell) then
        !    call cellFun(nnz())
        !    
        !    
        !    call adjustSparsity(kbcell(1)%dataV,0.2,kbcell(1)%dataV)
        !end if
        !
        !if (mpistat) then
        !    
        !    write(*,*) "Error in GetNonLocalPotentialK.f90. MPI is not avalable now."
        !    stop
        !    
        !    !do tt = 1,NumKbcell,1
        !    !    call distmat_allgather(FRC,kbcell(tt),kbcell(tt)%dataV)
        !    !    call adjustSparsity(kbcell(tt)%dataV,0.2,kbcell(tt)%dataV)
        !    !end do
        !end if
        !
    else
        !call repmat(ntv,1,kbcell)
        ntmp = 0
        do ii = 1,natom,1
            typeE = FRC%atom%element(ii)
            do jj = 1,FRC%SOB(typeE)%Vnl
                ntmp = ntmp +1
            end do
        end do
        allocate(kbsparse(ntv,ntmp))
        nkbcell = ntv
        
        kk = 1
        do ii = 1,natom,1
            pos(1,:)  = FRC%atom%XYZ(ii,:)
            elem      = FRC%atom%element(ii)
            nlnu      = size(FRC%element(elem)%vnllnu)
            allocate(lnu(nlnu),nnu(nlnu))
            lnu       = FRC%element(elem)%vnllnu
            lcut      = FRC%element(elem)%vnlLcutoff
            nnu       = FRC%element(elem)%vnlnnu
            ncut      = FRC%element(elem)%vnlNcutoff
            
            ntmp = 0
            do jj = 1,nlnu,1
                if (lnu(jj)<=lcut .and. nnu(jj)<=ncut) then
                    ntmp = ntmp+1
                end if
            end do
            allocate(inde(ntmp))
            ntmp = 0
            do jj = 1,nlnu,1
                if (lnu(jj)<=lcut .and. nnu(jj)<=ncut) then
                    ntmp       = ntmp+1
                    inde(ntmp) = jj
                end if
            end do
            
            !allocate(dataVnl(FRC%SOB(elem)%SizeOfVnl))
            !dataVnl(:) = FRC%ElementData(elem)%Vnl(:)
            
            do jj = 1,ntmp,1
                itmp    = inde(jj)
                !ll      = dataVnl(itmp)%L
                ll      = FRC%ElementData(elem)%Vnl(itmp)%L
                nindtmp = 0
                do pp = 1,nevec,1
                    if (evec(pp) == ii .and. oorb(pp) == itmp) then
                        nindtmp = nindtmp + 1
                    end if
                end do
                
                allocate(indtmp(nindtmp))
                nindtmp = 0
                do pp = 1,nevec,1
                    if (evec(pp) == ii .and. oorb(pp) == itmp) then
                        nindtmp         = nindtmp + 1
                        indtmp(nindtmp) = pp
                    end if
                end do
                
                do pp = 1,nindtmp
                    ntmp           = indtmp(pp)
                    KBenergy(ntmp) = FRC%ElementData(elem)%Vnl(itmp)%KBenergy
                end do
                 
                ntmp = mod(ii-1,mpisize)
                if (ntmp < 0) then
                     ntmp = ntmp + mpisize
                end if   
                if (ntmp == mpirank) then
                    call InterpRdist2Cart_Sparse(FRC%ElementData(elem)%Vnl(jj)%rrData,&
                                                 FRC%ElementData(elem)%Vnl(jj)%vvData,&
                                                 pos,ll,avec,fgridn,.TRUE.,ftout,tvtmp)
                    call ismemberRow(tvec,tvtmp,isLoc,indLoctmp)
                    call ismemberRow(tvtmp,tvec,isLoctmp,indLoc)
                    
                    ntmpxxx = size(indLoc)
                    if (any(isLoc .EQV. .TRUE.)) then
                        do pp = 1,ntmpxxx,1
                            qq = indLoc(pp)
                            allocate(kbsparse(qq,kk)%Index1(ftout(pp)%N_nonzero))
                            allocate(kbsparse(qq,kk)%Index2(ftout(pp)%N_nonzero))
                            allocate(kbsparse(qq,kk)%ValueN(ftout(pp)%N_nonzero))
                            kbsparse(qq,kk) = ftout(pp)
                        end do
                    end if
                    ! edited on 10th August
                    ntmpxxx = size(isLoc)
                    do pp = 1,ntmpxxx,1
                        if (.not. isLoc(pp)) then
                                kbsparse(pp,kk)%N_nonzero = 0
                                kbsparse(pp,kk)%SIZE1     = fn
                                kbsparse(pp,kk)%SIZE2     = 2*ll+1
                                allocate(kbsparse(pp,kk)%Index1(0))
                                allocate(kbsparse(pp,kk)%Index2(0))
                                allocate(kbsparse(pp,kk)%ValueN(0))
                        end if
                    end do
                    ! end edit
                    deallocate(indLoc,isLoctmp,indLoctmp,isLoc,tvtmp)
                    do pp = 1,size(ftout),1
                        deallocate(ftout(pp)%Index1,ftout(pp)%Index2,ftout(pp)%ValueN)
                    end do
                    deallocate(ftout)
                end if
                kk = kk +1
                deallocate(indtmp)
            end do
            deallocate(lnu,nnu,inde)!,dataVnl)
        end do
        
        allocate(FRC%potential%vnl%sparsekbcell(nkbcell))
        do ii = 1,nkbcell,1

            !write(*,*) "nkbcell =", nkbcell
            !write(*,*) "loop, "   , ii
            ! faster method
            ntmp2 = 0
            ntmp1 = 0
            do jj = 1,kk-1,1
                ntmp2 = ntmp2+kbsparse(ii,jj)%N_nonzero
                ntmp1 = ntmp1+kbsparse(ii,jj)%SIZE2
            end do
            !write(*,*) "FRC%potential%vnl%sparsekbcell(ii)%N_nonzero", ntmp2
            FRC%potential%vnl%sparsekbcell(ii)%N_nonzero = ntmp2
            FRC%potential%vnl%sparsekbcell(ii)%SIZE1     = kbsparse(ii,1)%SIZE1
            FRC%potential%vnl%sparsekbcell(ii)%SIZE2     = ntmp1
            allocate(FRC%potential%vnl%sparsekbcell(ii)%Index1(ntmp2))
            allocate(FRC%potential%vnl%sparsekbcell(ii)%Index2(ntmp2))
            allocate(FRC%potential%vnl%sparsekbcell(ii)%ValueN(ntmp2))

            mtmp1 = 0
            mtmp2 = 0
            do jj = 1,kk-1,1
                mtmp1 = mtmp2+1
                ntmp1 = kbsparse(ii,jj)%N_nonzero
                mtmp2 = mtmp2+ntmp1
                if (jj == 1) then
                        ntmp2 = 0
                else
                        !do qq = 1,jj-1,1
                        ntmp2 = ntmp2+kbsparse(ii,jj-1)%SIZE2
                        !end do
                end if

                FRC%potential%vnl%sparsekbcell(ii)%Index1(mtmp1:mtmp2) = kbsparse(ii,jj)%Index1
                FRC%potential%vnl%sparsekbcell(ii)%Index2(mtmp1:mtmp2) = kbsparse(ii,jj)%Index2+ntmp2
                FRC%potential%vnl%sparsekbcell(ii)%ValueN(mtmp1:mtmp2) = kbsparse(ii,jj)%ValueN


                write(*,*) size(FRC%potential%vnl%sparsekbcell(ii)%Index1)
                !write(*,*) "mtmp1 =", mtmp1, "mtmp2 =", mtmp2
                !write(*,*) "sparsekbsparse",jj
                !write(*,*) "SIZE1 =", kbsparse(ii,jj)%SIZE1
                !write(*,*) "SIZE2 =", kbsparse(ii,jj)%SIZE2
                !write(*,*) "N_Nonzero =", kbsparse(ii,jj)%N_nonzero
                !write(*,*) "Index1 ="
                !write(*,*)  kbsparse(ii,jj)%Index1
                !write(*,*) "Index2 ="
                !write(*,*)  kbsparse(ii,jj)%Index2
                !write(*,*) "ValueN ="
                !write(*,*)  kbsparse(ii,jj)%ValueN


            end do

            !write(*,*) "end of inner loop."
            !write(*,*) "sparsekbcell",ii
            !write(*,*) "SIZE1 =", FRC%potential%vnl%sparsekbcell(ii)%SIZE1
            !write(*,*) "SIZE2 =", FRC%potential%vnl%sparsekbcell(ii)%SIZE2
            !write(*,*) "N_Nonzero =", FRC%potential%vnl%sparsekbcell(ii)%N_nonzero
            !write(*,*) "Index1 ="
            !write(*,*)  FRC%potential%vnl%sparsekbcell(ii)%Index1
            !write(*,*) "Index2 ="   
            !write(*,*)  FRC%potential%vnl%sparsekbcell(ii)%Index2
            !write(*,*) "ValueN ="
            !write(*,*)  FRC%potential%vnl%sparsekbcell(ii)%ValueN
            !write(*,*) "Stop in NonlocalSparse."
            !stop
            !ntmp2 = 0
            !do jj = 1,kk-1,1
            !    ntmp2 = ntmp2 + size(kbcell(ii,jj)%vata,2)
            !end do
            !ntmp1 = size(kbcell(ii,1)%vata,1)
            !allocate(kbcelltmp(ii)%vata(ntmp1,ntmp2))
            !ntmp1 = 0
            !ntmp2 = 0
            !do jj = 1,kk-1,1
            !    ntmp1 = ntmp2+1
            !    ntmp2 = ntmp2+size(kbcell(ii,jj)%vata,2)
            !    kbcelltmp(ii)%vata(:,ntmp1:ntmp2) = kbcell(ii,jj)%vata
            !end do


            !ntmp1 = size(kbcell(ii,1)%vata,1)
            !ntmp2 = size(kbcell(ii,1)%vata,2)
            !allocate(vatatmp1(ntmp1,ntmp2))
            !vatatmp1 = kbcell(ii,1)%vata
            
            !do jj = 1,kk-2,1
            !    ntmp1 = size(kbcell(ii,jj)%vata,1)
            !    ntmp2 = size(kbcell(ii,jj)%vata,2)
            !    ntmp3 = size(kbcell(ii,jj+1)%vata,1)
            !    ntmp4 = size(kbcell(ii,jj+1)%vata,2)
            !    ntmp5 = size(vatatmp1,2)
            !    allocate(vatatmp2(ntmp1,ntmp5+ntmp4))
            !    call cat(2,vatatmp1,kbcell(ii,jj+1)%vata,vatatmp2)
            !    deallocate(vatatmp1)
            !    allocate(vatatmp1(ntmp1,ntmp5+ntmp4))
            !    vatatmp1 = vatatmp2
            !    deallocate(vatatmp2)
            !end do
            !ntmp1 = size(vatatmp1,1)
            !ntmp2 = size(vatatmp1,2)
            !allocate(kbcelltmp(ii)%vata(ntmp1,ntmp2))
            !kbcelltmp(ii)%vata = vatatmp1
            !deallocate(vatatmp1)
            !do jj = 1,kk-2,1
            !    kbcelltmp(ii)%vata(ii,:) = kbcell(ii,jj)%vata
            !end do
            !call cat(2,kbcell(ii,jj)%vata,kbcell(ii,jj+1)%vata,kbcelltmp)    
            !call adjustSparsity(kbcelltmp,0.2,kbcell(ii,1))
        end do

        !write(*,*) "end of outer loop"
        !write(*,*) "MINUS kbsparse"
        !write(*,*)  FRC%potential%vnl%sparsekbcell(2)%Index1-FRC%potential%vnl%sparsekbcell(1)%Index1
        !stop
                !kbcell = kbcell(:,1)
       !
        !ntmp1 = size(tvec,1)
        !ntmp2 = size(tvec,2)
        !tvecp = int(tvec)

        allocate(tvecp(ntmp1,ntmp2))
        tvecp = int(tvec)
        if (.not. useCell) then
                write(*,*) "Error in GetNonLocalPotentialK_Sparse.f90."
                write(*,*) "useCell Should be true."
            !call cellfunnnz(kbcelltmp,spnnz)
            !call cellfunnnz(Acell,spnnz)
            
            !call reduceOper(kbcelltmp,tvecp,(/0,0,0/),spnnz,kbcell2)
            !FRC%potential%vnl%kbcell = kbcell2
        !else
        !    ntmp1 = size(kbcelltmp)
        !    allocate(FRC%potential%vnl%kbcell(ntmp1))
        !    do ii = 1,ntmp1,1
        !        ntmp2 = size(kbcelltmp(ii)%vata,1)
        !        ntmp3 = size(kbcelltmp(ii)%vata,2)
        !        allocate(FRC%potential%vnl%kbcell(ii)%vata(ntmp2,ntmp3))
        !    end do
        !    
        !    FRC%potential%vnl%kbcell = kbcelltmp
            !tvec = 0
            !call adjustSparsity(kbcell(1,1)%vata,0.2,kbcell(1,1))
        end if
        
        if (mpistat) then
            
            write(*,*) "Error in GetNonLocalPotentialK.f90. MPI is not avalable now."
            !stop
            
            !do tt = 1,nkbcell
            !    do ii = 0,mpisize-1,1
            !        nindtmp = 0
            !        do pp = 1,nevec,1
            !           if (mod(evec(pp)-1,mpisize) == ii) then
            !                nindtmp = nindtmp + 1
            !            end if
            !        end do
            !        allocate(indtmp(nindtmp))
            !        nindtmp = 0
            !        do pp = 1,nevec,1
            !            if (mod(evec(pp)-1,mpisize) == ii) then
            !                nindtmp = nindtmp + 1
            !                indtmp(nindtmp) = pp
            !            end if
            !        end do
            !        
            !        if (sum(indtmp) /= 0) then
            !            call adjustSparsity(kbcell(tt)%dataV(:,indtmp),0.2,kbdata)
            !            call MPI_Bcast_variable(kbdata,ii,kbdata)
            !        end if
            !    end do
            !    call adjustSparsity(kbcell(tt)%dataV,0.2,kbcell(tt)%dataV)
            !end do
        end if
    end if
    
    !call cellfunnnz(kbcelltmp,spnnz)
    !call reduceOper(kbcelltmp,tvecp,(/0,0,0/),spnnz,kbcell3)
    !call adjustSparsity(kbtmp%dataV,0.2,kbsparse) !! sparse matrix not used.
    !nnzLoc = nnz(kbtmp%vata)
    !call cellfunnnz(kbtmp,spnnz)
    
    !write(*,*) "Nonlocal ="
    !write(*,*)  FRC%potential%vnl%sparsekbcell(1)%Index1(1)
    !write(*,*)  FRC%potential%vnl%sparsekbcell(1)%Index2(1)
    !write(*,*)  FRC%potential%vnl%sparsekbcell(1)%ValueN(1)
    !call PickInterpVnl(FRC)
    !allocate()
    !FRC%potential%vnl%kbcell = kbcell
    allocate(FRC%potential%vnl%kbvec(size(tvec,1),size(tvec,2)))
    FRC%potential%vnl%kbvec  = tvec
    
    if (kbsparseLogical) then
        FRC%potential%vnl%kbsparse = nnzLoc
    else
        FRC%potential%vnl%kbsparse = -1
    end if
    allocate(FRC%potential%vnl%Aorb(size(evec)))
    FRC%potential%vnl%Aorb     = evec
    allocate(FRC%potential%vnl%KBEnergy(size(KBEnergy)))
    FRC%potential%vnl%KBEnergy = KBEnergy
    
    return
    end subroutine GetNonLocalPotentialK_Sparse
    end module GetNonLocalPotentialK_Sparse_module

