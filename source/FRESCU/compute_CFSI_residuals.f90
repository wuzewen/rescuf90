! compute_CFSI_residuals.f90

!************************************************
      !
      !
      !
!************************************************

      module compute_CFSI_residuals_module
      contains
      subroutine compute_CFSI_residuals(FRC,tol,computeAll,failureFlag,residualNorms)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use ModBCDist_CMPLX_2D_module
      use GetGlobalInd_CMPLX_2D_module
      use GetKBorb_module
      use GetKBorb_Sparse_module
      use UpdateVeff2D_module
      use distmat_allgather_2D_module
      use GenHamFun_module
      use distmat_zeroslike_CC_2D_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      real*8  :: tol
      logical :: computeAll, failureFlag

      ! temporary variables
      logical :: mpistat, iscomplex
      integer :: mpirank, mpisize, iter, ispin, nspin, nkpt
      integer :: xband, ndots, nband, ii, kk, buffs, sizeofs
      integer :: blks, ss, jj, norb, ll, tmp(2), ntmp, dd
      integer :: blocksize
      real*8  :: kpt(3)

      integer, allocatable :: colvec(:), ia(:,:), ja(:,:)

      real*8,  allocatable :: kdir(:,:), lambda(:), ham(:,:)
      real*8,  allocatable :: lambdatmp(:)
      real*8,  allocatable :: RR(:), errtmp(:,:,:)

      complex*16, allocatable :: blockVectorR(:,:), hamu(:,:), hamd(:,:)

      type(VataMN_CMPLX_2D) :: psitmp
      type(VataMN_2D)       :: veff, veffin

      ! output variables
      real*8, allocatable :: residualNorms(:,:,:)

      ! body of this function
      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize

      iter    = FRC%scloop
      ispin   = FRC%spin%ispin
      nspin   = FRC%spin%nspin
      nkpt    = size(FRC%kpoint%ikdirect,1)
      allocate(kdir(nkpt,3))
      kdir    = FRC%kpoint%ikdirect
      nband   = FRC%eigensolver%nband
      xband   = FRC%eigensolver%emptyBand
      ndots   = product(FRC%domain%cgridn)
      veffin%vata(:,1) = FRC%potential%veffin(1)%vata
      call UpdateVeff2D(FRC,veffin,FRC%potential%vps,.FALSE.,veff)
      call distmat_allgather_2D(FRC,veff,FRC%potential%vloc)
      allocate(residualNorms(ndots,nkpt,nspin))

      do ii = 1,nkpt,1
          kpt = kdir(kk,:)
          buffs = FRC%option%buffersize
          if (any(kpt /= 0) .or. ispin == 4) then
                  iscomplex = .TRUE.
                  sizeofs   = 16
          else
                  iscomplex = .FALSE.
                  sizeofs   = 8
          end if

          blks = ceiling(dble(buffs)/dble(ndots)/dble(sizeofs))
          blks = blks*mpisize

          if (FRC%potential%vnlScheme) then
                  if (FRC%potential%vnl%KBorbIsSparse) then
                          call GetKBorb_Sparse(FRC,kpt)
                  else
                          call GetKBorb(FRC,kpt)
                  end if
          end if

          do ss = 1,nspin,1
              lambda = FRC%energy%ksnrg%vata(:,kk,ss)
              !if (ispin == 1 .or. ispin == 2) then
              !        call GenHamFun(FRC,kpt,ss,.FALSE.,X,ham,hamu,hamd)
              !else
              !        call GenHamFun(FRC,kpt,ss,.FALSE.,X,ham,hamu,hamd)
              !end if

              norb = FRC%psi(kk,ss)%n
              forall(jj=1:norb)
                      colvec(jj) = floor((dble(jj)-0.1D-11)/dble(blks))+1
              end forall

              do ll = 1,maxval(colvec),1
                  if (ispin == 1 .or. nspin == 2) then
                          tmp(1) = ndots
                          tmp(2) = count(colvec == ll)
                          call distmat_zeroslike_CC_2D(FRC,FRC%psi(kk,ss),tmp,psitmp)
                          ntmp = 0
                          do dd = 1,size(FRC%psi(kk,ss)%vata,2),1
                              if (colvec(dd) == ll) then
                                      ntmp = ntmp+1
                                      psitmp%vata(:,ntmp) = FRC%psi(kk,ss)%vata(:,dd)
                                      lambdatmp(ntmp) = lambda(dd)
                              end if
                          end do
                          allocate(RR(ntmp))
                          RR = 0.0D0
                          call ModBCDist_CMPLX_2D(FRC,psitmp,1,1,1,mpisize,.FALSE.,.FALSE.,psitmp)
                          call GetGlobalInd_CMPLX_2D(mpirank,psitmp,ia,ja)

                          blocksize = size(psitmp%vata,2)
                          if (blocksize > 0) then
                                  call GenHamFun(FRC,kpt,ss,.FALSE.,psitmp%vata,blockVectorR,hamu,hamd)
                          end if
                  else if (ispin == 4) then
                          write(*,*) "Error in compute_CFSI_residuals."
                  end if

                  errtmp = residualNorms(1:nband-xband+2,:,:)

                  if (any(errtmp > tol) .and. (.not. computeAll)) then
                          failureFlag = .TRUE.
                          return
                  end if 
              end do
          end do
      end do

      errtmp = residualNorms(1:nband-xband+2,:,:)

      if (any(errtmp > tol)) then
              failureFlag = .TRUE.
      else
              failureFlag = .FALSE.
      end if

      return
      end subroutine compute_CFSI_residuals
      end module compute_CFSI_residuals_module
