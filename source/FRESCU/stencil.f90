! stencil.f90
    
!********************************
    !
    !
    !
!********************************
    
    module stencil_module
    contains
    subroutine stencil(order,acc,coeff)
    
    use bsxfunPower_module
    use bsxfunTimes_module
    use leftdivision_module
    
    implicit none
    
    ! input valuables
    integer :: order, acc
    
    ! temporery valuables
    integer             :: n
    integer             :: i, ii
    real*8, allocatable :: a(:,:), b(:,:), c(:,:), tmp1(:,:),tmp2(:,:)
    real*8, allocatable :: coeff1(:,:)
    
    ! output valuables
    real*8, allocatable :: coeff(:)
    
    ! body of this function
    select case(order)
    case(1)
        n = acc/2
        allocate(a(n,1))
        forall(ii=1:n)
            a(ii,1) = dble(ii)
        end forall
        
        allocate(tmp1(1,n))
        allocate(tmp2(n,1))
        tmp1 = transpose(a)
        tmp2 = 2.0D0*a-1.0D0
        
        allocate(b(n,n))
        call bsxfunPower(tmp1,tmp2,b)
        
        b    = 2.0D0*transpose(b)
        
        a(:,:) = 0.0D0
        a(1,1) = 1.0D0
        allocate(coeff1(n,1))
        call leftdivision(b,a,coeff1)
        
        forall(i=1:n)
            coeff(i) = -coeff1(n+1-i,1)
        end forall
        coeff(n+1) = 0.0D0
        forall(i=n+2:2*n+1)
            coeff(i) = coeff1(i-n-1,1)
        end forall
        deallocate(a,b,tmp1,tmp2,coeff1)
    case(2)
        n = acc/2
        allocate(a(n+1,1))
        forall(ii=1:n+1)
            a(ii,1) = ii-1
        end forall
        
        allocate(b(n+1,n+1))
        allocate(tmp1(1,n+1))
        allocate(tmp2(n+1,1))
        tmp1 = transpose(a)
        tmp2 = 2.0D0*a
        call bsxfunPower(tmp1,tmp2,b)
        
        allocate(c(n+1,1))
        c(1,1) = 1.0D0
        forall(ii=2:n+1)
            c(ii,1) = 2.0D0
        end forall
        
        b = transpose(b)
        call bsxfunTimes(b,c,b)
        
        a(:,:) = 0.0D0
        a(2,1) = 2.0D0
        allocate(coeff1(n+1,1))
        call leftDivision(b,a,coeff1)
        
        forall(i=1:n)
            coeff(i) = coeff1(n+2-i,1)
        end forall
        forall(i=n+1:2*n+1)
            coeff(i) = coeff1(i-n,1)
        end forall
        deallocate(a,b,c,tmp1,tmp2,coeff1)
    end select
    
    return
    end subroutine stencil
    end module stencil_module
