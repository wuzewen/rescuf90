! MODBCDIST.f90

      module MODBCDIST_module

              use ModBCDist_1D_real8_module
              use ModBCDist_1D_cmplx_module
              use ModBCDist_2D_real8_module
              use ModBCDist_2D_cmplx_module

              implicit none

              interface MODBCDIST
                      module procedure ModBCDist_1D_real8
                      module procedure ModBCDist_1D_cmplx
                      module procedure ModBCDist_2D_real8
                      module procedure ModBCDist_2D_cmplx
              end interface MODBCDIST

      end module MODBCDIST_module

