! interpRad2prlpd.f90

!*****************************************************************************
      !
      !
      !
!*****************************************************************************

      module interpRad2prlpd_module
      contains
      subroutine interpRad2prlpd(rrdata,vvdata,pos,lqn,avec,nvec,firstCell,fval,glbi1,glbi2,glbi3)

      use inversion_module
      use cellInRange_module
      use ndgridA_Integer_module
      use bsxfunMinus_module
      use interp1_module
      use GenRealSH_module
      use bsxfunTimes_module

      implicit none

      ! input variables
      real*8, allocatable :: rrdata(:), vvdata(:)
      real*8              :: pos(3), avec(3,3)
      integer             :: lqn, nvec(3)
      logical             :: firstCell

      ! temporary variables

      ! output variables
      real*8   :: eps, rmax
      integer  :: inde, nf
      integer  :: ii, ntmp, npoint
      logical  :: issym
      character(len=20) :: method

      real*8 , allocatable :: fval(:,:), frdata(:), fvdata(:), ltmp(:,:)
      real*8 , allocatable :: ltmpt(:,:), post(:,:), txyz(:,:)
      real*8 , allocatable :: fvaltmp(:,:), xtmp(:,:)
      real*8 , allocatable :: dist(:), disttmp(:)
      real*8 , allocatable :: frgrid(:), ffgrid(:,:)
      real*8 , allocatable :: xtmp1(:), xtmp2(:), xtmp3(:)
      real*8 , allocatable :: Ylm(:,:), frgrix(:,:), ffgridtmp(:,:)
      integer, allocatable :: glbi1(:), glbi2(:), glbi3(:)
      integer, allocatable :: txyzInt(:,:)

      ! body of this function
      eps  = 1.0D-14
      inde = 0
      nf   = size(vvdata)
      do ii = 1,nf,1
          if (abs(vvdata(ii)) > eps) then
                  inde = ii
          end if
      end do
      inde = inde + 1
      allocate(frdata(inde))
      allocate(fvdata(inde))
      forall(ii=1:inde)
              frdata(ii) = rrdata(ii)
              fvdata(ii) = vvdata(ii)
      end forall
      if (inde < size(rrdata)) then
              fvdata(inde) = 0.0D0
      end if
      rmax = maxval(frdata)

      !write(*,*) "frdata ="
      !write(*,*)  frdata
      !write(*,*) "fvdata ="
      !write(*,*)  fvdata
      !write(*,*) "rmax   ="
      !write(*,*)  rmax

      allocate(ltmp(3,3),ltmpt(3,3))
      ltmp = 0.0D0
      forall(ii=1:3)
              ltmp(ii,ii) = dble(nvec(ii))
      end forall

      call inversion(ltmp,ltmpt)

      !write(*,*) "ltmpt = "
      !write(*,*)  ltmpt

      !write(*,*) " "
      ltmp = matmul(ltmpt,avec)

      !write(*,*) "ltmp ="
      !write(*,*)  ltmp

      issym = .TRUE.
      allocate(post(1,3))
      post(1,:) = pos(:)
      call cellInRange(ltmp,post,rmax,issym,txyz)

      !write(*,*) "txyz ="
      !write(*,*)  txyz
      
      if (firstCell) then
              write(*,*) "Error in interpRad2prlpd.f90. firstCell NO."
      end if

      ntmp = INT(abs(abs(txyz(2,1))-txyz(1,1)))+1
      allocate(glbi1(ntmp))
      forall(ii=1:ntmp)
              glbi1(ii) = txyz(1,1)+ii-1
      end forall
      ntmp = INT(abs(abs(txyz(2,2))-txyz(1,2)))+1
      allocate(glbi2(ntmp))
      forall(ii=1:ntmp)
              glbi2(ii) = txyz(1,2)+ii-1
      end forall
      ntmp = INT(abs(abs(txyz(2,3))-txyz(1,3)))+1
      allocate(glbi3(ntmp))
      forall(ii=1:ntmp)
              glbi3(ii) = txyz(1,3)+ii-1
      end forall

      !write(*,*) "glbi1 ="
      !write(*,*)  glbi1
      !write(*,*) "glbi2 ="
      !write(*,*)  glbi2
      !write(*,*) "glbi3 ="
      !write(*,*)  glbi3

      allocate(txyzInt(size(glbi1)*size(glbi2)*size(glbi3),3))
      call ndgridA_Integer(glbi1,glbi2,glbi3,txyzInt)
      !write(*,*) "txyzInt ="
      !write(*,*)  txyzInt
      deallocate(txyz)
      allocate(txyz(size(glbi1)*size(glbi2)*size(glbi3),3))
      txyz = matmul(dble(txyzInt),ltmp)

      !write(*,*) "txyz ="
      !write(*,*)  txyz

      npoint = size(txyz,1)

      if (lqn < 0) then
              allocate(fvaltmp(npoint,1))
              allocate(fval(npoint,1))
              fvaltmp = 0.0D0
              fval    = 0.0D0
      else
              allocate(fvaltmp(npoint,2*lqn+1))
              allocate(fval(npoint,2*lqn+1))
              fvaltmp = 0.0D0
              fval    = 0.0D0
      end if

      if (size(glbi1) == 0 .or. size(glbi2) == 0 .or. size(glbi3) == 0) then
              return
      end if

      allocate(xtmp(size(txyz,1),size(txyz,2)))
      call bsxfunMinus(txyz,post,xtmp)
      allocate(dist(npoint))
      dist = xtmp(:,1)**2+xtmp(:,2)**2+xtmp(:,3)**2
      dist = dist**0.5D0

      !write(*,*) "xtmp ="
      !write(*,*)  xtmp
      !write(*,*) "dist ="
      !write(*,*)  dist
      if (minval(dist) <= rmax) then
              ntmp = 0
              do ii = 1,npoint,1
                  if (dist(ii)<=rmax) then
                          ntmp = ntmp+1
                  end if
              end do
              allocate(disttmp(ntmp))
              ntmp = 0
              do ii = 1,npoint,1
                  if (dist(ii)<=rmax) then
                          ntmp          = ntmp+1
                          disttmp(ntmp) = dist(ii)
                  end if
              end do
              allocate(frgrid(ntmp))
              method = "spline"
              call interp1(frdata,fvdata,disttmp,method,0,frgrid)
              if (lqn<0) then
                      ntmp = 0
                      do ii = 1,npoint,1
                      if (dist(ii)<=rmax) then
                              ntmp       = ntmp+1
                              fval(ii,1) = fval(ii,1)+frgrid(ntmp)
                      end if
                      end do
              else
                      allocate(xtmp1(ntmp),xtmp2(ntmp),xtmp3(ntmp))
                      ntmp = 0
                      do ii = 1,npoint,1
                      if (dist(ii)<=rmax) then
                              ntmp        = ntmp+1
                              xtmp1(ntmp) = xtmp(ii,1)
                              xtmp2(ntmp) = xtmp(ii,2)
                              xtmp3(ntmp) = xtmp(ii,3)
                      end if
                      end do
                      call GenRealSH(xtmp1,xtmp2,xtmp3,lqn,1,Ylm)
                      allocate(frgrix(size(frgrid),1),ffgridtmp(size(Ylm,1),size(Ylm,2)))
                      frgrix(:,1) = frgrid(:)
                      call bsxfunTimes(frgrix,Ylm,ffgridtmp)
                      ntmp = 0
                      do ii = 1,npoint,1
                      if (dist(ii)<=rmax) then
                              ntmp       = ntmp+1
                              fval(ii,:) = fval(ii,:)+ffgridtmp(ntmp,:)
                      end if
                      end do
              end if
              !allocate(fval(size(ffgrid,1),size(ffgrid,2)))
              !fval = ffgrid
      !else
              !allocate(fval(0,0))
      end if
      !allocate(fval(size(ffgrid,1),size(ffgrid,2)))
      !fval = ffgrid

      return
      end subroutine interpRad2prlpd
      end module interpRad2prlpd_module

