! fdGradMat.f90
    
    module fdGradMat_module
    contains
    subroutine fdGradMat(n,acc,bc,Gu,Gv,Gw)
    
    use stencil_module
    use galleryCircul_module
    use repmat_module
    use spdiags_module
    
    implicit none
    
    ! input valuables
    integer              :: bc(3)
    integer              :: n(3)
    integer              :: acc
    
    ! temporery valuables
    integer              :: i, npoint, acctmp
    real*8, allocatable  :: stc1(:), stc2(:), stc3(:), stc4(:)
    real*8, allocatable  :: stctmp(:,:)
    
    ! output valuables
    real*8, allocatable  :: Gu(:,:), Gv(:,:), Gw(:,:)
    
    ! body of this function
    acctmp = 2*(minval(n-1)/2)
    acc    = min(acc,acctmp)
    npoint = acc+1
    allocate(stc1(npoint))
    call stencil(1,acc,stc1)
    
    if (bc(1) /= 0) then
        allocate(stc2(n(1)))
        forall(i=1:(npoint+1)/2)
            stc2(i) = stc1(i+(npoint+1)/2-1)
        end forall
        forall(i=(npoint+3)/2:(n(1)-(npoint+1)/2+1))
            stc2(i) = 0.0D0
        end forall
        forall(i=(n(1)-(npoint+1)/2+2):n(1))
            stc2(i) = stc1(i-(n(1)-(npoint+1)/2+1))
        end forall
        call galleryCircul(stc2,Gu)
        deallocate(stc2)
    else
        allocate(stctmp(npoint,n(1)))
        call repMat(stc1,n(1),1,stctmp)
        allocate(stc2(2*((npoint-1)/2)+1))
        forall(i=1:npoint)
            stc2(i) = dble(i-(npoint-1)/2-1)
        end forall
        call spdiags(stctmp,stc2,n(1),n(1),Gu)
        deallocate(stctmp)
    end if
    Gu = Gu*dble(n(1))


    if (bc(2) /= 0) then
        allocate(stc3(n(2)))
        forall(i=1:(npoint+1)/2)
            stc3(i) = stc1(i+(npoint+1)/2-1)
        end forall
        forall(i=(npoint+3)/2:(n(2)-(npoint+1)/2+1))
            stc3(i) = 0.0D0
        end forall
        forall(i=(n(2)-(npoint+1)/2+2):n(2))
            stc3(i) = stc1(i-(n(2)-(npoint+1)/2+1))
        end forall
        call galleryCircul(stc3,Gv)
        deallocate(stc3)
    else
        allocate(stctmp(npoint,n(2)))
        call repMat(stc1,n(2),1,stctmp)
        allocate(stc2(2*((npoint-1)/2)+1))
        forall(i=1:2*((npoint-1)/2)+1)
            stc2(i) = dble(i-(npoint-1)/2-1)
        end forall
        call spdiags(stctmp,stc2,n(2),n(2),Gv)
        deallocate(stctmp)
    end if
    Gv = Gv*dble(n(2))
        

    if (bc(3) /= 0) then
        allocate(stc4(n(3)))
        forall(i=1:(npoint+1)/2)
            stc4(i) = stc1(i+(npoint+1)/2-1)
        end forall
        forall(i=(npoint+3)/2:(n(3)-(npoint+1)/2+1))
            stc4(i) = 0.0D0
        end forall
        forall(i=(n(3)-(npoint+1)/2+2):n(3))
            stc4(i) = stc1(i-(n(3)-(npoint+1)/2+1))
        end forall
        call galleryCircul(stc4,Gw)
        deallocate(stc4)
    else
        allocate(stctmp(npoint,n(3)))
        call repMat(stc1,n(3),1,stctmp)
        allocate(stc2(2*((npoint-1)/2)+1))
        forall(i=1:2*((npoint-1)/2)+1)
            stc2(i) = dble(i-(npoint-1)/2-1)
        end forall
        call spdiags(stctmp,stc2,n(3),n(3),Gw)
        deallocate(stctmp)
    end if
    Gw = Gw*dble(n(3))
    
    deallocate(stc1)

    return
    end subroutine fdGradMat
    end module fdGradMat_module
