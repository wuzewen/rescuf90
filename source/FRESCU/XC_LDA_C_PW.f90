! XC_LDA_C_PW.f90

! ****************************************************************
      ! Implements the Perdew-Wang'92 local correlation (beyond RPA).
      ! Ref: J.P.Perdew & Y.Wang, PRB, 45, 13244 (1992)
      ! Original Written by L.C.Balbas and J.M.Soler. Dec'96.  Version 0.5.
      ! Modified by Lei Zhang using Matlab. November 2013. Version 0.1.
      ! Translated to FORTRAN here by Zewen Wu, August 2018.
      !
      ! ###### INPUT #######
      ! 
      !
      !
      !
      !
!*******************************************************************

      module XC_LDA_C_PW_module
      contains
      subroutine XC_LDA_C_PW(NSPIN,DS,EC,VC)

      implicit none

      ! input variables
      integer :: NSPIN
      real*8, allocatable :: DS(:,:)

      ! temporary variables
      integer :: npts, ntmp, nara, ii, ig
      real*8  :: DENMIN, ONE, pi, FOUTHD, HALF, THD, THRHLF
      real*8  :: P(3), A(3), ALPHA1(3), BETA(4,3)
      real*8, allocatable :: DTOT(:), ZETA(:), RS(:), DRSDD(:), DZDD(:,:)
      real*8, allocatable :: G(:,:), DGDRS(:,:), B(:), DBDRS(:), C(:)
      real*8, allocatable :: DCDRS(:), FPP0(:), F(:), DFDZ(:), ECtmp(:)
      real*8, allocatable :: DECDRS(:), DECDZ(:), DECDD(:,:)
      real*8, allocatable :: DENS(:), DStmp(:,:), DEDCZ(:)

      ! output variables
      real*8, allocatable :: EC(:), VC(:,:)

      ! body of this function
      DENMIN = 50.0D0*2.220446049250313D-16
      ONE    = 1.0D0+DENMIN
      npts   = size(DS,1)
      nara   = size(DS,2)
      allocate(EC(npts))
      EC     = 0.0D0
      allocate(VC(npts,NSPIN))
      VC     = 0.0D0
      allocate(DENS(npts))
      DENS   = sum(DS,2)
      ntmp   = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                 ntmp = ntmp+1
         end if
      end do
      allocate(DStmp(ntmp,nara))

      ntmp = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                 ntmp          = ntmp+1
                 DStmp(ntmp,:) = DS(ii,:) 
          end if
      end do

      pi     = 3.1415926535897932385D0
      FOUTHD = 4.0D0/3.0D0
      HALF   = 0.5D0
      THD    = 1.0D0/3.0D0
      THRHLF = 1.5D0
      P(1)   = 1.00D0
      P(2)   = 1.00D0
      P(3)   = 1.00D0
      A(1)   = 0.031091D0
      A(2)   = 0.015545D0
      A(3)   = 0.016887D0
      ALPHA1(1) = 0.21370D0
      ALPHA1(2) = 0.20548D0
      ALPHA1(3) = 0.11125D0

      BETA(1,1) = 7.5957D0
      BETA(1,2) = 14.1189D0
      BETA(1,3) = 10.357D0
      BETA(2,1) = 3.5876D0
      BETA(2,2) = 6.1977D0
      BETA(2,3) = 3.6231D0
      BETA(3,1) = 1.6382d0
      BETA(3,2) = 3.3662d0
      BETA(3,3) = 0.88026d0
      BETA(4,1) = 0.49294d0
      BETA(4,2) = 0.62517d0
      BETA(4,3) = 0.49671d0
  
      !write(*,*) "after constants."
      allocate(DTOT(ntmp))
      allocate(ZETA(ntmp))
      allocate(RS(ntmp))
      allocate(DRSDD(ntmp))
      allocate(DZDD(ntmp,nara))

      if (NSPIN == 1) then
              DTOT = DStmp(:,1)
              do ii = 1,ntmp,1
                  if (DTOT(ii) < DENMIN) then
                          DTOT(ii) = DENMIN
                  end if
              end do
              ZETA = 0.0D0
              RS   = (3.0D0/(4.0D0*pi*DTOT))**THD
              DRSDD = (-RS)/DTOT/3.0D0
              DZDD(:,1) = 0.0D0
      else
              DTOT = DStmp(:,1)+DStmp(:,2)
              do ii = 1,ntmp,1
                  if (DTOT(ii) < DENMIN) then
                       DTOT(ii) = DENMIN
                  end if  
              end do
              ZETA = (DStmp(:,1)-DStmp(:,2))/DTOT
              RS   = (3.0D0/(4.0D0*pi*DTOT))**THD
              DRSDD = (-RS)/DTOT/3.0D0
              DZDD(:,1) =  (ONE-ZETA)/DTOT
              DZDD(:,2) = -(ONE+ZETA)/DTOT
      end if

      !write(*,*) "before G."
      allocate(G(ntmp,3))
      G     = 0.0D0
      allocate(DGDRS(ntmp,3))
      DGDRS = 0.0D0

      allocate(B(ntmp), C(ntmp), DBDRS(ntmp), DCDRS(ntmp))
      !allocate(DGDRS(ntmp,3))

      do ig = 1,3,1
          B     = BETA(1,ig)*RS**HALF + BETA(2,ig)*RS &
                + BETA(3,ig)*RS**THRHLF + BETA(4,ig)*RS**(P(ig)+1.0D0)
          DBDRS = BETA(1,ig)*HALF/(RS**HALF)   + BETA(2,ig) &
                + BETA(3,ig)*THRHLF*(RS**HALF) &
                + BETA(4,ig)*(P(ig)+1.0D0)*(RS**P(ig))
          C     = 1.0D0 + 1.0D0/(2.0D0*A(ig)*B)
          DCDRS = -((C-1.0D0)*DBDRS/B)
          G(:,ig) = (-2.0D0)*A(ig)*(1.0D0+ALPHA1(ig)*RS)*log(C)
          DGDRS(:,ig) = (-2.0D0)*A(ig)*(ALPHA1(ig)*log(C) + &
                         (1.0D0+ALPHA1(ig)*RS)*DCDRS/C)
      end do

      !write(*,*) "after loop"
      allocate(FPP0(ntmp), F(ntmp), DFDZ(ntmp))
      allocate(ECtmp(ntmp), DECDRS(ntmp), DEDCZ(ntmp))

      C = 1.0D0/(2.0D0**FOUTHD-2.0D0)
      FPP0 = 8.0D0*C/9.0D0
      F    = ((ONE+ZETA)**FOUTHD + (ONE-ZETA)**FOUTHD-2.0D0)*C
      DFDZ = FOUTHD*((ONE+ZETA)**THD-(ONE-ZETA)**THD)*C

      ECtmp =  G(:,1)-G(:,3)*F/FPP0*(ONE-ZETA**4) &
            + (G(:,2)-G(:,1))*F*ZETA**4
      DECDRS =  DGDRS(:,1) - DGDRS(:,3)*F/FPP0*(ONE-ZETA**4) &
             + (DGDRS(:,2)-DGDRS(:,1))*F*ZETA**4
      DEDCZ  = (-G(:,3))/FPP0*(DFDZ*(ONE-ZETA**4)-F*4.0D0*ZETA**3) &
             + (G(:,2)-G(:,1))*(DFDZ*ZETA**4 + F*4.0D0*ZETA**3)

      !write(*,*) "after DEDCZ."
      allocate(DECDD(ntmp,nara))

      if (NSPIN == 1) then
              !write(*,*) "begining of the if"
              DECDD(:,1) = DECDRS*DRSDD
              !write(*,*) "after DECDD"
              ntmp = 0
              do ii = 1,npts,1
                  if (DENS(ii) > DENMIN) then
                      ntmp = ntmp+1
                      VC(ii,1) = ECtmp(ntmp)+DTOT(ntmp)*DECDD(ntmp,1)
                  end if
              end do
      else
              DECDD(:,1) = DECDRS * DRSDD + DEDCZ * DZDD(:,1)
              DECDD(:,2) = DECDRS * DRSDD + DEDCZ * DZDD(:,2)
              ntmp = 0
              do ii = 1,npts,1
                  if (DENS(ii) > DENMIN) then
                      ntmp = ntmp+1
                      VC(ii,1) = ECtmp(ntmp)+DTOT(ntmp)*DECDD(ntmp,1)
                      VC(ii,2) = ECtmp(ntmp)+DTOT(ntmp)*DECDD(ntmp,2)
                  end if
              end do
      end if
      !write(*,*) "after if"

      ntmp = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                  ntmp = ntmp+1
                  EC(ii) = ECtmp(ntmp)
          end if
      end do

      !write(*,*) "end of XC_LDA_C_PW."

      return
      end subroutine XC_LDA_C_PW
      end module XC_LDA_C_PW_module


