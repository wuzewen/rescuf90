! smiDSYEVD.f90

!***********************************
      !
      !
      !
!**********************************

      module smiDSYEVDMOD
      contains
      subroutine smiDSYEVD(jobz,UPLO,N,A,MB,NB,nprow,npcol,&
                      LWORK,W,Z)

      implicit none

      integer :: N, MB, NB, nprow, npcol, LWORK
      character(len=1) :: jobz
      real*8, allocatable :: A(), W(), Z()

      IBTYPE = 1
      LIWORK = 7*N+8*npcol+2
      nprocs = nprow*npcol
      mone   = -1
      iam    = 0
      ictxt  = 0
      myrow  = 0
      mycol  = 0

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,Z_np)
      A_np
      Z_np

      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descZ,N,N,MB,NB,izero,izero,ictxt,Z_np,descinfo)
      allocate(WORK())
      allocate(IWORK())
      call PDSYEVD(jobz,UPLO,N,A,ione,descA,W,Z,ione,ione,descZ,WORK,mone,IWORK,LIWORK,INFO)

      LIWORK = IWORK(1)
      deallocate(IWORK)
      allocate(IWORK())
      LWORKTMP = 1.5*WORK(1)/(WORK(1)+1024)+1024
      if (LWORK .LT. LWORKTMP .OR. LWORK .LT. 0) then
              LWORK = LWORKTMP
      end if 
      deallocate(WORK)
      allocate(LWORK)

      call PDSYEVD(jobz,UPLO,N,A,ione,descA,W,Z,ione,ione,descZ, &
              WORK,mone,IWORK,LIWORK,INFO)

      if (INFO .LT. 0) then
              write(*,*) 'Error'
      else if (INFO .GT. 0) then
              write(*,*) "Error"
      end if

      deallocate(WORK,IWORK)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiDSYEVD
      end module smiDSYEVDMOD
