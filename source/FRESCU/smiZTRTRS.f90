! smiZTRTRI.f90

!**************************************************
      !
      !
      !
!****************************************************

      module smiZTRTRSMOD
      contains
      subroutine smiZTRTRS(UPLO,TRANS,N,NRHS,A,B,MB,NB,nprow,npcol)

      implicit none

      integer :: N, MB, NB, nprow, npcol, nprocs
      complex*16, allocatable :: A(), B()

      character(len=1) :: UPLO,diag
      integer :: iam, INFO, ictxt, myrow, mycol

      !UPLO = 'U'
      diag = 'N'
      iam  =  0
      ictxt = 0
      myrow = 0
      mycol = 0
      nprocs = nprow*npcol

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,B_np)
      A_np  =  
      B_np  = 
      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descB,N,N,MB,NB,izero,izero,ictxt,B_np,descinfo)
      call PZTRTRS(UPLO,TRANS,diag,N,HRHS,A,ione,ione,descA,&
                    B,ione,ione,descB,INFO)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiZPOTRF
      end module smiZPOTRFMOD
