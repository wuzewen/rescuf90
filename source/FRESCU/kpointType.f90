! kpointType.f90
    
!***************************************************************
    !
    ! This is to define a type to contain kpoint information
    !
!***************************************************************
    
    module kpointType_module
    
    type :: kpointType
        integer              :: gridn(3)
        integer              :: currentreduce(3)
        integer, allocatable :: ikpt(:)
        integer, allocatable :: ired(:)
        integer, allocatable :: isop(:)
        real*8, allocatable  :: kcartesian(:,:)
        real*8, allocatable  :: kcartesianPoints(:,:)
        real*8, allocatable  :: kdirect(:,:)
        real*8, allocatable  :: ikdirect(:,:)
        !real*8, allocatable  :: kdirectPoints(:,:)
        character(len=20)    :: sampling
        real*8               :: kshift(3)
        character            :: sympoints
        character(len=20)    :: Ksamlingtype
        character(len=20)    :: Ktype
        real*8, allocatable  :: weight(:)
    end type kpointType
    
    end module