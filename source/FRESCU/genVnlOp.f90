! genVnlOp.f90
    
!*******************************************************************
    !
    !
    !
!*******************************************************************
    
    module genVnlOp_module
    contains
    subroutine genVnlOp(FRC,gpu,X,vnl)
    
    !external ZGEMM
    use FORTRAN_RESCU_CALCULATION_TYPE
    use bsxfunTimes_CMPLX_module
    external ZGEMM

    !implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    logical :: gpu
    complex*16, allocatable :: X(:,:)
    
    ! temporary variables
    !integer :: ii
    real*8  :: avec(3,3), dr
    complex*16, allocatable :: kbedr(:,:), vnltmp(:,:)
    !complex*16, allocatable :: chilm(:,:)!, chilmtmp(:,:)
    complex*16, allocatable :: chilmtmp(:,:)
    character(len=1) :: transa,transb
    integer          :: m, n, k, lda, ldb, ldc
    complex*16           :: alpha, beta
    !complex*16, allocatable :: chilmtmp2(:,:), vnl2(:,:)
    ! output variabels
    complex*16, allocatable :: vnl(:,:)
    
    ! body of this function
    !call det(FRC%domain%latvec,dr)
    avec = FRC%domain%latvec
    dr   = avec(1,1)*avec(2,2)*avec(3,3)&
          +avec(1,2)*avec(2,3)*avec(3,1)&
          +avec(1,3)*avec(2,1)*avec(3,2)&
          -avec(1,3)*avec(2,2)*avec(3,1)&
          -avec(1,1)*avec(2,3)*avec(3,2)&
          -avec(1,2)*avec(2,1)*avec(3,3)
    if (FRC%interpolation%vnl) then
        dr = dr/dble(product(FRC%domain%fgridn))
    else
        dr = dr/dble(product(FRC%domain%cgridn))
    end if
    !allocate(kbedr(size(FRC%potential%vnl%KBEnergy),1))
    !kbedr(:,1) = dcmplx(FRC%potential%vnl%KBEnergy(:)*dr)
    !allocate(chilm(size(FRC%potential%vnl%KBorb,1),size(FRC%potential%vnl%KBorb,2)))
    !chilm      = FRC%potential%vnl%KBorb
    
    if (gpu) then
        !call gpuArray(kbedr)
        !call gpuArray(chilm)
        !chilmtmp = matmul(transpose(chilm),X)
        !call bsxfunTimes(chilmtmp,kbedr,vnl)
        !vnl = matmul(chilm,vnl)
        !if (FRC%interpolation%vnl) then
            !call initFun(vnl,X,vnltmp)
            !call antFun(vnltmp,X,vnl)
        ! end if
    else
        !write(*,*) "kbedr ="
        !write(*,*)  kbedr
        !write(*,*) "chilm ="
        !write(*,*)  chilm
        allocate(chilmtmp(size(FRC%potential%vnl%KBorb,2),size(X,2)))
        !allocate(chilmtmp(size(chilm,2),size(X,2)))
        !allocate(chilmtmp2(size(chilm,2),size(X,2)))
        chilmtmp = (0.0D0,0.0D0)
        transa   = 'C'
        transb   = 'N'
        alpha    =  (1.0D0,0.0D0)
        beta     =  (0.0D0,0.0D0)
        !m        =  size(chilm,2)
        m        =  size(FRC%potential%vnl%KBorb,2)
        n        =  size(X,2)
        k        =  size(X,1)
        lda      =  k
        ldb      =  k
        ldc      =  m
        !call ZGEMM(transa,transb,m,n,k,alpha,chilm,lda,X,ldb,beta,chilmtmp,ldc)
        call ZGEMM(transa,transb,m,n,k,alpha,FRC%potential%vnl%KBorb,lda,X,ldb,beta,chilmtmp,ldc)
        !chilmtmp2 = matmul(conjg(transpose(chilm)),X)
        !write(*,*) "SIZES"
        !write(*,*) "chilm"   , size(chilm,1),    size(chilm,2)
        !write(*,*) "chilmtmp", size(chilmtmp,1), size(chilmtmp,2)
        !write(*,*) "X"       , size(X,1)       , size(X,2)
        !write(*,*) "chilmtmp ="
        !write(*,*)  count(abs(chilmtmp-chilmtmp2)>0.000000001D0)
        !allocate(vnltmp(size(chilmtmp,1),size(chilmtmp,2)))
        !forall(ii=1:size(chilmtmp,2))
        !        vnltmp(:,ii) = chilmtmp(:,ii)*kbedr(:,1)
        !end forall
        !call bsxfunTimes_CMPLX(chilmtmp,kbedr,vnltmp)
        do ii = 1,size(chilmtmp,2),1
            chilmtmp(:,ii) = chilmtmp(:,ii)*FRC%potential%vnl%KBEnergy*dr
        end do
        !write(*,*) "kbedr"   , size(kbedr,1)   , size(kbedr,2)
        !write(*,*) "vnltmp"  , size(vnltmp,1)  , size(vnltmp,2)
        !allocate(vnl(size(FRC%potential%vnl%KBorb,1),size(chilmtmp,2)))
        !allocate(vnl(size(chilm,1),size(vnltmp,2)))
        !allocate(vnl2(size(chilm,1),size(vnltmp,2)))
        vnl     =  (0.0D0,0.0D0)
        transa  =  'N'
        transb  =  'N'
        alpha   =  (1.0D0,0.0D0)
        beta    =  (0.0D0,0.0D0)
        m       =   size(FRC%potential%vnl%KBorb,1)
        !m       =   size(chilm,1)
        n       =   size(chilmtmp,2)
        k       =   size(FRC%potential%vnl%KBorb,2)
        !k       =   size(chilm,2)
        lda     =   m
        ldb     =   k
        ldc     =   m
        !call ZGEMM(transa,transb,m,n,k,alpha,chilm,lda,vnltmp,ldb,beta,vnl,ldc)
        call ZGEMM(transa,transb,m,n,k,alpha,FRC%potential%vnl%KBorb,lda,chilmtmp,ldb,beta,vnl,ldc)
        !vnl2 = matmul(chilm,vnltmp)

        !write(*,*) "Vnl ="
        !write(*,*)  count(abs(Vnl-Vnl2)>0.000000001D0)
        !write(*,*) "Stop in GenVnlOp."
        !stop
        !write(*,*) "vnl"     , size(vnl,1)     , size(vnl,2)
        if (FRC%interpolation%vnl) then
            write(*,*) "Error in genVnlOp.f90. FRC%interpolation%vnl should be false now."
            stop
            !call initFun(vnl,X,vnltmp)
            !call antFun(vnltmp,X,vnl)
        end if
    end if
    
    return
    end subroutine genVnlOp
    end module genVnlOp_module
