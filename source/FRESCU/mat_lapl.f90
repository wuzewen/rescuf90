! mat_lapl.f90

!**********************************************************
      !
      !
      !
!**********************************************************

      module mat_lapl_module
      contains
      subroutine mat_lapl(x,n,G,J1,L,J2,kpt,Lx)

      !use Acell_type
      !use Acell_cmplx_type
      use ComplexVata_2D_module
      use rkronprod_cmplx_module
      use rlkronprod_cmplx_module
      use lkronprod_cmplx_module

      implicit none

      ! input variables
      complex*16, allocatable :: x(:,:)
      integer                 :: n(3), ntmp1, ntmp2
      !type(Acell_cmplxType)   :: G(3), L(3)
      type(ComplexVata_2D)    :: G(3), L(3)
      real*8                  :: J1(3,3), J2(3,3), kpt(3)

      ! temporary variables
      real*8                  :: k2, kptt(3,1), kptm(3,1)
      complex*16              :: imnu
      complex*16, allocatable :: Lxtmp(:,:), ytmp(:,:), xtmp(:,:)
      complex*16, allocatable :: xtmp1(:,:)

      ! output variables
      complex*16, allocatable :: Lx(:,:)

      ! body of this function

      !write(*,*) "input of mat_lapl.f90"
      !write(*,*) "x = "
      !write(*,*)  x
      !write(*,*) "n = "
      !write(*,*)  n
      !write(*,*) "j1 ="
      !write(*,*)  j1
      !write(*,*) "j2 ="
      !write(*,*)  j2
      !write(*,*) "kpt ="
      !write(*,*)  kpt

      imnu      = (0.0D0,1.0D0)
      k2        = sum(kpt*kpt)
      kptt(:,1) = kpt
      kptm      = matmul(J1,kptt)
      ntmp1     = size(L(1)%vata,1)*size(x,1)/size(L(1)%vata,2)
      ntmp2     = size(x,2)
      allocate(Lx(ntmp1,ntmp2))
      allocate(xtmp(ntmp1,ntmp2))

      call rkronprod_cmplx(L(1)%vata,x,Lx)
      Lx        = Lx*J2(1,1)
      !write(*,*) "Lx ="
      !write(*,*)  Lx
      !stop
      !write(*,*) "Lx ="
      !write(*,*)  Lx
      call rlkronprod_cmplx(L(2)%vata,x,n(1),Lxtmp)
      Lxtmp     = Lxtmp*J2(2,2)
      Lx        = Lx+Lxtmp
      !write(*,*) "Lx ="
      !write(*,*)  Lx
      deallocate(Lxtmp)
      call lkronprod_cmplx(L(3)%vata,x,Lxtmp)
      Lxtmp     = Lxtmp*J2(3,3)
      Lx        = Lx+Lxtmp

      !write(*,*) "In mat_lapl, Lx ="
      !write(*,*)  Lx

      if (J2(1,2) /= 0.0D0 .or. J2(1,3) /= 0.0D0 .or. kptm(1,1) /= 0.0D0) then
              call rkronprod_cmplx(G(1)%vata,x,xtmp)
              if (kptm(1,1) /= 0.0D0) then
                      Lx = Lx+2.0D0*imnu*kptm(1,1)*xtmp
              end if
              !write(*,*) "Lx ="
              !write(*,*)  Lx
              !deallocate(xtmp)
              if (J2(1,2) /= 0.0D0) then
                      call rlkronprod_cmplx(G(2)%vata,xtmp,n(1),ytmp)
                      Lx = Lx+2.0D0*J2(1,2)*ytmp
                      !write(*,*) "ytmp ="
                      !write(*,*)  ytmp
                      !write(*,*) "J2(1,2)"
                      !write(*,*)  J2(1,2)
                      !write(*,*) "ytmp2 ="
                      !write(*,*)  2.0D0*J2(1,2)*ytmp
                      deallocate(ytmp)
              end if
              !write(*,*) "ytmp ="
              !write(*,*)  ytmp
              !write(*,*) "Lx ="
              !write(*,*)  Lx
              if (J2(1,3) /= 0.0D0) then
                      call lkronprod_cmplx(G(3)%vata,xtmp,ytmp)
                      Lx = Lx+2.0D0*J2(1,3)*ytmp
                      deallocate(ytmp)
              end if
              !write(*,*) "Lx ="
              !write(*,*)  Lx
              !deallocate(xtmp)
      end if
      if (J2(2,3) /= 0.0D0 .or. kptm(2,1) /= 0.0D0) then
              call rlkronprod_cmplx(G(2)%vata,x,n(1),xtmp1)
              if (kptm(2,1) /= 0.0D0) then
                      Lx = Lx+2.0D0*imnu*kptm(2,1)*xtmp1
              end if
              !deallocate(xtmp)
              if (J2(2,3) /= 0.0D0) then
                      call lkronprod_cmplx(G(3)%vata,xtmp1,ytmp)
                      Lx = Lx+2.0D0*J2(2,3)*ytmp
                      deallocate(ytmp)
              end if
              deallocate(xtmp1)
      end if
      if (kptm(3,1) /= 0.0D0) then
              call lkronprod_cmplx(G(3)%vata,x,ytmp)
              Lx = Lx+2.0D0*imnu*kptm(3,1)*ytmp
              deallocate(ytmp)
      end if
      if (k2 /= 0.0D0) then
              Lx = Lx - k2*x
      end if

      !write(*,*) "LxFinal ="
      !write(*,*)  Lx

      return
      end subroutine mat_lapl
      end module mat_lapl_module

