!  GetHartreePotential.f90 

!**************************************************************************
    !
    !
    !
!**************************************************************************

    module GetHartreePotential_module
    contains
    subroutine GetHartreePotential(FRC,rho,bc,symmetry,VHout)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    use distmat_zeroslike_1D_module
    use distmat_gather_1D_module
    use ModBCDist_1D_module
    use mixedPoissonSolver_module
    !use mixedPoissonSolver_FFTW_module
    use symmetrize_1D_module
    use SYMMETRIZEMOD
    use MPI_Bcast_variable_INT_1D_module
    !use initDistArray_1D_module

    implicit none

    ! input ariables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    logical                         :: symmetry
    type(VataMN_1D)                 :: rho
    integer                         :: bc(3)
    
    ! temporery valuables
    integer             :: ii, jj, fnxyz(3), rank
    integer             :: fnx, fny, fnz, mpirank, mpisize, ispin, tmp(2), fn
    logical             :: mpistat, rhonum
    type(VataMN_1D)     :: rhotmp, VH, VH1, VH2
    real*8, allocatable :: rhotmp2(:), rhotmp3(:,:,:), sym_t(:,:), sym_rec(:,:,:)
    integer, allocatable :: fntmp1(:), fntmp2(:), ranks(:)

    ! output valuables
    type(VataMN_1D)     :: VHout

    ! Body of hartreePotential
    !write(*,*) "I'm here at the begining of GetHartreePotential."
    !write(*,*) "input of this function"
    !write(*,*)  rho%vata
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    !rhonum  = .TRUE.
    !rhotmp  = rho
    
    !fnx = size(rho%vata)
    !fny = size(rho%vata,2)
    !fnz = size(rho%vata,3)
    fnx = FRC%domain%fgridn(1)
    fny = FRC%domain%fgridn(2)
    fnz = FRC%domain%fgridn(3)
    
    tmp(1) = rho%m
    tmp(2) = 1
    rank   = 0
    !write(*,*) "before distmat_zeroslike_1D."
    !write(*,*) rho%m, rho%n, rho%mblock, rho%nblock, rho%mproc, rho%nproc
    call distmat_zeroslike_1D(FRC,rho,tmp,rhotmp)
    !write(*,*) "after distmat_zeroslike_1D."
    if (ispin == 1) then
        rhotmp%vata = rho%vata
    else if (ispin == 2) then
        
        write(*,*) "Error in GetHartreePotential.f90. Collinear spin is not available now."
        stop
        ! rhotmp%dataA = sum(rho%dataA,2)
        
    else if (ispin == 4) then
        
        write(*,*) "Error in GetHartreePotential.f90. General spin is not available now."
        stop
        ! rhotmp%dataA = rho%dataA(:,1)
        
    end if
    !write(*,*) 'rhotmp:m,n',rhotmp%m,rhotmp%n
    allocate(rhotmp2(rhotmp%m))
    !write(*,*) "make 2"
    call distmat_gather_1D(FRC,rhotmp,rank,rhotmp2)
    !write(*,*) "atomrho"
    !write(*,*)  size(FRC%rho%atom%vata,1)
    !write(*,*) "I'm here before MixedPoissonSolver in GetHartreePotential."
    if (mpirank == 0) then
        allocate(rhotmp3(fnx,fny,fnz))
        rhotmp3 = reshape(rhotmp2,(/fnx,fny,fnz/))
        !write(*,*) "VHrhotmp3InGetVH."
        !do ii = 1,fnz,1
        !    write(*,*) ii
        !    do jj = 1,fnx,1
        !        write(*,*)  rhotmp3(jj,:,ii)
        !    end do
        !end do
        call MixedPoissonSolver(FRC,rhotmp3,bc,VH1%vata)
        !call MixedPoissonSolver_FFTW(FRC,rhotmp3,bc,VH1%vata)
        !write(*,*) "I'm here after MixedPoissonSolver in GetHartreePotential."
        !write(*,*) "rhotmp3"
        !write(*,*)  rhotmp3
        !write(*,*) "VH1%vata"
        !write(*,*)  VH1%vata
        
        if (FRC%symmetry%pointsymmetry .and. symmetry) then
            allocate(sym_rec(size(FRC%symmetry%sym_rec,1),size(FRC%symmetry%sym_rec,2),size(FRC%symmetry%sym_rec,3)))
            sym_rec = FRC%symmetry%sym_rec
            !allocate(sym_t(size(FRC%symmetry%sym_t,1),size(FRC%symmetry%sym_t,2)))
            !sym_t   = FRC%symmetry%sym_t
            !allocate(VH2%vata(size(VH1%vata)))
            fnxyz(1) = fnx 
            fnxyz(2) = fny
            fnxyz(3) = fnz
            call SYMMETRIZE(FRC,VH1%vata,fnxyz,sym_rec,FRC%symmetry%sym_t)
            !call symmetrize_1D(FRC,VH1%vata,fnxyz,sym_rec,FRC%symmetry%sym_t)
        else
                !allocate(VH2%vata(size(VH1%vata)))
                !VH2 = VH1
                !VH1%vata = 0.0D0
        end if
    else
        allocate(VH1%vata(fnx*fny*fnz))
        VH1%vata   = 0.0D0
        VH1%m      = fnx*fny*fnz
        VH1%n      = 1
        VH1%mblock = fnx*fny*fnz
        VH1%nblock = 1
        VH1%mproc  = mpisize
        VH1%nproc  = 1
    end if
    rhonum = .FALSE.
    !write(*,*) "I'm here after MixedPoissonSolver in GetHartreePotential."
    if (.not. rhonum) then
        fn = size(VH1%vata)
        if (mpistat) then
            
            !write(*,*) "Error in GetHartreePotential.f90. mpi is not available now."
            !stop
            allocate(fntmp1(1))
            fntmp1(1) = fn
            allocate(fntmp2(1))
            fntmp2(1) = fn
            allocate(ranks(mpisize))
            forall(ii=1:mpisize)
                    ranks(ii) = ii-1
            end forall
            call MPI_Bcast_variable_INT_1D(fntmp1,0,ranks,fntmp2)
            fn = fntmp2(1)
        end if
        !fn = ceiling(dble(fn)/dble(mpisize))
        call initDistArray_1D(fn,1,fn,1,mpisize,1,VH1)
        !write(*,*) "before VHout"
        !allocate(VHout%vata(size(rho%vata)))
        !write(*,*) 'size of VH2', size(VH2%vata)
        !write(*,*) 'mn', rho%mblock, rho%nblock,rho%mproc,rho%nproc
        !write(*,*) 'size of VHout', size(VHout%vata)
        call ModBCDist_1D(FRC,VH1,rho%mblock,rho%nblock,rho%mproc,rho%nproc,.FALSE.,.FALSE.,VHout)
        !allocate(VHout%vata(fn))
    end if

    !write(*,*) "in genVH. sizes", size(VHout%vata)
    
    return
    end subroutine GetHartreePotential
    end module GetHartreePotential_module
