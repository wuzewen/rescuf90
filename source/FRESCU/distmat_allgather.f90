! distmat_allgather.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module distmat_allgather_module
    contains
    subroutine distmat_allgather(FRC,dA,A)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_1D) :: dA
    
    ! temporary variables
    integer :: mpisize, mpirank, nx, ny
    
    ! output variables
    real*8, allocatable :: A(:)
    
    ! body of this function
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    !nx      = size(dA%vata,1)
    !ny      = size(dA%vata,2)
    
    if (mpisize == 1) then
        !allocate(A(size(dA%vata)))
        A = dA%vata
    else
        !if (mpirank == 0) then
        !    call GetGlobalInd_2D(mpirank,dA,iloc,jloc)
        !    ntmp = count(dA%vata /= 0)
        !    allocate(iA(ntmp))
        !    allocate(jA(ntmp))
        !    allocate(valA(ntmp))
        !    allocate(Atmp(1,1)%vata(ntmp,3))
        !    ntmp = 0
        !    do ii = 1,nx,1
        !        do jj = 1,ny,1
        !            if (dA%vata(ii,jj) /= 0) then
        !                ntmp = ntmp+1
        !                iA(ntmp)   = ii
        !                jA(ntmp)   = jj
        !                valA(ntmp) = dA%vata(ii,jj)
        !            end if
        !        end do
        !    end do
        !    forall(ii=1:ntmp)
        !        Atmp(1,1)%vata(ii,1) = iloc(iA(ii))
        !        Atmp(1,1)%vata(ii,2) = jloc(jA(ii))
        !        Atmp(1,1)%vata(ii,3) = valA(ii)
        !    end forall
        !end if
        !do ii = 1,mpisize-1,1
        !    if (mpirank == ii) then
        !        call GetGlobalInd_2D(mpirank,dA,iloc,jloc)
        !        ntmp = count(dA%vata /= 0)
        !        allocate(iA(ntmp))
        !        allocate(jA(ntmp))
        !        allocate(valA(ntmp))
        !        allocate(AtmpV(ntmp,3))
        !        ntmp = 0
        !        do ii = 1,nx,1
        !            do jj = 1,ny,1
        !                if (dA%vata(ii,jj) /= 0) then
        !                    ntmp = ntmp+1
        !                    iA(ntmp)   = ii
        !                    jA(ntmp)   = jj
        !                    valA(ntmp) = dA%vata(ii,jj)
        !                end if
        !            end do
        !        end do
        !        forall(ii=1:ntmp)
        !            AtmpV(ii,1) = iloc(iA(ii))
        !            AtmpV(ii,2) = jloc(jA(ii))
        !            AtmpV(ii,3) = valA(ii)
        !        end forall
        !        call MPI_send_variable(Atmp,0,0)
        !    else if (mpirank == 0) then
        !        call MPI_Recv_variable(ii,0,Atmp(ii+1,1))
        !    end if
        !end do
        !********************
        
        !********************
    end if
    
    return
    end subroutine distmat_allgather
    end module distmat_allgather_module
                
