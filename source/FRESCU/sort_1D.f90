! sort_1D.f90
    
!*****************************************************************************
    !
    !
    !
!*****************************************************************************
    
    module sort_1D_module
    contains
    subroutine sort_1D(A,B,ind)
    
    implicit none
    
    ! input variables
    real*8,  allocatable :: A(:)
    
    ! temporary variables
    integer              :: nn, ii
    real*8               :: Atmp
    real*8 , allocatable :: Btmp(:)
    
    ! output variables
    real*8 , allocatable :: B(:)
    integer, allocatable :: ind(:), indtmp(:)
    
    ! body of this function
    nn = size(A)
    allocate(B(nn),ind(nn),Btmp(nn))
    Btmp = A
    atmp = maxval(A)+1.0D0
    allocate(indtmp(1))
    do ii = 1,nn,1
        B(ii)           = minval(Btmp)
        indtmp          = minloc(Btmp)
        ind(ii)         = indtmp(1)
        Btmp(indtmp(1)) = Btmp(ind(ii))+atmp
    end do
    
    return
    end subroutine sort_1D
    end module sort_1D_module
        
