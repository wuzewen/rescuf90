!  xcMain.f90 
!
!  FUNCTIONS:
!  xcMain - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: xcMain
!
!  PURPOSE: This subroutine is used to generate exchange correlation potential
!  and density.
!  The only input is electronic density. 
!  If the system has symmetry, the exchange correlation potential and density
!  will be modified using symmetries, including space symmetry and point
!  symmetry.
! 
!  There is a list of all the XC functionals, the calculation will be done one
!  by one, and then they will be added together.
!
!****************************************************************************

    module xcMain_module
    contains
    subroutine xcMain(FRC)
    
    ! data type module
    use FORTRAN_RESCU_CALCULATION_TYPE
    !use rho_type_module
    use VataMN_1D_module
    use VataMN_2D_module
    
    ! function module
    use parseFuncName_module
    use calcXCinput_module
    use distmat_nnz_1D_module
    use distmat_nnz_2D_module
    use distmat_zeroslike_1D_module
    use distmat_zeroslike_2D_module
    use getXC_module
    !use initDistArray_module
    use distmat_feval_plus_1D_module
    use distmat_feval_plus_2D_module
    use modBCDist_1D_module
    use ModBCDist_2D_module
    use symmetrize_1D_module
    use symmetrize_2D_module
    !use applyUnitry_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    integer                        :: mpirank, mpisize, ispin, fgridn(3), fn, iter, nlist
    integer                        :: ii, nnztau, nvec(3), ntmp, jj, fnn(2)
    logical                        :: lxcstat, usevtau, gradFuncDer, laplFuncDer
    real*8                         :: pcc
    real*8, allocatable            :: tmp1(:,:), tmp2(:,:), tmp3(:,:), tmp4(:,:), U(:,:)
    real*8, allocatable            :: sym_rec(:,:,:), sym_t(:,:), erhovata(:), vrhovata(:,:)
    character(len=20), allocatable :: faprx(:), flist(:)
    type(VataMN_2D)                :: rho, sig, vsig, lapl, tau, VXCout, vrho, VKout, Vlapl, VXCoutO
    type(VataMN_2D)                :: vsigTmp, vtau, erhotmp
    type(VataMN_1D)                :: XCdens, rhov, erho, XCdensO
    real*8, allocatable            :: gradRho(:,:)
    character(len=20)    :: flax, numb
    integer :: cou
    ! output variables

    ! Body of xcMain
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    fgridn  = FRC%domain%fgridn
    fn      = product(fgridn)
    iter    = FRC%scloop
    lxcstat = FRC%functional%libxc
    nlist   = size(FRC%functional%list)
    allocate(flist(nlist))
    flist   = FRC%functional%list
    
    
    call parseFuncName(flist,faprx)
    usevtau = .FALSE.
    if ((any(faprx == "MGGA") .and. (.not. any(flist == "XC_MGGA_X_TB09")))) then
        usevtau = .TRUE.
    end if
    pcc = 0.5D0


    !write(*,*) 'in xcMain, before calcXCinput.'
    call calcXCinput(FRC,pcc,rho,sig,gradRho,lapl,tau,U)


    !write(numb,"(i4.4)") mpirank
    !flax = 'rhoFinal_p'//trim(numb)//'.txt'
    !cou  =  mpirank + 10
    !open(unit=cou,file=flax)
    !write(cou,*) rho%vata
    !close(cou)
    !stop
    !open(unit=12,file='xc.out')
    !write(12,*) "I'm here after calcXCinput in xcMain."
    !write(*,*) "output in xcMain: rho"
    !write(12,*)  rho%m, rho%n, rho%mblock, rho%nblock, rho%mproc, rho%nproc
    !do ii = 1,size(rho%vata,1),1
    !    write(*,*) rho%vata(ii,:)
    !end do
    !write(12,*) "output: sig"
    !write(12,*)  sig%vata

    !write(12,*) "output: gradRho"
    !write(12,*)  gradRho
    !write(12,*) "output: lapl"
    !write(12,*)  lapl%vata
    !write(12,*) "output: tau"
    !write(12,*)  tau%vata
    !write(12,*) "output: U"
    !write(12,*)  U

    
    !call distmat_nnz_2D(FRC,tau,nnztau)
    
    !if (nnztau == 0 .and. any(faprx == "MGGA")) then
    !    if (mpirank == 0) then
    !        write(*,*) "warning From xcMain"
    !    end if
    !    flist(1) = "XC_GGA_X_PBE"
    !    flist(2) = "XC_GGA_C_PBE"
    !    usevtau  = .FALSE.
    !end if
    
    allocate(rhov%vata(size(rho%vata,1)))
    rhov%vata(:) = rho%vata(:,1)
    rhov%m       = rho%m
    rhov%n       = rho%n
    rhov%mblock  = rho%mblock
    rhov%nblock  = rho%nblock
    rhov%mproc   = rho%mproc
    rhov%nproc   = rho%nproc
    fnn(1)       = fn
    fnn(2)       = 1
    call distmat_zeroslike_1D(FRC,rhov,fnn,XCdens)
    fnn(1)       = fn
    fnn(2)       = 2
    call distmat_zeroslike_2D(FRC,rho,fnn,VXCout)
    VXCout%nblock = 2
    !write(12,*) "XCdens after distmat_zeroslike in xcMain."
    !write(12,*)  XCdens%vata
    !write(12,*) "VXCout after distmat_zeroslike in xcMain."
    !do ii = 1,size(VXCout%vata,1),1
    !    write(12,*) VXCout%vata(ii,:)
    !end do
    if (usevtau) then
        write(*,*) "Error in xcMain.f90. usetau can't be true now."
        stop
        !call distmat_zerolike_2D(FRC,rho,(/fn,2/),VKout)
    end if
    !write(*,*) "rho before getXC."
    !do ii = 1,size(rho%vata,1),1
    !    write(*,*) ii,rho%vata(ii,:)
    !end do
    do ii = 1,nlist,1
        !erhotmp%vata(:,1) = erho%vata(:)
        !write(*,*) 'in xcmain, before getXC'
        call getXC(FRC,flist(ii),rho,sig,lapl,tau,erho%vata,vrho%vata,vsig,vlapl,vtau)
        !write(12,*) "erho%vata in xcMain"
        !write(12,*)  erho%vata
        !write(*,*) "vrho%vata in xcMain"
        !do jj = 1,size(vrho%vata,1),1
        !    write(*,*) vrho%vata(jj,:)
        !end do
        call initDistArray_1D(fn,1,rho%mblock,1,mpisize,1,erho)
        call initDistArray_2D(fn,2,rho%mblock,2,mpisize,1,vrho)
        gradFuncDer = .FALSE.
        laplFuncDer = .FALSE.
        !write(*,*) "erho%vata after initDist in xcMain"
        !write(*,*)  erho%vata
        !write(*,*) "vrho%vata after initDist in xcMain"
        !do jj = 1,size(vrho%vata,1),1
        !    write(*,*) vrho%vata(jj,:)
        !end do
        if (size(vsig%vata) /= 0) then
            gradFuncDer = .TRUE.
        end if
        if (size(vlapl%vata) /= 0) then
            laplFuncDer = .TRUE.
        end if
        if (trim(flist(ii)) == "XC_MGGA_X_TB09") then
            gradFuncDer = .FALSE.
            laplFuncDer = .FALSE.
        end if

        !write(*,*) 'in xcMian, after getXC.'
        
        if (gradFuncDer) then
            !if (lxcstat) then
            !    call initDistArray(fn,3,rho%mblock,3,mpisize,1,vsig)
            !    call bsxfunTimes(vsig%vata(:,1),2*gradRho%vata(:,1:3),tmp1)
            !    call bsxfunTimes(vsig%vata(:,2),  gradRho%vata(:,4:6),tmp2)
            !    call bsxfunTimes(vsig%vata(:,3),2*gradRho%vata(:,4:6),tmp3)
            !    call bsxfunTimes(vsig%vata(:,2),  gradRho%vata(:,1:3),tmp4)
            !    call cat(2,tmp1,tmp2,tmp3,tmp4)
            !    vsig%n = 6
            !else
            !    call InitDistArray(fn,6,rho%mblock,6,mpisize,1,vsig)
            !end if
            
            !call genGradFunPara(FRC,fgridn)
            !vsig = gFun(vsig)
            !vsigTmp%vata = vsig%vata(:,(/1,5,9,10,14,18/))
            !vsigTmp%n    = 6
            !call ModBCDist(FRC,vsig,vrho%mblock,1,mpisize,1,vsig)
            !vrho%vata(:,1) = vrho%vata(:,1)-sum(vsig%vata(:,(/1,2,3/)),2)
            !vrho%vata(:,2) = vrho%vata(:,2)-sum(vsig%vata(:,(/4,5,6/)),2)
        end if
        
        if (laplFuncDer) then
            !call InitDistArray(fn,2,rho%mblock,2,mpisize,1,vlapl)
            !call ModBCDist(FRC,vlapl,fn,2,1,mpisize,vlapl)
            !if (mpirank == 0) then
            !    call GenLaplFun(FRC,fLfun)
            !    vlapl%vata = fLfun(vlapl%vata)
            !end if
            !call ModBCDist(FRC,vlapl,fn/mpisize,2,mpisize,1,vlapl)
            !vrho%vata = vrho%vata + vlapl%vata
        end if
        !write(*,*) 'in xcMain, before distmat_feval_Plus_1D.'
        !allocate(XCdensO%vata(size(XCdens%vata)))
        call distmat_feval_Plus_1D(FRC,XCdens,erho,XCdensO)
        XCdens = XCdensO
        !write(12,*) "XCdens after distmat_feval_Plus in xcMain."
        !write(12,*)  XCdens%vata
        !write(*,*) 'in xcMain, before distmat_feval_Plus_2D.'
        !allocate(VXCoutO%vata(size(VXCout%vata,1),size(VXCout%vata,2)))
        call distmat_feval_Plus_2D(FRC,VXCout,vrho,VXCoutO)
        !write(*,*) 'in xcMain, after distmat_feval_Plus_2D.'
        VXCout = VXCoutO
        !write(*,*) "VXCout after distmat_feval_Plus in xcMain."
        !do jj = 1,size(VXCout%vata,1),1
        !write(numb,"(i4.4)") mpirank
        !flax = 'XCout_p'//trim(numb)//'.txt'
        !cou  =  mpirank + 10
        !open(unit=cou,file=flax)
        !write(cou,*) VXCout%vata
        !close(cou)
        !stop

        !end do
        if (usevtau) then
            write(*,*) "Error in xcMain.f90. usetau can't be true now."
            stop
            !call InitDistArray(fn,2,rho%mblock,2,mpisize,1,vtau)
            !call distmat_feval_Plus(FRC,VKout,vtau,VKout)
        end if
    end do

    !stop
    
    if (FRC%symmetry%pointsymmetry) then
        !write(*,*) 'here in xcmain, symmetry.'
        allocate(sym_rec(size(FRC%symmetry%sym_rec,1),size(FRC%symmetry%sym_rec,2),size(FRC%symmetry%sym_rec,3)))
        sym_rec = FRC%symmetry%sym_rec
        allocate(sym_t(size(FRC%symmetry%sym_t,1),size(FRC%symmetry%sym_t,2)))
        sym_t   = FRC%symmetry%sym_t
        
        nvec    = FRC%domain%fgridn
        deallocate(erho%vata)
        allocate(erho%vata(product(nvec)))
        deallocate(vrho%vata)
        allocate(vrho%vata(product(nvec),2))
        call ModBCDist_1D(FRC,XCdens,XCdens%m,XCdens%n,mpisize,1,.FALSE.,.FALSE.,erho)
        call ModBCDist_2D(FRC,VXCout,VXCout%m,VXCout%n,mpisize,1,.FALSE.,.FALSE.,vrho)
        !write(*,*) "erho after ModBCDist"
        !write(*,*)  erho%vata
        !write(*,*) "vrho after ModBCDist"
        !do jj = 1,size(vrho%vata,1),1
        !    write(*,*)  vrho%vata(jj,:)
        !end do

        !write(numb,"(i4.4)") mpirank
        !flax = 'VXC_para'//trim(numb)//'.txt'
        !cou  =  mpirank + 10
        !open(unit=cou,file=flax)
        !write(cou,*) vrho%vata
        !close(cou)
        !stop

        if (mpirank == 0) then
            !allocate(erhovata(size(erho%vata)))
            !allocate(vrhovata(size(vrho%vata,1),size(vrho%vata,2)))
            call symmetrize_1D(FRC,erho%vata,nvec,sym_rec,sym_t)
            !write(*,*) "vrho%vata before symmetrize."
            !write(*,*)  vrho%vata(:,1)
            !close(12)
            !write(*,*) "stop in xcMain"
            !stop

            call symmetrize_2D(FRC,vrho%vata,nvec,sym_rec,sym_t)
            !erho%vata = erhovata
            !vrho%vata = vrhovata
            !write(numb,"(i4.4)") mpirank
            !flax = 'VXCoutF_p'//trim(numb)//'.txt'
            !cou  =  mpirank + 10
            !open(unit=cou,file=flax)
            !write(*,*) 'vrho%vata after symm'
            !write(*,*)  vrho%vata
            !write(cou,*) 'vrhovata'
            !write(cou,*)  vrhovata
            !close(cou)
            !stop
            !deallocate(erhovata,vrhovata)
           ! write(12,*) "erho%vata after symmetrize."
            !write(12,*)  erho%vata
            !write(*,*) "vrho%vata after symmetrize."
            !do jj = 1,size(vrho%vata,1),1
            !    write(*,*) vrho%vata
            !end do
            !write(*,*) "stop in xcMain."
            !stop
        end if

        !stop
        !write(*,*) 'here'
        !write(numb,"(i4.4)") mpirank
        !flax = 'XCout'//trim(numb)//'.txt'
        !cou  =  mpirank + 10
        !open(unit=cou,file=flax)
        !write(cou,*) vrho%vata
        !close(cou)
        !stop
        !write(*,*) 'after symmetry.'
        call ModBCDist_1D(FRC,erho,XCdens%mblock,1,mpisize,1,.FALSE.,.FALSE.,XCdens)
        call ModBCDist_2D(FRC,vrho,VXCout%mblock,1,mpisize,1,.FALSE.,.FALSE.,VXCout)

        !write(numb,"(i4.4)") mpirank
        !flax = 'FRCVXC_p'//trim(numb)//'.txt'
        !cou  =  mpirank + 10
        !open(unit=cou,file=flax)
        !write(cou,*) VXCout%vata
        !close(cou)
        !stop
        !write(12,*) "XCdens after symmetrize"
        !write(12,*)  XCdens%vata
        !write(*,*) "VXCout after symmetrize"
        !do jj = 1,size(VXCout%vata,1),1
        !    write(*,*)  VXCout%vata(jj,:)
        !end do


        if (usevtau) then
            !call ModBCDist(FRC,VKout,VKout%m,VKout%n,mpisize,1,vtau)
            !if (mpirank == 0) then
            !    call symmetrize(vtau%vata,nvec,sym_rec,sym_t,vtau%vata)
            !end if
            !call ModBCDist(FRC,vtau,VKout%mblock,1,mpisize,1,VKout)
        end if
    end if
    
    !ntmp = size(FRC%rho%output)
    !allocate(FRC%energy%XCdens(ntmp))
    FRC%energy%XCdens = XCdens
        
    if (ispin == 4) then
        
        write(*,*) "Error in xcMain.f90. General(non-collinear) spin is not available now."
        stop
        !call applyUnitary(FRC,U,VXCout,VXCout)
    end if
        
    !allocate(FRC%potential%VXCout(ntmp))
    FRC%potential%VXCout = VXCout
        
    if (usevtau) then
        !if (ispin == 4) then
        !    call applayUnitary(FRC,U,VKout,VKout)
        !end if
        !FRC%potential%vtau = VKout
    end if
        
    if (trim(FRC%info%calculationType) == "self-consistent" .and. usevtau) then
        write(*,*) "Error in xcMain.f90. usetau can't be true now."
        stop
        !FRC%energy%tau = tau
    end if
    !close(12)
    return
    end subroutine xcMain
    end module xcMain_module

        
    !module applyUnitary_module
    !contains
    !subroutine applyUnitary(FRC,U,vxcd,vxc)
    !
    !use FORTRAN_RESCU_CALCULATION_TYPE
   ! 
    !implicit none
    
    !! input variables
    !type(FORTRAN_RESCU_CALCULATION) :: FRC
    !
    !! temparory variables
    
    !! output variables
    
    !! body of this function
    !call distmat_zeroslike(FRC,vxcd,(/vxcd%m,4/))
    !Utmp    = U%dataE
    !vxcdtmp = vxcd%dataE
    
    !vxc%vata(:,1) = 0.5*sum(vxcd,2)
    !vxc%vata(:,4) = 0.5*sum(vxcd,2)
    !vxcdtmp2 = 0.5*(vxcdtmp(:,1)-vxcdtmp(:,2))
    !vxc%dataE(:,1) = vxc%dataE(:,1) + U(:,1)*vxcdtmp2
    !vxc%dataE(:,2) = U(:,2)*vxcd
    !call conj(vxc%data(:,2),vxc%dataE(:,3))
    !vxc%dataE(:,4) = vxc%dataE(:,4) - U(:,1)*vxcd
    
    !return
    !end subroutine applyUnitary
    !end module applyUnitary_module
    
