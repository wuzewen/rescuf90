! VataMN_CMPLX_2D.f90
    
!**************************************************************
    !
    ! This module will contain a kind of data type including
    ! two dimentional real array vata, and integers m, n, 
    ! mblock, nblock, mproc, nproc.
    !
!**************************************************************
    
    module VataMN_CMPLX_2D_module
    
    type :: VataMN_CMPLX_2D
        complex*16, allocatable :: vata(:,:)
        integer                 :: m
        integer                 :: n
        integer                 :: mblock
        integer                 :: nblock
        integer                 :: mproc
        integer                 :: nproc
    end type VataMN_CMPLX_2D
    
    end module VataMN_CMPLX_2D_module
