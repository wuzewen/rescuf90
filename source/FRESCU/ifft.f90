! ifft.f90

!********************************************************************
      ! This is an interfece for inversion fast fourier transform.
      ! Many subroutine included as follows:
      ! (*) 2D array   input;
      ! (*) 2D pointer input;
      ! (*) 1D pointer input, but serves as 2D array;
      ! (*) 3D array   input;
      ! (*) 3D pointer input;
      ! (*) 1D pointer input, but serves as 3D array;
!********************************************************************

      module ifft_module

              !use ifft2_fftw_module
              use ifft3_fftw_module
              use ifft3_fftw_pointer3D_module
              use ifft3_fftw_pointer_module

              implicit none

              interface ifft
                      !module procedure ifft2_fftw
                      module procedure ifft3_fftw
                      module procedure ifft3_fftw_pointer3D
                      module procedure ifft3_fftw_pointer
              end interface ifft

      end module ifft_module
