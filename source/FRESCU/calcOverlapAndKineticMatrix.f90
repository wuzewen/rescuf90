! calcOverlapAndKineticMatrix.f90

!**************************************************************************************
      !
      !
      !
!**************************************************************************************

      module calcOverlapAndKineticMatrixMod
      contains
      subroutine calcOverlapAndKineticMatrix(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize
      sprs    = FRC%LCAO%sprs
      buff    = min(FRC%option%bufferSize,2**26)
      avec    = FRC%domain%latvec
      smimb   = FRC%smi%mb
      sminb   = FRC%smi%nb
      smimp   = FRC%smi%mp
      sminp   = FRC%smi%np

      call GetAtomOrbitalInfo(FRC)

      evec    = FRC%LCAO%parameters%evec
      Lorb    = FRC%LCAO%parameters%Lorb
      Morb    = FRC%LCAO%parameters%Morb
      Oorb    = FRC%LCAO%parameters%Oorb
      Rorb    = FRC%LCAO%parameters%Rorb
      Sorb    = FRC%LCAO%parameters%Sorb
      norb    = size(Lorb)
      xyz     = FRC%atom%xyz(evec,:)
      lmax    = maxval(Lorb)
      call GengauntMatrix(lmax,guanmt)

      do ii = 1,size(FRC%ElementData),1
      do jj = 1,size(FRC%ElementData(ii)%Orbital),1
      do kk = 1,size(FRC%ElementData),1
      do ll = 1,size(FRC%ElementData(kk)%Orbital),1
          fqmat2(:,ii,jj,kk,ll) = FRC%ElemetDataOrbitalSet(jj)%fqData* &
                  FRC%ElemetDataOrbitalSet(ll)%fqData
      end do
      end do
      end do
      end do

      fqmat = reshape(fqmat2,(//))

      q    = FRC%ElementData(1)%OrbitalSet(1)%qqData
      w    = FRC%ElementData(2)%OrbitalSet(1)%qwData
      nto2 = size(FRC%LCAO%mDispMapVector,1)
      txyz = FRC%LCAO%mDispMapVector
      
      call ismemberRow(txyz,FRC%LCAO%sbVec,~,sbind)
      sbDisp = FRC%LCAO%sbDisp(sbind)
      call GetGlobalInd(mpirank,sbDisp(1)%vata,iloc,jloc)
      iRorb = Rorb(iloc)
      jRorb = Rorb(jloc)
      maxrad = 2.0D0*maxval(Rorb)*maxval(q)*1.05D0
      call linespace(0,maxrad,ceiling(maxrad/0.05D0),juvrad)
      allocate(juvval(2*lmax+1),size(juvrad))
      do Luv = 0,lmax,2
          call calcSphericalBessel(Luv,juvrad,juvval(Luv+1,:))
      end do

      sprsMat = (norb**2*16*3/mpisize .GT. buff)

      do ll = 1,nto2,1
          S = sbDisp(ll)
          T = S
          xyz1 = xyz(iloc,:)
          xyz2 = xyz(jloc,:)
          call bsxfunPlus(xyz2,matmul(txyz(ll,:),avec),xyz2)
          call calcLocalBooleanOverlap(xyz1,xyz2,iRorb,jRorb,sbDisp(ll)%vata,slog)
          if (sprsMat) then
                  S%vata
                  T%vata
          else
                  S%vata
                  T%vata
                  ztemp
          end if

          if (nnz(slog)) then
              call find(slog,fslogi,fslogj)
              blks = ceiling(buff/size(q)/8/4)
              kk = 1
              do while (kk .LE. size(fslogi))
                  if (kk + blks .GT. size(fslogi)) then
                      blks = size(fslogi) -kk
                  end if
                  slogi = fslogi(kk:kk+blks)
                  slogj = fslogj(kk:kk+blks)
                  rijmat = xyz1(slogi,:)-xyz2(slogj,:)
                  dijmat = sqrt(sum(rijmat**2,2))
                  do Luv = 1,2*lmax,1
                      lu = Lorb(iloc(slogi))
                      lv = Lorb(jloc(slogj))
                      mu = Morb(iloc(slogi))
                      mv = Morb(jloc(slogj))
                      slogtmp = (Luv .GE. abs(lu-lv) .AND. &
                              luv .LE. lu+lv .AND. Luv .GE. abs(mv-mu))
                      if (nnz(slogtmp)) then
                          lu  = lu(slogtmp)
                          lv  = lv(slogtmp)
                          mu  = mu(slogtmp)
                          mv  = mv(slogtmp)
                          rij = rijmat(slogtmp,:)
                          call sub2ind
                          gaunt = 
                          call getsphHarm(luv,mv-mu,rij)
                          gaunt = im**(lu-lv-Luv)*gaunt*sphHarm
                          rij = dijmat(slogtmp)
                          call inerp1
                          slogtmpi = slogi(slogtmp)
                          slogtmpj = slogj(slogtmp)
                          call sub2ind()
                          temp = fqmat2(Rq12,:)*juv
                          if (sprsMat) then
                              lind = 
                              S%vata = 
                              T%vata = 
                          else
                              call sub2ind()
                              ztemp = 
                              S%vata = 
                              ztemp = 
                              T%vata = 
                              ztemp = 0.0D0
                          end if
                      end if
                  end do
                  kk = kk + blks + 1
              end do
          end if

          if (sprsMat) then
              if (.NOT. nnz(slog)) then
                  call find(slog,fslogi,fslogj)
              end if
              S%vata = 
              T%vata = 
          end if
          call adjustSparsity(S%vata,sprs)
          call adjustSparsity(S%vata,sprs)
          Scell(ll) = S
          Tcell(ll) = T
      end do
      
      if (all(FRC%kpoint%kdirect .EQ. 0.0D0)) then
          spfrac = 0.0D0
          do ii = 1,size(Scell),1
              call distmatNNZ(FRC,Scell(ii),nonzero)
              spfrac = max(spfrac,nonzero/Scell(ii)%m/Scell(ii)%n)
          end do
          issp = spfrac .LT. 0.05D0 .AND. Scell(ii)%m/Scell(ii)%n .GT. 10**6
          if (issp) then
              call cellsum(Scell,isspn)
          else
              isspn = -1
          end if

          call reduceSymOper(FRC,Scell,txyz,(/0,0,0/),issp,tmp)
          Scell = 
          call reduceSymOper(FRC,Tcell,txyz,(/0,0,0/),issp,tmp)
          Tcell = 
          txyz  = 0.0D0
      end if

      if (mpistat) then
              call MPI_Barrier()
      end if

      do ii = 1,size(Scell),1
          call distmat_nnz(FRC,Scell(ii),nonzero)
          if (nonzeros .GT. 0) then
              tmp = Scell(ii)
              spfrac = nonzero/tmp%m/tmp%n
              issp = spfrac .LT. 0.05D0 .AND. tmp%m*tmp%n .GT. 10**6
              if (issp) then
                  tmp%vata = 
              end if

              call ModBCDist(FRC,tmp,smimb,sminb,1,mpisize,.TRUE.,issp,tmp)
              call sph2real(tmp%vata,Morb,'l',tmp%vata)
              call ModBCDist(FRC,tmp,smimb,sminb,1,mpisize,.TRUE.,issp,tmp)
              call sph2real(tmp%vata,Morb,'r',tmp%vata)
              tmp%vata = 4.0D0*pi*tmp%vata
              call croparray(tmp%vata,eps,tmp%vata)
              call ModBCDist(FRC,tmp,smimb,sminb,1,mpisize,.FALSE.,issp)
              call adjustSparsity(tmp%vata,sprs)
          else
              Scell(ii) = 
          end if
          call distmat_nnz(FRC,Tcell(ii),nonzero)
          if (nonzero .GT. 0) then
              tmp = Tcell(ii)
              spfrac = nonzero/tmp%m/tmp%n
              issp = spfrac .LT. 0.05D0 .AND. tmp%m*tmp%n .GT. 10**6
              if (issp) then
                      sparse
              end if
              call ModBCDist(FRC,tmp,smimb,sminb,1,mpisize,.TRUE.,issp)
              call sph2real(tmp%vata,Morb,'l')
              call ModBCDist(FRC,tmp,smimb,sminb,1,mpisize,.TRUE.,issp)
              call sph2real(tmp%vata,Morb,'r')
              tmp%vata = 2.0D0*pi*tmp%vata
              calll croparray(tmp%vata,eps)
              call ModBCDist(FRC,tmp,smimb,sminb,smimp,msinp,.FALSE.,issp)
              call adjustSparsity(tmp%vata,sprs)
              Tcell(ii) = tmp
          else
              Tcell(ii) = 
          end if
      end do

      FRC%LCAO%Scell = Scell
      FRC%LCAO%Tcell = Tcell
      FRC%LCAO%svec  = txyz

      return
      end subroutine calcOverlapAndKineticMatrix
      end module calcOverlapAndKineticMatrixMod

