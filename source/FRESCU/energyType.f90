! energyType.f90
    
!****************************************************************
    !
    ! This module contains Energy data during the calculation.
    !
!****************************************************************
    
    module energyType_module
    
    use VataMN_1D_module
    use RealVata_3D_module
    
    type :: energyType
        real*8  :: Efermi
        logical :: ewaldE
        type(VataMN_1D)                :: XCdens
        type(RealVata_3D)              :: ksnrg
        real*8, allocatable            :: Eks(:)
        real*8, allocatable            :: Etot(:)
        real*8, allocatable            :: EdH(:)
        real*8, allocatable            :: nocc(:,:,:)
        real*8, allocatable            :: entropy(:,:)
    end type energyType
    
    end module energyType_module
