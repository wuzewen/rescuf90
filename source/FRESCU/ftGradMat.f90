! ftGradMat.f90
!**************************************************************************
    ! 
    ! This function is used to generate Gradient with FFT method.
    !
!**************************************************************************
    
    module ftGradMat_module
    contains
    subroutine ftGradMat(n,Gu,Gv,Gw)
    
    use fftfreq_module
    use fft2_module
    
    implicit none
    
    ! input valuables
    integer                  :: n(3)
    
    ! temporery valuables
    integer                  :: i
    complex*16               :: imnu, pi
    integer                  :: ntmp(3)
    real*8                   :: bvec(3,3)
    complex*16, allocatable  :: aa(:,:), bb(:,:), cc(:,:)
    real*8, allocatable      :: ku1(:,:,:), kv1(:,:,:), kw1(:,:,:), &
                                ku2(:,:,:), kv2(:,:,:), kw2(:,:,:), &
                                ku3(:,:,:), kv3(:,:,:), kw3(:,:,:)
    
    ! output valuables
    real*8, allocatable      :: Gu(:,:), Gv(:,:), Gw(:,:)
    complex*16, allocatable  :: Gutmp(:,:), Gvtmp(:,:), Gwtmp(:,:)
    
    ! body of this function
    imnu    = (0.0D0,1.0D0)
    pi      = (3.1415926535897932385D0,0.0D0)
    ntmp(2) = 1
    ntmp(3) = 1
    bvec    = 0.0D0
    forall(i=1:3)
        bvec(i,i) = 1.0D0
    end forall
    
    ntmp(1) = n(1)
    allocate(ku1(ntmp(1),ntmp(2),ntmp(3)))
    allocate(kv1(ntmp(1),ntmp(2),ntmp(3)))
    allocate(kw1(ntmp(1),ntmp(2),ntmp(3)))
    call fftFreq(ntmp,bvec,ku1,kv1,kw1)
    !allocate(Gu(n(1),n(1)))
    Gu  = 0.0D0
    forall(i=1:n(1))
        Gu(i,i) = 1.0D0
    end forall
    allocate(Gutmp(n(1),n(1)))
    call fft2(Gu,2,Gutmp)
    allocate(aa(n(1),n(1)))
    aa(:,:) = (0.0D0,0.0D0)
    forall(i=1:n(1))
        aa(i,i) = 2.0D0*imnu*dcmplx(pi)*dcmplx(ku1(i,1,1))/dble(n(1))
    end forall
    Gu = dble(matmul(conjg(transpose(Gutmp)),matmul(aa,Gutmp)))
    
    ntmp(1) = n(2)
    allocate(ku2(ntmp(1),ntmp(2),ntmp(3)))
    allocate(kv2(ntmp(1),ntmp(2),ntmp(3)))
    allocate(kw2(ntmp(1),ntmp(2),ntmp(3)))
    call fftFreq(ntmp,bvec,ku2,kv2,kw2)
    !allocate(Gv(n(2),n(2)))
    Gv  = 0.0D0
    forall(i=1:n(2))
        Gv(i,i) = 1.0D0
    end forall
    allocate(Gvtmp(n(2),n(2)))
    call fft2(Gv,2,Gvtmp)
    allocate(bb(n(2),n(2)))
    bb(:,:) = (0.0D0,0.0D0)
    forall(i=1:n(2))
        bb(i,i) = 2.0D0*imnu*pi*dcmplx(ku2(i,1,1))/dble(n(2))
    end forall
    Gv = dble(matmul(conjg(transpose(Gvtmp)),matmul(bb,Gvtmp)))
    
    ntmp(1) = n(3)
    allocate(ku3(ntmp(1),ntmp(2),ntmp(3)))
    allocate(kv3(ntmp(1),ntmp(2),ntmp(3)))
    allocate(kw3(ntmp(1),ntmp(2),ntmp(3)))
    call fftFreq(ntmp,bvec,ku3,kv3,kw3)
    !allocate(Gw(n(3),n(3)))
    Gw  = 0.0D0
    forall(i=1:n(3))
        Gw(i,i) = 1.0D0
    end forall
    allocate(Gwtmp(n(3),n(3)))
    call fft2(Gw,2,Gwtmp)
    allocate(cc(n(3),n(3)))
    cc(:,:) = (0.0D0,0.0D0)
    forall(i=1:n(3))
        cc(i,i) = 2.0D0*imnu*pi*dcmplx(ku3(i,1,1))/dble(n(3))
    end forall
    Gw = dble(matmul(conjg(transpose(Gwtmp)),matmul(cc,Gwtmp)))
    
    deallocate(aa,bb,cc)
    deallocate(ku1,kv1,kw1)
    deallocate(ku2,kv2,kw2)
    deallocate(ku3,kv3,kw3)
    deallocate(Gutmp,Gvtmp,Gwtmp)

    return
    end subroutine ftGradMat
    end module ftGradMat_module
