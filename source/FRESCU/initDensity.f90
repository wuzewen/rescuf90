! initDensity.f90
!********************************************
    !
    ! This is a function to init density
    !
!********************************************
    
    module initDensity_module
    contains
    subroutine initDensity(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType)              :: inputFromFile
    integer                              :: nline
    
    ! temporary variables
    integer :: ii
    logical :: rhoinputfileIsFind
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%density) then
        ii = 0
        nline = inputFromFile%NumberOfInput
        rhoinputfileIsFind = .FALSE.

        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("rhoin")) then
                rhoinputfileIsFind = .TRUE.
                exit
            end if
        end do
        if (rhoinputfileIsFind) then
            FRC%rho%inputfile = inputFromFile%NAV(ii)%value
            !call readRHOfile(FRC)
        end if
        
        if (trim(FRC%info%calculationType) == trim("band-structure") .or. &
                trim(FRC%info%calculationType) == trim("DOS") .or. &
                trim(FRC%info%calculationType) == trim("DFPT")) then
            if (.not. rhoinputfileIsFind) then
                write(*,*) "error"
            end if
        end if
        
        FRC%init%density = .TRUE.
    end if
    
    return
    end subroutine initDensity
    end module initDensity_module
                
