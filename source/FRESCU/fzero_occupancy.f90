! fzero_occupancy.f90
    
!***********************************************************************************************
    !
    !
    !
!***********************************************************************************************
    
    module fzero_occupancy_module
    contains
    subroutine fzero_occupancy(sigma,kweight,energy,ntot,mu0,mu)
    
    use fermiDirac_3D_module
    
    implicit none
    
    ! input variables
    real*8              :: sigma, ntot, mu0
    real*8, allocatable :: kweight(:), energy(:,:,:)
    
    ! temporary variables
    real*8, allocatable :: focc(:,:,:), foccT(:)
    real*8              :: init0, init1, init2, init3, tmp1, tmp2, mut, h
    real*8              :: ef0, ef02, ef1, ef2, ef3, ef4, initD, initM, initX
    real*8              :: initD2, initM2, initX2, h2, deltaF, deltaF2
    integer             :: ii, ntmp1, ntmp2
    
    ! output variables
    real*8 :: mu
    
    ! body of this function
    !write(*,*) "I'm here at the begining of fzero_occupancy."
    call fermiDirac_3D(sigma,mu0,energy,focc)
    !write(*,*) "sigma = ", sigma
    !write(*,*) "energy after fermiDirac_3D in fzero_occupancy."
    !do ii = 1,size(energy,1),1
    !    write(*,*)  energy(ii,:,1)
    !end do
    !write(*,*) "focc after fermiDirac_3D in fzero_occupancy."
    !do ii = 1,size(focc,1),1
    !    write(*,*)  focc(ii,:,1)
    !end do
    !write(*,*) "size of energy"
    !write(*,*)  size(energy)
    !write(*,*) "I'm here after fermiDirac in fzero_occupancy."
    allocate(foccT(size(focc,2)))
    !write(*,*) "I'm here before foccT in fzero_occupancy."
    foccT = sum(sum(focc,3),1)
    !write(*,*) "I'm here after foccT in fzero_occupancy."
    !write(*,*)  foccT
    !write(*,*) "size of foccT, kweight."
    !write(*,*)  size(foccT), size(kweight)
    deallocate(focc)
    !write(*,*) "kweight in fzero_occu."
    !write(*,*)  kweight
    init0 = sum(foccT*kweight)-ntot
    init1      = init0
    !write(*,*) "init0 init1 ="
    !write(*,*)  init0,init1
    if (init1 > 0) then
        init2      = 1.0D0
        init3      = 1.0D0
    else if (init1 == 0) then
        mu = mu0
        return
    else
        init2      = -1.0D0
        init3      = -1.0D0
    end if
    !write(*,*) "I'm here before h in fzero_occupancy."   
    h          = 0.10D0
    h2         = 0.000001D0
    mu         = mu0
    mut        = mu
    tmp1       = init0*init2
    tmp2       = init1*init3
    ntmp1      = 0
    ntmp2      = 0
    do while (tmp1 > 0.0 .and. tmp2 > 0.0)
        mu = mu+h
        ntmp1 = ntmp1+1
        call fermiDirac_3D(sigma,mu,energy,focc)
        !write(*,*) "I'm here after fermiDirac_3D in the loop."
        foccT = sum(sum(focc,3),1)
        deallocate(focc)
        !write(*,*) "foccT in fermiDirac_3D"
        !write(*,*)  foccT
        init2 = sum(foccT*kweight)-ntot
        tmp1  = init0*init2
        if (tmp1 <= 0.0) then
                ef1    = mu-h
                ef2    = mu
                initD  = init2
                initX  = init0
                deltaF = h
                do while (deltaF > h2)
                    ef0  = (ef1+ef2)/2.0
                    ntmp2 = ntmp2+1
                    call fermiDirac_3D(sigma,ef0,energy,focc)
                    foccT = sum(sum(focc,3),1)
                    deallocate(focc)
                    initM = sum(foccT*kweight)-ntot
                    if (initX*initM <= 0.0) then
                            ef1   = ef1
                            ef2   = ef0
                            initD = initM
                            !ef0 = (ef1+ef2)/2.0
                    else if (initM*initD <= 0.0) then
                            ef1 = ef0
                            ef2 = ef2
                            initX = initM
                            !ef0 = (ef1+ef2)/2.0
                    end if
                    deltaF = deltaF/2.0D0
                end do
            mu = (ef1+ef2)/2.0D0
            !mu = mu-h/2.0
            exit
        end if
        init0      = init2
        
        mut = mut-h
        call fermiDirac_3D(sigma,mut,energy,focc)
        foccT = sum(sum(focc,3),1)
        deallocate(focc)
        init3 = sum(foccT*kweight)-ntot
        tmp2  = init1*init3
        if (tmp2 <= 0) then
                ef3 = mut
                ef4 = mut+h
                initX2 = init3
                initD2 = init1
                deltaF2 = h
                do while (deltaF2 > h2)
                    ntmp2 = ntmp2+1
                    ef02 = (ef3+ef4)/2.0D0
                    call fermiDirac_3D(sigma,ef02,energy,focc)
                    foccT = sum(sum(focc,3),1)
                    deallocate(focc)
                    initM2 = sum(foccT*kweight)-ntot
                    if (initX2*initM2 <= 0.0) then
                            ef3    = ef3
                            ef4    = ef02
                            initD2 = initM2
                    else if (initM2*initD2 <= 0.0) then
                            ef3    = ef02
                            ef4    = ef4
                            initX2 = initM2
                    end if
                    deltaF2 = deltaF2/2.0D0
                end do
            mu = (ef3+ef4)/2.0D0
            !mu = mut+h/2.0
            exit
        end if
        init1      = init3
        !write(*,*) "init0 = init1 = "
        !write(*,*)  init0,  init1
        !if (init1 > 100 .and. init3 > 100) then
        !    mu = 0
        !    exit
        !end if
    end do
    !write(*,*) "number of steps 1 ="
    !write(*,*)  ntmp1
    !write(*,*) "number of steps 2 ="
    !write(*,*)  ntmp2
    
    return
    end subroutine fzero_occupancy
    end module fzero_occupancy_module
