! genVnlOp_Sparse.f90
    
!*******************************************************************
    !
    !
    !
!*******************************************************************
    
    module genVnlOp_Sparse_module
    contains
    subroutine genVnlOp_Sparse(FRC,gpu,X,vnl)
    
    !external ZGEMM
    use FORTRAN_RESCU_CALCULATION_TYPE
    use bsxfunTimes_CMPLX_module
    use SparseProduct_module

    !external ZGEMM

    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    logical :: gpu
    complex*16, allocatable :: X(:,:)
    
    ! temporary variables
    !integer :: ii
    logical :: conj  
    real*8  :: avec(3,3), dr
    complex*16, allocatable :: kbedr(:,:)
    complex*16, allocatable :: chilm(:,:)
    !character(len=1) :: transa,transb
    integer          :: m, n, k, lda, ldb, ldc
    !complex*16           :: alpha, beta
    !complex*16, allocatable :: chilmtmp2(:,:), vnl2(:,:)
    ! output variabels
    complex*16, allocatable :: vnl(:,:)
    
    ! body of this function
    !call det(FRC%domain%latvec,dr)
    avec = FRC%domain%latvec
    dr   = avec(1,1)*avec(2,2)*avec(3,3)&
          +avec(1,2)*avec(2,3)*avec(3,1)&
          +avec(1,3)*avec(2,1)*avec(3,2)&
          -avec(1,3)*avec(2,2)*avec(3,1)&
          -avec(1,1)*avec(2,3)*avec(3,2)&
          -avec(1,2)*avec(2,1)*avec(3,3)
    if (FRC%interpolation%vnl) then
        dr = dr/dble(product(FRC%domain%fgridn))
    else
        dr = dr/dble(product(FRC%domain%cgridn))
    end if
    allocate(kbedr(size(FRC%potential%vnl%KBEnergy),1))
    kbedr(:,1) = dcmplx(FRC%potential%vnl%KBEnergy(:)*dr)
    
    if (gpu) then
            write(*,*) "Error in genVnlOp_Sparse. GPU problem."
        !call gpuArray(kbedr)
        !call gpuArray(chilm)
        !chilmtmp = matmul(transpose(chilm),X)
        !call bsxfunTimes(chilmtmp,kbedr,vnl)
        !vnl = matmul(chilm,vnl)
        !if (FRC%interpolation%vnl) then
            !call initFun(vnl,X,vnltmp)
            !call antFun(vnltmp,X,vnl)
        ! end if
    else
        conj = .TRUE.
        call SparseProduct(FRC%potential%vnl%sparseKBorb,X,conj,chilm)
        !write(*,*) "size of chilm", size(chilm,1), size(chilm,2)
        !write(*,*) "chilm ="
        !do m = 1,size(chilm,2),1
        !    do n = 1,size(chilm,1),1
        !        write(*,*) n, m, chilm(n,m)
        !    end do
        !end do 
        !write(*,*) "sizes of before bsxfunTimes"
        !write(*,*)  size(chilm,1), size(chilm,2),size(kbedr,1),size(kbedr,2)
        call bsxfunTimes_CMPLX(chilm,kbedr,chilm)
        !write(*,*) "after bsx"
        conj = .FALSE.
        call SparseProduct(FRC%potential%vnl%sparseKBorb,chilm,conj,Vnl)
        !write(*,*) "size of Vnl", size(Vnl,1), size(Vnl,2)
        !write(*,*) "Vnl ="
        !do m = 1,size(vnl,2),1
        !do n = 1,size(vnl,1),1
        !write(*,*) n, m, vnl(n,m)
        !end do
        !end do
        if (FRC%interpolation%vnl) then
            write(*,*) "Error in genVnlOp.f90. FRC%interpolation%vnl should be false now."
            stop
            !call initFun(vnl,X,vnltmp)
            !call antFun(vnltmp,X,vnl)
        end if
    end if
    
    return
    end subroutine genVnlOp_Sparse
    end module genVnlOp_Sparse_module
