! clebschgordan.f90
    
!********************************************************************
    !
    !
    !
!********************************************************************
    
    module clebschgordan_module
    contains
    subroutine clebschgordan(j1,j2,j,m1,m2,m,C)
    
    use factorial_module
    
    implicit none
    
    ! input variables
    integer, allocatable :: j1(:), j2(:), j(:), m1(:), m2(:), m(:)
    
    ! temporary variables
    logical, allocatable :: ibad(:), inde(:)
    integer, allocatable :: j1tmp(:), j2tmp(:), jtmp(:), m1tmp(:), m2tmp(:), mtmp(:), k0(:), k1(:)
    integer :: ii, vdim, ntmp, mm, gtmp
    
    ! output variables
    integer, allocatable :: C(:)
    
    ! body of this function
    vdim = size(j1)
    
    allocate(C(vdim))
    allocate(ibad(vdim))
    ibad = .FALSE.
    do ii = 1,vdim,1
        if (j1(ii) .LT. 0   .or.  j2(ii) .LT. 0   .or. j(ii) .LT. 0  .or. &
            mod(2*j1(ii),1) .NE. 0  .or. mod(2*j2(ii),1) .NE. 0  .or. &
            mod(2*j(ii),1) .NE. 0  .or. &
            mod(2*m1(ii),1) .NE. 0  .or. mod(2*m2(ii),1) .NE. 0  .or. &
            mod(2*m(ii),1) .NE. 0  .or. &
            abs(m1(ii)) .GT. j1(ii) .or. abs(m2(ii)) .GT. j2(ii) .or. &
            abs(m(ii)) .GT. j(ii)  .or. &
            j1(ii)+m1(ii) .LT. 0    .or. j2(ii)+m2(ii) .LT. 0    .or. &
            j(ii)+m(ii) .LT. 0     .or. & 
            j1(ii)+j2(ii)+j(ii) .LT. 0 .or.  mod(j1(ii)+m1(ii),1) .NE. 0 .or. & 
            mod(j2(ii)+m2(ii),1) .NE. 0 .or. mod(j(ii)+m(ii),1) .NE. 0 .or. &
            mod(j1(ii)+j2(ii)+j(ii),1) .NE. 0) then
            ibad(ii) = .TRUE.
            C(ii)    = 0
        end if
    end do
    
    allocate(inde(vdim))
    inde = .FALSE.
    do ii = 1,vdim,1
        if ((m1(ii)+m2(ii)-m(ii) .NE. 0 .or. j(ii) .LT. abs(j1(ii)-j2(ii)) .or. &
                j(ii) .GT. j1(ii)+j2(ii)) .and. (.not. ibad(ii))) then
            inde(ii) = .TRUE.
            C(ii)    = 0
        end if
    end do
    
    do ii = 1,vdim,1
        if ((.not. inde(ii)) .and. (.not. ibad(ii))) then
            inde     = .TRUE.
        else
            inde(ii) = .FALSE.
        end if
    end do
    
    ntmp = count(inde .EQV. .TRUE.)
    allocate(j1tmp(ntmp))
    allocate(j2tmp(ntmp))
    allocate(jtmp(ntmp))
    allocate(m1tmp(ntmp))
    allocate(m2tmp(ntmp))
    allocate(mtmp(ntmp))
    allocate(k0(ntmp))
    allocate(k1(ntmp))
    
    forall(ii=1:ntmp)
        k0(ii) = max(0,j2tmp(ii)-jtmp(ii)-m1tmp(ii),j1tmp(ii)-jtmp(ii)+m2tmp(ii))
        k1(ii) = min(j1tmp(ii)+j2tmp(ii)-jtmp(ii),j1tmp(ii)-m1tmp(ii),j2tmp(ii)+m2tmp(ii))
    end forall
    
    !allocate(itmp(ntmp))
    do while (any(k0 .LE. k1))
        do ii = 1,ntmp,1
            if (k0(ii) .LE. k1(ii)) then
                mm = 1
                call factorial(k0(ii),gtmp)
                mm = mm*gtmp
                call factorial(j1tmp(ii)+j2tmp(ii)-jtmp(ii)-k0(ii),gtmp)
                mm = mm*gtmp
                call factorial(j1tmp(ii)-m1tmp(ii)-k0(ii),gtmp)
                mm = mm*gtmp
                call factorial(j2tmp(ii)+m2tmp(ii)-k0(ii),gtmp)
                mm = mm*gtmp
                call factorial(jtmp(ii)-j2tmp(ii)+m1tmp(ii)+k0(ii),gtmp)
                mm = mm*gtmp
                call factorial(jtmp(ii)-j1tmp(ii)-m2tmp(ii)+k0(ii),gtmp)
                mm = mm*gtmp
                C(ii) = C(ii) + ((-1)**k0(ii))/mm
                k0    = k0+1
                !itmp(ii) = .TRUE.
            else
                !itmp(ii) = .FALSE.
            end if
        end do
    end do
    
    ntmp = 0
    do ii = 1,vdim,1
        if (inde(ii)) then
            ntmp = ntmp+1
            mm   = 1
            call factorial(jtmp(ntmp)+j1tmp(ntmp)-j2tmp(ntmp),gtmp)
            mm = mm*gtmp
            call factorial(jtmp(ntmp)+j2tmp(ntmp)-j1tmp(ntmp),gtmp)
            mm = mm*gtmp
            call factorial(j1tmp(ntmp)+j2tmp(ntmp)-jtmp(ntmp),gtmp)
            mm = mm*gtmp
            call factorial(j1tmp(ntmp)+j2tmp(ntmp)+jtmp(ntmp)+1,gtmp)
            mm = mm/gtmp
            C(ii) = C(ii)*((2*jtmp(ntmp)+1)*mm)**0.5
            mm = 1
            call factorial(jtmp(ntmp)+mtmp(ntmp),gtmp)
            mm = mm*gtmp
            call factorial(jtmp(ntmp)-mtmp(ntmp),gtmp)
            mm = mm*gtmp
            call factorial(j1tmp(ntmp)+m1tmp(ntmp),gtmp)
            mm = mm*gtmp
            call factorial(j1tmp(ntmp)-m1tmp(ntmp),gtmp)
            mm = mm*gtmp
            call factorial(j2tmp(ntmp)+m2tmp(ntmp),gtmp)
            mm = mm*gtmp
            call factorial(j2tmp(ntmp)-m2tmp(ntmp),gtmp)
            mm = mm*gtmp
            C(ii) = C(ii)*(mm**0.5)
        end if
    end do
    
    return
    end subroutine clebschgordan
    end module clebschgordan_module
