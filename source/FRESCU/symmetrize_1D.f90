! symmetrize_1D.f90
    
    module symmetrize_1D_module
    contains
    subroutine symmetrize_1D(FRC,vector,nvec,symrec,symt)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use upGradeGrid_module
    use fftFreqA_module
    use NDgrid_module
    use bsxfunMod_module
    use fft3_module
    use fft3_fftw_module
    use ifft3_fftw_module
    use ifft3_module
    use bsxfunTimes_module
    use bsxfunTimes_CMPLX_module
    
    implicit none
    
    ! input valuables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer              :: nvec(3)
    real*8 , allocatable :: vector(:)
    !real*8 , allocatable :: hartreePotential(:,:,:)
    real*8 , allocatable :: symrec(:,:,:)
    real*8 , allocatable :: symt(:,:)
    
    ! temporery valuables
    integer              :: ii, jj!, kk
    !integer              :: nblk,
    integer              :: ngrid, nsym!, nxtmp1, nytmp1, nztmp1, nxtmp2, nytmp2, nztmp2
    integer              :: nv2(3)!, nv(3), cnx, cny, cnz, fnx, fny, fnz, nxtmp, nytmp, nztmp
    integer              :: ng2, nva, tmp(3,1)
    real*8               :: eyem(3,3), pi
    integer, allocatable :: ndx(:), ndy(:), ndz(:)
    integer, allocatable :: gd(:,:), gd1(:,:)!, symop(:,:,:)
    integer, allocatable :: inde(:,:)
    !real*8 , allocatable :: v_rec(:,:,:), t(:,:), symtmp(:), symtmpt(:,:,:)
    real*8 , allocatable :: vrec(:,:,:), realtmp(:)
    real*8 , allocatable :: fftfreq(:,:), ttmp(:,:)
    real*8 , allocatable :: ndxr(:), ndyr(:), ndzr(:), gdt(:,:,:), dex(:,:,:), phase(:,:,:)
    real*8 , allocatable :: gd_tmp(:,:), gd_tmp2(:,:), nvr(:,:)
    complex*16              :: imnu
    complex*16, allocatable :: vrec_c(:,:,:), v_rec_c(:,:,:), symvtr(:,:,:), v_rectm(:)
    complex*16, allocatable :: phase0(:,:), v_rectm2(:,:), tmpt(:,:), symvtmp(:), symvt(:,:,:)
    
    ! output valuables
    real*8, allocatable  :: symv(:)
    
    ! body of this function
    
    !********************************************
    !open(unit=13,file='symmetrize.txt')
    !write(13,*) "*************************** input of symmetrize_1D ****************************"
    !write(13,*) "nvec"
    !write(13,*)  nvec
    !write(13,*) "vector"
    !write(13,*)  vector
    !write(13,*) "symrec"
    !write(13,*)  symrec
    !write(13,*) "symt"
    !write(13,*)  symt



    pi    = 3.1415926535897932385D0
    ngrid = size(vector)
    !nblk  = size(vector,2)
    nsym  = size(symrec,3)
    imnu  = (0.0D0,1.0D0)
    allocate(realtmp(size(symt,2)))
    call upGradeGrid(nvec,symrec,nv2)
    ng2 = product(nv2)
    !write(13,*) "nv2"
    !write(13,*)  nv2
    !call eye(3,eyem)
    forall(ii=1:3)
        eyem(ii,ii) = 1.0D0
    end forall
    
    call fftFreqA(nv2,eyem,fftfreq)
    
    !write(13,*) "fftfreq in symmetrize_1D."
    !write(13,*)  fftfreq
    
    allocate(ndx(nv2(1)),ndy(nv2(2)),ndz(nv2(3)))
    forall(ii=1:nv2(1))
        ndx(ii) = ii-1
    end forall
    forall(ii=1:nv2(2))
        ndy(ii) = ii-1
    end forall
    forall(ii=1:nv2(3))
        ndz(ii) = ii-1
    end forall
    
    !call arrayN(0,nv2(1)-1,ndx)
    !call arrayN(0,nv2(2)-1,ndy)
    !call arrayN(0,nv2(3)-1,ndz)
    allocate(ndxr(nv2(1)),ndyr(nv2(2)),ndzr(nv2(3)))
    ndxr = dble(ndx)
    ndyr = dble(ndy)
    ndzr = dble(ndz)
    allocate(gdt(nv2(1),nv2(2),nv2(3)),dex(nv2(1),nv2(2),nv2(3)),phase(nv2(1),nv2(2),nv2(3)))
    call ndgrid(ndxr,ndyr,ndzr,gdt,dex,phase)
    !write(13,*) "phase"
    !write(13,*)  phase
    nva = nv2(1)*nv2(2)*nv2(3)
    allocate(gd(nva,3))
    gd(:,1) = reshape(gdt,(/nva/))
    gd(:,2) = reshape(dex,(/nva/))
    gd(:,3) = reshape(phase,(/nva/))
    !write(13,*) "gd in symmetrize_1D."
    !write(13,*)  gd
    allocate(vrec(nvec(1),nvec(2),nvec(3)))
    vrec = reshape(vector,(/nvec(1),nvec(2),nvec(3)/))
    vrec = cshift(vrec,-1,1)
    vrec = cshift(vrec,-1,2)
    vrec = cshift(vrec,-1,3)
    !allocate(vrec_c(nvec(1),nvec(2),nvec(3)))
    !allocate(v_rec_c(nvec(1),nvec(2),nvec(3)))
    !vrec_c  = dcmplx(vrec)
    FRC%Arr = dcmplx(vrec)
    !v_rec_c = cmplx(v_rec)
    !call fft3(vrec_c,3,v_rec_c)
    !call fft3(v_rec_c,2,vrec_c)
    !call fft3(vrec_c,1,v_rec_c)
    !call fft3_fftw(nvec(1),nvec(2),nvec(3),vrec_c)
    call fft3_fftw(FRC)
    !call fft3_fftw(FRC,vrec_c)
    !write(13,*) "v_rec_c in symmetrize_1D."
    !write(13,*)  v_rec_c
    if (.not. all(nv2 == nvec)) then
        write(*,*) "Error in symmetrize_1D.f90. nv2 should equal to nvec."
        stop
        !cnx    = nvec(1)
        !cny    = nvec(2)
        !cnz    = nvec(3)
        !fnx    = nv2(1)
        !fny    = nv2(2)
        !fnz    = nv2(3)
        !allocate(symv(nv2(1),nv2(2),nv2(3),nblk))
        !symv   = 0
        !nxtmp1 = cnx/2+1
        !nytmp1 = cny/2+1
        !nztmp1 = cnz/2+1
        !forall(ii=1:nxtmp,jj=1:nytmp,kk=1:nztmp)
        !    symv(ii) = v_rec(ii,jj,kk,:)
        !end forall
        !nxtmp2 = fnx-(cnx-cnx/2-1)
        !nytmp2 = fny-(cny-cny/2-1)
        !nztmp2 = fnz-(cnz-cnz/2-1)
        !forall(ii=nxtmp2:fnx,jj=nytmp2:fny,kk=nztmp2:fnz)
        !    symv(ii,jj,kk,:) = v_rec(ii-nxtmp1,jj-nytmp1,kk-nztmp1,:)
        !end forall
        
        !symvtmp = reshape(symv,(/ng2,nblk/))
        !v_rectm = symvtmp
        !symv    = 0
    else
        allocate(v_rectm(nva))
        v_rectm = reshape(dcmplx(FRC%arr),(/ng2/))
       !v_rectm = reshape(dcmplx(vrec_c),(/ng2/))
        allocate(symvtmp(ng2))
        symvtmp = (0.0D0,0.0D0)
    end if
    !write(13,*) "v_rectm in symmetrize_1D."
    !write(13,*)  v_rectm

    allocate(gd1(size(symrec,1),size(symrec,2)))
    allocate(gd_tmp(size(gd,1),size(gd1,2)))
    allocate(nvr(1,3))
    nvr(1,:) = dble(nv2(:))
    allocate(gd_tmp2(size(gd,1),size(gd1,2)))
    allocate(phase0(nva,1))
    allocate(ttmp(3,1))
    allocate(v_rectm2(size(v_rectm),1))
    v_rectm2(:,1) = v_rectm
    !write(13,*) "v_rectm2 in symmetrize_1D."
    !write(13,*)  v_rectm2
    allocate(tmpt(nva,1))
    allocate(inde(nva,1))
    do ii = 1,nsym,1
        !write(13,*) "#################################################################################"
        !write(13,*) "loop", ii, "in symmetrize_1D."
        gd1(:,:) = symrec(:,:,ii)
        !write(13,*) "symrec(:,:,ii)"
        !write(13,*)  symrec(:,:,ii)
        gd_tmp   = matmul(gd,gd1)
        !write(13,*) "gd_tmp = "
        !write(13,*)  gd_tmp
        call bsxfunMod(gd_tmp,nvr,gd_tmp2)
        !write(13,*) "gd_tmp2"
        !write(13,*)  gd_tmp2
        tmp(1,1) = 1
        tmp(2,1) = nv2(1)
        tmp(3,1) = nv2(1)*nv2(2)
        inde = matmul(gd_tmp2,tmp)+1
        !write(13,*) "inde in symmetrize_1D"
        !do jj = 1,size(inde),1    
        !write(13,*)  jj, inde(jj,1)
        !end do
        realtmp = symt(ii,:) 
        if (any(realtmp /= 0)) then
            !write(13,*) "loop", ii, "symt /= 0"
            ttmp(:,1) = symt(ii,:)
            phase0 = exp(dcmplx(2.0)*imnu*dcmplx(pi)*dcmplx(matmul(fftfreq,ttmp)))
            !write(13,*) "phase0 in symmetrize."
            !do jj = 1,size(phase0,1),1
            !    write(13,*)  jj, phase0(jj,1)
            !end do
            v_rectm2(:,1) = v_rectm(:)
            tmpt = v_rectm2*phase0
            !call bsxfunTimes_CMPLX(v_rectm2,phase0,tmpt)
            !write(13,*) "tmpt in symmetrize."
            !do jj = 1,size(tmpt,1),1
            !    write(13,*)  jj, tmpt(jj,1)
            !end do
            do jj = 1,size(inde,1)
                symvtmp(jj)  = symvtmp(jj) + tmpt(inde(jj,1),1)
            end do
        else
            !write(13,*) "loop", ii, "symt == 0"
            do jj = 1,size(inde,1)
                symvtmp(jj)  = symvtmp(jj) + v_rectm(inde(jj,1))
            end do
        end if
        !write(13,*) "symvtmp = "
        !do jj = 1,size(symvtmp),1
        !    write(13,*)  jj, symvtmp(jj)
        !end do
    end do
    symvtmp  = symvtmp/dble(nsym)
    !write(13,*) "symvtmp"
    !do jj = 1,size(symvtmp),1
    !    write(13,*)  jj, symvtmp(jj)
    !end do
    !allocate(symvt(nv2(1),nv2(2),nv2(3)))
    FRC%Arr = reshape(symvtmp,nv2)
    !symvt = reshape(symvtmp,nv2)
    !write(13,*) "symvt in symmetrize_1D."
    !do ii = 1,nv2(3),1
    !    write(13,*) "kk =", ii
    !    do jj = 1,nv2(1),1
    !        write(13,*)  symvt(jj,:,ii)
    !    end do
    !end do
    
    if (.not. all(nv2 == nvec)) then
        !allocate(symvt(nv2(1),nv2(2),nv2(3)))
        !symvt  = 0
        !nxtmp1 = cnx/2+1
        !nytmp1 = cny/2+1
        !nztmp1 = cnz/2+1
        !forall(ii=1:nxtmp,jj=1:nytmp,kk=1:nztmp)
        !    symvt(ii,jj,kk) = v_rec(ii,jj,kk)
        !end forall
        !nxtmp2 = fnx-(cnx-cnx/2-1)
        !nytmp2 = fny-(cny-cny/2-1)
        !nztmp2 = fnz-(cnz-cnz/2-1)
        !forall(ii=nxtmp:fnx,jj=nytmp:fny,kk=nztmp:fnz)
        !    symvt(ii,jj,kk) = v_rec(ii-nxtmp1,jj-nytmp1,kk-nztmp1)
        !end forall
    end if
    !allocate(symvtr(nv2(1),nv2(2),nv2(3)))
    !allocate(symtmpt(nv2(1),nv2(2),nv2(3)))
    !symvtr = cmplx(symvt)
    !call ifft3(symvt,3,symvtr)
    !call ifft3(symvtr,2,symvt)
    !call ifft3(symvt,1,symvtr)
    call ifft3_fftw(FRC)
    !call ifft3_fftw(FRC,symvt)
    !call ifft3_fftw(nv2(1),nv2(2),nv2(3),symvt)
    !write(13,*) "after ifft3 symvtr ="
    !do ii = 1,nv2(3),1
    !write(13,*) "kk = ", ii
    !do jj = 1,nv2(1),1
    !write(13,*)  symvtr(jj,:,ii)
    !write(13,*) " "
    !end do
    !end do
    FRC%Arr = cshift(FRC%Arr,1,1)
    !symvt = cshift(symvt,1,1)
    !write(13,*) "after cshift1 symvtr ="
    !do ii = 1,nv2(3),1
    !    write(13,*) "kk = ", ii
    !    do jj = 1,nv2(1),1
    !        write(13,*)  symvtr(jj,:,ii)
    !        write(13,*) " "
    !    end do
    !end do

    !symvt = cshift(symvt,1,2)
    !symvt = cshift(symvt,1,3)
    FRC%Arr = cshift(FRC%Arr,1,2)
    FRC%Arr = cshift(FRC%Arr,1,3)
    vector = dble(reshape(FRC%Arr,(/ngrid/)))
    !vector = dble(reshape(symvt,(/ngrid/)))
    !close(13)
    return
    end subroutine symmetrize_1D
    end module symmetrize_1D_module
