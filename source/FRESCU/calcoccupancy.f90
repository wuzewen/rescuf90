! calcoccupancy.f90
!********************************
    !
!********************************
    
    module calcoccupancy_module
    contains
    subroutine calcoccupancy(FRC,energy,mu,focc2,entropy)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use fermiDirac_3D_module
    use bsxfunTimes_module
    use fzero_occupancy_module
    use fzero_occupancy2_module
    use MPI_Bcast_variable_1D_module
    use MPI_Bcast_variable_3D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable             :: energy(:,:,:)
    
    ! temporary variables
    logical           :: mpistat
    character(len=20) :: method
    integer           :: nband, nkpt, ispin, nspin, xeig, nval, ii, jj, kk
    real*8            :: mu0, sigma, xt, ntot
    
    real*8, allocatable :: kweight(:), energyT(:,:,:), mu1(:,:,:), x(:), nfun(:), focc(:), ent(:,:), energyTmp(:,:,:)
    real*8, allocatable :: entropytmp(:,:)
    real*8, allocatable :: enerT(:), focc2(:,:,:), nocc2(:,:), kweight2(:,:), kweight1(:,:), entroy(:,:,:)
    
    integer, allocatable :: ranks(:)
    ! output variables
    real*8              :: mu, entropy
    real*8, allocatable :: nocc(:,:,:), focout(:,:,:), muout1(:), muout2(:)
    
    ! body of this function
    mpistat = FRC%mpi%status
    method  = FRC%kpoint%sampling
    nband   = size(energy,1)
    nkpt    = size(energy,2)
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    xeig    = FRC%eigensolver%emptyBand
    Nval    = FRC%eigensolver%Nvalence
    allocate(kweight(size(FRC%kpoint%weight)))
    kweight = FRC%kpoint%weight
    allocate(kweight1(size(kweight),1))
    allocate(kweight2(1,size(kweight)))
    kweight1(:,1) = kweight(:)
    kweight2(1,:) = kweight(:)
    !energyT = reshape(energy,(/nband,nkpt,nspin/))
    !write(*,*) "I'm here in calcoccupancy."
    !write(*,*) "method = "
    !write(*,*)  method   
    !write(*,*) "size of energy."
    !write(*,*)  size(energy,1), size(energy,2),size(energy,3)
    if (ispin == 1) then
        ntot = dble(Nval/2)
    else
        ntot = dble(Nval)
    end if
    
    !if (FRC%energy%EFermi > 20) then
    !    mu0 = FRC%energy%EFermi
    !else
    !    mu1 = energyT(nband-xeig:nband-xeig+1,:,:)
    !    mu0 = sum(mu1)/size(mu1)
    !end if
    mu0 = 0.0D0
    allocate(nocc(int(ntot),nkpt,nspin))
    if (trim(method) == trim("fermi-dirac")) then
        sigma = FRC%smearing%sigma
        if (sigma /= 0.0) then
            !write(*,*) "I'm here when sigma /= 0"
            !call fermiDirac(sigma,xt,enerT,focc)
            !call nfunsum(focc,kweight,nfun)

            !write(*,*) "input of fzero_occupancy. in calcoccupancy."
            !write(*,*) "sigma =", sigma
            !write(*,*) "kweight"
            !write(*,*)  kweight
            !write(*,*) "ntot", ntot
            !write(*,*) "mu0", mu0
            !write(*,*) "energy"
            !write(*,*)  energy



            call fzero_occupancy(sigma,kweight,energy,ntot,mu0,mu)
            !write(*,*) "I'm here after fzero_occupancy in calcoccupancy."
            !write(*,*) "mu =", mu
            call fermiDirac_3D(sigma,mu,energy,focc2)
            !write(*,*) "I'm here after fermiDirac in calcoccupancy."
            !write(*,*) "size of focc2"
            !write(*,*)  size(focc2,1), size(focc2,2), size(focc2,3)
            allocate(entroy(size(focc2,1),size(focc2,2),size(focc2)))
            entroy = 0.0D0
            do ii = 1,size(focc2,1),1
                do jj = 1,size(focc2,2),1
                    do kk = 1,size(focc2,3),1
                        if (focc2(ii,jj,kk) /= 0.0 .and. focc2(ii,jj,kk) /= 1.0) then
                            entroy(ii,jj,kk) = -(focc2(ii,jj,kk)*log(focc2(ii,jj,kk)) + &
                                               (1.0D0-focc2(ii,jj,kk))*log(1.0D0-focc2(ii,jj,kk)))
                        end if
                    end do
                end do
            end do
            allocate(ent(1,size(focc2,2)))
            ent = 0.0D0
            do kk = 1,size(ent,2),1
                do ii = 1,size(focc2,1),1
                    do jj = 1,size(focc2,3),1
                        ent(1,kk) = ent(1,kk)+entroy(ii,kk,jj)
                    end do
                end do
            end do
            !write(*,*) "size of ent and kweight1 in calcoccupancy."
            !write(*,*)  size(ent,1),size(ent,2),size(kweight1,1),size(kweight1,2)
            allocate(entropytmp(size(ent,1),size(kweight1,2)))
            entropytmp = matmul(ent,kweight1)
            entropy    = sigma*entropytmp(1,1)
            !write(*,*) "entropy in calcOccupancy."
            !write(*,*)  entropy
            !call bsxfunTimes(nocc2,kweight2,nocc2)
            do ii = 1,size(focc2,1),1
                do jj = 1,size(focc2,3),1
                    focc2(ii,:,jj) = focc2(ii,:,jj)*kweight(:)
                end do
            end do
            !write(*,*) "I'm here after nocc2 in calcoccupancy"
        else
            write(*,*) "Sigma = 0 is not available now."
            stop
            !allocate(nocc(size(energy,1),size(energy,2),size(energy,3)))
            forall(ii=1:int(ntot)/nspin)
                nocc(ii,:,:) = 1.0D0
            end forall
            allocate(energytmp(2,size(energy,2),size(energy,3)))
            mu = sum(energytmp)/dble(size(energy,2))/dble(size(energy,3))/2.0D0
            call bsxfunTimes(nocc2,kweight2,nocc2)
            entropy = 0.0D0
            !write(*,*) "I'm here after nocc2 in calcoccupancy"
        end if
    else if (trim(method) == trim("tetrahedron")) then
        write(*,*) "Error in calcoccupancy.f90. FRC%kpoint%sampling = tetrahedron is not available now."
        stop
        !nvec = kpoint%gridn
        !allocate(avec(3,3))
        !avec = FRC%domain%latvec
        !call inversion(avec,bvec)
        !bvec = transpose(bvec)
        !bvec = 2.0*pi*bvec
        !call tetra_tri(nvec,bvec,ttri)
        !ttri = FRC%kpoint%ired(ttri)
        !ttri = abs(ttri)
        !call fzero(ttri,energy,ntot,-1E-8,mu0,mum)
        !call fzero(ttri,energy,ntot,1E-8,mu0,mup)
        !mu   = (mum+mup)/2.0
        !if (trim(method) == trim("tetrahedron")) then
        !    call tetra_weight(ttri,energy,mu,.FALSE.,nocc)
        !else
        !    call tetra_weight(ttri,energy,mu,.TRUE.,nocc)
        !end if
        !entropy = 0
    else if (trim(method) == trim("gauss")) then
        write(*,*) "Error in calcoccupancy.f90. FRC%kpoint%sampling = gauss is not available now."
        stop
        !sigma   = FRC%smearing%sigma
        !sorder  = FRC%smearing%order
        !call methpaxton(sorder,sigma,energy,x,1,focc)
        !nfun    = matmul(focc,kweight)
        !call fzero(focc,kweight)
        !call hermiteExp2(sorder,sigma,mu,energy,entropy)
        !entropy    = 0.5*sigma*entropy
        !entropyTmp = sum(sum(entropy,3),1)
        !entropym   = matmul(entropyTmp,kweight)
    else
        write(*,*) "Invalid value method to calculate occupancies."
        stop
    end if
    
    if (ispin == 1) then
        focc2 = focc2*2.0D0
    end if
    
    if (mpistat) then
            allocate(ranks(FRC%mpi%mpisize))
            forall(ii=1:FRC%mpi%mpisize)
                    ranks(ii) = ii-1
            end forall
        !write(*,*) "MPI is not available now."
        allocate(focout(size(focc2,1),size(focc2,2),size(focc2,3)))
        call MPI_Bcast_variable_3D(focc2,0,ranks,focout)
        allocate(muout1(1),muout2(1))
        muout1(1) = mu
        call MPI_Bcast_variable_1D(muout1,0,ranks,muout2)
        mu    = muout2(1)
        focc2 = focout
        !stop
    end if

    !write(*,*) "stop in calcoccupancy."
    !stop
    
    return
    end subroutine calcoccupancy
    end module calcoccupancy_module
        
