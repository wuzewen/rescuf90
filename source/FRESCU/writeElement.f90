! writeElement.f90

      module writeElement_module
      contains
      subroutine writeElement(FRC,filnam)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam
      integer                         :: ii

      open(unit=11,file=filnam)
      do ii = 1,FRC%atom%numberOfElement,1
      write(11,*) "Element number = ", ii
      write(11,*) "Z = "
      write(11,*)  FRC%element(ii)%Z
      write(11,*) "aoLcutoff = "
      write(11,*)  FRC%element(ii)%aoZcutoff
      write(11,*) "species = "
      write(11,*)  FRC%element(ii)%species
      write(11,*) "Number of Atom = "
      write(11,*)  FRC%element(ii)%NumberOfAtom
      write(11,*) "Valence = "
      write(11,*)  FRC%element(ii)%valence
      write(11,*) "Vnl Ncutoff = "
      write(11,*)  FRC%element(ii)%VnlNcutoff
      write(11,*) "Vnl Lcutoff = "
      write(11,*)  FRC%element(ii)%vnlLcutoff
      write(11,*) "aolnu = "
      write(11,*)  FRC%element(ii)%aolnu
      write(11,*) "aopnu = "
      write(11,*)  FRC%element(ii)%aopnu
      write(11,*) "aoznu = "
      write(11,*)  FRC%element(ii)%aoznu
      write(11,*) "aornu = "
      write(11,*)  FRC%element(ii)%aornu
      write(11,*) "vnllnu = "
      write(11,*)  FRC%element(ii)%vnllnu
      write(11,*) "vnlenu = "
      write(11,*)  FRC%element(ii)%vnlenu
      write(11,*) "vnlnnu = "
      write(11,*)  FRC%element(ii)%vnlnnu
      write(11,*) "vnlrnu = "
      write(11,*)  FRC%element(ii)%vnlrnu
      end do

      close(11)
      return
      end subroutine writeElement
      end module writeElement_module
