! MPI_Bcast_variable.f90

      module MPI_Bcast_variable_CMPLX_2D_module
      contains
      subroutine MPI_Bcast_variable_CMPLX_2D(A,root,ranks,B)

      use MEX_BCAST_CMPLX_2D_module

      include 'mpif.h'

      !implicit none

      ! input variables
      complex*16, allocatable  :: A(:,:)
      integer              :: root
      integer, allocatable :: ranks(:)

      ! temporary variables
      integer :: mpisize, mpirank, rankn, ntmp, ii
      integer :: coun, grouproot, ierr
      integer, allocatable :: h(:,:)
      integer, allocatable :: dtmp(:,:), d(:,:)

      ! output variables
      complex*16, allocatable  :: B(:,:)

      ! body of this function
      !mpisize = MPI_Comm_size
      !mpirank = MPI_Comm_rank

      call MPI_COMM_RANK(MPI_COMM_WORLD,mpirank,ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD,mpisize,ierr)
      if (.not. any(ranks == mpirank)) then
              return
      end if

      B       = A
      rankn   = size(ranks)
      ntmp    = 0
      !do ii = 1,rankn,1
      !    if (ranks(ii) == root) then
      !            ntmp = ntmp+1
      !    end if
      !end do
      !allocate(grouproot(ntmp))
      !ntmp = 0
      do ii = 1,rankn,1
          if (ranks(ii) == root) then
                  !ntmp            = ntmp+1
                  grouproot = ii-1
                  exit
          end if
      end do

      !if (mpirank == root) then
      !        call MPI_Pack(A,h,d)
      !else
      !        allocate(h(10,1))
      !        h = 0
      !end if

      !call mex_bcast(h,10,grouproot,rankn,ranks)

      !coun = h(2,1)
      !if (mpirank /= root) then
      !        allocate(dtmp(coun,1))
      !        dtmp = 0
      !end if

      !if (coun == 1) then
      !        allocate(d(2,1))
      !        d(1,1) = dtmp(1,1)
      !        d(2,1) = 0
      !        call mex_bcast(d,2,grouproot,rankn,ranks)
      !else
      coun = size(B)
              call mex_bcast_CMPLX_2D(B,coun,grouproot,rankn,ranks)
      !end if

      !if (mpirank /= root) then
      !        call MPI_Unpack(h,dtmp,B)
      !end if

      return
      end subroutine MPI_Bcast_variable_CMPLX_2D
      end module MPI_Bcast_variable_CMPLX_2D_module

