! inputType.f90
!****************************************************
    
    ! This is a programmer defined data type, 
    ! which will be used to contain the variables
    ! read from control.input file
    
!****************************************************
    
    module inputType
    
    type :: NameAndValue
        character(len=50) :: name, value
    end type NameAndValue
    
    type :: inputFromFileType
        integer                         :: NumberofInput
        type(NameAndValue), allocatable :: NAV(:)
    end type inputFromFileType
    
    end module inputType