! calcSphericalBessel.f90

!*************************************************************************
      !
      !
      !
!*************************************************************************

      module calcSphericalBesselMod
      contains
      subroutine calcSphericalBessel(n,x,sj)

      implicit none

      N = size(x)
      allocate(sj(N))
      sj = 0.0D0

      do ii = 1,N,1
          if (x .NE. 0.0D0) then
                  sj(ii) = sqrt(pi/2.0D0/x(ii)) * besselj(n+0.5D0,x(ii))
          else if (n .EQ. 0) then
                  sj(ii) = 1.0D0
          end if
      end do

      return
      end subroutine calcSphericalBessel
      end module calcSphericalBesselMod
