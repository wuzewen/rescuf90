! symmetrizeCell.f90

!***********************************************************************
      !
      !
      !
!***********************************************************************

      module symmetrizeCellMod
      contains
      subroutine symmetrizeCell(FRC,Acell,At,includeZero)

      implicit none

      smistat = FRC%smi%status
      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank

      ncell   = size(Acell)
      allocate(Bcell(ncell-1))
      forall(ii=1:ncell)
              Bcell(ii)   = Acell(ii)
      end forall
      kk = 1

      do ii = 1,ncell,1
          call distmat_nnz(FRC,Acell(ii),nonzero)
          spfrac = nonzero/Acell(ii)%m*Acell(ii)%n
          issp   = spfrac .LT. 0.05D0 .AND. Acell(ii)%m*Acell(ii)%n .GT. 10**6
          if (any(At(ii,:) .NE. 0)) then
              Bt(kk,:) = -At(ii,:)
              if (smistat .AND. (.NOT. issp)) then
                  Bcell(kk)%vata = 0.0D0
                  Atmp           = Acell(ii)
                  call smiDGEADD('t',1,Atmp,0,Bcell(kk))
                  call adjustSparsity(Bcell(kk)%vata,0.25D0)
              else if (mpistat) then
                  call ModBCDist(FRC,Acell(ii),Acell(ii)%m,Acell(ii)%n,&
                          Acell(ii)%mproc,Acell(ii)%nproc,Atmp)
                  if (mpirank .EQ. 0) then
                      Atmp%vata = transpose(Atmp%vata)
                  end if
                  call ModBCDist(FRC,Atmp,Acell(ii)%mblock, &
                          Acell(ii)%nblock,Acell(ii)%mproc,Acell(ii)%nproc,Bcell(kk))
              else
                  Bcell(kk)%vata = transpose(Acell(ii)%vata)
              end if
              kk = kk+1
          else if (includeZero) then
              if (smistat .AND. (.NOT. issp)) then
                  A0   = Acell(ii)
                  Atmp = Acell(ii)
                  call GetGlobalInd(mpirank,Atmp,iloc,jloc)
                  call bsxfunGe(iloc,jloc,ind)
                  Atmp%vata(ind) = 0.0D0
              else if (mpistat) then
                 call ModBCDist(FRC,Acell(ii),Acell(ii)%m,Acell(ii)%n &
                         Acell(ii)%mproc,Acell(ii)%nproc,Atmp)
                 if (mpirank .EQ. 0) then
                     call triu()
                     call triu()
                     Atmp%vata = + transpose()
                 end if
                 call ModBCDist(FRC,Atmp,Acell(ii)%mblock, &
                         Acell(ii)%nblock,Acell(ii)%mproc,Acell(ii)%nproc,Acell(ii)
              else
                 call triu()
                 call triu()
                 Acell(ii)%vata = + transpose()
              end if
          end if
      end do 

      At    = []
      Acell = []

      return
      end subroutine symmetrizeCell
      end module symmetrizeCellMod
