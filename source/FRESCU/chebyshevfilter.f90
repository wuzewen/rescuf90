! chebyshevfilter.f90

!*************************************************************
      !
      !
      !
!*************************************************************

      module chebyshevfilter_module
      contains
      subroutine chebyshevfilter(degree,psi1,psi2,FRC,kpt,spin,gpu,c,e)
      !subroutine chebyshevfilter(degree,psi1,psi2,FRC,kpt,spin,gpu,c,e,pso1,pso2)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use GenHamFun_module

      !external zaxpby

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      logical :: gpu
      integer :: degree, spin
      real*8  :: kpt(3), c, e
      complex*16,  allocatable :: psi1(:,:), psi2(:,:)

      ! temporary variables
      integer :: kk
      real*8  :: pi
      complex*16,  allocatable :: hamu(:,:), hamd(:,:), psitmp(:,:)
      integer :: n
      real*8  :: a, b

      ! output variables
      complex*16,  allocatable :: pso1(:,:), pso2(:,:)

      ! body of this function
      pi   = 3.1415926535897932385D0
      allocate(pso1(size(psi1,1),size(psi1,2)))
      n = size(psi1)
      !pso1 = psi1
      !pso2 = psi2
      !write(*,*) "in chebyshevfilter"
      !write(*,*) "degree=",degree
      !write(*,*) "c =", c, "e =", e
      !write(*,*) "psi   ="
      !write(*,*)  psi1
      !allocate(psitmp(size(psi1,1),size(psi1,2)))

      !if (.not. allocated(psi2)) then
              do kk = 1,degree,1
                  !psitmp = psi1
                  call GenHamFun(FRC,kpt,spin,gpu,psi1,pso1,hamu,hamd)
                  !write(*,*) "pso1 ="
                  !write(*,*)  pso1
                  !write(*,*) "operpso1"
                  !write(*,*)  c*psitmp
                  !stop
                  pso1 = pso1/e 
                  psi1 = pso1 - (c/e + cos((2.0D0*dble(kk)-1.0D0)*pi/2.0D0/dble(degree)))*psi1
                  !a =  1.0D0/e
                  !b = -(c/e + cos((2.0D0*dble(kk)-1.0D0)*pi/2.0D0/dble(degree)))
                  !call zaxpby(n,a,pso1,1,b,psi1,1)
                  !psi1 = pso1 - psi1
                  !pso1 = (pso1-c*psitmp)/e-cos((2.0D0*dble(kk)-1.0D0)*pi/2.0D0/dble(degree))*psitmp
                  !psi1 = pso1
              end do
      !else 
              !do kk = 1,degree,1
      !end if
      deallocate(pso1)

      return
      end subroutine chebyshevfilter
      end module chebyshevfilter_module   
