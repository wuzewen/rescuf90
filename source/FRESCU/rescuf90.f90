!  rescu.f90 
!****************************************************************************
!
!  PROGRAM: rescu
!
!  PURPOSE:  Main program in RESCU FORTRAN version.
!
!****************************************************************************

    program rescu

    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    !use AtomSymbolType_module
    use readinputfile_module
    use InitMatRcal_module
    use SCF_module
    !use rescu_lcao_bs_module
    !use rescu_real_bs_module
    !use writeBandStructure_module
    !use rescu_dos_module
    !use writedos_module
    !use rescu_dftp_module
    !use rescu_force_module
    !use rescu_relax_module
    !use getPhonon_module
    !use init_structure_module
    use readSystemFile_module
    use displayInfos_module
    use writeEverything_module
 
    include 'mpif.h'

    !implicit none

    ! input Variables
    
    ! temporary variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType)              :: inputFromFile
    
    
    
    real*8                               :: RESCUf90_version
    integer                              :: nline, nvalue, nAtom, status
    integer                              :: ii, jj, ierr
    logical                              :: calculationTypeIsFind, CalcBSafterSCFIsFind
    !type(AtomSymbolType),    allocatable :: AtomSymbol(:)
    character(len=4)                     :: ElementName
    character(len=30)                    :: filnam
    ! output variables
    
    ! Body of rescuf90
    
    ! Defining the version of F-RESCU.
    FRC%version = "0.001"
    
    ! Reading input files.
    call readinputfile(inputFromFile)
    call readSystemFile(inputFromFile,FRC)
    
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%%                                                                    %%%%!
    !%%%%            Difining which calculation should F-RESCU do.           %%%%!
    !%%%%                                                                    %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    ii = 0
    calculationTypeIsFind = .FALSE.
    do while(ii < inputFromFile%NumberofInput)
        ii     = ii+1
        if (trim(inputFromFile%NAV(ii)%name) == trim("CalculationType")) then
            calculationTypeIsFind = .TRUE.
            exit
        end if
    end do
    if (calculationTypeIsFind) then
        FRC%info%calculationType = inputFromFile%NAV(ii)%value
    else
        FRC%info%calculationType = "self-consistent"
    end if
    
    ii = 0
    CalcBSafterSCFIsFind  = .FALSE.
    do while(ii < inputFromFile%NumberofInput)
        ii     = ii+1
        if (trim(inputFromFile%NAV(ii)%name) == trim("CalcBSafterSCF")) then
                CalcBSafterSCFIsFind  = .TRUE.
                exit
        end if
    end do
    if (CalcBSafterSCFIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                    FRC%info%calcBSafterSCF = .TRUE.
            else
                    FRC%info%calcBSafterSCF = .FALSE.
            end if
    else
            FRC%info%calcBSafterSCF = .FALSE.
    end if
    !write(*,*) "CalcBSafterSCFIsFind =", CalcBSafterSCFIsFind
    !write(*,*) "FRC%info%calcBSafterSCF", FRC%info%calcBSafterSCF
    !ii = 0
    !SpinTypeIsFind = .FALSE.
    !do while(ii<=nline)
    !    ii     = ii+1
    !    if (trim(inputFromFile(ii)%name) == trim("SpinType")) then
    !        SpinTypeIsFind = .TRUE.
    !        exit
    !    end if
    !end do
    
    !if (SpinTypeIsFind) then
    !    FRC%spin%SpinType = inputFromFile(ii)%value
    !else
    !    FRC%spin%SpinType = "degenerate"
    !end if
    
    !call readNumberOfElement(FRC%atom%numberOfElement)
    !call readPositionfileElement(FRC%atom%numberOfElement,AtomSymbol)
    !nAtom = 0
    !do ii = 1,FRC%atom%numberOfElement,1
    !    nAtom = nAtom + AtomSymbol(ii)%NumberOfAtoms
    !end do
    
    !allocate(AtomSymbol(FRC%atom%numberOfElement))
    !allocate(FRC%ElementData(FRC%atom%numberOfElement))
    
    !allocate(FRC%Atom%element(nAtom))
    !allocate(FRC%Atom%XYZ(nAtom,3))
    !allocate(FRC%Atom%FracXYZ(nAtom,3))
    
    !if (trim(FRC%spin%SpinType) == trim("degenerate")) then
    !    allocate(FRC%Atom%magmom(0))
    !    allocate(FRC%Atom%magmomD(0,0))
    !    allocate(FRC%Atom%magmomcart(0,0))
    !else if (trim(FRC%spin%SpinType) == trim("collinear")) then
    !    allocate(FRC%Atom%magmom(nAtom))
    !    allocate(FRC%Atom%magmomD(0,0))
    !    allocate(FRC%Atom%magmomcart(0,0))
    !else if (trim(FRC%spin%SpinType) == trim("non-collinear")) then
    !    allocate(FRC%Atom%magmom(nAtom))
    !    allocate(FRC%Atom%magmomD(nAtom,3))
    !    allocate(FRC%Atom%magmomcart(nAtom,3))
    !end if
    
    !allocate(FRC%Domain%bvalx(2))
    !allocate(FRC%Domain%bvaly(2))
    !allocate(FRC%Domain%bvalz(2))
    
    !allocate(FRC%kpoint%kcartesian(nk,3))
    
    !allocate(FRC%ph%mass(0))
    !allocate(FRC%ph%unitCellIndex(0))
    !allocate(FRC%ph%unitCellMass(0))
    
    !allocate(FRC%element(FRC%atom%numberOfElement))
    
    !do ii = 1,FRC%atom%numberOfElement,1
    !    FRC%ElementData(ii)%Symbol = AtomSymbol(ii)%species
    !    ElementName                = AtomSymbol(ii)%species
    !    call readSummeryOfBasis(ElementName,SOB)
        
    !    allocate(FRC%element(ii)%aolnu(SOB%SizeOfOrbitalSet))
    !    allocate(FRC%element(ii)%aopnu(SOB%SizeOfOrbitalSet))
    !    allocate(FRC%element(ii)%aornu(SOB%SizeOfOrbitalSet))
    !    allocate(FRC%element(ii)%aoznu(SOB%SizeOfOrbitalSet))
    !    do jj = 1,SOB%SizeOfOrbitalSet,1
    !        allocate(FRC%element(ii)%aornu(jj)%rnuD(SOB%SizeOfOrbitalSetrrData))
    !    end do
        
    !    allocate(FRC%element(ii)%vnllnu(SOB%SizeOfVnl))
    !    allocate(FRC%element(ii)%vnlenu(SOB%SizeOfVnl))
    !    allocate(FRC%element(ii)%vnlrnu(SOB%SizeOfVnl))
    !    allocate(FRC%element(ii)%vnlnnu(SOB%SizeOfVnl))
    !    do jj = 1,SOB%SizeOfOrbitalSet,1
    !        allocate(FRC%element(ii)%vnlrnu(jj)%rnuD(SOB%SizeOfVnlrrData))
    !    end do
        
        ! Vlocal
    !    allocate(FRC%ElementData(ii)%Vlocal(SOB%SizeOfVlocal))
    !    do jj = 1,SOB%SizeOfVlocal,1
    !        allocate(FRC%ElementData(ii)%Vlocal(jj)%rrData(SOB%SizeOfVlocalrrData))
    !        allocate(FRC%ElementData(ii)%Vlocal(jj)%rrData(SOB%SizeOfVlocaldrData))
    !        allocate(FRC%ElementData(ii)%Vlocal(jj)%rrData(SOB%SizeOfVlocalvvData))
    !    end do
        
        ! Rlocal
    !    allocate(FRC%ElementData(ii)%Rlocal(SOB%SizeOfRlocal))
    !    do jj = 1,SizeOfRlocal,1
    !        allocate(FRC%ElementData(ii)%Rlocal(jj)%rrData(SOB%SizeOfRlocalrrData))
    !        allocate(FRC%ElementData(ii)%Rlocal(jj)%rrData(SOB%SizeOfRlocaldrData))
    !        allocate(FRC%ElementData(ii)%Rlocal(jj)%rrData(SOB%SizeOfRlocalrhoData))
    !    end do
            
        ! Vnl
    !    allocate(FRC%ElementData(ii)%Vnl(SOB%SizeOfVnl))
    !    do jj = 1,SOB%SizeOfVnl,1
    !        allocate(FRC%ElementData(ii)%Vnl(jj)%rrData(SOB%SizeOfVnlrrData))
    !        allocate(FRC%ElementData(ii)%Vnl(jj)%rrData(SOB%SizeOfVnldrData))
    !        allocate(FRC%ElementData(ii)%Vnl(jj)%rrData(SOB%SizeOfVnlvvData))
    !        allocate(FRC%ElementData(ii)%Vnl(jj)%rrData(SOB%SizeOfVnlqqData))
    !        allocate(FRC%ElementData(ii)%Vnl(jj)%rrData(SOB%SizeOfVnlfqData))
    !        allocate(FRC%ElementData(ii)%Vnl(jj)%rrData(SOB%SizeOfVnlqwData))
    !    end do
        
        ! Vna
    !    allocate(FRC%ElementData(ii)%Vna(SOB%SizeOfVna))
    !    do jj = 1,SOB%SizeOfVna,1
    !        allocate(FRC%ElementData(ii)%Vna(jj)%rrData(SOB%SizeOfVnarrData))
    !        allocate(FRC%ElementData(ii)%Vna(jj)%rrData(SOB%SizeOfVnadrData))
    !        allocate(FRC%ElementData(ii)%Vna(jj)%rrData(SOB%SizeOfVnavvData))
    !    end do
        
        ! Rna
    !    allocate(FRC%ElementData(ii)%Rna(SOB%SizeOfRna))
    !    do jj = 1,SOB%SizeOfRna,1
    !        allocate(FRC%ElementData(ii)%Rna(jj)%rrData(SOB%SizeOfRnarrData))
    !        allocate(FRC%ElementData(ii)%Rna(jj)%rrData(SOB%SizeOfRnadrData))
    !        allocate(FRC%ElementData(ii)%Rna(jj)%rrData(SOB%SizeOfRnarhoData))
    !    end do
        
        ! RelPseudoP
    !    allocate(FRC%ElementData(ii)%RelPseudoP(SOB%SizeOfRelPseudoP))
    !    do jj = 1,SOB%SizeOfRelPseudoP,1
    !        allocate(FRC%ElementData(ii)%RelPseudoP(jj)%rrData(SOB%SizeOfRelPseudoPrrData))
    !        allocate(FRC%ElementData(ii)%RelPseudoP(jj)%rrData(SOB%SizeOfRelPseudoPdrData))
    !        allocate(FRC%ElementData(ii)%RelPseudoP(jj)%rrData(SOB%SizeOfRelPseudoPvvData_screened))
    !        allocate(FRC%ElementData(ii)%RelPseudoP(jj)%rrData(SOB%SizeOfRelPseudoPqqData_unscreened))
    !    end do
        
        ! OrbitalSet
    !    allocate(FRC%ElementData(ii)%OrbitalSet(SOB%SizeOfOrbitalSet))
    !    do jj = 1,SOB%SizeOfOrbitalSet,1
    !        allocate(FRC%ElementData(ii)%OrbitalSet(jj)%rrData(SOB%SizeOfOrbitalSetrrData))
    !        allocate(FRC%ElementData(ii)%OrbitalSet(jj)%rrData(SOB%SizeOfOrbitalSetdrData))
    !        allocate(FRC%ElementData(ii)%OrbitalSet(jj)%rrData(SOB%SizeOfOrbitalSetfrData))
    !        allocate(FRC%ElementData(ii)%OrbitalSet(jj)%rrData(SOB%SizeOfOrbitalSetqqData))
    !        allocate(FRC%ElementData(ii)%OrbitalSet(jj)%rrData(SOB%SizeOfOrbitalSetfqData))
    !        allocate(FRC%ElementData(ii)%OrbitalSet(jj)%rrData(SOB%SizeOfOrbitalSetqwData))
    !    end do
    !end do
    !write(*,*) "I'm here. In rescuf90.f90, before InitMatRcal."

    ! Switch calculation type and executing.
    if (trim(FRC%info%calculationType) == trim("self-consistent")) then
        !write(*,*) "I'm here before InitMatRcal."

        ! Init control parameters and data structure.
        call InitMatRcal(inputFromFile,FRC)

        ! Display control parameters and system information  on the main processor.
        if (FRC%mpi%rank == 0) then
                call displayInfos(FRC)
        end if
        !filnam = "atom.txt"
        !call writeEverything(FRC)
        !write(*,*) "I'm here. In rescuf90.f90, after InitMatRcal."

        ! Execute SCF calculation.
        call SCF(FRC)
    else if (trim(FRC%info%calculationType) == trim("band-structure")) then
        write(*,*) "Error in rescu.f90. Band Structure calculation is not available now."
        stop
        !call InitMatRcal(inputFromFile,FRC)
        !FRC%kpoint%Ksamlingtype = "line"
        !if (FRC%LCAO%status) then
        !    call rescu_lcao_bs(FRC)
        !else
        !    call rescu_real_bs(FRC)
        !end if
        !if (FRC%mpi%rank == 0) then
        !    call writeBandStructure(FRC)
        !end if
    else if (trim(FRC%info%calculationType) == trim("dos")) then
        write(*,*) "Error in rescu.f90. DOS calculation is not available now."
        stop
        !call InitMatRcal(inputFromFile,FRC)
        !call rescu_dos(FRC)
        !if (FRC%mpi%rank == 0) then
        !    call writeDOS(FRC)
        !end if
    else if (trim(FRC%info%calculationType) == trim("dftp")) then
        write(*,*) "Error in rescu.f90. DFPT calculation is not available now."
        stop
        !call InitMatRcal(inputFromFile,FRC)
        !call rescu_dftp(FRC)
    else if (trim(FRC%info%calculationType) == trim("force")) then
        write(*,*) "Error in rescu.f90. Force calculation is not available now."
        stop
        !call InitMatRcal(inputFromFile,FRC)
        !call rescu_force(FRC)
    else if (trim(FRC%info%calculationType) == trim("relaxation")) then
        write(*,*) "Error in rescu.f90. Relaxation calculation is not available now."
        stop
        !FRC%LCAO%status = .FALSE.
        !call InitMatRcal(inputFromFile,FRC)
        !call rescu_relax(FRC)
    else if (trim(FRC%info%calculationType) == trim("phonon")) then
        write(*,*) "Error in rescu.f90. Phonon calculation is not available now."
        stop
        !FRC%LCAO%status = .FALSE.
        !call InitMatRcal(inputFromFile,FRC)
        !call init_structure(FRC)
        !call getPhonon(FRC)
    end if

    ! If MPI is used, end it.
    if (FRC%mpi%status) then
         call MPI_BARRIER(MPI_COMM_WORLD,ierr)
         call MPI_FINALIZE(ierr)
    end if

    end program rescu
