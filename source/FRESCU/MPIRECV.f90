! MPIRECV.f90

!************************************************************************
      !
      !
      !
!**********************************************************************************************

      module MPIRECVMOD

      interface MPIRECV
              module procedure MPI_Recv_variable_real8_2D
              module procedure MPI_Recv_variable_real8_1D
              module procedure MPI_Recv_variable_cmplx_2D
              module procedure MPI_Recv_variable_cmplx_1D
      end interface MPIRECV

      end module MPIRECVMOD
