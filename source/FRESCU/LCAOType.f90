! LCAOType.f90
    
!**************************************************************
    !
    ! This is to define a type to contain LCAO information.
    !
!**************************************************************
    
    module LCAOType_module

    use VataMN_2D_module
    
    type :: SBoole_type
            real*8, allocatable :: itril(:)
            real*8, allocatable :: jtril(:)
            real*8, allocatable :: local(:)
    end type SBoole_type

    type :: LCAOType
        logical              :: cfsi
        integer              :: dynSubRed
        integer              :: numberOfOrbital
        logical              :: mulliken
        logical              :: aosparse
        real*8               :: sprs
        logical              :: status
        integer              :: VeffRed
        real*8 , allocatable :: aovec(:,:)
        real*8 , allocatable :: Svec(:,:)
        real*8 , allocatable :: Tvec(:,:)
        real*8 , allocatable :: Rorb(:)
        integer, allocatable :: evec(:)
        integer, allocatable :: Lorb(:)
        integer, allocatable :: Morb(:)
        integer, allocatable :: Oorb(:)
        integer, allocatable :: Sorb(:)
        integer, allocatable :: Zorb(:)
        type(VataMN_2D)      :: Smask
        type(VataMN_2D), allocatable :: Scell(:)
        type(VataMN_2D), allocatable :: Tcell(:)
        type(VataMN_2D), allocatable :: aokcell(:)
        type(VataMN_2D), allocatable :: aocell(:)
        type(SBoole_type)            :: SBoole
    end type LCAOType
    
    end module LCAOType_module
