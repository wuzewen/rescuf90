! reduceOperGen.f90

!***************************************************************************************
      !
      !
      !
!***************************************************************************************

      module reduceOperGenMod
      contains
      subroutine reduceOperGen(Acell,Amat,Av,kpt,spnnz)

      use reduceOperMod

      implicit none

      if (.NOT. allocated(Amat)) then
              call reduceOper(Acell,Av,kpt,spnnz)
      else
              call reduceOperMat(Acell(1),Amat,Av,kpt)
      end if

      return
      end subroutine reduceOperGen
      end module reduceOperGenMod

