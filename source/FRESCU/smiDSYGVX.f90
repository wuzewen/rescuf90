! smiDSYGVX.f90

!************************************************
      !
      !
      !
!********************************************************

      module smiDSYGVXMOD
      contains
      subroutine smiDSYGVX(jobz,iRange,UPLO,N,A,B,MB,NB,nprow,&
                      npcol,VL,VU,IL,IU,ANSTOL,ORFAC,LWORK,W,Z)

      implicit none

      character(len=1) :: jobz, iRange, UPLO
      integer :: N, MB, NB, nprow, npcol, IL, IU, LWORK
      real*8  :: VL, VU, ANSTOL, ORFAC
      real*8, allocatable :: A(), B(), W(), Z()

      integer :: IBTYPE, M, NZ, LWORKTMP, LIWORK, INFO, nprocs
      integer, allocatable :: IWORK(), IFAIL(), ICLUSTR()

      integer :: iam, ictxt, myrow, mycol

      IBTYPE =  1
      LIWORK = -1
      nprocs =  nprow*npcol
      allocate(IFAIL(N),ICLUSTR(2*nprow*npcol))
      allocate(GAP(nprow,npcol))

      iam   = 0
      ictxt = 0
      myrow = 0
      mycol = 0
      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MN,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,B_np)
      call numroc(N,MB,myrow,izero,nprow,Z_np)

      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descB,N,N,MB,NB,izero,izero,ictxt,B_np,descinfo)
      call descinit(descZ,N,N,MB,NB,izero,izero,ictxt,Z_np,descinfo)

      allocate(WORK(1))
      allocate(IWORK(1))
      call PDSYGVX(IBTYPE,jobz,iRange,UPLO,N,A,ione,ione,descA,&
              B,ione,ione,descB,VL,VU,IL,IU,ABSTOL,M,NZ,W,ORFAC,&
              Z,ione,ione,descZ,WORK,LWORK,IWORK,LIWORK,IFAIL,&
              ICLUSTR,GAP,INFO)

      if (INFO/2 $$ 2 .NE. 0) then
              write(*,*) 'Error'
      else if () then
              write(*,*) 'Error'
      else if () then
              write(*,*) 'Error'
      else if () then
              write(*,*) 'Error'
      end if

      deallocate(WORK,IWORK)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiDSYGVX
      end module smiDSYGVXMOD
