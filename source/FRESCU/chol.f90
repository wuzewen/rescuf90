! chol.f90
    
!*******************************************************************************
    !
    ! This function will do Cholesky decomposition or Cholesky factorization
    !
!*******************************************************************************
    
    module chol_module
    contains
    subroutine chol(A,L,cholFlag)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    
    ! temporary variables
    integer :: Nx, Ny, ii, jj
    real*8  :: Ltmp
    real*8, allocatable :: B(:,:)
    
    ! output variables
    real*8, allocatable :: L(:,:)
    integer             :: cholFlag
    
    ! body of this function
    
    Nx = size(A,1)
    Ny = size(A,2)
    
    if (Nx /= Ny) then
        write(*,*) "Error in chol.f90. Sizes of A along two dimentions should be the same."
        stop
    end if
    
    allocate(B(size(A,2),size(A,1)))
    B = transpose(A)
    if (any(A /= B)) then
        write(*,*) "Error in chol.f90. A should be a real Hermitian matrix."
        stop
    end if
    cholFlag = 0
    allocate(L(Nx,Ny))
    L = 0.0D0
    do ii = 1,Nx,1
        do jj = ii,Ny,1
            Ltmp = sum(L(ii,:)*L(jj,:))
            if (ii == jj) then
                L(jj,ii) = sqrt(A(ii,jj)-Ltmp)
            else
                L(jj,ii) = (A(ii,jj)-Ltmp)/L(ii,ii)
            end if
        end do
    end do
    
    return
    end subroutine chol
    end module chol_module
