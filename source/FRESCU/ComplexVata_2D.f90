! ComplexVata_2D.f90

!************************************************************************
      !
      ! This module is used to define a type of data.
      ! The data named vata is two dimentional allocatable complex
      ! array.
      !
!************************************************************************

      module ComplexVata_2D_module

              type :: ComplexVata_2D
                      complex*16, allocatable :: vata(:,:)
              end type ComplexVata_2D

      end module ComplexVata_2D_module
