! RHO_type.f90
    
!***********************************************
    !
    ! This is a data type to store RHO data
    !
!***********************************************
    
    module RHO_type
    
    type :: RHOtype
        real*8, allocatable :: vata(:)
        integer             :: m
        integer             :: n
        integer             :: mblock
        integer             :: nblock
        integer             :: mproc
        integer             :: nproc
    end type RHOtype
    
    end module RHO_type