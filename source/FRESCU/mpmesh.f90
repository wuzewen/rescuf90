! mpmesh.f90
    
!******************************************************
    !
    !
    !
!******************************************************
    
    module mpmesh_module
    contains
    subroutine mpmesh(nvec,shift,mesh)
    
    use ndgridA_module
    use bsxfunPlus_module
    
    implicit none
    
    ! input variables
    integer :: nvec(3)
    real*8  :: shift(3)
    
    ! temporary variables
    real*8 :: dx, dy, dz
    real*8, allocatable :: kx(:), ky(:), kz(:), nvectmp(:,:), meshtmp(:,:)
    integer :: ii
    
    ! output variables
    real*8, allocatable :: mesh(:,:)
    
    ! body of this function
    dx = 1.0D0/dble(nvec(1))
    dy = 1.0D0/dble(nvec(2))
    dz = 1.0D0/dble(nvec(3))
    allocate(kx(nvec(1)))
    forall(ii=1:nvec(1))
        kx(ii) = dx*(dble(ii-1))
    end forall
    allocate(ky(nvec(2)))
    forall(ii=1:nvec(2))
        ky(ii) = dy*(dble(ii-1))
    end forall
    allocate(kz(nvec(3)))
    forall(ii=1:nvec(3))
        kz(ii) = dz*(dble(ii-1))
    end forall
    
    allocate(mesh(product(nvec),3))
    call ndgridA(kx,ky,kz,mesh)
    
    allocate(nvectmp(1,3))
    forall(ii=1:3)
        nvectmp(1,ii) = mod(nvec(ii)+1,2)/dble(nvec(ii))/2.0
    end forall
    
    allocate(meshtmp(product(nvec),3))
    call bsxfunPlus(mesh,nvectmp,meshtmp)
    
    if (any(shift /= 0.0)) then
        forall(ii=1:3)
            nvectmp(1,ii) = shift(ii)/dble(nvec(ii))
        end forall
        
        call bsxfunPlus(meshtmp,nvectmp,mesh)
    else
        mesh = meshtmp
    end if
    
    mesh = mesh - dble(anint(mesh))
    
    return
    end subroutine mpmesh
    end module mpmesh_module
