! smearingType.f90
    
!********************************************************************
    !
    ! This is to define a type to contain smearing information.
    !
!********************************************************************
    
    module smearingType_module
    
    type :: smearingType
        integer              :: order
        real*8               :: sigma
        real*8               :: temperature
    end type smearingType
    
    end module smearingType_module