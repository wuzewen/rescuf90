! writeEigenSolver.f90

      module writeEigenSolver_module

      contains

      subroutine writeEigenSolver(FRC,filnam)

      use FORTRAN_RESCU_CALCULATION_TYPE

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam

      open(unit=10,file=filnam)
      write(10,*) "algo  = "
      write(10,*)  FRC%eigenSolver%algo
      write(10,*) "algoproj = "
      write(10,*)  FRC%eigenSolver%algoproj
      write(10,*) "adapCFD  = "
      write(10,*)  FRC%eigenSolver%adapCFD
      write(10,*) "bandi    = "
      write(10,*)  FRC%eigenSolver%bandi
      write(10,*) "Empty Band = "
      write(10,*)  FRC%eigenSolver%emptyBand
      write(10,*) "init = "
      write(10,*)  FRC%eigenSolver%init
      write(10,*) "ubMethod = "
      write(10,*)  FRC%eigenSolver%ubMethod
      write(10,*) "maxIt = "
      write(10,*)  FRC%eigenSolver%maxit
      write(10,*) "maxRestart = "
      write(10,*)  FRC%eigenSolver%maxrestart     
      write(10,*) "nsym = "
      write(10,*)  FRC%eigenSolver%nsym
      write(10,*) "orthogonalize = "
      write(10,*)  FRC%eigenSolver%orthogonalize
      write(10,*) "precond = "
      write(10,*)  FRC%eigenSolver%precond
      write(10,*) "pwmode = "
      write(10,*)  FRC%eigenSolver%pwmode
      write(10,*) "N valence = "
      write(10,*)  FRC%eigenSolver%Nvalence
      write(10,*) "tol = "
      write(10,*)  FRC%eigenSolver%tol
      write(10,*) "UBmaxIt = "
      write(10,*)  FRC%eigenSolver%UBmaxit
      write(10,*) "nband = "
      write(10,*)  FRC%eigenSolver%nband
      write(10,*) "norb = "
      write(10,*)  FRC%eigenSolver%norb
      
      close(10)
      return
      end subroutine writeEigenSolver
      end module writeEigenSolver_module




