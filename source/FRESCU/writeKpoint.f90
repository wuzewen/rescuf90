! writeKpoint.f90

      module writeKpoint_module
      contains
      subroutine writeKpoint(FRC,filnam)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam
      integer                         :: ii

      open(unit=11,file=filnam)

      write(11,*) "K Space grid number = "
      write(11,*)  FRC%kpoint%gridn
      write(11,*) "currentreduce = "
      write(11,*)  FRC%kpoint%currentreduce
      write(11,*) "ikpt = "
      write(11,*)  FRC%kpoint%ikpt
      write(11,*) "ired = "
      write(11,*)  FRC%kpoint%ired
      write(11,*) "isop = "
      write(11,*)  FRC%kpoint%isop

      !write(11,*) "size of coors"
      !write(11,*)  size(FRC%kpoint%kcartesian)
      !write(11,*)  size(FRC%kpoint%kcartesianPoints)
      !write(11,*)  size(FRC%kpoint%kdirect)
      !write(11,*)  size(FRC%kpoint%ikdirect)
      write(11,*)  "kcartesian = "
      do ii = 1,size(FRC%kpoint%kcartesian,1), 1
          write(11,*)  FRC%kpoint%kcartesian(ii,:)
      end do
      write(11,*)  "kdirect = "
      do ii = 1,size(FRC%kpoint%kdirect,1), 1
          write(11,*)  FRC%kpoint%kdirect(ii,:)
      end do
      write(11,*)  "ikdirect = "
      do ii = 1,size(FRC%kpoint%ikdirect,1),1
          write(11,*)  FRC%kpoint%ikdirect(ii,:)
      end do      
      close(11)
      return
      end subroutine writeKpoint
      end module writeKpoint_module
