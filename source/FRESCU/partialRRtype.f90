! partialRRtype.f90
    
!****************************************
    !
    !
    !
!****************************************
    
    module partialRRtype_module
    
    type :: optsType
        logical :: issym
        logical :: isreal
        real*8  :: tol
        integer :: maxit
    end type optsType
    
    type :: partialRRtype
        logical           :: status
        integer           :: peig
        real*8            :: RitzVectors
        type(optsType)    :: opts
        character(len=20) :: solver
    end type partialRRtype
    
    end module partialRRtype_module