! accumarray.f90
    
!*******************************************
    !
    !
    !
!*******************************************
    
    module accumarray1_module
    contains
    subroutine accumarray1(subs,val,sz,outA)
    
    implicit none
    
    ! input variables
    integer, allocatable :: subs(:)
    real*8,  allocatable :: val(:)
    integer              :: sz
    
    ! temporary variables
    integer :: m, n, ii, jj, ntmp
    
    ! output variables
    real*8,  allocatable :: outA(:)
    
    ! body of this function
    !Mmax = maxval(subs)
    m    = size(subs)
    n    = size(val)
    
    if (m /= n) then
        write(*,*) "Error in accumarray. subs's size and val's size should be the same."
        stop
    end if
    
    allocate(outA(sz))
    outA = 0.0D0
    
    do ii = 1,m,1
        ntmp       = subs(ii)
        outA(ntmp) = outA(ntmp)+val(ii)
    end do
    !do ii = 1,sz,1
    !    do jj = 1,m,1
    !        ntmp = subs(jj)
    !        if (ntmp == ii) then
    !            outA(ii) = outA(ii) + val(jj)
    !        end if
    !    end do
    !end do
    
    return
    end subroutine accumarray1
    end module accumarray1_module
