!  initMatRcal.f90 
!
!  FUNCTIONS:
!  initMatRcal - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: initMatRcal
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module initMatRcal_module
    contains
    subroutine initMatRcal(inputFromFile,FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    use initMPI_module
    use initInfo_module
    !use AddPaths_module
    use InitOption_module
    !use repxyz_module
    use initElement_module
    use GetAtomInfo_module
    use initLCAO_module
    use GetDomainInfo_module
    use initSpin_module
    use initSymmetry_module
    use initKpoint_module
    use initTempSmear_module
    use initDOS_module
    use initEigensolver_module
    use initPRR_module
    use initXC_module
    use initPotential_module
    use initInterPolation_module
    use initDensity_module
    use initEnergy_module
    use initSMI_module
    use initGPU_module
    use initMixer_module
    use initSR_module
    use initForce_module
    use initDFPT_module
    
    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    

    ! output variables


    ! Body of initMatRcal

    ! Init MPI information. Get rank for every prosessor and MPI size.
    call initMPI(inputFromFile,FRC)

    ! Set initiation information.
    call initInfo(inputFromFile,FRC)
    !call AddPaths(nline,inputFromFile,FRC)

    ! Init some control parameters.
    call InitOption(inputFromFile,FRC)
    !call repxyz(FRC)

    ! Get the information of all the elements.
    call initElement(inputFromFile,FRC)
    
    ! Get the information of all the atoms.
    call GetAtomInfo(inputFromFile,FRC)
    
    ! Set the parameters for LCAO basis simulation.
    call initLCAO(inputFromFile,FRC)
    
    ! Get the information of the simulation box.
    call GetDomainInfo(inputFromFile,FRC)
    
    ! Get the spin information of the system.
    call initSpin(inputFromFile,FRC)
    
    ! Get the symmetry information of the system.
    call initSymmetry(inputFromFile,FRC)
    
    ! Get the K space information fo the system.
    call initKpoint(inputFromFile,FRC)
    
    ! Get smearing information.
    call initTempSmear(inputFromFile,FRC)
    
    ! Get information for calculating DOS.
    call initDOS(inputFromFile,FRC)

    ! Get method and control parameters for eigen solver.
    call initEigensolver(inputFromFile,FRC)

    ! Get method and control parameters for partial real rho.
    call initPRR(inputFromFile,FRC)

    ! Get exchange-correlation potential type for the system.
    call initXC(inputFromFile,FRC)

    ! Get potential information for the system.
    call initPotential(inputFromFile,FRC)

    ! Get the method and control parameters for interpolations.
    call initInterPolation(inputFromFile,FRC)

    ! Get information for density.
    call initDensity(inputFromFile,FRC)

    ! Get the method and control parameter for calculating energy.
    call initEnergy(inputFromFile,FRC)

    ! Get information for doing ScaLAPACK calculations.
    call initSMI(inputFromFile,FRC)

    ! Get information for doing GPU calculations.
    call initGPU(inputFromFile,FRC)

    ! Get method and control parameters for mixing.
    call initMixer(inputFromFile,FRC)

    ! Get method and control parameters for structure relaxation calculation.
    call initSR(inputFromFile,FRC)

    ! Get method and control parameters for calculating force.
    call initForce(inputFromFile,FRC)

    ! Get methed and control parameters for DFPT calculation.
    call initDFPT(inputFromFile,FRC)
    
    !call rng(FRC%mpi%rank)
    
    !if (FRC%diffop%method == "FFT") then
    !    call fftw("planner","exhaustive")
    !    call fftw("dwisdom","none")
    !    gridn = FRC%domain%cgridn
    !    call initfftw(gridn)
    !    gridn = FRC%domain%fgridn
    !    call initfftw(gridn)
    !    call fftw("dwisdom",fftinfo)
    !    call fftw("dwisdom",fftinfo)
    !end if
    
    !if (FRC%option%plorder) then
    !    call plorder(FRC)
    !end if

    return
    end subroutine initMatRcal
    end module initMatRcal_module

