! writeSOB.f90

      module writeSOB_module
      contains
      subroutine writeSOB(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      integer :: ii, jj
      open(unit=11,file='SOB.txt')
      do ii = 1,FRC%atom%NumberOfElement,1
          write(11,*) "Element",ii
          write(11,*) "size of Vlocal"
          write(11,*) "rrData", FRC%SOB(ii)%Vlocrr
          write(11,*) "drData", FRC%SOB(ii)%Vlocdr
          write(11,*) "vvData", FRC%SOB(ii)%Vlocvv
          
          write(11,*) "size of Rlocal"
          write(11,*) "rrData", FRC%SOB(ii)%Rlocrr
          write(11,*) "drData", FRC%SOB(ii)%Rlocdr
          write(11,*) "rhoData",FRC%SOB(ii)%Rlocrho
          
          write(11,*) "size of Vnl", FRC%SOB(ii)%Vnl
          do jj = 1,FRC%SOB(ii)%Vnl,1
              write(11,*) "rrData", FRC%SOB(ii)%Vnlrr
              write(11,*) "drData", FRC%SOB(ii)%Vnldr
              write(11,*) "vvData", FRC%SOB(ii)%Vnlvv
              write(11,*) "qqData", FRC%SOB(ii)%Vnlqq
              write(11,*) "fqData", FRC%SOB(ii)%Vnlfq
              write(11,*) "qwData", FRC%SOB(ii)%Vnlqw
          end do

          write(11,*) "size of Vna"
          write(11,*) "rrData", FRC%SOB(ii)%Vnarr
          write(11,*) "drData", FRC%SOB(ii)%Vnadr
          write(11,*) "vvData", FRC%SOB(ii)%Vnavv

          write(11,*) "size of Rna"
          write(11,*) "rrData", FRC%SOB(ii)%Rnarr
          write(11,*) "drData", FRC%SOB(ii)%Rnadr
          write(11,*) "rhoData",FRC%SOB(ii)%Rnarho

          write(11,*) "size of RelpseudoP", FRC%SOB(ii)%PseP
          do jj =1,FRC%SOB(ii)%PseP,1
          write(11,*) "rrData", FRC%SOB(ii)%PsePrr
          write(11,*) "drData", FRC%SOB(ii)%PsePdr
          write(11,*) "vvData_screen", FRC%SOB(ii)%PsePvvS
          write(11,*) "vvData_unscrn", FRC%SOB(ii)%PsePvvU
          end do

          write(11,*) "size of OrbitalSet", FRC%SOB(ii)%Orbit
          do jj = 1,FRC%SOB(ii)%Orbit,1
          write(11,*) "rrData",FRC%SOB(ii)%orbi(jj)%rr
          write(11,*) "drData",FRC%SOB(ii)%orbi(jj)%dr
          write(11,*) "frData",FRC%SOB(ii)%orbi(jj)%fr
          write(11,*) "qqData",FRC%SOB(ii)%orbi(jj)%qq
          write(11,*) "fqData",FRC%SOB(ii)%orbi(jj)%fq
          write(11,*) "qwData",FRC%SOB(ii)%orbi(jj)%qw
          end do

      end do

      close(11)
      return
      end subroutine writeSOB
      end module writeSOB_module

