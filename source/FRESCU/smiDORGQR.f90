! smiDORGQR.f90

!*************************************************
      !
      !
      !
!***************************************************

      module smiDORGQRMOD
      contains
      subroutine smiDORGQR(M,N,A,nprocs,MB,NB,nprow,npcol)

      implicit none

      if (M .GT. N) then
              K = N
      else
              K = M
      end if

      iam   =  0
      ictxt =  0
      myrow =  0
      mycol =  0
      LWORK = -1

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(M,MB,myrow,izero,nprow,A_np)
      A_np = 

      call descinit(descA,M,N,MB,NB,izero,izero,ictxt,A_np,info)

      allocate(WROK())
      call PDGEQRF(M,N,A,ione,ione,descA,TAU,WORK,LWORK,info)
      LWORK = WORK(1)
      deallocate(WORK)
      allocate(WORK())
      call PDGEQRF(M,N,A,ione,ione,descA,TAU,WORK,LWORK,info)
      LWORK = -1
      deallocate(WORK)
      allocate(WORK())
      call PDGEQRF(M,N,K,A,ione,ione,descA,TAU,WORK,LWORK,info)
      LWORK = WORK(1)
      deallocate(WORK)
      allocate(WORK())
      call PDGEQRF(M,N,K,A,ione,ione,descA,TAU,WORK,LWORK,info)

      deallocate(WORK)

      call blacs_gridexit(ixtxt)

      return
      end subroutine smiDORGQR
      end module smiDORGQRMOD
