!  hamOper2Mat_Sparse.f90 
!
!  FUNCTIONS:
!  hamOper2Mat - Entry point of console application.
!

!**********************************************************************************************
!
!  PROGRAM: hamOper2Mat
!
!  PURPOSE: Hamiltonian matrix will be generated in the function.
!           Hamiltonian generated in this function is going to be Diagonalized using lapack.
!
!  STEPS  : 1. Kinetic operator will be generated using FRC%diffop.
!           2. Local potential will be generated from existing variables.
!           3. Nolocal potential will be generated using projectors and energy.
!           4. Hamiltonian will be got by adding the 3 Kinetic terms.
!
! INPUTS  : FRC, including 
!           FRC%diffop%Du, FRC%diffop%Dv, FRC%diffop%Dw, FRC%diffop%Duu, FRC%diffop%Dvv, FRC%diffop%Dww
!                Gradient Operators along 3 real space directions. These terms will be used to generate
!                Kinetic operator.
!           FRC%potential%VH   , hartree potential
!           FRC%potential%XCout, exchange correlation potential
!           FRC%potential%vps  , local psuedopotential
!           FRC%potential%vnl  , nonlocal potential
!           
!           kpt, k point coordinate
! 
! OUTPUT  : H, hamiltonian matrix.
!
!**********************************************************************************************

    module hamOper2Mat_Sparse_module
    contains
    subroutine hamOper2Mat_Sparse(FRC,kpt,H)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inversion_module
    use norm_2d_module
    use kron_module
    use diag_module
    use gatherLocalPotential_module
    use getKBorb_module
    use getKBorb_Sparse_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8                          :: kpt(3)
    
    ! temporary variables
    real*8  :: pi, k2, dr, latvec(3,3), kptt(1,3)
    integer :: iter, cgridn(3), n(3), nu, nv, nw, ii, jj

    real*8 , allocatable :: avec(:,:), bvec(:,:), tvec(:,:), J1(:,:)
    real*8 , allocatable :: Du(:,:), Dv(:,:), Dw(:,:), Duu(:,:), Dvv(:,:), Dww(:,:)
    real*8 , allocatable :: G1(:,:), G2(:,:), G3(:,:), L1(:,:), L2(:,:), L3(:,:), eye(:,:), Dtmp(:,:), lapl(:,:)
    real*8 , allocatable :: veff(:,:), kbedr(:,:), kbedrt(:,:)
    real*8 , allocatable :: J2(:,:), kptcart(:,:), kptcart2(:,:), tmp1(:,:), tmp2(:,:)
    complex*16, allocatable :: laplc(:,:), ctmp(:,:), Htmp(:,:), chilm(:,:), vnl(:,:), kbedrtc(:,:)
    complex*16, allocatable :: vnltmp(:,:), chilmtmp(:,:)
    
    character(len=1) :: transa, transb
    integer          :: mG1, nG2, kG1, LDA, LDB, LDC, job(8), nzmax, nnz, n1, info
    real*8           :: alpha, beta
    complex*16       :: calpha, cbeta
    integer   , allocatable :: ja(:), ia(:)
    complex*16, allocatable :: acsr(:)

    ! output variables
    complex*16, allocatable :: H(:,:)
    
    ! Body of hamOper2Mat
    !open(unit=11,file='hamOper2Mat.txt')

    !write(*,*) "kpt in hamOper2Mat."
    !write(*,*)  kpt
    allocate(kptcart(1,3))
    allocate(kptcart2(3,1))
    pi        = 3.1415926535897932385D0
    iter      = FRC%scloop
    allocate(avec(3,3))
    avec      = FRC%domain%latvec
    !write(*,*) "avec ="
    !write(*,*)  avec
    allocate(tvec(3,3))
    call inversion(avec,tvec)
    !write(*,*) "tvec in hamOper2Mat"
    !write(*,*)  tvec
    allocate(J1(3,3))
    J1        = transpose(tvec)
    !write(11,*) "J1 in hamOper2Mat"
    !write(11,*)  J1
    allocate(bvec(3,3))
    bvec      = 2.0D0*pi*J1
    !write(11,*) "bvec in hamOper"
    !write(11,*)  bvec

    kptt(1,:) = kpt
    kptcart   = matmul(kptt,bvec)
    !write(11,*) "kptcart ="
    !write(11,*)  kptcart
    allocate(J2(3,3))
    J2        = matmul(J1,tvec)
    !J2 = J1*tvec
    !write(*,*) "J2 in hamOper2Mat"
    !write(*,*)  J2
    !write(*,*) "kptcart before norm_2D in ham."
    !write(*,*)  kptcart
    call norm_2D(kptcart,2,k2)
    k2        = k2**2
    !write(11,*) "k2"
    !write(11,*)  k2
    kptcart2  = transpose(kptcart)
    kptcart2  = matmul(J1,kptcart2)
    kptcart   = transpose(kptcart2)
    !write(11,*) "kptcart after j1"
    !write(11,*)  kptcart


    allocate(Du(size(FRC%diffop%Du,1),size(FRC%diffop%Du,2)))
    allocate(Dv(size(FRC%diffop%Dv,1),size(FRC%diffop%Dv,2)))
    allocate(Dw(size(FRC%diffop%Dw,1),size(FRC%diffop%Dw,2)))
    allocate(Duu(size(FRC%diffop%Duu,1),size(FRC%diffop%Duu,2)))
    allocate(Dvv(size(FRC%diffop%Dvv,1),size(FRC%diffop%Dvv,2)))
    allocate(Dww(size(FRC%diffop%Dww,1),size(FRC%diffop%Dww,2)))
    Du        = FRC%diffop%Du
    Dv        = FRC%diffop%Dv
    Dw        = FRC%diffop%Dw
    Duu       = FRC%diffop%Duu
    Dvv       = FRC%diffop%Dvv
    Dww       = FRC%diffop%Dww
    !write(*,*)  "FRC%diffop%Dww"
    !write(*,*)   FRC%diffop%Dww
    !write(*,*) "FRC%diffop%Duu"
    !write(*,*)  FRC%diffop%Duu
    cgridn    = FRC%domain%cgridn
    n         = cgridn
    nu        = size(Du,1)
    nv        = size(Dv,1)
    nw        = size(Dw,1)
    !allocate(G1(nu*n(2)*n(3),nu*n(2)*n(3)))
    !allocate(G2(n(3)*nv*n(1),n(3)*nv*n(1)))
    !allocate(G3(nw*n(1)*n(2),nw*n(1)*n(2)))
    allocate(eye(n(2),n(2)))
    eye       = 0.0D0
    forall(ii=1:n(2))
        eye(ii,ii) = 1.00D0
    end forall
    !write(*,*) "I'm here before kron_Dtmp, in hamOper2Mat."
    call kron(eye,Du,Dtmp)
    !write(*,*) "size of Dtmp in hamOper2Mat."
    !write(*,*)  size(Dtmp,1), size(Dtmp,2)
    !write(*,*) "size of eye and Du"
    !write(*,*)  size(eye,1), size(eye,2), size(Du,1), size(Du,2)
    !write(*,*) "after call kron(eye,Du,Dtmp)." 
    !do ii = 1,size(Dtmp,1),1
    !    do jj = 1,size(Dtmp,2),1
    !        if (Dtmp(ii,jj) /= 0.0) then
    !                write(*,*) ii,jj,Dtmp(ii,jj)
    !        end if
    !    end do
    !end do

    deallocate(eye)
    allocate(eye(n(3),n(3)))
    eye     = 0.0D0
    forall(ii=1:n(3))
        eye(ii,ii) = 1.0D0
    end forall
    !write(*,*) "I'm here before kron_G1, in hamOper2Mat."
    call kron(eye,Dtmp,G1)
    !write(*,*) "I'm here after kron_G1, in hamOper2Mat."
    !write(*,*) "G1 = "
    !do jj = 1,size(G1,2),1
    !    do ii = 1,size(G1,1),1
    !    if (G1(ii,jj) /= 0.0) then
    !        write(*,*) ii, jj, G1(ii,jj)
    !    end if
    !    end do
    !end do
    deallocate(Dtmp)
    !write(*,*) "I'm here after deallocating Dtmp, in hamOper2Mat."
    deallocate(eye)
    allocate(eye(n(1),n(1)))
    eye     = 0.0D0
    forall(ii=1:n(1))
        eye(ii,ii) = 1.0D0
    end forall
    call kron(Dv,eye,Dtmp)
    deallocate(eye)
    allocate(eye(n(3),n(3)))
    eye     = 0.0D0
    forall(ii=1:n(3))
        eye(ii,ii) = 1.0D0
    end forall
    call kron(eye,Dtmp,G2)
    deallocate(Dtmp)
    
    deallocate(eye)
    allocate(eye(n(1)*n(2),n(1)*n(2)))
    eye     = 0.0D0
    forall(ii=1:n(1)*n(2))
        eye(ii,ii) = 1.0D0
    end forall
    call kron(Dw,eye,G3)
    deallocate(eye)
    !write(*,*) "I'm here after kron_G3 in hamOper2Mat"
    !write(*,*) "size of G*"
    !write(*,*)  size(G1,1), size(G1,2), size(G2,1), size(G2,2), size(G3,1), size(G3,2)
    allocate(eye(n(2),n(2)))
    eye     = 0.0D0
    forall(ii=1:n(2))
        eye(ii,ii) = 1.0D0
    end forall
    call kron(eye,Duu,Dtmp)
    !write(*,*) "eye in hamOper2Mat"
    !write(*,*)  eye
    !write(*,*) "Duu in hamOper2Mat"
    !write(*,*)  Duu
    deallocate(eye)
    allocate(eye(n(3),n(3)))
    eye     = 0.0D0
    forall(ii=1:n(3))
        eye(ii,ii) = 1.0D0
    end forall
    !write(*,*) "eye in hamOper2Mat"
    !write(*,*)  eye
    !write(*,*) "Dtmp in hamOper2Mat"
    !write(*,*)  Dtmp
    call kron(eye,Dtmp,L1)
    deallocate(Dtmp)
    !write(*,*) "I'm here after Kron_L1 in hamOper2Mat"
    !write(*,*) "L1 in hamOper2Mat"
    !do ii = 1,size(L1,1),1
    !    do jj = 1,size(L1,2),1
    !    if (L1(ii,jj) /= 0.0) then
    !        write(*,*)  ii,jj,L1(ii,jj)
    !    end if
    !    end do
    !end do
    deallocate(eye)
    allocate(eye(n(1),n(1)))
    eye     = 0.0D0
    forall(ii=1:n(1))
        eye(ii,ii) = 1.0D0
    end forall
    call kron(Dvv,eye,Dtmp)
    deallocate(eye)
    allocate(eye(n(3),n(3)))
    eye     = 0.0D0
    forall(ii=1:n(3))
        eye(ii,ii) = 1.0D0
    end forall
    call kron(eye,Dtmp,L2)
    deallocate(Dtmp)
    !write(*,*) "I'm here after Kron_L2 in hamOper2Mat"
    deallocate(eye)
    allocate(eye(n(1)*n(2),n(1)*n(2)))
    eye     = 0.0D0
    forall(ii=1:n(1)*n(2))
        eye(ii,ii) = 1.0D0
    end forall
    call kron(Dww,eye,L3)
    deallocate(eye)
    !write(*,*) "I'm here after Kron_L3 in hamOper2Mat"
    allocate(lapl(n(1)*n(2)*n(3),n(1)*n(2)*n(3)))
    !write(*,*) "J2 in hamOper2Mat."
    !write(*,*)  J2
    lapl = L1*J2(1,1)
    lapl = L2*J2(2,2)+lapl
    lapl = L3*J2(3,3)+lapl

    !write(*,*) "lapl after L"
    !do jj = 1,size(lapl,2),1
    !    do ii = 1,size(lapl,1),1
    !        if (lapl(ii,jj) /= 0.0) then
    !             write(*,*) ii, jj, lapl(ii,jj)
    !        end if
    !    end do
    !end do



    allocate(laplc(product(n),product(n)))
    allocate(tmp1(product(n),product(n)))
    allocate(tmp2(product(n),product(n)))
    !write(*,*) "I'm here after lapl in hamOper2Mat"
    !write(*,*) "kptcart in hamOper2mat."
    !write(*,*)  kptcart
    laplc = dcmplx(lapl)

    if (J2(1,2) /= 0.0 .or. J2(1,3) /= 0.0 .or. kptcart(1,1) /= 0.0) then
        !write(*,*) "I'm here lapl_1, case one."
        if (kptcart(1,1) /= 0.0) then
            !write(*,*) "I'm here lapl_1, case one_one."
            laplc = laplc + (0.0D0,2.0D0)*dcmplx(kptcart(1,1))*dcmplx(G1)
            !write(*,*) "I'm here lapl_1, after case one_one."
        end if
        !write(*,*) "I'm here lapl_1, after case one_one."
        !write(*,*) laplc(1,1)
        if (J2(1,2) /= 0.0) then
            !write(*,*) "I'm here lapl_1, case one_two."
            !write(*,*)  size(laplc,1), size(laplc,2), J2(1,2)
            !write(*,*)  size(G1,1), size(G1,2), size(G2,1), size(G2,2)
            !tmp1  = matmul(G1,G2) !*********** Use blas to do it ***********
            !tmp2  = matmul(G2,G1) !*********** Use blas to do it ***********
            transa = 'N'
            transb = 'N'
            mG1    =  size(G1,1)
            nG2    =  size(G2,2)
            kG1    =  size(G2,1)
            LDA    =  mG1
            LDB    =  KG1
            LDC    =  mG1
            alpha  =  1.0D0
            beta   =  0.0D0
            call DGEMM(transa,transb,mG1,nG2,kG1,alpha,G1,LDA,G2,LDB,beta,tmp1,LDC)
            mG1    =  size(G2,1)
            nG2    =  size(G1,2)
            kG1    =  size(G1,1)
            LDA    =  mG1
            LDB    =  KG1
            LDC    =  mG1
            call DGEMM(transa,transb,mG1,nG2,kG1,alpha,G2,LDA,G1,LDB,beta,tmp2,LDC)
            tmp1  = tmp1+tmp2
            laplc = laplc+dcmplx(J2(1,2))*dcmplx(tmp1)
            !laplc = laplc+cmplx(J2(1,2))*cmplx(matmul(G1,G2)+matmul(G2,G1))
            !write(*,*) "I'm here lapl_1, after case one_two."
        end if
        !write(*,*) "I'm here lapl_1, after case one_two."
        !write(*,*) laplc(1,1)
        if (J2(1,3) /= 0.0) then
            !write(*,*) "I'm here lapl_1, case one_three."
            !tmp1  = matmul(G1,G3)
            !tmp2  = matmul(G3,G1)
            transa = 'N'
            transb = 'N'
            mG1    =  size(G1,1)
            nG2    =  size(G3,2)
            kG1    =  size(G3,1)
            LDA    =  mG1
            LDB    =  KG1
            LDC    =  mG1
            alpha  =  1.0D0
            beta   =  0.0D0
            call DGEMM(transa,transb,mG1,nG2,kG1,alpha,G1,LDA,G3,LDB,beta,tmp1,LDC)
            mG1    =  size(G3,1)
            nG2    =  size(G1,2)
            kG1    =  size(G1,1)
            LDA    =  mG1
            LDB    =  KG1
            LDC    =  mG1
            call DGEMM(transa,transb,mG1,nG2,kG1,alpha,G3,LDA,G1,LDB,beta,tmp2,LDC)
            tmp1  = tmp1+tmp2
            laplc = laplc+dcmplx(J2(1,3))*dcmplx(tmp1)
            !laplc = laplc+cmplx(J2(1,3))*cmplx(matmul(G1,G3)+matmul(G3,G1))
            !write(*,*) "I'm here lapl_1, after case one_three."
        end if
        !write(*,*) "I'm here lapl_1, after case one_three."
        !write(*,*) laplc(1,1)
    end if
        !write(*,*) "I'm here lapl_1, case two."
        !laplc = cmplx(lapl)
    !
    !write(*,*) "I'm here after lapl_1 in hamOper2Mat"
    if (J2(2,3) /= 0.0 .or. kptcart(1,2) /= 0.0) then
        if (kptcart(1,2) /= 0) then
            laplc = laplc+(0.0D0,2.0D0)*dcmplx(kptcart(1,2))*dcmplx(G2)
        end if
        !write(*,*) "I'm here lapl_1, after case two_one."
        !write(*,*) laplc(1,1)
        if (J2(2,3) /= 0.0) then
            !tmp1  = matmul(G2,G3)
            !tmp2  = matmul(G3,G2)
            transa = 'N'
            transb = 'N'
            mG1    =  size(G2,1)
            nG2    =  size(G3,2)
            kG1    =  size(G3,1)
            LDA    =  mG1
            LDB    =  KG1
            LDC    =  mG1
            alpha  =  1.0D0
            beta   =  0.0D0
            call DGEMM(transa,transb,mG1,nG2,kG1,alpha,G2,LDA,G3,LDB,beta,tmp1,LDC)
            mG1    =  size(G3,1)
            nG2    =  size(G2,2)
            kG1    =  size(G2,1)
            LDA    =  mG1
            LDB    =  kG1
            LDC    =  mG1
            call DGEMM(transa,transb,mG1,nG2,kG1,alpha,G3,LDA,G2,LDB,beta,tmp2,LDC)
            tmp1  = tmp1+tmp2
            laplc = laplc+dcmplx(J2(2,3))*dcmplx(tmp1)
            !laplc = laplc+cmplx(J2(2,3))*cmplx(matmul(G2,G3)+matmul(G3,G2))
        end if
        !write(*,*) "I'm here lapl_1, after case two_two."
        !write(*,*) laplc(1,1)
    end if
    !write(*,*) "I'm here after lapl_2 in hamOper2Mat"
    if (kptcart(1,3) /= 0.0) then
        laplc = laplc+(0.0D0,2.0D0)*dcmplx(kptcart(1,3))*dcmplx(G3)
    end if
    !write(*,*) "I'm here lapl_1, after case three."
    !write(*,*) laplc(1,1)
    
    if (k2 /= 0.0) then
        allocate(ctmp(size(laplc,1),size(laplc,2)))
        forall(ii=1:size(laplc,1))
            ctmp(ii,ii) = (1.0D0,0.0D0)
        end forall
        
        laplc = laplc-dcmplx(k2)*ctmp
    end if
    !write(*,*) "I'm here lapl_1, after case four."
    !write(*,*) laplc(1,1)

    !write(*,*) "laplc in hamOper2Mat"
    open(unit=11,file='laplc.txt')
    do jj = 1,size(laplc,2),1
        do ii = 1,size(laplc,1),1
            if (laplc(ii,jj) /= (0.0,0.0)) then
                write(11,*) laplc(ii,jj)
            end if
        end do
    end do
    close(11)



    !write(*,*) "I'm here after lapl_3 in hamOper2Mat"
    call gatherLocalPotential(FRC)
    !write(*,*) "I'm here after gatherLocalPotential in hamOper2Mat"
    !write(*,*) "Size of veff"
    !write(*,*)  size(veff,1), size(veff,2)
    !deallocate(veff)
    !write(*,*) "I'm here after deallocate veff."
    !write(*,*) "FRC%potential%vloc = "
    open(unit=11,file='vloc.txt')
    do ii = 1,size(FRC%potential%vloc),1
    write(11,*)  FRC%potential%vloc(ii,1)
    end do
    close(11)


    !write(*,*) "stop in hamOper2mat."
    !stop


    call diag(FRC%potential%vloc,veff)
    !write(*,*) "Size of veff"
    !write(*,*)  size(veff,1), size(veff,2)
    !write(*,*) "I'm here after diag in hamOper2Mat"
    !deallocate(FRC%potential%vnl%KBorb)
    !write(*,*) "kpt in hamOper2mat"
    !write(*,*)  kpt
    if (FRC%potential%vnl%KBorbIsSparse) then
            call GetKBorb_Sparse(FRC,kpt)
    else
            call GetKBorb(FRC,kpt)
    end if
    !write(*,*) "I'm here after GetKBorb in hamOper2Mat"
    !write(*,*) "FRC%potential%vnl%KBorb"
    !open(unit=11,file='kborb.txt')
    !do ii = 1,size(FRC%potential%vnl%KBorb,2)
    !    do jj = 1,size(FRC%potential%vnl%KBorb,1)
    !    if (FRC%potential%vnl%KBorb(jj,ii) /= 0.0) then
    !        write(11,*)  FRC%potential%vnl%KBorb(jj,ii)
    !    end if
    !    end do
        !write(*,*)  FRC%potential%vnl%KBorb(:,ii)
    !end do
    !close(11)
    latvec = FRC%domain%latvec
    dr     = latvec(1,1)*latvec(2,2)*latvec(3,3)  &
            +latvec(1,2)*latvec(2,3)*latvec(3,1)  &
            +latvec(1,3)*latvec(2,1)*latvec(3,2)  &
            -latvec(1,1)*latvec(2,3)*latvec(3,2)  &
            -latvec(1,2)*latvec(2,1)*latvec(3,3)  &
            -latvec(1,3)*latvec(2,2)*latvec(3,1)
    dr     = dr/dble(product(cgridn))
    allocate(kbedr(size(FRC%potential%vnl%KBEnergy),1))
    kbedr(:,1)  = FRC%potential%vnl%KBEnergy*dr
    !write(*,*) "kbedr in hamOper2mat."
    !write(*,*)  kbedr


    open(unit=11,file='vnl_hamOper.txt')
    if (FRC%potential%vnl%KBorbIsSparse) then
            !write(*,*) "Here sparse."
            allocate(chilm(FRC%potential%vnl%sparsekborb%SIZE1,FRC%potential%vnl%sparsekborb%SIZE2))
            nzmax  = FRC%potential%vnl%sparsekborb%N_nonzero
            job(1) = 1
            job(2) = 1
            job(3) = 1
            job(4) = 2
            job(5) = nzmax
            job(6) = 0
            nnz    = nzmax
            n1     = FRC%potential%vnl%sparsekborb%SIZE1
            allocate(acsr(nzmax),ja(nzmax),ia(FRC%potential%vnl%sparsekborb%SIZE1+1))
            call mkl_zcsrcoo(job,n1,acsr,ja,ia,nnz,                &
                             FRC%potential%vnl%sparsekborb%ValueN, &
                             FRC%potential%vnl%sparsekborb%Index1, &
                             FRC%potential%vnl%sparsekborb%Index2, info)
            call mkl_zdnscsr(job,FRC%potential%vnl%sparsekborb%SIZE1, &
                                 FRC%potential%vnl%sparsekborb%SIZE2, &
                           chilm,FRC%potential%vnl%sparsekborb%SIZE1, &
                           acsr,ja,ia,info)
            write(11,*) "kborbsparse ="
            do ii = 1,nzmax,1
            write(11,*) FRC%potential%vnl%sparsekborb%Index1(ii), &
                        FRC%potential%vnl%sparsekborb%Index2(ii), &
                        FRC%potential%vnl%sparsekborb%ValueN(ii)
            end do
            write(11,*) "acsr ="
            do ii = 1,nzmax,1
            write(11,*) ja(ii), acsr(ii)
            end do
    else
            allocate(chilm(size(FRC%potential%vnl%KBorb,1),size(FRC%potential%vnl%KBorb,2)))
            chilm  = FRC%potential%vnl%KBorb
    end if
    !write(*,*) "kbedr before diag."
    !write(*,*)  kbedr
    call diag(kbedr,kbedrt)

    !write(*,*) "kbedrt in hamOper2mat."
    !do ii = 1,size(kbedrt,1),1
    !    write(*,*) kbedrt(ii,:)
    !end do


    allocate(kbedrtc(size(kbedrt,1),size(kbedrt,2)))
    kbedrtc = dcmplx(kbedrt)
    !write(*,*) "I'm here after diag kbedr"
    !allocate(H(n(1)*n(2)*n(3),n(1)*n(2)*n(3)))
    !write(*,*) "size of chilm"
    !write(*,*)  size(chilm,1), size(chilm,2)
    allocate(chilmtmp(size(chilm,2),size(chilm,1)))
    !write(*,*) "I'm here after allocate chilmtmp."
    chilmtmp = transpose(chilm)
    chilmtmp = conjg(chilmtmp)
    !write(*,*) "I'm here after chilmtmp in hamoper2mat."
    !write(*,*) "chilmtmp in hamOper2mat."
    !do ii = 1,size(chilmtmp,2),1
    !    do jj = 1,size(chilmtmp,1),1
    !    if (chilmtmp(jj,ii) /= (0.0,0.0)) then
    !        write(*,*) jj,ii,chilmtmp(jj,ii)
    !    end if
    !    end do
        !write(*,*) chilmtmp(ii,:)
    !end do

    allocate(vnl(n(1)*n(2)*n(3),n(1)*n(2)*n(3)))
    allocate(vnltmp(size(kbedrtc,1),size(chilmtmp,2)))
    !write(*,*) "size of vnltmp"
    !write(*,*)  size(vnltmp,1),size(vnltmp,2)
    transa = 'N'
    transb = 'N'
    mG1    =  size(kbedrtc,1)
    nG2    =  size(chilmtmp,2)
    kG1    =  size(chilmtmp,1)
    LDA    =  mG1
    LDB    =  kG1
    LDC    =  mG1
    calpha = (1.0D0,0.0D0)
    cbeta  = (0.0D0,0.0D0)
    call ZGEMM(transa,transb,mG1,nG2,kG1,calpha,kbedrtc,LDA,chilmtmp,LDB,cbeta,vnltmp,LDC)
    !vnltmp = matmul(kbedrtc,chilmtmp)
    !write(*,*) "vnltmp ="
    !do ii = 1,size(vnltmp,2),1
    !    do jj = 1,size(vnltmp,1),1
    !    if (chilmtmp(jj,ii) /= (0.0,0.0)) then
    !        write(*,*) jj,ii,chilmtmp(jj,ii)
    !    end if
    !    end do
        !write(*,*) chilmtmp(ii,:)
    !end do
    mG1    = size(chilm,1)
    nG2    = size(vnltmp,2)
    kG1    = size(vnltmp,1)
    LDA    = mG1
    LDB    = kG1
    LDC    = mG1
    call ZGEMM(transa,transb,mG1,nG2,kG1,calpha,chilm,LDA,vnltmp,LDB,cbeta,vnl,LDC)
    !vnl    = matmul(chilm,vnltmp)
    !open(unit=11,file='vnl_hamOper.txt')
    write(11,*) "vnl in hamOper2mat"
    do ii = 1,size(vnl,1),1
        do jj = 1,size(vnl,2),1
            if (vnl(ii,jj) /= 0.0) then
                write(11,*) ii, jj, vnl(ii,jj)
            end if
        end do
    end do
    close(11)
    !vnl    = matmul(chilm,matmul(kbedr,transpose(chilm)))
    !write(*,*) "I'm here after getting vnl"
    !write(*,*) "size of veff and vnl, laplc"
    !write(*,*)  size(veff), size(vnl), size(laplc)
    H      = (-0.5D0,0.0D0)*laplc + dcmplx(veff) + vnl
    !write(*,*) "I'm here after getting H"
    allocate(Htmp(n(1)*n(2)*n(3),n(1)*n(2)*n(3)))
    Htmp   =  transpose(H)
    Htmp   =  conjg(Htmp)
    H      =  (0.5D0,0.0D0)*(H+Htmp)
    !H      =  (0.5,0.0)*(H+transpose(H))
    !write(*,*) "I'm here after H in hamOper2Mat"
    !write(*,*) "size of H"
    !write(*,*)  size(H,1), size(H,2)
    !write(*,*) "H ="
    open(unit=11,file='H.txt')
    do ii = 1,size(H,2),1
        do jj = 1,size(H,1),1
            !if (H(jj,ii) /= (0.0D0,0.0D0)) then
                write(11,*) jj,ii,H(jj,ii)
            !end if
        end do
    end do
    close(11)
    !write(*,*) "stop in hamOper2mat."
    !close(11)
    !stop
    return
    end subroutine hamOper2Mat_Sparse
    end module hamOper2Mat_Sparse_module

