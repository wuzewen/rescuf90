! smiDGEEV.f90

!*****************************************
      !
      !
      !
!***************************************************

      module smiDGEEVMOD
      contains
      subroutine smiDGEEV(jobz,N,A,W,Z,nprocs,MNB,B,nprow,npcol)

      implicit none

      character(len=1) :: jobz
      integer :: N, MB, NB, nprocs, nprow, npcol
      real*8, allocatable :: A(), W(), Z()

      character(len=1) :: UPLO
      real*8 :: VL, VU, ABSTOL, ORFAC
      integer :: IL, IU, M, NZ, INFO
      real*8, allocatable :: GAP(:)
      integer, allocatable :: IFAIL(:), ICLUSTR(:)

      UPLO   = 'U'
      IL     =  13
      IU     = -13
      ABSTOL = -1.0D0
      ORFAC  =  0.000001D0
      LWORK  = -1
      LIWORK = -1
      allocate(IFAIL(N), ICLUSTER(2*nprow*npcol))
      allocate(GAP(nprow*npcol))
      iam    =  0
      ictxt  =  0
      myrow  =  0
      mycol  =  0
      call blacs_pinfo(iam,mprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      A_np = 
      call numroc(N,MB,myrow,izero,nprow,Z_np)
      Z_np = 

      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,INFO)
      call descinit(descZ,N,N,MB,NB,izero,izero,ictxt,Z_np,INFO)

      allocate(WORK())
      allocate(IWORK())

      call PDSYEV(jobz,UPLO,N,A,ione,ione,descA,W,Z,ione,ione,&
              descZ,WORK,LWORK,INFO)
      LWORK = WORK(1)
      deallocate(WORK)
      allocate(WORK)
      LIWORK = IWAORK(1)
      deallocate(LIWORK)
      allocate(LIWORK)

      call PDSYEV(jobz,UPLO,N,A,ione,ione,descA,W,Z,ione,ione,&
              descZ,WORK,LWORK,INFO)

      if (INFO .NE. 0) then
          write(*,*) 'Error in smiDGEEV'
      end if

      deallocate(WORK,IWORK)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiDGEEV
      end module smiDGEEVMOD
