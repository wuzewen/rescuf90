! spline.f90
    
!*****************************************************************
    !
    ! This is a function to do interplation with spline method.
    ! While X is the coordinate, it should be an Array.
    ! Y is the value locate at X, it is an array, too.
    ! ddy0 is the 2nd-order derivation if Y at the first point.
    ! ddye is the 2nd-order derivation if Y at the last point.
    ! Xin is the coordinates at where the interplations should
    ! be calculated. And S is the result.
    ! while the first and second order dirivation can also be 
    ! calculated, but will not be outputed.
    !
!*****************************************************************
    
    module spline_module
    contains
    subroutine spline(X,Y,ddY0,ddYe,Xin,S)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: X(:), Y(:), Xin(:)
    real*8              :: ddY0, ddYe
    
    ! temporary variables
    integer             :: N, M, ii, jj
    real*8, allocatable :: dY(:), ddY(:), dS(:), ddS(:), H(:)
    real*8              :: T, H0, H1, beta, alpha
    
    ! output variables
    real*8, allocatable :: S(:)
    
    ! body of this function
    N = size(X)
    M = size(Xin)
    allocate(dY(N),ddY(N),H(N))
    allocate(dS(M),ddS(M))
    
    !new part of boundary condition.
    ddY0  = (Y(3)+Y(1))/((X(2)-X(1))*(X(3)-X(1)))
    ddYe  = (Y(N)+Y(N-2))/((X(N)-X(N-1))*(X(N-1)-X(N-2)))

    dY(1) = -0.5D0
    H0    = X(2)-X(1)
    H(1)  = 3.0D0*(Y(2)-Y(1))/(2.0D0*H0)-ddY0*H0/4.0D0
    
    do ii = 2,N-1,1
        H1     =  X(ii+1) - X(ii)
        alpha  =  H0/(H0+H1)
        beta   = (1.0D0-alpha)*(Y(ii)-Y(ii-1))/H0
        beta   =  3.0D0*(beta+alpha*(Y(ii+1)-Y(ii))/H1)
        dY(ii) = -alpha/(2.0D0+(1.0D0-alpha)*dY(ii-1))
        H(ii)  =  beta-(1.0D0-alpha)*H(ii-1)
        H(ii)  =  H(ii)/(2.D0+(1.0D0-alpha)*dY(ii-1))
        H0     =  H1
    end do
    
    dY(N) = (3.0D0*(Y(N)-Y(N-1))/H1 + ddYe*H1/2.0D0 - H(N-1))/(2.0D0+dY(N-1))
    
    do ii = N-1,1,-1
        dY(ii) = dY(ii)*dY(ii+1)+H(ii)
    end do
    
    do ii = 1,N-1,1
        H(ii) = X(ii+1)-X(ii)
    end do
    
    do ii = 1,N-1,1
        H1      = H(ii)*H(ii)
        ddy(ii) = 6.0D0*(Y(ii+1)-Y(ii))/H1-2.0D0*(2.0D0*dY(ii)+dY(ii+1))/H(ii)
    end do
    
    H1 = H(N-1)*H(N-1)
    ddY(N) = 6.0D0*(Y(N-1)-Y(N))/H1+2.0D0*(2.0D0*dY(N)+dY(N-1))/H(N-1)
    
    T  = 0.0D0
    do ii = 1,N-1,1
        H1 = 0.5D0*H(ii)*(Y(ii)+Y(ii+1))
        H1 = H1-H(ii)*H(ii)*H(ii)*(ddY(ii)+ddY(ii+1))/24.0D0
        T  = T+H1
    end do
    
    do ii = 1,M,1
        if (Xin(ii) >= X(N)) then
            jj = N-1
            write(*,*) "Xin >= X"
            write(*,*) Xin(ii), X(N)
            stop
        else
            jj = 1
            do while (Xin(ii) > X(jj+1))
                jj = jj+1
            end do
        end if
        H1      = (X(jj+1)-Xin(ii))/H(jj)
        S(ii)   = (3.0D0*H1*H1-2*H1*H1*H1)*Y(jj)
        S(ii)   = S(ii)+H(jj)*(H1*H1-H1*H1*H1)*dY(jj)
        DS(ii)  = 6.0D0*(H1*H1-H1)*Y(jj)/H(jj)
        DS(ii)  = DS(ii)+(3.0D0*H1*H1-2.0D0*H1)*dY(jj)
        ddS(ii) = (6.0D0-12.0D0*H1)*Y(jj)/(H(jj)*H(jj))
        ddS(ii) = ddS(ii)+(2.0D0-6.0D0*H1)*dY(jj)/H(jj)
        H1      = (Xin(ii)-X(jj))/H(jj)
        S(ii)   = S(ii)+(3.0D0*H1*H1-2.0D0*H1*H1*H1)*Y(jj+1)
        S(ii)   = S(ii)-H(jj)*(H1*H1-H1*H1*H1)*dY(jj+1)
        dS(ii)  = dS(ii)-6.0D0*(H1*H1-H1)*Y(jj+1)/H(jj)
        dS(ii)  = dS(ii)+(3.0D0*H1*H1-2.0D0*H1)*dY(jj+1)
        ddS(ii) = ddS(ii)+(6.0D0-12.0D0*H1)*Y(jj+1)/(H(jj)*H(jj))
        dds(ii) = ddS(ii)-(2.0D0-6.0D0*H1)*dY(jj+1)/H(jj)
    end do
    
    return
    end subroutine spline
    end module spline_module
    
