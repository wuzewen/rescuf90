! flipud.f90
    
!****************************************************************
    !
    ! this function has the same function with function flipud
    ! in matlab.
    !
!****************************************************************
    
    module flipud_module
    contains
    subroutine flipud(A,B)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    
    ! temporary variables
    integer             :: n, m, ii
    
    ! output variables
    real*8, allocatable :: B(:,:)
    
    ! body of this function
    n = size(A,1)
    m = size(A,2)
    allocate(B(n,m))
    do ii = 1,n,1
        B(ii,:) = A(n+1-ii,:)
    end do
    
    return
    end subroutine flipud
    end module flipud_module