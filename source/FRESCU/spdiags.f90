! spdiags.f90
!**********************************************************************
    ! This function has the same function with the function spdiags
    ! in matlab. MatrixA should be a two dimentional matrix, and 
    ! ArrayB should be an one dimentional array. dim1 and dim2 is 
    ! the size of the output matrix MatrixC
!**********************************************************************
    
    module spdiags_module
    contains
    subroutine spdiags(MatrixA,ArrayB,dim1,dim2,MatrixC)
    
    implicit none
    
    ! input valuables
    real*8, allocatable  :: MatrixA(:,:)
    real*8, allocatable  :: ArrayB(:)
    integer              :: dim1, dim2
    
    ! temporery valuables
    integer              :: Melement
    integer              :: N
    integer              :: i, j
    integer, allocatable :: ArrayT(:)
    
    ! output valuables
    real*8, allocatable :: MatrixC(:,:)
    
    ! body of this function
    N = size(MatrixA,2)
    allocate(ArrayT(N))
    allocate(MatrixC(dim1,dim2))
    MatrixC(:,:) = 0.0D0
    do i = 1,dim1,1
        ArrayT = MatrixA(i,:)
        ArrayT = cshift(ArrayT,int(-ArrayB(i)))
        do j = 1,dim2,1
            if (j+ArrayB(i)<1 .or. j+ArrayB(i)>N) then
                Melement  = 0
            else
                Melement = ArrayT(j)
            end if
            MatrixC(i,j) = Melement
        end do
    end do
    
    return
    end subroutine spdiags
    end module spdiags_module
    
