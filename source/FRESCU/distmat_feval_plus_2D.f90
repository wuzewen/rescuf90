! distmat_feval_plus_2D.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module distmat_feval_plus_2D_module
    contains
    subroutine distmat_feval_plus_2D(FRC,dA,dB,dC)
    
    use VataMN_2D_module
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ModBCDist_2D_module
    use distmat_issame_2D_module
    
    implicit none
    
    ! intput vatiables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_2D)                 :: dA, dB
    
    ! temporary variables
    integer :: mpisize
    logical :: issame
    
    ! output variables
    type(VataMN_2D)                 :: dC
    
    ! body of this function
    mpisize = FRC%mpi%mpisize
    call distmat_issame_2D(dA,dB,issame)
    !write(*,*) 'issame', issame
    !write(*,*) 'dA', dA%m, dA%n, dA%mblock, dA%nblock, dA%mproc, dA%nproc
    !write(*,*) size(dA%vata,1), size(dA%vata,2)
    !write(*,*) 'dB', dB%m, dB%n, dB%mblock, dB%nblock, dB%mproc, dB%nproc
    !write(*,*) size(dB%vata,1), size(dB%vata,2)
    if (mpisize == 1 .or. issame) then
        dC = dB
    else
        call ModBCDist_2D(FRC,dB,dA%mblock,dA%nblock,dA%mproc,dA%mproc,.FALSE.,.FALSE.,dC)
    end if
    
    dC%vata = dA%vata+dC%vata
    
    return
    end subroutine distmat_feval_plus_2D
    end module distmat_feval_plus_2D_module
