! setdiff.f90
    
!*******************************************************************
    !
    !
    !
!*******************************************************************
    
    module setdiff_module
    contains
    subroutine setdiff(A,B,inde)
    
    implicit none
    
    ! input variables
    integer, allocatable :: A(:), B(:)
    
    ! temporary variables
    integer, allocatable :: Atmp1(:), Atmp2(:), indetmp(:)
    integer              :: nA, nB, mtmp, ntmp, ii, jj
    
    ! output variables
    integer, allocatable :: inde(:)
    
    ! body of this fucntion
    nA = size(A)
    nB = size(B)
    
    if (nA == nB) then
        if (all(A == B)) then
            allocate(inde(0))
            return
        end if
    end if
    
    
    allocate(Atmp1(1))
    Atmp1 = A(1)
    mtmp  = 0
    if (nA == 1) then
        if (any(B == Atmp1(1))) then
            allocate(inde(1))
            inde = A(1)
        end if
    else
        mtmp = 0
        if (all(B /= Atmp1(1))) then
            mtmp = mtmp+1
            allocate(inde(1))
            inde(1) = A(1)
        end if
            
        ntmp = 1
        do ii = 2,nA,1
            if (all(Atmp1 /= A(ii))) then
                ntmp = ntmp+1
                allocate(Atmp2(ntmp))
                forall(jj=1:ntmp-1)
                    Atmp2(jj) = Atmp1(jj)
                end forall
                deallocate(Atmp1)
                allocate(Atmp1(ntmp))
                Atmp1 = Atmp2
                deallocate(Atmp2)
                
                if (all(B /= A(ii))) then
                    mtmp = mtmp+1
                    allocate(indetmp(mtmp))
                    if (mtmp == 1) then
                        allocate(inde(1))
                    else
                        forall(jj=1:mtmp-1)
                            indetmp(jj) = inde(jj)
                        end forall
                    end if
                    indetmp(mtmp) = A(ii)
                    deallocate(inde)
                    allocate(inde(mtmp))
                    inde = indetmp
                    deallocate(indetmp)
                end if
            end if
        end do
    end if
    
    return
    end subroutine setdiff
    end module setdiff_module