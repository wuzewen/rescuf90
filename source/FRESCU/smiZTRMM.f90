! smiZTRMM.f90

!*********************************************
      !
      !
      !
!***********************************************

      module smiZTRMMMOD
      contains
      subroutine smiZGEMM(side,UPLO,trans,alpha,A,C, &
                      nprocs,MA,NA,MBA,NBA,nprowA,npcolA, &
                      MC,NC,MBC,NBC,nprowC,npcolC)

      implicit none

      integer          :: nprocs
      integer          :: MA, NA, MBA, NBA, nprowA, npcolA
      !integer          :: MB, NB, MBB, NBB, nprowB, npcolB
      integer          :: MC, NC, MBC, NBC, nprowC, npcolC
      character(len=1) :: trans!a, transb
      complex*16       :: alpha!, beta
      complex*16, allocatable :: A(:,:), B(:,:), C(:,:)

      integer :: iam, ictxtA, ictxtB, ictxtC, K
      integer :: myrowA, mycolA, myrowB, mycolB, myrowC, mycolC
      character(len=1) :: nchar

      iam    =  0
      ictxtA =  0
      ictxtB =  0
      ictxtC =  0
      myrowA =  0
      mycolA =  0
      myrowB =  0
      mycolB =  0
      myrowC =  0
      mycolC =  0
      nchar  = 'n'
      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxtA)
      call blacs_gridinit(ictxtA,'Row-major',nprowA,npcolA)
      call blacs_gridinfo(ictxtA,nprowA,npcolA,myrowA,mycolA)

      !call blacs_get(-1,0,ictxtB)
      !call blacs_gridinit(ictxtB,'Row-major',nprowB,npcolB)
      !call blacs_gridinfo(ictxtB,nprowB,npcolB,myrowB,mycolB)

      call blacs_get(-1,0,ictxtC)
      call blacs_gridinit(ictxtC,'Row-major',nprowC,npcolC)
      call blacs_gridinfo(ictxtC,nprowC,npcolC,myrowC,mycolC)

      izero = 0
      ione  = 1
      call numroc(MA,MBA,myrowA,izero,nprowA,A_np)
      A_np = 
      !call numroc(MB,MBB,myrowB,izero,nprowB,B_np)
      !B_np =
      call numroc(MC,MBC,myrowC,izero,nprowC,C_np)
      C_np =

      !K = 
      call descinit(descA,MA,NA,MBA,NBA,izero,izero,ictxtA,A_np,infoA)
      !call descinit(descB,MB,NB,MBB,NBB,izero,izero,ictxtB,B_np,infoB)
      call descinit(descC,MB,NB,MBC,NBC,izero,izero,ictxtC,C_np,infoC)

      call PZTRMM(side,UPLO,trans,diag,MC,NC,K,alpha,A,ione,ione,descA,&
              C,ione,ione,descC)
      call blacs_gridexit(ictxtA)
      !call blacs_gridexit(ictxtB)
      call blacs_gridexit(ictxtC)

      return
      end subroutine smiZTRMM
      end module smiZTRMMMOD
