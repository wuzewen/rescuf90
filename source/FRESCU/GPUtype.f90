! GPUtype_module
    
!***********************
    !
    !
    !
!***********************
    
    module GPUtype_module
    
    type :: GPUtype
        logical              :: status
        integer              :: GPUperNode
        integer              :: ProcPerNode
    end type GPUtype
    
    end module GPUtype_module