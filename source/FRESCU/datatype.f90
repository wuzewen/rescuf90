! datatype.f90
    
    module dataType_module
    
    use VataMN_2D_module
    
    type :: orbiType
        integer  :: rr, dr, fr, qq, fq, qw
    end type orbiType
    
    type :: SummeryOfBas
        integer  :: Vloc, Vlocrr, Vlocdr, Vlocvv
        integer  :: Rloc, Rlocrr, Rlocdr, Rlocrho
        integer  :: Vnl,  Vnlrr,  Vnldr,  Vnlvv,   Vnlqq,  Vnlfq, Vnlqw
        integer  :: Vna,  Vnarr,  Vnadr,  Vnavv
        integer  :: Rna,  Rnarr,  Rnadr,  Rnarho
        integer  :: PseP, PsePrr, PsePdr, PsePvvS, PsePvvU
        integer  :: OrbitalSet
        type(orbiType), allocatable :: orbit(:)
    end type SummeryOfBas
    
    type :: VlocalType
        real*8               :: Rcore
        integer              :: Ncore
        real*8, allocatable  :: rrData(:), drData(:), vvData(:)
    end type VlocalType
    
    type :: RlocalType
        real*8, allocatable  :: rrData(:), drData(:), rhoData(:)
    end type RlocalType
    
    type :: VnlType
        real*8, allocatable :: rrData(:), drData(:), vvData(:), qqData(:), fqData(:), qwData(:)
        real*8  :: L
        real*8  :: KBenergy, KBcosine, E
        real*8  :: isGhost
    end type VnlType
    
    type :: VnaType
        real*8, allocatable :: rrData(:), drData(:), vvData(:)
    end type VnaType
    
    type :: RnaType
        real*8, allocatable :: rrData(:), drData(:), rhoData(:)
    end type RnaType
            
    type :: RelPseudoPtype
        real*8               :: N, L
        real*8               :: J, Rc
        real*8, allocatable  :: rrData(:), drData(:), vvData_screened(:), vvData_unscreened(:)
    end type RelPseudoPtype
            
    type :: OrbitalSetType
        real*8, allocatable :: rrData(:), drData(:), frData(:), qqData(:), fqData(:), qwData(:)
        real*8              :: N, L
        real*8              :: E, Population, CoulombEnergyU, ExchangeEnergyJ
        character(len=20)   :: OrbitalType
    end type OrbitalSetType
            
    
        
    type :: ElementAtomType
        integer           :: Z, N
        character(len=4)  :: Symbol
        character(len=20) :: shell
        character(len=20) :: name
        character(len=20) :: mass
    end type ElementAtomType
    
    type :: ElementDataType
        character(len=4)                  :: Symbol
        type(ElementAtomType)             :: ElementAtom
        type(RelPseudoPtype), allocatable :: RelPseudoP(:)
        type(VlocalType)                  :: Vlocal
        type(RlocalType)                  :: Rlocal
        type(OrbitalSetType), allocatable :: OrbitalSet(:)
        type(VnlType),        allocatable :: Vnl(:)
        type(VnaType)                     :: Vna
        type(RnaType)                     :: Rna
    end type ElementDataType
    
    
    type :: mpi_type
        logical :: status
        integer :: rank
        integer :: mpisize
    end type mpi_type
    
    type :: domain_type
        integer :: fgridn(3)
        real*8  :: latvec(3,3)
    end type domain_type
    
    type :: atom_type
        integer :: element(4)
        real*8  :: xyz(4,3)
        integer :: numberOfAtom 
        integer :: numberOfElement
    end type atom_type
    
    type :: smi_type
        logical :: status
    end type smi_type
    
    type :: vna_type
        real*8, allocatable  :: dataA(:)
        integer              :: m, mblock, mproc
        integer              :: n, nblock, nproc
    end type vna_type
        
    type :: potential_type
        logical :: fourierinit
        type(VataMN_2D) :: vna
    end type potential_type
    
    type :: option_type
        logical :: initParaReal
    end type option_type
    
    
    type :: FRC_type
        type(mpi_type)                     :: mpi
        type(smi_type)                     :: smi
        type(domain_type)                  :: domain
        type(atom_type)                    :: atom
        type(potential_type)               :: potential
        type(option_type)                  :: option
        type(ElementDataType), allocatable :: ElementData(:)
        type(SummeryOfBas), allocatable    :: SOB(:)
    end type FRC_type
    
    
    end module dataType_module
