! MPI_Send_variable.f90

!**********************************************************
      !
      !
      !
!**********************************************************

      module MPI_Send_variable_module
      contains
      subroutine MPI_Send_variable(vari,dest,tag,bloc)

      include 'mpif.h'

      ! input variables

      ! temporary variables

      ! output variables


      ! body of this funtion
      varinfo     = 0
      varinfo(1)  = 0
      issp        = varinfo(1)
      varinfo(2)  = 1
      isrl        = varinfo(2)
      varinfo(3)  = bloc
      varinfo(4)  = count(vari /= 0.0)
      vaeinfo(5)  = 2
      varinfo(6)  = size(vari,1)
      varinfo(7)  = size(vari,2)
      varisize(1) = varinfo(6)
      varisize(2) = varinfo(7)

      call mex_send(varinfo,16,dest,tag)
      tag         = tag + 1
      kk          = 0

      if (issp) then
              N
      else
              varn = size(vari)
      end if

      do while (kk < varn)
          ntmp = minval(kk+bloc,varn)
          forall(ii = kk+1:ntmp)
                  inde(ii-kk) = ii
          end forall
          if (issp .and. isrl) then
                  forall(ii=1:ntmp-kk)
                          buf(ii)         = ind(inde(ii))
                          buf(ii+ntmp-kk) = val(inde(ii))
                  end forall
          else

