! calcSelectedAtomList.f90

!********************************************
      !
      !
      !
!********************************************

      module calcSelectedAtomListMod
      contains
      subroutine calcSelectedAtomList(mpirank,distmat,atomInd,nt,atomList)

      implicit none

      natom = maxval(atomInd)

      call getGlobalInd(mpirank,distMat,iloc,jloc)
      forall(ii=1:size(iloc))
              atomList(ii,1) = atomInd(iloc(ii))
              atomList(ii,2) = atomInd(jloc(ii))
      end forall

      call unique(atomList)
      forall(ii=1:nt)
              tmp(ii) = (ii-1)*natom
      end forall

      call bsxfunPlus(atomList,tmp,atomList)

      return
      end subroutine calcSelectedAtomList
      end module calcSelectedAtomListMod
