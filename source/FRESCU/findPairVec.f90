! findPairVec.f90

!***********************************************************
      !
      !
      !
!***********************************************************

      module findPairVec_moudle
      contains
      subroutine findPairVec(aovec,aopair,pairVecOut,pairInd,vecInd)

      implicit none

      ! input variables


      ! temporary variables


      ! output variables


      ! body of this function
      call triu(aopair,aop)
      ntmp1 = 0
      do ii = 1,size(aop,1),1
          if (any(aop(ii,:) /= 0.0)) then
                  ntmp1 = ntmp1+1
          end if
      end do
      ntmp2 = 0
      do ii = 1,size(aop,2),1
          if (any(aop(:,ii) /= 0.0)) then
                  ntmp2 = ntmp2+1
          end if
      end do

      allocate(ia(ntmp1*ntmp2))
      allocate(ja(ntmp1*ntmp2))
      allocate(pairVec(ntmp1*ntmp2))
      ntmp1 = 0
      do ii = 1,size(aop,2),1
          do jj = 1,size(aop,1),1
              if (aop(jj,ii) /= 0) then
                  ntmp1 = ntmp1+1
                  ia(ntmp1) = jj
                  ja(ntmp1) = ii
              end if
          end do
      end do

      forall(ii=1:ntmp1)
              pairVec(ii,:) = aovec(ja(ii),:)-aovec(ia(ii),:)
      end forall

      call uniqueRow(pairVec,paiVecOut,pairInd,vecInd)

      return
      end subroutine findPairVec
      end module findPairVec_moudle
