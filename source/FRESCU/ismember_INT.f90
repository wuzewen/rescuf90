! ismember_INT.f90
    
!**************************************************************
    !
    ! This is is a function to judge wether A(ii,jj) is an 
    ! element in B. If so, isLoc(ii,jj) is true, if not, 
    ! isLoc(ii,jj) is false.
    !
!**************************************************************
    
    module ismember_INT_module
    contains
    subroutine ismember_INT(A,B,isLoc)
    
    implicit none
    
    ! input variables
    integer, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    integer :: mA, nA, ii, jj
    
    ! output variables
    logical, allocatable :: isLoc(:,:)
    
    ! body of this function
    mA = size(A,1)
    nA = size(A,2)
    
    do ii = 1,mA,1
        do jj = 1,nA,1
            isLoc(ii,jj) = any(B==A(ii,jj))
        end do
    end do
    
    return
    end subroutine ismember_INT
    end module ismember_INT_module
        
