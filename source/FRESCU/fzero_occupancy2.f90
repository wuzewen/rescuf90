! fzero_occupancy2.f90
    
!***********************************************************************************************
    !
    !
    !
!***********************************************************************************************
    
    module fzero_occupancy2_module
    contains
    subroutine fzero_occupancy2(sigma,kweight,energy,ntot,mu0,mu)
    
    use fermiDirac_3D_module
    
    implicit none
    
    ! input variables
    real*8              :: sigma, ntot, mu0
    real*8, allocatable :: kweight(:), energy(:,:,:)
    
    ! temporary variables
    real*8, allocatable :: focc(:,:,:), foccT(:)
    real*8              :: init0, init1, init2, init3, tmp1, tmp2, mut, h
    real*8              :: ef0, ef02, ef1, ef2, initM
    real*8              :: h2, deltaF
    real*8              :: inf1, inf2
    integer             :: ntmp
    
    ! output variables
    real*8 :: mu
    
    ! body of this function
    call fermiDirac_3D(sigma,mu0,energy,focc)
    allocate(foccT(size(focc,2)))
    foccT = sum(sum(focc,3),1)
    deallocate(focc)
    init0 = sum(foccT*kweight)-ntot
    init1      = init0
    if (init1 > 0) then
        init2      = 1.0D0
        init3      = 1.0D0
    else if (init1 == 0) then
        mu = mu0
        return
    else
        init2      = -1.0D0
        init3      = -1.0D0
    end if
    h          = 1.0D0
    h2         = 0.000001D0
    mu         = mu0
    mut        = mu
    tmp1       = init0*init2
    tmp2       = init1*init3
    ntmp = 0
    do while (tmp1 > 0.0 .and. tmp2 > 0.0)
        mu = mu+h
        ntmp = ntmp +1
        call fermiDirac_3D(sigma,mu,energy,focc)
        foccT = sum(sum(focc,3),1)
        deallocate(focc)
        init2 = sum(foccT*kweight)-ntot
        tmp1  = init0*init2
        if (tmp1 == 0) then
                return
        else if (tmp1 < 0) then
                ef1  = mu0
                ef2  = mu
                inf1 = init0
                inf2 = init2
                exit
        end if

        mut = mut-h
        call fermiDirac_3D(sigma,mut,energy,focc)
        foccT = sum(sum(focc,3),1)
        deallocate(focc)
        init3 = sum(foccT*kweight)-ntot
        tmp2  = init1*init3

        if (tmp2 == 0) then
                return
        else if (tmp2 < 0) then
                ef2  = mu0
                ef1  = mut
                inf1 = init3
                inf2 = init1
                exit
        end if
    end do
    !write(*,*) "number of steps 1 ="
    !write(*,*)  ntmp

    deltaF = h
    ntmp   = 0
    do while (deltaF > h2)
        ntmp = ntmp+1
        ef0 = (ef1+ef2)/2.0D0
        call fermiDirac_3D(sigma,ef02,energy,focc)
        foccT = sum(sum(focc,3),1)
        deallocate(focc)
        initM = sum(foccT*kweight)-ntot
        if (initM == 0) then
                mu = ef0
                return
        else if (initM*inf1 < 0) then
                ef1  = ef1
                ef2  = ef0
                inf1 = inf1
                inf2 = initM
        else if (initM*inf2 < 0) then
                ef1  = inf1
                ef2  = ef2
                inf1 = initM
                inf2 = inf2
        end if
    end do
    !write(*,*) "number of steps 2 ="
    !write(*,*)  ntmp
    mu = (ef1+ef2)/2.0D0
    
    return
    end subroutine fzero_occupancy2
    end module fzero_occupancy2_module
