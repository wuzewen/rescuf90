! Acell_type
    
!*****************************************************************
    !
    ! This type of data will be used in GetNonLocalPotential
    ! and reduceOper
    !
!*****************************************************************
    
    module Acell_cmplx_type
    
    type :: Acell_cmplxType
        complex*16, allocatable :: vata(:,:)
    end type
    
    end module Acell_cmplx_type
