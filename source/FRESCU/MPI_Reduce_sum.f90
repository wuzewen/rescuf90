! MPI_Reduce_sum.f90

      module MPI_Reduce_sum_module
      contains
      subroutine MPI_Reduce_sum(vari,ranks,root,redvar)

      include 'mpif.h'

      ! input variables

      ! temporary variables

      ! output variables

      ! body of this function
      call MPI_INIT(ierr)
      call MPI_COMM_RANK(MPI_COMM_WORLD,mpirank,ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD,mpisize,ierr)

      if (all(ranks /= mpirank)) then
              redvar = 0
              return
      end if

      rankn = size(ranks)

      ntmp  = 0
      do ii = 1,rankn,1
          if (ranks(ii) == root) then
                  ntmp = ntmp +1
          end if
      end do
      allocate(grouproot(ntmp))
      ntmp  = 0
      do ii = 1,rankn,1
          if (ranks(ii) == root) then
                  ntmp = ntmp +1
                  grouproot(ntmp) = ii - 1
          end if
      end do

      sendinfo = 0
      sendinfo(1) = 0
      sendinfo(2) = 0

      call mex_allreduce_sum(sendinfo,recvinfo,16,rankn,ranks)

      if (recvinfo(1) /= 0) then
              issp = .TRUE.
      else
              issp = .FALSE.
      end if
      if (recvinfo(2) /= 0) then
              isrl = .FALSE.
      else
              isrl = .TRUE.
      end if

      if (issp) then
              call MPI_Reduce(vari,'PLUS',ranks,root,redvar)
      else
              nvar = size(vari)
              if (isrl .and. nvar == 1) then
                      allocate(redvar(2,1))
                      vari2()
                      call mex_reduce_sum(vari2,redvar,nvar,grouproot,rankn,ranks)
                      redvar = redvar(1)
              else if (isrl) then
                      allocate(size(redvar,1),size(redvar,2))
                      redvar = 0
                      call mex_reduce_sum(vari,redvar,2,grouproot,rankn,ranks)
              else if (nvar == 1) then
                      vartmp(1) = real(vari)
                      vartmp(2) = imag(vari)
                      allocate(redvar(2,1))
                      call mex_reduce_sum(vartmp,redvar,2,grouproot,rankn,ranks)
                      redvar = (redvar(1),redvar(2))
              else
                      vartmp = real(vari)
                      allocate(redvarr(size(vari,1),size(vari,2)))
                      call mex_reduce_sum(vartmp,redvarr,nvar,grouproot,rankn,ranks)
                      vartmp = imag(vari)
                      allocate(redvari(size(vari,1),size(vari,2)))
                      redvari = 0
                      call mex_reduce_sum(vartmp,redvari,nvar,grouproot,rankn,ranks)
                      redvar = (redvarr,redvari)
              end if
      end if

      return
      end subroutine MPI_Reduce_sum
      end module MPI_Reduce_sum_module
