! spinType.f90
    
!*************************************************************
    !
    ! This is to define a type to contain spin information.
    !
!*************************************************************
    
    module spinType_module
    
    type :: spinType
        integer              :: ispin
        logical              :: SOI
        character(len=20)    :: spinType
        integer              :: nspin
        !real*8, allocatable  :: magmom(:,:)
    end type spinType
    
    end module spinType_module