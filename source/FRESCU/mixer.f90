!  mixer.f90 
!
!  FUNCTIONS:
!  mixer - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: mixer
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module mixer_module
    contains
    subroutine mixer(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_2D_module
    use determineVinVout_module
    use linear_module
    use Broyden_module
    use determineVnew_module
    
    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    !integer  ::  SCFloop
    
    ! temporery variables
    !character(len=20) :: broy, broyker, dii, john, johnker, lin, pul
    integer         :: mpirank, iter, ntmp1, ntmp2
    logical         :: mpistat
    type(VataMN_2D) :: vin, vout, vnew
    
    ! output variables
    

    ! Body of mixer
    !write(*,*) "I'm here at the beginging of mixer."
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    iter    = FRC%scloop
    
    ntmp1   = size(FRC%rho%input(1)%vata,1)
    ntmp2   = size(FRC%rho%input(1)%vata,2)
    allocate(vin%vata(ntmp1,ntmp2))
    allocate(vout%vata(ntmp1,ntmp2))
    allocate(vnew%vata(ntmp1,ntmp2))
    call determineVinVout(FRC,vin,vout)
    !write(*,*) "vin and vout in mixer."
    !write(*,*)  vin%m, vin%n, vin%mblock, vin%nblock, vin%mproc, vin%nproc
    !write(*,*)  vout%m, vout%n, vout%mblock, vout%nblock, vout%mproc, vout%nproc
    !write(*,*) "Vin ="
    !write(*,*)  vin%vata
    !write(*,*) "vout ="
    !write(*,*)  vout%vata
    !select case (FRC%Mixing%Method)
    !case (1)
    if (trim(FRC%Mixing%Method) == trim("Broyden")) then
        !write(*,*) "Error in mixer.f90. Broyden if not available now. Please use linear."
        !stop
        call Broyden(FRC,vin,vout,vnew)
    !case (2)
    else if (trim(FRC%Mixing%Method) == trim("BroydenKerker")) then
        write(*,*) "Error in mixer.f90. BroydenKerker if not available now. Please use linear."
        stop
        !call BroydenKerker(FRC,vin,vout,vnew)
    !case (3)
    else if (trim(FRC%Mixing%Method) == trim("diis")) then
        write(*,*) "Error in mixer.f90. diis if not available now. Please use linear."
        stop
        !call DIIS(FRC)
    !case (4)
    else if (trim(FRC%Mixing%Method) == trim("johnson")) then
        write(*,*) "Error in mixer.f90. johnson if not available now. Please use linear."
        stop
        !call Johnson(FRC)
    !case (5)
    else if (trim(FRC%Mixing%Method) == trim("JohnsonKerker")) then
        write(*,*) "Error in mixer.f90. JohnsonKerker if not available now. Please use linear."
        stop
        !call JohnsonKerker(FRC)
    !case (6)
    else if (trim(FRC%Mixing%Method) == trim("Linear")) then
        call Linear(FRC,vin,vout,vnew)
    !case (7)
    else if (trim(FRC%Mixing%Method) == trim("Pulay")) then
        write(*,*) "Error in mixer.f90. Pulay if not available now. Please use linear."
        stop
        !call Pulay(FRC,vin,vout,vnew)
    !end select
    end if
    !write(*,*) "vnew in mixer."
    !write(*,*)  vnew%m, vnew%n, vnew%mblock, vnew%nblock, vnew%mproc, vnew%nproc
    !write(*,*)  vnew%vata

    call determineVnew(FRC,vnew)
    

    return
    end subroutine mixer
    end module mixer_module

