! calcev_lcao.f90

!*****************************************************************
      !
      !
      !
!*****************************************************************

      module calcev_lcao_module
      contains
      subroutine calcev_lcao(FRC,aosub,coeff,ev)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none
 
      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      type(VataMN_2D)                 :: aosub, coeff

      ! temporary varibales
      logical :: mpistat
      integer :: mpirank, mpisize

      ! output variables
      type(VataMN_2D) :: ev

      ! body of this function
      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize

      if (mpistat) then
              write(*,*) "Error in calcev_lcao. MPI"
      else
              ev      = aosub
              ev%vata = matmul(aosub%vata,coeff%vata)
              ev%n    = coeff%n
              ev%nb   = coeff%n
      end if

      return
      end subroutine calcev_lcao
      end module calcev_lcao_module

