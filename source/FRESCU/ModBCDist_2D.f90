! ModBCDist_2D.f90
    
!**************************************
    !
    !
    !
!**************************************
    
    module ModBCDist_2D_module
    contains
    recursive subroutine ModBCDist_2D(FRC,A,mb2,nb2,mp2,np2,iscomplex,issp,B)
    
    use VataMN_2D_module
    use FORTRAN_RESCU_CALCULATION_TYPE
    use GetGlobalInd_2D_module
    use bsxfunMinus_module
    use flipud_module
    use fliplr_module
    use triu_module
    use ismember_INT_module
    use MPI_Send_variable_real8_2D_module
    use MPI_Recv_variable_real8_2D_module

    !use dataType_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_2D)      :: A
    integer              :: mb2, nb2, mp2, np2, mb1, nb1, mp1, np1, m, n
    logical              :: iscomplex, issp, isrl
    
    ! temporary variables
    logical              :: mpistat, smistat
    integer              :: mpirank, mpisize, ii, jj, kk, ntmp, ntmptmp, ann, amm
    integer              :: dest
    type(VataMN_2D)      :: psilin
    integer, allocatable :: ia(:,:), ja(:,:), i1(:,:), i2(:,:), j1(:,:), j2(:,:)
    logical, allocatable :: iloc1(:,:), jloc1(:,:), iloc2(:,:), jloc2(:,:)
    integer, allocatable :: indn(:), Iind(:), indm(:)
    real*8, allocatable  :: tmp1(:), tmp2(:), tmpMPI1(:,:), tmpMPI2(:,:), indmat(:,:), indmat1(:,:), indmat2(:,:)
    real*8, allocatable  :: indmat3(:,:)
    real*8, allocatable  :: indmtmp(:), indntmp(:), BdataAtmp(:,:), AB(:,:)
    real*8, allocatable  :: AvataT(:,:), BvataT(:,:)
    character(len=20)    :: flax, numb
    integer :: cou, tag, bufs
    
    ! output variables
    type(VataMN_2D)      :: B
    

    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    smistat = FRC%smi%status

    !write(numb,"(i4.4)") mpirank
    !flax = 'mod'//trim(numb)//'.txt'
    !cou  =  mpirank + 10
    !open(unit=cou,file=flax)
    !write(cou,*) mpistat, mpirank, mpisize, smistat
    if (.not. mpistat) then
        B        = A
        B%mblock = mb2
        B%nblock = nb2
        B%mproc  = mp2
        B%nproc  = np2

        !write(*,*) "not mpi"
        return
        !exit
    end if
    
    mb1 = A%mblock
    nb1 = A%nblock
    mp1 = A%mproc
    np1 = A%nproc
    
    if (mb1 == mb2 .and. nb1 == nb2 .and. mp1 == mp2 .and. np1 == np2) then
        B = A
        !write(*,*) 'B=A'
    !    !exit
        return
    end if
    
    !if (issp == -1) then
    !    call ModBCDist_1D(FRC,A,mb2,nb2,mp2,np2,.false.,.FALSE.,B) 
    !    !exit
    !    return
    !end if
    
    !m = A%m
    !n = A%n
    !call initDistArray(m,n,mb2,nb2,mp2,np2,B)
    !call distmat_isreal(FRC,A,isrl)
   ! 
    !if (smistat .and. (.not. issp) .and. (.not. isrl)) then
    !    psilin            =  A
    !    !psilin%dataA(:,1) =  real(A%dataA)
    !    !psilin%dataA(:,2) =  imag(A%dataA)
    !    call ModBCDist_1D(FRC,psilin,mb2,nb2,mp2,np2,.TRUE.,.FALSE.,B)
    !    call GetGlobalInd_1D(mpirank,B,ia,ja)
    !    !forall(ii = 1:2:)
    !    tmp1 = B%dataA(1::2)
    !    tmp2 = B%dataA(2::2)
    !    tmp1 = tmp1 + tmp2
    !    allocate(BdataAtmp(size(ia),size(ja)))
    !    BdataAtmp = reshape(tmp1,(/size(ia),size(ja)/))
    !else if (smistat .and. (.not. issp) .and. iscomplex) then
    !    call GetGlobalInd_1D(mpirank,B)
    !    B%dataA = 0
    !    !call MPI_Barrier
    !    !call smiZGEMR2D(A,B)
    !    !call MPI_Barrier
    !    !exit
    !    return
    !else if (smistat .and. issp) then
    !    call GetGlobalInd_1D(mpirank,B,ia,ja)
    !    B%dataA = 0
    !    !call MPI_Barrier
    !    !call smiZGEMR2D(A,B)
    !    !call MPI_Barrier
    !    !exit
    !    return
    !if (smistat) then
    !    call GetGlobalInd_1D(mpirank,B,ia,ja)
    !    allocate(B%vata(size(ia),size(ja)))
    !    call MPI_Barrier
    !    call smi
    !else
        m = A%m
        n = A%n
        call InitDistArray_2D(m,n,mb2,nb2,mp2,np2,B)
        call GetGlobalInd_2D(mpirank,B,ia,ja)
    !    

    !write(cou,*) "ia ="
    !write(cou,*)  ia
    !write(cou,*) "ja ="
    !write(cou,*)  ja
        allocate(tmpMPI1(mpisize,1),tmpMPI2(1,mpisize))
        forall (ii = 1:mpisize)
            tmpMPI1(ii,1) = dble(ii)
            tmpMPI2(1,ii) = dble(ii)
        end forall
    !   
        allocate(indmat(mpisize,mpisize))
        call bsxfunMinus(tmpMPI1,tmpMPI2,indmat)

        indmat = -indmat
        !write(cou,*) 'indmat ='
        !write(cou,*)  indmat

        !allocate(indmat1(mpisize,mpisize))
        call flipud(indmat,indmat1)

        !write(cou,*) 'indmat1 ='
        !write(cou,*)  indmat1
        allocate(indmat2(mpisize,mpisize))
        indmat2 = 0.0D0
        forall(ii = 1:mpisize)
            indmat2(ii,ii) = dble(mpisize)
        end forall
        allocate(indmat3(mpisize,mpisize))
        call fliplr(indmat2,indmat3)

        !write(cou,*) 'indmat3 = '
        !do ii = 1,size(indmat3,1),1
        !write(cou,*)  indmat3
        !end do
        indmat = indmat1-indmat3
        
        call triu(indmat,0,indmat2)

        indmat = indmat2
        forall(ii=1:mpisize)
                indmat(ii,ii) = 0.0D0
        end forall

        !write(cou,*) 'indmat ='
        !write(cou,*)  indmat
        do ii = 1,mpisize,1
            do jj = 1,mpisize,1
                if (indmat(ii,jj) /= 0) then
                    ntmp          = mod(int(-indmat(ii,jj)),mpisize)
                    if (ntmp<0) then
                            ntmp = ntmp + mpisize
                    end if
                    indmat(ii,jj) = mpisize - ntmp!mod(-indmat(ii,jj),dble(mpisize))
                end if
            end do
        end do
        
        !write(cou,*) 'indmat ='
        !write(cou,*)  indmat
        call GetGlobalInd_2D(mpirank,A,i1,j1)

        !write(cou,*) 'A index'
        !write(cou,*) 'i1 ='
        !write(cou,*)  i1
        !write(cou,*) 'j1 ='
        !write(cou,*)  j1

        call GetGlobalInd_2D(mpirank,B,i2,j2)

        !write(cou,*) 'B index'
        !write(cou,*) 'i2 ='
        !write(cou,*)  i2
        !write(cou,*) 'j2'
        !write(cou,*)  j2
        allocate(iloc1(size(i1,1),size(i1,2)))
        allocate(jloc1(size(j1,1),size(j1,2)))
        allocate(iloc2(size(i2,1),size(i2,2)))
        allocate(jloc2(size(j2,1),size(j2,2)))
        call ismember_INT(i1,i2,iloc1)
        call ismember_INT(j1,j2,jloc1)
        call ismember_INT(i2,i1,iloc2)
        call ismember_INT(j2,j1,jloc2)
    !    
    !write(cou,*) 'iloc1 ='
    !write(cou,*)  iloc1
    !write(cou,*) 'jloc1 ='
    !write(cou,*)  jloc1
    !write(cou,*) 'iloc2 ='
    !write(cou,*)  iloc2
    !write(cou,*) 'jloc2 ='
    !write(cou,*)  jloc2
        if (any(iloc1 .EQV. .TRUE.) .and. any(iloc2 .EQV. .TRUE.) .and. &
                any(jloc1 .EQV. .TRUE.) .and. any(jloc2 .EQV. .TRUE.)) then
                !write(cou,*) 'In if before loop.'
                ann = 0
            allocate(AB(count(iloc1 .EQV. .TRUE.),count(jloc1 .EQV. .TRUE.)))
            do jj = 1,size(jloc1,2),1
                if (jloc1(1,jj)) then
                    ann = ann+1
                    amm = 0
                    do kk = 1,size(iloc1,2),1
                        if (iloc1(1,kk)) then
                            amm         = amm+1
                            AB(amm,ann) = A%vata(kk,jj)
                        end if
                    end do
                end if
            end do
            ann = 0
            do jj = 1,size(jloc2,2),1
                if (jloc2(1,jj)) then
                    ann = ann+1
                    amm = 0
                    do kk = 1,size(iloc2,2),1
                        if (iloc2(1,kk)) then
                            amm = amm+1
                            B%vata(kk,jj) = AB(amm,ann)
                        end if
                    end do
                end if
            end do
        end if

        !write(cou,*) 'After if before loop.'
    !    
        do ii = 1,mpisize,1
            !write(cou,*) 'loop =', ii
            ntmp = 0
            do jj = 1,mpisize,1
                do kk = 1,mpisize,1
                    if (indmat(jj,kk) == ii) then
                        ntmp = ntmp + 1
                    end if
                end do
            end do
            allocate(indmtmp(ntmp),indntmp(ntmp))
            ntmp = 0
            do jj = 1,mpisize,1
                do kk = 1,mpisize,1
                    if (indmat(jj,kk) == ii) then
                        ntmp          = ntmp + 1
                        indmtmp(ntmp) = jj
                        indntmp(ntmp) = kk
                    end if
                end do
            end do
            !write(cou,*) 'indmtmp ='
            !write(cou,*)  indmtmp
            !write(cou,*) 'indntmp ='
            !write(cou,*)  indntmp
            
            ntmp = 0
            do jj = 1,size(indmtmp),1
                if (int(indmtmp(jj)) == mpirank+1 .or. int(indntmp(jj)) == mpirank+1) then
                    ntmp = ntmp+1
                end if
            end do
            allocate(Iind(ntmp))
            !write(cou,*) 'ntmp of Iind', ntmp
            ntmp = 0
            do jj = 1,size(indmtmp),1
                if (indmtmp(jj) == mpirank+1 .or. indntmp(jj) == mpirank+1) then
                    ntmp       = ntmp+1
                    Iind(ntmp) = jj
                end if
            end do
            
            allocate(indm(ntmp),indn(ntmp))
            forall(jj=1:ntmp)
                indm(jj) = indmtmp(Iind(jj))-1
                indn(jj) = indntmp(Iind(jj))-1
            end forall
           ! write(cou,*) 'After index in the loop.'
            !write(cou,*) 'ntmp =', ntmp
            !write(cou,*) 'indm ', indm, 'indn', indn
            if (ntmp /= 0) then
                if (any(indn == mpirank)) then
                    dest = indn(1)
                    deallocate(i1,j1)
                    call GetGlobalInd_2D(dest,A,i1,j1)
                    dest = indm(1)
                    deallocate(i2,j2)
                    call GetGlobalInd_2D(dest,B,i2,j2)
                    deallocate(iloc1,jloc1)
                    allocate(iloc1(size(i1,1),size(i1,2)))
                    allocate(jloc1(size(j1,1),size(j1,2)))
                    call ismember_INT(i1,i2,iloc1)
                    call ismember_INT(j1,j2,jloc1)
                    dest = indm(1)
                    deallocate(i1,j1)
                    call GetGlobalInd_2D(dest,A,i1,j1)
                    dest = indn(1)
                    deallocate(i2,j2)
                    call GetGlobalInd_2D(dest,B,i2,j2)
                    deallocate(iloc2,jloc2)
                    allocate(iloc2(size(i2,1),size(i2,2)))
                    allocate(jloc2(size(j2,1),size(j2,2)))
                    call ismember_INT(i2,i1,iloc2)
                    call ismember_INT(j2,j1,jloc2)
                    if (any(iloc1 .EQV. .TRUE.) .and. any(jloc1 .EQV. .TRUE.)) then
                        allocate(AvataT(count(iloc1 .EQV. .TRUE.),count(jloc1 .EQV. .TRUE.)))
                        amm = 0
                        do jj = 1,size(jloc1,2),1
                                if (jloc1(1,jj)) then
                                    amm = amm+1
                                    ann = 0
                                    do kk = 1,size(iloc1,2),1
                                        if (iloc1(1,kk)) then
                                                ann             = ann+1
                                                AvataT(ann,amm) = A%vata(kk,jj)
                                        end if
                                    end do
                                end if
                        end do
                        dest = indm(1)
                        !write(cou,*) 'before first send.'
                        !write(cou,*) 'AvataT ='
                        !write(cou,*)  AvataT
                        !write(cou,*) 'dest =', dest
                        tag  = 0
                        bufs = 2**20
                        call MPI_Send_variable_real8_2D(AvataT,dest,tag,bufs)
                        !write(cou,*) 'rank', mpirank,'send',AvataT, 'to rank', dest
                        deallocate(AvataT)
                    end if
                    if (any(iloc2 .EQV. .TRUE.) .and. any(jloc2 .EQV. .TRUE.)) then
                            allocate(BvataT(count(iloc2 .EQV. .TRUE.),count(jloc2 .EQV. .TRUE.)))
                            dest = indm(1)
                            !write(cou,*) 'before first recv.'
                            !write(cou,*) 'dest =', dest
                            tag  = 0
                            call MPI_Recv_variable_real8_2D(dest,tag,BvataT)
                            !write(cou,*) 'after first recv.'
                            !write(cou,*) 'BvataT'
                            !write(cou,*)  BvataT
                            !write(cou,*) 'rank', mpirank,'recv',BvataT, 'from rank', dest
                            amm = 0
                            do jj = 1,size(jloc2,2),1
                                if (jloc2(1,jj)) then
                                    amm = amm+1
                                    ann = 0
                                    do  kk = 1,size(iloc2,2),1
                                        if (iloc2(1,kk)) then
                                            ann           = ann+1
                                            B%vata(kk,jj) = BvataT(ann,amm)
                                        end if
                                    end do
                                end if
                            end do
                            deallocate(BvataT)
                    end if
                end if
                
                if (mpirank == indm(1)) then
                    dest = indm(1)
                    deallocate(i1,j1)
                    call GetGlobalInd_2D(dest,A,i1,j1)
                    dest = indn(1)
                    deallocate(i2,j2)
                    call GetGlobalInd_2D(dest,B,i2,j2)
                    deallocate(iloc1,jloc1)
                    allocate(iloc1(size(i1,1),size(i1,2)))
                    allocate(jloc1(size(j1,1),size(j1,2)))
                    call ismember_INT(i1,i2,iloc1)
                    call ismember_INT(j1,j2,jloc1)
                    dest = indn(1)
                    deallocate(i1,j1)
                    call GetGlobalInd_2D(dest,A,i1,j1)
                    dest = indm(1)
                    deallocate(i2,j2)
                    call GetGlobalInd_2D(dest,B,i2,j2)
                    deallocate(iloc2,jloc2)
                    allocate(iloc2(size(i2,1),size(i2,2)))
                    allocate(jloc2(size(j2,1),size(j2,2)))
                    call ismember_INT(i2,i1,iloc2)
                    call ismember_INT(j2,j1,jloc2)
                    if (any(iloc2 .EQV. .TRUE.) .and. any(jloc2 .EQV. .TRUE.)) then
                            allocate(BvataT(count(iloc2 .EQV. .TRUE.),count(jloc2 .EQV. .TRUE.)))
                            dest = indn(1)
                            !write(cou,*) 'before 2nd recv.'
                            !write(cou,*) 'dest =', dest
                            tag  = 0
                            call MPI_Recv_variable_real8_2D(dest,tag,BvataT)
                            !write(cou,*) 'after 2nd recv.'
                            !write(cou,*) 'rank', mpirank,'recv',BvataT, 'from rank', dest
                            !write(cou,*) 'BvataT'
                            !write(cou,*)  BvataT
                            amm = 0
                            !write(cou,*) 'BvataT =', BvataT
                            !write(cou,*) 'jloc2  =', jloc2
                            !write(cou,*) 'iloc2  =', iloc2
                            do jj = 1,size(jloc2,2),1
                                if (jloc2(1,jj)) then
                                    amm = amm+1
                                    ann = 0
                                    do kk = 1,size(iloc2,2),1
                                        if (iloc2(1,kk)) then
                                            ann           = ann+1
                                            B%vata(kk,jj) = BvataT(ann,amm)
                                        end if
                                    end do
                                end if
                            end do
                            deallocate(BvataT)
                    end if
                    if (any(iloc1 .EQV. .TRUE.) .and. any(jloc1 .EQV. .TRUE.)) then
                            allocate(AvataT(count(iloc1 .EQV. .TRUE.),count(jloc1 .EQV. .TRUE.)))
                            amm = 0
                            do jj = 1,size(jloc1,2),1
                                if (jloc1(1,jj)) then
                                    amm = amm+1
                                    ann = 0
                                    do kk = 1,size(iloc1,2),1
                                        if (iloc1(1,kk)) then
                                            ann             = ann+1
                                            AvataT(ann,amm) = A%vata(kk,jj)
                                        end if
                                    end do
                                end if
                            end do
                            dest = indn(1)
                            !write(cou,*) 'A%vata ='
                            !write(cou,*)  A%vata
                            !write(cou,*) 'size of iloc1, jloc1', size(iloc1,1),size(jloc1,1)
                            !write(cou,*) 'before 2nd send.'
                            !write(cou,*) 'AvataT'
                            !write(cou,*)  AvataT
                            !write(cou,*) 'dest =', dest
                            tag  = 0
                            bufs = 2**20
                            call MPI_Send_variable_real8_2D(AvataT,dest,tag,bufs)
                            !write(cou,*) 'rank', mpirank,'send',AvataT, 'to rank', dest
                            !write(cou,*) 'after 2nd send.'
                            deallocate(AvataT)
                    end if
                end if
            end if
            deallocate(indmtmp,indntmp,Iind)
            deallocate(indm,indn)
        end do
    !end if
    !close(cou)
    return
    end subroutine ModBCDist_2D
    end module ModBCDist_2D_module
