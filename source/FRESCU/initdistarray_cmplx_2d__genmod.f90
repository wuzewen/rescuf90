        !COMPILER-GENERATED INTERFACE MODULE: Tue Jan 15 11:50:54 2019
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INITDISTARRAY_CMPLX_2D__genmod
          INTERFACE 
            SUBROUTINE INITDISTARRAY_CMPLX_2D(M,N,MB,NB,MP,NP,A)
              USE VATAMN_CMPLX_2D_MODULE
              INTEGER(KIND=4) :: M
              INTEGER(KIND=4) :: N
              INTEGER(KIND=4) :: MB
              INTEGER(KIND=4) :: NB
              INTEGER(KIND=4) :: MP
              INTEGER(KIND=4) :: NP
              TYPE (VATAMN_CMPLX_2D) :: A
            END SUBROUTINE INITDISTARRAY_CMPLX_2D
          END INTERFACE 
        END MODULE INITDISTARRAY_CMPLX_2D__genmod
