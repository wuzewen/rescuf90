! determineVnew
!********************
    !
!********************
    
    module determineVnew_module
    contains
    subroutine determineVnew(FRC,vnew)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_2D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_2D)                 :: vnew
    
    ! temporery variables
    integer :: iter
    
    ! output variabls
    !write(*,*) "I'm here at the begining of determineVnew."
    iter = FRC%scloop
    if (trim(FRC%Mixing%Mixingtype) == trim("density") .and. &
            (trim(FRC%info%calculationType) == trim("self-consistent") .or. &
            trim(FRC%info%calculationType) == trim("relaxation") .or. &
            trim(FRC%info%calculationType) == trim("phonon"))) then
        if (iter > 1) then
            !allocate(FRC%Rho%Input(iter+1)%vata(size(Vnew%vata,1),size(Vnew%vata,2)))
        end if
        FRC%Rho%Input(2)         = Vnew
    else if (trim(FRC%Mixing%Mixingtype) == trim("Potential") .and. &
            (trim(FRC%info%calculationType) == trim("SCF") .or. &
            trim(FRC%info%calculationType) == trim("relaxation") .or. &
            trim(FRC%info%calculationType) == trim("phonon"))) then
        !FRC%Potential%VeffIn(iter+1)  = Vnew
        write(*,*) "Error in determineVinVout.f90. Potential can't be mixed now, please use Density."
        stop
    else if (trim(FRC%Mixing%Mixingtype) == trim("Density") .and. &
            trim(FRC%info%calculationType) == trim("DFPT")) then
        write(*,*) "Error in determineVinVout.f90. DFPT is not available now."
        stop
        !FRC%Rho%DeltaIn(iter+1)       = Vnew
    else if (trim(FRC%Mixing%Mixingtype) == trim("Potential") .and. &
            trim(FRC%info%calculationType) == trim("DFPT")) then
        write(*,*) "Error in determineVinVout.f90. Potential can't be mixed now, please use Density."
        stop
        !FRC%Potential%DeltaIn(iter+1) = Vnew
    else if (trim(FRC%Mixing%Mixingtype) == trim("DensityMatrix") .and. &
            (trim(FRC%info%calculationType) == trim("SCF") .or. &
            trim(FRC%info%calculationType) == trim("relaxation") .or. &
            trim(FRC%info%calculationType) == trim("phonon"))) then
        write(*,*) "Error in determineVinVout.f90. DensityMatrix can't be mixed now, please use Density."
        stop
        !FRC%LCAO%DMin = Vnew
        !call extractDM(FRC,Vnew,FRC%LCAO%DMcell)
    end if
    
    return
    end subroutine determineVnew
    end module determineVnew_module
