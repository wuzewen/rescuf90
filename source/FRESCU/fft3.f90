! fft.f90
    
    module fft3_module
    contains
    subroutine fft3(A,dim,B)
    
    implicit none
    
    ! input valuables
    integer               ::  dim
    complex*16, allocatable  ::  A(:,:,:)
    
    ! temporery valuables
    integer               ::  i, j
    integer               ::  N
    complex*16, parameter    ::  imnu = (0.0D0,1.0D0), pi = (3.1415926535897932385D0,0.0D0)
    
    ! output valuables
    complex*16, allocatable  ::  B(:,:,:)
    
    ! body of this function
    
    N = size(A,dim)
    B(:,:,:) = (0.0D0,0.0D0)
    do i = 1,N,1
        do j = 1,N,1
            if (dim==1) then
                B(i,:,:) = B(i,:,:)+A(j,:,:)*exp(-imnu*2.0D0*pi*dcmplx(i-1)*(j-1)/dcmplx(N))
            else if (dim==2) then
                B(:,i,:) = B(:,i,:)+A(:,j,:)*exp(-imnu*2.0D0*pi*dcmplx(i-1)*(j-1)/dcmplx(N))
            else if (dim==3) then
                B(:,:,i) = B(:,:,i)+A(:,:,j)*exp(-imnu*2.0D0*pi*dcmplx(i-1)*(j-1)/dcmplx(N))
            end if
        end do
    end do
    
    end subroutine fft3
    end module fft3_module
    
