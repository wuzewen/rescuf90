! fft3_fftw_pointer.f90

      module fft3_fftw_pointer_module
      contains
      !subroutine fft3_fftw(FRC,A)
      subroutine fft3_fftw_pointer(planfft,Arr,Brr)

      !use FORTRAN_RESCU_CALCULATION_TYPE
      use, intrinsic :: iso_c_binding
      !include 'fftw3.f03'

      type(C_PTR)         :: planfft
      complex*16, pointer :: Arr(:), Brr(:)
      !integer             :: L, M, N

      !type(FORTRAN_RESCU_CALCULATION) :: FRC
      !type(C_PTR) :: plan
      !integer(C_INT)                              :: L, M, N
      !complex(C_DOUBLE_COMPLEX), dimension(L,M,N) :: Arr, Brr
      !complex*16, allocatable         :: A(:,:,:)!, B(:,:,:)

      !FRC%Arr = A
      !call dfftw_plan_dft_3d(plan,L,M,N,Arr,Brr,FFTW_FORWARD,FFTW_ESTIMATE)
      call dfftw_execute_dft(planfft,Arr,Brr)
      !call dfftw_destroy_plan(plan)
      !B   = Brr
      !A       = FRC%Arr

      return
      end subroutine fft3_fftw_pointer
      end module fft3_fftw_pointer_module
