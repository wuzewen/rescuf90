! MPI_Send_variable_real8_1D.f90

!**********************************************************
      !
      !
      !
!**********************************************************

      module MPI_Send_variable_real8_1D_module
      contains
      subroutine MPI_Send_variable_real8_1D(vari,dest,tag,bloc)

      include 'mpif.h'

      ! input variables
      integer              :: tag, bloc
      real*8, allocatable  :: vari(:)
      integer :: dest

      ! temporary variables
      integer :: isrl, issp, varisize, kk, varn, ntmp, ii, ierr
      integer, allocatable :: varinfo(:), inde(:)

      real*8, allocatable :: buf(:), buftmp(:)

      ! output variables


      ! body of this funtion
      allocate(varinfo(16))
      varinfo     = 0
      varinfo(1)  = 0
      issp        = varinfo(1)
      varinfo(2)  = 1
      isrl        = varinfo(2)
      varinfo(3)  = bloc
      varinfo(4)  = count(vari /= 0.0)
      varinfo(5)  = 2
      varinfo(6)  = size(vari,1)
      varisize    = varinfo(6)

      call MPI_SEND(varinfo,16,MPI_INT,dest,tag,MPI_COMM_WORLD,ierr)
      tag         = tag + 1
      !kk          = 0

      varn = size(vari)

      !do while (kk < varn)
      !    ntmp = kk+bloc
      !    ntmp = min(ntmp,varn)
      !    forall(ii = kk+1:ntmp)
      !            inde(ii-kk) = ii
      !    end forall
      !    allocate(buf(ntmp-kk))
      !    forall(ii=1:ntmp)
      !            buf(ii) = vari(inde(ii))
      !    end forall
      !    if (ntmp == 1) then
      !            buftmp(1) = buf(1)
      !            buftmp(2) = 0
      !            call MPI_SEND(buf,2,MPI_DOUBLE,dest,tag,MPI_COMM_WORLD)
      !            tag = tag+1
      !    else
      call MPI_SEND(vari,varn,MPI_DOUBLE,dest,tag,MPI_COMM_WORLD,ierr)
      !            tag = tag+1
      !    end if
      !    kk = kk + bloc

      !end do

      return
      end subroutine MPI_Send_variable_real8_1D
      end module MPI_Send_variable_real8_1D_module
