! dos_main.f90

      module dos_main_module
      contains
      subroutine dos_main(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC

      ! temporary variables
      integer :: mpirank
      
      ! body of this function
      mpirank = FRC%mpi%rank
      iter    = FRC%scloop

      if (.not. FRC%dos%pdos) then
            if (mpirank == 0) then
                  write(*,*) "Computing density of states."
            end if
            call calcDOS(FRC)
      else
            if (.not. (UseAocell .or. UseKcell) then
                  call GetAtomicOrbitalSubspace(FRC)
            end if
            if (.not. (UseScell .and. UseSvec)) then
                  if (mpirank == 0) then
                      write(*,*) "Generating overlap and kinetic energy matrices"
                  end if
                  call calcOverlapAndKineticMatrix(FRC)
                  call symmetrizecell(FRC,FRC%LCAO%Scell,FRC%LCAO%Svec)
            end if
            if (mpirank == 0) then
                    write(*,*) "Computing projected density of states."
            end if
            call calcPartialDOS(FRC)
      end if

      if (FRC%dos%ldos%status) then
              if (.not. (UseAocell .or. UseKcell) then
                      call GetAtomicOrbitalSubspace(FRC)
              end if
              if (mpirank == 0) then
                      write(*,*) "Computing local density of states."
              end if
              call calcLDOS(FRC)
      end if
      write(*,*) "The calculation of DOS finished."

      end subroutine dos_main
      end module dos_main_module
      
