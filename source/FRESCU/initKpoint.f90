! initKpoint.f90
!*********************************************
    !
    ! This is the function to init Kpoint.
    !
!*********************************************
    
    module initKpoint_module
    contains
    subroutine initKpoint(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    use inversion_module
    use initSymmetry_module
    use permute_module
    use isMemberRow_module
    use mpmesh_module
    use fold_k_mesh_module
    use accumArray_module
    use symIndex_module
    use ismember_1DintArray_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    !external, function                   :: sign
    logical                              :: timerev
    logical                              :: kpointTypeIsFind, kpointSamplingIsFind, kpointShiftIsFind
    logical                              :: kdirectIsFind, kcartesianIsFind, KpointGridnIsFind, ksymmetryPointsIsFind
    integer                              :: ii, jj, nkpt, tmp(3), ksymmetryPointsSize
    integer                              :: nline, ntmp
    real*8                               :: tol, crystr(3,3), klen, eps, pi, spe
    integer, allocatable                 :: ikpt(:), isop(:), ired(:), symDirT(:,:,:), iredtmp(:), symInd(:)
    integer, allocatable                 :: symrecTmp(:,:,:), ikpttmp(:), isoptmp(:), ikinput1(:), ikinput2(:), eyeInd(:)
    real*8, allocatable                  :: kred(:,:), wieght(:), sympts(:,:), symtmp(:,:), tmp1(:,:), tmp2(:,:), eyem(:,:)
    real*8, allocatable                  :: avec(:,:), bvec(:,:), weight(:), kdirect(:,:), sym_dir(:,:,:), symDirTmp(:,:,:)
    real*8, allocatable                  :: kredtmp(:,:), symTrans(:,:)
    logical,allocatable                  :: nonsense(:), upind(:)
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%Kpoint) then
        timerev = .FALSE.
        kpointTypeIsFind      = .FALSE.
        kpointSamplingIsFind  = .FALSE.
        kpointShiftIsFind     = .FALSE.
        kdirectIsFind         = .FALSE.
        kcartesianIsFind      = .FALSE.
        KpointGridnIsFind     = .FALSE.
        ksymmetryPointsIsFind = .FALSE.
        nline = inputFromFile%NumberOfInput
        pi    = 3.1415926535897932385D0
        spe   = 2.0D-52
        allocate(avec(3,3),bvec(3,3))
        avec  = FRC%domain%latvec
        call inversion(avec,bvec)
        bvec  = 2.0D0*pi*transpose(bvec)
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("kpointType")) then
                kpointTypeIsFind = .TRUE.
                exit
            end if
        end do
        if (kpointTypeIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%kpoint%kType
        else
            FRC%kpoint%kType = "full"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("kpointSampling")) then
                kpointSamplingIsFind = .TRUE.
                exit
            end if
        end do
        if (kpointSamplingIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%kpoint%sampling
        else
            FRC%kpoint%sampling   = "fermi-dirac"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("kpointShiftX")) then
                kpointShiftIsFind = .TRUE.
                exit
            end if
        end do
        if (kpointShiftIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%kpoint%kshift(1)
        else
            FRC%kpoint%kshift(1)   = 0.0D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("kpointShiftY")) then
                kpointShiftIsFind = .TRUE.
                exit
            end if
        end do
        if (kpointShiftIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%kpoint%kshift(2)
        else
            FRC%kpoint%kshift(2)   = 0.0D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("kpointShiftZ")) then
                kpointShiftIsFind = .TRUE.
                exit
            end if
        end do
        if (kpointShiftIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%kpoint%kshift(3)
        else
            FRC%kpoint%kshift(3)   = 0.0D0
        end if
        
        if (trim(FRC%kpoint%kType) == trim("MonkhorstPack")) then
            FRC%kpoint%kType = "full"
        end if
        !write(*,*) "I'm in initKpoint, before calculation."
        if (trim(FRC%kpoint%kType) == trim("full")) then
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("kdirect")) then
            !        kdirectIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (kdirectIsFind) then
            !    call readKpointfile(FRC)
            !end if
            
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("kcartesian")) then
            !        kcartesianIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if ((.not. kdirectIsFind) .and. kcartesianIsFind) then
            !    call readKpointfile(FRC)
            !    call inversion(FRC%domain%latvec,bvec)
            !    bvec = 2*pi*transpose(bvec)
            !    call rightdivision(FRC%kpoint%directKpoints,bvec)
            !end if
            
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("KpointGridn")) then
            !        KpointGridnIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (KpointGridnIsFind) then
            !    FRC%kpoint%gridn = inputFromFile%NAV(ii)%value
            !else
            !    FRC%kpoint%gridn = (/1,1,1/)
            !end if
            !write(*,*) "I'm here in initKpoint before mpmesh."
            !if ((.not. kdirectIsFind) .and. (.not. kcartesianIsFind)) then
            call mpmesh(FRC%kpoint%gridn,FRC%kpoint%kshift,FRC%kpoint%kdirect)
            !end if
            !write(*,*) "I'm here in initKpoint after mpmesh."
            
            nkpt = size(FRC%kpoint%kdirect,1)
            if (all(FRC%kpoint%kdirect == 0)) then
                FRC%symmetry%spacesymmetry = .FALSE.
                FRC%symmetry%pointsymmetry = .FALSE.
                FRC%symmetry%timereversal  = .FALSE.
                FRC%init%symmetry          = .FALSE.
                deallocate(FRC%symmetry%sym_dir,FRC%symmetry%sym_t,FRC%symmetry%sym_perm,FRC%symmetry%sym_rec)
                call initSymmetry(inputFromFile,FRC)
            end if 
            
            !allocate(kred(nkpt,3),ikpt(nkpt),isop(nkpt))
            !allocate(kred(size(FRC%kpoint%kdirect,1),size(FRC%kpoint%kdirect,2)))
            !kred = FRC%kpoint%kdirect
            
            !forall(ii=1:nkpt)
            !    isop(ii) = 1
            !end forall
            
            timerev = FRC%symmetry%timereversal
            tmp(1) = 2
            tmp(2) = 1
            tmp(3) = 3
            allocate(sym_dir(size(FRC%symmetry%sym_dir,1),size(FRC%symmetry%sym_dir,2),size(FRC%symmetry%sym_dir,3)))
            sym_dir = dble(FRC%symmetry%sym_dir)
            call permute(sym_dir,tmp,symDirTmp)

            !write(*,*) "I'm in initKpoint, after pemute."

            allocate(symDirT(size(symDirTmp,1),size(symDirTmp,2),size(symDirTmp,3)))
            symDirT = int(symDirTmp)
            !write(*,*) "I'm in initKpoint, before symTrans."
            tol = 10E-6
            !write(*,*) "FRC%symmetry%sym_dir ="
            !write(*,*)  FRC%symmetry%sym_dir
            if (FRC%symmetry%spacesymmetry .or. FRC%symmetry%pointsymmetry .or. timerev) then
                !write(*,*) "I'm in initKpoint, before fold_k_mesh."
                call fold_k_mesh(FRC%kpoint%kdirect,symDirT,timerev,tol,kred,ikpt,ired,isop)
                !write(*,*) "I'm in initKpoint, after fold_k_mesh."
            else
                allocate(kred(nkpt,3),ikpt(nkpt),isop(nkpt))
                kred = FRC%kpoint%kdirect
                forall(ii=1:nkpt)
                    ikpt(ii) = ii
                end forall
                isop   = 1
            end if
            !write(*,*) "I'm in initKpoint, after symTrans1."
            allocate(symTrans(size(FRC%symmetry%sym_t,1),size(FRC%symmetry%sym_t,2)))
            symTrans = FRC%symmetry%sym_t

            !write(*,*) "I'm in initKpoint, after symTrans2."

            if (FRC%symmetry%spacesymmetry) then
                ntmp = 0
                do ii = 1,size(FRC%symmetry%sym_t,1),1
                    if (all(symTrans(ii,:) == 0)) then
                        ntmp = ntmp+1
                    end if
                end do
                allocate(symInd(ntmp))
                ntmp = 0
                do ii = 1,size(FRC%symmetry%sym_t,1),1
                    if (all(symTrans(ii,:) == 0)) then
                        ntmp         = ntmp+1
                        symInd(ntmp) = ii
                    end if
                end do
                
                allocate(symrecTmp(3,3,ntmp))
                forall(jj=1:ntmp)
                    symrecTmp(:,:,jj) = symDirT(:,:,symInd(jj))
                end forall
                
                if (size(symrecTmp,3) > 0) then
                    call fold_k_mesh(FRC%kpoint%kdirect,symrectmp,timerev,tol,kredtmp,ikpttmp,iredtmp,isoptmp)
                    allocate(ikinput1(size(ikpttmp)))
                    allocate(ikinput2(size(ikpt)))
                    ikinput1 = abs(ikpttmp)
                    ikinput2 = abs(ikpt)
                    call ismember_1DintArray(ikinput1,ikinput2,upind)
                    do ii = 1,size(upind),1
                        if (upind(ii)) then
                            ikpt(ii) = ikpttmp(ii)
                            if (isoptmp(ii) < 0) then
                                isop(ii) = -symInd(abs(isoptmp(ii)))
                            else if (isoptmp(ii) > 0) then
                                isop(ii) = symInd(abs(isoptmp(ii)))
                            else
                                isop(ii) = 0
                            end if
                        end if
                    end do
                    
                    deallocate(kredtmp,ikpttmp,iredtmp,isoptmp,ikinput1,ikinput2,upind)
                end if
                deallocate(symInd,symrecTmp)
            end if
            !write(*,*) "I'm in initKpoint, after symInd and symrecTmp"            
            if ((FRC%symmetry%spaceSymmetry .or. FRC%symmetry%pointSymmetry) .and. timerev) then
                allocate(eyem(3,3))
                eyem = 0
                forall(ii=1:3)
                    eyem(ii,ii) = 1
                end forall
                call symIndex(symDirT,eyem,symInd)
                
                allocate(symrecTmp(3,3,size(symInd)))
                forall(jj=1:size(symInd))
                    symrecTmp(:,:,jj) = symDirT(:,:,symInd(jj))
                end forall
                
                if (size(symrecTmp,3) > 0) then
                    call fold_k_mesh(FRC%kpoint%kdirect,symrectmp,timerev,tol,kredtmp,ikpttmp,iredtmp,isoptmp)
                    allocate(ikinput1(size(ikpttmp)))
                    allocate(ikinput2(size(ikpt)))
                    ikinput1 = abs(ikpttmp)
                    ikinput2 = abs(ikpt)
                    call ismember_1DintArray(ikinput1,ikinput2,upind)
                    do ii = 1,size(upind),1
                        if (upind(ii)) then
                            ikpt(ii) = ikpttmp(ii)
                            if (isoptmp(ii) < 0) then
                                isop(ii) = -symInd(abs(isoptmp(ii)))
                            else if (isoptmp(ii) > 0) then
                                isop(ii) = symInd(abs(isoptmp(ii)))
                            else
                                isop(ii) = 0
                            end if
                        end if
                    end do
                end if
                deallocate(eyem)
                
            end if
            
            
            
            allocate(eyem(3,3))
            eyem = 0
            forall(ii=1:3)
                eyem(ii,ii) = 1
            end forall
            call symIndex(symDirT,eyem,eyeInd)
            
            ntmp = 0
            do ii = 1,nkpt,1
                if (ii == ikpt(ii)) then
                    ntmp     = ntmp+1
                    isop(ii) = eyeInd(1)
                end if
            end do
            
            
            
            tol = 10D-8
            
            allocate(tmp1(nkpt,3))
            forall(ii=1:nkpt,jj=1:3)
                tmp1(ii,jj) = dble(anint(FRC%kpoint%kdirect(abs(ikpt(ii)),jj)/tol))*tol
            end forall
            allocate(tmp2(size(kred,1),size(kred,2)))
            forall(ii=1:size(kred,1),jj=1:size(kred,2))
                tmp2(ii,jj) = dble(anint(kred(ii,jj)/tol))*tol
            end forall
            if (FRC%symmetry%spacesymmetry .or. FRC%symmetry%pointsymmetry .or. timerev) then
                deallocate(ired)
            end if
            call ismemberRow(tmp1,tmp2,nonsense,ired)
            do ii = 1,nkpt,1
                if (ikpt(ii) == 0) then
                    ired(ii) = ired(ii)*0
                else if (ikpt(ii) < 0) then
                    ired(ii) = -ired(ii)
                end if
            end do
            tol = 1.0D0
            !allocate(iredtmp(size(ired)))
            iredtmp = abs(ired)
            call accumarray(iredtmp,tol,weight)
            !do ii = 1,nkpt,1
                
            weight = weight/sum(weight)
            allocate(FRC%kpoint%kcartesian(size(FRC%kpoint%kdirect,1),size(FRC%kpoint%kdirect,2)))
            FRC%kpoint%kcartesian = matmul(FRC%kpoint%kdirect,bvec)
            allocate(FRC%kpoint%ikdirect(size(kred,1),size(kred,2)))
            FRC%kpoint%ikdirect   = kred
            allocate(FRC%kpoint%weight(size(weight)))
            FRC%kpoint%weight     = weight
            allocate(FRC%kpoint%ikpt(size(ikpt)))
            FRC%kpoint%ikpt       = ikpt
            allocate(FRC%kpoint%ired(size(ired)))
            FRC%kpoint%ired       = ired
            allocate(FRC%kpoint%isop(size(isop)))
            FRC%kpoint%isop       = isop
        else if (trim(FRC%kpoint%kType) == trim("line")) then
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("kdirect")) then
            !        kdirectIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (kdirectIsFind) then
            !    call readKpointfile(FRC)
            !    FRC%kpoint%gridn(1)  = size(FRC%kpoint%kdirectPoints,1)
            !    FRC%kpoint%gridn(2)  = 0
            !    FRC%kpoint%gridn(3)  = 0
            !    FRC%kpoint%sympoints = 0
            !    forall(ii=1:FRC%kpoint%gridn(1))
            !        FRC%kpoint%permkpt = ii
            !    end forall
            !end if
            
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile(jj)%name) == trim("kcartesian")) then
            !        kcartesianIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if ((.not. kdirectIsFind) .and. kcartesianIsFind) then
            !    call readKpointfile(FRC)
            !    call inversion(FRC%domain%latvec,bvec)
            !    bvec = 2*pi*transpose(bvec)
            !    call rightdivision(FRC%kpoint%directKpoints,bvec,FRC%kpoint%directKpoints)
            !    FRC%kpoint%gridn(1) = size(FRC%kpoint%kdirectPoints,1)
            !    FRC%kpoint%gridn(2) = 0
            !    FRC%kpoint%gridn(3) = 0
            !    FRC%kpoint%sympoints = 0
            !    forall(ii=1:FRC%kpoint%gridn(1))
            !        FRC%kpoint%permkpt = ii
            !    end forall
            !end if
            
            !if ((.not. kdirectIsFind) .and. (.not. kcartesianIsFind)) then
            !    crystr = FRC%domain%bravaisLattice
            !    ii = 0
            !    do while(ii < nline)
            !        ii     = ii+1
            !        if (trim(inputFromFile%NAV(ii)%name) == trim("ksymmetryPoints")) then
            !            ksymmetryPointsIsFind = .TRUE.
            !            exit
            !        end if
            !    end do
            !    if (ksymmetryPointsIsFind) then
            !        FRC%kpoint%ksymmetryPoints = inputFromFile%NAV(ii)%value
            !    else
            !        call autoline(crystr,FRC%kpoint%ksymmetryPoints)
            !    end if
            !    
            !    ksymmetryPointsSize = size(FRC%kpoint%ksymmetryPoints)
            !    if (all(ksymmetryPointsSize == 0)) then
            !        ksymmetryPointsIsempty = .TRUE.
            !    end if
            !    
            !    if (ksymmetryPointsIsempty .and. mpirank == 0) then
            !        write(*,*) "error"
            !    end if
            !    
            !    sympts = FRC%kpoint%ksymmetryPoints
            !    ii = 0
            !    do while(ii < nline)
            !        ii     = ii+1
            !        if (trim(inputFromFile%NAV(ii)%name) == trim("kgridn")) then
            !            kgridnIsFind = .TRUE.
            !            exit
            !        end if
            !    end do
            !    if (kgridnIsFind) then
            !        FRC%kpoint%kgridn = inputFromFile%NAV(ii)%value
            !        call parsekline(sympts,symtmp)
            !        call klinelength(crystr,symtmp,avec,klen)
            !        FRC%kpoint%kgridn = ceiling(sum(klen)/0.015)
            !    else
            !        FRC%kpoint%kgridn = inputFromFile%NAV(ii)%value
            !    end if
            !    nkpt = FRC%kpoint%kgridn(1)
            !    call kline(crystr,sympts,nkpt,avec,FRC%kpoint%sympoints,FRC%kpoint%kdirect,FRC%kpoint%symindex,FRC%kpoint%permkpt)
            !end if
            
            !forall(ii=1:nkpt)
            !    FRC%kpoint%weight(ii) = 1/nkpt
            !    FRC%kpoint%ired(ii)   = ii
            !    FRC%kpoint%isop(ii)   = 1
            !end forall
            
            !FRC%LCAO%dynSubRed = .TRUE.
            !FRC%DOS%status     = .FALSE.
            !FRC%force%status   = .FALSE.
        end if
        
        forall(ii=1:3)
            FRC%kpoint%currentreduce(ii) = int(1.0/spe)
        end forall
        FRC%init%kpoint = .TRUE.
    end if
    
    return
    end subroutine initKpoint
    end module initKpoint_module
    
