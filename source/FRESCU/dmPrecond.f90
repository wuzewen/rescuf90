! dmPrecond.f90

!**************************************************
      !
      !
      !
!****************************************************

      module dmPrecondMod
      contains
      subroutine dmPrecond(FRC,rho,bc)

      implicit none

      bmin = FRC%mixing%betamin
      sigma = 2.0D0
      xyz   = FRC%atom%xyz
      natom = size(FRC%atom%element)
      forall(ii=1:natom)
              ind(ii) = FRC%atom%element(ii) .EQ. 1
      end forall
      ntmp  = count(ind .EQV. .TRUE.)
      allocate(xyztmp(ntmp))
      ntmp = 0
      do ii = 1,natom,1
          if (ind(ii)) then
                  ntmp = ntmp + 1
                  xyztmp(ntmp) = xyz(ii,3)
          end if
      end do
      zmin = minval(xyztmp)
      zmax = maxval(xyztmp)
      call fzeroZZZ(zmin,z0)
      call fzeroZZZ(zmax,z1)
      z0 = min(z0,z1)
      z1 = max(z0,z1)
      call bsxfumTimes
      beta = sqrt(beta)
      rho = rho*beta

      return
      end subroutine dmPrecond
      end module dmPrecondMod

