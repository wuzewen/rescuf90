! MPISEND.f90

!************************************************************************
      !
      !
      !
!**********************************************************************************************

      module MPISENDMOD

      interface MPISEND
              module procedure MPI_Send_variable_real8_2D
              module procedure MPI_Send_variable_real8_1D
              module procedure MPI_Send_variable_cmplx_2D
              module procedure MPI_Send_variable_cmplx_1D
      end interface MPISEND

      end module MPISENDMOD
