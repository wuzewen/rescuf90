! writeBasis.f90

      module writeBasis_module
      contains
      subroutine writeBasis(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      open(unit=11,file='Vnl.txt')
      do ii = 1,FRC%SOB(1)%SizeOfVnl
      write(11,*) "Vnl",ii
      write(11,*) "rrData", FRC%ElementData(1)%Vnl(ii)%rrData
      write(11,*) "vvData", FRC%ElementData(1)%Vnl(ii)%vvData
      end do


