! norm.f90
!*****************************************
    !
!*****************************************
    
    module norm_2D_module
    contains
    subroutine norm_2D(ArrayA,p,mo)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: ArrayA(:,:)
    integer             :: p
    
    ! temporery variables
    real*8, allocatable :: ArrayT(:,:)
    
    ! output variables
    real*8              :: mo
    
    ! body of this function
    allocate(ArrayT(size(ArrayA,1),size(ArrayA,2)))
    ArrayT = abs(ArrayA)

    !write(*,*) "abs(ArrayT) in no2"
    !write(*,*)  ArrayT
    !write(*,*)  "P = ", p
    mo     = (sum(ArrayT**dble(p)))**(dble(1.0D0)/dble(p))
    !write(*,*)  "sum**"
    !write(*,*)  sum(ArrayT**p)

    !write(*,*) "ArrayA in norm_2D."
    !write(*,*)  ArrayA
    !write(*,*) "mo in NOrm2D"
    !write(*,*) mo
    
    return
    end subroutine norm_2D
    end module norm_2D_module
