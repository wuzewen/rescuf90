! smiZHEEVX.f90

!******************************************************
      !
      !
      !
!***********************************************************

      module smiZHEEVXMOD
      contains
      subroutine smiZHEEVX(jobz,irange,UPLO,N,A,MB,NB,nprow,npcol,&
                      VL,VU,IL,IU,ABSTOL,ORFAC,LRWORK,W,Z)

      implicit none

      integer :: N, MB, NB, nprow, npcol
      integer :: IL, IU, LRWORK
      character(len=1) :: jobz, irange, UPLO
      real*8 :: VL, VU, ABSTOL, ORFAC
      complex*16, allocatable :: A(), W(), Z()

      integer :: M, NZ, LWORK, LRWORKTMP, LIWORK, INFO, nprocs
      integer, allocatable :: IWORK(), IFAIL(), ICLUSTR()
      complex*16, allocatable :: WORK(), RWORK(), GAP()

      integer :: iam, ictxt, myrow, mycol

      LWORK  = -1
      LIWORK = -1
      nprocs =  nprow*npcol
      ictxt  =  0
      myrow  =  0
      mycol  =  0
      allocate(IFAIL(N),ICLUSTR(2*nprow*npcol))
      allocate(GAP(nprow*npcol))

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,Z_np)
      A_np = 
      Z_np = 

      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descZ,N,N,MB,NB,izero,izero,ictxt,Z_np,descinfo)

      allocate(WORK(1))
      allocate(RWORK(3))
      allocate(IWORK(1))

      call PZHEEVX(jobz,irange,UPLO,N,A,ione,ione,descA,VL,VU,IL,IU,&
              ABSTOL,M,NZ,W,ORFAC,Z,ione,ione,descZ,WORK,LWORK,RWORK,&
              LRWORK,IWORK,LIWORK,IFAIL,ICLUSTR,GAP,INFO)
      LWORK = WORK(1)
      deallocate(WORK)
      allocate(WORK(LWORK))
      LIWORK = IWORK(1)
      deallocate(IWORK)
      allocate(IWORK(LIWORK))

      LRWORKTMP = 1.5*RWORK(1)*RWORK(1)/(RWORK(1)+1024)+1024
      if (LRWORK .LT. LRWORKTMP .OR. LRWORK .LT. 0) then
              LRWORK = LRWORKTMP
      end if
      deallocate(RWORK)
      allocate(RWORK(LRWORK))
      call PZHEEVX(jobz,irange,UPLO,N,A,ione,ione,descA,VL,VU,IL,IU,&
              ABSTOL,M,NZ,W,ORFAC,Z,ione,ione,descZ,WORK,LWORK,RWORK,&
              LRWORK,IWORK,LIWORK,IFAIL,ICLUSTR,GAP,INFO)
      if (INFO) then
              write(*,*) 'Error'
      else if () then
              write(*,*) 'Error'
      else if () then
              write(*,*) 'Error'
      else if () then
              write(*,*) 'Error'
      end if
      deallocate(WORK,IWORK,RWORK)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiZHEEVX
      end module smiZHEEVXMOD
