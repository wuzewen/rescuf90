        !COMPILER-GENERATED INTERFACE MODULE: Tue Jan 15 11:51:10 2019
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INITDISTARRAY_2D_SPARSE__genmod
          INTERFACE 
            SUBROUTINE INITDISTARRAY_2D_SPARSE(M,N,MB,NB,MP,NP,A)
              USE SPARSEVATAMN_2D_REAL8_MODULE
              INTEGER(KIND=4) :: M
              INTEGER(KIND=4) :: N
              INTEGER(KIND=4) :: MB
              INTEGER(KIND=4) :: NB
              INTEGER(KIND=4) :: MP
              INTEGER(KIND=4) :: NP
              TYPE (SPARSEVATAMN_2D_REAL8) :: A
            END SUBROUTINE INITDISTARRAY_2D_SPARSE
          END INTERFACE 
        END MODULE INITDISTARRAY_2D_SPARSE__genmod
