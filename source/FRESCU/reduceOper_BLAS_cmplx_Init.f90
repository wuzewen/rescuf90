! reduceOper_BLAS_cmplx.f90
    
!*************************************************************************
    !
    ! In this function, kpt = [0 0 0] is used in GetNonLocalPotentialK.
    ! While if kpt is not [0 0 0], this function should be fixed, with
    ! complex numbers and arrays. When it is done, renew this notes.
    ! Another important thing is, sparse matrix is not used here.
    !
!*************************************************************************
    
    module reduceOper_BLAS_cmplx_Init_module
    contains
    subroutine reduceOper_BLAS_cmplx_Init(Acell,Av,kpt,oper)
    
    !use RealVata_2D_module
    !use bsxfunTimes_module
    use SparseArray_2D_Cmplx_module
    use SparseArray_2D_Real8_module
    use SparseVataMN_2D_Real8_module

    implicit none
    
    ! input variables
    !logical                        :: spnnz
    !type(SparseArray_2D_Real8), allocatable :: Acell(:)
    type(SparseVataMN_2D_Real8), allocatable :: Acell(:)
    real*8                         :: kpt(3)
    real*8, allocatable            :: Av(:,:)
    
    ! temporary variables
    real*8     :: pi, datatmp(3,1), tmp
    integer    :: ncell
    complex*16 :: im
    integer    :: ii, job(8), info, jj, x
    integer    :: nzmax1, n1, nnz1, nzmax2, n2, nnz2, nzmax
    integer    :: m, n, sort, request
    integer    :: ntmp1, ntmp2
    complex*16 :: beta
    character(len=1) :: trans
    integer, allocatable    :: ja1(:), ia1(:), ja2(:), ia2(:), jc(:), ic(:)
    complex*16, allocatable :: acsr1(:), acsr2(:), c(:)
    type(SparseArray_2D_Cmplx), allocatable :: OperCell(:)
    
    ! output variables
    type(SparseArray_2D_Cmplx) :: Oper
    
    ! body of this function
    im    = (0.0D0,1.0D0)
    pi    = 3.1415926535897932385D0
    ncell = size(Acell)
    allocate(OperCell(ncell))
    
    do ii = 1,ncell,1
        datatmp(:,1)           = Av(ii,:)
        OperCell(ii)%SIZE1     = Acell(ii)%SIZE1
        OperCell(ii)%SIZE2     = Acell(ii)%SIZE2
        OperCell(ii)%N_nonzero = Acell(ii)%N_nonzero
        allocate(OperCell(ii)%Index1(OperCell(ii)%N_nonzero))
        allocate(OperCell(ii)%Index2(OperCell(ii)%N_nonzero))
        allocate(OperCell(ii)%ValueN(OperCell(ii)%N_nonzero))
        OperCell(ii)%Index1    = Acell(ii)%Index1
        OperCell(ii)%Index2    = Acell(ii)%Index2
        if (OperCell(ii)%N_nonzero > 0) then
                 tmp                   = kpt(1)*datatmp(1,1)+kpt(2)*datatmp(2,1)+kpt(3)*datatmp(3,1) !matmul(kpt,datatmp) !
                 OperCell(ii)%ValueN   = dcmplx(Acell(ii)%ValueN)*exp(dcmplx(2.0D0)*im*dcmplx(pi)*cmplx(tmp))
        end if
    end do

    if (ncell == 1) then
            Oper%SIZE1     = OperCell(1)%SIZE1
            Oper%SIZE2     = OperCell(1)%SIZE2
            Oper%N_nonzero = OperCell(1)%N_nonzero
            allocate(Oper%Index1(Oper%N_nonzero))
            allocate(Oper%Index2(Oper%N_nonzero))
            allocate(Oper%ValueN(Oper%N_nonzero))
            Oper%Index1    = OperCell(1)%Index1
            Oper%Index2    = OperCell(1)%Index2
            Oper%ValueN    = OperCell(1)%ValueN
    else
            nzmax1         = OperCell(1)%N_nonzero
            job(1)         = 1
            job(2)         = 1
            job(3)         = 1
            job(4)         = 2
            job(5)         = nzmax1
            job(6)         = 0
            n1             = OperCell(1)%SIZE1
            nnz1           = OperCell(1)%N_nonzero
            !write(*,*) OperCell(1)%SIZE1, OperCell(1)%SIZE2,OperCell(1)%N_nonzero
            !write(*,*) size(OperCell(1)%Index1),size(OperCell(1)%Index2),size(OperCell(1)%ValueN)
            !do jj = 1,OperCell(1)%N_nonzero,1
            !write(*,*) OperCell(1)%Index1(jj),OperCell(1)%Index2(jj),OperCell(1)%ValueN(jj)
            !end do
            
            
            !write(*,*) "sizes", n1, nnz1
            !write(*,*) "OperCell(1)%Index1 ="
            !write(*,*)  OperCell(1)%Index1
            !write(*,*) "OperCell(1)%Index2 ="
            !write(*,*)  OperCell(1)%Index2
            allocate(acsr1(nnz1),ja1(nnz1),ia1(n1+1))
            call mkl_zcsrcoo(job,n1,acsr1,ja1,ia1,nnz1,OperCell(1)%ValueN,OperCell(1)%Index1,OperCell(1)%Index2,info)

            !write(*,*) "info  =", info
            !do jj = 1,nnz1,1
            !write(*,*) OperCell(1)%Index1(jj),ja1(jj),acsr1(jj)
            !end do
            !write(*,*) "size of ia", size(ia1)
            !do jj = 1,n1+1,1
            !write(*,*) ia1(jj)
            !end do
            !write(*,*) "acsr1 ="
            !write(*,*)  acsr1
            !write(*,*) "ja1 =",size(ja1),ja1
            !write(*,*) "ia1 =",ia1

            do ii = 2,ncell,1
                !write(*,*) "ii =", ii
                nzmax2         = OperCell(ii)%N_nonzero
                job(5)         = nzmax2
                n2             = OperCell(ii)%SIZE1
                nnz2           = OperCell(ii)%N_nonzero
                allocate(acsr2(nnz2),ja2(nnz2),ia2(n2+1))
                call mkl_zcsrcoo(job,n2,acsr2,ja2,ia2,nnz2,OperCell(ii)%ValueN,OperCell(ii)%Index1,OperCell(ii)%Index2,info)
                !do jj = 1,nnz2,1
                !write(*,*) ja2(jj),acsr2(jj)
                !end do
                !write(*,*) "acsr2 ="
                !write(*,*)  acsr2
                !write(*,*) "ja2 =",size(ja2),ja2
                !write(*,*) "ia2 =",ia2
                trans   = 'N'
                request =  1
                sort    =  3
                m       =  OperCell(ii)%SIZE1
                n       =  OperCell(ii)%SIZE2
                beta    =  (1.0D0,0.0D0)
                nzmax   =  nzmax2+nzmax1
                allocate(ic(m+1))
                allocate(c(nzmax))
                allocate(jc(nzmax))
                !write(*,*) "parameters ="
                !write(*,*) "trans =", trans, "request =", request, "sort =", sort, "m = ", m, "n =", n

                !write(*,*) "size of inputs",size(acsr1),size(ja1),size(ia1),size(acsr2),size(ja2),size(ia2)
                !write(*,*) "acsr1 ="
                !write(*,*)  acsr1
                !write(*,*) "ja1 ="
                !write(*,*)  ja1
                !write(*,*) "ia1 ="
                !write(*,*)  ia1
                !write(*,*) "acsr2 ="
                !write(*,*)  acsr2
                !write(*,*) "ja2 ="
                !write(*,*)  ja2
                !write(*,*) "ia2 ="
                !write(*,*)  ia2
                call mkl_zcsradd(trans,0,sort,m,n,acsr1,ja1,ia1,beta,acsr2,ja2,ia2,c,jc,ic,nzmax,info)
                !write(*,*) "ic",ic
                !write(*,*) "c=",c
                !stop
                !request =  2
                !nzmax   =  ic(m+1)-1
                !deallocate(jc,c)
                !allocate(jc(ic(m+1)-1),c(ic(m+1)-1))
                !call mkl_zcsradd(trans,request,sort,m,n,acsr1,ja1,ia1,beta,acsr2,ja2,ia2,c,jc,ic,nzmax,info)
                ntmp1 = count(jc == 0)
                ntmp1 = nzmax-ntmp1
                deallocate(acsr1,ja1,ia1,acsr2,ja2,ia2)
                allocate(acsr1(ntmp1),ja1(ntmp1),ia1(size(ic)))
                acsr1  = c(1:ntmp1)
                ja1    = jc(1:ntmp1)
                ia1    = ic
                nzmax1 = nzmax
                deallocate(c,ic,jc)
            end do
            !stop
            ntmp1 = count(ja1 == 0)
            ntmp2 = count(ia1 == 0)
            nzmax = nzmax - ntmp1
            allocate(c(nzmax),jc(nzmax))

            forall(ii=1:nzmax)
                    c(ii) = acsr1(ii)
                    jc(ii) = ja1(ii)
            end forall

            job(1) = 0
            job(5) = nzmax
            job(6) = 3

            !write(*,*) "nzmax =", nzmax
            !write(*,*) "size of C", size(c), size(jc), size(ia1)
            allocate(Oper%ValueN(nzmax),Oper%Index1(nzmax),Oper%Index2(nzmax))
            call mkl_zcsrcoo(job,n1,c,jc,ia1,nzmax,Oper%ValueN,Oper%Index1,Oper%Index2,info)
            Oper%SIZE1     = OperCell(1)%SIZE1
            Oper%SIZE2     = OperCell(1)%SIZE2
            Oper%N_nonzero = nzmax
            !write(*,*) "Sucessful"
    end if

    return
    end subroutine reduceOper_BLAS_cmplx_Init
    end module reduceOper_BLAS_cmplx_Init_module
