! solveKSrealDirectly.f90
    
!*****************************************************************
    !
    !
    !
!*****************************************************************
    
    module solveKSrealDirectly_module
    contains
    subroutine solveKSrealDirectly(FRC)
    
    use hamOper2mat_module
    use hamOper2mat_Sparse_module
    use FORTRAN_RESCU_CALCULATION_TYPE
    external ZHEEV
    !implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    integer :: nkpt, ii, N, LWORK, info, nband, jj, kk, LDA!, iter
    real*8  :: kpt(3)
    character(len=1) :: JOBZ, UPLO
    complex*16, allocatable :: H(:,:), WORK(:)
    !complex, allocatable :: H(:,:), WORK(:)
    !real   , allocatable :: RWORK(:), W(:)
    real*8 , allocatable :: RWORK(:), W(:)
    ! output variables
    
    ! body of this function
    iter  = FRC%scloop
    !write(*,*) "iter=",iter
    nband = FRC%eigenSolver%nband
    !write(*,*) "nband=",nband
    nkpt  = size(FRC%kpoint%ikdirect,1)
    !write(*,*) "nkpt=",nkpt
    JOBZ  = 'V'
    UPLO  = 'U'
    N     = product(FRC%domain%cgridn)
    !write(*,*) "N=", N
    LDA   = N
    allocate(W(N))
    !LDVL  = N
    !allocate(VR(N,N))
    !LDVR  = N
    LWORK =  2*N-1
    allocate(WORK(LWORK))
    allocate(RWORK(3*N-2))
    !allocate(FRC%energy%ksnrg(iter)%vata(FRC%eigenSolver%nband,nkpt,1))
    !write(*,*) "I'm here before the loop in solveKSrealDirectly."
    !write(*,*) "ikdirect"
    !write(*,*)  FRC%kpoint%ikdirect
    allocate(H(N,N))
    do ii = 1,nkpt,1
        kpt = FRC%kpoint%ikdirect(ii,:)
        !write(*,*) "I'm here before hamOper2Mat in solveKSrealDirectly"
        !call hamOper2Mat(FRC,kpt,H)
        call hamOper2Mat_Sparse(FRC,kpt,H)
        !write(*,*) "size of H"
        !write(*,*)  size(H,1), size(H,2)
        !do jj = 1,size(H,1),1
        !    do kk = 1,size(H,2),1
        !        write(*,*)  jj,kk,H(jj,kk)
        !    end do
        !end do

        !stop
        !write(*,*) "H ="
        !write(*,*)  H
        !write(*,*) "kpt in solveKSreal."
        !write(*,*)  kpt
        !write(*,*) "I'm here after hamOper2Mat in solveKSrealDirectly"
        !write(*,*) "inputs of ZHEEV"
        !write(*,*) "JOBZ, UPLO,N,LDA,LWORK,INFO"
        !write(*,*)  JOBZ, UPLO,N,LDA,LWORK,INFO
        !write(*,*) "size of W, WORK, RWORK"
        !write(*,*)  size(W), size(WORK), size(RWORK)
        call ZHEEV(JOBZ,UPLO,N,H,LDA,W,WORK,LWORK,RWORK,INFO)
        !call CHEEV(JOBZ,UPLO,N,H,LDA,W,WORK,LWORK,RWORK,INFO)
        !write(*,*) "I'm here after ZHEEV in solveKSrealDirectly"
        !deallocate(FRC%psi(ii,1)%vata)
        !write(*,*) "size of psi in solveKSrealDirectly"
        !write(*,*)  size(FRC%psi,1), size(FRC%psi,2)
        !write(*,*) "I'm here after deallocate psi"
        !allocate(FRC%psi(ii,1)%vata(size(H,1),size(H,2)))
        !write(*,*) "size of psi%vata"
        !write(*,*)  size(FRC%psi(ii,1)%vata,1), size(FRC%psi(ii,1)%vata,2)
        !write(*,*) "size of H after ZHEEV"
        !write(*,*)  size(H,1), size(H,2), ii
        !write(*,*)  size(H(:,1:nband),1), size(H(:,1:nband),2)
        !write(*,*)  size(FRC%psi(ii,1)%vata,1), size(FRC%psi(ii,1)%vata,2)
        !write(*,*) "LOOKING"
        !write(*,*)  H
        !write(*,*) "what happened here."
        !if (iter == 1) then
        FRC%psi(ii,1)%vata                  = H(:,1:nband)
        !end if
        !write(*,*) "here is A"
        FRC%psi(ii,1)%m                     = size(H,1)
        !write(*,*) "here is B", FRC%psi(ii,1)%m
        FRC%psi(ii,1)%n                     = size(H,2)
        !write(*,*) "here is C", FRC%psi(ii,1)%n
        FRC%psi(ii,1)%mblock                = size(H,1)
        !write(*,*) "here is D", FRC%psi(ii,1)%mblock
        FRC%psi(ii,1)%nblock                = 1
        !write(*,*) "here is E", FRC%psi(ii,1)%nblock
        FRC%psi(ii,1)%mproc                 = 1
        !write(*,*) "here is F", FRC%psi(ii,1)%mproc
        FRC%psi(ii,1)%nproc                 = 1
        !write(*,*) "I'm here after psi", FRC%psi(ii,1)%nproc
        !deallocate(FRC%energy%ksnrg(iter)%vata)
        !allocate(FRC%energy%ksnrg(iter)%vata(size(H,1),nkpt,1))
        FRC%energy%ksnrg%vata(:,ii,1) = W(1:nband)
        !write(*,*) "INFO = ", INFO
        !write(*,*) "Wave function in solveKSrealDirectly."
        !do jj = 1,size(H,1),1
        !    do kk = 1,size(H,2),1
        !        write(*,*) jj,kk,H(jj,kk)
        !    end do
        !end do
        !write(*,*) "W in solveKSrealDirectly."
        !write(*,*)  W(1:20)
        !write(*,*) "wavefunction in solveKSrealDirectly."
        !do kk = 1,20,1
        !do jj = 1,size(H,1),1
        !    write(*,*)  jj,kk,H(jj,kk)
        !end do
        !end do
        !deallocate(H)
        !write(*,*) "stop in solveKSrealDirectly."
        !stop
    end do
    !write(*,*) "ksnrg in solveKS."
    !do ii = 1,size(FRC%energy%ksnrg(iter)%vata,1),1
    !    write(*,*) FRC%energy%ksnrg(iter)%vata(ii,:,1)
    !end do
    !write(*,*) "iter =", iter
    !if (iter == 2) then
    !    write(*,*) "I'm here at the end of solveKSrealDirectly and stop."
    !    stop
    !end if
    return
    end subroutine solveKSrealDirectly
    end module solveKSrealDirectly_module
