! calcVeffCell.f90

!********************************************************************
      !
      !
      !
!***********************************************************************************

      module calcVeffCellMod
      contains
      subroutine calcVeffCell(FRC,veff,veffCell,vv)

      implicit none

      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpirank
      mb      = FRC%smi%mb
      nb      = FRC%smi%nb
      mp      = FRC%smi%mp
      np      = FRC%smi%np

      buff    = FRC%option%buffersize
      sprs    = FRC%LCAO%sprs
      dr      = vol/product(FRC%domain%cgridn)
      natom   = size(FRC%atom%elemnt)
      ns      = FRC%spin%ispin
      atomInd = FRC%LCAO%parameters%evec
      no      = size(atmoInd)
      pairVec = FRC%LCAO%mDispMapVector
      nt      = size(pairVec)
      sbDisp  = FRC%LCAO%sbDisp
      sbVec   = FRC%LCAO%sbVec
      nnzSmask = FRC%LCAO%nnzSmaskLoc
      call reduceOper(sbDisp,sbVec,(/0,0,0/),nnzSmask,Smask)
      call GetGlobalInd(Smask,iloc,jloc)

      if (ns .EQ. 4) then
              Veff%vata(:,1) = dble(veff%vata(:,1))
              veff%vata(:,2) = dble(veff%vata(:,2))
              veff%vata(:,3) = aimag(veff%vata(:,3))
              veff%vata(:,4) = dble(veff%vata(:,4))
      end if

      basis = FRC%LCAO%basis
      samllRegion = FRC%LCAO%samllregion
      calculateFlags = 
      mConvergeIndex = FRC%LCAO%mConvergeIndexLocal
      mGridCluster   = 5
      mImageAtoms    = FRC%LCAO%mImageAtoms
      mDispMapIndex  = FRC%LCAO%mDispMapIndex
      call calcSelectedAtomList(mpirank,Smask,atomInd,size(mImageAtoms)/natom,selatom)
      SelectedAtomList = selatom
      vDense           = nnzSmask .LT. 0 .AND. buff .GT. nt*no*no*2

      if (vDense) then
              xx
      else
          call ismemberRow(sbVec,pairVec,ind1,ind2)
          vtmp = sbDisp(ind1)
          vtmp = 
          vtmp = 
          do ii = 1,size(vtmp),1
              forall(jj=1:size(iloc),kk=1:size(jloc))
                      tmp(iloc(jj),jloc(kk)) = vtmp(ii)%vata(jj,kk)
              end forall
              vtmp(ii)%vata = tmp
          end do
      end if

      do ss = 1,ns,1
          if (vDense) then
              forall(ii=1:nt)
                      matrix(1)%xx(ii)%vata = 0.0D0
              end forall
          else
              matrix = vtmp
              call cellfunSpfun(eps,)
          end if
          call distmat_submatrix(FRC,veff,ss,rRho)
          call distmat_gather(FRC,rRho)
          call convertRO2U(matrix,basis,rRho,smallRegion,CalculateFlag,&
                  mCoverageAtomList)
          if (mpistat) then
                  call MPI_Barrier()
          end if

          veffCell(1:nt,ss) = matrix(1)%xx(1)%vata
          call cellFunUni()
          call cellFunAdj()
          call cellFunIni()
          call symmetrizeCell(FRC,veffCell,pairVec,1,vv)
          if (all(FRC%kpoint%kdirect .EQ. 0.0D0)) then
              do ii = 2,size(veffCell,1),1
                  if (allocated(veffCell(ii,ss)%vata)) then
                      veffCell(1,ss)%vata = veffCell(1,ss)%vata + &
                              veffCell(ii,ss)%vata
                  end if
                  delet
              end do
          end if
      end do

      if (all(FRC%kpoint%kdirect .EQ. 0.0D0)) then
          vv = (/0,0,0/)
          veffCell = veffCell(1,:)
      end if

      if (ns .EQ. 4) then
              call cellFunUni()
              call cellFunUni()
              call cellFunxy()
              call celFUnUni()
      end if

      return
      end subroutine calcVeffCell
      end module calcVeffCellMod
