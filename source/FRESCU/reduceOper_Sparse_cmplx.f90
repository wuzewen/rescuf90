! reduceOper_Sparse_cmplx.f90
    
!*************************************************************************
    !
    ! In this function, kpt = [0 0 0] is used in GetNonLocalPotentialK.
    ! While if kpt is not [0 0 0], this function should be fixed, with
    ! complex numbers and arrays. When it is done, renew this notes.
    ! Another important thing is, sparse matrix is not used here.
    !
!*************************************************************************
    
    module reduceOper_Sparse_cmplx_module
    contains
    subroutine reduceOper_Sparse_cmplx(Acell,Av,kpt,oper)
    
    !use RealVata_2D_module
    !use bsxfunTimes_module
    use SparseArray_2D_Cmplx_module
    use SparseArray_2D_Real8_module
    use SparseVataMN_2D_Real8_module

    implicit none
    
    ! input variables
    !logical                        :: spnnz
    !type(SparseArray_2D_Real8), allocatable :: Acell(:)
    type(SparseVataMN_2D_Real8), allocatable :: Acell(:)
    real*8                         :: kpt(3)
    real*8, allocatable            :: Av(:,:)
    
    ! temporary variables
    real*8     :: pi, datatmp(3,1), tmp
    integer    :: ncell, Nleft
    complex*16 :: im
    integer    :: ii, jj, kk
    integer    :: ltmp, mtmp, ntmp, ktmp, kk1, kk2
    integer    :: aa(2), bb(2)
    integer, allocatable    :: nsiz(:)
    integer, allocatable    :: inde1(:,:), inde2(:,:), indeA(:,:)
    complex*16, allocatable :: valu1(:), valu2(:), valuA(:)
    type(SparseArray_2D_Cmplx), allocatable :: OperCell(:)
    
    ! output variables
    type(SparseArray_2D_Cmplx) :: Oper
    
    ! body of this function
    im   = (0.0D0,1.0D0)
    pi   = 3.1415926535897932385D0
    !m    = size(Acell(1)%vata,1)
    !n    = size(Acell(1)%vata,2)
    !write(*,*) "kpt in reduceOper_cmplx."
    !write(*,*)  kpt
    !write(*,*) "Av in reduceOper_cmplx."
    !write(*,*)  Av
    !allocate(oper(m,n))
    !oper = (0.0D0,0.0D0)
    
    !if (spnnz >= 0) then
    ncell = size(Acell)
    allocate(OperCell(ncell))
    
    do ii = 1,ncell,1
        datatmp(:,1)           = Av(ii,:)
        OperCell(ii)%SIZE1     = Acell(ii)%SIZE1
        OperCell(ii)%SIZE2     = Acell(ii)%SIZE2
        OperCell(ii)%N_nonzero = Acell(ii)%N_nonzero
        allocate(OperCell(ii)%Index1(OperCell(ii)%N_nonzero))
        allocate(OperCell(ii)%Index2(OperCell(ii)%N_nonzero))
        allocate(OperCell(ii)%ValueN(OperCell(ii)%N_nonzero))
        OperCell(ii)%Index1    = Acell(ii)%Index1
        OperCell(ii)%Index2    = Acell(ii)%Index2
        if (OperCell(ii)%N_nonzero > 0) then
                 tmp                   = kpt(1)*datatmp(1,1)+kpt(2)*datatmp(2,1)+kpt(3)*datatmp(3,1) !matmul(kpt,datatmp) !
                 OperCell(ii)%ValueN   = dcmplx(Acell(ii)%ValueN)*exp(dcmplx(2.0D0)*im*dcmplx(pi)*cmplx(tmp))
        end if
    end do

    if (ncell == 1) then
            Oper%SIZE1     = OperCell(1)%SIZE1
            Oper%SIZE2     = OperCell(1)%SIZE2
            Oper%N_nonzero = OperCell(1)%N_nonzero
            allocate(Oper%Index1(Oper%N_nonzero))
            allocate(Oper%Index2(Oper%N_nonzero))
            allocate(Oper%ValueN(Oper%N_nonzero))
            Oper%Index1    = OperCell(1)%Index1
            Oper%Index2    = OperCell(1)%Index2
            Oper%ValueN    = OperCell(1)%ValueN
    else
            ntmp = 0
            allocate(nsiz(ncell))
            do ii = 1,ncell,1
                ntmp     = ntmp+OperCell(ii)%N_nonzero
                nsiz(ii) = OperCell(ii)%N_nonzero
            end do
            write(*,*) "ntmp =", ntmp
            !maxsize = maxval(nsiz)
            allocate(inde1(ntmp,2))
            allocate(inde2(ntmp,2))
            allocate(indeA(ntmp,2))
            allocate(valu1(ntmp))
            allocate(valu2(ntmp))
            allocate(valuA(ntmp))
            inde1 = 0
            inde2 = 0
            indeA = 0
            valu1 = (0.0D0,0.0D0)
            valu2 = (0.0D0,0.0D0)
            valuA = (0.0D0,0.0D0)
            ltmp  = OperCell(1)%N_nonzero
            !write(*,*) "ltmp =", ltmp
            forall(ii=1:ltmp)
                    inde1(ii,1) = OperCell(1)%Index1(ii)
                    inde1(ii,2) = OperCell(1)%Index2(ii)
                    valu1(ii)   = OperCell(1)%ValueN(ii)
            end forall
            !ktmp = 0
            do ii = 2,ncell,1
                    !write(*,*) "ii = ", ii
                    mtmp = OperCell(ii)%N_nonzero
                    !write(*,*) "mtmp =", mtmp
                    forall(jj=1:mtmp)
                            inde2(jj,1) = OperCell(ii)%Index1(jj)
                            inde2(jj,2) = OperCell(ii)%Index2(jj)
                            valu2(jj)   = OperCell(ii)%ValueN(jj)
                    end forall

                    kk1 = 1
                    kk2 = 1
                    ktmp = 0
                    !write(*,*) "ntmp =", ntmp
                    do jj = 1,ntmp,1
                        aa   = inde1(kk1,:)
                        bb   = inde2(kk2,:)
                        ktmp = ktmp+1
                        if (all(aa == bb)) then
                                indeA(ktmp,:) = aa
                                valuA(ktmp)   = valu1(kk1)+valu2(kk2)
                                kk1 = kk1+1
                                kk2 = kk2+1
                                write(*,*) "count the same array."
                        else if (aa(2) < bb(2)) then
                                indeA(ktmp,:) = aa
                                valuA(ktmp)   = valu1(kk1)
                                kk1 = kk1+1
                        else if (aa(2) > bb(2)) then
                                indeA(ktmp,:) = bb
                                valuA(ktmp)   = valu2(kk2)
                                kk2 = kk2+1
                        else if (aa(2) == bb(2) .and. aa(1) < bb(1)) then
                                indeA(ktmp,:) = aa
                                valuA(ktmp)   = valu1(kk1)
                                kk1 = kk1+1
                        else
                                indeA(ktmp,:) = bb
                                valuA(ktmp)   = valu2(kk2)
                                kk2 = kk2+1
                        end if

                        if (kk1 > ltmp .and. kk2 > mtmp) then
                                Nleft = 0
                                exit
                        else if (kk2 > mtmp) then
                                Nleft = ltmp - kk1 + 1
                                forall(kk=1:Nleft)
                                        indeA(kk+ktmp,:) = inde1(kk1+kk-1,:)
                                        valuA(kk+ktmp)   = valu1(kk1+kk-1)
                                end forall
                                ktmp = ktmp + Nleft
                                exit
                        else if (kk1 > ltmp) then
                                Nleft = mtmp - kk2 + 1
                                forall(kk=ktmp+1:ktmp+Nleft)
                                        indeA(kk+ktmp,:) = inde2(kk2+kk-1,:)
                                        valuA(kk+ktmp)   = valu2(kk2+kk-1)
                                end forall
                                ktmp = ktmp + Nleft
                                exit
                        end if
                    end do
                    ltmp  = ktmp
                    inde1 = indeA
                    valu1 = valuA
                    !write(*,*) "kk1", kk1, "kk2",kk2
                    !write(*,*) "ktmp", ktmp
                    !write(*,*) "inde1 ="
                    !write(*,*)  inde1
                    !stop
            end do
            !write(*,*) "ktmp", ktmp
            Oper%SIZE1     = OperCell(1)%SIZE1
            Oper%SIZE2     = OperCell(1)%SIZE2
            Oper%N_nonzero = ktmp
            !allocate(Oper%Index1(ktmp))
            !allocate(Oper%Index2(ktmp))
            !allocate(Oper%ValueN(ktmp))
            Oper%Index1    = indeA(:,1)
            Oper%Index2    = indeA(:,2)
            Oper%ValueN    = valuA
    end if

    return
    end subroutine reduceOper_Sparse_cmplx
    end module reduceOper_Sparse_cmplx_module
