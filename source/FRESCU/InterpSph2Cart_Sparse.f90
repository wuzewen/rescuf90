! InterpSph2Cart_Sparse.f90
    
!******************************************************************************************
    !
    ! In this function, the input rrdata and frdata are numbers, not characters. Because
    ! A variable's content can't be turned to another variable's name in fortran. 
    ! rrdata and frdata should be assigned before this function be called.
    ! While, in matlab version, it is rfield and ffield instead of rrdata and frdata as 
    ! input. And they are characters.
    !
!******************************************************************************************
    
    module InterpSph2Cart_Sparse_module
    contains
    subroutine InterpSph2Cart_Sparse(rrdata,frdata,pos,lqn,gridcoord,avec,bc,ind,fout)
    
    use cellInRange_module
    use flipud_module
    use bsxfunPlus_module
    use interp1_module
    use genrealSH_module
    use bsxfunTimes_module
    use SparseVataMN_2D_Real8_module
    
    implicit none
    
    ! input variables
    integer              :: bc(3), lqn
    real*8               :: pos(1,3), avec(3,3)
    integer, allocatable :: ind(:)
    real*8 , allocatable :: gridcoord(:,:)
    real*8 , allocatable :: rrdata(:), frdata(:)
    
    ! temporary variables
    character(len=20)    :: method
    integer              :: n, inde, ii, jj, nf, nr, nn, ntvec, ntmp, nnn
    real*8               :: eps, rmax
    logical              :: issym
    logical, allocatable :: nonzero(:)
    integer, allocatable :: NonzeroInd(:)
    real*8, allocatable  :: txyz(:,:), txyzout(:,:), ffgrid(:), xtmp(:,:), dist(:), frgrid(:), disttmp(:)
    real*8, allocatable  :: Ylm(:,:), ffgridtmp(:,:), avectmp(:,:), tmp(:,:), xtmp1(:), xtmp2(:), xtmp3(:)
    real*8, allocatable  :: ftgrid(:,:), post(:,:), grgridtmp(:,:)
    real*8, allocatable  :: rrdata2(:), frdata2(:)
    
    ! output variables
    type(SparseVataMN_2D_Real8) :: fout
    
    ! body of this function
    eps     = 2.2204D-16
    n       = size(gridcoord,1)
    nr      = size(rrdata)
    nf      = size(frdata)
    allocate(avectmp(3,3))
    allocate(tmp(1,3))
    avectmp = avec
    inde    = 0
    
    do ii = 1,nf,1
        if (abs(frdata(ii))>eps) then ! .and. abs(frdata(ii)) == 1) then
            inde = ii
        end if
    end do
    
    inde = inde + 1
    
    allocate(rrdata2(inde),frdata2(inde))

    forall(ii=1:inde)
            rrdata2(ii) = rrdata(ii)
            frdata2(ii) = frdata(ii)
    end forall

    if (inde<size(rrdata)) then
            frdata2(inde) = 0.0D0
    end if
    rmax = maxval(rrdata2)
    
    if (any(bc>0)) then
        issym = .FALSE.
        allocate(post(1,3))
        post = pos
        call cellInRange(avectmp,post,rmax,issym,txyz)
        
        nnn = size(txyzout,1)
        call flipud(txyz,txyzout)
        
        txyz = matmul(-txyzout,avec)
    else
        allocate(txyz(1,3))
        txyz = 0.0D0
    end if
    
    ntvec  = size(txyz,1)
    allocate(ftgrid(n,2*lqn+1))
    ftgrid = 0.0D0
    
    allocate(xtmp(n,3))
    allocate(dist(n))
    do ii = 1,ntvec,1
        forall(jj=1:3)
            tmp(1,jj)  = -pos(1,jj)-txyz(ii,jj)
        end forall
        
        call bsxfunPlus(gridcoord,tmp,xtmp)
        
        !forall(jj=1:n)
        !    dist(jj) = (xtmp(jj,1)**2.0D0+xtmp(jj,2)**2.0D0+xtmp(jj,3)**2.0D0)**0.5D0
        !end forall
        dist(:) = xtmp(:,1)**2+xtmp(:,2)**2+xtmp(:,3)**2
        dist    = dist**0.5D0
        
        !write(*,*) "dist = "
        !write(*,*)  dist
        if (minval(dist) <= rmax) then
            ntmp = 0
            do jj = 1,n,1
                if (dist(jj) <= rmax) then
                    ntmp = ntmp + 1
                end if
            end do
            allocate(NonzeroInd(ntmp))
            allocate(disttmp(ntmp))
            ntmp = 0
            do jj = 1,n,1
                if (dist(jj) <= rmax) then
                    ntmp             = ntmp + 1
                    disttmp(ntmp)    = dist(jj)
                    NonzeroInd(ntmp) = jj
                end if
            end do
            !write(*,*) "nonzeroInd ="
            !write(*,*)  NonzeroInd
            method  = "spline"
            allocate(frgrid(ntmp))
            
            call interp1(rrdata2,frdata2,disttmp,method,0,frgrid)
            deallocate(disttmp)

            !write(*,*) "frgrid ="
            !write(*,*)  frgrid

            if (lqn < 0) then
                !ntmp = 0
                do jj = 1,ntmp,1
                    !if (dist(jj) <= rmax) then
                    !    ntmp = ntmp+1
                    !    ftgrid(jj,1) = ftgrid(jj,1)+frgrid(ntmp)
                    !end if
                    ftgrid(NonzeroInd(jj),1) = ftgrid(NonzeroInd(jj),1)+frgrid(jj)
                end do
            else
                !if (lqn == 1) then
                !    allocate(Ylm(3,1))
                !else if (lqn == 1) then
                !    allocate(Ylm(3,3))
                !else if (lqn == 2) then
                !    allocate(Ylm(3,5))
                !else if (lqn == 3) then
                !    allocate(Ylm(3,7))
                !end if
                allocate(xtmp1(ntmp),xtmp2(ntmp),xtmp3(ntmp))
                forall(jj=1:ntmp)
                    xtmp1(jj) = xtmp(NonzeroInd(jj),1)
                    xtmp2(jj) = xtmp(NonzeroInd(jj),2)
                    xtmp3(jj) = xtmp(NonzeroInd(jj),3)
                end forall
                
                call GenRealSH(xtmp1,xtmp2,xtmp3,lqn,1,Ylm)
                allocate(grgridtmp(size(frgrid),1))
                grgridtmp(:,1) = frgrid
                call bsxfunTimes(grgridtmp,Ylm,Ylm)
                !write(*,*) "Ylm ="
                do jj = 1,ntmp,1
                    !ntmp = 0
                    !if (dist(jj) <= rmax) then
                    !        ntmp = ntmp + 1
                    !        ftgrid(jj,:) = ftgrid(jj,:) + Ylm(ntmp,:)
                    !end if
                    ftgrid(NonzeroInd(jj),:) = ftgrid(NonzeroInd(jj),:) + Ylm(jj,:)
                    !write(*,*) ftgrid(NonzeroInd(jj),:)
                end do
                !write(*,*) Ylm
                deallocate(Ylm)
            end if
            deallocate(frgrid)
        end if    
    end do
    ntmp = count(ftgrid /= 0.0D0)
    allocate(fout%Index1(ntmp))
    allocate(fout%Index2(ntmp))
    allocate(fout%ValueN(ntmp))
    fout%SIZE1     = n
    fout%SIZE2     = 2*lqn+1
    fout%N_nonzero = ntmp
    ntmp           = 0
    do jj = 1,2*lqn+1,1
        do ii = 1,n,1
            if (ftgrid(ii,jj) /= 0.0D0) then
                    ntmp              = ntmp+1
                    fout%Index1(ntmp) = ii
                    fout%Index2(ntmp) = ind(jj)
                    fout%ValueN(ntmp) = ftgrid(ii,jj)
            end if
        end do
    end do
    deallocate(txyz,ftgrid)
    !write(*,*) 'sopt in InterpSph2Cart_Sparse'
    !stop

    return
    end subroutine InterpSph2Cart_Sparse
    end module InterpSph2Cart_Sparse_module
        
