! InitFromRandParaK.f90

!*************************************
      !
      !
      !
!**************************************************

      module InitFromRandParaKMod
      contains
      subroutine InitFromRandParaK(FRC,time)

      use 

      implicit none

      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize
      time    = 0.0D0
      if (mpirank .EQ. 0) then
              write(*,*) 'Generating Plane-Wave subspace:'
      end if
      nvec    = FRC%domain%cgridn
      ispin   = FRC%spin%ispin
      nspin   = FRC%spin%nspin
      nkpt    = size(FRC%kpoint%ikdirect,1)
      allocate(kdir(nkpt,3))
      kdir    = FRC%kpoint%ikdirect
      nband   = FRC%eigensolver%nband
      cn      = product(nvec)
      vol     = 
      dr      = vol/dble(cn)
      allocate(nrg(nband,nkpt,nspin))
      nrg     = 0.0D0
      allocate(FRC%psi(nkpt,nspin))
      call UpdateVeff(FRC,FRC%potential%veffin(1)%vata,FRC%potential%vps,veff)
      call distmat_allgather(FRC,veff,FRC%potential%vloc)
      if (allocated(FRC%potential%vtau)) then
          call distmat_allgather(FRC,FRC%potential%vloc,FRC%potential%vtauloc)
      end if

      do ii = mpirank+1,nkpt*nspin,mpisize
          call sub2ind(nkpt,nspin,ii,kk,ss)
          kpt  = kdir(kk,:)
          call GetKBorb(FRC,kpt)
          if (ispin .EQ. 1 .OR. ispin .EQ. 2) then
              call system_clock()
              do jj = 1,
                  random_number(tt)
              end do
              do jj = 1,nband,1
              do kk = 1,2*cn,1
                  random_number(tt)
                  vs%vata(kk,jj) = tt
              end do
              end do
          else if (ispin .EQ. 4) then
              call system_clock()
              do  jj = 1,
                  random_number(tt)
              end do
              do jj = 1,nband,1
              do kk = 1,2*cn,1
                  random_number(tt)
                  vs(kk,jj) = tt
              end do
              end do
          end if
          if (trim(FRC%eigenSolver%init) .EQ. 'rr') then
              call GenHamfun()
              call ZGEMM()
              call ZGEEV()
              call ZGEMM()
          else
              do jj = 1,nband,1
                  call ZGEMM()
              end do
          end if
          if (ispin .EQ. 1 .OR. ispin .EQ. 2) then
                  FRC%psi(kk,ss)%vata = vs
          else
                  FRC%psi(kk,1)%vata = vs(1:cn,:)
                  FRC%psi(kk,2)%vata = vs(cn+1:2*cn,:)
          end if
      end do
      if (mpistat) then
              call MPI_Allreduce_sum(nrg)
      end if
      FRC%energy%ksnrg = nrg
      if (mpirank .EQ. 0) then
              write(*,*) 'time'
      end if

      return
      end subroutine InitFromRandParaK
      end module InitFromRandParaKMod
