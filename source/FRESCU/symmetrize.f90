! symmetrize.f90

      module symmetrize_module

              use symmetrize_1D_module
              use symmetrize_2D_module

              implicit none

              interface symmetrize
                      module procedure symmetrize_1D
                      module procedure symmetrize_2D
              end interface symmetrize

      end module symmetrize_module

