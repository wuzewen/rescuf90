! MPIBCAST.f90

!***********************************************************************************************
      !
      !
      !
!******************************************************************************

      module MPIBCASTMOD

      interface MPIBCAST
              module procedure MPI_Bcast_real8_1D
              module procedure MPI_Bcast_read8_2D
              module procedure MPI_Bcast_real8_3D
              module procedure MPI_Bcast_cmplx_1D
              module procedure MPI_Bcast_cmplx_2D
              module procedure MPI_Bcast_cmplx_3D
              module procedure MPI_Bcast_integ_1D
              module procedure MPI_Bcast_integ_2D
              module procedure MPI_Bcast_integ_3D
      end interface MPIBCAST

      end module MPIBCASTMOD
