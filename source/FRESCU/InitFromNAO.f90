! InitFromNAO.f90

!*******************************************************************
      !
      !
      !
!*******************************************************************

      module InitFromNAO_module
      contains
      subroutine InitFromNAO(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use ks_main_lcao_scf_module
      use GetAtomOrbitalSubspcae_module
      use calcev_lcao_module
      use reduceOper_module
      use VataMN_2D_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC

      ! temporary variables
      logical                      :: aosparse
      integer                      :: mpirank, ispin, nspin, nkpt
      integer                      :: kk, ss
      real*8                       :: kpt(3)
      real*8, allocatable          :: kdir(:,:), aov(:,:)
      type(VataMN_2D)              :: aosub
      type(VataMN_2D), allocatable :: aocell(:)

      ! output variables

      ! body of this function
      mpirank = FRC%mpi%rank
      ispin   = FRC%spin%ispin
      nspin   = FRC%spin%nspin
      FRC%LCAO%veffRed   = .TRUE.
      FRC%LCAO%dynSubRed = .TRUE.
      if (mpirank .EQ. 0) then
              write(*,*) 'Calculating atomic orbital coefficients'
      end if
      call ks_main_lcao_scf(FRC)
      if (mpirank .EQ. 0) then
              write(*,*) 'Time used.'
      end if

      call GetAtomOrbitalSubspcae(FRC)

      nkpt  = size(FRC%kpoint%ikdirect,1)
      allocate(kdir(size(nkpt,3)))
      kdir  = FRC%kpoint%ikdirect
      !nkpt  = size(kdir,1)
      
      !call ks_main_lcao_scf(FRC)

      if (FRC%LCAO%dynSubRed) then
              !if (product(FRC%domain%cgridn) /= FRC%LCAO%aocell(1)%m) then
              !        call GetAtomicOrbitalSubspace(FRC,FRC%domain%cgridn,.FALSE.)
              !end if
              aocell   = FRC%LCAO%aocell
              aov      = FRC%LCAO%aovec
              aosparse = FRC%LCAO%aosparse
      else
              !if (product(FRC%domain%cgridn) /= FRC%LCAO%aocell(1)%m) then
              !        call GetAtomicOrbitalSubspace(FRC,FRC%domain%cgridn,.FALSE.)
              !end if
              aocell   = FRC%LCAO%aokcell
      end if

      do kk = 1,nkpt,1
          kpt = kdir(kk,:)
          if (FRC%LCAO%dynSubRed) then
                  call reduceOper(aocell,aov,kpt,aosparse,aosub)
          else
                  aosub = aocell(kk)
          end if
          do ss = 1,nspin,1
              if (ispin .EQ. 1 .or. ispin .EQ. 2) then
                      call calcev_lcao(FRC,aosub,FRC%LCAO%coeff(kk,ss),kpt,FRC%psi(kk,ss))
              else if (ispin == 4) then
                      call calcev_lcao(FRC,aosub,FRC%LCAO%coeff(kk,1),kpt,FRC%psi(kk,1))
                      call calcev_lcao(FRC,aosub,FRC%LCAO%coeff(kk,2),kpt,FRC%psi(kk,2))
              end if
          end do
      end do

      if (FRC%functional%includeU) then
              allocate(flist(3))
              flist(1)%vata = 'XSX'
              flist(2)%vata = 'XHX'
              flist(3)%vata = 'XVX'
      else
              allocate(flist(7))
              flist(1)%vata = 'XSX'
              flist(2)%vata = 'XHX'
              flist(3)%vata = 'XVX'
              flist(4)%vata = 'aokcell'
              flist(5)%vata = 'aocell'
              flist(6)%vata = 'aovec'
              flist(7)%vata = 'aosparse'
      end if

      return
      end subroutine InitFromNAO
      end module InitFromNAO_module
