! inversion.f90
!************************************************
    ! Matrix's inversion: B = A**-1
!************************************************
    
    
    module uptri_module
    contains
    subroutine uptri(A,b,X,n)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), b(:), x(:)
    integer :: n, i, j
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    
    x(n) = b(n)/A(n,n)
    do i = n-1,1,-1
        x(i) = b(i)
        do j = i+1,N,1
            x(i) = x(i) -a(i,j)*x(j)
        end do
        x(i) = x(i)/a(i,i)
    end do
    
    return
    end subroutine uptri
    end module uptri_module
    
    
    
    module elgauss_module
    contains
    subroutine elgauss(A,b,x,N)
    
    use uptri_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), b(:), x(:)
    integer             :: N, j, k, i
    
    ! temporary variables
    integer :: id_max
    real*8, allocatable :: Aup(:,:), Bup(:)
    real*8, allocatable :: AB(:,:), vtemp1(:), vtemp2(:)
    real*8              :: elmax, temp
    
    ! output ariables
    
    ! body of this function
    allocate(Aup(N,N),Bup(N),AB(N,N+1),vtemp1(N+1),vtemp2(N+1))
    
    AB(1:N,1:N) = A
    forall(j=1:N)
        AB(j,N+1)   = b(j)
    end forall
    
    
    do k = 1,N-1,1
        elmax  = dabs(AB(k,k))
        id_max = k
        do i = k+1,n,1
            if (dabs(AB(i,k))>elmax) then
                elmax  = AB(i,k)
                id_max = i
            end if
        end do
        vtemp1  = AB(k,:)
        vtemp2  = AB(id_max,:)
        AB(k,:) = vtemp2
        AB(id_max,:) = vtemp1
        
        do i=k+1,N,1
            temp = AB(i,k)/AB(k,k)
            AB(i,:) = AB(i,:) - temp*AB(k,:)
        end do
    end do
    
    Aup(:,:) = AB(1:N,1:N)
    Bup(:)   = AB(:,N+1)
    call uptri(Aup,Bup,x,n)
    
    return
    end subroutine elgauss
    end module elgauss_module

    
    
    
    module matreq_module
    contains
    subroutine matreq(A,B,X,n,m)
    
    use elgauss_module
    
    implicit none
    
    ! input variables
    integer             :: m, n, i, j
    real*8, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    real*8, allocatable :: Btemp(:), Xtemp(:)
    
    ! output variables
    real*8, allocatable :: X(:,:)
    
    ! body of this function
    
    allocate(Btemp(n),Xtemp(n))
    do i = 1,m,1
        forall(j=1:m)
            Btemp(j) = B(j,i)
        end forall
        
        call elgauss(A,Btemp,Xtemp,n)
        
        forall(j=1:m)
            X(j,i) = Xtemp(j)
        end forall
        
    end do

    return
    end subroutine matreq
    end module matreq_module
    
    
    
    
    
    module inversion_module
    contains
    subroutine inversion(A,invA)
    
    use matreq_module
    !use matreq_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    
    ! temporery variables
    integer             :: m, n, i
    real*8, allocatable :: E(:,:)
    
    ! output variables
    real*8, allocatable :: invA(:,:)
    
    ! body of this function
    
    m = size(A,1)
    n = size(A,2)
    
    allocate(E(n,n))
    E = 0.0D0
    
    forall(i=1:n)
        E(i,i) = 1.0D0
    end forall
    
    call matreq(A,E,invA,n,n)
    
    return
    end subroutine inversion
    end module inversion_module
    
    
    
    
