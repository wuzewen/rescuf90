! InitDistArray.f90

      module InitDistArray_module

              use InitDistArray_1D_real8_module
              use InitDistArray_1D_cmplx_module
              use InitDistArray_2D_real8_module
              use InitDistArray_2D_cmplx_module

              implicit none

              interface InitDistArray
                      module procedure InitDistArray_1D_real8
                      module procedure InitDistArray_1D_cmplx
                      module procedure InitDistArray_2D_real8
                      module procedure InitDistArray_2D_cmplx
              end interface InitDistArray

      end module InitDistArray_module
