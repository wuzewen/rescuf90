! MEX_BCAST_1D.f90

!***********************************************************************
      !
      !
      !
!***********************************************************************

      module MEX_BCAST_1D_module
      contains
      subroutine MEX_BCAST_1D(buf,coun,root,rankn,ranks)

      include 'mpif.h'

      ! input variables
      integer :: coun, root, rankn
      integer, allocatable :: ranks(:)
      real*8 , allocatable :: buf(:)

      ! temporary variables
      integer :: world_size, ierr, world_group, prime_group, prime_comm

      ! output variables

      ! body of this function
      call MPI_COMM_SIZE(MPI_COMM_WORLD,world_size,ierr)

      if (rankn == world_size) then
              call MPI_BCAST(buf,coun,MPI_DOUBLE,root,MPI_COMM_WORLD,ierr)
      else
              call MPI_COMM_GROUP(MPI_COMM_WORLD,world_group,ierr)
              call MPI_GROUP_INCL(world_group,rankn,ranks,prime_group)
              call MPI_COMM_CREATE_GROUP(MPI_COMM_WORLD,prime_group,0,prime_comm,ierr)
              call MPI_BCAST(buf,coun,MPI_DOUBLE,root,prime_comm,ierr)
              call MPI_GROUP_FREE(world_group,ierr)
              call MPI_GROUP_FREE(prime_group,ierr)
              call MPI_COMM_FREE(prime_comm)
      end if

      return
      end subroutine MEX_BCAST_1D
      end module MEX_BCAST_1D_module
