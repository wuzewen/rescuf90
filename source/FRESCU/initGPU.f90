! initGPU.f90
!************************
    !
!************************
    
    module initGPU_module
    contains
    subroutine initGPU(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType)              :: inputFromFile
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    FRC%gpu%status = .FALSE.
    FRC%init%GPU   = .TRUE.
    
    return
    end subroutine initGPU
    end module initGPU_module