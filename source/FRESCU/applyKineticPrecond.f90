! applyKineticPrecond.f90

!*****************************************************
      !
      !
      !
!*****************************************************

      module applyKineticPrecond_module
      contains
      subroutine applyKineticPrecond(FRC,prekin,vin,vout)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use  fft3_fftw_module
      use ifft3_fftw_module
      use bsxfunTimes_CMPLX_3D_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      real*8    , allocatable :: prekin(:,:,:)
      complex*16, allocatable :: vin(:,:)

      ! temporary variables
      integer                 :: m, n_loc, nvec(3)
      !complex*16, allocatable :: vtmp1(:,:,:), vtmp2(:,:,:)
      complex*16, allocatable :: cprekin(:,:,:)

      ! output variables
      complex*16, allocatable :: vout(:,:)

      ! body of this function
      m       = size(vin,1)
      n_loc   = size(vin,2)
      nvec(1) = size(prekin,1)
      nvec(2) = size(prekin,2)
      nvec(3) = size(prekin,3)
      if (n_loc > 0) then
              !allocate(vtmp1(nvec(1),nvec(2),nvec(3)))
              !allocate(vtmp2(nvec(1),nvec(2),nvec(3)))
              allocate(cprekin(size(prekin,1),size(prekin,2),size(prekin,3)))
              !vtmp1 = reshape(vin,(/nvec(1),nvec(2),nvec(3)/))
              !call fft3_fftw(nvec(1),nvec(2),nvec(3),vtmp1)
              FRC%Arr = reshape(vin,(/nvec(1),nvec(2),nvec(3)/))
              call fft3_fftw(FRC)
              !call fft3_fftw(FRC,vtmp1)
              cprekin = dcmplx(prekin)
              call bsxfunTimes_CMPLX_3D(FRC%Arr,cprekin,FRC%Arr)
              !call bsxfunTimes_CMPLX_3D(vtmp1,cprekin,vtmp2)
              !call ifft3_fftw(nvec(1),nvec(2),nvec(3),vtmp2)
              !call ifft3_fftw(FRC,vtmp2)
              call ifft3_fftw(FRC)
              vout = reshape(FRC%Arr,(/m,n_loc/))
              !vout = reshape(vtmp2,(/m,n_loc/))
      else
              vout = vin
      end if

      return
      end subroutine applyKineticPrecond
      end module applyKineticPrecond_module


