! SparseArray_2D_Real8.f90

!***********************************************************************
      !
      !
      !
!***********************************************************************

      module SparseArray_2D_Real8_module

              type :: SparseArray_2D_Real8
                      integer :: SIZE1, SIZE2
                      integer :: N_nonzero
                      integer, allocatable :: Index1(:)
                      integer, allocatable :: Index2(:)
                      real*8 , allocatable :: ValueN(:)
              end type SparseArray_2D_Real8

      end module SparseArray_2D_Real8_module
