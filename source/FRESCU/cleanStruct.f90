! cleanStruct.f90

      module cleanStruct_module
      contains
      subroutine cleanStruct(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC

      integer :: iter

      iter = FRC%scloop
      if (iter >= 4) then
          deallocate(FRC%potential%VHout(iter-2)%vata)
          deallocate(FRC%potential%VXCout(iter-2)%vata)
          deallocate(FRC%potential%veffout(iter-2)%vata)
          !deallocate(FRC%potential%veffin(iter-1)%vata)
          !if (iter >= 3) then
              deallocate(FRC%potential%veffin(iter-2)%vata)
              deallocate(FRC%rho%input(iter-2)%vata)
          !end if
          deallocate(FRC%rho%output(iter-2)%vata)
          deallocate(FRC%energy%XCdens(iter-2)%vata)
      end if

      return
      end subroutine cleanStruct
      end module cleanStruct_module
