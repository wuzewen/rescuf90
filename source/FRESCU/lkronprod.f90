! rkronprod.f90
    
!*****************************************************************
    !
    !
    !
!*****************************************************************
    
    module lkronprod_module
    contains
    subroutine lkronprod(A,B,KA)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    integer :: p, q, n, m
    real*8, allocatable :: TA(:,:), RA(:,:), SA(:,:), TB(:,:), RB(:,:)
    
    ! output variables
    real*8, allocatable :: KA(:,:)
    
    ! body of this function
    p = size(A,1)
    q = size(A,2)
    n = size(B,1)/q
    m = size(B,2)
    
    TA = transpose(A)
    TB = transpose(B)
    RB = reshape(TB,(/n*m,q/))
    RA = matmul(RB,TA)
    SA = reshape(RA,(/m,p*n/))
    KA = transpose(SA)
    
    return
    end subroutine lkronprod
    end module lkronprod_module