!  GetNeutralAtomDensity.f90 
!****************************************************************************
!
!  PROGRAM: GetNeutralAtomDensity
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module GetNeutralAtomDensity_module
    contains
    subroutine GetNeutralAtomDensity(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use RHO_type
    use RHODM_type
    use rnaParaAtom_module
    use rnaParaSpace_module
    use VataMN_2D_module
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    integer         :: ispin
    type(VataMN_2D) :: Rho
    type(VataMN_2D) :: rhodm
    
    
    ! output variables
    

    ! Body of GetNeutralAtomDensity
    ispin = FRC%spin%ispin
    FRC%option%initParaReal = .TRUE.
    !write(*,*) "FRC%option%initParaReal =", FRC%option%initParaReal
    if (FRC%option%initParaReal) then
        !write(*,*) "Getting Neutral Atom Density with ParaReal."
        !write(*,*) "Error in GetNeutralAtomDensity.f90. paralell is not avaliable now."
        !stop
        !write(*,*) "using rnaParaSpace."
        call rnaParaSpace(FRC,rho,rhodm)
    else
        call rnaParaAtom(FRC,rho,rhodm)
    end if
    
    !allocate(FRC%rho%atom%vata(size(rho%vata,1),size(rho%vata,2)))
    FRC%rho%atom     = Rho
    
    !if (ispin == 1) then
    !allocate(FRC%rho%input(FRC%option%maxSCfIteration))
    !allocate(FRC%rho%output(FRC%option%maxSCfIteration))
    allocate(FRC%rho%input(1)%vata(size(rhodm%vata,1),size(rhodm%vata,2)))
    FRC%rho%input(1) = rhodm
    
    !else if (ispin == 2) then
    !    FRC%rho%input(1) = rhodm
    !else if (ispin == 4) then
    !    FRC%rho%input(1) = rhodm
    !end if
    deallocate(Rho%vata,rhodm%vata)
    
    return
    end subroutine GetNeutralAtomDensity
    end module GetNeutralAtomDensity_module

