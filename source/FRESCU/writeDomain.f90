! writeDomain.f90

      module writeDomain_module
      contains
      subroutine writeDomain(FRC,filnam)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam

      open(unit=10,file=filnam)

      write(10,*) "Lattice Vectors                = "
      write(10,*)  FRC%domain%latvec(1,:)
      write(10,*)  FRC%domain%latvec(2,:)
      write(10,*)  FRC%domain%latvec(3,:) 
      write(10,*) "Boundary Conditions            = "
      write(10,*)  FRC%domain%boundary
      write(10,*) "Low Resolution                 = "
      write(10,*)  FRC%domain%lowres
      write(10,*) "High Resolution                = "
      write(10,*)  FRC%domain%highres
      write(10,*) "Real Space Grids Number (Low)  = "
      write(10,*)  FRC%domain%cgridn
      write(10,*) "Real Space Grids Number (Hogh) = "
      write(10,*)  FRC%domain%fgridn
      close(10)
      return
      end subroutine writeDomain
      end module writeDomain_module
