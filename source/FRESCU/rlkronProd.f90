! rkronprod.f90
    
!*****************************************************************
    !
    !
    !
!*****************************************************************
    
    module rlkronprod_module
    contains
    subroutine rlkronprod(A,B,nx,KA)
    
    use lkronprod_module
    
    implicit none
    
    ! input variables
    integer             :: nx
    real*8, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    integer :: p, q, n, m
    real*8, allocatable :: T(:,:), Atmp(:,:)
    
    ! output variables
    real*8, allocatable :: KA(:,:)
    
    ! body of this function
    p = nx*size(A,1)
    q = Nx*size(A,2)
    n = size(B,1)/q
    m = size(B,2)
    
    allocate(T(q,m*n))
    T = reshape(B,(/q,m*n/))
    call lkronprod(A,T,Atmp)
    KA = reshape(Atmp,(/p*n,m/))
    
    return
    end subroutine rlkronprod
    end module rlkronprod_module