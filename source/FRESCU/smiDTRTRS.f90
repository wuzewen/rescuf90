! smiDTRTRS.f90

!**************************************************
      !
      !
      !
!**************************************************

      module smiDTRTRSMOD
      contains
      subroutine smiDTRTRS(UPLO,trans,N,NRHS,A,B,MB,NB,nprow,npcol)

      implicit none

      character(len=1) :: UPLO, trans
      integer :: N, NRHS, MB, NB, nprow, npcol
      real*8, allocatable :: A(), B()

      character(len=1) :: UPLO, DIAG
      integer :: iam, ictxt, myrow, mycol

      DIAG  = 'N'
      iam   =  0
      ictxt =  0
      myrow =  0
      mycol =  0
      nprocs = nprow*npcol

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)
      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,B_np)
      A_np = 
      B_np = 
      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descB,N,N,MB,NB,izero,izero,ictxt,B_np,descinfo)
      call PDTRTRS(UPLO,trans,DIAG,N,NRHS,A,ione,ione,descA,& 
                  B,ione,ione,descB,info)
      if (INFO .LT. 0) then
              write(*,*) 'Error'
      else if (INFO .GT. 0) then
              write(*,*) 'Error'
      end if
      call blacs_gridexit(ictxt)

      return
      end subroutine smiDTRTRI
      end module smiDTRTRIMOD
