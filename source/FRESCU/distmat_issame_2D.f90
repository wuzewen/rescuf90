! distmat_issame_2D.f90
    
!***************************************************
    !
    !
    !
!***************************************************
    
    module distmat_issame_2D_module
    contains
    subroutine distmat_issame_2D(dA,dB,issame)
    
    use VataMN_2D_module
    
    implicit none
    
    ! input variables
    type(VataMN_2D) :: dA, dB
    
    ! temporary variables
    
    ! output variables
    logical      :: issame
    
    ! body of this function
    issame = .FALSE.
    if (dA%m .EQ. dB%m .and. dA%n .EQ. dB%n .and. & 
            dA%mblock .EQ. dB%mblock .and. dA%nblock .EQ. dB%nblock .and. &
            dA%mproc  .EQ. dB%mproc  .and. dA%nproc  .EQ. dB%nproc) then
        issame = .TRUE.
    end if
    
    return
    end subroutine distmat_issame_2D
    end module distmat_issame_2D_module
        
