!  GetInitialSubspace.f90 
!****************************************************************************
!
!  PROGRAM: GetInitialSubspace
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module GetInitialSubspace_module
    contains
    subroutine GetInitialSubspace(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use initFromSin_module
    !use InitFromNAO_module

    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    integer :: mpisize, mpirank
    
    ! output variables
    

    ! Body of GetInitialSubspace
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    
    if (trim(FRC%eigenSolver%init) == trim('rand') .and. FRC%mpi%paraK) then
            write(*,*) "Error in GetInitialSubspace. rand is not."
    !    call InitFromRandParaK(FRC)
    else if (trim(FRC%eigenSolver%init) == trim('LCAO')) then
        !call InitFromNAO(FRC)
        !if (FRC%mpi%paraK) then
        !        write(*,*) "Error in GetInitialSubspace. Parallel is not."
        !    !call distWaveFunctionKpoint(FRC)
        !end if
    else if (trim(FRC%eigensolver%init) == trim("sin/cos")) then
        !    write(*,*) "Error in GetInitialSubspace. SIN is not."
        !write(*,*) "I'm here before initFromSin in GetInitialSubspace."
        call InitFromSin(FRC)
        !write(*,*) "I'm here after initFromSin in GetInitialSubspace."
    !else if (psiStatus) then         ! unfinished
    !    if (psiIsChar) then
    !        nkpt = size(KpointIKDrection,1)
    !        call fileParts()
    !    end if
    else
        write(*,*) 'Error in GetInitialSubspace. No initial subspace.'
    end if
    

    return
    end subroutine GetInitialSubspace
    end module

