! sph2realMat.f90

!********************************************************************************
      !
      !
      !
!********************************************************************************

      module sph2realMatMod
      contains
      subroutine sph2realMat(Morb,U)

      implicit none

      n = size(Morb)
      allocate(U(n,n))
      U = 0.0D0
      forall(ii=1:n)
              U(ii,ii) = U(ii,ii) + 1.0D0
      end forall

      mtmp = 0
      ntmp = 0
      do ii = 1,size(Morb),1
          if (Morb(ii) .EQ. -1) then
                  mtmp = mtmp + 1
          else if (Morb(ii) .EQ. 1) then
                  ntmp = ntmp + 1
          end if
      end do
      allocate(ind1(mtmp),ind2(ntmp))
      mtmp = 0
      ntmp = 0
      do ii = 1,size(Morb),1
          if (Morb(ii) .EQ. -1) then
                  mtmp       = mtmp + 1
                  ind1(mtmp) = ii
          else if (Morb(ii) .EQ. 1) then
                  ntmp       = ntmp + 1
                  ind2(ntmp) = ii
          end if
      end do

      n1 = sum(ind1)
      n2 = sum(ind2)

      forall(ii=1:mtmp,jj=1:ntmp)
              U(ind1(ii),ind1(ii)) = im/sqrt(2)
              U(ind2(jj),ind1(ii)) = im/sqrt(2)
              U(ind1(ii),ind2(jj)) = 1.0D0/sqrt(2)
              U(ind2(jj),ind2(jj)) = -1.0D0/sqrt(2)
      end forall

      deallocate(ind1,ind2)

      mtmp = 0
      ntmp = 0
      do ii = 1,size(Morb),1
          if (Morb(ii) .EQ. -2) then
                  mtmp = mtmp + 1
          else if (Morb(ii) .EQ. 2) then
                  ntmp = ntmp + 1
          end if  
      end do
      allocate(ind1(mtmp),ind2(ntmp))
      mtmp = 0
      ntmp = 0
      do ii = 1,size(Morb),1
          if (Morb(ii) .EQ. -1) then
                  mtmp       = mtmp + 1
                  ind1(mtmp) = ii
          else if (Morb(ii) .EQ. 1) then
                  ntmp       = ntmp + 1
                  ind2(ntmp) = ii
          end if  
      end do
      n1 = sum(ind1)
      n2 = sum(ind2)

      forall(ii=1:mtmp,jj=1:ntmp)
              U(ind1(ii),ind1(ii)) = im/sqrt(2)
              U(ind2(jj),ind1(ii)) = -im/sqrt(2)
              U(ind1(ii),ind2(jj)) = 1.0D0/sqrt(2)
              U(ind2(jj),ind2(jj)) = 1.0D0/sqrt(2)
      end forall

      deallocate(ind1,ind2)

      mtmp = 0
      ntmp = 0
      do ii = 1,size(Morb),1
          if (Morb(ii) .EQ. -3) then
                  mtmp = mtmp + 1
          else if (Morb(ii) .EQ. 3) then
                  ntmp = ntmp + 1
          end if
      end do
      allocate(ind1(mtmp),ind2(ntmp))
      mtmp = 0
      ntmp = 0
      do ii = 1,size(Morb),1
          if (Morb(ii) .EQ. -1) then
                  mtmp       = mtmp + 1
                  ind1(mtmp) = ii
          else if (Morb(ii) .EQ. 1) then
                  ntmp       = ntmp + 1
                  ind2(ntmp) = ii
          end if
      end do
      n1 = sum(ind1)
      n1 = sum(ind1)
      forall(ii=1:mtmp,jj=1:ntmp)
              U(ind1(ii),ind1(ii)) = im/sqrt(2)
              U(ind2(jj),ind1(ii)) = im/sqrt(2)
              U(ind1(ii),ind2(jj)) = 1.0D0/sqrt(2)
              U(ind2(jj),ind2(jj)) = -1.0D0/sqrt(2)
      end forall
      deallocate(ind1,ind2)

      return
      end subroutine sph2realMat
      end module sph2realMatMod
