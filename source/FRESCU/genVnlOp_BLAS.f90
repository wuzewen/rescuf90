! genVnlOp_BLAS.f90
    
!*******************************************************************
    !
    !
    !
!*******************************************************************
    
    module genVnlOp_BLAS_module
    contains
    subroutine genVnlOp_BLAS(FRC,gpu,X,vnl)
    
    !external ZGEMM
    use FORTRAN_RESCU_CALCULATION_TYPE
    use bsxfunTimes_CMPLX_module
    !use SparseProduct_module

    !external ZGEMM

    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    logical :: gpu
    complex*16, allocatable :: X(:,:)
    
    ! temporary variables
    !integer :: ii
    logical :: conj  
    real*8  :: avec(3,3), dr
    complex*16, allocatable :: kbedr(:,:)
    complex*16, allocatable :: chilm(:,:)
    character(len=1) :: trans
    integer          :: m, n, k, lda, ldb, ldc, ii
    complex*16           :: alpha, beta
    !complex*16, allocatable :: chilmtmp2(:,:), vnl2(:,:)
    ! output variabels
    complex*16, allocatable :: vnl(:,:)
    
    ! body of this function
    !call det(FRC%domain%latvec,dr)
    avec = FRC%domain%latvec
    dr   = avec(1,1)*avec(2,2)*avec(3,3)&
          +avec(1,2)*avec(2,3)*avec(3,1)&
          +avec(1,3)*avec(2,1)*avec(3,2)&
          -avec(1,3)*avec(2,2)*avec(3,1)&
          -avec(1,1)*avec(2,3)*avec(3,2)&
          -avec(1,2)*avec(2,1)*avec(3,3)
    if (FRC%interpolation%vnl) then
        dr = dr/dble(product(FRC%domain%fgridn))
    else
        dr = dr/dble(product(FRC%domain%cgridn))
    end if
    !allocate(kbedr(size(FRC%potential%vnl%KBEnergy),1))
    !kbedr(:,1) = dcmplx(FRC%potential%vnl%KBEnergy(:)*dr)
    
    if (gpu) then
            write(*,*) "Error in genVnlOp_Sparse. GPU problem."
        !call gpuArray(kbedr)
        !call gpuArray(chilm)
        !chilmtmp = matmul(transpose(chilm),X)
        !call bsxfunTimes(chilmtmp,kbedr,vnl)
        !vnl = matmul(chilm,vnl)
        !if (FRC%interpolation%vnl) then
            !call initFun(vnl,X,vnltmp)
            !call antFun(vnltmp,X,vnl)
        ! end if
    else
        trans     = 'C'
        m         =  FRC%potential%vnl%sparseKBorb%SIZE1
        n         =  size(X,2)
        k         =  FRC%potential%vnl%sparseKBorb%SIZE2
        alpha     =  (1.0D0,0.0D0)
        beta      =  (0.0D0,0.0D0)
        ldb       =  size(X,1)
        ldc       =  k
        allocate(chilm(k,n))
        !matdescra = 'GLNF'
        chilm     = (0.0D0,0.0D0)
        !write(*,*) "size of kborb index"
        !write(*,*)  size(FRC%potential%vnl%sparseKBorb%Index1),size(FRC%potential%vnl%sparseKBorb%Index2)
        !write(*,*) "size of X", size(X,1), size(X,2)
        !write(*,*) "size of A", m, k
        !write(*,*) "m,n,k",m,n,k,size(FRC%potential%vnl%sparseKBorb%ValueN),FRC%potential%vnl%sparseKBorb%N_nonzero
        !write(*,*) "X",size(X,1),size(X,2)
        !write(*,*) "Index1",FRC%potential%vnl%sparseKBorb%Index1

        !write(*,*) maxval(FRC%potential%vnl%sparseKBorb%Index1),maxval(FRC%potential%vnl%sparseKBorb%Index2)
        !do ii = 1,FRC%potential%vnl%sparseKBorb%N_nonzero,1
        !write(*,*) FRC%potential%vnl%sparseKBorb%Index1(ii),FRC%potential%vnl%sparseKBorb%Index2(ii),FRC%potential%vnl%sparseKBorb%ValueN(ii)
        !end do
        call mkl_zcoomm(trans,m,n,k,alpha,'glnf',& 
                        FRC%potential%vnl%sparseKBorb%ValueN,&
                        FRC%potential%vnl%sparseKBorb%Index1,&
                        FRC%potential%vnl%sparseKBorb%Index2,&
                        FRC%potential%vnl%sparseKBorb%N_nonzero,&
                        X,ldb,beta,chilm,ldc)
        !call SparseProduct(FRC%potential%vnl%sparseKBorb,X,conj,chilm)
        !write(*,*) "X ="
        !write(*,*)  X
        !write(*,*) "size of chilm", size(chilm,1), size(chilm,2)
        !write(*,*) "chilm ="
        !write(*,*)  chilm
        !do m = 1,size(chilm,2),1
        !    do n = 1,size(chilm,1),1
        !        write(*,*) n, m, chilm(n,m)
        !    end do
        !end do 
        !write(*,*) "sizes of before bsxfunTimes"
        !write(*,*)  size(chilm,1), size(chilm,2),size(kbedr,1),size(kbedr,2)
        !call bsxfunTimes_CMPLX(chilm,kbedr,chilm)
        do ii = 1,size(chilm,2),1
            chilm(:,ii) = chilm(:,ii)*FRC%potential%vnl%KBEnergy*dr
        end do
        !write(*,*) "after bsx"
        trans     = 'N'
        m         =  FRC%potential%vnl%sparseKBorb%SIZE1
        n         =  size(chilm,2)
        k         =  FRC%potential%vnl%sparseKBorb%SIZE2
        alpha     =  (1.0D0,0.0D0)
        beta      =  (0.0D0,0.0D0)
        ldb       =  size(chilm,1)
        ldc       =  m
        allocate(Vnl(m,n))
        call mkl_zcoomm(trans,m,n,k,alpha,'GLNF',&
                        FRC%potential%vnl%sparseKBorb%ValueN,&
                        FRC%potential%vnl%sparseKBorb%Index1,&
                        FRC%potential%vnl%sparseKBorb%Index2,&
                        FRC%potential%vnl%sparseKBorb%N_nonzero,&
                        chilm,ldb,beta,Vnl,ldc)
        !conj = .FALSE.
        !call SparseProduct(FRC%potential%vnl%sparseKBorb,chilm,conj,Vnl)
        !write(*,*) "size of Vnl", size(Vnl,1), size(Vnl,2)
        !write(*,*) "Vnl ="
        !do m = 1,size(vnl,2),1
        !do n = 1,size(vnl,1),1
        !write(*,*) n, m, vnl(n,m)
        !end do
        !end do
        !stop
        if (FRC%interpolation%vnl) then
            write(*,*) "Error in genVnlOp.f90. FRC%interpolation%vnl should be false now."
            stop
            !call initFun(vnl,X,vnltmp)
            !call antFun(vnltmp,X,vnl)
        end if
    end if
    
    return
    end subroutine genVnlOp_BLAS
    end module genVnlOp_BLAS_module
