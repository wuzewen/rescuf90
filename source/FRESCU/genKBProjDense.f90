! genKBProjDense.f90

!**************************************************************************
      !
      ! This function is used to get Nonlocal potential projectors 
      ! which is saved as sparse matrix. 
      !
      ! FRC%potential%vnl%kbDcell :: None-zero elements of Nonlocal
      ! potential projectors
      ! FRC%potential%vnl%g1Ind   :: Index on 1st dimention
      ! FRC%potential%vnl%g2Ind   :: Index on 2nd dimention
      ! FRC%potential%vnl%g3Ind   :: Index on 3rd dimention
      !
!**************************************************************************

      module genKBProjDense_module
      contains
      subroutine genKBProjDense(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use RealVata_1D_module
      use IntegerVata_1D_module
      use GetKBOrbitalInfo_module
      use interpRad2prlpd_module
      use unionCell_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC

      ! temporary variables
      integer :: gridn(3), natom, nevec
      integer :: ii, jj, kk, pp, itmp, nindtmp
      integer :: elem, ntmp, lcut, ncut, mtmp, fvn, ll
      integer :: LenVata, nn1, nn2, nn3, nn4
      real*8  :: avec(3,3), pos(3)

      integer, allocatable :: elev(:), lnu(:), nnu(:), inde(:)
      integer, allocatable :: fvL(:), g1Atom(:), g2Atom(:), g3Atom(:)
      integer, allocatable :: g1ind(:), g2ind(:), g3ind(:)
      real*8 , allocatable :: rrdata(:), vvdata(:)
      real*8 , allocatable :: fvAtom(:,:,:,:), fvAtomTmp(:,:,:,:)
      real*8 , allocatable :: KBenergy(:), evec(:), oorb(:)

      type(RealVata_2D)   , allocatable :: fvalTmp(:)
      type(IntegerVata_1D), allocatable :: g1Tmp(:), g2Tmp(:), g3Tmp(:)

      ! output variables

      ! body of this function
      call GetKBOrbitalInfo(FRC)

      gridn = FRC%domain%fgridn
      avec  = FRC%domain%latvec
      !xyz   = FRC%atom%XYZ
      natom = FRC%atom%NumberOfAtom
      allocate(elev(natom))
      elev  = FRC%atom%element
      nevec = size(FRC%potential%vnl%evec)
      allocate(KBenergy(nevec))
      allocate(evec(nevec))
      allocate(oorb(nevec))
      oorb  = FRC%potential%vnl%Oorb
      evec  = FRC%potential%vnl%evec
      allocate(FRC%potential%vnl%kbDcell(natom))
      allocate(FRC%potential%vnl%g1Ind(natom))
      allocate(FRC%potential%vnl%g2Ind(natom))
      allocate(FRC%potential%vnl%g3Ind(natom))
      !allocate(kbDcell(natom,1))
      KBenergy = 0.0D0

      do ii = 1,natom,1
          pos  = FRC%atom%XYZ(ii,:)
          elem = elev(ii)
          ntmp = size(FRC%element(elem)%vnllnu)
          allocate(lnu(size(FRC%element(elem)%vnllnu)))
          allocate(nnu(size(FRC%element(elem)%vnlnnu)))
          lnu  = FRC%element(elem)%vnllnu
          lcut = FRC%element(elem)%vnlLcutoff
          nnu  = FRC%element(elem)%vnlnnu
          ncut = FRC%element(elem)%vnlNcutoff
          mtmp = 0
          do jj = 1,ntmp,1
              if ((lnu(jj) <= lcut) .and. (nnu(jj) <= ncut)) then
                      mtmp = mtmp+1
              end if
          end do
          allocate(inde(mtmp))
          mtmp = 0
          do jj = 1,ntmp,1
              if ((lnu(jj) <= lcut) .and. (nnu(jj) <= ncut)) then
                      mtmp       = mtmp+1
                      inde(mtmp) = jj
              end if
          end do

          do jj = 1,mtmp,1
              itmp = inde(jj)
              nindtmp = 0
              do pp = 1,nevec,1
                  if (evec(pp) == ii .and. oorb(pp) == itmp) then
                          nindtmp = nindtmp+1
                          KBenergy(pp) = FRC%ElementData(elem)%Vnl(itmp)%KBenergy
                  end if
              end do
          end do

          !write(*,*) "xyz ="
          !write(*,*)  xyz
          !write(*,*) "pos ="
          !write(*,*)  pos
          !write(*,*) "elem ="
          !write(*,*)  elem

          !write(*,*) "ntmp ="
          !write(*,*)  ntmp
          !write(*,*) "inde ="
          !write(*,*)  inde
          !vata = FRC%ElementData(elem)%vnl
          LenVata = size(FRC%ElementData(elem)%vnl)
          fvN  = 0
          allocate(fvL(LenVata))
          allocate(fvalTmp(LenVata))
          allocate(g1Tmp(LenVata))
          allocate(g2Tmp(LenVata))
          allocate(g3Tmp(LenVata))
          do jj = 1,LenVata,1
              ll = FRC%ElementData(elem)%vnl(jj)%L
              fvN = fvN+2*ll+1
              fvL(jj) = ll
              !write(*,*) "fvN ="
              !write(*,*)  fvN
              !write(*,*) "fvL ="
              !write(*,*)  fvL
              allocate(rrdata(size(FRC%ElementData(elem)%vnl(jj)%rrdata)))
              rrdata = FRC%ElementData(elem)%vnl(jj)%rrdata
              allocate(vvdata(size(FRC%ElementData(elem)%vnl(jj)%vvdata)))
              vvdata = FRC%ElementData(elem)%vnl(jj)%vvdata
              call interpRad2prlpd(rrdata,vvdata,pos,ll,avec,gridn,.FALSE., & 
                                   fvalTmp(jj)%vata,g1Tmp(jj)%vata,g2Tmp(jj)%vata,g3Tmp(jj)%vata)
              deallocate(rrdata,vvdata)
              !write(*,*) "fvalTmp(jj)%vata ="
              !write(*,*)  size(fvalTmp(jj)%vata)
              !write(*,*) "g1Tmp(jj)%vata   ="
              !write(*,*)  g1Tmp(jj)%vata
              !write(*,*) "g2Tmp(jj)%vata   ="
              !write(*,*)  g2Tmp(jj)%vata
              !write(*,*) "g3Tmp(jj)%vata   ="
              !write(*,*)  g3Tmp(jj)%vata
          end do
          call unionCell(g1Tmp,g1Atom)
          call unionCell(g2Tmp,g2Atom)
          call unionCell(g3Tmp,g3Atom)

          !write(*,*) "g1Atom ="
          !write(*,*)  g1Atom
          !write(*,*) "g2Atom ="
          !write(*,*)  g2Atom
          !write(*,*) "g3Atom ="
          !write(*,*)  g3Atom
          !do jj = 1,LenVata,1
          !    call union(g1Tmp(jj),g1Atom)
          !    call union(g2Tmp(jj),g2Atom)
          !    call union(g3Tmp(jj),g3Atom)
          !end do

          !stop
          !write(*,*) "fvN =", fvN
          allocate(fvAtom(size(g1Atom),size(g2Atom),size(g3Atom),fvN))
          !kk     = 1
          fvAtom = 0.0D0
          kk     = 1

          do jj = 1,LenVata,1
              allocate(g1ind(size(g1Tmp(jj)%vata)))
              allocate(g2ind(size(g2Tmp(jj)%vata)))
              allocate(g3ind(size(g3Tmp(jj)%vata)))
              g1ind = g1Tmp(jj)%vata-minval(g1Tmp(jj)%vata)+1
              g2ind = g2Tmp(jj)%vata-minval(g2Tmp(jj)%vata)+1
              g3ind = g3Tmp(jj)%vata-minval(g3Tmp(jj)%vata)+1
              !write(*,*) "g*ind"
              !write(*,*)  g1ind
              !write(*,*) " "
              !write(*,*)  g2ind
              !write(*,*) " "
              !write(*,*)  g3ind
              allocate(fvAtomTmp(size(g1Ind),size(g2Ind),size(g3Ind),2*fvL(jj)+1))
              fvAtomTmp = &
                  reshape(fvalTmp(jj)%vata,(/size(g1Ind),size(g2Ind),size(g3Ind),2*fvL(jj)+1/))
              !write(*,*) "size of fvAtomTmp"
              !write(*,*)  size(fvAtomTmp)
              !write(*,*) "size of fvalTmp(jj)%vata"
              !write(*,*)  size(fvalTmp(jj)%vata)
              forall(nn1=1:size(g1Ind),nn2=1:size(g2Ind),nn3=1:size(g3Ind),nn4=kk:kk+2*fvL(jj))
                      fvAtom(g1Ind(nn1),g2Ind(nn2),g3Ind(nn3),nn4) = &
                            fvAtomTmp(nn1,nn2,nn3,nn4-kk+1)
              end forall
              kk = kk + 2*fvL(jj)+1
              deallocate(g1ind,g2ind,g3ind)
              deallocate(fvAtomTmp)
          end do

          !stop
          allocate(FRC%potential%vnl%kbDcell(ii)%vata(size(g1Atom)*size(g2Atom)*size(g3Atom),fvN))
          allocate(FRC%potential%vnl%g1Ind(ii)%vata(size(g1Atom)))
          allocate(FRC%potential%vnl%g2Ind(ii)%vata(size(g2Atom)))
          allocate(FRC%potential%vnl%g3Ind(ii)%vata(size(g3Atom)))
          FRC%potential%vnl%kbDcell(ii)%vata = reshape(fvAtom,(/size(g1Atom)*size(g2Atom)*size(g3Atom),fvN/))
          FRC%potential%vnl%g1Ind(ii)%vata   = g1Atom
          FRC%potential%vnl%g2Ind(ii)%vata   = g2Atom
          FRC%potential%vnl%g3Ind(ii)%vata   = g3Atom

          !write(*,*) "results ="
          !do jj = 1,size(FRC%potential%vnl%kbDcell(ii)%vata,2),1
          !do kk = 1,size(FRC%potential%vnl%kbDcell(ii)%vata,1),1
          !write(*,*)  kk,jj,FRC%potential%vnl%kbDcell(ii)%vata(kk,jj)
          !end do
          !end do
          !write(*,*) " "
          !write(*,*)  FRC%potential%vnl%g1Ind(ii)%vata
          !write(*,*) " "
          !write(*,*)  FRC%potential%vnl%g2Ind(ii)%vata
          !write(*,*) " "
          !write(*,*)  FRC%potential%vnl%g3Ind(ii)%vata

          do jj = 1,LenVata,1
              deallocate(fvalTmp(jj)%vata,g1Tmp(jj)%vata,g2Tmp(jj)%vata,g3Tmp(jj)%vata)
          end do
          deallocate(fvalTmp,g1Tmp,g2Tmp,g3Tmp)
          deallocate(lnu,nnu,inde,fvL,g1Atom,g2Atom,g3Atom)
          deallocate(fvAtom)

      end do

      allocate(FRC%potential%vnl%Aorb(size(evec)))
      FRC%potential%vnl%Aorb     = evec
      allocate(FRC%potential%vnl%KBEnergy(size(KBEnergy)))
      FRC%potential%vnl%KBEnergy = KBenergy
      !write(*,*) "Aorb", evec
      !write(*,*) "KBenergy", FRC%potential%vnl%KBEnergy
      !stop

      return
      end subroutine genKBProjDense
      end module genKBProjDense_module

