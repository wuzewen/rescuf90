! calcsize.f90

!**************************************************
      !
      !
      !
!**************************************************

      subroutine calcsize(mpisize,mpirank,ngrid,nsize)

      implicit none

      ! input variables
      integer :: mpisize, mpirank, ngrid

      ! temporary variables
      integer :: ntmp

      ! output variables
      integer :: nsize

      ! body of this function
      ntmp = ceiling(dble(ngrid)/dble(mpisize))

      !write(*,*) 'mpisize', mpisize
      !write(*,*) 'in calcsize, ntmp =', ntmp
      !write(*,*) 'ngrid' ,ngrid
      if (mpirank < mpisize-1) then
              nsize = ntmp
      else
              nsize = ngrid-ntmp*(mpirank)
      end if

      return
      end subroutine calcsize
