! ElementDataType.f90
    
!*********************
    !
    !
    !
!***********************
    
    
    module ElementDataType_module
    
    type :: RelPseudoPtype
        integer              :: N, L
        real*8               :: J, Rc
        real*8, allocatable  :: rrData(:), drData(:), vvData_screened(:), vvData_unscreened(:)
    end type RelPseudoPtype
    
    type :: VlocalType
        real*8               :: Rcore, Ncore
        real*8, allocatable  :: rrData(:), drData(:), vvData(:)
    end type VlocalType
            
    type :: RlocalType
        real*8, allocatable  :: rrData(:), drData(:), rhoData(:)
    end type RlocalType
            
    type :: OrbitalSetType
        real*8, allocatable :: rrData(:), drData(:), frData(:), qqData(:), fqData(:), qwData(:)
        integer             :: N, L
        real*8              :: E, Population, CoulombEnergyU, ExchangeEnergyJ
        character(len=20)   :: OrbitalType
    end type OrbitalSetType
        
    type :: VnlType
        real*8, allocatable :: rrData(:), drData(:), vvData(:), qqData(:), fqData(:), qwData(:)
        real*8  :: L, kbsparce
        real*8  :: KBenergy, KBcosine, E
        real*8  :: isGhost
        integer :: N
    end type VnlType
            
    type :: VnaType
        real*8, allocatable :: rrData(:), drData(:), vvData(:)
    end type VnaType
            
    type :: RnaType
        real*8, allocatable :: rrData(:), drData(:), rhoData(:)
    end type RnaType
        
    type :: ElementAtomType
        integer           :: Z, N
        character(len=4)  :: Symbol
        character(len=20) :: shell
        character(len=20) :: name
        real*8            :: mass
    end type ElementAtomType
    
    type :: ElementDataType
        character(len=20)                 :: XCfunctional
        character(len=4)                  :: Symbol
        type(ElementAtomType)             :: ElementAtom
        type(RelPseudoPtype), allocatable :: RelPseudoP(:)
        type(VlocalType)                  :: Vlocal
        type(RlocalType)                  :: Rlocal
        type(OrbitalSetType), allocatable :: OrbitalSet(:)
        type(VnlType),        allocatable :: Vnl(:)
        type(VnaType)                     :: Vna
        type(RnaType)                     :: Rna
    end type ElementDataType
    
    end module ElementDataType_module