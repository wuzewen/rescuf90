! diag_lcao.f90

!************************************************************
      !
      !
      !
!************************************************************

      module diag_lcao_module
      contains
      subroutine diag_lcao(FRC,iscomplex,evOnly,ZZ,theta)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      logical                         :: iscomplex, evOnly

      ! temporary variables
      logical :: mpistat, smistat
      integer :: mpirank, mpisize, bandi(2), nband, norb, mb, nb, mp, np
      character(len=20) :: algo

      real*8    , allocatable :: theta(:)
      complex*16, allocatable :: SSD(:,:), HHD(:,:)

      type(VataMN_CMPLX_2D) :: SS, HH, ZZ

      ! output variables


      ! body of this function
      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize
      smistat = FRC%smi%status

      algo    = FRC%eigenSolver%algoproj
      bandi   = FRC%eigenSolver%bandi
      nabnd   = FRC%eigenSolver%nband
      norb    = FRC%XHX%m
      mb      = FRC%smi%mb
      nb      = FRC%smi%nb
      mp      = FRC%smi%mp
      np      = mpisize/mp

      SS      = FRC%XSX
      HH      = FRC%XVX
      HH%vata = FRC%XHX%vata+FRC%XVX%vata

      if (evOnly .and. (.not. smistat)) then
              allocate(ZZ(0,0))
      else if (smistat .and. trim(algo) == trim('DSYGV')) then
              call distmat_zeroslike(FRC,FRC%XVX,ZZ)
      else
              tmp(1) = norb
              tmp(2) = nband
              call distmat_zeroslike(FRC,FRC%XVX,tmp,ZZ)
      end if

      if (smistat) then
          abstol = FRC%smi%abstol
          orfac  = FRC%smi%orfac
          lwork  = FRC%smi%lwork
          if (    trim(algo) .EQ. trim('DSYEV') &
                  trim(algo) .EQ. trim('DSYEVD') &
                  trim(algo) .EQ. trim('DSYEVX')) then
              call distmat_zeroslike(FRC,HH,ZZ)
              evrange = 'A'
          else if (trim(algo) .EQ. trim('DSYEVR') &
                   trim(algo) .EQ. trim('DSYEVX') &
                   trim(algo) .EQ. trim('DSYGVX')) then
              call distmat_zeroslike(FRC,HH,ZZ)
              evrange = 'I'
          end if
          if (evOnly) then
                  jobz = 'N'
          else
                  jobz = 'V'
          end if

          if (iscpmlex) then
              if (    trim(algo) .EQ. trim('DSYEV')  .OR. &
                      trim(algo) .EQ. trim('DSYEVD') .OR. &
                      trim(algo) .EQ. trim('DSYGVR') .OR. &
                      trim(algo) .EQ. trim('DSYGVX')) then
                  call smiZPOTRF(SS)
                  call smiZHEGST(HH,SS)
                  if (trim(algo) .EQ. trim('DSYEV')) then
                      call smiZHEEV(jobz,'U',HH,ZZ,theta)
                  else if (trim(algo) .EQ. trim('DSYEVD')) then
                      call smiZHEEVD('V','U',HH,ZZ,theta)
                  else if (trim(algo) .EQ. trim('DSYGVR')) then
                      call smiZHEEVR(jobz,'I','U',HH,bandi(1),bandi(2),&
                                          lwork,ZZ,theta)
                  else if (trim(algo) .EQ. trim('DSYGVX')) then
                      call smiZHEEVX(jobz,'I','U',HH,bandi(1),bandi(2),&
                                          abstol,orfac,lwork,Z,theta)
                  end if
                  if (.NOT. evOnly) then
                      call smiZTRTRS('U','N',SS,ZZ)
                  end if
              else if (trim(algo) .EQ. trim('DSYGV')) then
                  call smiZHEGVX(jobz,'A','U',HH,SS,bandi(1),bandi(2),&
                                 abstol,orfac,lwork,ZZ)
              else if (trim(algo) .EQ. trim('DSYGVX')) then
                  call smiZHEGVX(jobz,'I','U',HH,SS,bandi(1),bandi(2),&
                                 abstol,orfac,lwork,ZZ)
              end if
          else
              if (    trim(algo) .EQ. trim('DSYEV')  .OR. &
                      trim(algo) .EQ. trim('DSYEVD') .OR. &
                      trim(algo) .EQ. trim('DSYGVR') .OR. &
                      trim(algo) .EQ. trim('DSYGVX')) then
                  call smiDPOTRF(SS)
                  call smiDSYGST(HH,SS)
                  if (trim(algo) .EQ. trim('DSYEV')) then
                      call smiDSYEV(jobz,'U',HH,ZZ,theta)
                  else if (trim(algo) .EQ. trim('DSYEVD')) then
                      call smiDSYEVD('V','U',HH,ZZ,theta)
                  else if (trim(algo) .EQ. trim('DSYGVR')) then
                      call smiDSYEVR(jobz,'I','U',HH,bandi(1),bandi(2),&
                                       ZZ,theta)
                  else if (trim(algo) .EQ. trim('DSYGVX')) then
                      call smiDSYEVX(jobz,'I','U',HH,bandi(1),bandi(2),&
                              abstol,orfac,lwork,ZZ,theta)
                  end if
                  if (.NOT. evOnly) then
                      call smiDTRTRS('U','N',SS,ZZ)
                  end if
              else if (trim(algo) .EQ. trim('DSYGV')) then
                  call smiDSYGVX(jobz,'A','U',HH,SS,bandi(1),bandi(2), &
                          abstol,orfac,lwork,ZZ,theta)
              else if (trim(algo) .EQ. trim('DSYGVX')) then
                  call smiDSYGVX(jobz,'I','U',HH,SS,bandi(1),bandi(2), &
                          abstol,orfac,lwork,ZZ,theta)
              end if
          end if

          if (trim(evrange) .EQ. 'A') then
              theta = theta(bandi(1):bandi(2))
              if (.NOT. evOnly) then
                  call ModBCDist(FRC,ZZ,ZZ%mblock,ZZ%nblock,mpisize,1,ZZ)
                  if (allocated(ZZ%vata)) then
                      ZZ%vata = ZZ%vata(:,bandi(1):bandi(2))
                  end if
                  ZZ%n = nband
                  call ModBCDist(FRC,ZZ,ZZ%mblock,ZZ%nblock, &
                                   FRC%XSX%mproc,FRC%XSX%nproc)
              end if
          end if
              !write(*,*) "Error in diag_lcao. SMI"
      else
          call distmat_gather(FRC,SS,SSD)
          call distmat_gather(FRC,HH,HHD)
          if (mpirank == 0) then
              SStmp = transpose(SSD)
              SStmp = conjg(SStmp)
              SSD   = 0.5D0*(SSD+SSTMP)
              HHTMP = TRANSPOSE(HHD)
              HHTMP = conjg(HHtmp)
              HHD   = 0.5D0*(HHD+HHtmp)
          end if

          if (evOnly) then
              if (mpirank == 0) then
                  call ZHEGV(1,'V','U',N,HH,N,SS,N,W,WORK,LWORK,RWORK,INFO)
                  forall(ii=bandi(1):bandi(2))
                      theta(ii-bandi(1)+1) = W(ii)
                  end forall
              else
                  theta = 0.0D0
              end if
          else
              if (mpirank == 0) then
                  call ZHEGV(1,'V','U',N,HH,N,SS,N,W,WORK,LWORK,RWORK,INFO)
                  forall(ii=bandi(1):bandi(2))
                      theta(ii-bandi(1)+1)     = W(ii)
                      ZZ%vata(:,ii-bandi(1)+1) = HH(:,ii)
                  end forall
              else
                  theta   = 0.0D0
                  ZZ%vata = 0.0D0
              end if
              ZZ%mblock = ZZ%m
              ZZ%nblock = ZZ%n
              call ModBCDist(FRC,ZZ,mb,nb,mp,np,.FALSE.,ZZ)
          end if
          if (mpistat) then
              call BCAST(theta,0)
              !write(*,*) "Error in diag_lcao. MPI"
          end if
      end if

      return
      end subroutine diag_lcao
      end module diag_lcao_module

