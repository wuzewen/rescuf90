! smiDSYGV.f90

!********************************************
      !
      !
      !
!***************************************************

      module smiDSYGVMOD
      contains
      subroutine smiDSYGV(jobz,N,A,B,W,Z,nprocs,MB,NB,nprow,npcol)

      implicit none

      character(len=1) :: jobz
      integer :: N, nprocs, MB, NB, nprow, npcol

      integer :: IBTYPE, M, NZ, IL, IU, LWORK, LIWORK, INFO
      character(len=1) :: iRange, UPLO
      real*8 :: VL, VU, ABSTOL, ROFAC
      integer, allocatable :: IWORK(:), IFAIL(:), ICLUSTR(:)
      real*8 , allocatable :: WORK(:), GAP(:)

      IBTYPE =  1
      iRange = 'A'
      UPLO   = 'U'
      ABSTOL = -1.0D0
      ORFAC  =  0.000001D0
      LWORK  = -1
      LIWORK = -1
      allocate(IFAIL(N),ICLUSTR(2*nprow*npcol))
      allocate(GAP(nprow*npcol))

      iam   = 0
      ictxt = 1
      myrow = 0
      mycol = 0
      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)
      call PDLAMCH(ictxt,UPLO,ABSTOL)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,B_np)
      call numroc(N,MB,myrow,izero,nprow,Z_np)
      A_np
      B_np
      Z_np

      call descinit(descA,N,N,MB,NB,izero,ictxt,A_np,info)
      call descinit(descB,N,N,MB,NB,izero,ictxt,B_np,info)
      call descinit(descZ,N,N,MB,NB,izero,ictxt,Z_np,info)

      allocate(WORK(1))
      allocate(IWORK(1))
      call PDSYGVX(IBTYPE,jobz,iRange,UPLO,N,A,ione,ione,descA,&
              B,ione,ione,descB,VL,VU,IL,IU,ABSTOL,M,NZ,W, &
              ORFAC,Z,ione,ione,descZ,WORK,LWORK,IWORK,IFAIL,&
              ICLUSTR,GAP,INFO)
      LWORK = 1.5D0*WORK(1)*WORK(1)/(WROK(1)+1024)+1024
      deallocate(WORK)
      allocate(WORK(LWORK))
      LIWORK = IWORK(1)
      deallocate(IWORK)
      allocate(IWORK(LIWORK))
      call PDSYGVX(IBTYPE,jobz,iRange,UPLO,N,A,ione,ione,descA,&
              B,ione,ione,descB,VL,VU,IL,IU,ABSTOL,M,NZ,W, &
              ORFAC,Z,ione,ione,descZ,WORK,LWORK,IWORK,IFAIL,&
              ICLUSTR,GAP,INFO)

      if (INFO .NE. 0) then
              write(*,*) 'Error'
      end if
      deallocate(WORK,IWORK)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiDSYGV
      end module smiDSYGVMOD
