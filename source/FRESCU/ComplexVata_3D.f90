! ComplexVata_3D.f90

!************************************************************************
      !
      ! This module is used to define a type of data.
      ! The data named vata is 3 dimentional allocatable complex
      ! array.
      !
!************************************************************************

      module ComplexVata_3D_module

              type :: ComplexVata_3D
                      complex*16, allocatable :: vata(:,:,:)
              end type ComplexVata_3D

      end module ComplexVata_3D_module
