! unionCell.f90

!***************************************************************************
      !
      ! This function is used to ngenerate all the unique integers in
      ! CellA. The results are saved in ArrayB, which is sorted.
      ! 
      ! CellA  :: input,  IntegerVata_1D data type
      ! ArrayB :: output, 1D Integer array
      !
!***************************************************************************

      module unionCell_module
      contains
      subroutine unionCell(CellA,ArrayB)

      use uniqueInteger_module
      use IntegerVata_1D_module

      implicit none

      ! input variables
      type(IntegerVata_1D), allocatable :: CellA(:)

      ! temporary varoables
      integer :: NCell, ntmp, mtmp, ii, jj
      integer, allocatable :: Atmp(:)

      ! output variables
      Integer, allocatable :: ArrayB(:)

      ! body of this function
      NCell = size(CellA)
      ntmp  = 0
      do ii = 1,NCell,1
          ntmp = ntmp + size(CellA(ii)%vata)
      end do
      allocate(Atmp(ntmp))
      ntmp  = 0
      mtmp  = 0
      do ii = 1,NCell,1
          ntmp = mtmp + 1
          mtmp = mtmp + size(CellA(ii)%vata)
          forall(jj=ntmp:mtmp)
                  Atmp(jj) = CellA(ii)%vata(jj-ntmp+1)
          end forall
      end do
      !write(*,*) "Atmp =", Atmp
      call uniqueInteger(Atmp,ArrayB)

      return
      end subroutine unionCell
      end module unionCell_module
