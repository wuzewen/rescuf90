! DISTMATISREAL.f90

!****************************************************************************************
      !
      !
      !
!****************************************************************************************

      module DISTMATISREALMOD

      interface DISTMATISREAL
              module procedure distmat_isreal_1Dreal
              module procedure distmat_isreal_2Dreal
              module procedure distmat_isreal_1Dcmplx
              module procedure distmat_isreal_2Dcmplx
      end interface DISTMATISREAL

      end module DISTMATISREALMOD
