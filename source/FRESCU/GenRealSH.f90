! GenRealSH.f90
    
!*********************************
    !
    !
    !
!*********************************
    
    module GenRealSH_module
    contains
    subroutine GenRealSH(x,y,z,ll,mm,Ylm)
    
    use bsxfunRdivide_module
    
    implicit none
    
    ! input variables
    real*8, allocatable  :: x(:), y(:), z(:)
    integer              :: ll, mm
    
    ! temporary variables
    real*8               :: pi, tmpR, tmpR1, tmpR2, tmpR3
    integer              :: n, ntmp, itmp, ii
    real*8, allocatable  :: r(:), tmp(:,:), rtmp(:,:), rrtmp(:,:)
    integer, allocatable :: inde(:)
    
    ! output variables
    real*8, allocatable  :: Ylm(:,:)
    
    ! body of this function
    pi   = 3.1415926535897932385D0
    n    = size(x)
    allocate(r(n))
    r    = sqrt(x**2.0D0+y**2.0D0+z**2.0D0)
    !allocate(inde(n))
    allocate(rtmp(n,1))
    rtmp(:,1) = r(:)
    
    ntmp = 0
    do ii = 1,n,1
        if (r(ii) == 0) then
            ntmp = ntmp+1
        end if
    end do
    allocate(inde(ntmp))
    ntmp = 0
    do ii = 1,n,1
        if (r(ii) == 0) then
            ntmp = ntmp+1
            inde(ntmp) = ii
        end if
    end do
    !inde = r == 0
    
    !write(*,*) "ll in genrealsh"
    !write(*,*)  ll
    
    select case (ll)
    case (0)
        
        !write(*,*) "case 0 in genrealsh"
        
        allocate(Ylm(n,1))
        forall(ii=1:n)
            Ylm(ii,1) = (sqrt(pi)**(-1.0D0))/2.0D0
        end forall
        
    case (1)
        
        !write(*,*) "case 1 in genrealsh"
        
        allocate(tmp(n,3))
        allocate(Ylm(n,3))
        tmpR     = (3.0D0*(4.0D0*pi)**(-1.0D0))**0.5D0
        tmp(:,1) = y*tmpR
        tmp(:,2) = z*tmpR
        tmp(:,3) = x*tmpR
        !tmp      = (3/4/pi)**0.5*tmp
        call bsxfunRdivide(tmp,rtmp,Ylm)
        do ii = 1,ntmp,1
            itmp        = inde(ii)
            Ylm(itmp,:) = 0.0D0
        end do
    case(2)
        
        !write(*,*) "case 2 in genrealsh"
        
        allocate(tmp(n,5))
        allocate(Ylm(n,5))
        tmp(:,1) = ((15.0D0/pi)**0.5D0)*x*y/2.0D0
        tmp(:,2) = ((15.0D0/pi)**0.5D0)*y*z/2.0D0
        tmp(:,3) = ((05.0D0/pi)**0.5D0)*(2.0D0*z**2.0D0-x**2.0D0-y**2.0D0)/4.0D0
        tmp(:,4) = ((15.0D0/pi)**0.5D0)*x*z/2.0D0
        tmp(:,5) = ((15.0D0/pi)**0.5D0)*(x**2.0D0-y**2.0D0)/4.0D0
        allocate(rrtmp(n,1))
        rrtmp = rtmp**2
        call bsxfunRdivide(tmp,rrtmp,Ylm)
        do ii = 1,ntmp,1
            itmp        = inde(ii)
            Ylm(itmp,:) = 0.0D0
        end do
    case(3)
        
        !write(*,*) "case 3 in genrealsh"
        
        allocate(tmp(n,7))
        allocate(Ylm(n,7))
        tmp(:,1) = ((35.0D0/2.0D0/pi)**0.5D0)*(3.0D0*x**2.0D0-y**2.0D0)*y/4.0D0
        tmp(:,2) = ((105.0D0/pi)**0.5D0)*(x*y*z)/2.0D0
        tmp(:,3) = ((21.0D0/2.0D0/pi)**0.5D0)*y*(4.0D0*z**2.0D0-x**2.0D0-y**2.0D0)/4.0D0
        tmp(:,4) = ((7.0D0/pi)**0.5D0)*z*(2.0D0*z**2.0D0-3.0D0*x**2.0D0-3.0D0*y**2.0D0)/4.0D0
        tmp(:,5) = ((21.0D0/2.0D0/pi)**0.5D0)*x*(4.0D0*z**2.0D0-x**2.0D0-y**2.0D0)/4.0D0
        tmp(:,6) = ((105.0D0/pi)**0.5D0)*(x**2.0D0-y**2.0D0)*z/4.0D0
        tmp(:,7) = ((35.0D0/2.0D0/pi)**0.5D0)*(x**2.0D0-3.0D0*y**2.0D0)*x/4.0D0
        allocate(rrtmp(n,1))

        !write(*,*) "I'm here in genrealSH."
        !write(*,*)  tmp



        rrtmp = rtmp**3.0D0
        call bsxfunRdivide(tmp,rrtmp,Ylm)
        do ii = 1,ntmp,1
            itmp        = inde(ii)
            Ylm(itmp,:) = 0.0D0
        end do
    end select
    
    return
    end subroutine GenRealSH
    end module GenRealSH_module
