! RealVata_1D.f90
    
!*****************************************************************
    !
    ! This module is used to define a type of data.
    ! The data is 1 dimentional allocatable real array.
    !
!*****************************************************************
    
    module RealVata_1D_module
    
    type :: RealVata_1D
        real*8, allocatable :: vata(:)
    end type
    
    end module RealVata_1D_module
