! distmat_zeroslike_CMPLX_2D.f90
    
!****************************************************************************
    !
    ! This functions generate a data type containing 2 dimentional vata.
    !
!****************************************************************************
    
    module distmat_zeroslike_CMPLX_2D_module
    contains
    subroutine distmat_zeroslike_CMPLX_2D(FRC,dA,n,dB)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use GetGlobalInd_2D_module
    use VataMN_2D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_CMPLX_2D)      :: dA
    integer                    :: n(2)
    
    ! temporary variables
    integer              :: mpirank
    integer, allocatable :: ia(:,:), ja(:,:)
    
    ! output variables
    type(VataMN_2D)      :: dB
    
    ! body of this function
    !if (n(2)<2) then
    !    write(*,*) "Error in distmat_zeroslike_2D.f90. n(2) should be larger than 1 as input."
    !    write(*,*) "Or you may need distmat_zeroslike_1D.f90 for one dimentioanl vata."
    !    stop
    !end if
    
    mpirank = FRC%mpi%rank
    !write(*,*) "I'm here before InitDistArray in distmat_zerolike_2D."   
    call InitDistArray_2D(n(1),n(2),dA%mblock,dA%nblock,dA%mproc,dA%nproc,dB)
    !write(*,*) "I'm here after InitDistArray in distmat_zerolike_2D."
    if (dB%mblock > n(1)) then
        dB%mblock = max(n(1),1)
    end if
    
    if (dB%nblock > n(2)) then
        dB%nblock = max(n(2),1)
    end if
    !write(*,*) "I'm here before GetGlobalInd_2D in distmat_zerolike_2D."
    call GetGlobalInd_2D(mpirank,dB,ia,ja)
    !write(*,*) "I'm here after GetGlobalInd_2D in distmat_zerolike_2D."
    allocate(dB%vata(size(ia),size(ja)))
    dB%vata = 0.0D0
    
    return
    end subroutine distmat_zeroslike_CMPLX_2D
    end module distmat_zeroslike_CMPLX_2D_module
