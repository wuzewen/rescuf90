! smiDSYEV.f90

!**********************************************
      !
      !
      !
!*******************************************************
      
      module smiDSYEVMOD
      contains
      subroutine smiDSYEV(jobz,UPLO,N,A,MB,NB,nprow,npcol,W,Z)

      implicit none

      integer :: jobz, N, MB, NB, nprow, npcol
      real*8, allocatable :: A(), W(), Z()

      LWORK  = -1
      nprocs = nprow*npcol
      iam    = 0
      ictxt  = 0
      myrow  = 0
      mycol  = 0

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      A_np = 
      call numroc(N,MB,myrow,izero,nprow,Z_np)
      Z_np = 
      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descZ,N,N,MB,NB,izero,izero,ictxt,Z_np,descinfo)
      allocate(WORK())
      call PDSYEV(jobz,UPLO,N,A,ione,ione,descA,W,Z,ione,ione,descZ,&
              WORK,info)
      LWORK = 1.5D0*WORK(1)*WORK(1)/(WORK(1)+1024)+1024
      deallocate(WORK)
      allocate(WORK())
      call PDSYEV(jobz,UPLO,N,A,ione,ione,descA,W,Z,ione,ione,descZ,&
              WORK,info)
      if (info .NE. 0) then
              write(*,*) 'Error'
      end if

      deallocate(WORK)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiDSYEV
      end module smiDSYEVMOD
