! cat.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    module cat_module
    contains
    subroutine cat(dim,A,B,C)
 
    use errMod

    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:)
    integer             :: dim
    
    ! temporary variables
    integer             :: mA, nA, mB, nB, ii, ntmp
    character(len=100)  :: mes
    
    ! output variables
    real*8, allocatable :: C(:,:)
    
    ! body of this function
    mA = size(A,1)
    nA = size(A,2)
    mB = size(B,1)
    nB = size(B,2)
    
    if (dim == 1) then
        
        if (nA /= nB) then
            mes = 'Error in cat'
            call err(mes)
            !write(*,*) "Error in cat. if dimention has been setted to 1, the size of A and B in another dimention should be the same."
            stop
        end if
        ntmp = mA + mB
        allocate(C(ntmp,nA))
        forall(ii=1:mA)
            C(ii,:) = A(ii,:)
        end forall
        forall(ii=mA+1:mA+mB)
            C(ii,:) = B(ii-mA,:)
        end forall
        
    else if (dim == 2) then
        
        if (mA /= mB) then
            mes = 'Error in cat'
            call err(mes)
            !write(*,*) "Error in cat. if dimention has been setted to 2, the size of A and B in another dimention should be the same."
            stop
        end if
        
        ntmp = nA + nB
        !allocate(C(mA,ntmp))
        forall(ii=1:nA)
            C(:,ii) = A(:,ii)
        end forall
        forall(ii=nA+1:nA+nB)
            C(:,ii) = B(:,ii-nA)
        end forall
        
    else
        mes = 'Error in cat'
        call err(mes)
        !write(*,*) "Error in cat.f90. dim should be 1 or 2, not any other number."
        
    end if
    
    return
    end subroutine cat
    end module cat_module
