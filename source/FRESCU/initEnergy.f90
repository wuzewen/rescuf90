! initEnergy.f90
!********************************
    !
!********************************
    
    module initEnergy_module
    contains
    subroutine initEnergy(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType)              :: inputFromFile
    
    ! temporary variables
    integer :: ii, nline, status
    logical :: energyInputFileIsFind, NRGenergyIsFind, FermiEnergyIsFind, XCdensIsFind
    logical :: ewaldEIsFind, energyEksIsFind, energyEtotIsFind, energyEdHIsFind
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%energy) then
        energyInputFileIsFind = .FALSE.
        NRGenergyIsFind       = .FALSE.
        FermiEnergyIsFind     = .FALSE.
        XCdensIsFind          = .FALSE.
        ewaldEIsFind          = .FALSE.
        energyEksIsFind       = .FALSE.
        energyEtotIsFind      = .FALSE.
        energyEdHIsFind       = .FALSE.
        nline                 =  inputFromFile%numberOfInput
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("energyInputFile")) then
                energyInputFileIsFind = .TRUE.
                exit
            end if
        end do
        if (trim(FRC%info%calculationType) == trim("DOS") .and. energyInputFileIsFind) then
            write(*,*) "Error in initEnergy.f90. DOS calculation is not available now."
            stop
            !FRC%energy%inputFile = inputFromFile%NAV(ii)%value
            !inquire(file=FRC%energy%inputFile,exist=alive)
            !if (alive) then
            !    call readEnergyFile(FRC)
            !end
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("NRGenergy")) then
            !        NRGenergyIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (NRGenergyIsFind) then
            !    FRC%energy                 = inputFromFile%NAV(ii)%value
            !    FRC%init%energy            = .TRUE.
            !    FRC%option%maxSCFiteration = 0
            !    exit
            !    return
            !end
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("FermiEnergy")) then
                FermiEnergyIsFind = .TRUE.
                exit
            end if
        end do
        if (FermiEnergyIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%energy%EFermi
        else
            FRC%energy%EFermi = 0.0D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("XCdens")) then
                XCdensIsFind = .TRUE.
                exit
            end if
        end do
        if (XCdensIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%energy%XCdens%vata
        else
            
            !FRC%energy%XCdens(1)%vata  = 0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("ewaldE")) then
                ewaldEIsFind = .TRUE.
                exit
            end if
        end do
        if (ewaldEIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%energy%ewaldE = .FALSE.
            else
                FRC%energy%ewaldE = .TRUE.
            end if
        else
            FRC%energy%ewaldE = .TRUE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("energyEks")) then
                energyEksIsFind = .TRUE.
                exit
            end if
        end do
        if (energyEksIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%energy%Eks
        else
            !FRC%energy%Eks = 0.0D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("energyEtot")) then
                energyEtotIsFind = .TRUE.
                exit
            end if
        end do
        if (energyEtotIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%energy%Etot
        else
            !FRC%energy%Etot = 0.0D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("energyEdH")) then
                energyEdHIsFind = .TRUE.
                exit
            end if
        end do
        if (energyEksIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%energy%EdH
        else
            !FRC%energy%EdH = 0.0D0
        end if
        
        FRC%init%energy = .TRUE.
    end if
    
    return
    end subroutine initEnergy
    end module initEnergy_module
