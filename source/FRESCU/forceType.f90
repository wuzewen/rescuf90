! forceType.f90
    
!**************************************************
    !
    ! This type contains the force information
    !
!**************************************************
    
    module forceType_module
    
    type :: forceType
        logical :: status
        integer :: sampling(3)
        integer :: lebedev
        real*8  :: resolution
        integer :: gridn(3)
    end type forceType
    
    end module forceType_module