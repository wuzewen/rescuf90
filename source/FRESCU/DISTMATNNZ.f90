! DISTMATNNZ.f90

! ****************************************************************************************
      !
      !
      !
      !
!*****************************************************************************************

      module DISTMATNNZMOD

              interface DISTMATNNZ
                      module procedure distmat_nnz_1D
                      module procedure distmat_nnz_2D
              end interface DISTMATNNZ

      end module DISTMATNNZMOD
