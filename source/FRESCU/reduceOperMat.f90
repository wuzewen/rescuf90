! reduceOperMat.f90

!************************************************************************************
      !
      !
      !
!************************************************************************************

      module reduceOperMatMod
      contains
      subroutine reduceOperMat(Acell,Amat,Av,kpt)

      implicit none

      im    = (0.0D0,1.0D0)
      phase = exp(2.0D0*im*pi*matmul(kpt,transpose(conjg(Av))))
      vata  = matmul(Amat,transpose(phase))
      Acell%vata = reshape(data,size(Acell%vata,1),size(Acell%vata,2))

      return
      end subroutine reduceOperMat
      end module reduceOperMatMod
