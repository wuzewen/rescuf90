! spruceCell.f90

!****************************************************************************
      !
      !
      !
!****************************************************************************

      module sprucecellMod
      contains
      subroutine spruceCell(FRC,Acell,idel)

      implicit none

      call cellfunisempty(Acell,Aocc)
      call cellfunnnz(Aocc,Atmp)
      Annz(Aocc) = Atmp
      if (FRC%mpi%status) then
              call MPI_Allreduce_sum(Annz)
      end if

      do ii = 1,size(Annz),1
          if (Annz(ii) .EQ. 0) then
                  idel(ii) = .TRUE.
          else
                  idel(ii) = .FALSE.
          end if
      end do

      return
      end subroutine spruceCell
      end module spruceCellMod

