! smiDSYEVX.f90

!*********************************************************
      !
      !
      !
!*********************************************************

      module smiDSYEVXMOD
      contains
      subroutine smiDSYEVX(jobz,iRange,UPLO,N,A,MB,NB,nprow,npcol,&
                      VL,VU,IL,IU,ABSTOL,ORFAC,LWORK,W,Z)

      implicit none

      character(len=1) :: jobz, iRange, UPLO
      integer :: N, MB, NB, nprow, npcol, IL, IU, LWORK
      real*8, allocatable :: A(), W(), Z()
      real*8 :: VL, VU, ABSTOL, ORFSC

      integer :: M, NZ, LWORKTMP, LIWORK, INFO, nprocs
      integer :: iam, ictxt, myrow, mycol

      LIWORK = -1
      nprocs =  nprow*npcol
      iam    =  0
      ictxt  =  0
      myrow  =  0
      mycol  =  0

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,Z_np)
      A_np = 
      Z_np = 

      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descZ,N,N,MB,NB,izero,izero,ictxt,Z_np,descinfo)

      allocate(WORK(1))
      allocate(IWORK(1))
      call PDSYEVX(jobz,iRange,UPLO,N,A,ione,ione,descA,VL,VU,IL,IU,&
              ABSTOL,M,NZ,W,ORFAC,Z,ione,ione,descZ,WORK,LWORK,IWORK,&
              LIWORK,IFAIL,ICLUSTR,GAP,INFO)
      LIWORK = IWORK(1)
      deallocate(IWORK)
      allocate(IWORK(LIWORK))
      LWORKTMP = 1.5*WORK(1)*WORK(1)/(WORK(1)+1024)+1024
      if (LWORK .LT. LWORKTMP .OR. LWORK .LT. 0) then
              LWORK = LWORKTMP
      end if
      deallocate(WORK)
      allocate(WORK(LWORK))
      call PDSYEVX(jobz,iRange,UPLO,N,A,ione,ione,descA,VL,VU,IL,IU,&
              ABSTOL,M,NZ,W,ORFAC,Z,ione,ione,descZ,WORK,LWORK,IWORK,&
              LIWORK,IFAIL,ICLUSTR,GAP,INFO)

      if (INFO/2 $$ 2 .NE. 0) then
              write(*,*) 'Error'
      else if () then
              write(*,*) 'Error'
      else if () then
              write(*,*) 'Error'
      end if

      deallocate(WORK,IWORK)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiDSYEVX
      end module smiDSYEVXMOD
