! calcRho.f90
    
!*************************************************************************
    !
    !
    !
!*************************************************************************
    
    module calcRho_module
    contains
    subroutine calcRho(FRC,nocc,evec,rho)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ModBCDist_2D_module
    use ModBCDist_1D_module
    use GetGlobalInd_2D_module
    use distmat_zeroslike_CMPLX_2D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable             :: nocc(:)
    type(VataMN_CMPLX_2D)           :: evec
    
    ! temporary variables
    logical              :: mpistat, dg
    integer              :: mpirank, mpisize, ngrid, tmp(2), ii, ntmp
    type(VataMN_2D)      :: psi, rho2
    integer, allocatable :: ia(:,:), ja(:,:)
    real*8 , allocatable :: nocct(:), rhoVata(:,:)
    complex*16, allocatable :: rhotmp(:)
    
    ! output variables
    type(VataMN_1D)                 :: rho
    
    ! body of this function
    !write(*,*) "I'm here at the beginging of calcRho."
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ngrid   = product(FRC%domain%fgridn)
    
    if (all(FRC%domain%fgridn == FRC%domain%cgridn)) then
        dg = .FALSE.
    else
        dg = .TRUE.
    end if
    
    !intFun = FRC%interpolation%IntFun
    rho%vata = 0.0D0
    if (dg .and. (.not. FRC%interpolation%interpRho .NE. 0)) then

        write(*,*) "error in calcRho. FRC%domain%fgridn should equre to FRC%domain%cgridn."
        stop
        !call ModBCDist_2D(FRC,evec,1,1,1,mpisize,.FALSE.,.FALSE.,psi)
        !call GetGlobalInd_2D(mpirank,psi,ia,ja)
        !allocate(nocct(size(ja)))
        !forall(ii=1:size(ja))
        !    nocct(ii) = nocc(ja(ii,1))
        !end forall
        !
        !allocate(rhoVata(ngrid,1))
        !rhoVata   = 0
        !do ii = 1,size(psi%vata,2),1
        !    !call intFun(psi%vata(:,ii))
        !    rhoVata(:,1) = rhoVata(:,1)+nocct(ii)*real(rhotmp*rhotmp)
        !end do
        
        !call InitDistArray_1D(ngrid,1,ngrid,1,1,mpisize,rho)
    else
        tmp(1) = ngrid
        tmp(2) = 1
        !write(*,*) "I'm here before dismat_zerolike_2D in calcRho."
        !write(*,*) "evec before dismat_zerolike_2D in calcRho."
        !write(*,*)  evec%m, evec%n, evec%mblock, evec%nblock, evec%mproc, evec%nproc
        !write(*,*)  evec%vata
        call distmat_zeroslike_CMPLX_2D(FRC,evec,tmp,rho2)
        rho%m      = rho2%m
        rho%n      = rho2%n
        rho%mblock = rho2%mblock
        rho%nblock = rho2%nblock
        rho%mproc  = rho2%mproc
        rho%nproc  = rho2%nproc
        !write(*,*) "I'm here after dismat_zerolike_2D in calcRho."
        !write(*,*) "in calcRho, nocct = "
        !write(*,*)  nocc
        !write(*,*)  size(nocc)
        allocate(rhotmp(size(evec%vata,1)))
        do ii = 1,size(nocc),1
            rhotmp   = evec%vata(:,ii)
            !write(*,*) "in calcRho, rhotmp = "
            !write(*,*)  rhotmp
            !rho%vata = rho%vata+real(rhotmp*conjg(rhotmp))
            rho%vata = rho%vata+nocc(ii)*dble(rhotmp*conjg(rhotmp))
        end do
        !write(*,*) "rho ="
        !write(*,*) rho%vata
        !write(*,*) "stop in updateRho"
        !stop
        if (dg) then
            call ModBCDist_1D(FRC,rho,1,1,1,mpisize,.FALSE.,.FALSE.,rho)
            !if (size(rho%vata,2) > 0) then
            !    rho%vata = FRC%interpolation%intFun(rho%vata)
            !end if
            rho%m = ngrid
        end if
    end if
    ntmp = ceiling(dble(rho%m)/dble(mpisize))
    !write(*,*) "I'm here before ModBCDist in calcRho."
    call ModBCDist_1D(FRC,rho,ntmp,1,mpisize,1,.FALSE.,.FALSE.,rho)
    
    return
    end subroutine calcRho
    end module calcRho_module
