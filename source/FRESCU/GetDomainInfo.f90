! GetDomainInfo.f90
!*****************************************
    !
!*****************************************
    
    module GetDomainInfo_module
    contains
    subroutine GetDomainInfo(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    use inversion_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType) :: inputFromFile
    
    ! temporary variables
    real*8                               :: pi, bohr2ang, ang2bohr, nm2bohr
    logical                              :: unitsIsFind, unitsLatvecIsFind, ortho
    logical                              :: cgridnxIsFind, cgridnyIsFind, cgridnzIsFind, lowresIsFind, diffopMethodIsFind
    logical                              :: fgridnxIsFind, fgridnyIsFind, fgridnzIsFind, highresIsFind, diffopaccuracyIsFind
    logical                              :: boundaryIsFind, PWmodeIsFind, boundaryXIsFind, boundaryYIsFind, boundaryZIsFind
    integer                              :: sizeBC, pstatus, ii, jj, bc(3)
    integer, allocatable                 :: locateBC(:),tmp(:)
    logical                              :: bvalxIsFind, bvalyIsFind, bvalzIsFind
    real*8                               :: avectmp(3,3), dime(3)
    real*8, allocatable                  :: xyz(:,:), avec(:,:), bvec(:,:)
    integer                              :: nline
    
    ! output variables
    
    ! body of this function
    !write(*,*) "I'm in GetDomainInfo's begining"
    !write(*,*) cgridnxIsFind
    cgridnxIsFind        = .FALSE.
    cgridnyIsFind        = .FALSE.
    cgridnzIsFind        = .FALSE.
    lowresIsFind         = .FALSE.
    diffopMethodIsFind   = .FALSE.
    fgridnxIsFind        = .FALSE.
    fgridnyIsFind        = .FALSE.
    fgridnzIsFind        = .FALSE.
    highresIsFind        = .FALSE.
    diffopaccuracyIsFind = .FALSE.
    boundaryIsFind       = .FALSE.
    PWmodeIsFind         = .FALSE.
    boundaryXIsFind      = .FALSE.
    boundaryYIsFind      = .FALSE.
    boundaryZIsFind      = .FALSE.
    pi                   = 3.1415926535897932385D0
    bohr2ang             = 0.52917721067D0
    ang2bohr             = 1.0D0/bohr2ang
    nm2bohr              = ang2bohr*10.0D0
    unitsIsFind          = .FALSE.
    unitsLatvecIsFind    = .FALSE.
    ortho                = .FALSE.
    nline                = inputFromFile%NumberofInput
    !write(*,*) "lattice in getDomaininfo."
    !write(*,*)  FRC%domain%latvec
    if (.not. FRC%init%domaininfo) then
        !ii = 0
        !do while(ii<=nline)
        !    ii     = ii+1
        !    if (trim(inputFromFile(ii)%name) == trim("units")) then
        !        unitsIsFind = .TRUE.
        !        exit
        !    end if
        !end do
        
        !ii = 0
        !do while(ii<=nline)
        !    ii     = ii+1
        !    if (trim(inputFromFile(ii)%name) == trim("unitsLatvec")) then
        !        unitsLatvecIsFind = .TRUE.
        !        exit
        !    end if
        !end do
        
        !if (.not. unitsIsFind) then
        !    FRC%units%latvec = "bohr"
        !else if (.not. unitsLatvecIsFind) then
        !    FRC%units%latvec = "bohr"
        !else
        !    FRC%units%latvec = inputFromFile(ii)value
        !end if
        
        !call readLattice(FRC)
        !avec = FRC%domain%latvec
        
        if (trim(FRC%units%latvec) == trim("Angstrom")) then
            FRC%domain%latvec = FRC%domain%latvec*ang2bohr
        else if (trim(FRC%units%latvec) == trim("nm")) then
            FRC%domain%latvec = FRC%domain%latvec*nm2bohr
        end if
        !write(*,*) "lattice in getDomaininfo."
        !write(*,*)  FRC%domain%latvec
        !call readCoordinate(FRC)
        
        if (trim(FRC%atom%coordinateType) == trim("Fractional") .or. trim(FRC%atom%coordinateType) == trim("Direct")) then
            allocate(FRC%atom%xyz(FRC%atom%NumberOfAtom,3))
            FRC%atom%xyz = matmul(FRC%atom%fracxyz,avec)
            deallocate(FRC%atom%fracxyz)
        end if
        
        !FRC%domain%latvec = avec
        
        FRC%units%latvec  = "Bohr"
        
        allocate(avec(3,3),bvec(3,3))
        avec = FRC%domain%latvec
        call inversion(avec,bvec)
        FRC%domain%recvec = transpose(bvec)*2.0D0*pi
        dime = sqrt(sum(avec*avec,2))
        
        if (FRC%option%centerMolecule) then
            write(*,*) "Error in GetDomainInfo.f90. FRC%option%centerMolecule should be false now."
            stop
            !allocate(XYZ(FRC%atom%NumberOfAtom,3))
            !xyz = FRC%atom%xyz
            !COB = dim/2
            !COM = sum(xyz,1)/size(xyz,1)
            !call bsxfunPlus(xyz,-COM+COB,xyz)
            !FRC%atom%xyz = xyz
        end if
        !write(*,*) "I'm in GetDomainInfo, before PW mode"
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("pwmode")) then
                PWmodeIsFind = .TRUE.
                exit
            end if
        end do
        PWmodeIsFind = .FALSE.
        !write(*,*) "I'm in GetDomainInfo, after PW mode"
        !write(*,*) PWmodeIsFind
        if (PWmodeIsFind) then
            !write(*,*) "I'm in GetDomainInfo, before reading PW mode"
            read(inputFromFile%NAV(ii)%value,"(i2)") pstatus
            !write(*,*) "I'm in GetDomainInfo, after reading PW mode"
            if (pstatus /= 0) then
                FRC%option%pwmode = .TRUE.
            else
                FRC%option%pwmode = .FALSE.
            end if
        else
            FRC%option%pwmode = .FALSE.
        end if
        
        if (FRC%option%pwmode) then
            write(*,*) "Error in GetDomainInfo.f90. FRC%option%pwmode should be false now."
            stop
            !call Plan_fft(FRC)
        end if
        !write(*,*) cgridnxIsFind
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("cgridnx")) then
                cgridnxIsFind = .TRUE.
                exit
            end if
        end do
        !write(*,*) "I'm in GetDomainInfo, after finding Nx."
        !write(*,*) cgridnxIsFind
        if (cgridnxIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") pstatus
            FRC%domain%cgridn(1) = pstatus
        end if
        !write(*,*) "I'm in GetDomainInfo, after finding Ny."
        !write(*,*) cgridnyIsFind
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("cgridny")) then
                cgridnyIsFind = .TRUE.
                exit
            end if
        end do
        if (cgridnyIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") pstatus
            FRC%domain%cgridn(2) = pstatus
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("cgridnz")) then
                cgridnzIsFind = .TRUE.
                exit
            end if
        end do
        !write(*,*) "I'm in GetDomainInfo, after finding Nz."
        !write(*,*)  cgridnzIsFind
        if (cgridnzIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") pstatus
            FRC%domain%cgridn(3) = pstatus
        end if
        !write(*,*) "I'm in GetDomainInfo. before NxNyNz."        
        if (.not. (cgridnxIsFind .and. cgridnyIsFind .and. cgridnzIsFind)) then
            if (.not.((.not. cgridnxIsFind) .and. (.not. cgridnyIsFind) .and. (.not. cgridnzIsFind))) then
                write(*,*) "Warning in GetDomainInfo.f90. Cgridn is not given completly. Lowres will be used."
            end if
            
            jj = 0
            do while(jj < nline)
                jj     = jj+1
                if (trim(inputFromFile%NAV(jj)%name) == trim("Lowres")) then
                    lowresIsFind = .TRUE.
                    exit
                end if
            end do
            if (lowresIsFind) then
                read(inputFromFile%NAV(jj)%value,*) FRC%domain%lowres
            else
                FRC%domain%lowres = 0.5D0
            end if
        end if
        
        !if (cgridnIsFind) then
        !    FRC%domain%cgridn = inputFromFile(ii)%value
        !    FRC%domain%lowres = 0.5
        !else if (.not. lowresIsFind) then
        !    FRC%domain%lowres = 0.5
        !else if (lowresIsFind) then
        !    FRC%domain%lowres = inputFromFile(jj)%value
        !end if
        !write(*,*) "I'm in GetDomainInfo, before lowres."

        if (lowresIsFind) then
            FRC%domain%cgridn = ceiling(dime/FRC%domain%lowres)
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("fgridnx")) then
                fgridnxIsFind = .TRUE.
                exit
            end if
        end do
        if (fgridnxIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") pstatus
            FRC%domain%fgridn(1) = pstatus
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("fgridny")) then
                fgridnyIsFind = .TRUE.
                exit
            end if
        end do
        if (fgridnyIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") pstatus
            FRC%domain%fgridn(2) = pstatus
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("fgridnz")) then
                fgridnzIsFind = .TRUE.
                exit
            end if
        end do
        if (fgridnzIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") pstatus
            FRC%domain%fgridn(3) = pstatus
        end if
        
        if (.not. (fgridnxIsFind .and. fgridnyIsFind .and. fgridnzIsFind)) then
            if (.not.((.not. fgridnxIsFind) .and. (.not. fgridnyIsFind) .and. (.not. fgridnzIsFind))) then
                write(*,*) "Warning in GetDomainInfo.f90. Fgridn is not given completly. Highres will be used."
            end if
            
            jj = 0
            do while(jj < nline)
                jj     = jj+1
                if (trim(inputFromFile%NAV(jj)%name) == trim("Highres")) then
                    highresIsFind = .TRUE.
                    exit
                end if
            end do
            if (highresIsFind) then
                read(inputFromFile%NAV(jj)%value,*) FRC%domain%highres
                FRC%domain%fgridn = ceiling(dime/FRC%domain%highres)
            else
                FRC%domain%fgridn = FRC%domain%cgridn
            end if
        end if
        
        !if (highresIsFind) then
    
        !jj = 0
        !do while(jj<=nline)
        !    jj     = jj+1
        !    if (trim(inputFromFile(jj)%name) == trim("highres")) then
        !        highresIsFind = .TRUE.
        !        exit
        !    end if
        !end do
        
        !if (fgridnIsFind) then
        !    FRC%domain%fgridn  = inputFromFile(ii)%value
        !    FRC%domain%highres = 0.5
        !else if (highresIsFind) then
        !    FRC%domain%highres = inputFromFile(jj)%value
        !    FRC%domain%fgridn  = ceiling(dim/FRC%domain%highres)
        !else if (.not. highresIsFind) then
        !    FRC%domain%fgridn  = FRC%domain%cgridn
        !    FRC%domain%highres = FRC%domain%highres
        !end if
        
        do ii = 1,3,1
            if (FRC%domain%fgridn(ii) < FRC%domain%cgridn(ii)) then
                FRC%domain%fgridn(ii) = FRC%domain%cgridn(ii)
            end if
        end do
        
        if (FRC%LCAO%status) then
            FRC%domain%fgridn  = FRC%domain%cgridn
        end if
        FRC%domain%next = FRC%domain%fgridn
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("boundaryConditionX")) then
                boundaryXIsFind = .TRUE.
                exit
            end if
        end do
        if (boundaryXIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%domain%boundary(1)
        else
            FRC%domain%boundary(1) = 1
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("boundaryConditionY")) then
                boundaryYIsFind = .TRUE.
                exit
            end if
        end do
        if (boundaryYIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%domain%boundary(2)
        else
            FRC%domain%boundary(2) = 1
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("boundaryConditionZ")) then
                boundaryZIsFind = .TRUE.
                exit
            end if
        end do
        if (boundaryZIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%domain%boundary(3)
        else
            FRC%domain%boundary(3) = 1
        end if
        
        
        
        !if (boundaryIsFind) then
        !    FRC%domain%boundary = inputFromFile(ii)%value
        !else
        !    FRC%domain%boundary = (/1,1,1/)
        !end if
        
        bc = FRC%domain%boundary
        
        if (any(bc/=1)) then
            write(*,*) "Error in GetDomainInfo.f90. The boundary condition must be periodical now."
            stop
            !sizeBC = 0
            !do ii = 1,3,1
            !    if (bc(ii)/=1) then
            !        sizeBC = sizeBC+1
            !    end if
            !end do
        
            !allocate(locateBC(sizeBC))
            !nn = 0
            !do ii = 1,3,1
            !    if (bc(ii)/=1) then
            !        nn = nn+1
            !        locateBC(nn) = ii
            !    end if
            !end do
        
            !allocate(tmp(2*sizeBC))
            !forall (ii=1:sizeBC)
            !    tmp(ii) = transpose(avec(:,locateBC(ii)))
            !end forall
            !forall (ii=1:sizeBC)
            !    tmp(ii+sizeBC) = avec(locateBC(ii),:)
            !end forall
            
            !if (sum(tmp)/=2) then
            !    write(*,*) "error"
            !end if
        end if
        
        !if (FRC%domain%boundary(1) ==0) then
        !    ii = 0
        !    do while(ii<=nline)
        !        ii     = ii+1
        !        if (trim(inputFromFile(ii)%name) == trim("bvalx")) then
        !            bvalxIsFind = .TRUE.
        !            exit
        !        end if
        !    end do
        !    if (.not. bvalxIsFind) then
        !        write(*,*) "error. You specified Dirichlet boundary condition along the x-direction, but you have not defined the value at the boundaries (domain.bvalx)."
        !    !else if (size(FRC%domain%bvalx)/=2) then
        !    !    write(*,*) "error. You specified Dirichlet boundary condition along the x-direction, and domain.bvalx is not of length 2."
        !    end if
        !end if
        
        !if (FRC%domain%boundary(2) ==0) then
        !    ii = 0
        !    do while(ii<=nline)
        !        ii     = ii+1
        !        if (trim(inputFromFile(ii)%name) == trim("bvaly")) then
        !            bvalyIsFind = .TRUE.
        !            exit
        !        end if
        !    end do
        !    if (.not. bvalyIsFind) then
        !        write(*,*) "error. You specified Dirichlet boundary condition along the y-direction, but you have not defined the value at the boundaries (domain.bvaly)."
        !    !else if (size(FRC%domain%bvaly)/=2) then
        !    !    write(*,*) "error. You specified Dirichlet boundary condition along the y-direction, and domain.bvaly is not of length 2."
        !    end if
        !end if
        
        !if (FRC%domain%boundary(3) ==0) then
        !    ii = 0
        !    do while(ii<=nline)
        !        ii     = ii+1
        !        if (trim(inputFromFile(ii)%name) == trim("bvalz")) then
        !            bvalzIsFind = .TRUE.
        !            exit
        !        end if
        !    end do
        !    if (.not. bvalzIsFind) then
        !        write(*,*) "error. You specified Dirichlet boundary condition along the z-direction, but you have not defined the value at the boundaries (domain.bvalz)."
        !    !else if (size(FRC%domain%bvalx)/=2) then
        !    !    write(*,*) "error. You specified Dirichlet boundary condition along the z-direction, and domain.bvalz is not of length 2."
        !    end if
        !end if
        
        !allocate(xyz(FRC%Atom%nAtom,3))
        !xyz = FRC%atom%xyz
        !call inversion(avec,avectmp)
        !xyz = matmul(mod(matmul(xyz,avectmp),1),avec)
        !do ii = 1,3,1
        !    if (bc(ii)==1) then
        !        FRC%atom%xyz(:,ii) = xyz(:,ii)
        !    end if
        !end do
        
        forall(ii=1:3)
            avectmp(ii,ii) = FRC%domain%latvec(ii,ii)
        end forall
        if (all(avectmp == FRC%domain%latvec)) then
            ortho = .TRUE.
        else
            ortho = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DiffopAccuracy")) then
                DiffopAccuracyIsFind = .TRUE.
                exit
            end if
        end do
        if (DiffopAccuracyIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%diffop%accuracy
        else
            FRC%diffop%accuracy = 16
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("DiffopMethod")) then
                DiffopMethodIsFind = .TRUE.
                exit
            end if
        end do
        if (DiffopMethodIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%diffop%method
        else
            if (all(FRC%domain%boundary > 0) .and. (.not. ortho)) then
                FRC%diffop%method = "fft"
            else if (all(FRC%domain%boundary > 0)) then
                FRC%diffop%method = "ft"
            else
                FRC%diffop%method = "fd"
            end if
        end if
        
        FRC%init%domaininfo = .true.
    end if
    
    return
    end subroutine GetDomainInfo
    end module GetDomainInfo_module
