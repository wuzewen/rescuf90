! initSubspaceNonscf.f90

!****************************************************************************************
      !
      ! PURPOSE : to provide subspace for CFSI in non-scf calculations.
      ! 
      ! 
      !
      !
!****************************************************************************************

      module initSubspaceNonscf_module
      contains
      subroutine initSubspaceNonscf(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use selectKpointIndex_module
      use ks_main_lcao_scf_module
      use GetAtomicOrbitalSubspace_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC

      ! temporary variables

      ! output variables

      ! body of this function
      ispin    = FRC%spin%ispin
      bandi    = FRC%eigensolver%bandi
      nband    = FRC%eigensolver%nband
      nkpt     = size(FRC%kpoint%ikdirect,1)
      allocate(kdir(nkpt,3))
      kdir     = FRC%kpoint%ikdirect
      call selectKpointIndex(FRC,kdir,inde)
      calctype = FRC%info%calculationType
      FRC%eigensolver%bandi(1) = 1
      FRC%eigensolver%bandi(2) = nband
      deallocate(FRC%kpoint%ikdirect)
      allocate(FRC%kpoint%ikdirect())
      FRC%info%calculationType = 'self-consistent'
      call ks_main_lcao_scf(FRC)
      FRC%info%calculationType = calctype
      coeff = FRC%LCAO%coeff
      FRC%LCAO%coeff = coeff
      FRC%kpoint%ikdirect = kdir
      FRC%eigensolver%bandi = bandi

      call GetAtomicOrbitalSubspace(FRC,.FALSE.)

      return
      end subroutine initSubspaceNonscf
      end module initSubspaceNonscf_module

