!  mixedPoissonSolver.f90 
!  this is a function solving poisson equation
    
!  FUNCTIONS:
!  poisson - Entry point of console application.
!
!****************************************************************************
!
!  PROGRAM: poisson
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module mixedPoissonSolver_module
    contains
    subroutine mixedPoissonSolver(FRC,rho,bc,VHout)

    !use Cell_3D_cmplx
    use ComplexVata_3D_module
    use triu_module
    use tril_module
    use inversion_module
    use repmat_module
    use getFrequency_3D_module
    !use circshift_module
    use permute_module
    use permute_cmplx_module
    use setBoundaryValue_module
    !use dst_module
    use fft3_module
    use fft3_fftw_module
    !use dct_module
    use bsxfunTimes_module
    use bsxfunTimes_3D_module
    use bsxfunTimes_3D_CMPLX_module
    use bsxfunPlus_module
    use bsxfunPlus_3D_module
    use bsxfunPlus_3D_CMPLX_module
    !use idst_module
    use ifft3_module
    use ifft3_fftw_module
    !use idct_module
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none

    ! input valuables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer                         :: bc(3)
    real*8                          :: lattice(3,3)
    real*8, allocatable             :: rho(:,:,:)
    
    ! temporery valuables
    integer              :: szrho(3), ntmp, tt(3)
    integer              :: ii, jj!, tmp(3)
    real*8, allocatable  :: j1(:,:), j2(:,:), j3(:,:), avec(:,:), VHtmp(:,:,:)
    real*8, allocatable  :: tmp2(:,:,:), tmp(:,:,:)
    !complex              :: ktmp(:,:) ! k(3,?)
    complex*16, allocatable :: VH(:,:,:), tmp1(:,:,:), rhotmp(:,:,:), rhoc(:,:,:)
    type(ComplexVata_3D)   :: k(3), ktmp(3)
    real*8               :: pi
    
    ! output valuables 
    real*8, allocatable    :: VHout(:)
    

    ! Body of poisson
    pi   = 3.1415926535897932385D0
    allocate(avec(3,3))
    avec = FRC%domain%latvec
    allocate(j1(3,3),j2(3,3),j3(3,3))
    j2   = transpose(avec)
    j2   = matmul(avec,j2)
    call inversion(j2,j1)
    j2   = 0.0D0
    j3   = 0.0D0
    call triu(j1, 0,j2)
    call tril(j1,-1,j3)
    j3 = transpose(j3)
    j2 = j2+j3
    !write(*,*) "j2 in mixedPossionSolver"
    !write(*,*)  j2
    !deallocate(j1,j3)
    
    
    szrho(1) = size(rho,1)
    szrho(2) = size(rho,2)
    szrho(3) = size(rho,3)

    !write(*,*) "mixedP, rho ="
    !do ii = 1,szrho(3),1
    !    do jj = 1,szrho(1),1
    !    write(*,*) rho(jj,:,ii)
    !    end do
    !end do
    
    !allocate(tmm())
    !call repmat(tmm,1,3,k)
    allocate(k(1)%vata(szrho(1),1,1))
    allocate(k(2)%vata(1,szrho(2),1))
    allocate(k(3)%vata(1,1,szrho(3)))
    do ii = 1,3,1
        call getFrequency_3D(szrho(ii),bc(ii),ktmp(ii)%vata)
        !if (ii == 1) then
        !        tt(1) = 1
        !        tt(2) = 2
        !        tt(3) = 3
        !else if (ii == 2) then
        !        tt(1) = 3
        !        tt(2) = 1
        !        tt(3) = 2
        !else if (ii == 3) then
        !        tt(1) = 2
        !        tt(2) = 3
        !        tt(3) = 1
        !end if
        !write(*,*) "I'm here after tt"
        !call circshift((/1,2,3/),(/0,ii-1/),tmp)
        !call permute_cmplx(ktmp(ii)%vata,tt,k(ii)%vata)
    end do
    k(1)%vata(:,1,1) = ktmp(1)%vata(:,1,1)
    k(2)%vata(1,:,1) = ktmp(2)%vata(:,1,1)
    k(3)%vata(1,1,:) = ktmp(3)%vata(:,1,1)
    !call getFrequency(nx,bc(2),k(2))
    !call getFrequency(nx,bc(3),k(3))
    !write(*,*) "K in mixedPossionSolver"
    !write(*,*)  k(1)
    !write(*,*)  k(2)
    !write(*,*)  k(3)
    !call setBoundaryValue(FRC,rho,bc,VH)
    call setBoundaryValue(FRC,rho,bc,FRC%Arr)
    !write(*,*) "VHtmp"
    !write(*,*)  VHtmp
    !allocate(VHtmp2(szrho(1),szrho(2),szrho(3)))
    !allocate(VH(szrho(1),szrho(2),szrho(3)))
    !VHtmp2 = dcmplx(VHtmp)
    !do ii = 1,3,1
    !    if (bc(ii)==0) then
    !        
    !        write(*,*) "Error in mixedPoissonSolver.f90. bc = 0 is not available."
    !        stop
    !        
    !        !call dst(VHtmp,ii,VH)
    !    else if (bc(ii)==1) then
    !        call fft3(VHtmp2,ii,VH)
    !    else if (bc(ii)==2) then
    !        
    !        write(*,*) "Error in mixedPoissonSolver.f90. bc = 2 is not available."
    !        stop
    !        
    !        !call dct(VHtmp,ii,VH)
    !    end if
    !    VHtmp2 = VH
    !end do
    call fft3_fftw(FRC)
    !call fft3_fftw(FRC,VH)
    !call fft3_fftw(szrho(1),szrho(2),szrho(3),VH)
    !VHtmp2 = VH

    rho  = 0.0D0
    allocate(rhoc(szrho(1),szrho(2),szrho(3)))
    rhoc = dcmplx(rho)
    allocate(rhotmp(szrho(1),szrho(2),szrho(3)))
    do ii = 1,3,1
        do jj = ii,3,1
            if (j2(ii,jj) /= 0) then
                allocate(tmp1(max(size(k(jj)%vata,1),size(k(ii)%vata,1)),max(size(k(jj)%vata,2), &
                         size(k(ii)%vata,2)),max(size(k(jj)%vata,3),size(k(ii)%vata,3))))
                call bsxfunTimes_3D_cmplx(k(jj)%vata,k(ii)%vata,tmp1)
                tmp1 = tmp1*dcmplx(J2(ii,jj))
                call bsxfunPlus_3D_CMPLX(rhoc,tmp1,rhotmp)
                rhoc = rhotmp
                deallocate(tmp1)
            end if
        end do
    end do
    
    FRC%Arr = -dcmplx(4.0D0*pi)*FRC%Arr/rhotmp
    !VH = -dcmplx(4.0D0*pi)*VH/rhotmp
    if (.not. any(bc==0)) then
            FRC%Arr(1,1,1) = dcmplx(0.0D0)
        !VH(1,1,1) = dcmplx(0.0D0)
    end if
    !VHtmp2 = cmplx(VHtmp)
    !do ii = 1,3,1
    !    if (bc(ii)==0) then
    !        !call idst(VHtmp,ii,VH)
    !    else if (bc(ii)==1) then
    !        call ifft3(VHtmp2,ii,VH)
    !    else if (bc(ii)==2) then
    !        !call idct(VHtmp,ii,VH)
    !    end if
    !    VHtmp2 = VH
    !end do
    call ifft3_fftw(FRC)
    !call ifft3_fftw(FRC,VH)
    !call ifft3_fftw(szrho(1),szrho(2),szrho(3),VH)
    !VHtmp2 = VH


    ntmp  = product(szrho)
    allocate(VHout(ntmp))
    VHout = dble(reshape(FRC%Arr,(/ntmp/)))
    
    return

    end subroutine mixedPoissonSolver
    end module mixedPoissonSolver_module
