! smiDTRMM.f90

!*******************************************
      !
      !
      !
!*******************************************************

      module smiDTRMMMOD
      contains
      subroutine smiDTRMM(side,uplo,trans,alpha,A,C,nprocs,&
                      MA,NA,MBA,NBA,nprowA,npcolA,MC,NC,MBC,NBC,nprowC,npcolC)

      implicit none

      integer :: MA, NA, MBA, NBA, nprowA, npcolA
      integer :: MC, NC, MBC, NBC, nprowC, npcolC
      character(len=1) :: side, uplo, trans
      real*8 :: alpha, beta

      character(len=1) :: diag
      integer :: iam, ictxtA, ictxtC, myrowA, mycolA, myrowC, mycolC

      diag    = 'n'
      iam     =  0
      ictxtA  =  0
      ictxtC  =  0
      myrowA  =  0
      mycolA  =  0
      myrowC  =  0
      mycolC  =  0
      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxtA)
      call blacs_gridinit(ictxtA,'Row-major',nprowA,npcolA)
      call blacs_gridinfo(ictxtA,nprowA,npcolA,myrowA,mycolA)
      call blacs_get(-1,0,ictxtC)
      call blacs_gridinit(ictxtC,'Row-major',nprowC,npcolC)
      call blacs_gridinfo(ictxtC,nprowC,npcolC,myrowC,mycolC)

      izero = 0
      ione  = 1
      call numroc(MA,MBA,myrowA,izero,nprowA,A_np)
      call numroc(MC,MBC,myrowC,izero,nprowC,C_np)
      A_np = 
      C_np = 

      call descinit(descA,MA,NA,MBA,NBA,izero,izero,ictxtA,A_np,infoA)
      call descinit(descC,MC,NC,MBC,NBC,izero,izero,ictxtC,C_np,infoC)

      call PDTRMM(side,uplo,trans,diag,MC,NC,alpha,A,ione,ione,descA,&
              C,ione,ione,descC)
      call blacs_gridexit(ictxtA)
      call blacs_gridexit(ictxtC)

      return
      end subroutine smiDTRMM
      end module smiDTRMMMOD
