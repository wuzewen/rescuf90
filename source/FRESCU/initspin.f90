! initspin.f90
!********************************
    !
!********************************
    
    module initspin_module
    contains
    subroutine initspin(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    integer :: nline
    integer :: ii, natom, ll, mm, status
    logical :: spinTypeIsFind, magmomIsFind, SOIisFind, magmomcartIsFind, magmomdIsFind, IspinIsFind
    real*8, allocatable  :: magcart(:,:), r(:), theta(:), phi(:)
    real*8  :: tmp(3)
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%spin) then
        spinTypeIsFind   = .FALSE. 
        magmomIsFind     = .FALSE.
        SOIisFind        = .FALSE.
        magmomcartIsFind = .FALSE.
        magmomdIsFind    = .FALSE.
        IspinIsFind      = .FALSE.
        natom = FRC%atom%numberOfAtom
        nline = inputFromFile%NumberofInput
        !FRC%spin%ispin    = 1
        !FRC%spin%SOI      = .FALSE.
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("spinType")) then
                spinTypeIsFind = .TRUE.
                exit
            end if
        end do
        if (spinTypeIsFind) then
            !FRC%spin%spinType = inputFromFile(ii)%value
            read(inputFromFile%NAV(ii)%value,*) FRC%spin%spinType
        else
            FRC%spin%spinType = "degenerate"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("Ispin")) then
                IspinIsFind = .TRUE.
                exit
            end if
        end do
        if (IspinIsFind) then
            !FRC%spin%spinType = inputFromFile(ii)%value
            read(inputFromFile%NAV(ii)%value,*) FRC%spin%Ispin
        else
            FRC%spin%Ispin = 1
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("SOI")) then
                SOIIsFind = .TRUE.
                exit
            end if
        end do
        if (SOIIsFind) then
            !FRC%spin%spinType = inputFromFile(ii)%value
            read(inputFromFile%NAV(ii)%value,*) status
            if (status /= 0) then
                FRC%spin%SOI = .TRUE.
            else
                FRC%spin%SOI = .FALSE.
            end if
        else
            FRC%spin%SOI = .FALSE.
        end if
        
        if (trim(FRC%spin%spinType) == trim("degenerate")) then
            FRC%spin%spinType = "degenerate"
            FRC%spin%ispin    = 1
            FRC%spin%nspin    = 1
        else if (trim(FRC%spin%spinType) == trim("collinear")) then
            FRC%spin%spinType = "collinear"
            FRC%spin%ispin    = 2
            FRC%spin%nspin    = 2
            
            Write(*,*) "Error in initspin.f90. collinear spin is not available now."
            stop
            !ii = 0
            !do while(ii<=nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile(ii)%name) == trim("magmom")) then
            !        magmomIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (magmomIsFind) then
            !    FRC%spin%magmom = inputFromFile(ii)%value
            !else
            !    forall(ii=1:natom)
            !        FRC%spin%magmom(ii) = 1/(natom*natom*0.25)
            !    end forall
            !end if
        else if (trim(FRC%spin%spinType) == trim("non-collinear")) then
            FRC%spin%spinType = "non-collinear"
            FRC%spin%ispin    = 4
            FRC%spin%nspin    = 1
        else
            write(*,*) "Error in initspin.f90. Invalid spin type."
            stop
        end if
        
        !ii = 0
        !do while(ii < nline)
        !    ii     = ii+1
        !    if (trim(inputFromFile%NAV(ii)%name) == trim("SOI")) then
        !        SOIisFind = .TRUE.
        !        exit
        !    end if
        !end do
        !if (SOIisFind) then
        !    FRC%spin%SOI = inputFromFile(ii)%value
        !else
        !    FRC%spin%SOI = .FALSE.
        !end if
        
        if (trim(FRC%spin%spinType) /= trim("non-collinear")) then
            FRC%spin%SOI = .FALSE.
        end if
        
        if (FRC%spin%SOI) then
            write(*,*) "Error in initspin.f90. SOI is not available now."
            stop
            !do ii = 1,natom,1
            !    call AddSOI(FRC%element(ii)%VnlSO)
            !end do
        end if
        
        if (FRC%spin%ispin == 4) then
            
            write(*,*) "Error in initspin.f90. ispin should be 1 now."
            stop
            !ii = 0
            !do while(ii<=nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile(ii)%name) == trim("magmomcart")) then
            !        magmomcartIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (magmomcartIsFind) then
            !    FRC%atom%magmomcart = inputFromFile(ii)%value
            !end if
            
            !magmomIsFind = .FALSE.
            !ii = 0
            !do while(ii<=nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile(ii)%name) == trim("magmom")) then
            !        magmomIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (magmomIsFind) then
            !    FRC%atom%magmom = inputFromFile(ii)%value
            !end if
            
            !ii = 0
            !do while(kk<=nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile(ii)%name) == trim("magmomd")) then
            !        magmomdIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (magmomdIsFind) then
            !    FRC%atom%magmomd = inputFromFile(ii)%value
            !end if
           ! 
            !if (magmomcartIsFind) then
            !    allocate(magcart(FRC%atom%numberOfAtom,3))
            !    allocate(r(FRC%atom%numberOfAtom))
            !    allocate(theta(FRC%atom%numberOfAtom))
            !    allocate(phi(FRC%atom%numberOfAtom))
            !    magcart              = inputFromFile(ii)%value
            !    r                    = sqrt(sum(magmart*magcart,2))
            !    theta                = acos(magcart(:,3)/r)
            !    phi                  = atan2(magcart(:2),magcart(:,1))
            !    FRC%atom%magmom(:,1) = r
            !    FRC%atom%magmom(:,2) = theta
            !    FRC%atom%magmom(:,3) = phi
            !    tmp(1)               = 1
            !    tmp(2)               = 180/pi
            !    tmp(3)               = 1/pi
            !    call bsxfunTimes(FRC%atom%magmom,tmp,FRC%atom%magmomd)
            !else if ((.not. magmomIsFind) .and. (.not. magmomdIsFind)) then
            !    do ll = 1,natom,1
            !        do mm = 1,3,1
            !            call random_number(magtmp(ll,mm))
            !        end do
            !    end do
            !    tmp(1)               = 1
            !    tmp(2)               = pi
            !    tmp(3)               = 2*pi
            !    call bsxfunTimes(magtmp,tmp,FRC%atom%magmom)
            !    tmp(1)               = 1
            !    tmp(2)               = 180/pi
            !    tmp(3)               = 1/pi
            !    call bsxfunTimes(FRC%atom%magmom,tmp,FRC%atom%magmomd)
            !else if ((.not. magmomIsFind) .and. magmomdIsFind) then
            !    tmp(1)               = 1
            !    tmp(2)               = 180/pi
            !    tmp(3)               = 1/pi
            !    call bsxfunTimes(FRC%atom%magmomd,tmp,FRC%atom%magmom)
            !else if (magmomIsFind) then
            !    tmp(1)               = 1
            !    tmp(2)               = 180/pi
            !    tmp(3)               = 1/pi
            !    call bsxfunTimes(FRC%atom%magmom,tmp,FRC%atom%magmomd)
            !else
            !    write(*,*) "error"
            !end if
        end if
        
        FRC%init%spin = .TRUE.
    end if
    
    return
    end subroutine initspin
    end module initspin_module
                
