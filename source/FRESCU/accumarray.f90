! accumarray.f90
    
!******************************************************************
    !
    !
    !
!******************************************************************
    
    module accumarray_module
    contains
    subroutine accumarray(A,val,ou)
    
    implicit none
    
    ! input variables
    integer, allocatable :: A(:)
    real*8               :: val
    
    ! temporary variables
    integer :: n, ii, jj, ntmp
    integer, allocatable :: tmp1(:), tmp2(:)
    
    ! output variables
    real*8,  allocatable :: ou(:)
    
    ! body of this function
    n = size(A)
    
    allocate(tmp1(1))
    tmp1 = A(1)
    ntmp = 1
    do ii = 2,n,1
        if (all(tmp1 /= A(ii))) then
            ntmp = ntmp+1
            allocate(tmp2(ii))
            forall(jj=1:ntmp-1)
                tmp2(jj) = tmp1(jj)
            end forall
            tmp2(ntmp) = A(ii)
            deallocate(tmp1)
            allocate(tmp1(ntmp))
            tmp1 = tmp2
            deallocate(tmp2)
        end if
    end do
    
    allocate(ou(ntmp))
    do ii = 1,ntmp,1
        ou(ii) = val*dble(count(A == tmp1(ii)))
    end do
    
    return
    end subroutine accumarray
    end module accumarray_module
