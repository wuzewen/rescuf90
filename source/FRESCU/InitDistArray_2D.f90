! InitDistArray_2D.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    subroutine InitDistArray_2D(M,N,MB,NB,MP,NP,A)
    
    use VataMN_2D_module
    
    implicit none
    
    ! input variables
    integer      :: M, N, MB, NB, MP, NP
    
    ! ouput variables
    type(VataMN_2D) :: A
    
    ! body of this function
    A%m      = M
    A%n      = n
    A%mblock = MB
    A%nblock = NB
    A%mproc  = MP
    A%nproc  = NP
    
    return
    end subroutine InitDistArray_2D