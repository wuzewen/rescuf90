! msb2.f90

!******************************************
      !
      !
      !
!***********************************************

      module msb2Mod
      contains
      subroutine msb2(FRC,mu_m,vu_m,mu_mp1)

      use 

      implicit none

      mpistat  = FRC%mpi%status
      iter     = FRC%scloop
      alpha    = FRC%mixing%alpha
      betamix  = FRC%mixing%beta
      initlin  = FRC%mixing%initlin
      betalin  = FRC%mixing%betalin
      invJ0    = FRC%mixing%invJ0
      ipScheme = FRC%mixing%ipScheme
      maxh     = FRC%mixing%maxhistory
      R        = FRC%mixing%R
      mu_0     = mu_m
      F_m      = vu_m - mu_m
      mu_mm1   = FRC%mixing%mu%mm1
      F_mm1    = FRC%mixing%F_mm1
      MF_mm1   = FRC%mixing%MF_mm1

      if (iter .LE.) then
              mu_mp1 = betalin*F_m
              if (trim(invJ0) .EQ. 'kerker') then
                      call mixingMetricWrapper(FRC,mu_0,mu_mp1)
              end if
              mu_mp1 = mu_m + mu_mp1
              MF_m   = F_m
              if (trim(ipScheme) .EQ. 'nonuniform') then
                      call mixingMetricWrapper(FRC,mu_0,F_m,MF_m)
              end if
              sig = betamix*0.1D0
              NormF = dble(matmul(transpose(conjg(F_m)),MF_m))
              if (mpistat) then
                      call MPI_Allreduce_sum(normF)
              end if
      else
              normD_mm1 = FRC%mixing%normF_mm1
              sig_mm1   = FRC%mixing%sig_mm1
              MF_m      = F_M
              if (trim(ipScheme) .EQ. 'nonuniform') then
                      call mixingMetricWrapper(FRC,mu_0,F_m,MF_m)
              end if
              call bsxfunMinus(MF_mm1,MF_m,MY)
              call bsxfunMinus(F_mm1,F_m,Y)
              normF = dble(matmul(transpose(conjg(F_m)),MF_m))
              yty = transpose(conjg(y))*MY
              if (mpistat) then
                      call MPI_Allreduce_sum(yty)
              end if
              yty = 0.5D0*(yty+transpose(conjg(yty)))
              forall(ii=1:)
                      psi(ii,ii) = 1.0D0/sqrt(yty(ii))
              end forall
              call bsxfunTimes(yty,conjg(psi),yty)
              call bsxfunTimes(yty,psi,yty)
              call bsxfunTimes(MY,conjg(psi),MY)
              call bsxfunTimes(Y,conjg(psi),Y)
              call bsxfunMinus(mu_mm1,mu_m,s)
              call bsxfunTimes(s,conjg(psi),s)
              tmp = matmul(conjg(MY),F_m)
              if (mpistat) then
                      call MPI_Allreduce_sum(tmp)
              end if
              AMAT = yty
              forall(ii=1:)
                      AMAT(ii,ii) = AMAT(ii,ii)+alpha
              end forall
              call inverion()
              AMAT    = matmul(AMAT,tmp)
              mu_mp1  = mu_m
              tmp     = -matmul(s,AMAT)
              !Mtmp    = tmp
              if (trim(ipScheme) .EQ. 'nonuniform') then
                      call mixingMetriceWrapper(FRC,mu_0,tmp,Mtmp)
              else 
                      Mtmp = tmp
              end if
              normp = dble(matmul(transpose(conjg(tmp)),Mtmp))
              if (mpistat) then
                      call MPI_Allreduce_sum(normp)
              end if
              normp = sqrt(normp)
              mu_mp1 = mu_mp1+tmp
              tmp = -(F_m-matmul(y,AMAT))
              if (trim(ipScheme) .EQ. 'nonuniform') then
                      call mixingMetriceWrapper(FRC,mu_0,tmp,Mtmp)
              end if
              normu = dble(matmul(transpose(conjg(tmp)),Mtmp))
              if (mpistat) then
                      call MPI_Allreduce_sum(normu)
              end if
              if (trim(invJ0) .EQ. 'kerker') then
                      call mixingMetriceWrapper(FRC,mu_0,tmp,tmp)
              end if
              sig = min(betamix,R*normu/normF)
              sig = min(sig,sig_mm1*max(0.5D0,min(2.0D0,normF_mm1/normF)))
              mu_mp1 = mu_mp1 - sig*tmp
      end if

      if (iter .GT. maxh) then
              forall(ii=1:-1)
                      mu_mm1(:,ii) = mu_mm1(:,ii+1)
                      F_mm1(:,ii)  = F_mm1(:,ii+1)
                      MF_mm1(:,ii) = MF_mm1(:,ii+1)
              end forall
              mu_mm1(:,) = mu_m
              F_mm1(:,)  = F_m
              MF_mm1(:,) = MF_m
      else
              mu_mm1(:,iter) = mu_m
              F_mm1(:,iter)  = F_m
              MF_mm1(:,iter) = MF_m
      end if

      FRC%mixing%mu_mm1    = mu_mm1
      FRC%mixing%F_mm1     = F_mm1
      FRC%mixing%MF_mm1    = MF_mm1
      FRC%mixing%sig_mm1   = sig
      FRC%mixing%normF_mm1 = normF

      mu_mp1 = reshape(mu_mp1,(//))

      return
      end subroutine msb2
      end module msb2Mod
