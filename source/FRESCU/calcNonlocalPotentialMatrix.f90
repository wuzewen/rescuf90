! calcNonlocalPotentialMatrix.f90

!****************************************************************************
      !
      !
      !
!****************************************************************************

      module calcNonlocalPotentialMatrixMod
      contains
      subroutine calcNonlocalPotentialMatrix(FRC)

      implicit none

      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize
      sprs    = FRC%LCAO%sprs
      buff    = min(2**26,FRC%option%bufferSize)
      avec    = FRC%domain%latvec
      smimb   = FRC%smi%mb
      sminb   = FRC%smi%nb
      smimp   = FRC%smi%mp
      sminp   = FRC%smi%np
      xyz     = FRC%atom%xyz

      call GetAtomicOrbitalInfo(FRC)

      evec1 = FRC%LCAO%parameters%evec
      Lorb1 = FRC%LCAO%parameters%Lorb
      Morb1 = FRC%LCAO%parameters%Morb
      Oorb1 = FRC%LCAO%parameters%Oorb
      Rorb1 = FRC%LCAO%parameters%Rorb
      Sorb1 = FRC%LCAO%parameters%Sorb
      Norb1 = size(Lorb1)

      call GetKBOrbitalInfo(FRC)

      evec2 = FRC%LCAO%parameters%evec
      Lorb2 = FRC%LCAO%parameters%Lorb
      Morb2 = FRC%LCAO%parameters%Morb
      Oorb2 = FRC%LCAO%parameters%Oorb
      Rorb2 = FRC%LCAO%parameters%Rorb
      Sorb2 = FRC%LCAO%parameters%Sorb
      Norb2 = size(Lorb2)

      call booleOverlapO2(FRC,xyz,evec1,Rorb1,evec2,Rorb2,sbDisp,txyz)
      nt = size(txyz,1)

      call GetGlobalInd(mpirank,sbDisp(1),iloc,jloc)

      allocate(iRorb(size(iloc)))
      forall(ii=1:size(iloc))
              iRorb(ii) = Rorb1(iloc(ii))
      end forall
      allocate(jRorb(size(jloc)))
      forall(ii=1:size(jloc))
              jRorb(ii) = Rorb1(jloc(ii))
      end forall

      lmax(1) = maxval(Lorb1)
      lmax(2) = maxval(Lorb2)

      call genGauntMatrix(lmax,gauntmat)

      do ii = 1,size(FRC%ElementData),1
          do jj = 1,size(FRC%ElementData(ii)%OrbitalSet)
              do kk = 1,size(FRC%ElementData),1
                  do ll = 1,size(FRC%ElementData(ii)%OrbitalSet)
                      fqmat2(:,ii,jj,kk,ll) = & 
                             FRC%ElementData(ii)%OrbitalSet(jj)%fqData &
                            *FRC%ElementData(kk)%OrbitalSet(ll)%fqData
                  end do
              end do
          end do
      end do

      fqmat2 = reshape(fqmat2,(//))

      q = FRC%ElementData(1)%OrbitalSet(1)%qqData
      w = FRC%ElementData(1)%OrbitalSet(1)%qwData
      maxrad = (maxval(Rorb1)+maxval(Rorb2))*maxval(q)*1.05D0
      call linspace(0,maxrad,ceiling(maxrad/0.05D0),juvrad)
      allocate(juvval(2*lmax,size(juvrad)))
      juvval = 0.0D0

      do Luv = 0,2*lmax,1
          call calcSphericalBessel(Luv,juvrad)
          juvval(Luv+1,:)
      end do

      sprsMat = norb1*norb2*16*2/mpisize .GT. buff

      do ll = 1,nt,1
          kb = sbDist(ll)
          allocate(xyz1(size(iloc),3))
          allocate(xyz2(size(jloc),3))
          forall(ii=1:size(iloc))
                  xyz1(ii,:) = xyz(evec1(iloc(ii)),:)
          end forall
          forall(ii=1:size(jloc))
                  xyz2(ii,:) = xyz(evec2(jloc(ii)),:)
          end forall
          call bsxfunPlus()

          call calcLocalBooleanOverlap(xyz1,xyz2,matmul(txyz(ll,:),avec),slog)
          if (sprsMat) then
                  KB%vata = 
          else
                  KB%vata = 
          end if

          if (nnz(slog)) then
              call find(slog,fslogi,flogj)
              blks = ceiling(buff/size(q))
              kk   = 1
              do while (kk .LE. size(fslogi))
                  if (kk+blks .GT. size(fslogi)) then
                          blks = size(fslogi)-kk
                  end if
                  allocate(slogi(blks),slogj(blks))
                  forall(ii=1:blks)
                          slogi(ii)    = fslogi(kk+ii-1)
                          slogj(ii)    = fslogj(kk+ii-1)
                          rijmat(ii,:) = xyz1(sligi(ii),:)-xyz2(slogj(ii),:)
                          dijmat(ii)   = sqrt(sum(rijmat(ii,:)))
                  endforall
                  do Luv = 0,2*lmax,1
                      allocate(lu(blks),lv(blks))
                      allocate(mu(blks),mv(blks))
                      forall(ii=1:blks)
                              lu(ii) = Lorb1(iloc(slogi(ii)))
                              lv(ii) = Lorb2(jloc(slogj(ii)))
                              mu(ii) = Morb1(iloc(slogi(ii)))
                              mv(ii) = Morb2(jloc(slogj(ii)))
                      end forall

                      do ii = 1,blks,1
                          if (Luv .GE. abs(lu(ii)-lv(ii)) &
                                  Luv .LE. lu(ii)+lv(ii)  &
                                  Luv .GE. abs(mu(ii)-mv(ii))) then
                              slogtmp(ii) = .TRUE.
                          else
                              slogtmp(ii) = .FALSE.
                          end if
                      end do

                      if (nnz(slogtmp)) then
                          ntmp = count(slogtmp .EQV. .TRUE.)
                          allocate(lutmp(ntmp),lvtmp(ntmp))
                          allocate(mutmp(ntmp),mvtmp(ntmp))
                          allocate(rij(ntmp))
                          ntmp = 0
                          do ii = 1,blks,1
                              if (slogtmp(ii)) then
                                  ntmp = ntmp+1
                                  lutmp(ntmp) = lu(ii)
                                  lvtmp(ntmp) = lv(ii)
                                  mutmp(ntmp) = mu(ii)
                                  mvtmp(ntmp) = mv(ii)
                                  rij(ntmp,:) = rijmat(ii,:)
                              end if
                          end do
                          call sub2ind(size(guantmat),lu+1,lv+1,mu+lu+1,&
                                  mv+lv+1,Luv-abs(lu-lv)+1)
                          gaunt = guantmat(gaunt)
                          call getsphHarm(Luv,mv-mu,rij,sphHarm)
                          gaunt = im**(lu-lv+Luv)*guant*shpHarm
                          call interp1()
                          call sub2ind()
                          temp = fqmat2()*juv
                          call sub2ind()
                          if (sprsMat) then
                              do ii = 1,blks,1
                                  if (slogtmp(ii)) then
                                      kb%vata(kk+ii-1) = kb%vata(kk+ii-1)&
                                              gaunt(ii)*temp(ii)*q(ii)**2*w(ii)
                                  end if
                              end do
                          else
                              ztemp(lind) = guant*temp*q**2*w
                              kb%vata     = kb%vata+ztemp
                              ztemp(lind) = 0
                          end if
                      end if
                  end do
                  kk = kk+blks+1
              end do
          end if
          if (sprsMat) then
              if (.not. nnz(slog)) then
                  call find(slog,fslogi,fslogj)
              end if
              sparse
          end if
          call adjustSparsity(kb%vata,sprs,kb%vata)
          kbcell(ll) = kb
      end do

      if (all(FRC%Kpoint%kdirect .EQ. 0.0D0)) then
          spfrac = 0.0D0
          do ii = 1,size(kbcell),1
              call diatmat_nnz(FRC,kbcell(ii),nonzero)
              spfrac = max(spfrac,nonzero/kbcell(ii)%m/kbcell(ii)%n)
          end do
          issp = spfrac .LT. 0.05 .AND. kbcell(ii)%m*kbcell(ii)%n .GT. 10**6
          if (issp) then
              isspn = cellsum(kbcell)
          else
              isspn = -1
          end if
          call reduceOper(kbcell,txyz,(/0,0,0/),issp,tmp)
          kbcell = tmp
          txyz = (/0,0,0/)
      end if

      if (mpistat) then
              call MPI_Barrier()
      end if

      do ii = 1,size(kbcell),1
          call distmat_nnz(FRC,kbcell(ii),nonzero)
          if (nonzero .GT. 0) then
              tmp = kbcell(ii)
              spfrac = nonzero/tmp%m/tmp%n
              issp = spfrac .LT. 0.05D0 .AND. tmp%m*tmp%n .GT. 10**6
              if (issp) then
                  sparse
              else
                  full
              end if
              call ModBCDist(FRC,tmp,smimb,sminb,1,mpisize,.TRUE.,issp)
              call sph2real(tmp%vata,Morb1,'l')
              call ModBCDist(FRC,tmp,smimb,sminb,mpisize,1,.TRUE.,issp)
              call sph2real(tmp%vata,Morb2,'r')
              tmp%vata = tmp%vata*4.0D0*pi
              call cropArray(tmp%vata,eps)
              call ModBCDist(FRC,tmp,smimb,sminb,smimp,sminp,.FALSE.,issp)
              call adjustSparsity(tmp%vata,sprs)
              kbcell(ii) = tmp
          else
              delet
          end if
      end do

      allocate(sbt(size(sbDisp),3))

      call cellfunnnz(sbDisp,spnnz)
      spnnz = maxval(spnnz)
      spnum = size(sbDisp(1)%vata)
      if (spnnz/spnum .GE. 0.2D0) then
          spnnz = -1
      end if

      call reduceOper(sbDisp,sbt,(//0,0,0),spnnz,sbk)
      call distmat_zeroslike(FRC,sbk,(/Norb1,Norb1/),tmp)
      call kbProj2Vnl(FRC,tmp,sbk,sbk,ones,tmp)
      FRC%LCAO%kbMask = logical(tmp)

      call sprececell(FRC,kbcell,idel)
      deallocate()
      
      FRC%LCAO%kbcell = kbcell
      FRC%LCAO%kbtv   = txyz


      return
      end subroutine calcNonlocalPotentialMatrix
      end module calcNonlocalPotentialMatrixMod
