! cfsiKpt.f90

!************************************************************
      !
      !
      !
!************************************************************

      module cfsiKpt_module
      contains
      !subroutine cfsiKpt(FRC,kpt,ss,c,e,X,prekin,iscomplex,waveFun)
      subroutine cfsiKpt(FRC,kpt,ss,c,e,X,prekin,iscomplex)
      
      use FORTRAN_RESCU_CALCULATION_TYPE
      use distmat_zeroslike_CMPLX_2D_module
      use distmat_zeroslike_CC_2D_module
      use GenHamFun_module
      use bsxfunTimes_CMPLX_module
      use calcKineticPrecond_module
      use calcFilterTarget_module
      use chebyshevFilter_module
      use modBCDist_CMPLX_2D_module
      use applyKineticPrecond_module
      use GetGlobalInd_CMPLX_2D_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      logical :: iscomplex
      integer :: ss
      real*8  :: kpt(3), c, e
      type(VataMN_CMPLX_2D) :: X
      !complex*16, allocatable :: X(:,:)
      real*8    , allocatable :: prekin(:,:,:)

      ! temporary variables
      logical :: adapCF
      integer :: mpisize, buff, cn, maxit, nband, precond
      integer :: sizeofc, ntm(2), blks, ll, ntmp, ii
      integer :: jj, cc, kk, spin
      real*8  :: dr, tol, avec(3,3), LU(2)
      integer, allocatable :: colvec(:), ia(:,:), ja(:,:)
      real*8 , allocatable :: LB(:,:), UB(:,:)
      real*8 , allocatable :: resreal(:,:)!, ev(:,:)
      complex*16, allocatable :: res(:,:), cfdeg(:,:), restmp(:,:)
      complex*16, allocatable :: tmp(:,:), psivata(:,:)
      complex*16, allocatable :: tmp2(:,:), nonsense(:,:), hamu(:,:)
      complex*16, allocatable :: hamd(:,:), ev(:,:)
      type(VataMN_CMPLX_2D)   :: psitmp, psi, waveFun, psitmp2, psi2

      character(len=20)    :: flax, numb
      integer :: cou

      ! output variables

      ! body of this function
      !write(numb,"(i4.4)") FRC%mpi%rank
      !flax = 'cfsiKpt'//trim(numb)//'.txt'
      !cou  =  FRC%mpi%rank + 10
      !open(unit=cou,file=flax)
      !write(cou,*) 'in cfsiKpt.'
      !write(cou,*) 'X', X%m, X%n, X%mblock, X%nblock, X%mproc, X%nproc
      !write(cou,*) 'X', size(X%vata,1),size(X%vata,2)
      !write(cou,*)  X%vata
      !close(cou)
      mpisize = FRC%mpi%mpisize
      buff    = FRC%option%bufferSize
      cn      = product(FRC%domain%cgridn)
      maxit   = FRC%eigenSolver%maxit
      nband   = FRC%eigenSolver%nband
      avec    = FRC%domain%latvec
      dr      = avec(1,1)*avec(2,2)*avec(3,3)+avec(1,2)*avec(2,3)*avec(3,1)+avec(1,3)*avec(2,1)*avec(3,2) &
               -avec(1,3)*avec(2,2)*avec(3,1)-avec(1,1)*avec(2,3)*avec(3,2)-avec(1,2)*avec(2,1)*avec(3,3)
      tol     = min(FRC%eigensolver%tol(1),FRC%eigensolver%tol(2))
      tol     = min(tol,FRC%mixing%dRho/10.0D0)
      precond = FRC%eigenSolver%precond
      adapCF  = FRC%eigenSolver%adapCFD

      if (iscomplex) then
              sizeofc = 16
      else
              sizeofc = 8
      end if

      blks    = ceiling(dble(buff)/dble(cn)/dble(sizeofc))
      blks    = blks*mpisize

      allocate(colvec(nband))
      do ii = 1,nband,1
          colvec(ii) = floor((dble(ii)-(1.0D-12))/dble(blks))+1
      end do

      !write(*,*) "in cfsiKpt, precond="
      !write(*,*)  precond

      allocate(psi%vata(size(X%vata,1),ntm(2)))
      !allocate(tmp(size(X%vata,1),size(X%vata,2)))
      !allocate(ev(size(X%vata,1),size(X%vata,2)))
      !allocate(res(size(X%vata,1),size(X%vata,2)))
      !allocate(restmp(size(X%vata,1),size(X%vata,2)))
      !allocate(psitmp%vata(size(X%vata,1),size(X%vata,2)))
      !allocate(psitmp%vata(X%m,X%n))
      call InitDistArray_CMPLX_2D(X%m,X%n,1,1,1,mpisize,psitmp)
      call GetGlobalInd_CMPLX_2D(FRC%mpi%rank,psitmp,ia,ja)
      allocate(psitmp%vata(size(ia),size(ja)))
      !write(cou,*) 'ia ja ='
      !write(cou,*)  ia
      !write(cou,*)  ja
      !write(*,*)  colvec
      allocate(ev(size(ia),size(ja)))
      allocate(res(size(ia),size(ja)))
      allocate(tmp(size(ia),size(ja)))
      do ll = 1,maxval(colvec),1
          ntm(1)  = cn
          ntm(2)  = count(colvec == ll)
          call distmat_zeroslike_CC_2D(FRC,X,ntm,psitmp2)
          !allocate(psi%vata(size(waveFun%vata,1),ntm(2)))
          ntmp  = 0
          !write(*,*) 'in cfsiKpt, size of X,',size(X%vata,1),size(X%vata,2)
          do ii = 1,nband,1
              if (colvec(ii) == ll) then
                  ntmp = ntmp + 1
                  psitmp2%vata(:,ntmp) = X%vata(:,ii)
              end if
          end do
          call ModBCDist_CMPLX_2D(FRC,psitmp2,1,1,1,mpisize,.FALSE.,.FALSE.,psitmp)
          !write(numb,"(i4.4)") FRC%mpi%rank
          !      flax = 'cfsiKpt'//trim(numb)//'.txt'
          !            cou  =  FRC%mpi%rank + 10
          !                  open(unit=cou,file=flax)
          !write(*,*) 'psitmp'
          !do ii = 1,size(psitmp%vata,2),1
          !write(cou,*) ii
          !write(cou,*) psitmp%vata(:,ii)
          !end do

          if (ntm(2) > 0) then
                  if (precond >= 0 .or. adapCF) then
                          call GenHamFun(FRC,kpt,ss,.FALSE.,psitmp%vata,tmp,hamu,hamd)
                          !write(*,*) "in cfsikpt, tmp ="
                          !write(*,*)  tmp
                          ev = dble(sum(conjg(tmp)*psitmp%vata))*dr
                          call bsxfunTimes_CMPLX(psitmp%vata,ev,res)
                          res = tmp-res
                  end if
                  if (precond >= 0) then
                          !call calcKineticPrecond(FRC,precond,kpt,prekin)
                          call applyKineticPrecond(FRC,prekin,res,restmp)
                          !deallocate(prekin)
                          psitmp%vata = psitmp%vata-restmp
                          deallocate(restmp)
                  end if
                  if (adapCF) then
                          !resreal = sum(conjg(res)*res)*dr
                          !cfdeg   = res
                          !cfdeg   = maxit
                          !do cc = 1,size(psitmp%vata,2),1
                          !    LU(1) = LB(kk,ss)
                          !    LU(2) = UB(kk,ss)
                          !    call CalcFilterTarget(ev(cc,1),res(cc,1),tol,LU,maxit,cfdeg(cc,1))
                          !end do
                          !do cc = 1,maxit,1
                          !    ntmp  = count(cfdeg == cc)
                          !    allocate(psivata(size(psitmp%vata,1),ntmp))
                          !    ntmp = 0
                          !    do jj = 1,size(psitmp%vata,2),1
                          !         if (cfdeg(jj,1) == cc) then
                          !                 ntmp = ntmp+1
                          !                 psivata(:,ntmp) = psitmp%vata(:,jj)
                          !         end if
                          !    end do
                          !    call chebyshevfilter(cc,psivata,psivata,nonsense)
                          !    ntmp = 0
                          !    do jj = 1,size(psitmp%vata,2),1
                          !        if (cfdeg(jj,1) == cc) then
                          !                ntmp = ntmp+1
                          !                psitmp%vata(:,jj) = psivata(:,ntmp)
                          !        end if
                          !    end do
                          !end do
                  else
                          !write(*,*) "before psitmp ="
                          !write(*,*)  psitmp%vata
                          !allocate(psi2%vata(size(psitmp%vata,1),size(psitmp%vata,2)))
                          !psi2 = psitmp
                          !call chebyshevfilter(maxit,psitmp%vata,nonsense,FRC,kpt,ss,.FALSE.,c,e,psi2%vata,nonsense)
                          call chebyshevfilter(maxit,psitmp%vata,nonsense,FRC,kpt,ss,.FALSE.,c,e)

                          !write(*,*) "after psitmp ="
                          !write(*,*)  psitmp%vata
                  end if
                  !write(numb,"(i4.4)") FRC%mpi%rank
                  ! flax = 'cfsiKpt'//trim(numb)//'.txt'
                  !       cou  =  FRC%mpi%rank + 10
                  !             open(unit=cou,file=flax)
                  !                     write(cou,*) 'in cfsiKpt.'
                  !write(cou,*) 'psi2'
                  !write(cou,*) 'maxit =', maxit
                  !write(cou,*) 'c =', c, 'e =', e
                  !do ii = 1,size(psi2%vata,2),1
                  !write(cou,*) ii
                  !write(cou,*) psi2%vata(:,ii)
                  !end do
          end if
          call ModBCDist_CMPLX_2D(FRC,psitmp,X%mblock,1,mpisize,1,.FALSE.,.FALSE.,psitmp2)
          ntmp = 0
          do ii = 1,count(colvec == ll),1
              if (colvec(ii) == ll) then
                      ntmp = ntmp+1
                      X%vata(:,ii) = psitmp2%vata(:,ntmp)
              end if
          end do
          deallocate(psitmp2%vata)
      end do

      !close(cou)
      !stop
      return
      end subroutine cfsiKpt
      end module cfsiKpt_module


