! rkronprod.f90
    
!*****************************************************************
    !
    !
    !
!*****************************************************************
    
    module lkronprod_cmplx_module
    contains
    subroutine lkronprod_cmplx(A,B,KA)

    external ZGEMM
    
    !implicit none
    
    ! input variables
    complex*16, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    integer :: p, q, n, m
    complex*16, allocatable :: TA(:,:), RA(:,:), SA(:,:), TB(:,:), RB(:,:)
    character(LEN=1) :: transa, transb
    integer          :: ii, jj, kk, LDA, LDB, LDC
    complex*16       :: alpha, beta

    ! output variables
    complex*16, allocatable :: KA(:,:)
    
    ! body of this function
    p = size(A,1)
    q = size(A,2)
    n = size(B,1)/q
    m = size(B,2)
    allocate(TA(q,p))
    allocate(TB(m,n*q))
    allocate(RB(n*m,q))
    allocate(RA(n*m,p))
    allocate(SA(m,p*n))
    TA = transpose(A)
    TB = transpose(B)
    RB = reshape(TB,(/n*m,q/))

    transa = 'N'
    transb = 'N'
    ii     =  n*m
    jj     =  p
    kk     =  q
    LDA    =  ii
    LDB    =  kk
    LDC    =  ii
    alpha  = (1.0D0,0.0D0)
    beta   = (0.0D0,0.0D0)
    call ZGEMM(transa,transb,ii,jj,kk,alpha,RB,LDA,TA,LDB,beta,RA,LDC)
    !RA = matmul(RB,TA)
    SA = reshape(RA,(/m,p*n/))
    allocate(KA(p*n,m))
    KA = transpose(SA)
    
    return
    end subroutine lkronprod_cmplx
    end module lkronprod_cmplx_module
