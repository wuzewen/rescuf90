! smiZHEEVD.f90

!**********************
      !
      !
      !
!******************************************

      module smiZHEEVDMOD
      contains
      subroutine smiZHEEVD(jobz,UPLO,N,A,MB,NB,nprow,npcol,LWORK,W,Z)

      implicit none

      character(len=1) :: jobz, UPLO
      integer :: MB, NB, nprow, npcol, LWORK, N
      complex*16, allocatable :: W(), Z(), A()

      integer :: LWORKTMP, LRWORKTMP, LIWROK, INFO, mprocs
      integer :: mone, LRWORK
      integer, allocatable :: IWORK(:)
      complex*16, allocatable :: WORK(:), RWORK(:)

      integer :: iam, ictxt, myrow, mycol

      LIWORK =  7*N+8*npcol+2
      nprocs =  nprow*npcol
      mone   = -1
      iam    =  0
      ictxt  =  0
      myrow  =  0
      mycol  =  0

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,Z_np)
      A_np = 
      Z_np = 

      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descZ,N,N,MB,NB,izero,izero,ictxt,Z_np,descinfo)
      allocate(WORK(2))
      allocate(RWORK(3))
      allocate(IWORK(1))
      call PZHEEVD(jobz,UPLO,N,A,ione,ione,descA,W,Z,ione,ione,deacsZ,&
              WORK,mone,RWORK,LRWORK,IWORK,LIWORK,INFO)
      LWORK = WORK(1)
      deallocate(WORK)
      allocate(WORK(LWORK))
      LIWORK = IWORK(1)
      deallocate(IWORK)
      allocate(IWORK(LIWORK))
      LRWORKTMP = 1.5*RWORK(1)*RWORK(1)/(RWORK(1)+1024)+1024
      if (LRWORK .LT. LRWORKTMP .OR. LRWORK .LT. 0) then
              LWORK = LRWORKTMP
      end if
      deallocate(RWORK)
      allocate(RWORK(LRWORK))

      call PZHEEVD(jobz,UPLO,N,A,ione,ione,descA,W,Z,ione,ione,deacsZ,&
              WORK,mone,RWORK,LRWORK,IWORK,LIWORK,INFO)

      if (INFO .LT. 0) then
              write(*,*) 'Error'
      else if (INFO .GT. 0) then
              write(*,*) 'Error'
      end if

      deallocate(WORK,RWORK,IWORK)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiZHEEVD
      end module smiZHEEVDMOD
