! VataMN_1D.f90
    
!**************************************************************
    !
    ! This module will contain a kind of data type including
    ! one dimentional real array vata, and integers m, n, 
    ! mblock, nblock, mproc, nproc.
    !
!**************************************************************
    
    module VataMN_1D_module
    
    type :: VataMN_1D
        real*8, allocatable :: vata(:)
        integer             :: m
        integer             :: n
        integer             :: mblock
        integer             :: nblock
        integer             :: mproc
        integer             :: nproc
    end type VataMN_1D
    
    end module VataMN_1D_module