! InitDistArray_2D_Sparse.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    subroutine InitDistArray_2D_Sparse(M,N,MB,NB,MP,NP,A)
    
    use SparseVataMN_2D_Real8_module
    
    implicit none
    
    ! input variables
    integer      :: M, N, MB, NB, MP, NP
    
    ! ouput variables
    type(SparseVataMN_2D_Real8) :: A
    
    ! body of this function
    A%m      = M
    A%n      = n
    A%mblock = MB
    A%nblock = NB
    A%mproc  = MP
    A%nproc  = NP
    
    return
    end subroutine InitDistArray_2D_Sparse
