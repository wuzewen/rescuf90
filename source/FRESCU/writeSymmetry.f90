! writeSymmetry.f90

      module writeSymmetry_module
      contains
      subroutine writeSymmetry(FRC,filnam)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam
      integer                         :: ii, jj

      open(unit=11,file=filnam)
      write(11,*) "space symmetry"
      write(11,*)  FRC%symmetry%spaceSymmetry
      write(11,*) "point symmetry"
      write(11,*)  FRC%symmetry%pointSymmetry
      write(11,*) "time reversal"
      write(11,*)  FRC%symmetry%timereversal
      write(11,*) "sym_rec"
      do ii = 1,size(FRC%symmetry%sym_rec,1),1
          write(11,*) ii, "part"
          do jj = 1,size(FRC%symmetry%sym_rec,2),1
              write(11,*) FRC%symmetry%sym_rec(ii,jj,:)
          end do
      end do
      write(11,*) "sym_dir"
      do ii = 1,size(FRC%symmetry%sym_dir,1),1
          write(11,*) ii, "part"
          do jj = 1,size(FRC%symmetry%sym_dir,2),1
              write(11,*) FRC%symmetry%sym_dir(ii,jj,:)
          end do
      end do
      write(11,*) "sym_t"
      do ii = 1,size(FRC%symmetry%sym_dir,1),1
          write(11,*) FRC%symmetry%sym_dir(ii,:)
      end do
      write(11,*) "sym_perm"
      do ii = 1,size(FRC%symmetry%sym_perm,1),1
          write(11,*) FRC%symmetry%sym_perm(ii,:)
      end do

      close(11)
      return
      end subroutine writeSymmetry
      end module writeSymmetry_module

