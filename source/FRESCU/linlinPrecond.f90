! linlinPrecond.f90

!****************************************************
      !
      !
      !
!******************************************************

      module linlinPrecondMod
      contains
      subroutine linlinPrecond(FRC,rho,bc)

      use 

      implicit none

      avec  = FRC%domain%latvec
      j2    = matmul(avec,transpose(conjg(avec)))
      k0    = FRC%mixing%kerkerNorm
      bctmp = FRC%domain%boundary
      FRC%domain%boundary = (/1,1,0/)
      call GenLaplFun(FRC,(/0,0,0/),.FALSE.)
      buff  = 0
      natom = size(FRC%atom%xyz,1)
      allocate(xyz(natom,3))
      xyz   = FRC%atom%xyz
      ntmp  = count(FRC%atom%element .EQ. 1)
      allocate(ind(ntmp))
      ntmp  = 0
      do ii = 1,natom,1
      ntmp  = ntmp+1
      if (FRC%atom%element(ii) .EQ. 1)
          ztmp(ntmp) = xyz(ii,3)
      end if
      end do

      zmin   = minval(ztmp)-buff
      zmax   = maxval(ztmp)+buff
      fgridn = FRC%domain%fgridn
      nu     = fgridn(1)
      nv     = fgridn(2)
      mw     = fgridn(3)
      allocate(u(nu),v(nv),w(nw))
      forall(ii=1:nu)
              u(ii) = dble(ii)/dble(nu)
      end forall
      forall(ii=1:nv)
              v(ii) = dble(ii)/dble(nv)
      end forall
      forall(ii=:nw)
              w(ii) = dble(ii)/dble(nw)
      end forall
      call ndgrid(u,v,w,zx)
      zx    = matmul(zx,avec)
      z     = zx(:,3)
      sigma = 1.6D0
      bfun  = k0**2 * (0.5D0*(erf((z-zmin)/sigma)+1.0D0) - &
              0.5D0*(erf((z-zmax)/sigma)-1.0D0)
      call GenPrecondFun(FRC,0,(/0,0,0/),.FALSE.,perfun)
      call bicgstab(b,1.0D-12,100,M,rho,failflag)
      if (failflag) then
              write(*,*) 'warning'
      end if

      return
      end subroutine linlinPrecond
      end module linlinPrecondMod
