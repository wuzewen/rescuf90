! triu_cmplx.f90
! this is a function which has the same function with triu in matlab for complex.
    
    module triu_cmplx_module
    contains
    subroutine triu_cmplx(A,k,B)
    
    ! input valuables
    integer             :: k
    complex*16, allocatable :: A(:,:)
    
    ! temporery valuables
    integer             :: m, n
    integer             :: i, j
    
    ! output valuables
    complex*16, allocatable :: B(:,:)
    
    ! body of the function
    m = size(A,1)
    n = size(A,2)
    B = 0.0D0
    do i = 1, m, 1
        do j = 1, n, 1
            if  (i+k <= j)  then
                B(i,j) = A(i,j)
            end if
        end do
    end do
    return
    end subroutine triu_cmplx
    end module triu_cmplx_module
