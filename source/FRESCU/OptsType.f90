! OptsType.f90

!***********************************************
      !
      !
      !
!***********************************************

      module OptsType_module

      type :: Opts_Type
              integer :: issym
              logical :: isreal
              real*8  :: tol
              integer :: maxit
      end type Opts_Type

      end module OptsType_module
