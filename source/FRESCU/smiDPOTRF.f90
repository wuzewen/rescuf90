! smiDPOTRF.f90

!***************************************
      !
      !
      !
!************************************************

      module smiDPOTRFMOD
      contains
      subroutine smiDPOTRF(N,A,nprocs,MB,NB,nprow,npcol)

      implicit none

      integer :: N, MB, NB, nprow, npcol
      real*8, allocatable :: A()


      iam   = 0
      ictxt = 0
      myrow = 0
      mycol = 0
      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      A_np = 

      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)

      call PDPOTRF(UPLO,N,A,ione,ione,descA,info)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiDPOTRF
      end module smiDPOTRFMOD
