! rnaParaAtom.f90
    
!******************************************************
    !
    !
    !
!******************************************************
    
    module rnaParaAtom_module
    contains
    subroutine rnaParaAtom(FRC,rho,rhodm)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use RHO_type
    use calcFilling_module
    use InterpRdist2Cart_module
    use ModBCDist_module
    use ModBCDist_2D_module
    use RealVata_2D_module
    use VataMN_2D_module
    use MPI_ALLREDUCE_SUM_REAL8_2D_module

    include 'mpif.h'
    
    !implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical :: mpistat
    integer :: mpirank, mpisize, ispin, fgridn(3), fn, natom, typeE
    integer :: ii, lqn, ierr, ntmp !, jj
    real*8  :: lvec(3,3), pos(1,3), fillFrac, mgm, theta, phi, val
    
    real*8,  allocatable :: magmom(:,:), rrdata(:), rhoData(:), rhocv(:,:)
    integer, allocatable :: lmind(:), ranks(:)
    real*8,  allocatable :: rhodmtmp(:,:), rhoVata(:,:), rhovatatmp(:), nonesne(:,:)
    type(VataMN_2D)         :: rhott, rhodmtt
    type(RealVata_2D), allocatable :: rhotmp(:,:)
    
    ! output variables
    type(VataMN_2D) :: rho, rhodm
    
    !type(RHODMtype) :: rhodm
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    fgridn  = FRC%domain%fgridn
    fn      = product(fgridn)
    natom   = FRC%atom%numberOfAtom
    lvec    = FRC%domain%latvec
    

    !call MPI_INIT(ierr)
    !call MPI_COMM_RANK(MPI_COMM_WORLD,mpirank,ierr)
    !call MPI_COMM_SIZE(MPI_COMM_WORLD,mpisize,ierr)
    !mpistat = .TRUE.


    call calcFilling(FRC,fillFrac)
    
    allocate(rhoVata(fn,1))
    !allocate(rhotmp(1,1)%vata(fn,1))
    !rhotmp(1,1)%vata(:,1) = 0
    rhoVata               = 0.0D0
    
    if (ispin == 1) then
        allocate(rhodmtmp(fn,1))
        rhodmtmp = 0.0D0
        rhoVata  = 0.0D0
    else if (ispin == 2) then
        magmom   = FRC%atom%magmom
        lmind    = FRC%atom%element
        allocate(rhodmtmp(fn,2))
        rhodmtmp  = 0.0D0
        rhoVata   = 0.0D0
    else if (ispin == 4) then
        magmom = FRC%atom%magmom
        lmind  = FRC%atom%element
        allocate(rhodmtmp(fn,4))
        rhodmtmp  = 0.0D0
        rhoVata   = 0.0D0
    end if
    
    do ii = mpirank+1,natom,mpisize
        typeE    = FRC%atom%element(ii)
        pos(1,:) = FRC%atom%XYZ(ii,:)
        lqn      = -1
        call InterpRdist2Cart(FRC%ElementData(typeE)%Rna%rrData, &
                              FRC%ElementData(typeE)%Rna%rhoData,&
                              pos,lqn,lvec,fgridn,.FALSE.,rhotmp,nonesne,rhovatatmp)
        rhoVata(:,1)  = rhoVata(:,1) + rhovatatmp   !rhotmp(1,1)%vata
        if (ispin == 1) then
            rhodmtmp(:,1) = rhodmtmp(:,1) + rhovatatmp*fillFrac
        else if (ispin == 2) then
            mgm           = magmom(ii,1)
            rhodmtmp(:,1) = rhodmtmp(:,1) + 0.5D0*(1.0D0+mgm)*rhovatatmp*fillFrac
            rhodmtmp(:,2) = rhodmtmp(:,2) + 0.5D0*(1.0D0-mgm)*rhovatatmp*fillFrac
        else if (ispin == 4) then
            mgm              = magmom(ii,1)
            theta            = magmom(ii,2)
            phi              = magmom(ii,3)
            val              = FRC%element(lmind(ii))%valence
            rhodmtmp(:,1)    = rhodmtmp(:,1) + rhovatatmp*fillFrac
            rhotmp(1,1)%vata = mgm*rhotmp(1,1)%vata*fillFrac/val
            rhodmtmp(:,2)    = rhodmtmp(:,2) + sin(theta)*rhovatatmp*cos(phi)
            rhodmtmp(:,3)    = rhodmtmp(:,3) + sin(theta)*rhovatatmp*sin(phi)
            rhodmtmp(:,4)    = rhodmtmp(:,4) + cos(theta)*rhovatatmp
        end if
        deallocate(rhovatatmp,rhotmp,nonesne)
    end do
    if (mpistat) then
        allocate(ranks(mpisize))
        forall(ii=1:mpisize)
                ranks(ii) = ii-1
        end forall
        allocate(rhocv(size(rhoVata,1),size(rhoVata,2)))
        call MPI_ALLREDUCE_SUM_REAL8_2D(rhoVata,ranks,rhocv)
        rhoVata =  rhocv
        deallocate(rhocv)
    end if 
    
    call InitDistArray_2D(fn,1,fn,1,1,mpisize,rho)
    allocate(rho%vata(size(rhoVata,1),size(rhoVata,2)),rhott%vata(size(rhovata,1),size(rhoVata,2)))
    rho%vata = rhoVata
    rhott    = rho
    !rho         = rhott
    ntmp     = ceiling(dble(fn)/dble(mpisize))
    call ModBCDist_2D(FRC,rhott,ntmp,1,mpisize,1,.FALSE.,.FALSE.,rho)
    !write(*,*) "marker 1"
    if (mpistat) then
        allocate(rhocv(size(rhodmtmp,1),size(rhodmtmp,2)))
        call MPI_Allreduce_sum_real8_2D(rhodmtmp,ranks,rhocv)
        rhodmtmp = rhocv
        deallocate(rhocv)
    end if
    
    if (ispin == 1) then
        call initDistArray_2D(fn,1,fn,1,1,mpisize,rhodm)
        allocate(rhodm%vata(size(rhodmtmp,1),size(rhodmtmp,2)),rhodmtt%vata(size(rhodmtmp,1),size(rhodmtmp,2)))
        rhodm%vata  = rhodmtmp
        rhodmtt     = rhodm
        ntmp        = ceiling(dble(fn)/dble(mpisize))
        call ModBCDist_2D(FRC,rhodmtt,ntmp,1,mpisize,1,.FALSE.,.FALSE.,rhodm)
    else if (ispin == 2) then
        
        write(*,*) "Error in rnaParaAtom. spin system is not available now."
        stop
        
        !call initDistArray(fn,2,fn,2,1,mpisize,rhodm,rhodmtmp)
        !call ModBCDist(FRC,rhodmtmp,fn/mpisize,1,mpisize,1,rhodm)
    else if (ispin == 4) then
        
        write(*,*) "Error in rnaParaAtom. spin system is not available now."
        stop
        
        !call initDistArray(fn,4,fn,4,1,mpisize,rhodm,rhodmtmp)
        !call ModBCDist(FRC,rhodmtmp,fn/mpisize,1,mpisize,1,rhodm)
    end if


    !call MPI_FINALIZE(ierr)
    
    return
    end subroutine rnaParaAtom
    end module rnaParaAtom_module
