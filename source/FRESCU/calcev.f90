! calcev.f90

!*********************************************************
      !
      !
      !
!*********************************************************

      module calcev_module
      contains
      !subroutine calcev(FRC,vspacein,coeff,vspace)
      subroutine calcev(FRC,vspace,coeff)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use distmat_isreal_cmplx_module
      use ModBCDist_CMPLX_2D_module
      use GetGlobalInd_CMPLX_2D_module
      use distmat_allgather_CMPLX_2D_module
      use MPI_Bcast_variable_CMPLX_2D_module

      external ZGEMM

      !implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      type(VataMN_CMPLX_2D)           :: coeff
      type(VataMN_CMPLX_2D)           :: vspacein

      ! temporary variables
      logical                         :: mpistat, gpustat, creal
      integer                         :: mpisize, sizeoff, buff, ii, jj
      type(VataMN_CMPLX_2D)           :: ev
      complex*16, allocatable         :: psitmp(:,:), evtmp(:,:)
      complex*16, allocatable         :: coeffvata(:,:)
      integer   , allocatable         :: ib(:,:), jb(:,:), ranks(:)
      integer                         :: m, n, k, lda, ldb, ldc
      character(len=1)                :: transa, transb
      complex*16                      :: alpha, beta
      complex*16, allocatable         :: vcc(:,:)

      ! output variables
      type(VataMN_CMPLX_2D)           :: vspace

      ! body of this function
      mpistat  = FRC%mpi%status
      mpisize  = FRC%mpi%mpisize
      gpustat  = FRC%gpu%status

      !call distmat_isreal_CMPLX(FRC,coeff,creal)
      creal = .FALSE.

      if (creal) then
              sizeoff = 16
      else
              sizeoff = 8
      end if

      if (gpustat) then
             write(*,*) "Error in calcev.f90. GPU is not available now."
      else
              buff   = FRC%option%bufferSize
              !ev     = vspacein
              !ev     = vspace
              !vspace = vspacein
              if (coeff%m*coeff%n > buff/sizeoff) then
                      ev     = vspace
                      call ModBCDist_CMPLX_2D(FRC,coeff,coeff%m,1,1,mpisize,.FALSE.,.FALSE.,coeff)
                      do ii = 0,mpisize-1,1
                          if (mpistat) then
                                  allocate(ranks(mpisize))
                                  forall(jj=1:mpisize)
                                          ranks(jj) = jj-1
                                  end forall
                                  call MPI_Bcast_variable_CMPLX_2D(coeff%vata,ii,ranks,psitmp)
                                  !write(*,*) "Error in calcev. MPI."
                          else
                                  psitmp = coeff%vata
                          end if
                          call GetGlobalInd_CMPLX_2D(ii,coeff,ib,jb)
                          evtmp = matmul(vspace%vata,psitmp)
                          do jj = 1,size(evtmp,2),1
                              ev%vata(:,jb(jj,1)) = evtmp(:,jj)
                          end do
                      end do
                      vspace = ev
              else
                      !write(*,*) 'In calcEV.'
                      !write(*,*) 'sizes coeff',size(coeff%vata,1),size(coeff%vata,2)
                      allocate(coeffvata(size(coeff%vata,1),size(coeff%vata,2)))
                      call distmat_allgather_CMPLX_2D(FRC,coeff,coeffvata)
                      transa = 'N'
                      transb = 'N'
                      m      =  size(vspace%vata,1)
                      n      =  size(coeff%vata,2)
                      k      =  size(vspace%vata,2)
                      lda    =  m
                      ldb    =  k
                      ldc    =  m
                      alpha  = (1.0D0,0.0D0)
                      beta   = (0.0D0,0.0D0)
                      !write(*,*) "m =", m, "n =", n
                      !stop
                      allocate(vcc(m,k))
                      vcc    = vspace%vata
                      call ZGEMM(transa,transb,m,n,k,alpha,vcc,lda,coeffvata,ldb,beta,vspace%vata,ldc)
                      !vspace%vata = matmul(vspace%vata,coeffvata)
                      !allocate(vcc(m,k))
                      !vcc = matmul(vcc,coeffvata)
                      !vcc = vspace%vata-vcc
                      !write(*,*) "vcc ="
                      !write(*,*)  count(abs(vcc)>0.0000000001D0)
                      !stop
              end if
      end if

      return
      end subroutine calcev
      end module calcev_module

