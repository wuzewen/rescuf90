! write2DarrayToFile.f90

!******************************************************************************



!******************************************************************************


        module write1DarrayToFile_module
        contains
        subroutine write1DarrayToFile(Ararry,filename)

        implicit none

        ! input variables
        real*8, allocatable :: Ararry(:)
        character(len=20)   :: filename

        ! temporary variables
        integer :: n1, ii

        ! output variables

        ! body of this function
        n1 = size(Ararry)
        !n2 = size(Ararry,2)
        open(unit=10,file=filename,status='replace',access='sequential',action='write')
        do ii = 1,n1,1
                write(10,*) Ararry(ii)
        end do
        close(10)

        return
        end subroutine write2DarrayToFile
        end module write2DarrayToFile_module

