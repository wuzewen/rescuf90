! Broyden2.f90

!***************************************************
      !
      !
      !
!***********************************************************

      module Broyden2Mod
      contains
      subroutine Broyden2(FRC,mu_m,vu_m,mu_mp1)

      use 

      implicit none

      mpistat  = FRC%mpi%status
      mpirank  = FRC%mpi%rank
      iter     = FRC%scloop
      betamix  = FRC%mixing%beta
      initlin  = FRC%mixing%initlin
      betalin  = FRC%mixing%betalin
      invJ0    = FRC%mixing%invJ0
      isScheme = FRC%mixing%ipScheme
      maxh     = FRC%mixing%maxhistory
      WI       = FRC%mixing%WI
      mu_0     = mu_m
      F_m      = vu_m-mu_m
      DF       = FRC%mixing%DF
      u        = FRC%mixing%u
      vt       = FRC%mixing%vt

      if (iter .GT. **) then
              forall(ii=1:maxh-1)
                      DF(:,ii) = DF(:,ii+1)
                      u(:,ii)  = U(:,ii+1)
                      vt(:,ii) = vt(:,ii+1)
              end forall
              forall(ii=1:maxh-1,jj=1:maxh-1)
                      FRC%mixing%FINF(ii,jj) = FRC%mixing%FINF(ii+1,jj+1)
              end forall
      end if

      if (iter .LE. initlin) then
              mu_mp1 = batalin*F_m
              if (trim(invJ0) .EQ. 'kerker') then
                      call mixingMetricWrapper(FRC,mu_0,mu_mp1)
              end if
              mu_mp1 = mu_m-mu_mp1
              MF_m   = F_m
              if (trim(ipScheme) .EQ. 'nonuniform') then
                      call mixingMetricWrapper(FRC,mu_0,F_m,MF_m)
              end if
      else
              mu_mm1 = FRC%mixing%mu_mm1
              F_mm1  = FRC%mixing%F_mm1
              MF_mm1 = FRC%mixing%MF_mm1
              MF_m   = F_m
              if (trim(ipScheme) .EQ. 'nonuniform') then
                      call mixingMetricWrapper(FRC,mu_0,F_m,MF_m)
              end if
              MY = MF_m - MF_mm1
              Y  = F_m - F_mm1

              yty = matmul(transpose(conjg(MY)),y)
              if (mpistat) then
                      call MPI_Allreduce_sum(yty)
              end if

              yty = sqrt(yty)
              call inversion(yty)
              MY  = matmul(MY,yty)
              Y   = matmul(Y,yty)
              DF  = 
              U   = 
              
              tmp = betamix*y
              if (trim(invJ0) .EQ. 'kerker') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              end if
              u(:,*) = u(:,*) + tmp

              if (iter - initlin .EQ. 1) then
                      FINF = 0.0D0
              else
                      allocate(FINF())
                      FINF = 0.0D0
                      forall(ii=1:,jj=1:)
                              FINF(ii,jj) = FRC%mixing%FINF(ii,jj)
                      end forall
              end if
              tmp = matmul(transpose(conjg(DF)),MY)
              if (mpistat) then
                      call MPI_Allreduce_sum(tmp)
              end if

              FINF(:,) = tmp
              FINF(,:) = FINF(:,)
              FINF(,) = 1.0D0
              AMAT = 0.0D0
              forall(ii=1:)
                      AMAT(ii,ii) = 1.0D0
              end forall
              AMAT = AMAT + FINF*WI**2
              allocate(BETAQ())
              forall(ii=1:)
                      BETAQ(ii,ii) = 1.0D0
              end forall
              call invertion(,MTMP)
              if (mpirank .EQ. 0) then
                      !call invertion(,MTMP)
                      BETAQ = BETAQ+WI**2*matmul(MTMP,FINF)
              end if
              if (mpistat) then
                      call MPI_Bcast_variable(BETAQ,0)
              end if
              Z1 = matmul(u,MTMP)*WI**2
              if (iter-initlin .GT. 1) then
                      forall(ii=1:)
                              Z2(ii,:) = BETAQ(ii,:)
                      end forall
                      Z2 = matmul(vt,Z2)
                      vy = Z1+Z2
              else
                      vt = Z1
              end if

              mu_mp1 = mu_m
              tmp    = matmul(transpose(conjg(DF)),MF_m)
              if (mpistat) then
                      call MPI_Allreduce_sum(tmp)
              end if
              mu_mp1 = mu_mp1 - matmul(vt,tmp)
              tmp    = betamix*F_m
              if (trim(invJ0) .EQ. 'kerker') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              end if
              mu_mp1          = mu_mp1+tmp
              FRC%mixing%DF   = DF
              FRC%mixing%FINF = FINF
              FRC%mixing%u    = u
              FRC%mixing%vt   = vt
      end if
      FRC%mixing%mu_mm1 = mu_m
      FRC%mixing%F_mm1  = F_m
      FRC%mixing%MF_mm1 = MF_m
      mu_mp1%vata = reshape(mu_mp1,size())

      return
      end subroutine Broyden2
      end module Broyden2Mod
