! eigensolverType.f90
    
!********************************************************************
    !
    ! This is to define a type to contain eigensolver information
    !
!********************************************************************
    
    module eigensolverType_module
    
    type :: eigensolverType
        character(len=10)    :: algo
        character(len=10)    :: algoproj
        integer              :: adapCFD
        integer              :: bandi(2)
        integer              :: emptyBand
        character(len=10)    :: init
        character(len=10)    :: ubMethod
        integer              :: maxIT
        integer              :: maxRestart
        integer              :: nsym
        integer              :: orthogonalize
        integer              :: precond
        integer              :: pwmode
        integer              :: Nvalence
        real*8               :: tol(2)
        integer              :: UBmaxit
        integer              :: nband
        integer              :: norb
        real*8, allocatable  :: prekin(:,:,:)
    end type eigensolverType
    
    end module eigensolverType_module