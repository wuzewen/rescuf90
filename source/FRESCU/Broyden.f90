! Broyden.f90

! This function provide bryden method to do mixing in mixer.f90.

      module Broyden_module
      contains
      subroutine Broyden(FRC,mu_m,vu_m,mu_mpl)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use VataMN_2D_module
      use MPI_Allreduce_sum_real8_2D_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      type(VataMN_2D)                 :: vu_m, mu_m

      ! temporary variables
      logical :: mpistat
      integer :: mpisize, iter, initlin, maxp, datasize, ii, itpmin
      real*8  :: beta, betalin
      real*8, allocatable :: mu(:), vu(:), u(:,:), vt(:,:), vtt(:,:), F_m(:)
      real*8, allocatable :: F_mml(:), fdiff(:,:), fdifft(:,:)
      real*8, allocatable :: norm2fdiff(:,:), mu_mml(:), u_m(:)
      real*8, allocatable :: vtxtmp(:,:), vtxfdiff(:,:), uv(:,:)
      real*8, allocatable :: eta_m(:,:), vttmp(:,:), vtxF_m(:,:)
      real*8, allocatable :: f_mt(:,:), utmp(:,:), uvtmp(:,:), utvt(:,:)

      integer, allocatable :: ranks(:)
      real*8 , allocatable :: norm2fdifftmp(:,:), vtxfdifftmp(:,:), vtxF_mtmp(:,:)

      ! output variables
      type(VataMN_2D)                 :: mu_mpl

      ! body of this function
      mpistat  = FRC%mpi%status
      mpisize  = FRC%mpi%mpisize
      iter     = FRC%scloop
      initlin  = FRC%mixing%initlin
      beta     = FRC%mixing%beta
      betalin  = FRC%mixing%betalin
      maxp     = FRC%mixing%maxhistory
      datasize = size(mu_m%vata)
      !allocate(mu_mpl%vata(datasize))
      mu_mpl   = mu_m
      allocate(mu(datasize))
      allocate(vu(datasize))
      mu       = mu_m%vata(:,1)
      vu       = vu_m%vata(:,1)

      !write(*,*) "mu ="
      !write(*,*)  mu
      !write(*,*) "vu ="
      !write(*,*)  vu
      
      allocate(u(datasize,maxp))
      allocate(vt(datasize,maxp))
      allocate(vtt(datasize,maxp+1))
      vtt      = 0.0D0
      !if (iter > 1) then
      u        = FRC%mixing%u
      vt       = FRC%mixing%vt
      forall(ii=1:maxp)
              vtt(:,ii) = vt(:,ii)
      end forall
      !else
      !        u        = 0.0
      !        vt       = 0.0
      !end if

      !allocate(vt())
     ! if (iter > 1) then
      !        vt       = FRC%mixing%vt
      !else
      !        vt       = 0.0
      !end if
      allocate(F_m(datasize))
      F_m      = vu-mu

      !write(*,*) "F_m in Broyden."
      !write(*,*)  F_m
      if (iter <= initlin) then
              mu_mpl%vata(:,1) = mu+betalin*F_m
      else
              allocate(F_mml(datasize))
              allocate(fdiff(datasize,1))
              allocate(fdifft(1,datasize))
              F_mml       = FRC%mixing%F_mml

              fdiff(:,1)  = F_m-F_mml
              !write(*,*) "fdiff = "
              !write(*,*)  fdiff
              allocate(norm2fdiff(1,1))
              fdifft      = transpose(fdiff)
              norm2fdiff  = matmul(fdifft,fdiff)
              !write(*,*) "norm2fdiff ="
              !write(*,*)  norm2fdiff
              if (mpistat) then
                      allocate(ranks(mpisize))
                      forall(ii=1:mpisize)
                              ranks(ii) = ii-1
                      end forall
                      allocate(norm2fdifftmp(size(norm2fdiff,1),size(norm2fdiff,2)))
                      call MPI_Allreduce_sum_real8_2D(norm2fdiff,ranks,norm2fdifftmp)
                      norm2fdiff = norm2fdifftmp
         !write(*,*) "Error in Broyden.f90. MPI is not available now."
              end if
              if (iter <= maxp+2) then
                     vtt(:,iter-1)     = fdiff(:,1)/norm2fdiff(1,1)
                     !call inversion(norm2fdiff,fdifftmp)
                     !vttmp        = matmul(fdiff,fdifftmp)
                     !vtL(:,2:end) = vttmp
              else
                     !forall(ii=1:maxp)
                     !        vtt(:,ii)   = vtt(:,ii+1)
                             !vt(:,maxp) = fdiff/norm2fdiff(1,1)
                     !end forall
                     vtt(:,maxp+1) = fdiff(:,1)/norm2fdiff(1,1)
              end if
              if (iter <= maxp+1) then
                      forall(ii=1:maxp)
                              vt(:,ii) = vtt(:,ii)
                      end forall
              else
                      forall(ii=1:maxp)
                              vt(:,ii) = vtt(:,ii+1)
                      end forall
              end if
              allocate(mu_mml(datasize))
              mu_mml       = FRC%mixing%mu_mml
              allocate(u_m(datasize))
              u_m          = beta*fdiff(:,1)+(mu-mu_mml)
              !write(*,*) "u_m = "
              !write(*,*)  u_m
              if (iter > 2) then
                      itpmin  = min(iter-2,maxp)
                      !write(*,*) "itpmin =", itpmin
                      !if (iter <= maxp+1) then
                      !        allocate(vtxtmp(iter-2,datasize))
                      !        allocate(uvtmp(datasize,iter-2))
                      !        forall(ii=1:iter-2)
                      !                vtxtmp(ii,:) = vt(:,ii)
                      !                uvtmp(:,ii)  =  u(:,ii)
                      !        end forall
                      !else
                      !        allocate(vtxtmp(maxp-1,datasize))
                      !        allocate(uvtmp(datasize,maxp-1))
                      !        forall(ii=1:maxp-1)
                      !                vtxtmp(ii,:) = vt(:,ii)
                      !                uvtmp(:,ii)  =  u(:,ii)
                      !        end forall
                      !end if
                      allocate(vtxtmp(itpmin,datasize))
                      allocate(uvtmp(datasize,itpmin))
                      forall(ii=1:itpmin)
                             vtxtmp(ii,:) = vtt(:,ii)
                             uvtmp(:,ii)  =   u(:,ii)
                      end forall
                      !write(*,*) "vtxtmp ="
                      !write(*,*)  vtxtmp
                      allocate(vtxfdiff(itpmin,1))
                      vtxfdiff = matmul(vtxtmp,fdiff)
                      !write(*,*) "vtxfdiff ="
                      !write(*,*)  vtxfdiff
                      if (mpistat) then
                              allocate(vtxfdifftmp(size(vtxfdiff,1),size(vtxfdiff,2)))
                              call MPI_Allreduce_sum_real8_2D(vtxfdiff,ranks,vtxfdifftmp)
                              vtxfdiff = vtxfdifftmp
          !write(*,*) "Error in Broyden.f90. MPI is not available now."
                      end if
                      allocate(uv(datasize,1))
                      uv  = matmul(uvtmp,vtxfdiff)
                      !write(*,*) "uv ="
                      !write(*,*)  uv
                      u_m = u_m-uv(:,1)
              end if
              if (iter <= maxp+1) then
                      u(:,iter-1) = u_m
              else
                      forall(ii=1:maxp-1)
                              u(:,ii) = u(:,ii+1)
                      end forall
                      u(:,maxp) = u_m
              end if

              !u2(:,1:size(u,2)) = u
              !u2(:,size(u,2)+1:size(u,2)+size(u_m,2)) = u_m

              allocate(eta_m(datasize,1))
              eta_m(:,1)  = beta*F_m
              !write(*,*) "eta_m = "
              !write(*,*)  eta_m

              itpmin = min(iter-1,maxp)
              allocate(vttmp(itpmin,datasize))
              forall(ii=1:itpmin)
                      vttmp(ii,:) = vt(:,ii)
              end forall
              allocate(vtxF_m(itpmin,1))
              allocate(f_mt(datasize,1))
              f_mt(:,1) = F_m
              vtxF_m = matmul(vttmp,f_mt)
              !write(*,*) "vttmp ="
              !write(*,*)  vttmp
              !write(*,*) "f_mt = "
              !write(*,*)  f_mt
              !write(*,*) "vtxF_m"
              !write(*,*)  vtxF_m
              if (mpistat) then
                      allocate(vtxF_mtmp(size(vtxF_m,1),size(vtxF_m,2)))
                      call MPI_Allreduce_sum_real8_2D(vtxF_m,ranks,vtxF_mtmp)
                      vtxF_m = vtxF_mtmp
          !write(*,*) "Error in Broyden.f90. MPI is not available now."
              end if

              allocate(utmp(datasize,itpmin))
              forall(ii=1:itpmin)
                      utmp(:,ii) = u(:,ii)
              end forall
              !write(*,*) "utmp ="
              !write(*,*)  utmp


              allocate(utvt(datasize,1))
              utvt  = matmul(utmp,vtxF_m)

              eta_m = eta_m - utvt !matmul(utmp,vtxF_m)
              !write(*,*) "eta_m ="
              !write(*,*)  eta_m
              mu_mpl%vata(:,1) = mu+eta_m(:,1)
      end if

     ! if (size(u,2) > maxp) then
     !         uend = u(:,2:en)
     !         vtLd = vtL(:,2:en)
     ! end if

      FRC%mixing%u      = u
      FRC%mixing%vt     = vt
      FRC%mixing%mu_mml = mu
      FRC%mixing%F_mml  = F_m
      FRC%mixing%maxFm  = maxval(abs(F_m))
      !mu_mpl%vata       = reshape(mu_mpl%vata,datasize)

      !write(*,*) "mu_mpl in broyden"
      !write(*,*)  mu_mpl%vata
      
      !write(*,*) "u = "
      !do ii = 1,size(u,1),1
      !write(*,*)  u(ii,:)
      !end do

      !write(*,*) "vt = "
      !do ii = 1,size(vt,1),1
      !write(*,*)  vt(ii,:)
      !end do

      !write(*,*) "mu = "
      !write(*,*)  mu
      !write(*,*) "F_m = "
      !write(*,*)  F_m
      !write(*,*) "maxFm"
      !write(*,*)  FRC%mixing%maxFm
      !if (iter == 22) then
      !        stop
      !end if
      return
      end subroutine Broyden
      end module Broyden_module
