! CalcFilterTarget.f90

!**********************************************************
      !
      !
      !
!**********************************************************

      module CalcFilterTarget_module
      contains
      subroutine CalcFilterTarget(ev,res,tol,bound,maxit,targe)

      implicit none

      ! input variables
      real*8  :: ev, res, tol, bound(2)
      integer :: maxit

      ! temporary variables
      integer :: kk
      real*8  :: e, c, x, psi0, psi1, psi2

      ! output variables
      integer :: targe

      ! body of this function
      e = (bound(2)-bound(1))/2.0D0
      c = (bound(2)+bound(1))/2.0D0
      x = (ev-c)/e
      psi0 = 1.0D0
      if (res < tol) then
              targe = 0
              return
      end if
      psi1 = x
      do kk = 2,maxit,1
          psi2 = 2.0D0*x*psi1-psi0
          if (res/abs(psi2) < tol) then
                  exit
          end if
          psi0 = psi1
          psi1 = psi2
      end do
      targe = kk

      return
      end subroutine CalcFilterTarget
      end module CalcFilterTarget_module
