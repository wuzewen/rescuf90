! BSXFTIMES.f90

!**********************************************************************
      !
      ! This is a interface for bsxfun times operation.
      ! This interface includes many kinds of data types 
      ! and dimentions of arrays.
      ! Data types include: double real, double complex.
      ! Dementions include: 2D, 3D, 4D.
      !
      ! Name details:
      !          bsxfuntimes_*d_*d_*
      !     The first * represent: The demention of the first input
      !          array.
      !     The second * represent: The demention of the second input
      !          array.
      !     The second * represent: The data type of the inputs.
      !          real8: double real
      !          cmplx: double complex
      !
!**********************************************************************

      module BSXFTIMES_module

              use bsxfuntimes_2d_2d_real8_module
              use bsxfuntimes_2d_2d_cmplx_module
              use bsxfuntimes_2d_3d_real8_module
              use bsxfuntimes_2d_3d_cmplx_module
              use bsxfuntimes_3d_2d_real8_module
              use bsxfuntimes_3d_2d_cmplx_module
              use bsxfuntimes_3d_3d_real8_module
              use bsxfuntimes_3d_3d_cmplx_module
              use bsxfuntimes_4d_4d_real8_module
              use bsxfuntimes_4d_4d_cmplx_module

              implicit none

              interface BSXFTIMES
                      module procedure bsxfuntimes_2d_2d_real8
                      module procedure bsxfuntimes_2d_2d_cmplx 
                      module procedure bsxfuntimes_2d_3d_real8
                      module procedure bsxfuntimes_2d_3d_cmplx
                      module procedure bsxfuntimes_3d_2d_real8
                      module procedure bsxfuntimes_3d_2d_cmplx
                      module procedure bsxfuntimes_3d_3d_real8
                      module procedure bsxfuntimes_3d_3d_cmplx
                      module procedure bsxfuntimes_4d_4d_real8
                      module procedure bsxfuntimes_4d_4d_cmplx
              end interface BSXFTIMES

      end module BSXFTIMES_module
