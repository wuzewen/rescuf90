! getrsphase.f90
    
!****************************************************
    !
    !
    !
!****************************************************
    
    module getrsphase_module
    contains
    subroutine getrsphase(FRC,kk,grid,eikr)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ndGridA_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8                          :: kk(3)
    integer                         :: grid(3)
    
    ! temporary variables
    integer              :: nx, ny, nz, ii
    real*8, allocatable  :: x(:), y(:), z(:), xyz(:,:)
    real*8               :: pi
    real*8, allocatable  :: kktmp(:,:), kcp(:,:), xyztmp(:,:)
    complex*16              :: im
    
    ! output variables
    complex*16, allocatable :: eikr(:,:)
    
    ! body of this function
    im         = dcmplx(0.0D0,1.0D0)
    pi         = 3.1415926535897932385D0
    nx         = grid(1)
    ny         = grid(2)
    nz         = grid(3)
    allocate(kktmp(3,1))
    allocate(kcp(1,1))
    allocate(xyztmp(1,3))
    kktmp(:,1) = kk(:)
    !write(*,*) "kk in getrsphase."
    !write(*,*)  kk
    
    allocate(x(nx),y(ny),z(nz))
    forall(ii=1:nx)
        x(ii) = dble(ii)/dble(nx)
    end forall
    forall(ii=1:ny)
        y(ii) = dble(ii)/dble(ny)
    end forall
    forall(ii=1:nz)
        z(ii) = dble(ii)/dble(nz)
    end forall
    
    allocate(xyz(nx*ny*nz,3))
    call ndgridA(x,y,z,xyz)
    !write(*,*) "xyz in getrsphase."
    !write(*,*)  xyz
    allocate(eikr(nx*ny*nz,1))
    !write(*,*) "test in getrsphase."
    !write(*,*)  matmul(xyz(1,:),kktmp)
    !kcp = pi*matmul(xyz(ii,:),kktmp)
    do ii= 1,nx*ny*nz,1
        xyztmp(1,:) = xyz(ii,:)
        kcp         = matmul(xyztmp,kktmp)
        eikr(ii,1)  = exp(-(2.0D0,0.0D0)*im*dcmplx(pi*kcp(1,1)))
        !eikr(ii,1) = exp(-(2.0,0.0)*im*cmplx(pi*matmul(xyz(ii,:),kktmp)))
    end do
    
    return
    end subroutine getrsphase
    end module getrsphase_module
