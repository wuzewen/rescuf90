! functionalType.f90
    
!***********************************************************
    !
    ! This type contains the xc functional information
    !
!***********************************************************
    
    module functionalType_module
    
    type :: functionalType
        logical                        :: libxc
        logical                        :: isrelativistic
        logical                        :: hybrid
        character(len=20)              :: ouput
        character(len=20), allocatable :: list(:)
    end Type functionalType
    
    end module functionalType_module
