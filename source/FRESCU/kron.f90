! kron.f90
    
!************************************************************
    !
    !
    !
!************************************************************
    
    module kron_module
    contains
    subroutine kron(A,B,C)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    integer :: mA, nA, mB, nB, ii, jj, kk, ll
    
    ! output variables
    real*8, allocatable :: C(:,:)
    
    ! body of this function
    mA = size(A,1)
    nA = size(A,2)
    mB = size(B,1)
    nB = size(B,2)
    allocate(C(mA*mB,nA*nB))   
    do ii = 1,mA,1
        do jj = 1,nA,1
            forall (kk=1:mB,ll=1:nB)
                C((ii-1)*mB+kk,(jj-1)*nB+ll) = A(ii,jj)*B(kk,ll)
            end forall
        end do
    end do
    
    return
    end subroutine kron
    end module kron_module
