! spdiags_4in.f90
    
!***************************************************************
    !
    !
    !
!***************************************************************
    
    module spdiags_4in_module
    contains
    subroutine spdiags_4in(A,d,m,n,B)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    integer,allocatable :: d(:)
    integer             :: m, n
    
    ! temporary variables
    integer :: Nx, Ny, Nd, Nn, jj, ii
    
    ! ouput variables
    real*8, allocatable :: B(:,:) 
    
    ! body of this function
    Nx = size(A,1)
    Ny = size(A,2)
    Nd = size(d)
    Nn = min(m,n)
    
    if (Nd /= Ny) then
        write(*,*) "Error in spdiags_4in.f90. The inputs' size is not correct."
        stop
    end if
        
    allocate(B(m,n))
    B = 0.0D0
    
    if (m >= n) then
            do ii = 1,Nd,1
                if (d(ii) < 0) then
                    forall(jj=1:Nn+d(ii))
                            B(-d(ii)+jj,jj) = A(jj,ii)
                    end forall
                else if (d(ii) >= 0) then
                    forall(jj=1:Nn-d(ii))
                            B(jj,d(ii)+jj) = A(jj+d(ii),ii)
                    end forall
                end if
            end do
    else
            if (d(ii) < 0) then
                    forall(jj=1:Nn+d(ii))
                            B(-d(ii)+jj,jj) = A(jj-d(ii),ii)
                    end forall
            else
                    forall(jj=1:Nn+d(ii))
                            B(jj,d(ii)+jj) = A(jj,ii)
                    end forall
            end if
    end if
    
    return
    end subroutine spdiags_4in
    end module spdiags_4in_module
