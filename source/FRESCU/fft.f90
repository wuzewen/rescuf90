! fft.f90

!*************************************************************
      !
      ! This is the interface of fast fourier transform. 
      ! Includeing different kinds of cases:
      ! (1) 2D array   input;
      ! (2) 2D pointer input;
      ! (*) 1D pointer input, but serves as 2D array;
      ! (1) 3D array   input;
      ! (2) 3D pointer input;
      ! (3) 1D pointer input, but serves as 3D array.
      !
!*************************************************************

      module fft_module

              !use fft2_fftw_module
              use fft3_fftw_module
              use fft3_fftw_pointer3D_module
              use fft3_fftw_pointer_module

              implicit none

              interface fft
                      !module procedure fft2_fftw
                      module procedure fft3_fftw
                      module procedure fft3_fftw_pointer3D
                      module procedure fft3_fftw_pointer
              end interface fft

      end module fft_module
