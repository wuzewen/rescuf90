! GenLaplFun.f90
    
!*****************************************************
    !
    !
    !
!*****************************************************
    
    module GenLaplFun_module
    contains
    !subroutine GenLaplFun(FRC,kpt,gpu,X,cLkfun,fLkfun)
    subroutine GenLaplFun(FRC,kpt,gpu,X,cLkfun)
    !subroutine GenLaplFun(FRC,kpt,gpu,X,ham)

    use FORTRAN_RESCU_CALCULATION_TYPE
    !use Cell_2D_real
    use inversion_module
    use fftFreq_module
    use fft_lapl_module
    use mat_lapl_module
    !use Acell_cmplx_type
    !use RealVata_2D_module
    use ComplexVata_2D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8  :: kpt(3)
    logical :: gpu
    complex*16, allocatable :: X(:,:), ham(:,:)
    
    ! temporary variables
    real*8, allocatable :: avec(:,:), bvec(:,:), j1(:,:), j2(:,:), ku(:,:,:), kv(:,:,:), kw(:,:,:), k2(:,:,:)
    real*8, allocatable :: kl(:,:,:), km(:,:,:), kn(:,:,:), k3(:,:,:)
    real*8, allocatable :: Du(:,:), Dv(:,:), Dw(:,:), Duu(:,:), Dvv(:,:), Dww(:,:)
    real*8, allocatable :: Dl(:,:), Dm(:,:), Dn(:,:), Dll(:,:), Dmm(:,:), Dnn(:,:)
    real*8              :: pi, kptm(3), kptm1(1,3)
    integer             :: n(3)
    !type(Acell_cmplxType)   :: tmp1(3), tmp2(3), tmp3(3), tmp4(3)
    type(ComplexVata_2D) :: tmp1(3), tmp2(3), tmp3(3), tmp4(3)
    ! output variables
    complex*16, allocatable :: cLkfun(:,:), fLkfun(:,:)
    
    ! body of this function
    pi         = 3.1415926535897932385D0
    allocate(avec(3,3),bvec(3,3),j1(3,3),j2(3,3))
    avec       = FRC%domain%latvec
    call inversion(avec,bvec)
    j1         = transpose(bvec)
    bvec       = 2.0D0*pi*j1
    kptm1(1,:) = kpt(:)
    kptm1      = matmul(kptm1,bvec)
    kptm(:)    = kptm1(1,:)
    j2         = matmul(j1,transpose(j1))
    !write(*,*) "kpt in Genlapl."
    !write(*,*)  kpt
    
    if (trim(FRC%diffop%method) == "fft") then
        n = FRC%domain%cgridn
        allocate(ku(n(1),n(2),n(3)))
        allocate(kv(n(1),n(2),n(3)))
        allocate(kw(n(1),n(2),n(3)))
        call fftfreq(n,bvec,ku,kv,kw)
        ku = ku+kptm(1)
        kv = kv+kptm(2)
        kw = kw+kptm(3)
        allocate(k2(n(1),n(2),n(3)))
        k2 = -ku*ku-kv*kv-kw*kw
        !k2 = -ku*ku-kv*kv-kw*kw
        if (gpu) then
            write(*,*) "Error in GenLaplFun.f90. GPU is not avalaible now."
            stop
        !    !call gpuArray(k2)
        end if
        call fft_lapl(FRC,X,n,k2,cLkfun)
        deallocate(ku,kv,kw,k2)
        !n  = FRC%domain%fgridn
        !allocate(kl(n(1),n(2),n(3)))
        !allocate(km(n(1),n(2),n(3)))
        !allocate(kn(n(1),n(2),n(3)))
        !call fftfreq(n,bvec,ku,kv,kw)
        !kl = kl+kptm(1)
        !km = km+kptm(2)
        !kn = kn+kptm(3)
        !allocate(k3(n(1),n(2),n(3)))
        !!k2 = -ku*conjg(ku)-kv*conjg(kv)-kw*conjg(kw)
        !k3 = -kl*kl-km*km-kn*kn
        !if (gpu) then
        !    write(*,*) "Error in GenLaplFun.f90. GPU is not avalaible now."
        !    stop
        !    !call gpuArray(k2)
        !end if
        !call fft_lapl(FRC,X,n,k3,fLkfun)
    else
        !write(*,*) "Error in GenLaplFun.f90. FRC%diffop%method should be fft."
        !stop
        n   = FRC%domain%cgridn
        !allocate(Du(n(1),n(1)))
        !allocate(Dv(n(2),n(2)))
        !allocate(Dw(n(3),n(3)))
        !allocate(Duu(n(1),n(1)))
        !allocate(Dvv(n(2),n(2)))
        !allocate(Dww(n(3),n(3)))
        !Du  = FRC%diffop%Du
        !Dv  = FRC%diffop%Dv
        !Dw  = FRC%diffop%Dw
        !Duu = FRC%diffop%Duu
        !Dvv = FRC%diffop%Dvv
        !Dww = FRC%diffop%Dww
        if (gpu) then
            write(*,*) "Error in GenLaplFun.f90. GPU is not avalaible now."
            stop
            !call gpuArray(n)
            !call gpuArray(j1)
            !call gpuArray(j2)
            !call gpuArray(kpt)
            !call gpuArray(Du)
            !call gpuArray(Dv)
            !call gpuArray(Dw)
            !call gpuArray(Duu)
            !call gpuArray(Dvv)
            !call gpuArray(Dww)
        end if
        allocate(tmp1(1)%vata(n(1),n(1)))
        allocate(tmp1(2)%vata(n(2),n(2)))
        allocate(tmp1(3)%vata(n(3),n(3)))
        allocate(tmp2(1)%vata(n(1),n(1)))
        allocate(tmp2(2)%vata(n(2),n(2)))
        allocate(tmp2(3)%vata(n(3),n(3)))
        tmp1(1)%vata = dcmplx(FRC%diffop%Du)
        tmp1(2)%vata = dcmplx(FRC%diffop%Dv)
        tmp1(3)%vata = dcmplx(FRC%diffop%Dw)
        tmp2(1)%vata = dcmplx(FRC%diffop%Duu)
        tmp2(2)%vata = dcmplx(FRC%diffop%Dvv)
        tmp2(3)%vata = dcmplx(FRC%diffop%Dww)
        call mat_lapl(x,n,tmp1,J1,tmp2,J2,kptm,cLkfun)
        !n   = FRC%domain%fgridn
        !allocate(Dl(n(1),n(1)))
        !allocate(Dm(n(2),n(2)))
        !allocate(Dn(n(3),n(3)))
        !allocate(Dll(n(1),n(1)))
        !allocate(Dmm(n(2),n(2)))
        !allocate(Dnn(n(3),n(3)))
        !Dl  = FRC%diffop%fDu
        !Dm  = FRC%diffop%fDv
        !Dn  = FRC%diffop%fDw
        !Dll = FRC%diffop%fDuu
        !Dmm = FRC%diffop%fDvv
        !Dnn = FRC%diffop%fDww
        !if (gpu) then
        !    write(*,*) "Error in GenLaplFun.f90. GPU is not avalaible now."
        !    stop
            !call gpuArray(n)
            !call gpuArray(j1)
            !call gpuArray(j2)
            !call gpuArray(kpt)
            !call gpuArray(Du)
            !call gpuArray(Dv)
            !call gpuArray(Dw)
            !call gpuArray(Duu)
            !call gpuArray(Dvv)
            !call gpuArray(Dww)
        !end if
        !allocate(tmp3(1)%vata(n(1),n(1)))
        !allocate(tmp3(2)%vata(n(2),n(2)))
        !allocate(tmp3(3)%vata(n(3),n(3)))
        !allocate(tmp4(1)%vata(n(1),n(1)))
        !allocate(tmp4(2)%vata(n(2),n(2)))
        !allocate(tmp4(3)%vata(n(3),n(3)))
        !tmp3(1)%vata = dcmplx(Dl)
        !tmp3(2)%vata = dcmplx(Dm)
        !tmp3(3)%vata = dcmplx(Dn)
        !tmp4(1)%vata = dcmplx(Dll)
        !tmp4(2)%vata = dcmplx(Dmm)
        !tmp4(3)%vata = dcmplx(Dnn)
        !call mat_lapl(x,n,tmp3,J1,tmp4,J2,kptm,fLkfun)
    end if

    !ham = ham - 0.5D0*cLkFun
    !deallocate(cLkfun)
    
    return
    end subroutine GenLaplFun
    end module GenLaplFun_module
