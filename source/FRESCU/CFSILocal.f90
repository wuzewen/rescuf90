! CSFILocal. f90

!**********************************************************************
      !
      ! This is the main function of chebyshev filter with parallel K
      ! points. It will be used in KS_main_real_scf_paraK.f90.
      !
      ! Input and output :
      ! FRC : a customized type of struture data, which will be used
      ! through the whole code.
      !
      ! Main subroutines used :
      ! UpdateVeff         : getting effective potential
      ! distmat_allgather  : data communication
      ! MPI_Bcast_variable : MPI ?
      ! ind2sub            : Subscripts from linear index
      ! GetKBorb           : getting nonlocal chi_lm
      ! GenHamFun          : getting Hamiltonian
      ! LanczosUB          : getting Up boundary with Lanczos Method
      ! chebyshevfilter    : doing chebyshev filter
      ! MPI_Allreduce_sum  : MPI ?
      ! 
      ! Compared to CFSI.f90
      ! calcCFBound.f90 cfsiKpt.f90 projectHamitonian,f90 diag_proj.f90
      ! calcev.f90 are all included here.
      !
!**********************************************************************

      module CFSILocal_module
      contains
      subroutine CFSILocal(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use VataMN_CMPLX_2D_module
      use UpdateVeff_module
      use distmat_allgather_1D_module
      use GetKBorb_module
      use GetKBorb_Sparse_module
      use calcKineticPrecond_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      !real*8, allocatable             :: LB(:,:), UB(:,:)

      ! temporary variables
      logical :: gpustat, iscomplex, mpistat
      integer :: mpirank, mpisize, cn, maxit, nband, buff
      integer :: gpurank, iter, ispin, nspin, nkpt, precond
      integer :: kk, ss, ksp(2), ii, jj, kk, mm
      real*8  :: kpt(3), e, c, latvec(3,3), vol, dr
      real*8, allocatable   :: LB(:,:), UB(:,:), nrg(:,:,:)
      real*8, allocatable   :: kdir(:,:), prekin(:,:,:), vloc(:)
      real*8, allocatable   :: theta(:)

      complex*16, allocatable :: XHX(:,:), XHY(:,:), XSX(:,:), XSY(:,:)

      type(VataMN_CMPLX_2D) :: XHX, XSX
      type(VataMN_1D)       :: veff
      type(VataMN_CMPLX_2D) :: ZZ

      ! output variables

      ! body of this function
      !write(*,*) "I'm here in CFSI."
      mpistat  = FRC%mpi%status
      mpirank  = FRC%mpi%rank
      mpisize  = FRC%mpi%mpisize
      !gpustat  = FRC%gpu%status
      !gpurank  = FRC%gpu%rank
      iter     = FRC%scloop
      ispin    = FRC%spin%ispin
      nspin    = FRC%spin%nspin
      cn       = product(FRC%domain%cgridn)
      latvec   = FRC%domain%latvec
      vol      = latvec(1,1)*latvec(2,2)*latvec(3,3)  &
                +latvec(1,2)*latvec(2,3)*latvec(3,1)  &
                +latvec(1,3)*latvec(2,1)*latvec(3,2)  &
                -latvec(1,1)*latvec(2,3)*latvec(3,2)  &
                -latvec(1,2)*latvec(2,1)*latvec(3,3)  &
                -latvec(1,3)*latvec(2,2)*latvec(3,1)
      dr       = vol/dble(cn)
      nkpt     = size(FRC%kpoint%ikdirect,1)
      allocate(kdir(nkpt,3))
      kdir     = FRC%kpoint%ikdirect
      maxit    = FRC%eigensolver%maxit
      nband    = FRC%eigensolver%nband
      precond  = FRC%eigensolver%precond
      buff     = FRC%option%bufferSize
      allocate(nrg(nband,nkpt,nspin))
      nrg      = 0.0D0

      call UpdateVeff(FRC,FRC%potential%veffin(1),FRC%potential%vps,.FALSE.,veff)
      !write(*,*) "In CFSI, veff ="
      !write(*,*)  veff%vata
      allocate(vloc(size(veff%vata)))
      call distmat_allgather_1D(FRC,veff,vloc)
      FRC%potential%vloc(:,1) = vloc

      !if (allocated(FRC%potential%vtau)) then
      !        call distmat_allgather(FRC,FRC%potential%vtau,FRC%potential%vtauloc)
      !end if
      !write(*,*) "in cfsi, precond ="
      !write(*,*)  precond
      eval = FRC%energy%ksnrg%vata
      allocate(LB(nkpt,nspin))
      forall(ii=1:nkpt,jj=1:nspin)
              LB(ii,jj) = maxval(eval(:,ii,jj))
      end forall

      if (mpistat) then
              call MPI_Bcast_variable(LB,0.0D0,LB)
      end if

      allocate(UB(nkpt,nspin))
      UB    = 0.0D0
      niter = FRC%eigensolver%ubmaxit

      allocate(ksp(2))
      ksp(1) = nkpt
      ksp(2) = nspin

      do ii = mpirank+1,mpisize,nkpt*nspin
          call ind2sub(ksp,ii,kk,ss)
          kpt = kdir(kk,:)
          if (FRC%potential%vnl%KBorbIsSparse) then
                  call GetKBorb_Sparse(FRC,kpt)
          else
                  call GetKBorb(FRC,kpt)
          end if
          if (ispin == 1 .or. ispin == 2) then
                  !nham = cn
                  if (any(kpt /= 0.0D0)) then
                          anykpt = .TRUE.
                  else
                          anykpt = .FALSE.
                  end if
                  call LanczosUB(FRC,kpt,ss,anykpt,cn,niter,UB(kk,ss))
          else if (ispin == 4) then
                  nham = 2*cn
          end if
      end do

      if (mpistat) then
              !call MPI_Allreduce_sum(real(UB),UB)
      end if

      do ii = mpirank+1,mpisize,nkpt*nspin
          call ind2sub(ksp,ii,kk,ss)
          kpt = kdir(kk,:)
          if (any(kpt /= 0) .or. ispin == 4) then
                  iscomplex = .TRUE.
                  sizeofc   =  16
          else
                  iscomplex = .FALSE.
                  sizeofc   =  8
          end if
          if (FRC%potential%vnlScheme) then
                  if (FRC%potential%vnl%KBorbIsSparse) then
                          call GetKBorb_Sparse(FRC,kpt)
                  else
                          call GetKBorb(FRC,kpt)
                  end if
          end if
          e    = (UB(kk,ss)-LB(kk,ss))/2.0D0
          c    = (UB(kk,ss)+LB(kk,ss))/2.0D0
          blks = ceiling(dble(buff)/dble(cn)/dble(sizeofc))
          blks = blks*mpisize
          allocate(colvec(nband))
          do kk = 1,nband,1
              colvec(kk) = floor((dble(kk)-(1.0D-12))/dble(blks))+1
          end do

          do ll = 1,maxval(colvec),1
              if (ispin == 1 .or. ispin == 2) then
                      ntmp  = 0
                      do mm = 1,nband,1
                          if (colvec(mm) == ll) then
                                  ntmp = ntmp + 1
                                  psitmp%vata(:,ntmp) = X%vata(:,mm)
                          end if
                      end do
                      call chebyshevfilter(maxit,psitmp%vata,nonsense,FRC,kpt,ss,.FALSE.,c,e,psitmp%vata,nonsense)
                      ntmp = 0
                      do mm = 1,count(colvec == ll),1
                          if (colvec(mm) == ll) then
                                  ntmp = ntmp+1
                                  FRC%psi(kk,ss)%vata(:,mm) = psitmp%vata(:,ntmp)
                          end if
                      end do
              else if (ispin == 4) then
                      write(*,*) "Error in CFSILocal.f90. ISPIN = 4"
              end if
          end do

          if (ispin == 1 .or. ispin == 2) then
                  psitmp = FRC%psi(kk,ss)
                  call GenHamFun(FRC,kpt,ss,.false.,psitmp%vata,Xtmp,nonsense1,nonsense2)
          else if (ispin == 4) then
                  write(*,*) "Error in CFSILocal.f90. ISPIN = 4"
          end if

          Ytmp = conjg(transpose(psitmp%vata))
          XHX  = matmul(Ytmp,Xtmp)
          XHX  = XHX*dr
          XHY  = conjg(transpose(XHX))
          XHX  = 0.5D0*(XHX+XHY)
          XSX  = matmul(Ytmp,psitmp%vata)
          XSX  = XSX*dr
          XSY  = conjg(transpose(XSX))
          XSX  = 0.5D0*(XSX+XSY)

          call ZHEGV(1,'V','U',N,XHX,N,XSX,N,W,WORK,LWORK,RWORK,INFO)

          nrg(:,kk,ss) = W
          psitmp%vata  = matmul(psitmp%vata,XHX)

          if (ispin == 1 .or. ispin == 2) then
                  FRC%psi(kk,ss) = psitmp
          else if (ispin == 4) then
                  write(*,*) "Error in CFSILocal.f90. ISPIN = 4"
          end if
      end do

      if (mpistat) then
              call MPI_Allreduce_sum(nrg,nrg)
      end if

      FRC%energy%ksnrg = nrg

      return
      end subroutine CFSILocal
      end module CFSILocal_module



