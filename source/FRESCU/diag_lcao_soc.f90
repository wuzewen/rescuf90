! diag_lcao_soc.f90

!************************************************
      !
      !
      !
!*************************************************

      module diag_lcao_socMod
      contains
      subroutine diag_lcao_soc(FRC,evOnly,ZZ,theta)

      implicit none
      
      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpiszie = FRC%mpi%mpisize
      smistat = FRC%smi%status
      nband   = FRC%eigenSolver%nband
      norb    = FRC%XSX%m
      algo    = FRC%eigensolver%algoproj
      bandi   = FRC%eigensolver%bandi

      if (smistat) then
          mb = FRC%XSX%mblock
          nb = FRC%XSX%nblock
          mp = FRC%XSX%mproc
          np = FRC%XSX%nproc
          call InitDistArray(2*norb,2*norb,2*norb,2*norb,mp,np,SS)
          call distmat_gather(FRC,FRC%XSX,tmpM)
          if (mpirank .EQ. 0) then
              SS%vata = 0.0D0
              mtmp = size(tmpM,1)
              ntmp = size(tmpM,2)
              forall(ii=1:mtmp,jj=ntmp)
                  SS%vata(ii,jj)           = tmpM(ii,jj)
                  SS%vata(ii+mtmp,jj+ntmp) = tmpM(ii,jj)
              end forall
          end if
          call ModBCDist(FRC,SS,mb,nb,mp,np)

          call InitDistArray(2*norb,2*norb,2*norb,2*norb,mp,np,HH)
          call distmat_gather(FRC,FRC%XHX(1),tmpM)
          if (mpirank .EQ. 0) then
              HH%vata = 0.0D0
              forall(ii=1:norb,jj=1:norb)
                  HH%vata(ii,jj) = HH%vata(ii,jj)+tmpM(ii,jj)
              end forall
          end if

          call distmat_gather(FRC,FRC%XHX(2),tmpM)
          if (mpirank .EQ. 0) then
              forall(ii=1:norb,jj=1:norb)
                  HH%vata(ii,ii+norb) = HH%vata(ii,ii+norb)+tmpM(ii,jj)
                  HH%vata(ii+norb,ii) = HH%vata(ii+norb,ii)+conjg(tmpM(jj,ii))
              end forall
          end if

          call distmat_gather(FRC,FRC%XHX(3),tmpM)
          if (mpirank .EQ. 0) then
              forall(ii=1:norb,jj=1:norb)
                  HH%vata(ii+norb,ii+norb) = HH%vata(ii+norb,ii+norb)+tmpM(ii,jj)
              end forall
          end if
          call ModBCDist(FRC,HH,mb,nb,mp,np)

          abstol = FRC%smi%abstol
          orfac  = FRC%smi%orfac
          lwork  = FRCsmi%lwork
          if (trim(algo) .EQ. trim('DSYEV') .OR. &
                  trim(algo) .EQ. trim('DSYEVD') .OR. &
                  trim(algo) .EQ. trim('DSYGV')) then
              call distmat_zeroslike(FRC,HH,ZZ)
              evrange = 'A'
          else if (trim(algo) .EQ. trim('DSYEVR') .OR. &
                  trim(algo) .EQ. trim('DSYEVX') .OR. &
                  trim(algo) .EQ. trim('DSYGVX')) then
              call distmat_zeroelike(FRC,HH,(/2*norb,nband/),ZZ)
              evrange = 'I'
          end if

          if (evOnly) then
                  jobz = 'N'
          else
                  jobz = 'V'
          end if

          if (trim(algo) .EQ. trim('DSYEV') .OR. &
                  trim(algo) .EQ. trim('DSYEVD') .OR. &
                  trim(algo) .EQ. trim('DSYEVR') .OR. &
                  trim(algo) .EQ. trim('DSYEVX')) then
              call smiZPOTRF(SS)
              call smiZHEGST(HH,SS)
              if (trim(algo) .EQ. trim('DSYEV')) then
                  call smiZHEEV(jobz,'U',HH,ZZ,theta)
              else if (trim(algo) .EQ. trim('DSYEVD')) then
                  call smiZHEEVD('V','U',HH,ZZ,theta)
              else if (trim(algo) .EQ. trim('DSYEVR')) then
                  call smiZHEEVR(jobz,'I','U',HH,bandi(1),bandi(2),ZZ,theta)
              else if (trim(algo) .EQ. trim('DSYEVX')) then
                  call smiZHEEVX(jobz,'I','U',HH,bandi(1),bandi(2), &
                          abstol,orfac,lwork,ZZ,theta)
              end if
              if (.NOT. evOnly) then
                      call smiZTRTRS('U','N',SS,ZZ)
              end if
          else if (trim(algo) .EQ. trim('DSYGV')) then
              call smiZHEGVX(jobz,'A','U',HH,SS,bandi(1),bandi(2),&
                      abstol,orfac,lwork,ZZ,theta)
          else if (trim(algo) .EQ. trim('DSYGVX')) then
              call smiZHEGVX(jobz,'I','U',HH,SS,bandi(1),bandi(2),&
                      abstol,orfac,lwork,ZZ)
          end if

          if (trim(evrange) .EQ. 'A') then
              forall(ii=1:bandi(2)-bandi(1))
                      theta(ii) = ii-1+bandi(1)
              end forall
              if (.NOT. evOnly) then
                  call ModBCDist(FRC,ZZ,ZZ%mblock,ZZ%nblock,mpisize,1)
                  if (allocated(ZZ%vata)) then
                          ZZ%vata = 
                  end if
                  ZZ%n = nband
              end if
          end if

          if (.NOT. evOnly) then
              call ModBCDist(FRC,ZZ%mblock,ZZ%nblock,1,mpisize)
              HH   = ZZ
              HH%m = norb
              SS   = ZZ
              SS%m = norb
              if (allocated(ZZ%vata)) then
                      delet
              end if
              call ModBCDist(FRC,HH,mb,nb,mp,np)
              call ModBCDist(FRC,SS,mb,nb,mp,np)
              ZZ = 
          end if
      else
          call distmat_gather(FRC,FRC%XSX,tmpM)
          if (mpirank .EQ. 0) then
              SS%vata = 0.0D0
              mtmp = size(tmpM,1)
              ntmp = size(tmpM,2)
              forall(ii=1:mtmp,jj=ntmp)
                  SS%vata(ii,jj)           = tmpM(ii,jj)
                  SS%vata(ii+mtmp,jj+ntmp) = tmpM(ii,jj)
              end forall
          end if

          call distmat_gather(FRC,FRC%XHX(1),tmpM)
          if (mpirank .EQ. 0) then
              HH%vata = 0.0D0
              forall(ii=1:norb,jj=1:norb)
                  HH%vata(ii,jj) = HH%vata(ii,jj)+tmpM(ii,jj)
              end forall
          end if

          call distmat_gather(FRC,FRC%XHX(2),tmpM)
          if (mpirank .EQ. 0) then
              forall(ii=1:norb,jj=1:norb)
                  HH%vata(ii,ii+norb) = HH%vata(ii,ii+norb)+tmpM(ii,jj)
                  HH%vata(ii+norb,ii) = HH%vata(ii+norb,ii)+conjg(tmpM(jj,ii))
              end forall
          end if

          call distmat_gather(FRC,FRC%XHX(3),tmpM)
          if (mpirank .EQ. 0) then
              forall(ii=1:norb,jj=1:norb)
                  HH%vata(ii+norb,ii+norb) = HH%vata(ii+norb,ii+norb)+tmpM(ii,jj)
              end forall
              SS = 0.5D0*(SS+transpose(conjg(SS)))
              HH = 0.5D0*(HH+transpose(conjg(HH)))
          end if

          call iniTDistArray(2*norb,nband,2*norb,nband,1,mpisize,ZZ)
          if (mpirank .EQ. 0) then
              if (evOnly) then
                  call ZHEEV(HH,SS,theta)
              else
                  call ZHEEV(HH,SS,ZZ%vata,theta)
                  ZZ%vata = 
                  ZZ%vata = 
              end if
              theta = theta()
          else
              allocate(theta(nband))
              theta = 0.0D0
          end if

          if (mpistat) then
              call MPI_Bcast_variable(theta,0)
          end if

          if (evOnly) then
              delet 
          else
              HH = ZZ
              HH%m = norb
              SS = ZZ
              SS%m = norb
              if (mpirank .EQ. 0) then
                  delet
              end if
              mb = FRC%XSX%mblock
              nb = FRC%XSX%nblock
              mp = FRC%XSX%mproc
              np = FRC%XSX%nproc
              call ModBCDist(FRC,HH,mb,nb,mp,np)
              call ModBCDist(FRC,SS,mb,nb,mp,np)
              ZZ = 
          end if
      end if

      return
      end subroutine diag_lcao_soc
      end module diag_lcao_socMod



