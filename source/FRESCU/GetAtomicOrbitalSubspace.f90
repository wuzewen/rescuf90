! GetAtomicOrbitalSubspace.f90

!*********************************************************************
      !
      !
      !
!*********************************************************************

      module GetAtomicOrbitalSubspace_module
      contains
      subroutine GetAtomicOrbitalSubspace(FRC,gridn,printswitch)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      integer                         :: gridn(3)
      logical                         :: printswitch

      ! temporary variables
      logical :: mpistat, usecell, aosparse, bsCal, dosCal
      integer :: mpirank, mpisize, N, nproj, natom, nkpt, ntv, npoint
      integer :: kk, elem, lcut, zcut, jj, ll, nnzLoc
      real*8  :: avec(3,3), rad, pos(3), kpt(3)

      logical, allocatable :: isLoc(:), idel(:) 
      integer, allocatable :: evec(:), oorb(:), lnu(:), znu(:), inde(:)
      integer, allocatable :: indLoc(:), pairBoole(:,:), pairVec(:,:)
      integer, allocatable :: vecInd(:)
      real*8 , allocatable :: rorb(:), kdir(:,:), xyz(:,:), tvec(:,:)
      real*8 , allocatable :: GridCoord(:,:)

      type(Acell), allocatable :: aocell(:)
      type(VataMN_2D)          :: aotmp

      ! output variables

      ! body of this function
      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize

      call GetAtomicOrbital(FRC)

      N       = product(gridn)
      avec    = FRC%domain%latvec
      evec    = FRC%LCAO%parameters%evec
      rorb    = FRC%LCAO%parameters%Rorb
      oorb    = FRC%LCAO%parameters%Oorb
      nproj   = size(evec,1)
      natom   = FRC%atom%NumberOfAtom

      kdir    = FRC%kpoint%ikdirect
      nkpt    = size(kdir,1)

      if (any(FRC%kpoint%kdirect /= 0.0D0)) then
              usecell = .TRUE.
      else
              usecell = .FALSE.
      end if

      if (.not. usecell) then
              FRC%LCAO%dynSubRed = .FALSE.
              FRC%LCAO%veffRed   = .FALSE.
              scrinfo            = 'Gammma point calculation'
      end if

      rad   = maxval(rorb)
      xyz   = FRC%atom%xyz
      call cellInRange(avec,xyz,rad,tvec)
      call flipud(tvec,tvec)
      tvec  = -tvec
      ntv   =  size(tvec,1)
      call GetGridPointCoord(FRC,gridn,GridCoord)
      npoint = size(GridCoord,1)

      if (FRC%option%initParaReal .and. usecell) then
              call circum_sphere(avec,rsph)
              ones   = 0.0D0
              forall(ii=1:3)
                      ones(ii,ii) = 1.0D0
              end forall
              center = 0.5D0*matmul(ones,avec)
              call InitDistArray(N,nproj,N/mpisize,1,mpisize,1,aosub)
              call repmat(aosub,ntv,1,aocell)

              do tt = 1,ntv,1
                  aotmp = aosub
                  do ii = 1,natom,1
                      vect = tvec(tt,:)
                      pos  = FRC%atom%xyz(ii,:)+matmul(vect,avec)
                      elem = FRC%atom%element(ii)
                      lnu  = FRC%element(ii)%aolnu
                      lcut = FRC%element(ii)%aoLcutoff
                      znu  = FRC%element(ii)%aoznu
                      zcut = FRC%element(ii)%aoZcutoff
                      ntmp = 0
                      do jj = 1,size(lnu),1
                          if (lnu(ii) <= lcut .and. znu(ii) <= zvut) then
                                  ntmp = ntmp+1
                          end if
                      end do
                      allocate(inde(ntmp))
                      ntmp = 0
                      do jj = 1,size(lnu),1
                          if (lnu(ii) <= lcut .and. znu(ii) <= zvut) then
                                  ntmp = ntmp+1
                                  inde(ntmp) = jj
                          end if
                      end do
                      vata = FRC%ElementData(elem)%OrbitalSet
                      do jj = 1,ntmp,1
                          post = pos-center
                          call norm(post,tmp1)
                          tmp2 = rsph+vata(jj)%rrData(size(vata(jj)%rrData))
                          if (tmp1 < tmp2) then
                                  ll = vata(jj)%parameters%L
                                  ntmp2 = 0
                                  do kk = 1,size(evec),1
                                      if (evec(kk) == ii .and. oorb(kk) == jj) then
                                              ntmp2 = ntmp2+1
                                      end if
                                  end do
                                  allocate(aoind(ntmp2))
                                  ntmp2 = 0
                                  do kk = 1,size(evec),1
                                      if (evec(kk) == ii .and. oorb(kk) == jj) then
                                              ntmp2 = ntmp2+1
                                              aoind(ntmp2) = kk
                                      end if
                                  end do
                                  rrData = vata(jj)%rrData
                                  frData = vata(jj)%frData
                                  call InterpSph2Cart(rrData,frData,pos,ll,GridCoord,avec,.FALSE.,aotmpvata)
                                  do kk = 1,ntmp2,1
                                      aotmp%vata(:,aoind(kk)) = aotmpvata(kk)
                                  end do
                          end if
                      end do
                  end do
              end do
              call sprucecell(FRC,aocell,idel)
              allocate(aocellnew(size(aocell)-size(idel))
              ntmp = 0
              do jj = 1,size(aocell),1
                  if (all(jj /= idel)) then
                          ntmp = ntmp+1
                          aocellnew(ntmp) = aocell(jj)
                          tvecNew(ntmp,:) = tvec(jj,:)
                  end if
              end do
              if (.not. usecell) then
                      !
              end if
      else if (mpistat .and. (.not. usecell)) then
              write(*,*) "Error in GetAtomicOrbitalSubspace.f90. MPI"
      else
              allocate(aocell(ntv))
              kk = 1
              do ii = 1,natom,1
                  pos  = FRC%atom%xyz(ii,:)
                  elem = FRC%atom%element(ii)
                  lnu  = FRC%element(elem)%aolnu
                  lcut = FRC%element(elem)%aoLcutoff
                  znu  = FRC%element(elem)%aoznu
                  zcut = FRC%element(elem)%aoZcutoff
                  ntmp = 0
                  do jj = 1,size(lnu),1
                      if (lnu <= lcut .and. znu <= zcut) then
                              ntmp = ntmp+1
                      end if
                  end do
                  ntmp = 0
                  do jj = 1,size(lnu),1
                      if (lnu <= lcut .and. znu <= zcut) then
                              ntmp       = ntmp+1
                              inde(ntmp) = jj
                      end if
                  end do
                  vata = FRC%ElementData(elem)%OrbitalSet
                  do jj = 1,ntmp,1
                      ll    = vata(inde(jj))%L
                      isLoc = .FALSE.
                      if (mod(ii-1,mpisize) == mpirank) then
                              rrData = vata(inde(jj))%rrData
                              frData = vata(inde(jj))%frData
                              call InterpRdist2Cart(rrData,frData,pos,ll,avec,gridn,.TRUE.,aotmp,tvtmp,nonsense)
                              call ismemberRow(tvec,tvtmp,isLoc,nonsense)
                              call ismemberRow(tvetmp,tvec,nonsense,indLoc)
                       end if
                       kk = kk+1
                   end do
               end do
               do ii = 1,size(aocell,1),1
                   call InitDistArray(N,nproj,N/mpisize,1,mpisize,1,aocell(ii,1))
               end do

               if (.not. usecell) then
                       !
               end if

               if (mpistat) then
                   write(*,*) "Error in GetAtomicOrbitalSubspace. MPI"
               end if

       end if

       call reduceOper(aocell,tvec,(/0.0D0,0.0D0,0.0D0/),.TRUE.,aotmp)
       call nnz(aotmp%vata,nnzLoc)

       call findCellPair(FRC,alcell,pairBoole)
       call findPairVec(tvec,pairBoole,PairVec,nonsense,vecInd)

       call distmat_nnz(FRC,aotmp,nnzTot)
       buff = FRC%option%bufferSize

       FRC%LCAO%veffRed = .FALSE.
       if (nkpt > 15) then
               FRC%LCAO%veffRed = .TRUE.
       end if

       if (nkpt*nnzTot/mpisize > buff/16) then
               FRC%LCAO%dynSubRed = .TRUE.
             scrinfo = 'The atomic subspace memory requirement is large'
       else
               FRC%LCAO%dynSubRed = .FALSE.
               if (nkpt < 100) then
                       FRC%LCAO%veffRed = .FALSE.
               end if
       end if

       if (trim(FRC%info%calculationType) == trim('band-structure') & 
       .or.trim(FRC%info%calculationType) == trim('dos')) then
           FRC%LCAO%dynSubRed = .TRUE.
           FRC%LCAO%veffRed   = .TRUE.
       end if
       
       if (FRC%LCAO%veffRed) then
               FRC%LACO%pairBoole = pairBoole
               FRC%LCAO%pairVec   = pairVec
               FRC%LCAO%vecind    = vecInd
               FRC%LCAO%aocell    = aocell
       end if

       if (FRC%LCAO%dynSubRed) then
               FRC%LCAO%aocell
       else if (usecell) then
               do kk = 1,nkpt,1
                   kpt = kdir(kk,:)
                   call reduceOper(aocell,tvec,kpt,aosparse,FRC%LCAO%aokcell(kk))
               end do
       else if (.not. usecell) then
               FRC%LCAO%aocell = aocell
       end if

       FRC%LCAO%aokvec = tvec

       if (aosparse) then
               FRC%LCAO%aosparse = nnzLoc
       else
               FRC%LCAO%aosparse = -1
       end if

       return
       end subroutine GetAtomicOrbitalSubspace
       end module GetAtomicOrbitalSubspace_module



