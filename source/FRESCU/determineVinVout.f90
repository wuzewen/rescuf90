! determineVinVout.f90
    
!****************************************************
    !
    !
    !
!****************************************************
    
    module determineVinVout_module
    contains
    subroutine determineVinVout(FRC,vin,vout)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_2D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    integer :: mpirank, iter
    
    ! output variables
    type(VataMN_2D)                 :: vin, vout
    
    
    ! body of this function
    !write(*,*) "I'm here at the begining of determineVinVout."
    mpirank = FRC%mpi%rank
    iter    = FRC%scloop
    
    if (trim(FRC%Mixing%Mixingtype) == trim("density") .and. &
            (trim(FRC%info%calculationType) == trim("self-consistent") .or. &
            trim(FRC%info%calculationType) == trim("relaxation") .or. &
            trim(FRC%info%calculationType) == trim("phonon"))) then
        Vin  = FRC%Rho%Input(1)
        Vout = FRC%Rho%Output(2)
    else if (trim(FRC%Mixing%Mixingtype) == trim("Potential") .and. &
            (trim(FRC%info%calculationType) == trim("SCF") .or. &
            trim(FRC%info%calculationType) == trim("relaxation") .or. &
            trim(FRC%info%calculationType) == trim("phonon"))) then
        write(*,*) "Error in determineVinVout.f90. Potential can't be mixed now, please use Density."
        stop
        !Vin  = FRC%Potential%VeffIn
        !Vout = FRC%Potential%VeffOut
    else if (trim(FRC%Mixing%Mixingtype) == trim("Density") .and. &
            trim(FRC%info%calculationType) == trim("DFPT")) then
        write(*,*) "Error in determineVinVout.f90. DFPT is not available now."
        stop
        !Vin  = FRC%Rho%DeltaIn
        !Vout = FRC%Rho%DeltaOut
    else if (trim(FRC%Mixing%Mixingtype) == trim("Potential") .and. &
            trim(FRC%info%calculationType) == trim("DFPT")) then
        write(*,*) "Error in determineVinVout.f90. Potential can't be mixed now, please use Density."
        stop
        !Vin  = PotentialDeltaIn
        !Vout = PotentialDeltaOut
    else if (trim(FRC%Mixing%Mixingtype) == trim("DensityMatrix") .and. &
            (trim(FRC%info%calculationType) == trim("SCF") .or. &
            trim(FRC%info%calculationType) == trim("relaxation") .or. &
            trim(FRC%info%calculationType) == trim("phonon"))) then
        write(*,*) "Error in determineVinVout.f90. DensityMatrix can't be mixed now, please use Density."
        stop
        !nkpt  = size(DMcell,1)
        !nspin = size(DMcell,2)
        !call GetGlobalInd()
        !if (SCFloop==1) then
        !    nocc = ExxDMxx
        !    call spdiags(nocc,0,norb,norb,dmtmp)
        !    call InitDistArray()
        !    call repmat()
        !    reshape(VinData,,nkpt*nspin)
        !else
        !    Vin = LCAODMin
        !end
        !call InitDistArray()
        !VoutData(:) = 0
        !do i = 1,nkpt*nspin
        !    call ind2sub()
        !    VoutData = DMcellData
        !end
        !VoutData = reshape(VoutData,,nkpt*nspin)
    end if
    
    return
    end subroutine determineVinVout
    end module determineVinVout_module
