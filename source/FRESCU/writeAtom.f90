! writeAtom.f90

      module writeAtom_module

      contains

      subroutine writeAtom(FRC,filnam)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam
      integer                         :: ii

      open(unit=10,file=filnam)
      write(10,*) "System Name        = "
      write(10,*) FRC%atom%systemName
      write(10,*) "Coordinate Type    = "
      write(10,*) FRC%atom%coordinateType
      write(10,*) "Number of Element  = "
      write(10,*) FRC%atom%NumberOfElement
      write(10,*) "Number of Atom     = "
      write(10,*) FRC%atom%numberOfAtom
      write(10,*) "Element Index      = "
      write(10,*) FRC%atom%element
      write(10,*) "Positions of Atoms = "
      if (size(FRC%atom%XYZ) /= 0) then
              do ii = 1,size(FRC%atom%XYZ,1),1
                        write(10,*) FRC%atom%XYZ(ii,:)
              end do
      end if
      !if (size(FRC%atom%FracXYZ) /= 0) then
      !        do ii = 1,size(FRC%atom%FracXYZ,1),1
      !                  write(10,*) FRC%atom%FracXYZ(ii,:)
      !        end do    
      !end if

      close(10)

      return
      end subroutine writeAtom
      end module writeAtom_module

