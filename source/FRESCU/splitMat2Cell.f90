! splitMat2Cell.f90
    
!*******************************************
    !
    !
    !
!*******************************************
    
    module splitMat2Cell_module
    contains
    subroutine splitMat2Cell(tx,sx,ut)
    
    use uniqueR_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: tx(:)
    
    ! temporary variables
    integer             :: n, m, ii, jj
    real*8, allocatable :: tu(:)
    
    ! output variables
    integer, allocatable :: ut(:), sx(:)
    
    ! body of this function
    call uniqueR(tx,tu)
    m = size(tx)
    n = size(tu)
    allocate(sx(n),ut(n))
    ut = int(tu)
    sx = 0
    do ii = 1,n,1
        do jj = 1,m,1
            if (int(tx(jj)) == ut(ii)) then
                sx(ii) = sx(ii) + 1     ! tx(jj)
            end if
        end do
    end do
    
    return
    end subroutine splitMat2Cell
    end module splitMat2Cell_module