! bsxfunTimes_4D_3D_cmplx.f90

!******************************************************************************
    !
    ! This function has the same function with function bsxfun(@times,*,*)
    ! in matlab. 
    !
    ! MatrixA :: input , 4 Dimentional array
    ! MatrixB :: input , 3 Dimentional array
    ! MatrixC :: output, 4 Dimentional array
    ! 
!******************************************************************************
    
    module bsxfunTimes_4D_3D_cmplx_module
    contains
    subroutine bsxfunTimes_4D_3D_cmplx(MatrixA,MatrixB,MatrixC)
    
    implicit none
    
    ! input valuables
    complex*16,allocatable :: MatrixA(:,:,:,:), MatrixB(:,:,:)
    
    ! temporery valuables
    !complex*16,allocatable :: tmpB(:,:,:,:)
    integer                :: ii
    integer                :: nA(4), nB(3)

    ! output valuables
    complex*16,allocatable :: MatrixC(:,:,:,:)
    
    ! body of this function
    nA(1) = size(MatrixA,1)
    nA(2) = size(MatrixA,2)
    nA(3) = size(MatrixA,3)
    nA(4) = size(MatrixA,4)
    nB(1) = size(MatrixB,1)
    nB(2) = size(MatrixB,2)
    nB(3) = size(MatrixB,3)
    
    if (nA(1) /= nB(1) .or. nA(2) /= nB(2) .or. nA(3) /= nB(3)) then
            write(*,*) "Error in bsxfunTimes_4D_3D_cmplx.f90."
            write(*,*) "Please check the sizes of the inputs."
            stop
    end if

    !allocate(tmpB(nA(1),nA(2),nA(3),nA(4)))
    !forall(ii=1:nA(4))
    do ii = 1,nA(4),1
            MatrixC(:,:,:,ii) = MatrixA(:,:,:,ii)*MatrixB
    !end forall
    end do

    return
    end subroutine bsxfunTimes_4D_3D_cmplx
    end module bsxfunTimes_4D_3D_cmplx_module
    
