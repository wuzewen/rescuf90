! find_sym_op.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module find_sym_op_module
    contains
    subroutine find_sym_op(element,pos,avec,spaceSymmetry,tol,ph,magmom,allSym,allTrans,allPerm,allSym_rec)
    
    use bsxfunMinus_module
    use inversion_module
    use calcpermSym_module
    use uniqueRow_module
    use intersectRow_module
    
    implicit none
    
    ! input variables
    integer, allocatable :: element(:)
    real*8,  allocatable :: pos(:,:), avec(:,:), magmom(:,:)
    logical              :: spaceSymmetry, ph
    real*8               :: tol
    
    ! temporary variables
    integer :: natom, nelem, ii, jj, kk, ll, x(3), N, mtmp, ntmp, ttmp, nsym, ind, ss, nt1
    real*8  :: met(3,3), met_tmp(3,3), stmp(3,3), tvt(1,3), bvec(3,3), pi, alltn(3,3), allout(3,3)
    logical :: failflag
    integer,allocatable :: deter(:), perm(:), tmptrans(:,:), tmpperm(:,:), symop(:,:), symopt(:,:,:)
    integer,allocatable :: symopr(:,:,:), allSymTmp(:,:,:)
    real*8, allocatable :: magmomTMP(:,:), posx(:,:), posN(:,:), allTransTmp(:,:), allPermTmp(:,:)
    real*8, allocatable :: postmp(:,:), tvecss(:,:), tvecs(:,:), tvecsT(:,:), tvectmp(:,:), tvectmp1(:,:)
    real*8, allocatable :: tvectmp2(:,:), avtmp(:,:), a2b(:,:), b2a(:,:)
    
    ! output variables
    integer, allocatable :: allSym(:,:,:), allSym_rec(:,:,:)
    real*8,  allocatable :: allTrans(:,:), allPerm(:,:)
    
    ! body of this function
    pi    = 3.1415926535897932385D0
    natom = size(element)
    nelem = maxval(element)
    if (size(magmom) == 0) then
        allocate(magmomTMP(natom,1))
        magmomTMP = 0.0D0
    else
        allocate(magmomTMP(natom,size(magmom,2)))
        magmomTMP = magmom
    end if
    
    met = matmul(avec,transpose(avec))
    
    forall(ii=1:3)
        x(ii) = ii-2
    end forall
    
    N = 3**9
    allocate(symop(N,9))
    do ii = 1,9,1
        ntmp = 3**ii
        mtmp = ntmp/3
        ttmp = 0
        do jj = 1,N/ntmp,1
            do kk = 1,3,1
                do ll = 1,mtmp,1
                    ttmp = ttmp+1
                    symop(ttmp,ii) = x(kk)
                end do
            end do
        end do
    end do
    
    allocate(deter(N))
    
    deter  = symop(:,1)*symop(:,5)*symop(:,9)+symop(:,2)*symop(:,6)*symop(:,7) &
           + symop(:,3)*symop(:,4)*symop(:,8)-symop(:,7)*symop(:,5)*symop(:,3) &
           - symop(:,8)*symop(:,6)*symop(:,1)-symop(:,9)*symop(:,4)*symop(:,2)
    allocate(symopt(3,3,N))
    !write(*,*) "deter in find_sym_op."
    !write(*,*)  deter

    symopt = reshape(transpose(symop),(/3,3,N/))
    
    nsym = 0
    do ii = 1,N,1
        if (abs(deter(ii)) == 1) then
            nsym = nsym+1
        end if
    end do
    
    !deallocate(symop)
    allocate(symopr(3,3,nsym))
    ttmp = 0
    do ii = 1,N,1
        if (abs(deter(ii)) == 1) then
            ttmp = ttmp+1
            symopr(:,:,ttmp) = symopt(:,:,ii)
        end if
    end do
    
    deallocate(deter)
    allocate(deter(nsym))
    deter = 0
    
    do ii = 1,nsym,1
        stmp    = dble(symopr(:,:,ii))
        met_tmp = matmul(stmp,avec)
        met_tmp = matmul(met_tmp,transpose(met_tmp))
        met_tmp = abs(met_tmp-met)
        if (maxval(met_tmp) < tol) then
            deter(ii) = 1
        end if
    end do
    
    ntmp = count(deter == 1)
    deallocate(symopt)
    allocate(symopt(3,3,ntmp))
    
    
    ttmp = 0
    do ii = 1,nsym,1
        if (deter(ii) == 1) then
            ttmp = ttmp+1
            symopt(:,:,ttmp) = symopr(:,:,ii)
        end if
    end do
    
    nsym = ttmp
    allocate(postmp(1,3))

    allocate(posN(natom,3))
    allocate(posx(natom,3))  
    allocate(tvecss(natom,3))
    ntmp = 0
    !write(*,*) "size of loop", nsym
    do ii = 1,nsym,1
        !write(*,*) "######################### ii =", ii
        stmp = symopt(:,:,ii)
        !allocate(posx(natom,3))
        posx = matmul(pos,stmp)
        !allocate(posN(natom,3))
        forall(jj=1:size(pos,1),kk=1:3)
            posN(jj,kk) = anint(posx(jj,kk))
        end forall
        posx = posx - posN
        !write(*,*) "posx ="
        !do jj = 1,size(posx,1),1
        !write(*,*)  posx(jj,:)
        !end do
        !write(*,*) "spaceSymmetry"
        !write(*,*)  spaceSymmetry
        if (spaceSymmetry) then
            ind = 1
            do jj = 1,size(element),1
                if (element(jj) == 1) then
                    ind = jj
                    exit
                end if
            end do
            !write(*,*) "ind = ", ind
            postmp(1,:) = pos(ind,:)
            !allocate(tvecss(natom,3))
            call bsxfunMinus(postmp,posx,tvecss)
            tvecss = anint(tvecss/tol)*tol
            tvecss = tvecss+0.0000001D0
            tvecss = tvecss-anint(tvecss)
            tvecss = anint(tvecss/tol)*tol

            !write(*,*) "tvecss ="
            !write(*,*)  tvecss
            call uniqueRow(tvecss,tvecst)
            !write(*,*) "tvecst"
            !write(*,*)  tvecst
            allocate(tvectmp(size(tvecst,1)+1,3))
            tvectmp(1,:) = 0.0D0
            forall(jj=1:size(tvecst,1),kk=1:3)
                tvectmp(jj+1,kk) = tvecst(jj,kk)
            end forall
            !write(*,*) "before second loop"
            !write(*,*) "tvectmp ="
            !write(*,*)  tvectmp
            do ss = 2,nelem
                ind = 1
                do jj = 1,natom,1
                    if (element(jj) == ss) then
                        ind = jj
                        exit
                    end if
                end do
                !write(*,*) "ind=",ind
                postmp(1,:) = pos(ind,:)
                call bsxfunMinus(postmp,posx,tvecss)
                !write(*,*) "tvecss",tvecss
                tvecss = anint(tvecss/tol)*tol
                tvecss = tvecss+0.0000001D0
                !write(*,*) "tvecss",tvecss
                tvecss = tvecss-anint(tvecss)
                allocate(tvecs(size(tvecss,1),size(tvecss,2)))
                tvecs  = anint(tvecss/tol)*tol
                !write(*,*) "tvecs", tvecs
                deallocate(tvecst)
                call uniqueRow(tvecs,tvecst)
                allocate(tvectmp1(size(tvecst,1)+1,size(tvecst,2)))
                tvectmp1(1,:) = 0.0D0
                tvectmp1(2:size(tvecst,1),:) = tvecst(:,:)
                !write(*,*) "size of input"
                !write(*,*)  size(tvectmp,2), size(tvectmp1,2)
                call intersectRow(tvectmp,tvectmp1,tvectmp2)
                !write(*,*) "after intersectRow"
                deallocate(tvectmp)
                !write(*,*) "after deallocate(tvectmp)"
                allocate(tvectmp(size(tvectmp2,1),size(tvectmp2,2)))
                tvectmp = tvectmp2
                !write(*,*) "tvectmp2 ="
                !write(*,*)  tvectmp2
                deallocate(tvectmp2,tvectmp1,tvecs)
                !write(*,*) "end of second loop"
            end do
            deallocate(tvecst)
        else
            allocate(tvectmp(1,3))
            tvectmp = 0.0D0
        end if
        !write(*,*) "after if symmetry, tvectmp ="
        !do jj = 1,size(tvectmp,1),1
        !write(*,*)  tvectmp(jj,:)
        !end do
        tvectmp = dble(anint(tvectmp/tol))*tol
        call uniqueRow(tvectmp,tvectmp2)
        !write(*,*) "after unique. tvectmp2 ="
        !do jj = 1,size(tvectmp,2),1
        !write(*,*)  tvectmp2(:,jj)
        !end do
        nt1 = size(tvectmp2,1)
        !write(*,*) "before another loop.",nt1
        do jj = 1,nt1,1
            !stmp = symopt(:,:,i)
            tvt(1,:) = tvectmp2(jj,:)
            !write(*,*) "before calcPerm."
            call calcPermSym(stmp,tvt,pos,tol,perm,failflag)
            !write(*,*) "after calcPerm", failflag
            !write(*,*) "stmp = ", stmp
            !write(*,*) "pos  = ", pos
            !write(*,*) "tvt  = ", tvt
            !write(*,*) "tol  = ", tol
            !write(*,*) "perm = ", perm
            if (.not. failflag) then
                if (.not. all(element(perm) == element)) then
                    failflag = .TRUE.
                    !deallocate(perm)
                else
                    failflag = .FALSE.
                end if
                !if (.not. all(magmom(perm,:) == magmom)) then
                !    failflag = .TRUE.
                    
                !else if (failflag) then
                !    failflag = .TRUE.
                !else
                !    failflag = .FALSE.
                !end if
                if (failflag) then
                        deallocate(perm)
                end if
            end if
            !write(*,*) "after failure flag"
            !write(*,*)  failflag
            if ((.not. failflag) .and. (.not. ph)) then
                ntmp = ntmp+1

                !write(*,*) "tvectmp"
                !do kk = 1,size(tvectmp,1),1
                !write(*,*)  tvectmp(kk,:)
                !end do
                !write(*,*) "jj = ", jj
                !write(*,*) "tvectmp(jj,:)"
                !write(*,*)  tvectmp(jj,:)

                allocate(allTransTmp(ntmp,3))
                allocate(allSymTmp(3,3,ntmp))
                allocate(allPermTmp(size(perm,1),ntmp))
                !allocate(allTransTmp(ntmp,3))
                if (ntmp == 1) then
                    allTransTmp(ntmp,:)    = tvectmp2(jj,:)
                    allSymTmp(:,:,ntmp)    = symopt(:,:,ii)
                    allPermTmp(:,ntmp)     = perm(:)
                    !allTransTmp(ntmp,:) = tvectmp(jj,:)
                else if (ntmp > 1) then
                    forall(kk=1:ntmp-1)
                        allTransTmp(kk,:) = allTrans(kk,:)
                        allSymTmp(:,:,kk) = allSym(:,:,kk)
                        allPermTmp(:,kk)  = allPerm(:,kk)
                    end forall
                    allTransTmp(ntmp,:) = tvectmp2(jj,:)
                    allSymTmp(:,:,ntmp) = symopt(:,:,ii)
                    allPermTmp(:,ntmp)  = perm(:)
                    deallocate(allTrans)
                    deallocate(allSym)
                    deallocate(allPerm)
                end if
                allocate(allTrans(ntmp,3))
                allTrans = allTransTmp
                deallocate(allTransTmp)
                allocate(allSym(3,3,ntmp))
                allSym   = allSymTmp
                !write(*,*) "allSym111 = "
                !write(*,*)  allSym
                deallocate(allSymTmp)
                allocate(allPerm(size(perm),ntmp))
                allPerm  = allPermTmp
                deallocate(allPermTmp)
                !write(*,*) "I'm here after alltrans"
                !allTrans((ii-1)*nt1+jj,:) = tvectmp(jj,:)
                !allSym(:,:,(ii-1)*nt1+jj) = symopt(:,:,ii)
                !allPerm(:,(ii-1)*nt1+jj)  = perm
                deallocate(perm)
                exit
            else if (.not. failflag) then
                write(*,*) "Error in find_sym_op.f90. ph is not available now."
                stop
                !tmpTrans(jj,:) = tvectmp(jj,:)
                !tmpPerm(:,jj)  = perm
            end if
            !write(*,*) ii, jj, failflag
            !deallocate(perm)
        end do
        !write(*,*) "after int."
        !write(*,*) "allTrans ="
        !write(*,*)  alltrans
        !write(*,*) "allsym = "
        !do kk =1,size(allsym,3),1
        !write(*,*)  allsym(:,:,kk)
        !end do
        !write(*,*) "allPerm ="
        !write(*,*)  allPerm
        if (ph) then
            write(*,*) "Error in find_sym_op.f90. ph is not available now."
            stop
            !nt = size(tmpTrans,1)
            !allTrans(ii,:) = tmpTrans
            !allPerm(:,ii)  = tmpPerm
            !allSym(:,:,ii) = symopt 
        end if
        deallocate(tvectmp,tvectmp2)
    end do

    !write(*,*) "allSym ="
    !write(*,*)  allSym
    
    nsym = size(allSym,3)
    !write(*,*) "size of allSym ", nsym
    allocate(allSym_rec(size(allSym,1),size(allSym,2),nsym))
    allSym_rec = 0
    allocate(avtmp(3,3))
    call inversion(avec,avtmp)
    bvec = 2.0D0*pi*avtmp
    allocate(a2b(3,3),b2a(3,3))
    a2b  = matmul(bvec,avtmp)
    call inversion(a2b,b2a)
    do ii = 1,nsym,1
        alltn = dble(allSym(:,:,ii))
        allout = matmul(a2b,matmul(alltn,b2a))
        do jj = 1,3,1
            do kk = 1,3,1
                allSym_rec(jj,kk,ii) = anint(allout(jj,kk))
            end do
        end do
    end do
    allSym_rec = anint(dble(allSym_rec))
    
    return
    end subroutine find_sym_op
    end module find_sym_op_module
