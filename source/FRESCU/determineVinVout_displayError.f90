! determineVinVout_displayError.f90
    
!*****************************************************************************
    !
    !
    !
!*****************************************************************************
    
    module determineVinVout_displayError_module
    contains
    subroutine determineVinVout_displayError(FRC,vin,vout,rin,rout)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    integer :: iter
    
    ! output variables
    type(VataMN_2D) :: rin, rout
    type(VataMN_1D) :: vin, vout
    
    ! body of this function
    iter = FRC%scloop
    allocate(rin%vata(size(FRC%rho%input(1)%vata,1),size(FRC%rho%input(1)%vata,2)))
    allocate(rout%vata(size(FRC%rho%input(1)%vata,1),size(FRC%rho%input(1)%vata,2)))
    allocate(vin%vata(size(FRC%potential%veffin(1)%vata)))
    allocate(vout%vata(size(FRC%potential%veffin(1)%vata)))
    if (trim(FRC%mixing%mixingType) == trim("density") .and. &
            (trim(FRC%info%calculationType) == trim("self-consistent") .or. & 
            trim(FRC%info%calculationType) == trim("relaxation") .or. &
            trim(FRC%info%calculationType) == trim("phonon"))) then
        !write(*,*) "I'm here mix density and scf in determineVinout."
        rin  = FRC%rho%input(1)
        rout = FRC%rho%output(2)
        vin  = FRC%potential%veffin(1)
        vout = FRC%potential%veffin(2)
    else if (trim(FRC%mixing%mixingType) == trim("potential") .and. &
            (trim(FRC%info%calculationType) == trim("self-consistent") .or. &
            trim(FRC%info%calculationType) == trim("relaxation") .or. & 
            trim(FRC%info%calculationType) == trim("phonon"))) then
        rin  = FRC%rho%output(1)
        rout = FRC%rho%output(2)
        vin  = FRC%potential%veffin(1)
        vout = FRC%potential%veffout
    else if (trim(FRC%mixing%mixingType) == trim("density-matrix") .and. &
            (trim(FRC%info%calculationType) == trim("self-consistent") .or. &
            trim(FRC%info%calculationType) == trim("relaxation") .or. &
            trim(FRC%info%calculationType) == trim("phonon"))) then
        rin  = FRC%rho%output(1)
        rout = FRC%rho%output(2)
        vin  = FRC%potential%veffin(1)
        vout = FRC%potential%veffin(2)
    else if (trim(FRC%mixing%mixingType) == trim("density") .and. &
            (trim(FRC%info%calculationType) == trim("dfpt"))) then
        rin  = FRC%rho%deltaIn
        rout = FRC%rho%deltaOut(2)
        vin  = FRC%potential%deltaIn
        vout = FRC%potential%deltaOut(2)
    else if (trim(FRC%mixing%mixingType) == trim("potential") .and. &
            (trim(FRC%info%calculationType) == trim("dfpt"))) then
        rin  = FRC%rho%deltaOut(1)
        rout = FRC%rho%deltaOut(2)
        vin  = FRC%potential%deltaIn
        vout = FRC%potential%deltaOut(1)
    end if
    
    return
    end subroutine determineVinVout_displayError
    end module determineVinVout_displayError_module
