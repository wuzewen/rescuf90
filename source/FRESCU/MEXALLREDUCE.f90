! MEXALLREDUCE.f90

!***********************************************************************************************
      !
      !
      !
!***********************************************************************************************

      module MEXALLREDUCEMOD

      interface MEXALLREDUCE
              module procedure Mex_Allreduce_1D_real
              module procedure MAR1R
              module procedure MAR2R
              module procedure MAR1I
              module procedure MAR2I
              module procedure MAR1C
              module procedure MAR2C
      end interface MEXALLREDUCE

      end module MEXALLREDUCEMOD
