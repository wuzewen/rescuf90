! ks_main_lcao_scf.f90

!*******************************************************************
      !
      !
      !
!*******************************************************************

      module ks_main_lcao_scf_module
      contains
      subroutine ks_main_lcao_scf(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC

      ! temporary variables
      integer :: mpisize, diaglim, iter, nkpt, nabnd, ispin, nspin
      integer :: ntmp
      real*8  :: kpt(3)

      integer, allocatable :: sv(:,:), tv(:,:)
      real*8 , allocatable :: kdir(:,:)

      type(VataMN_1D) :: veff
      type(VataMN_2D) :: aok
      type(VataMN_2D), allocatable :: Scell(:), Tcell(:), aocell(:)
      

      ! output variables

      ! body of this function
      mpisize = FRC%mpi%mpisize
      mpirank = FRC%mpi%rank

      if (FRC%LCAO%cfsi) then
              diaglim = 1
      else
              diaglim = FRC%option%maxSCFiteration(1)+1
      end if

      buff   = FRC%option%bufferSize
      sprs   = FRC%LCAO%sprs
      iter   = FRC%scloop
      nkpt   = size(FRC%Kpoint%ikdirect,1)
      allocate(kdir(nkpt,3))
      kdir   = FRC%kpoint%ikirect
      !nkpt   = size(kdir,1)
      nabnd  = FRC%eigenSolver%nband
      ispin  = FRC%spin%ispin
      nspin  = FRC%spin%nspin
      
      FRC%energy%ksnrg = 0.0D0

      Scell  = FRC%LCAO%Scell
      sv     = FRC%LCAO%Svec
      !Tcell  = FRC%LCAO%Tcell
      !tv     = FRC%LCAO%Tvec
      call linearizeCellDistArray(Scell,buff,sprs,sdata)

      if (FRC%LCAO%dynSubRed .and. (.not. FRC%LCAO%veffRed)) then
              aocell   = FRC%LCAO%aocell
              aov      = FRC%LCAO%aovec
              aosparse = FRC%LCAO%aosparse
      else if (.not. FRC%LCAO%veffRed) then
              aocell   = FRC%LCAO%aokcell
      end if

      inclVeff = FRC%LCAO%veffRed
      call recalcLCAOHam(FRC,inclVeff,.FALSE.,.FALSE.,.FALSE.,Hcell,hv)
      do ss = 1,size(Hcell,2),1
          call linearizeCellDistArray(Hcell(:,ss),buff,sprs,hdata(ss))
      end do

      ntmp = ceiling(dble(veff%m)/dble(mpisize))

      if (.NOT. FRC%LCAOveffRed) then
              call UpdateVeff(FRC,FRC%potential%veffin,FRC%potential%vps,veff)
              call ModBCDist_1D(FRC,veff,ntmp,1,mpisize,1,.FALSE.,veff)
      end if

      allocate(showkpt(nkpt))
      showkpt = .FALSE.
      evOnly  = .FALSE.

      calcType = FRC%info%calculationType
      if (trim(calcType) .EQ. trim('band-structure') .OR. &
              trim(calcType) .EQ. trim('band-unfolding') .OR. &
              trim(calcType) .EQ. trim('dos') .OR. &
              trim(calcType) .EQ. trim('force') .OR. &
              trim(calcType) .EQ. trim('mulliken') .OR. &
              trim(calcType) .EQ. trim('potential') .OR. &
              trim(calcType) .EQ. trim('wannier')) then 
              if ((.NOT. FRC%dos%pdos) .AND. &
                      (.NOT. FRC%option%saveWavefunction) &
                      (.NOT. FRC%option%saveLCAO) &
                      (.NOT. FRC%dos%ldos)   &
                      (.NOT. FRC%LCAO%mulliken) &
                      (.NOT. FRC%option%savePartialWeights) &
                      (.NOT. trim(calcType) .EQ. trim('band-folding')) &
                      (.NOT. trim(calcType) .EQ. trim('wannier'))) then
                  evOnly = .TRUE.
              end if
              ntmp = ceiling(real(nkpt)/100.0D0)
              forall(ii=1:ntmp:nkpt)
                      showkpt(ii) = .TRUE.
              end forall
      else
              if (ispin .EQ. 4) then
                      call repmat(nkpt,2,FRC%LCAO%coeff)
              end if
      end if


      !if (FRC%LCAO%veffRed) then
      !        call calcVeffCell(FRC,veff,Vcell,vv)
      !        FRC%LCAO%Vcell = Vcell
      !        FRC%LCAO%Vvec  = vv
      !end if

      do kk = 1,nkpt,1
          kpt = kdir(kk,:)
          !call reduceOper(Scell,sv,kpt,FRC%XSX)
          !call reduceOper(Tcell,tv,kpt,FRC%XHX)
          call reduceOperGen(Scell,sdata,sv,kpt,nnzSmask,FRC%XSX)

          if (FRC%LCAO%dynSubRed .and. (.not. FRC%LCAO%veffRed)) then
                  call reduceOper(aocell,aov,kpt,aosparse,aok)
          else if (.not. FRC%LCAO%veffRed) then
                  aok = aocell(kk)
          end if

          !if (trim(FRC%spin%spintype) == trim('non-collinear')) then
          !        write(*,*) "Error in ks_main_lcao_scf. SPIN"
          !end if
          if (ispin .EQ. 4) then
              do ss = 1,3,1
                  call reduceOperGen(Hcell(:,ss),hdata(ss),hv,kpt, &
                          nnzSmask,FRC%XHX(ss)
                  if (.NOT. FRC%LCAO%veffRed) then
                      veffss = veff
                      colInd = (/1,3,4/)
                      veffss%vata = veffss%vata(:,colInd(ss))
                      veffss%n = 1
                      call projectVeff(FRC,aok,aok,veffss,tmp)
                      FRC%XHX(ss)%vata = FRC%XHX(ss)%vata+tmp%vata
                  end if
              end do
          end if


          do ss = 1,nspin,1
              if (ispin .EQ. 1 .OR. ispin .EQ. 2) then
                  call reduceOperGen(Hcell(:,ss),hdata(ss),hv,kpt,nnzSmask,FRC%XHX)
                  if (.NOT. FRC%LCAO%veffRed) then
                      veffss%m      = veff%m
                      veffss%n      = 1
                      veffss%mblock = veff%mblock
                      veffss%nblock = veff%nblock
                      veffss%mproc  = veff%mproc
                      veffss%nproc  = veff%nproc
                      veffss%vata   = veff%vata(:,ss)
                      call projecVeff(FRC,aok,aok,veffss,tmp)
                      FRC%XHX%vata = FRC%XHX%vata+tmp%vata
                  end if
              end if

              if (ispin .EQ. 1 .OR. ispin .EQ. 2 .AND. .NOT. evOnly) then
                  call diag_lcao(FRC,.TRUE.,FRC%LCAO%coeff(kk,ss),FRC%energy%ksnrg(:,kk,ss))
              else if (ispin .EQ. 1 .OR. ispin .EQ. 2) then
                  call diag_lcao(FRC,.TRUE.,.TRUE.,FRC%energy%ksnrg(:,kk,ss))
              else if (ispin .EQ. 4 .AND. .NOT. evOnly) then
                  call diag_lcao(FRC,FRC%LCAO%coeff(kk,1:2),FRC%energy%ksnrg(:,kk,ss))
              else if (ispin .EQ. 4) then
                  call diag_lcao(FRC,.TRUE.,FRC%energy%ksnrg(:,kk,ss))
              end if
          end do
      end do

      !        if (FRC%LCAO%veffRed .and. (ispin == 1 .or. ispin == 2)) then
      !                Vcelltmp = Vcell(:,ii)
      !                call reduceOper(Vcelltmp,vv,kpt,FRC%XVX)
      !        else if (ispin == 1 .and. ispin == 2) then
      !                veffii%vata   = veff%vata(:,ii)
      !                veffii%m      = veff%m
      !                veffii%n      = 1
      !                veffii%mblock = veff%mblock
      !                veffii%nblock = veff%nblock
      !                veffii%mproc  = veff%mproc
      !                veffii%nproc  = veff%nproc
      !                call projectVeff(FRC,aok,aok,veffii,FRC%XVX)
      !        end if

      !        if (FRC%functional%hybrid) then
      !                write(*,*) "Error in ks_main_lcao_scf. Hybrid"
      !        end if

      !        if (iter <= diaglim .and. (ispin == 1 .and. ispin == 2)) then
      !                if (any(kpt /= 0)) then
      !                        iskpt = .TRUE.
      !                else
      !                        iskpt = .FALSE.
      !                end if
      !                call diag_lcao(FRC,iskpt,FRC%psi(kk,ii),ksnrg)
      !                FRC%energy%ksnrg(:,kk,ii) = ksnrg
      !        else if (iter <= diaglim .and. ispin == 4) then
      !                write(*,*) "Error in ks_main_lcao_scf. SPIN"
      !        else
      !                if (.not. FRC%option%plorder) then
      !                        call reduceHamiltonian(FRC)
      !                end if
      !                call CFSI_lcao(FRC)
      !        end if
      !    end do
      !end do

      return
      end subroutine ks_main_lcao_scf
      end module ks_main_lcao_scf_module

