! readPseudoPotential.f90
!***************************************
    !
!***************************************
    
    module readPseudoPotential_module
    contains
    subroutine readPseudoPotential(species,SOB,ElementData)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    !type :: SummeryOfBas
    !    integer  :: SizeOfVlocal,    SizeOfVlocalrrData,    SizeOfVlocaldrData,    SizeOfVlocalvvData
    !    integer  :: SizeOfRlocal,    SizeOfRlocalrrData,    SizeOfRlocaldrData,    SizeOfRlocalrhoData
    !    integer  :: SizeOfVnl,       SizeOfVnlrrData,       SizeOfVnldrData,       SizeOfVnlvvData,                SizeOfVnlqqData,                  SizeOfVnlfqData,       SizeOfVnlqwData
    !    integer  :: SizeOfVna,       SizeOfVnarrData,       SizeOfVnadrData,       SizeOfVnavvData
    !    integer  :: SizeOfRna,       SizeOfRnarrData,       SizeOfRnadrData,       SizeOfRnarhoData
    !    integer  :: SizeOfRelPseudoP,SizeOfRelPseudoPrrData,SizeOfRelPseudoPdrData,SizeOfRelPseudoPvvData_screened,SizeOfRelPseudoPvvData_unscreened
    !    integer  :: SizeOfOrbitalSet,SizeOfOrbitalSetrrData,SizeOfOrbitalSetdrData,SizeOfOrbitalSetfrData,         SizeOfOrbitalSetqqData,           SizeOfOrbitalSetfqData,SizeOfOrbitalSetqwData
    !end type SummeryBas
    type(SummeryOfBas) :: SOB
    
    ! temporary variables
    character(len=100)  :: nonsense, filename
    character(len=4)    :: species
    integer             :: ii, jj
    real*8              :: tmp(10)
    real*8, allocatable :: tnp(:)
    
    ! output variables
    !type :: ElementType
    !    type :: RelPseudoPtype
    !        integer              :: N, L
    !        real*8               :: J, Rc
    !        real*8, allocatable  :: rrData(:), drData(:), vvData_screened(:), vvData_unscreened(:)
    !    end type RelPseudoPtype
    !    type(RelPseudoPtype), allocatable :: RelPseudoP(:)
    !    
    !    type :: VlocalType
    !        real*8               :: Rcore, Ncore
    !        real*8, allocatable  :: rrData(:), drData(:), vvData(:)
    !    end type VlocalType
    !    type(VlocalType)  :: Vlocal
    !    
    !    type :: RlocalType
    !        real*8, allocatable  :: rrData(:), drData(:), rhoData(:)
    !    end type RlocalType
    !    type(RlocalType)  :: Rlocal
    !    
    !    type :: OrbitalSetType
    !        real*8, allocatable :: rrData(:), drData(:), frData(:), qqData(:), fqData(:), qwData(:)
    !        integer             :: N, L
    !        real*8              :: E, Population, CoulombEnergyU, ExchangeEnergyJ
    !        character(len=20)   :: OrbitalType
    !    end type OrbitalSetType
    !    type(OrbitalSetType), allocatable :: OrbitalSet(:)
    !    
    !    type :: VnlType
    !        real*8, allocatable :: rrData(:), drData(:), vvData(:), qqData(:), fqData(:), qwData(:)
    !        integer :: L
    !        real*8  :: KBenergy, KBcosine, E
    !        logical :: isGhost
    !    end type VnlType
    !    type(VnlType), allocatable :: Vnl(:)
    !    
    !    type :: VnaType
    !        real*8, allocatable :: rrData(:), drData(:), vvData(:)
    !    end type VnaType
    !    type(VnaType) :: Vna
   ! 
    !    type :: RnaType
    !        real*8, allocatable :: rrData(:), drData(:), rhoData(:)
    !    end type RnaType
    !    type(RnaType) :: Rna
    !    
    !    type :: AtomType
    !        integer           :: Z, N
    !        character(len=4)  :: Symbol
    !        character(len=20) :: shell
    !        character(len=20) :: name
    !        real*8            :: mass
    !    end type AtomType
    !    type(atomType) :: Atom
    !    
    !end type ElementType
    
    type(ElementDataType) :: ElementData
    
    integer :: NumOfLine, Remainder, ntmp
    
    ! body of this function
    
    !write(*,*) "I'm here reading basis."
    filename = trim(species)//".bas"
    !fileName = 'C.bas'
    open(unit=10,file=fileName,status='old',access='sequential',action='read')
    ntmp = 34+SOB%Orbit
    do ii = 1,ntmp,1
        read(10,*) nonsense
    end do
    !write(*,*) nonsense   
    ! Vlocal
    read(10,*) nonsense  ! 037
    read(10,*) nonsense
    read(10,*) ElementData%Vlocal%Rcore, ElementData%Vlocal%Ncore
    
    read(10,*) nonsense     ! rrData
    NumOfLine = SOB%Vlocrr/10
    Remainder = SOB%Vlocrr-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) tmp(:)
        ElementData%Vlocal%rrData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Vlocrr-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Vlocal%rrData(NumOfLine*10+1:SOB%Vlocrr) = tnp(:)
        deallocate(tnp)
    end if
    
    read(10,*) nonsense     ! drData
    NumOfLine = SOB%Vlocdr/10
    Remainder = SOB%Vlocdr-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) tmp(:)
        ElementData%Vlocal%drData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Vlocdr-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Vlocal%drData(NumOfLine*10+1:SOB%Vlocdr) = tnp(:)
        deallocate(tnp)
    end if
    
    read(10,*) nonsense     ! vvData
    NumOfLine = SOB%Vlocvv/10
    Remainder = SOB%Vlocvv-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) tmp(:)
        ElementData%Vlocal%vvData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Vlocvv-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Vlocal%vvData(NumOfLine*10+1:SOB%Vlocvv) = tnp(:)
        deallocate(tnp)
    end if
    !write(*,*) "I'm here after Vlocal."
    ! Rlocal 
    read(10,*) nonsense
    
    read(10,*) nonsense  ! rrData
    NumOfLine = SOB%Rlocrr/10
    Remainder = SOB%Rlocrr-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) tmp(:)
        ElementData%Rlocal%rrData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Rlocrr-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Rlocal%rrData(NumOfLine*10+1:SOB%Rlocrr) = tnp
        deallocate(tnp)
    end if
    
    read(10,*) nonsense  ! drData
    NumOfLine = SOB%Rlocdr/10
    Remainder = SOB%Rlocdr-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) tmp(:)
        ElementData%Rlocal%drData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Rlocdr-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Rlocal%drData(NumOfLine*10+1:SOB%Rlocdr) = tnp(:)
        deallocate(tnp)
    end if
    
    read(10,*) nonsense  ! rhoData
    NumOfLine = SOB%Rlocrho/10
    Remainder = SOB%Rlocrho-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) tmp(:)
        ElementData%Rlocal%rhoData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Rlocrho-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Rlocal%rhoData(NumOfLine*10+1:SOB%Rlocrho) = tnp(:)
        deallocate(tnp)
    end if
    !write(*,*) "I'm here after Rlocal"
    ! Vnl
    read(10,*) nonsense
    read(10,*) nonsense
    
    do jj = 1,SOB%Vnl
        read(10,*) nonsense
        
        !write(*,*) nonsense
        
        read(10,*) nonsense
        !read(10,*) nonsense
        !write(*,*) jj
        !write(*,*) nonsense
        !write(*,*) SOB%SizeOfVnl
        
        read(10,*) ElementData%Vnl(jj)%L,ElementData%Vnl(jj)%KBenergy, &
                ElementData%Vnl(jj)%KBcosine,ElementData%Vnl(jj)%E,ElementData%Vnl(jj)%isGhost
        
        !write(*,*) ElementData%Vnl(jj)%L,ElementData%Vnl(jj)%KBenergy,ElementData%Vnl(jj)%KBcosine,ElementData%Vnl(jj)%E,ElementData%Vnl(jj)%isGhost
        
        read(10,*) nonsense
        
        !write(*,*) nonsense
        
        NumOfLine = SOB%Vnlrr/10
        Remainder = SOB%Vnlrr-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) tmp(:)
            ElementData%Vnl(jj)%rrData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%Vnlrr-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%Vnl(jj)%rrData(NumOfLine*10+1:SOB%Vnlrr) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense ! drData
        NumOfLine = SOB%Vnldr/10
        Remainder = SOB%Vnldr-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) tmp(:)
            ElementData%Vnl(jj)%drData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%Vnldr-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%Vnl(jj)%drData(NumOfLine*10+1:SOB%Vnldr) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense ! vvData
        NumOfLine = SOB%Vnlvv/10
        Remainder = SOB%Vnlvv-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) tmp(:)
            ElementData%Vnl(jj)%vvData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%Vnlvv-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%Vnl(jj)%vvData(NumOfLine*10+1:SOB%Vnlvv) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense ! qqData
        NumOfLine = SOB%Vnlqq/10
        Remainder = SOB%Vnlqq-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) tmp(:)
            ElementData%Vnl(jj)%qqData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%Vnlqq-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%Vnl(jj)%qqData(NumOfLine*10+1:SOB%Vnlqq) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense ! fqData
        NumOfLine = SOB%Vnlfq/10
        Remainder = SOB%Vnlfq-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) tmp(:)
            ElementData%Vnl(jj)%fqData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%Vnlfq-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%Vnl(jj)%fqData(NumOfLine*10+1:SOB%Vnlfq) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense ! qqData
        NumOfLine = SOB%Vnlqw/10
        Remainder = SOB%Vnlqw-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) tmp(:)
            ElementData%Vnl(jj)%qwData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%Vnlqw-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%Vnl(jj)%qwData(NumOfLine*10+1:SOB%Vnlqw) = tnp(:)
            deallocate(tnp)
        end if
    end do
    
    !write(*,*) "end of Vnl"
    
    ! Vna
    read(10,*) nonsense
    
    read(10,*) nonsense  ! rrData
    NumOfLine = SOB%Vnarr/10
    Remainder = SOB%Vnarr-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) tmp(:)
        ElementData%Vna%rrData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Vnarr-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Vna%rrData(NumOfLine*10+1:SOB%Vnarr) = tnp(:)
        deallocate(tnp)
    end if
    
    read(10,*) nonsense  ! drData
    NumOfLine = SOB%Vnadr/10
    Remainder = SOB%Vnadr-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) tmp(:)
        ElementData%Vna%drData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Vnadr-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Vna%drData(NumOfLine*10+1:SOB%Vnadr) = tnp(:)
        deallocate(tnp)
    end if
    
    read(10,*) nonsense  ! vvData
    NumOfLine = SOB%Vnavv/10
    Remainder = SOB%Vnavv-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) tmp(:)
        ElementData%Vna%vvData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Vnavv-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Vna%vvData(NumOfLine*10+1:SOB%Vnavv) = tnp(:)
        deallocate(tnp)
    end if
    !write(*,*) "I'm here after Rna"
    ! Rna
    read(10,*) nonsense
    
    read(10,*) nonsense  ! rrData
    NumOfLine = SOB%Rnarr/10
    Remainder = SOB%Rnarr-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) tmp(:)
        ElementData%Rna%rrData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Rnarr-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Rna%rrData(NumOfLine*10+1:SOB%Rnarr) = tnp(:)
        deallocate(tnp)
    end if
    
    read(10,*) nonsense  ! drData
    NumOfLine = SOB%Rnadr/10
    Remainder = SOB%Rnadr-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) tmp(:)
        ElementData%Rna%drData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Rnadr-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Rna%drData(NumOfLine*10+1:SOB%Rnadr) = tnp(:)
        deallocate(tnp)
    end if
    
    read(10,*) nonsense  ! rhoData
    NumOfLine = SOB%Rnarho/10
    Remainder = SOB%Rnarho-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) tmp(:)
        ElementData%Rna%rhoData((ii-1)*10+1:ii*10) = tmp(:)
    end do
    if (Remainder/=0) then
        allocate(tnp(SOB%Rnarho-NumOfLine*10))
        read(10,*) tnp(:)
        ElementData%Rna%rhoData(NumOfLine*10+1:SOB%Rnarho) = tnp(:)
        deallocate(tnp)
    end if
    
    !write(*,*) ElementData%Rna%rhoData(NumOfLine*10+1:SOB%SizeOfRnarhoData)
    !write(*,*) "before RelPseudoP"
    ! RelPseudoP
    read(10,*) nonsense
    read(10,*) nonsense
    
    do jj = 1,SOB%PseP,1
        read(10,*) nonsense
        read(10,*) nonsense
        
        !write(*,*) jj
        !write(*,*) nonsense
        
        !read(10,*) nonsense
        
        !write(*,*) nonsense
        
        read(10,*) ElementData%RelPseudoP(jj)%N, ElementData%RelPseudoP(jj)%L, &
                ElementData%RelPseudoP(jj)%J, ElementData%RelPseudoP(jj)%Rc
        
        read(10,*) nonsense  ! rrData
        
        !write(*,*) nonsense
        
        NumOfLine = SOB%PsePrr/10
        Remainder = SOB%PsePrr-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%RelPseudoP(jj)%rrData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%PsePrr-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%RelPseudoP(jj)%rrData(NumOfLine*10+1:SOB%PsePrr) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense  ! drData
        NumOfLine = SOB%PsePdr/10
        Remainder = SOB%PsePdr-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%RelPseudoP(jj)%drData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%PsePdr-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%RelPseudoP(jj)%drData(NumOfLine*10+1:SOB%PsePdr) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense  ! vvData_screened
        NumOfLine = SOB%PsePvvS/10
        Remainder = SOB%PsePvvS-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%RelPseudoP(jj)%vvData_screened((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%PsePvvS-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%RelPseudoP(jj)%vvData_screened(NumOfLine*10+1:SOB%PsePvvS) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense  ! vvData_unscreened
        NumOfLine = SOB%PsePvvU/10
        Remainder = SOB%PsePvvU-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%RelPseudoP(jj)%vvData_unscreened((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%PsePvvU-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%RelPseudoP(jj)%vvData_unscreened(NumOfLine*10+1:SOB%PsePvvU) = tnp(:)
            deallocate(tnp)
        end if
    end do
    !write(*,*) "before orbitalset"
    ! OrbitalSet
    read(10,*) nonsense
    read(10,*) nonsense
    do jj = 1,SOB%Orbit,1
        read(10,*) nonsense
        read(10,*) nonsense
        read(10,*) ElementData%OrbitalSet(jj)%N,ElementData%OrbitalSet(jj)%L, &
                ElementData%OrbitalSet(jj)%OrbitalType,ElementData%OrbitalSet(jj)%E, &
                ElementData%OrbitalSet(jj)%population, &
                ElementData%OrbitalSet(jj)%CoulombEnergyU, &
                ElementData%OrbitalSet(jj)%ExchangeEnergyJ
        
        read(10,*) nonsense  ! rrData
        
        !write(*,*) jj
        !write(*,*) nonsense
        
        NumOfLine = SOB%orbi(jj)%rr/10
        Remainder = SOB%orbi(jj)%rr-10*NumOfLine
        !write(*,*) "start of data"
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%OrbitalSet(jj)%rrData((ii-1)*10+1:ii*10) = tmp(:)
            !write(*,*) ElementData%OrbitalSet(jj)%rrData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%orbi(jj)%rr-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%OrbitalSet(jj)%rrData(NumOfLine*10+1:SOB%orbi(jj)%rr) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense  ! drData
        NumOfLine = SOB%orbi(jj)%dr/10
        Remainder = SOB%orbi(jj)%dr-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%OrbitalSet(jj)%drData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
                allocate(tnp(SOB%orbi(jj)%dr-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%OrbitalSet(jj)%drData(NumOfLine*10+1:SOB%orbi(jj)%dr) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense  ! frData
        NumOfLine = SOB%orbi(jj)%fr/10
        Remainder = SOB%orbi(jj)%fr-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%OrbitalSet(jj)%frData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%orbi(jj)%fr-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%OrbitalSet(jj)%frData(NumOfLine*10+1:SOB%orbi(jj)%fr) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense  ! qqData
        NumOfLine = SOB%orbi(jj)%qq/10
        Remainder = SOB%orbi(jj)%qq-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%OrbitalSet(jj)%qqData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%orbi(jj)%qq-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%OrbitalSet(jj)%qqData(NumOfLine*10+1:SOB%orbi(jj)%qq) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense  ! fqData
        NumOfLine = SOB%orbi(jj)%fq/10
        Remainder = SOB%orbi(jj)%fq-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%OrbitalSet(jj)%fqData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%orbi(jj)%fq-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%OrbitalSet(jj)%fqData(NumOfLine*10+1:SOB%orbi(jj)%fq) = tnp(:)
            deallocate(tnp)
        end if
        
        read(10,*) nonsense  ! qwData
        NumOfLine = SOB%orbi(jj)%qw/10
        Remainder = SOB%orbi(jj)%qw-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) tmp(:)
            ElementData%OrbitalSet(jj)%qwData((ii-1)*10+1:ii*10) = tmp(:)
        end do
        if (Remainder/=0) then
            allocate(tnp(SOB%orbi(jj)%qw-NumOfLine*10))
            read(10,*) tnp(:)
            ElementData%OrbitalSet(jj)%qwData(NumOfLine*10+1:SOB%orbi(jj)%qw) = tnp(:)
            deallocate(tnp)
        end if
    end do
    
    close(10)
    
    return
    end subroutine readPseudoPotential
    end module readPseudoPotential_module
