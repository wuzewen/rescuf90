! DISTMATALLGATHER.f90

!***************************************************************************
      !
      !
      !
!***************************************************************************

      module DISTMATALLGATHERMOD

      interface DISTMATALLGATHER
              module procedure distmat_allgather_real_1D
              module procedure distmat_allgather_real_2D
              module procedure distmat_allgather_cmplx_1D
              module procedure distmat_allgather_cmplx_2D
      end interface DISTMATALLGATHER

      end module DISTMATALLGATHERMOD
