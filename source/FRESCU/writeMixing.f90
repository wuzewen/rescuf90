! writeMixing.f90

      module writeMixing_module

      contains
      subroutine writeMixing(FRC,filnam)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam

      open(unit=11,file=filnam)

      write(11,*) "alpha"
      write(11,*)  FRC%mixing%alpha
      write(11,*) "beta"
      write(11,*)  FRC%mixing%beta
      write(11,*) "betalin"
      write(11,*)  FRC%mixing%betalin
      write(11,*) "initlin"
      write(11,*)  FRC%mixing%initlin
      write(11,*) "lambda"
      write(11,*)  FRC%mixing%lambda
      write(11,*) "maxHistory"
      write(11,*)  FRC%mixing%maxHistory
      write(11,*) "method"
      write(11,*)  FRC%mixing%method
      write(11,*) "tol"
      write(11,*)  FRC%mixing%tol
      write(11,*) "dRho"
      write(11,*)  FRC%mixing%dRho
      write(11,*) "MixingType"
      write(11,*)  FRC%mixing%MixingType
      
      close(11)
      return
      end subroutine writeMixing
      end module writeMixing_module
