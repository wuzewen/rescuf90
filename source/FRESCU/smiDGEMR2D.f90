! smiDGEMR2D.f90

!************************************
      !
      !
      !
!************************************

      module smiDGEMR2DMOD
      contains
      subroutine smiDGEMR2D(A,B,nprocs,M,N,MBA,NBA,nprowA,&
                      npcolA,erderA,MBB,NBB,nprowB,npcolB,oderB)

      implicit none

      integer :: nprocs, M, N
      integer :: MBA, NBA, nprowA, npcolA
      integer :: MBB, NBB, nprowB, npcolB

      character(len=1) :: orderA, orderB

      real*8, allocatable :: A(), B()

      integer :: iam, ictxtA, ictxtB, myrowA, mycolA, myrowB, mycolB

      iam    = 0
      ictxtA = 0
      ictxtB = 0
      myrowA = 0
      mycolA = 0
      myrowB = 0
      mycolB = 0

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxtA)
      call blacs_gridinit(ictxtA,orderA,nprowA,npcolA)
      call blacs_gridinfo(ictxtA,nprowA,npcolA,myrowA,mycolA)
      call blacs_get(-1,0,ictxtB)
      call blacs_gridinit(ictxtB,orderB,nprowB,npcolB)
      call blacs_gridinfo(ictxtB,nprowB,npcolB,myrowB,mycolB)

      izero = 0
      ione  = 1
      call numroc(M,MBA,myrowA,izero,nprowA,A_np)
      A_np = 
      call numroc(M,MBB,myrowB,izero,nprowB,B_np)
      B_np =

      call descinit(descA,M,N,MBA,NBA,izero,izero,ictxtA,A_np,infoA)
      call descinit(descB,M,N,MBB,NBB,izero,izero,ictxtB,B_np,infoB)

      call PDGEMR2D(M,N,A,ione,ione,descA,B,ione,ione,descB,ictxtA)

      call blacs_gridexit(ictxtA)
      call blacs_gridexit(ictxtB)

      return
      end subroutine smiDGEMR2D
      end module smiDGEMR2DMOD
