! FRC_RHO_TYPE.f90
    
!********************************************************************************
    !
    ! This type will be used in FRC structure data to contain rho infomation
    !
!********************************************************************************
    
    module FRC_RHO_TYPE
    
    use RHO_type
    use RHOdm_type
    use VataMN_1D_module
    use VataMN_2D_module
    
    type :: FRCrhoType
        character(len=20)            :: inputfile
        type(VataMN_2D)              :: input(2)
        type(VataMN_2D)              :: output(2)
        type(VataMN_2D)              :: deltain
        type(VataMN_2D)              :: deltaout(3)
        type(VataMN_2D)              :: atom
        type(VataMN_2D)              :: gradRpc
        type(VataMN_2D)              :: pc
    end type FRCrhoType
    
    end module FRC_RHO_TYPE
