!  updateOccupancy.f90 
!****************************************************************************
!
!  PROGRAM: updateOccupancy
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module updateOccupancy_module
    contains
    subroutine updateOccupancy(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use calcoccupancy_module
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    integer             :: iter
    real*8              :: mu, entropy
    real*8, allocatable :: energy(:,:,:), nocc(:,:,:), eks(:,:), ener2(:,:), nocc2(:,:)
    
    ! output variables

    ! Body of updateOccupancy
    !write(*,*) "I'm here at the begining of updateOccupancy."
    !allocate(energy(size(FRC%energy%ksnrg(iter)%vata,1),size(FRC%energy%ksnrg(iter)%vata,2),size(FRC%energy%ksnrg(iter)%vata,3)))
    iter   = FRC%scloop
    allocate(energy(size(FRC%energy%ksnrg%vata,1),size(FRC%energy%ksnrg%vata,2),size(FRC%energy%ksnrg%vata,3)))
    energy = FRC%energy%ksnrg%vata
    !write(*,*) "size of FRC%energy%ksnrg(iter)%vata"
    !write(*,*)  size(FRC%energy%ksnrg(iter)%vata,1),size(FRC%energy%ksnrg(iter)%vata,2),size(FRC%energy%ksnrg(iter)%vata,3)
    !write(*,*) "size of energy"
    !write(*,*)  size(energy,1),size(energy,2),size(energy,3)
    !write(*,*) "energy ="
    !write(*,*)  energy
    !write(*,*) "I'm here before calcOccupancy in updateOccupancy."
    call calcoccupancy(FRC,energy,mu,nocc,entropy)
    !write(*,*) "I'm here after calcOccupancy in updateOccupancy."
    FRC%energy%EFermi    = mu
    FRC%energy%nocc      = nocc
    !FRC%energy%entropy   = entropy
    !eks                  = ener2*nocc2
    !FRC%energy%Eks(iter) = sum(eks)
    !write(*,*) "output of updateOccupancy."
    !write(*,*)  nocc
    !write(*,*)  entropy
    !write(*,*) "I'm here at the end of updateOccupancy."
    return
    end subroutine updateOccupancy
    end module updateOccupancy_module

