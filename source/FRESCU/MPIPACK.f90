! MPIPACK.f90

!******************************************************************************
      !
      !
      !
!******************************************************************************

      module MPIPACKMOD

      interface MPIPACK
              module procedure MPI_Pack_integ_1D
              module procedure MPI_Pack_integ_2D
              module procedure MPI_Pack_integ_3D
              module procedure MPI_Pack_real_1D
              module procedure MPI_Pack_real_2D
              module procedure MPI_Pack_real_3D
      end interface MPIPACK

      end module MPIPACKMOD
