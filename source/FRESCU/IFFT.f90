! IFFT.f90

!****************************************************************************************
      !
      !
      !
!****************************************************************************************

      module IFFTMOD

      interface IFFT
              module procedure ifft2
              module procedure ifft_fftw
              module procedure ifft_fftw_pointer
              module procedure ifft4
      end interface IFFT
      end module IFFTMOD
