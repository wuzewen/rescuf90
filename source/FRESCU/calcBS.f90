! calcBS.f90

      module calcBS_module
      contains
      subroutine calcBS(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use hamOper2Mat_module
      use hamOper2Mat_Sparse_module

      external ZHEEV

      !implicit none
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      integer :: ntmp, ii, N, LWORK, INFO, nabnd, LDA
      integer :: nband, jj
      real*8  :: kpt(3)
      character(len=1) :: JOBZ, UPLO
      complex*16, allocatable :: H(:,:), WORK(:)
      real*8,     allocatable :: RWORK(:), W(:), kpoint(:,:), bs(:,:)

      open(unit=11,file='bsk.txt',status='old',access='sequential',action='read')
      read(11,*) ntmp

      allocate(kpoint(ntmp,3))
      do ii = 1,ntmp,1
          read(11,*) kpt
          kpoint(ii,:) = kpt(:)
      end do
      close(11)
      !write(*,*) "I'm here after read K."
      write(*,*) "Band Structure calculation starts."
      FRC%scloop = FRC%scloop+1
      nband      = FRC%eigenSolver%nband
      JOBZ       = 'V'
      UPLO       = 'U'
      N          = product(FRC%domain%cgridn)
      LDA        = N
      allocate(W(N))
      LWORK      =  2*N-1
      allocate(WORK(LWORK))
      allocate(RWORK(3*N-2))
      allocate(H(N,N))
      allocate(bs(nband,ntmp))
      !write(*,*) "I'm here before loop."
      do ii = 1,ntmp,1
          write(*,*) "calculating",ii,"of",ntmp,"kpoints."
          kpt = kpoint(ii,:)
          !write(*,*) "I'm here after kpt."
          call hamOper2mat_Sparse(FRC,kpt,H)
          !write(*,*) "I'm here after hamOper2mat."
          call ZHEEV(JOBZ,UPLO,N,H,LDA,W,WORK,LWORK,RWORK,INFO)
          !write(*,*) "I'm here after ZHEEV."
          bs(:,ii) = W(1:nband)
      end do 
      !write(*,*) "I'm here after loop"
      open(unit=11,file='bs.out')
      do ii = 1,ntmp,1
          do jj = 1,nband,1
              write(11,*) bs(jj,ii)
          end do
      end do

      close(11)

      return
      end subroutine calcBS
      end module calcBS_module
