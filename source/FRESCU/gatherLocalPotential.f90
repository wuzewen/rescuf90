! gatherLocalPotential.f90
    
!****************************************************
    !
    !
    !
!****************************************************
    
    module gatherLocalPotential_module
    contains
    subroutine gatherLocalPotential(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    use VataMN_2D_module
    use updateVeff_module
    use UpdateVeff2D_module
    use distmat_allgather_module
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    !integer                         :: iter
    
    ! temporary variables
    logical :: anterp
    integer :: iter, ii
    type(VataMN_1D) :: vefftmp
    type(VataMN_2D) :: veff, veffin
    real*8, allocatable :: vloc(:)
    ! output variables
    
    ! body of this function
    !write(*,*) "I'm here at the begining in gatherLocalPotential"
    iter   = FRC%scloop
    anterp = .not. FRC%interpolation%vloc
    allocate(veffin%vata(size(FRC%potential%veffin(1)%vata),1))
    !write(*,*) "veffin, in gatherLocalPotential"
    !write(*,*)  FRC%potential%veffin(1)%vata
    veffin%vata(:,1) = FRC%potential%veffin(1)%vata
    veffin%m         = FRC%potential%veffin(1)%m
    veffin%n         = FRC%potential%veffin(1)%n
    veffin%mproc     = FRC%potential%veffin(1)%mproc
    veffin%nproc     = FRC%potential%veffin(1)%nproc
    veffin%mblock    = FRC%potential%veffin(1)%mblock
    veffin%nblock    = FRC%potential%veffin(1)%nblock
    !write(*,*) "I'm here before UpdateVeff in gatherLocalPotential"
    call UpdateVeff2D(FRC,veffin,FRC%potential%vps,anterp,veff)

    !write(*,*) "FRC%potential%vps="
    !do ii = 1,size(FRC%potential%vps%vata),1
    !write(*,*)  FRC%potential%vps%vata(ii)
    !end do
    !write(*,*) "veffin in gatherLocalPotential."
    !write(*,*)  veffin%vata
    !write(*,*) "veff   in gatherLocalPotential."
    !write(*,*)  veff%vata
    !write(*,*) "I'm here after UpdateVeff in gatherLocalPotential"
    allocate(vefftmp%vata(size(FRC%potential%veffin(1)%vata)))
    vefftmp%vata      = veff%vata(:,1)
    vefftmp%m         = veff%m
    vefftmp%n         = veff%n
    vefftmp%mproc     = veff%mproc
    vefftmp%nproc     = veff%nproc
    vefftmp%mblock    = veff%mblock
    vefftmp%nblock    = veff%nblock
    !write(*,*) "I'm here before distmat in gatherLocalPotential"
    !write(*,*) "size of vefftmp%vata"
    !write(*,*)  size(vefftmp%vata)
    allocate(vloc(size(FRC%potential%veffin(1)%vata)))
    call distmat_allgather(FRC,vefftmp,vloc)
    !write(*,*) "I'm here after distmat in gatherLocalPotential"
    !write(*,*) "size of FRCvloc, vloc."
    !allocate(FRC%potential%vloc(size(vloc),1))
    !write(*,*)  size(FRC%potential%vloc,1), size(vloc)
    FRC%potential%vloc(:,1) = vloc
    
    return
    end subroutine gatherLocalPotential
    end module gatherLocalPotential_module
