! ftLaplMat.f90
!**************************************************************************
    !
    ! This function is used to generate Laplacian operator with FFT.
    !
!**************************************************************************
    
    module ftLaplMat_module
    contains
    subroutine ftLaplMat(n,Lu,Lv,Lw)
    
    use fftFreq_module
    use fft2_module
    
    
    implicit none
    
    ! input valuables
    integer :: n(3)
    
    ! temporery valuables
    integer                 :: ii
    real*8                  :: p, tmp(3,3)
    complex*16              :: imnu, pi
    integer                 :: ntmp(3)
    real*8, allocatable     :: aa(:,:), bb(:,:), cc(:,:)
    
    real*8, allocatable     :: ku1(:,:,:), kv1(:,:,:), kw1(:,:,:), &
                               ku2(:,:,:), kv2(:,:,:), kw2(:,:,:), &
                               ku3(:,:,:), kv3(:,:,:), kw3(:,:,:)
    
    complex*16, allocatable :: Lutmp(:,:), Lvtmp(:,:), Lwtmp(:,:)
    
    ! output valuables
    real*8, allocatable     :: Lu(:,:), Lv(:,:), Lw(:,:)
    
    ! body of this function
    imnu = (0.0D0,1.0D0)
    p    =  3.1415926535897932385D0
    pi   = (3.1415926535897932385D0,0.0D0)
    ntmp(2) = 1
    ntmp(3) = 1
    tmp(:,:) = 0.0D0
    forall(ii=1:3)
        tmp(ii,ii) = 1.0D0
    end forall
    
!############################################################
    ntmp(1) = n(1)
    allocate(ku1(n(1),1,1))
    allocate(kv1(n(1),1,1))
    allocate(kw1(n(1),1,1))
    call fftFreq(ntmp,tmp,ku1,kv1,kw1)
    ku1 = 2.0D0*p*ku1
    !allocate(Lu(n(1),n(1)))
    Lu  = 0.0D0
    forall(ii=1:n(1))
        Lu(ii,ii) = 1.0D0
    end forall
    allocate(Lutmp(n(1),n(1)))
    call fft2(Lu,2,Lutmp)
    allocate(aa(n(1),n(1)))
    aa(:,:) = 0.0D0
    forall(ii=1:n(1))
        aa(ii,ii) = -ku1(ii,1,1)*ku1(ii,1,1)/dble(n(1))
    end forall
    Lu = dble(matmul(conjg(transpose(Lutmp)),matmul(dcmplx(aa),Lutmp)))
    Lu = (Lu+transpose(Lu))/2.0D0
    
!############################################################
    ntmp(1) = n(2)
    allocate(ku2(n(2),1,1))
    allocate(kv2(n(2),1,1))
    allocate(kw2(n(2),1,1))
    call fftFreq(ntmp,tmp,ku2,kv2,kw2)
    ku2 = 2.0D0*p*ku2
    !allocate(Lv(n(2),n(2)))
    Lv  = 0.0D0
    forall(ii=1:n(2))
        Lv(ii,ii) = 1.0D0
    end forall
    allocate(Lvtmp(n(2),n(2)))
    call fft2(Lv,2,Lvtmp)
    allocate(bb(n(2),n(2)))
    bb(:,:) = 0.0D0
    forall(ii=1:n(2))
        bb(ii,ii) = -ku2(ii,1,1)*ku2(ii,1,1)/dble(n(2))
    end forall
    Lv = dble(matmul(conjg(transpose(Lvtmp)),matmul(dcmplx(bb),Lvtmp)))
    Lv = (Lv+transpose(Lv))/2.0D0
    
!##############################################################
    ntmp(1) = n(3)
    allocate(ku3(n(3),1,1))
    allocate(kv3(n(3),1,1))
    allocate(kw3(n(3),1,1))
    call fftFreq(ntmp,tmp,ku3,kv3,kw3)
    ku3 = ku3*2.0D0*p
    !allocate(Lw(n(3),n(3)))
    Lw  = 0.0D0
    forall(ii=1:n(3))
        Lw(ii,ii) = 1.0D0
    end forall
    allocate(lwtmp(n(3),n(3)))
    call fft2(Lw,2,Lwtmp)
    allocate(cc(n(3),n(3)))
    cc(:,:) = 0.0D0
    forall(ii=1:n(3))
        cc(ii,ii) = -ku3(ii,1,1)*ku3(ii,1,1)/dble(n(3))
    end forall
    Lw = dble(matmul(conjg(transpose(Lwtmp)),matmul(dcmplx(cc),Lwtmp)))
    Lw = (Lw+transpose(Lw))/2.0D0
    deallocate(aa,bb,cc)
    deallocate(ku1,kv1,kw1)
    deallocate(ku2,kv2,kw2)
    deallocate(ku3,kv3,kw3)
    deallocate(Lutmp,Lvtmp,Lwtmp)

    return
    end subroutine ftLaplMat
    end module ftLaplMat_module
