! BSXFPLUS.f90

      module BSXFPLUS_module

              use bsxfunplus_2d_2d_real8_module
              use bsxfunplus_2d_2d_cmplx_module
              use bsxfunplus_2d_3d_real8_module
              use bsxfunplus_2d_3d_cmplx_module
              use bsxfunplus_3d_2d_real8_module
              use bsxfunplus_3d_2d_cmplx_module
              use bsxfunplus_3d_3d_real8_module
              use bsxfunplus_3d_3d_cmplx_module
              use bsxfunplus_4d_4d_real8_module
              use bsxfunplus_4d_4d_cmplx_module

              implicit none

              interface BSXFPLUS
                      module procedure bsxfunplus_2d_2d_real8
                      module procedure bsxfunplus_2d_2d_cmplx 
                      module procedure bsxfunplus_2d_3d_real8
                      module procedure bsxfunplus_2d_3d_cmplx
                      module procedure bsxfunplus_3d_2d_real8
                      module procedure bsxfunplus_3d_2d_cmplx
                      module procedure bsxfunplus_3d_3d_real8
                      module procedure bsxfunplus_3d_3d_cmplx
                      module procedure bsxfunplus_4d_4d_real8
                      module procedure bsxfunplus_4d_4d_cmplx
              end interface BSXFPLUS

      end module BSXFPLUS_module
