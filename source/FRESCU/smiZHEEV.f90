! smiZHEEV.f90

!*************************************************
      !
      !
      !
!***********************************************************

      module smiZHEEVMOD
      contains
      subroutine smiZHEEV(jobz,UPLO,N,A,MB,NB,nprow,npcol,W,Z)

      implicit none

      integer :: N, MB, NB, nprow, npcol
      character(len=1) :: jobz, UPLO
      complex*16, allocatable :: A(), W(), Z()

      integer :: INFO, LWORK, LRWORK, nprocs
      complex*16, allocatable :: WROK(), RWORK()

      integer :: iam, ictxt, myrow, mycol

      LWORK  = -1
      LRQORK = -1
      nprocs =  nprow*npcol
      iam    =  0
      ictxt  =  0
      myrow  =  0
      mycol  =  0
      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,Z_np)
      A_np = 
      Z_np = 

      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descZ,N,N,MB,NB,izero,izero,ictxt,Z_np,descinfo)

      allocate(WORK(1))
      allocate(RWORK(1))
      call PZHEEV(jobz,UPLO,N,A,ione,ione,descA,W,Z,ione,ione,descZ,&
              WORK,LWORK,RWORK,LRWORK,INFO)
      LWORK = WORK(1)
      LRWORK = RWORK(1)
      deallocate(WORK,RWORK)
      allocate(WORK(LWORK),RWORK(LRWORK))
      call PZHEEV(jobz,UPLO,N,A,ione,ione,descA,W,Z,ione,ione,descZ,&
              WORK,LWORK,RWORK,LRWORK,INFO)

      if (INFO .NE. 0) then
              write(*,*) 'Error'
      end if
      deallocate(WORK,RWORK)
      call blcs_gridexit(ictxt)

      return
      end subroutine smiZHEEV
      end module smiZHEEVMOD
