! MPI_Recv_variable_real8_2D.f90

!*****************************************************************
      !
      !
      !
!*****************************************************************

      module MPI_Recv_variable_real8_2D_module
      contains
      subroutine MPI_Recv_variable_real8_2D(sor,tag,vari)

      include 'mpif.h'

      !implicit none

      ! input variables
      integer :: sor
      integer :: tag

      ! temporary variables
      integer :: varinfo(16), issp, isrl, bloc, nonzero, vardim
      integer :: varsiz1, varsiz2, ierr
      integer :: varn, kk, ntmp, ii

      integer,allocatable :: inde(:)
      real*8, allocatable :: buf(:), buftmp(:)
      ! output variables
      real*8, allocatable :: vari(:,:)

      ! body of this function
      varinfo = 0

      !write(*,*) 'before recv info.', sor
      call MPI_RECV(varinfo,16,MPI_INT,sor,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
      issp    = varinfo(1)
      isrl    = varinfo(2)
      bloc    = varinfo(3)
      nonzero = varinfo(4)
      vardim  = varinfo(5)
      varsiz1 = varinfo(6)
      varsiz2 = varinfo(7)
      tag     = tag+1
     ! write(*,*)  varinfo
     ! write(*,*) 'after recv info'
      !allocate(vari(varsiz1,varsiz2))
      varn    = varsiz1*varsiz2

      !kk      = 0
      !do while (kk < varn)
          !ntmp = min(kk+bloc,varn)
          !forall(ii=1:ntmp-kk)
          !        inde(ii) = kk+ii
          !end forall
          !if (varn == 1) then
          !        allocate(buftmp(2))
          !        buftmp = 0.0D0
          !write(*,*) 'in MPI_Recv_variable_real8_2D, before send'
          !        call MPI_RECV(buftmp,2,MPI_DOUBLE,sor,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr) 
          !        tag = tag+1
                  !allocate(vari(1))
          !        vari(1,1) = buftmp(1)
          !else
                  !allocate(vari())
                  call MPI_RECV(vari,varn,MPI_DOUBLE,sor,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
          !        tag = tag+1
          !end if
          !write(*,*) 'in MPI_Recv_variable_real8_2D, after send'

          !forall(ii=1:ntmp)
          !        vari(inde(ii)) = buf(ii)
          !end forall
      !end do

      !kk = kk + bloc

      return
      end subroutine MPI_Recv_variable_real8_2D
      end module MPI_Recv_variable_real8_2D_module

