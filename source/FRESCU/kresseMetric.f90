! kresseMetric.f90

!***************************************
      !
      !
      !
!***********************************************

      module kresseMetricMod
      contains 
      subroutine kresseMetric(FRC,rho,bc)

      implicit none

      avec = FRC%domain%latvec
      j2   = matmul(avec,transpose(avec))
      call inversion(j2)
      call triu(j2,j2tmp)
      call tril(j2,-1,j2)
      j2 = j2tmp+transpose(j2)
      szrho(1) = size(rho,1)
      szrho(2) = size(rho,2)
      szrho(3) = size(rho,3)
      call isreal(rho,rlrho)

      do ii =1,3,1
          call genFreq(szrho(ii),bc(ii),k(ii)%vata)
          call circshift((/1,2,3/),(/0,ii-1/),ktmp)
          call permute(k(ii)%vata,ktmp,k(ii)%vata)
      end do

      do ii = 1,3,1
      if (bc(ii) .EQ. 0) then
              call dst(rho,ii)
      else if (bc(ii) .EQ. 1) then
              call fft(rho,ii)
      else if (bc(ii) .EQ. 2) then
              call dct(rho,ii)
      end if

      allocate(k2(szrho))
      k2 = 0.0D0
      do ii = 1,3,1
      do jj = ii,3,1
      if (j2(ii,jj)) then
              call bsxfunTimes(k(jj)%vata,k(ii)%vata,k2tmp)
              call bsxfunMinus(k2,k2tmp,k2)
      end if
      end do
      end do

      kmin = minval(k2)
      kmax = maxval(k2)
      if (kmin .EQ. k2(1,1,1)) then
              k2(1,1,1) = k2(1,1,1)+kmax
              kmin      = minval(k2)
              k2(1,1,1) = k2(1,1,1)-kmax
      end if

      q2 = 19.0D0*(kmin*kmax)/(kmax-20.0D0*kmin)
      q2 = max(q2,19.0D0*kmin)
      q2 = min(q2,19.0D0*21.0D0*kmin)

      k2 = (k2-q2)/k2
      k2(1,1,1) = mean
