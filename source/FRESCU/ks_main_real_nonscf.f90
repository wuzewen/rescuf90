! ks_main_real_nonscf.f90

      module ks_main_real_nonscf_module
      contains
      subroutine ks_main_real_nonscf(FRC)

      implicit none

      ! input variables

      ! temporary variable

      ! output variables

      ! body of this fucntion
      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi$mpoisize

      cn      = product(FRC%domain%cgridn)
      vol     = FRC%domain%vol
      dr      = vol/dble(cn)
      !kdir    = FRC%kpoint%ikdirect
      nkpt    = size(FRC%kpoint%ikdirect,1)
      allocate(kdir(nkpt,3))
      kdir    = FRC%kpoint%ikdirect
      call calcKpointDiff(FRC,kdir,dk)
      nband   = FRC%eigensolver%nband
      ispin   = FRC%spin%ispin
      nspin   = FRC%spin%nspin
      maxit   = FRC%eigensolver%maxit
      tol     = FRC%eigensolver%tol
      allocate(Ek(nband,nkpt,nspin))
      maxRestart = FRC%eigensolver%maxRestart
      eigalg     = FRC%eigensolver%algo

      if (FRC%dos%pdos) then
              Scell = FRC%LCAO%Scell
              sv    = FRC%LCAO%Svec
      end if

      if (trim(eigalg) == trim('lobpcg') .OR. FRC%dos%pdos) then
              call GetAtomicOrbitalSubspace(FRC,~,.FALSE.)
              if (FRC%LCAO%dynSub%aocell) then
                      aocell   = FRC%LCAO%aocell
                      aov      = FRC%LCAO%aovec
                      aosparse = FRC%LCAO%aosparse
              else
                      aocell   = FRC%LCAO%aocell
              end if
      end if
      call UpdateVeff(FRC,FRC%potential%veffin(1),FRC%potential%vps,veff)
      call distmat_allgather(FRC,veff,FRC%potential%vloc)

      if (trim(eigalg) == trim('lobpcg')) then
              call ks_main_lcao_scf(FRC)
      end if

      if (trim(eigalg) == trim('lobpcg') .AND. (.NOT. FRC%mpi%parak)) then
              do kk = 1,nkpt,1
                  kpt = kdir(kk,:)
                  call GetKBorb(FRC,kpt)
                  if (FRC%eigensolver%precond) then
                          degree = FRC%eigensolver%precon
                          call calcKineticPrecond(FRC,degree,kpt,prekin)
                  end if
                  do ss = 1,nspin,1
                      if (ispin == 1 .OR. ispin == 2) then
                              call GenHamFun(FRC,kpt,ss,ham)
                      else
                              write(*,*) 'Error in ks_main_real_nonscf.'
                      end if
                      dkcond = (dk(kk) > 0.1 .OR. any(kpt ) .OR.)
                      if (dkcond .OR. FRC%dos%pdos) then
                              if (FRC%LCAO%dynSubRed) then
                                   call reduceOper(aocell,aov,kpt,aosparse,aosub)
                              else
                                   aosub = aocell(kk)
                              end if
                      end if
                      if (dkcond .AND. (ispin == 1 .OR. ispin == 2)) then
                              call calcev_lcao(FRC,aosub,FRC%psi(kk,ss))
                      else if (kdcond) then
                              ntmp = ceiling(2.0D0*dble(cn)/dble(mpisize))
                              call InitDistArray(2*cn,nband,ntmp,1,1,mpisize,blockX)
                              call calcev_lcao(FRC,aosub,FRC%psi(kk,1),Xtmp)
                              call ModBCDist(FRC,Xtmp,1,1,1,mpisize,Ytmp)
                              blockX%vata = Ytmp%vata
                              call calcev_lcao(FRC,aosub,FRC%psi(kk,2),Xtmp)
                              call ModBCDist(FRC,Xtmp,1,1,1,mpisize,Ytmp)
                              ntmp = ceiling(dble(blockX%m)/dble(mpisize))
                              call ModBCDist(FRC,blockX,ntmp,1,mpisize,1)
                      end if
                      restartIt = 1
                      do while (restartIt <= maxRestart)
                          call
lobpcg_para(FRC,blockX%vata,ham,~,prefun,~,tol,maxit,blockX%vata,Ek,failureFlag,~,~)
                          if (failureFlag) then
                                  exit
                          else
                                  restartIt = restartIt + 1
                          end if
                      end do
                      
                      call validateDOSrange(FRC,Ek)
                      if (FRC%dos%pdos .AND. (ispin == 1 .OR. ispin == 2)) then
                              call reduceOper(Scell,sv,kpt,XSX)
                              call GetGlobalInd(mpirank,blockX,iloc,jloc)
                              call getrphase(FRC,kpt,eikr)
                              call bsxfunTimes(blockX%vata,)
                              call
wavefunc2coeff(FRC,XSX,aosub,blockX,coeff(kk,ss))
                      end if
                  end do
                  if (mpirank == 0) then
                          if (failureFlag) then
                                  write(*,*) 'Failed to converge'
                          end if
                          call datevec(datenum(clock))
                  end if
              end do
      else





                     

