! diag_proj.f90

!*************************************************************
      !
      ! In this function, eigen values and eigen vectors will be
      ! obtained. The eigen problem is HH*ZZ = theta*SS*ZZ.
      ! What will be obtained is theta(eigen values) and ZZ(eigen
      ! vectors).
      !
      ! Here, if MPI is used, all the projected hamiltonian elements
      ! of HH and SS will be gethered on the main process(rank = 0). The
      ! calculation will only be executed on the main process.
      ! After calculation, theta will be broadcast to every process,
      ! and, ZZ will be redistributed on every process.
      !
      ! LAPACK lib is used to solve this problem, the subroutine will be
      ! used is ZHEGV, since the Hamiltonians are Hermitian matrices.
      !
!*************************************************************

      module diag_proj_module
      contains
      subroutine diag_proj(FRC,HH,SS,iscomplex,ZZ,theta)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use MPI_Bcast_variable_1D_module
      use distmat_gather_CMPLX_module

      external ZHEGV

      !implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      type(VataMN_CMPLX_2D)           :: HH, SS
      logical                         :: iscomplex

      ! temporary variables
      logical :: mpistat, smistat
      integer :: mpirank, mpisize, N, LWORK, INFO, SH1, SH2, ii
      real*8 , allocatable    :: W(:), RWORK(:)
      complex*16, allocatable :: h(:,:), htmp(:,:), s(:,:), stmp(:,:)
      complex*16, allocatable :: WORK(:), SStmp(:,:), HHtmp(:,:)
      real*8    , allocatable :: thetatmp(:)
      integer   , allocatable :: ranks(:)

      ! output variables
      real*8, allocatable   :: theta(:)
      type(VataMN_CMPLX_2D) :: ZZ

      ! body of this function
      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize
      smistat = FRC%smi%status

      if (smistat) then
              write(*,*) "Error in diag_proj. SMI not available."
              stop
      else if (mpistat) then
              !write(*,*) "Error in diag_proj. MPI not available."
              npsi = SS%m
              mp   = FRC%smi%mp
              !write(*,*) 'mp =', mp
              np   = mpisize/mp
              allocate(SStmp(size(SS%vata,1),size(SS%vata,2)))
              allocate(HHtmp(size(HH%vata,1),size(HH%vata,2)))

              !write(*,*) 'in diag_proj. before gather.'
              call distmat_gather_CMPLX(FRC,SS,0,SStmp)
              !write(*,*) 'in diag_proj. after gather 1.'
              call distmat_gather_CMPLX(FRC,HH,0,HHtmp)
              !write(*,*) 'in diag_proj. after gather 2.'
              if (mpirank == 0) then
                      SStmp = 0.5D0*(SStmp+conjg(transpose(SStmp)))
                      HHtmp = 0.5D0*(HHtmp+conjg(transpose(HHtmp)))
                      N     = size(SStmp,1)
                      allocate(W(N))
                      LWORK = 2*N-1
                      allocate(WORK(LWORK))
                      allocate(RWORK(3*N-2))
                      call ZHEGV(1,'V','U',N,HHtmp,N,SStmp,N,W,WORK,LWORK,RWORK,INFO)
                      allocate(theta(N))
                      theta = W
              else
                      allocate(theta(size(HH%vata,1)))
                      theta = 0.0D0
                      HHtmp = (0.0D0,0.0D0)
                      !allocate(HHtm)
                      !allocate(ZZ(0))
              end if
              allocate(thetatmp(size(theta)))
              thetatmp = theta
              !write(*,*) 'theta =', theta
              allocate(ranks(mpisize))
              forall(ii=1:mpisize)
                      ranks(ii) = ii-1
              end forall
              call MPI_Bcast_variable_1D(thetatmp,0,ranks,theta)
              call InitDistArray_CMPLX_2D(npsi,npsi,npsi,npsi,mp,np,ZZ)
              allocate(ZZ%vata(size(HHtmp,1),size(HHtmp,2)))
              ZZ%vata = HHtmp
      else
              SH1  = size(HH%vata,1)
              SH2  = size(HH%vata,2)
              allocate(ZZ%vata(SH1,SH2))
              allocate(h(SH1,SH2))
              allocate(htmp(SH2,SH1))
              ZZ   = HH
              h    = HH%vata
              htmp = transpose(h)
              htmp = conjg(htmp)
              h    = 0.5D0*(h+htmp)
              SH1  = size(SS%vata,1)
              SH2  = size(SS%vata,2)
              allocate(s(SH1,SH2))
              allocate(stmp(SH2,SH1))
              s    = SS%vata
              stmp = transpose(s)
              stmp = conjg(stmp)
              s    = 0.5D0*(s+stmp)

              N    = size(s,1)
              allocate(W(N))
              LWORK = 2*N-1
              allocate(WORK(LWORK))
              allocate(RWORK(3*N-2))
              call ZHEGV(1,'V','U',N,h,N,s,N,W,WORK,LWORK,RWORK,INFO)
              allocate(theta(N))
              theta   = W
              ZZ%vata = h
      end if

      return
      end subroutine diag_proj
      end module diag_proj_module

