! SparseArray_2D_Cmplx.f90

!******************************************************************************
      !
      !
      !
!******************************************************************************

      module SparseArray_2D_Cmplx_module

              type :: SparseArray_2D_Cmplx
                      integer :: SIZE1, SIZE2
                      integer :: N_nonzero
                      integer   , allocatable :: Index1(:)
                      integer   , allocatable :: Index2(:)
                      complex*16, allocatable :: ValueN(:)
              end type SparseArray_2D_Cmplx

      end module SparseArray_2D_Cmplx_module
