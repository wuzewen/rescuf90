! projectVeff.f90

!******************************************
      !
      !
      !
!***************************************************

      module projectVeffMod
      contains
      subroutine projectVeff(FRC,X1,X2,veff,XVX)

      implicit none

      mpistat  = FRC%mpi%status
      mpirank  = FRC%mpi%rank
      mpisize  = FRC%mpi%mpisize
      mb       = FRC%smi%mb
      nb       = FRC%smi%nb
      mp       = FRC%smi%mp
      np       = FRC%smi%np
      buff     = FRC%option%buffersize
      sprs     = FRC%LCAO%sprs
      vol      = 
      dr       = vol/dble(product(FRC%domain%cgridn))
      nnzSB    = FRC%LCAO%nnzSmask
      nnzSBloc = FRC%LCAO%nnzSmaskLoc
      no       = X1%n
      nspin    = veff%n
      gdloc    = size(X2%vata,1)
      sbspfr   = nnzSB/(no**2)
      aospfr   = nnz(X2%vata)/size(X2%vata)

      if (nspin .EQ. 1) then
          call distmat_isreal(FRC,X1,isrl)
          !iscomplex = .NOT. isrl
          if (isrl) then
              sizeof = 8
          else
              sizeof = 16
          end if
          sbDisp = FRC%LCAO%sbDisp
          sbVec  = FRC%LCAO%sbVec
          call reduceOper(sbDisp,shVec,(/0,0,0/),nnzSBloc,Smask)
          blks   = ceiling(dble(buff)/dble(no)/dble(sizeof)/dble(sbspfr)/4.0D0)
          blks   = min(no,blks)
          call ModBCDist(FRC,Smask,1,blks,1,mpisize)
          call adjustSparsity(Smask%vata,sprs)
          XVX    = Smask
          do ii = 0,mpisize-1,1
              call GetGlobalInd(ii,Smask,iloc,jloc)
              if (size(iloc)*size(jloc) .GE. 0) then
                  if (mpistat) then
                      call MPI_Bcast_variable(Smask%vata,ii,xvxtmp)
                  else
                      xvxtmp = Smask%vata
                  end if
                  xvxval = 0.0D0
                  logitm = 
                  blks   = ceiling(dble(buff)/dble(gdloc)/dble(aospfr)/4.0D0)
                  kk     = 1
                  do while (kk .LE. size(iloc)) then
                      if (kk+blks .GT. size(jloc)) then
                          blks = size(jloc) - kk
                      end if
                      forall(jj=1:blks)
                              jlockk(jj) = jloc(kk+jj-1)
                      end forall
                      if (issparseX2) then
                          call spdiags()
                      else
                          call bsxfunTimes(veff%vata*dr,X2%vata,veffX2)
                      end if
                      tmp = matmul(transpose(conjg(veffX2)),X1%vata)
                      forall(jj=1:blks)
                          xvxval(kk+ii-1,:) = xvxval(kk+ii-1,:)+tmp(jj,:)
                      end forall
                      kk = kk + blks + 1
                  end do
                  xvxval = transpose(conjg(xvxval))
                  if (issparse) then
                      call full()
                  end if
                  if (mpistat) then
                      call MPI_Reduce_sum(xvxval,ii)
                  end if
                  if (mpirank .EQ. ii .AND. issparse) then
                      XVX%vata(xvxtmp) = xvxval
                  else if (mpirank .EQ. ii)
                      XVX%vata = xvxval
                  end if
              end if
          end do
          call ModBCDist(FRC,XVX,mb,nb,mp,np)
      else if (nspin .EQ. 4) then
          write(*,*) 'Error'
          stop
      end if

      return
      end subroutine projectVeff
      end mosule projectVeffMod
