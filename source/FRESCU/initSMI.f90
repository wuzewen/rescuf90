!initSMI.f90
!*****************************************
    !
!*****************************************
    
    module initSMI_module
    contains
    subroutine initSMI(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    integer                              :: nline
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType)              :: inputFromFile
    
    ! temporary variables
    logical :: SMIstatusIsFind, SMImbIsFind, SMInbIsFind, SMImpIsFind, SMInpIsFind
    logical :: SMImbpsiIsFind, SMInbpsiIsFind, SMIabstolIsFind, SMIorfacIsFind, SMIlworkIsFind
    integer :: ii, status, jj, dmp, dnp, cn
    integer :: mpisize
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%SMI) then
        mpisize         =  FRC%mpi%mpisize
        SMIstatusIsFind = .FALSE.
        SMImbIsFind     = .FALSE.
        SMInbIsFind     = .FALSE.
        SMImpIsFind     = .FALSE.
        SMInpIsFind     = .FALSE.
        SMImbpsiIsFind  = .FALSE.
        SMInbpsiIsFind  = .FALSE.
        SMIabstolIsFind = .FALSE.
        SMIorfacIsFind  = .FALSE.
        SMIlworkIsFind  = .FALSE.
        
        if (FRC%mpi%status) then
                call isqrt(mpisize,dmp,dnp)
            !write(*,*) "Error in initSMI.f90. MPI is not available now."
            !stop
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("SMIstatus")) then
            !        SMIstatusIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (SMIstatusIsFind) then
            !    read(inputFromFile%NAV(ii)%value,"(i2)") status
            !    if (status /= 0) then
            !        FRC%SMI%status = .TRUE.
            !    else
            !        FRC%SMI%status = .FALSE.
            !    end if
            !else
            !    FRC%SMI%status = .FALSE.
            !end if
            
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("SMImb")) then
            !        SMImbIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (SMImbIsFind) then
            !    read(inputFromFile%NAV(ii)%value,"(i2)") FRC%SMI%mb
            !else
            !    FRC%SMI%mb = 64
            !end if
        
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("SMInb")) then
            !        SMInbIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (SMInbIsFind) then
            !    read(inputFromFile%NAV(ii)%value,"(i2)") FRC%SMI%nb
            !else
            !    FRC%SMI%nb = 64
            !end if
        
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("SMImp")) then
            !        SMImpIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            
            !jj = 0
            !do while(jj < nline)
            !    jj     = jj+1
            !    if (trim(inputFromFile%NAV(jj)%name) == trim("SMInp")) then
            !        SMInpIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            
            !call isqrt(FRC%mpi%mpisize,"flat",dmp,dnp)
           ! 
            !if (SMImpIsFind) then
            !    read(inputFromFile%NAV(ii)%value,"(i2)") FRC%SMI%mp
            !    FRC%SMI%np = FRC%mpi%mpisize/FRC%SMI%mp
            !else if (SMInpIsFind) then
            !    read(inputFromFile%NAV(jj)%value,"(i2)") FRC%SMI%np
            !    FRC%SMI%mp = FRC%mpi%mpisize/FRC%SMI%np
            !else
                FRC%SMI%mp = dmp
                FRC%SMI%np = dnp
            !end if
        
            !cn = product(FRC%domain%cgridn)
            
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("SMImbpsi")) then
            !        SMImbpsiIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (SMImbpsiIsFind) then
            !    read(inputFromFile%NAV(ii)%value,"(i2)") FRC%SMI%mbpsi
            !else
            !    FRC%SMI%mbpsi = (cn/FRC%mpi%mpisize)
            !end if
            
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("SMInbpsi")) then
            !        SMInbpsiIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (SMInbpsiIsFind) then
            !    read(inputFromFile%NAV(ii)%value,"(i2)") FRC%SMI%nbpsi
            !else
            !    FRC%SMI%nbpsi = 1
            !end if
            
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("SMIabstol")) then
            !        SMIabstolIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (SMIabstolIsFind) then
            !    read(inputFromFile%NAV(ii)%value,"(i2)") FRC%SMI%abstol
            !else
            !    FRC%SMI%abstol = 0
            !end if
        
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("SMIorfac")) then
            !        SMIorfacIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (SMIorfacIsFind) then
            !    read(inputFromFile%NAV(ii)%value,"(i2)") FRC%SMI%orfac
            !else
            !    FRC%SMI%orfac = 0
            !end if
            
            !ii = 0
            !do while(ii < nline)
            !    ii     = ii+1
            !    if (trim(inputFromFile%NAV(ii)%name) == trim("SMIlwork")) then
            !        SMIlworkIsFind = .TRUE.
            !        exit
            !    end if
            !end do
            !if (SMIlworkIsFind) then
            !    read(inputFromFile%NAV(ii)%value,"(i2)") FRC%SMI%lwork
            !else
            !    FRC%SMI%lwork = 2.0**16
            !end if
        else
            FRC%SMI%mb     = 1
            FRC%SMI%nb     = 1
            FRC%SMI%mp     = 1
            FRC%SMI%np     = 1
            FRC%SMI%mbpsi  = 1
            FRC%SMI%nbpsi  = 1
            FRC%SMI%abstol = 0
            FRC%SMI%orfac  = 0
            FRC%SMI%lwork  = 0
        end if
        
        FRC%init%smi = .TRUE.
    end if
    
    return
    end subroutine initSMI
    end module initSMI_module
