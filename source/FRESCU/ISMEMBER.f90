! ISMEMBER.f90

!***************************************************************************************
      !
      !
      !
!***************************************************************************************

      module ISMEMBERMOD

      interface ISMEMBER
              module procedure ismember_1Dint
              module procedure ismember_2Dint
              module procedure ismember_1Dreal
              module procedure ismember_2Dreal
              module procedure ismemberRow
      end interface ISMEMBER

      end module ISMEMBERMOD
