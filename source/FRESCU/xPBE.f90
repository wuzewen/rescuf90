! xPBE.f90

! ***********************************************************************
      ! This subroutine will provide exchange potential
      ! with GGA_PBE method.
      ! All the inputs and outputs are describe as follows:
      ! rho     : input , two dimentional array (ngrid * ?), double precision
      ! sigma   : input , two dimentional array (ngrid * 6), double precision
      ! Ex1     : output, one dimentiinal array (ngrid), double precision
      ! DExDP1  : output, 
      ! DExDN1  : output,
      ! DExDGP1 : output,
      ! DExDGN1 : output,
! **********************************************************************

      module xPBE_module
      contains
      subroutine xPBE(rho,sigma,EX1,DExDP1,DExDN1,DExDGP1,DExDGN1)

      use XC_LDA_X_module
      
      implicit none

      ! input variables
      real*8, allocatable :: rho(:,:), sigma(:,:)


      ! temporary variables
      integer :: npts, nara, ntmp, ii
      real*8  :: DENMIN, EightThird, ConstS2, Kappa, Mu
      real*8, allocatable :: DENS(:), rhotmp(:,:), sigmatmp(:,:)
      real*8, allocatable :: RhoSP(:), GRhoSPX(:), GRhoSPY(:), GRhoSPZ(:)
      real*8, allocatable :: RhoSN(:), GRhoSNX(:), GRhoSNY(:), GRhoSNZ(:)
      real*8, allocatable :: PNtmp(:,:)
      real*8, allocatable :: DExDP(:), DExDGPX1(:,:), DExDGPX(:)
      real*8, allocatable :: DExDN(:), DExDGNX1(:,:), DExDGNX(:)
      real*8, allocatable :: GD2(:), FS2(:), S2P(:), TS(:), FP(:)
      real*8, allocatable :: DFDS2(:), DFDGD(:), EX(:)
      real*8, allocatable :: DExDGPY(:), DExDGPZ(:)
      real*8, allocatable :: S2N(:), FN(:)
      real*8, allocatable :: DExDGNY(:), DExDGNZ(:)
      real*8, allocatable :: DExDGP(:,:), DExDGN(:,:)

      ! output variables
      real*8, allocatable :: EX1(:), DExDP1(:), DExDN1(:)
      real*8, allocatable :: DExDGP1(:,:), DExDGN1(:,:)


      ! body of this function
      DENMIN = 50.0D0*2.220446049250313D-16
      npts   = size(rho,1)
      nara   = size(rho,2)
      allocate(EX1(npts))
      allocate(DExDP1(npts))
      allocate(DExDN1(npts))
      allocate(DExDGP1(npts,3))
      allocate(DExDGN1(npts,3))
      EX1     = 0.0D0
      DExDP1  = 0.0D0
      DExDN1  = 0.0D0
      DExDGP1 = 0.0D0
      DExDGN1 = 0.0D0

      allocate(DENS(npts))
      DENS    = sum(rho,2)
      ntmp    = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                  ntmp = ntmp+1
          end if
      end do
      allocate(rhotmp(ntmp,nara))
      allocate(sigmatmp(ntmp,size(sigma,2)))
      ntmp = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                  ntmp             = ntmp+1
                  rhotmp(ntmp,:)   = rho(ii,:)
                  sigmatmp(ntmp,:) = sigma(ii,:)
          end if
      end do

      !write(*,*) "in xPBE, rhotmp = "
      !do ii = 1,size(rhotmp,1),1
      !write(*,*) rhotmp(ii,:)
      !end do
      !write(*,*) "in xPBE, sigmatmp = "
      !do ii = 1,size(sigmatmp,1),1
      !write(*,*) sigmatmp(ii,:)
      !end do



      EightThird = 2.666666666666666667D0
      ConstS2    = 0.0164553078460205575D0
      Kappa      = 0.804D0
      Mu         = 0.2195164512208958D0

      allocate(RhoSP(ntmp))
      allocate(GRhoSPX(ntmp))
      allocate(GRhoSPY(ntmp))
      allocate(GRhoSPZ(ntmp))
      allocate(RhoSN(ntmp))
      allocate(GRhoSNX(ntmp))
      allocate(GRhoSNY(ntmp))
      allocate(GRhoSNZ(ntmp))
      RhoSP   = rhotmp(:,1)
      GRhoSPX = sigmatmp(:,1)
      GRhoSPY = sigmatmp(:,2)
      GRhoSPZ = sigmatmp(:,3)
      RhoSN   = rhotmp(:,2)
      GRhoSNX = sigmatmp(:,4)
      GRhoSNY = sigmatmp(:,5)
      GRhoSNZ = sigmatmp(:,6)

      allocate(PNtmp(ntmp,2))
      PNtmp(:,1)  =  RhoSP
      PNtmp(:,2)  =  RhoSP
      call XC_LDA_X(.FALSE.,2,PNtmp,DExDP,DExDGPX1)

      !write(*,*) "in xPBE, RhoSP = "
      !write(*,*)  RhoSP
      !write(*,*) "in xPBE, DExDP = "
      !write(*,*)  DExDP
      !write(*,*) "in xPBE, DExDGDPX1 ="
      !do ii = 1,size(DExDGPX1,1),1
      !write(*,*) DExDGPX1(ii,:)
      !end do

      allocate(DExDGPX(ntmp))
      DExDGPX     =  DExDGPX1(:,1)
      
      !write(*,*) "after first call XC_LDA_X."     
      PNtmp(:,1)  =  RhoSN
      pNtmp(:,2)  =  RhoSN
      call XC_LDA_X(.FALSE.,2,PNtmp,DExDN,DExDGNX1)
      allocate(DExDGNX(ntmp))
      DExDGNX     =  DExDGNX1(:,1)

      !write(*,*) "after second call XC_LDA_X."

      allocate(GD2(ntmp))
      allocate(FS2(ntmp))
      allocate(S2P(ntmp))
      allocate(TS(ntmp))
      allocate(FP(ntmp))
      allocate(DFDS2(ntmp))
      allocate(DFDGD(ntmp))
      allocate(EX(ntmp))
      !allocate(DExDP(ntmp))
      !allocate(DExDGPX(ntmp))
      allocate(DExDGPY(ntmp))
      allocate(DExDGPZ(ntmp))
      !allocate(DExDGNY(ntmp))
      !allocate(DExDGNZ(ntmp))
      GD2         =  GRhoSPX*GRhoSPX + GRhoSPY*GRhoSPY + GRhoSPZ*GRhoSPZ
      FS2         =  ConstS2*(RhoSP**(-EightThird))
      S2P         =  FS2*GD2
      TS          =  Kappa/(Kappa+Mu*S2P)
      !write(*,*) "after TS"
      FP          =  1.0D0+Kappa-TS*Kappa
      DFDS2       =  TS*TS*Mu
      DFDGD       =  2.0D0*RhoSP*DExDP*DFDS2*FS2
      Ex          =  RhoSP*DExDP*FP
      !write(*,*) "after EX"
      DExDP       =  DExDGPX*FP-EightThird*DExDP*DFDS2*S2P
      DExDGPX     =  DFDGD*GRhoSPX
      DExDGPY     =  DFDGD*GRhoSPY
      DExDGPZ     =  DFDGD*GRhoSPZ
      
      !write(*,*) "after DExDGPZ"

      allocate(FN(ntmp))
      allocate(S2N(ntmp))
      !allocate(DExDGNX(ntmp))
      allocate(DExDGNY(ntmp))
      allocate(DExDGNZ(ntmp))

      GD2         =  GRhoSNX*GRhoSNX + GRhoSNY*GRhoSNY + GRhoSNZ*GRhoSNZ
      FS2         =  ConstS2*(RhoSN**(-EightThird))
      S2N         =  FS2*GD2
      TS          =  Kappa/(Kappa + Mu*S2N)
      FN          =  1.0D0 + Kappa - TS*Kappa
      DFDS2       =  TS*TS*Mu
      DFDGD       =  2.0D0*RhoSN*DExDN*DFDS2*FS2
      Ex          =  Ex + RhoSN*DExDN*FN
      DExDN       =  DExDGNX*FN - EightThird*DExDN*DFDS2*S2N
      DExDGNX     =  DFDGD*GRhoSNX
      DExDGNY     =  DFDGD*GRhoSNY
      DExDGNZ     =  DFDGD*GRhoSNZ
      Ex          =  Ex/(RhoSP + RhoSN)

      !write(*,*) "after EX before DExDGP"
      allocate(DExDGP(ntmp,3))
      allocate(DExDGN(ntmp,3))

      DExDGP(:,1) =  DExDGPX
      DExDGP(:,2) =  DExDGPY
      DExDGP(:,3) =  DExDGPZ
      DExDGN(:,1) =  DExDGNX
      DExDGN(:,2) =  DExDGNY
      DExDGN(:,3) =  DExDGNZ
      ntmp        = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                  ntmp          = ntmp+1
                  Ex1(ii)       = Ex(ntmp)
                  DExDP1(ii)    = DExDP(ntmp)
                  DExDN1(ii)    = DExDN(ntmp)
                  DExDGP1(ii,:) = DExDGP(ntmp,:)
                  DExDGN1(ii,:) = DExDGN(ntmp,:)
          end if
      end do

      return
      end subroutine xPBE
      end module xPBE_module


