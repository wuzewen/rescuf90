! eig.f90
    
!****************************************************
    !
    !
    !
!****************************************************
    
    module eig_module
    contains
    subroutine eig(A,eps,U,V,Vec,JT)
    
    use general2HessenbergMatrix_module
    use eigQR_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    real*8              :: eps
    
    ! temporary variables
    real*8, allocatable :: Atmp(:,:)
    
    ! output variables
    real*8, allocatable :: U(:), V(:), Vec(:,:)
    integer             :: JT
    
    ! body of this function
    call general2HessenbergMatrix(A,Atmp)
    call eigQR(A,eps,U,V,Vec,JT)
    
    return
    end subroutine eig
    end module eig_module