! DomainType.f90
    
!****************************************************************
    !
    ! This is to define a type to contain domain information
    !
!****************************************************************
    
    module DomainType_module
    
    type :: DomainType
        character(len=10)    :: bravaisLattice
        integer              :: boundary(3)
        real*8, allocatable  :: bvalx(:)
        real*8, allocatable  :: bvaly(:)
        real*8, allocatable  :: bvalz(:)
        integer              :: cgridn(3)
        integer              :: fgridn(3)
        integer              :: next(3) ! confused
        real*8               :: lowres
        real*8               :: highres
        real*8               :: latvec(3,3)
        real*8               :: recvec(3,3)
    end type DomainType
    
    end module DomainType_module