! lda_x.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    module lda_x_module
    contains
    subroutine lda_x(rho,order,zk,vrho,v2rho)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: rho(:,:)
    integer             :: order
    
    ! tmeporary variables
    real*8 :: alpha, ax, tol, pi, ax1
    !real(kind=double) :: ax1
    integer :: nx, ny, nz, ii, jj, kk!, ntmp
    !integer, allocatable :: indx(:,:), indy(:,:)
    real*8, allocatable :: dens(:), zeta(:), rs(:), opz(:), omz(:), fz(:)!, rhotmp(:,:)
    real*8, allocatable :: dfzdz(:), dedrs(:), drs(:), vrhot(:), dedz(:) 
    
    ! output variables
    real*8, allocatable :: zk(:), vrho(:,:), v2rho(:,:)
    
    ! body of this function
    pi    = 3.1415926535897932385D0
    alpha = 4.0D0/3.0D0
    alpha = 1.5D0*alpha-1.0D0
    !write(*,*) "alpha = ", alpha
    ax    = -alpha*0.458165293283142893475554485052D0
    !write(*,*) "ax =", ax
    tol   = 1.0D-15
    nx    = size(rho,1)
    ny    = size(rho,2)
    !nz    = size(rho,3)
    !write(*,*) "rho = "!, size(rho,1), size(rho,2)   
    !do ii = 1,nx,1
    !    write(*,*) ii, rho(ii,:)
    !end do
    do ii = 1,nx,1
        do jj = 1,ny,1
        !write(*,*) rho(ii,:)
            !do kk = 1,nz,1
                if (rho(ii,jj)<tol) then
                        
                    
                    rho(ii,jj) = tol
                end if
            !end do
        end do
    end do
    !ntmp = 0
    !do ii = 1,nx,1
    !    if (rho(ii,1) >= tol) then
    !            ntmp = ntmp+1
    !    end if
    !end do
    !allocate

    !write(*,*) "rho ="
    !write(*,*)  rho
    allocate(dens(nx))
    dens    = sum(rho,2)
    !do ii = 1,nx,1
    !    write(*,*) ii, dens(ii)
    !end do
    allocate(zeta(nx))
    zeta(:) = (rho(:,1)-rho(:,2))/dens
    !do ii = 1,nx,1
    !    write(*,*) ii, zeta(ii)
    !end do
    allocate(rs(nx))
    rs      = (3.000D0/4.000D0/pi/dens)**(1.000D0/3.0000D0)
    !do ii = 1,nx,1
    !   write(*,*) ii, rs(ii)
    !end do
    allocate(opz(nx))
    opz     = 1.0D0+zeta
    allocate(omz(nx))
    omz     = 1.0D0-zeta
    allocate(fz(nx))
    fz      = 0.5D0*(opz**(4.000D0/3.000D0)+omz**(4.000D0/3.000D0))
    allocate(zk(nx))
    zk      = ax*fz/rs
    
    if (order < 1) then
        return
    end if
    allocate(dfzdz(nx))
    dfzdz     = 1.0D0/6.0D0*(4.0D0*opz**(1.00D0/3.0D0)-4.0D0*omz**(1.0D0/3.0D0))
    allocate(dedrs(nx))
    dedrs     = ax*(-fz)/(rs**2.0D0)
    allocate(drs(nx))
    drs       = -1.00D0/3.00D0*rs/dens
    allocate(vrhot(nx))
    vrhot     = zk+dens*dedrs*drs
    allocate(dedz(nx))
    dedz      = ax*dfzdz/rs
    !deallocate(vrho)
    allocate(vrho(nx,2))
    vrho(:,1) = vrhot-(zeta-1.0D0)*dedz
    vrho(:,2) = vrhot-(zeta+1.0D0)*dedz
    !do ii = 1,nx,1
    !   write(*,*) ii, vrho(ii,:)
    !end do
    if (order < 2) then
        return
    end if
    
    write(*,*) "Error in lda_x.f90. order should be less than 2."
    stop
    !d2fzdz2 = 1.0/18.0*(4.0*(1/(opz**(2/3))+1/(omz**(2/3))))
    
    !ntmp = 0
    !do ii = 1,size(zeta)
    !    if (abs(zeta())==1.0) then
    !        ntmp = ntmp+1
    !        d2fzdz2(ntmp) = 1/eps
    !    end if
    !end do
    !allocate(inde(ntmp))
    !ntmp = 0
    !do ii = 1,size(zeta)
    !    if (abs(zeta())==1.0) then
    !        ntmp = ntmp+1
    !        inde(ntmp) = ii
    !    end if
    !end do
    
    !d2edrs2 =  ax*2.0*fz/rs**3
    !d2edrsz = -ax*dfzdz/rs**2
    !d2edz2  =  ax*d2fzdz2/rs
    !d2rs    = -4/3*drs/dens
    !v2rho(1,:) = dedrs*(2.0*drs+dens*d2rs)+dens*d2edrs2*drs*drs
    !sigmat(1,:) = -1
    !sigmat(3,:) =  1
    !sigmat(2,1) = -1
    !sigmat(2,2) =  1
    
    !do ii = 3,1,-1
    !    v2rho2(ii,:) = v2rho2(1,:) - d2edrsz*(2.0*zeta+sigmat(ii,1)+sigmat(ii,2))*drs+(zeta+sigmat(ii,1))*(zeta+sigmat(ii,2))*d2edz2/dens
    !end do
    
    return
    end subroutine lda_x
    end module lda_x_module
