! diafftplan.f90

      module diafftplan_module
      contains
      subroutine diafftplan(FRC)

      call dfftw_destroy_plan(FRC%planfft)

      return
      end subroutine diafftplan
      end module diafftplan_module
