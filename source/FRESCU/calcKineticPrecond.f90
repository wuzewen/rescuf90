! calcKineticPrecond.f90
    
!***********************************************************************
    !
    !
    !
!***********************************************************************
    
    module calcKineticPrecond_module
    contains
    subroutine calcKineticPrecond(FRC,degree,kpt,precond)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inversion_module
    use fftFreq_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer                         :: degree
    real*8                          :: kpt(3)
    
    ! temporary variables
    integer              :: nvec(3), ntmp, ii, jj
    real*8               :: kptt(1,3), kcart(1,3), pi
    real*8, allocatable  :: avec(:,:), bvec(:,:), ku(:,:,:), kv(:,:,:), kw(:,:,:)
    real*8, allocatable  :: x(:,:,:), precondtmp(:,:,:)
    
    ! output variables
    real*8, allocatable  :: precond(:,:,:)
    
    ! body of this function
    pi        = 3.1415926535897932385D0
    allocate(avec(3,3),bvec(3,3))
    avec      = FRC%domain%latvec
    call inversion(avec,bvec)
    bvec      = transpose(bvec)
    bvec      = 2.0D0*pi*bvec
    kptt(1,:) = kpt(:)
    kcart     = matmul(kptt,bvec)
    nvec      = FRC%domain%cgridn
    ntmp      = product(nvec)
    allocate(ku(nvec(1),nvec(2),nvec(3)))
    allocate(kv(nvec(1),nvec(2),nvec(3)))
    allocate(kw(nvec(1),nvec(2),nvec(3)))
    call fftFreq(nvec,bvec,ku,kv,kw)
    ku        = ku+kcart(1,1)
    kv        = kv+kcart(1,2)
    kw        = kw+kcart(1,3)
    !x         = ku*conjg(ku)+kv*conjg(kv)+kw*conjg(kw)
    allocate(x(nvec(1),nvec(2),nvec(3)))
    x         = ku*ku+kv*kv+kw*kw
    x         = 0.25D0*x
    
    select case (degree)
    case (-1)
        allocate(precond(0,0,0))
    case (0)
        allocate(precondtmp(nvec(1),nvec(2),nvec(3)))
        allocate(precond(nvec(1),nvec(2),1))
        precondtmp = 1.0D0/x/2.0D0
        forall(ii=1:nvec(1),jj=1:nvec(2))
            precond(ii,jj,1) = minval(precondtmp(ii,jj,:))
        end forall
    case (1)
        allocate(precondtmp(nvec(1),nvec(2),nvec(3)))
        allocate(precond(nvec(1),nvec(2),1))
        precondtmp = 1.0D0/(2.0D0*x-2.0D0)
        forall(ii=1:ntmp)
            precond(ii,jj,1) = minval(precondtmp(ii,jj,:))
        end forall
    case (2)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 9.0D0+6.0D0*x+4.0D0*x**2.0D0
        precond = precond/(precond+8.0D0*x**3D0)
    !case (3)
    !    precond = 27.0+18.0*x+4.0*x**2.0
    !    precond = precond/(precond+8.0*x**3)
    case (4)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 81.0D0+54.0D0*x+36.0D0*x**2.0D0+24.0D0*x**3.0D0+16.0D0*x**4.0D0
        precond = precond/(precond+32.0D0*x**5.0D0)
    case (5)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 243.0D0+162.0D0*x+108.0D0*x**2.0D0+72.0D0*x**3.0D0+ &
                48.0D0*x**4.0D0+32.0D0*x**5.0D0
        precond = precond/(precond+64.0D0*x**6.0D0)
    case (6)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 729.0D0+486.0D0*x+324.0D0*x**2.0D0+216.0D0*x**3.0D0+ &
                144.0D0*x**4.0D0+96.0D0*x**5.0D0+64.0D0*x**6.0D0
        precond = precond/(precond+128.0D0*x**7.0D0)
    case (7)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 2187.0D0+1458.0D0*x+972.0D0*x**2.0D0+648.0D0*x**3.0D0+ &
                432.0D0*x**4.0D0+288.0D0*x**5.0D0+192.0D0*x**6.0D0+128.0D0*x**7.0D0
        precond = precond/(precond+256.0D0*x**8.0D0)
    case default
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 81.0D0+54.0D0*x+36.0D0*x**2.0D0+24.0D0*x**3.0D0+16.0D0*x**4.0D0
        precond = precond/(precond+32.0D0*x**5.0D0)
    end select
    
    return
    end subroutine calcKineticPrecond
    end module calcKineticPrecond_module
