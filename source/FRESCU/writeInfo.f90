! writeInfo.f90

      module writeInfo_module
      contains
      subroutine writeInfo(FRC,filnam)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam

      open(unit=11,file=filnam)

      write(11,*) "Calculation Type = "
      write(11,*)  FRC%info%calculationType

      close(11)

      return
      end subroutine writeInfo
      end module writeInfo_module
