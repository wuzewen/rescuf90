! A_module.f90
    
!*********************************************
    !
    ! A data type used in initDistArray.f90
    !
!*********************************************
    
    module A_module
    
    type :: A_type
        integer :: m
        integer :: n
        integer :: mblock
        integer :: nblock
        integer :: mproc
        integer :: nproc
        real*8, allocatable :: dataA(:,:)
    end type A_type
    
    end module