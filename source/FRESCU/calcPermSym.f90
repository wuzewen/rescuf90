! calcPermSym.f90
    
!*********************************************************
    !
    !
    !
!*********************************************************
    
    module calcPermSym_module
    contains
    subroutine calcPermSym(sym,trans,pos,tol,perm,failflag)
    
    use bsxfunPlus_module
    use gendistmatper_module
    
    implicit none
    
    ! input variables
    real*8  :: sym(3,3)
    real*8  :: tol, trans(3)
    real*8, allocatable :: pos(:,:)
    
    ! temporary variables
    real*8, allocatable :: xx(:,:), transR(:,:), dist(:,:), dist2(:), spos(:,:)
    integer,allocatable :: mtmp(:)
    real*8              :: symR(3,3), eyem(3,3)
    integer             :: ii, ntmp
    logical             :: ismember
    
    ! output variables
    integer, allocatable :: perm(:)
    logical              :: failflag
    
    ! body of this function
    failflag = .FALSE.
    symR     = dble(sym)
    allocate(xx(size(pos,1),3))
    xx = matmul(pos,symR)
    allocate(transR(1,3))
    transR(1,:) = trans(:)
    allocate(spos(size(pos,1),3))
    call bsxfunPlus(xx,transR,spos)
    eyem = 0.0D0
    forall(ii=1:3)
        eyem(ii,ii) =1.0D0
    end forall
    
    call gendistmatper(pos,spos,eyem,dist)
    
    ntmp = size(dist,1)
    allocate(dist2(ntmp))
    allocate(perm(ntmp))
    
    allocate(mtmp(1))
    do ii = 1,ntmp,1
        dist2(ii) = minval(dist(ii,:))
        mtmp      = minloc(dist(ii,:))
        perm(ii)  = mtmp(1)
    end do
    
    ismember = .FALSE.
    do ii = 1,ntmp,1
        if (perm(ii) > size(pos,1) .or. perm(ii) < 1) then
            ismember = .TRUE.
            exit
        end if
    end do
    
    if ((any(dist2 > tol)) .or. ismember) then
        deallocate(perm)
        failflag = .TRUE.
        return
    end if
    
    return
    end subroutine calcPermSym
    end module calcPermSym_module
