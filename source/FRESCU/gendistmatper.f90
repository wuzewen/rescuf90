! gendistmatper.f90
    
!************************************************************
    !
    !
    !
!************************************************************
    
    module gendistmatper_module
    contains
    subroutine gendistmatper(X,Y,avec,D)
    
    use permute_module
    use bsxfunMinus_3D_module
    
    implicit none
    
    ! input variables
    real*8, allocatable :: X(:,:), Y(:,:)
    real*8              :: avec(3,3)
    
    ! temporary variables
    integer :: tmp(3), sizeD(3)
    real*8, allocatable :: Xtmp(:,:), Ytmp(:,:)
    real*8, allocatable :: XX(:,:,:), YY(:,:,:), Xout(:,:,:), Yout(:,:,:), Dtmp(:,:,:), Dtmt(:,:)
    
    ! output variables
    real*8, allocatable :: D(:,:)
    
    ! body of this function
    allocate(Xtmp(size(X,1),3))
    allocate(Ytmp(size(Y,1),3))
    Xtmp = matmul(X,avec)
    Ytmp = matmul(Y,avec)
    tmp  = (/3,1,2/)
    allocate(XX(size(X,1),size(X,2),1),YY(size(Y,1),size(Y,2),1))
    XX(:,:,1) = Xtmp(:,:)
    YY(:,:,1) = Ytmp(:,:)
    call permute(YY,tmp,Yout)
    tmp  = (/1,3,2/)
    call permute(XX,tmp,Xout)
    allocate(Dtmp(max(size(Yout,1),size(Xout,1)),max(size(Yout,2),size(Xout,2)),max(size(Yout,3),size(Xout,3))))
    call bsxfunMinus_3D(Yout,Xout,Dtmp)
    Dtmp = Dtmp - anint(Dtmp)
    
    sizeD(1) = size(Dtmp,1)
    sizeD(2) = size(Dtmp,2)
    sizeD(3) = size(Dtmp,3)
    allocate(Dtmt(product(sizeD)/3,3))
    Dtmt = reshape(Dtmp,(/product(sizeD)/3,3/))
    Dtmt = matmul(Dtmt,avec)
    Dtmp = reshape(Dtmt,(/sizeD(1),sizeD(2),sizeD(3)/))
    allocate(D(sizeD(1),sizeD(2)))
    D = (sum(Dtmp*Dtmp,3))**0.5D0
    
    return
    end subroutine gendistmatper
    end module gendistmatper_module
    
