! INITDISTARRAY.f90

!********************************************************************************
      !
      !
      !
!**********************************************************************************

      module INITDISTARRAY

      interface INITDISTARRAY
              module procedure InitDistArray_1D
              module procedure InitDistArray_2D
              module procedure InitDistArray_CMPLX_1D
              module procedure InitDistArray_CMPLX_2D
      end interface INITDISTARRAY

      end module INITDISTARRAYMOD
