! writeDiffop.f90

      module writeDiffop_module
      contains
      subroutine writeDiffop(FRC,filnam)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam
      integer                         :: ii

      open(unit=11,file=filnam)

      write(11,*) "accuracy" 
      write(11,*)  FRC%diffop%accuracy
      write(11,*) "method"
      write(11,*)  FRC%diffop%method
      write(11,*) "Du"
      do ii = 1,size(FRC%diffop%Du,1),1
          write(11,*)  FRC%diffop%Du(ii,:)
      end do
      write(11,*) "Dv"
      do ii = 1,size(FRC%diffop%Dv,1),1
          write(11,*)  FRC%diffop%Dv(ii,:)
      end do
      write(11,*) "Dw"
      do ii = 1,size(FRC%diffop%Dw,1),1
          write(11,*) FRC%diffop%Dw(ii,:)
      end do
      write(11,*) "fDu"
      do ii = 1,size(FRC%diffop%fDu,1),1
          write(11,*) FRC%diffop%fDu(ii,:)
      end do
      write(11,*) "fDv"
      do ii = 1,size(FRC%diffop%fDv,1),1
          write(11,*) FRC%diffop%fDv(ii,:)
      end do
      write(11,*) "fDw"
      do ii = 1,size(FRC%diffop%fDw,1),1
          write(11,*)  FRC%diffop%fDw(ii,:)
      end do
      write(11,*) "Duu"
      do ii = 1,size(FRC%diffop%Duu,1),1
          write(11,*)  FRC%diffop%Duu(ii,:)
      end do
      write(11,*) "Dvv"
      do ii = 1,size(FRC%diffop%Dvv,1),1 
          write(11,*)  FRC%diffop%Dvv(ii,:)
      end do
      write(11,*) "Dww"
      do ii = 1,size(FRC%diffop%Dww,1),1
          write(11,*) FRC%diffop%Dww(ii,:)
      end do
      write(11,*) "fDuu"
      do ii = 1,size(FRC%diffop%fDuu,1),1
          write(11,*)  FRC%diffop%fDuu(ii,:) 
      end do
      write(11,*) "fDvv"
      do ii = 1,size(FRC%diffop%fDvv,1),1
          write(11,*)  FRC%diffop%fDvv(ii,:)
      end do
      write(11,*) "fDww"
      do ii = 1,size(FRC%diffop%fDww,1),1 
          write(11,*) FRC%diffop%fDww(ii,:) 
      end do
      
      close(11)
      return
      end subroutine writeDiffop
      end module writeDiffop_module
