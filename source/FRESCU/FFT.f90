! FFT.f90

!****************************************************************************************
      !
      !
      !
!****************************************************************************************

      module FFTMOD

      interface FFT
              module procedure fft2
              module procedure fft_fftw
              module procedure fft_fftw_pointer
              module procedure fft4
      end interface FFT
      end module FFTMOD
