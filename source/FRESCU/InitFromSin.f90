! InitFromSin.f90
    
!**********************************************************************************
    !
    ! 
    !
!**********************************************************************************
    
    module InitFromSin_module
    contains
    subroutine InitFromSin(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ndgrid_module
    use sort_1D_module
    use modBCDist_2D_module
    use modBCDist_CMPLX_2D_module
    use GetGlobalInd_2D_module
    use GetGlobalInd_CMPLX_2D_module
    use distmat_allgather_2D_module
    use distmat_allgather_1D_module
    use projectHamiltonian_module
    use getKBorb_module
    use GetKBorb_Sparse_module
    use bsxfunTimes_CMPLX_module
    use updateVeff_module
    use GenHamFun_module
    use MPI_Allreduce_sum_real8_1D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    real*8               :: pi, omgx, omgy, omgz, phix, phiy, phiz, kpt(3)
    integer              :: mpirank, mpisize, ispin, nspin, nvec(3)
    integer              :: nx, ny, nz, nband, cn, ntmp, ntmp2, nkpt
    integer              :: ii, jj, kk, nn, ss, intmp, qq, nia, nja
    integer              :: initntmp
    logical              :: mpistat, rrfind
    real*8,  allocatable :: kdir(:,:), avec(:,:), xtmp(:), ytmp(:), ztmp(:)
    real*8,  allocatable :: nrg(:), nrgU(:), tmp(:,:)
    real*8,  allocatable :: vt(:), itmp(:,:,:), jtmp(:,:,:), ktmp(:,:,:)
    real*8,  allocatable :: ijk2(:,:,:), xt(:,:,:), yt(:,:,:), zt(:,:,:)
    real*8,  allocatable :: ijk2tmp(:), ijktmp2(:), itmptmp(:), jtmptmp(:)
    real*8,  allocatable :: ktmptmp(:), xtt(:), ytt(:), ztt(:)
    real*8,  allocatable :: vloc(:)
    integer, allocatable :: ind2(:), iitmp(:), jjtmp(:), kktmp(:)
    integer, allocatable :: iA(:,:), jA(:,:), ranks(:)
    complex*16              :: imnu
    complex*16, allocatable :: phase(:,:), vatatmp(:,:), vdt(:,:), vtd(:,:)
    complex*16, allocatable :: ham(:,:), nonsense1(:,:), nonsense2(:,:)
    type(VataMN_2D)       :: veff2D
    type(VataMN_CMPLX_2D) :: vs, vstmp
    type(VataMN_1D)       :: veff_1D, veffin, veff
    type(VataMN_CMPLX_2D) :: XHX, XSX
    
    character(len=20)    :: flax, numb
    integer :: cou
    ! output variables
    
    ! body of this function
    imnu    = (0.0D0,1.0D0)
    pi      = 3.1415926535897932385D0
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    nkpt    = size(FRC%kpoint%ikdirect,1)
    allocate(kdir(nkpt,3))
    kdir    = FRC%kpoint%ikdirect
    allocate(avec(3,3))
    avec    = FRC%domain%latvec
    nvec    = FRC%domain%cgridn
    nx      = nvec(1)
    ny      = nvec(2)
    nz      = nvec(3)
    nband   = FRC%eigensolver%nband
    cn      = product(nvec)
    ntmp    = ceiling(dble(nband)**(1.0D0/3.0D0))
    ntmp2   = ceiling(dble(ntmp)/2.0D0)

    allocate(vt(2*ntmp2+1))
    forall(ii=1:2*ntmp2+1)
        vt(ii) = ii-ntmp2-1
    end forall
    allocate(nrg(nband))
    nrg  = 0.0D0
    allocate(nrgU(nband))
    nrgU = 0.0D0
    allocate(itmp(2*ntmp2+1,2*ntmp2+1,2*ntmp2+1))
    allocate(jtmp(2*ntmp2+1,2*ntmp2+1,2*ntmp2+1))
    allocate(ktmp(2*ntmp2+1,2*ntmp2+1,2*ntmp2+1))
    call ndgrid(vt,vt,vt,itmp,jtmp,ktmp)


    allocate(ijk2(2*ntmp2+1,2*ntmp2+1,2*ntmp2+1))
    ijk2 = itmp**2+jtmp**2+ktmp**2
    intmp   = size(ijk2,1)*size(ijk2,2)*size(ijk2,3)
    allocate(ijk2tmp(intmp))
    ijk2tmp = reshape(ijk2,(/intmp/))
    call sort_1D(ijk2tmp,ijktmp2,ind2)
    !write(*,*) "ind2 ="
    !write(*,*)  ind2
    allocate(itmptmp(intmp),jtmptmp(intmp),ktmptmp(intmp))
    itmptmp = reshape(itmp,(/intmp/))
    jtmptmp = reshape(jtmp,(/intmp/))
    ktmptmp = reshape(ktmp,(/intmp/))
    allocate(iitmp(intmp),jjtmp(intmp),kktmp(intmp))
    forall(ii=1:intmp)
        iitmp(ii) = itmptmp(ind2(ii))
        jjtmp(ii) = jtmptmp(ind2(ii))
        kktmp(ii) = ktmptmp(ind2(ii))
    end forall
    !deallocate(ijk2tmp)
    !allocate(ijk2tmp(2*ntmp2+1))
    ijk2tmp = iitmp**2+jjtmp**2+kktmp**2
    
    ntmp = count(ijk2tmp == 0)
    ntmp = intmp-ntmp
    deallocate(itmptmp,jtmptmp,ktmptmp)
    allocate(itmptmp(ntmp),jtmptmp(ntmp),ktmptmp(ntmp))
    ntmp = 0
    do ii = 1,intmp,1
        if (ijk2tmp(ii) /= 0) then
            ntmp = ntmp+1
            itmptmp(ntmp) = iitmp(ii)
            jtmptmp(ntmp) = jjtmp(ii)
            ktmptmp(ntmp) = kktmp(ii)
        end if
    end do
    initntmp = ceiling(dble(cn)/dble(mpisize))
    call initDistArray_CMPLX_2D(cn,nband,initntmp,1,mpisize,1,FRC%psi(1,1))
    !call initDistArray_CMPLX_2D(cn,nband,initntmp,1,mpisize,1,vs)
    call GetGlobalInd_CMPLX_2D(mpirank,FRC%psi(1,1),ia,ja)
    !call GetGlobalInd_CMPLX_2D(mpirank,vs,ia,ja)
    nia  = size(ia)
    nja  = size(ja)
    !allocate(vs%vata(nia,nja))
    allocate(phase(nia,1))

    allocate(xtmp(nx))
    allocate(ytmp(ny))
    allocate(ztmp(nz))
    forall(ii=1:nx)
        xtmp(ii) = dble(ii)/dble(nx)
    end forall
    forall(ii=1:ny)
        ytmp(ii) = dble(ii)/dble(ny)
    end forall
    forall(ii=1:nz)
        ztmp(ii) = dble(ii)/dble(nz)
    end forall
    allocate(xt(nx,ny,nz),yt(nx,ny,nz),zt(nx,ny,nz))
    call ndgrid(xtmp,ytmp,ztmp,xt,yt,zt)
    
    deallocate(xtmp,ytmp,ztmp)
    allocate(xtmp(nia),ytmp(nia),ztmp(nia))
    allocate(xtt(nx*ny*nz),ytt(nx*ny*nz),ztt(nx*ny*nz))
    xtt = reshape(xt,(/nx*ny*nz/))
    ytt = reshape(yt,(/nx*ny*nz/))
    ztt = reshape(zt,(/nx*ny*nz/))
    !write(*,*) 'in InitFromSin, nia =', nia
    forall(ii=1:nia)
        xtmp(ii) = xtt(ia(1,ii))
        ytmp(ii) = ytt(ia(1,ii))
        ztmp(ii) = ztt(ia(1,ii))
    end forall
    !allocate(FRC%potential%veffin(1))
    !allocate(veffin%Vata(size(FRC%potential%veffin(1)%vata)))
    !veffin = FRC%potential%veffin(1)
    !write(*,*) 'veffin%n =', veffin%n
    !veffin%Vata(:,1) = FRC%potential%veffin(1)%vata
    !veffin%m         = FRC%potential%veffin(1)%m
    !veffin%n         = 1 !FRC%potential%veffin(1)%n
    FRC%potential%veffin(1)%n = 1
    !veffin%mblock    = FRC%potential%veffin(1)%mblock
    !veffin%nblock    = FRC%potential%veffin(1)%nblock
    !veffin%mproc     = FRC%potential%veffin(1)%mproc
    !veffin%nproc     = FRC%potential%veffin(1)%nproc
    call UpdateVeff(FRC,FRC%potential%veffin(1),FRC%potential%vps,.FALSE.,veff)


    !write(*,*) "veff%vata ="
    !write(*,*)  veff%vata
    !stop
    !allocate(vloc(cn))
    !write(*,*) "veff", veff%m, veff%n, veff%mblock, veff%nblock, veff%mproc, veff%nproc

    call distmat_allgather_1D(FRC,veff,vloc)

    !write(*,*) "vloc ="
    !write(*,*)  vloc
    FRC%potential%vloc(:,1) = vloc
    deallocate(vloc)
    !write(*,*) "vloc ="
    call InitDistArray_CMPLX_2D(FRC%psi(1,1)%m,FRC%psi(1,1)%n,1,1,1,mpisize,vstmp)
    deallocate(ia,ja)
    call GetGlobalInd_CMPLX_2D(mpirank,vstmp,ia,ja)
    allocate(vstmp%vata(size(ia),size(ja)))
    do kk = 1,nkpt,1
        kpt = kdir(kk,:)
        !write(*,*) "Okpt ="
        !write(*,*)  kpt
        !deallocate(FRC%potential%vnl%KBorb)
        !write(*,*) "I'm here before GetKBorb in InitFromSin.", kk
        if (FRC%potential%vnlScheme) then
                if (FRC%potential%vnl%KBorbIsSparse) then
                        call GetKBorb_Sparse(FRC,kpt)
                else
                        call GetKBorb(FRC,kpt)
                end if
        end if
        !write(*,*) "Gkpt =" 
        !write(*,*)  kpt
        !write(*,*)  mpirank,FRC%potential%vnl%sparseKBorb%N_nonzero
        !write(*,*) "I'm here after GetKBorb in InitFromSin."
        do ss = 1,nspin,1
            call initDistArray_CMPLX_2D(cn,nband,initntmp,1,mpisize,1,FRC%psi(kk,ss))
            !vs = vsnew
            do nn = 1,nband,1
                omgx = 2.0D0*pi*dble(itmptmp(nn))
                omgy = 2.0D0*pi*dble(jtmptmp(nn))
                omgz = 2.0D0*pi*dble(ktmptmp(nn))
                if (omgx < 0) then
                    phix = (-1.0D0)*pi/4.0D0+pi/4.0D0
                else if (omgx == 0) then
                    phix = 0.0D0+pi/4.0D0
                else
                    phix = pi/4.0D0+pi/4.0D0
                end if
                
                if (omgy < 0) then
                    phiy = (-1.0D0)*pi/4.0D0+pi/4.0D0
                else if (omgy == 0) then
                    phiy = 0.0D0+pi/4.0D0
                else
                    phiy = pi/4.0D0+pi/4.0D0
                end if
                
                if (omgz < 0) then
                    phiz = (-1.0D0)*pi/4.0D0+pi/4.0D0
                else if (omgz == 0) then
                    phiz = 0.0D0+pi/4.0D0
                else
                    phiz = pi/4.0D0+pi/4.0D0
                end if
                !phiy = sign(omgy)*pi/4.0+pi/4.0
                !phiz = sign(omgz)*pi/4.0+pi/4.0
                !write(*,*) 'loop', kk, ss, nn
                !write(*,*) 'size ia', size(ia)
                !write(*,*) 'size xtmp', size(xtmp)
                forall(ii=1:size(xtmp))
                        FRC%psi(kk,ss)%vata(ii,nn) = dcmplx(cos(omgx*xtmp(ii)+phix)*cos(omgy*ytmp(ii)+phiy)*cos(omgz*ztmp(ii)+phiz))
                    !vs%vata(ii,nn) = dcmplx(cos(omgx*xtmp(ii)+phix)*cos(omgy*ytmp(ii)+phiy)*cos(omgz*ztmp(ii)+phiz))
                end forall
                !write(*,*) "omgx, omgy, omgz ="
                !write(*,*)  omgx, omgy, omgz
            end do

            !write(*,*) "I'm here after band loop."
            !write(*,*) "vs%vata ="
            !do ii = 1,size(vs%vata,1),1
            !do jj = 1,size(vs%vata,2),1

            !write(*,*) ii,jj, vs%vata(ii,jj)
            !end do
            !end do
            !stop
            
            if (any(kpt /= 0)) then
                !allocate(phase(size(ia),1))
                forall(ii=1:size(xtmp))
                    phase(ii,1) = exp(-imnu*(xtmp(ii)*kpt(1)+ytmp(ii)*kpt(2)+ztmp(ii)*kpt(3)))
                end forall
                !allocate(vatatmp(size(vs%vata,1),size(vs%vata,2)))
                !vatatmp = dcmplx(vs%vata)
                !call bsxfunTimes_CMPLX(vatatmp,phase,vatatmp)
                !vs%vata = vatatmp
                !deallocate(vatatmp)
                !deallocate(phase,vatatmp)
                do ii = 1,size(FRC%psi(kk,ss)%vata,2),1
                    FRC%psi(kk,ss)%vata(:,ii) = FRC%psi(kk,ss)%vata(:,ii)*phase(:,1)
                    !vs%vata(:,ii) = vs%vata(:,ii)*phase(:,1)
                end do
            end if
            !write(*,*) "after phase, phase ="
            !write(*,*)  phase

            !stop
            !write(*,*) "fkpt ="
            !write(*,*)  kpt   
            if (ispin == 1 .or. ispin == 2) then
                !call projectHamiltonian(FRC,kpt,vs,XHX,XSX)
            else if (ispin == 4) then
                write(*,*) "Error in InitFromSin.f90. Non-collinear spin is not available now."
                stop
            end if
            !write(*,*) 'here after projectH.'
            !write(*,*) "xkpt ="
            !write(*,*)  kpt

            !write(*,*) "XHX%vata = "
            !write(*,*)  XHX%vata

            !write(*,*) "XSX%vata = "
            !write(*,*)  XSX%vata

            !stop
                
            rrfind = .FALSE.
            if (rrfind) then
                write(*,*) "Error in InitFromSin.f90. rr is not available now."
                stop
            else
                !write(*,*) 'in initfromsin, before initarray.'
                !call InitDistArray_CMPLX_2D(vs%m,vs%n,1,1,1,mpisize,vstmp)
                !call InitDistArray_CMPLX_2D(FRC%psi(kk,ss)%m,FRC%psi(kk,ss)%n,1,1,1,mpisize,vstmp)
                !deallocate(ia,ja)
                !write(*,*) 'in initfromsin, before getglobalind.'

                !call GetGlobalInd_CMPLX_2D(mpirank,vstmp,ia,ja)
                !if (allocated(vstmp%vata)) then
                !        deallocate(vstmp%vata)
                !end if
                !allocate(vstmp%vata(size(ia),size(ja)))
                !write(*,*) 'in initfromsin, after getglobalind.'
                !write(*,*) 'vs', vs%m, vs%n, vs%mblock, vs%nblock, vs%mproc, vs%nproc
                !write(*,*) 'vata', size(vs%vata,1), size(vs%vata,2)
                !write(*,*) 'vstmp', vstmp%m, vstmp%n, vstmp%mblock, vstmp%nblock, vstmp%mproc, vstmp%nproc
                !write(*,*) 'vata', size(vstmp%vata,1), size(vstmp%vata,2)
                !call ModBCDist_CMPLX_2D(FRC,vs,1,1,1,mpisize,.false.,.false.,vstmp)
                call ModBCDist_CMPLX_2D(FRC,FRC%psi(kk,ss),1,1,1,mpisize,.false.,.false.,vstmp)
                !deallocate(ia,ja)
                
                nrg = 0
                allocate(vtd(1,size(vstmp%vata,1)))
                allocate(vdt(size(vstmp%vata,1),1))
                allocate(ham(size(vstmp%vata,1),1))
                ham = (0.0D0,0.0D0)
                allocate(tmp(size(vtd,1),size(ham,2)))
                !write(*,*) "vkpt ="
                !write(*,*)  kpt
                ! I don't think the loop below is nessesary, maybe the last loop account
                ! So just size jj = size(vstmp%vata,2),size(vstmp%vata,2),1 to have a try
                do jj = size(vstmp%vata,2),size(vstmp%vata,2),1 
                !do jj = 1,size(vstmp%vata,2),1
                    vtd(1,:) = conjg(vstmp%vata(:,jj))
                    vdt(:,1) = vstmp%vata(:,jj)
                    call GenHamFun(FRC,kpt,1,.FALSE.,vdt,ham,nonsense1,nonsense2)
                    tmp      = dble(matmul(vtd,ham))
                    !nrg(:,:,:) = tmp(:,:)a
                    !write(*,*) 'in initFromsin, after GenHamFum',jj
                    forall(qq=1:size(ja))
                        nrg(ja(1,qq)) = tmp(1,1)
                    end forall
                    !write(*,*) "size of vtd ="
                    !write(*,*)  size(vtd,1), size(vtd,2)
                    !write(*,*) "rank, vtd =", mpirank
                    !write(*,*)  vtd
                    !write(*,*) "size of ham"
                    !write(*,*)  size(ham,1), size(ham,2)
                    !write(*,*) "rank, ham =", mpirank
                    !write(*,*)  ham
                    !write(*,*) "tmp ="
                    !write(*,*)  tmp

                    !write(*,*) "Stop Here"
                    !stop


                end do

                !write(*,*) "vtd ="
                !write(*,*)  vtd
                !write(*,*) "kpt ="
                !write(*,*)  kpt
                !write(*,*) "ham ="
                !write(*,*)  ham
                !write(*,*) "rank, nrg = ", mpirank
                !write(*,*)  nrg
                    
                if (mpistat) then
                    !write(*,*) "Error in InitFromSin.f90. MPI is not available now."
                    !stop
                    !
                    allocate(ranks(mpisize))
                    forall(jj=1:mpisize)
                            ranks(jj) = jj-1
                    end forall
                    call MPI_Allreduce_sum_real8_1D(nrg,ranks,nrgU)
                    nrg = nrgU
                    deallocate(ranks)
                end if
                    
                !if (ispin == 1 .or. ispin == 2) then
                    !allocate(FRC%psi(kk,ss)%vata(size(vs%vata,1),size(vs%vata,2)))
                    !FRC%psi(kk,ss) = vs
                    !write(*,*) 'size of psi.'
                    !write(*,*) size(FRC%psi(kk,ss)%vata)
                    !stop
                !else if (ispin == 4) then
                !    write(*,*) "Error in InitFromSin.f90. Non-collinear spin is not available."
                !    stop
                !end if
                
                FRC%energy%ksnrg%vata(:,kk,ss) = nrg(:)
                deallocate(vtd,vdt,ham,tmp)
                !deallocate(XHX%vata,XSX%vata,vtd,vdt,ham,tmp)
            end if
        end do
        !write(*,*) "stop here."
        !stop

    end do

    !write(numb,"(i4.4)") mpirank
    !flax = 'InitSS'//trim(numb)//'.txt'
    !cou  =  mpirank + 10
    !open(unit=cou,file=flax)
    !write(cou,*) mpistat, mpirank
    !do ii = 1,size(FRC%psi,1),1
    !do jj = 1,size(FRC%psi,2),1
    !write(cou,*) ii, jj
    !write(cou,*) size(FRC%psi(ii,jj)%vata,1),size(FRC%psi(ii,jj)%vata,2)
    !do kk = 1,size(FRC%psi(ii,jj)%vata,2),1
    !write(cou,*) 'cloumn', kk
    !write(cou,*) FRC%psi(ii,jj)%vata(:,kk)
    !write(cou,*) "FRC%energy%ksnrg%vata ="
    !end do
    !write(cou,*) "FRC%energy%ksnrg%vata ="
    !write(cou,*)  FRC%energy%ksnrg%vata(:,ii,jj)
    !end do
    !end do
    !write(cou,*) "FRC%energy%ksnrg%vata ="
    !write(cou,*)  FRC%energy%ksnrg%vata
    !write(cou,*) 'nrg =', nrg
    !close(cou)
    !write(*,*) "FRC%psi1 ="
    !write(*,*)  FRC%psi(1,1)%vata(3,3)
    !write(*,*) "FRC%psi2 ="
    !write(*,*)  FRC%psi(2,1)%vata
    !write(*,*) "FRC%energy%ksnrg%vata ="
    !write(*,*)  FRC%energy%ksnrg%vata
        
    !write(*,*) "Stop in InitFromSin."
    !stop

    return
    end subroutine InitFromSin
    end module InitFromSin_module
