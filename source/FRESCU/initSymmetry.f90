! initSymmetry.f90
!****************************
    !
!****************************
    
    module initSymmetry_module
    contains
    subroutine initSymmetry(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    use inversion_module
    use symIndex_module
    use rmSymNonPeriodic_module
    use find_sym_op_module
    
    implicit none
    
    ! input variabes
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temperary variables
    integer :: nline, nkpt, ii, status, ntmp, mtmp
    logical :: kpointNxIsFind, kpointNyIsFind, kpointNzIsFind, spaceSymmetryIsFind, pointSymmetryIsFind, timereversalIsFind
    logical :: spacesym, ph
    real*8  :: tol
    !type(elementType) :: element
    integer, allocatable :: element(:), idel(:), inde(:), sym_rec(:,:,:), sym_dir(:,:,:)
    real*8,  allocatable :: avec(:,:), pos(:,:), tmp(:,:), sym_t(:,:), sym_perm(:,:)
    real*8,  allocatable :: eyem(:,:), magmom(:,:)
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%symmetry) then
        !ii = 0
        !do while(ii<=nline)
        !    ii     = ii+1
        !    if (trim(inputFromFile(ii)%name) == trim("symmetry")) then
        !        symmetryIsFind = .TRUE.
        !        exit
        !    end if
        !end do
        !if (symmetryIsFind) then
        !    FRC%symmetry = inputFromFile(ii)%value
        !end if
        kpointNxIsFind      = .FALSE.
        kpointNyIsFind      = .FALSE.
        kpointNzIsFind      = .FALSE.
        spaceSymmetryIsFind = .FALSE.
        pointSymmetryIsFind = .FALSE.
        timereversalIsFind  = .FALSE.
        spacesym            = .FALSE.
        ph    = .FALSE.
        tol   = 1.0D-6
        nline = inputFromFile%NumberofInput
        
        nkpt  = 1
        ii    = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("kpointNx")) then
                kpointNxIsFind = .TRUE.
                exit
            end if
        end do
        if (kpointNxIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%kpoint%gridn(1)
        else
            write(*,*) "Kpoint number on X direction is not given. FRC%kpoint%gridn(1) will set to be 1."
            FRC%kpoint%gridn(1) = 1
        end if
        
        ii    = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("kpointNy")) then
                kpointNyIsFind = .TRUE.
                exit
            end if
        end do
        if (kpointNyIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%kpoint%gridn(2)
        else
            write(*,*) "Kpoint number on Y direction is not given. FRC%kpoint%gridn(2) will set to be 1."
            FRC%kpoint%gridn(2) = 1
        end if
        
        ii    = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("kpointNz")) then
                kpointNzIsFind = .TRUE.
                exit
            end if
        end do
        if (kpointNzIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%kpoint%gridn(3)
        else
            write(*,*) "Kpoint number on Z direction is not given. FRC%kpoint%gridn(3) will set to be 1."
            FRC%kpoint%gridn(3) = 1
        end if
        
        if (.not. (kpointNxIsFind .and. kpointNyIsFind .and. kpointNzIsFind)) then
            FRC%kpoint%gridn = 1
        end if
        
        nkpt = product(FRC%kpoint%gridn)
        
        ii    = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("spaceSymmetry")) then
                spaceSymmetryIsFind = .TRUE.
                exit
            end if
        end do
        if (spaceSymmetryIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%symmetry%spaceSymmetry = .FALSE.
            else
                FRC%symmetry%spaceSymmetry = .TRUE.
            end if
        else
            FRC%symmetry%spaceSymmetry = .TRUE.
        end if
        
        ii    = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("pointSymmetry")) then
                pointSymmetryIsFind = .TRUE.
                exit
            end if
        end do
        if (pointSymmetryIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%symmetry%pointSymmetry = .FALSE.
            else
                FRC%symmetry%pointSymmetry = .TRUE.
            end if
        else
            FRC%symmetry%pointSymmetry = .TRUE.
        end if
        
        ii    = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("timeReversal")) then
                timeReversalIsFind = .TRUE.
                exit
            end if
        end do
        if (timeReversalIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%symmetry%timeReversal = .FALSE.
            else
                FRC%symmetry%timeReversal = .TRUE.
            end if
        else
            FRC%symmetry%timeReversal = .TRUE.
        end if
        
        if (trim(FRC%info%calculationType) == trim("band-structure") .or. & 
                trim(FRC%info%calculationType) == trim("dfpt") .or. &
                nkpt==1 .or. trim(FRC%spin%spintype) == trim("non-collinear")) then
            FRC%symmetry%spaceSymmetry = .FALSE.
            FRC%symmetry%pointSymmetry = .FALSE.
            FRC%symmetry%timeReversal  = .FALSE.
        !else
        !    FRC%symmetry%spaceSymmetry = .TRUE.
        !    FRC%symmetry%pointSymmetry = .TRUE.
        !    FRC%symmetry%timeReversal  = .TRUE.
        end if
        
        if (FRC%symmetry%spaceSymmetry) then
            FRC%symmetry%pointSymmetry = .TRUE.
        end if
        
        allocate(element(FRC%atom%numberOfAtom))
        element = FRC%atom%element
        allocate(avec(3,3),tmp(3,3))
        avec    = FRC%domain%latvec
        call inversion(avec,tmp)
        allocate(pos(FRC%atom%numberOfAtom,3))
        pos     = matmul(FRC%atom%xyz,tmp)
        
        if (FRC%symmetry%pointSymmetry) then
            spacesym = FRC%symmetry%spaceSymmetry
            if (size(FRC%atom%magmom) /= 0) then
                allocate(magmom(FRC%atom%NumberOfAtom,size(FRC%atom%magmom,2)))
                magmom   = FRC%atom%magmom
            end if
            
            !magmom   = FRC%atom%magmom
            
            if (FRC%mpi%rank == 0 .and. size(pos,1) >= 1000) then
                write(*,*) "warning. You are trying to calculate the symmetries of a unit cell including more" // &
               " than 1000 atoms. This is likely to be slow and require a lot of memory. Is it really what you want?"
            end if
            !write(*,*) "input of find_sym_op in initsymmetry."
            !write(*,*) "element"
            !write(*,*)  element
            !write(*,*) "pos"
            !write(*,*)  pos
            !write(*,*) "avec"
            !write(*,*)  avec
            !write(*,*) "spacesym"
            !write(*,*)  spacesym
            !write(*,*) "tol"
            !write(*,*)  tol
            call find_sym_op(element,pos,avec,spacesym,tol,ph,magmom,sym_dir,sym_t,sym_perm,sym_rec)
            !call eye(3,eyem)
            !write(*,*) "sym_dir ="
            !write(*,*)  sym_dir

            allocate(eyem(3,3))
            eyem = 0.0D0
            forall(ii=1:3)
                eyem(ii,ii) = 1.0D0
            end forall

            !write(*,*) "sym_t in initSymmetry.", size(sym_t,1), size(sym_t,2)
            !do ii = 1,size(sym_t,1),1
            !write(*,*)  sym_t(ii,:)
            !end do
            !write(*,*) "sym_rec in initSymmetry.", size(sym_rec,1), size(sym_rec,2), size(sym_rec,3)
            !do ii = 1,size(sym_rec,3),1
            !write(*,*) ii
            !write(*,*)  sym_rec(:,:,ii)
            !end do
            
            call symIndex(sym_rec,eyem,inde)
            ntmp = size(inde)
            do ii = 1,ntmp,1
                sym_t(inde(ntmp),:) = 0
            end do
            
            
            !call cat(3,sym_dir(:,:,index),sym_dir(:,:,(.not. index)),sym_dir)
            !call cat(3,sym_rec(:,:,index),sym_rec(:,:,(.not. index)),sym_rec)
            !call cat(3,sym_perm(:,:,index),sym_perm(:,:,(.not. index)),sym_perm)
            !call cat(1,sym_t(index,:),sym_t((.not. index),:))
            !sym_t(1,:) = 0
            
            !index = any(sym_t,2)
            !call cat(3,sym_dir(:,:,(.not. index)),sym_dir(:,:,index),sym_dir)
            !call cat(3,sym_rec(:,:,(.not. index)),sym_rec(:,:,index),sym_rec)
            !call cat(3,sym_perm(:,:,(.not. index)),sym_perm(:,:,index),sym_perm)
            !call cat(1,sym_t((.not. index),:),sym_t(index,:))
            
            call rmSymNonPeriodic(FRC%domain%boundary,sym_dir,sym_t,idel)
            ntmp = size(idel)
            mtmp = size(sym_dir,3)
            !write(*,*) "ntmp =",ntmp,"mtmp =",mtmp
            allocate(FRC%symmetry%sym_dir(3,3,mtmp-ntmp))
            allocate(FRC%symmetry%sym_t(mtmp-ntmp,3))
            allocate(FRC%symmetry%sym_perm(size(Sym_perm,1),mtmp-ntmp))
            allocate(FRC%symmetry%sym_rec(3,3,mtmp-ntmp))
            ntmp = 0
            do ii = 1,mtmp,1
                if (all(idel /= ii)) then
                    ntmp = ntmp+1
                    FRC%symmetry%sym_dir(:,:,ntmp) = sym_dir(:,:,ntmp)
                    FRC%symmetry%sym_t(ntmp,:)     = sym_t(ntmp,:)
                    FRC%symmetry%sym_perm(:,ntmp)  = sym_perm(:,ntmp)
                    FRC%symmetry%sym_rec(:,:,ntmp) = sym_rec(:,:,ntmp)
                end if
            end do
                    
            !sym_dir(:,:,idel) = 0
            !sym_t(idel,:)     = 0
            !sym_perm(:,idel)  = 0
            !sym_rec(:,:,idel) = 0
        else
            allocate(FRC%symmetry%sym_dir(3,3,1))
            allocate(FRC%symmetry%sym_rec(3,3,1))
            allocate(FRC%symmetry%sym_t(1,3))
            allocate(FRC%symmetry%sym_perm(1,FRC%Atom%numberOfAtom))
            FRC%symmetry%sym_dir = 0
            FRC%symmetry%sym_t   = 0
            FRC%symmetry%sym_rec = 0
            forall(ii=1:3)
                FRC%symmetry%sym_dir(ii,ii,1) = 1
                FRC%symmetry%sym_rec(ii,ii,1) = 1
            end forall
            forall(ii=1:FRC%Atom%numberOfAtom)
                FRC%symmetry%sym_perm(1,ii) = ii
            end forall
            
            !call eye(3,eyem)
            !sym_dir = eyem
            !sym_rec = eyem
            !sym_t   = 0
            !natom = size(FRC%atom%element,1)
            !forall(ii=1:natom)
            !    sym_perm(ii) = ii
            !end forall
        end if
        
        FRC%init%symmetry = .TRUE.
    end if
    
    return
    end subroutine initSymmetry
    end module initSymmetry_module
    

        
