! precondT.f90
    
!***************************************************************
    !
    !
    !
!***************************************************************
    
    module precondT_module
    contains
    subroutine precondT(FRC,degree,precond)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inversion_module
    use fftFreq_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer :: degree
    
    ! temporary variables
    real*8, allocatable :: avec(:,:), bvec(:,:), ku(:,:,:), kv(:,:,:), kw(:,:,:), k2(:,:,:)
    real*8              :: pi
    integer             :: nvec(3), ii, jj
    
    ! output variables
    real*8, allocatable :: precond(:,:,:)
    
    ! body of this function
    allocate(avec(3,3))
    allocate(bvec(3,3))
    pi   = 3.1415926535897932385D0
    avec = FRC%domain%latvec
    nvec = FRC%domain%cgridn
    call inversion(avec,bvec)
    bvec = 2.0D0*pi*transpose(bvec)
    allocate(ku(nvec(1),nvec(2),nvec(3)))
    allocate(kv(nvec(1),nvec(2),nvec(3)))
    allocate(kw(nvec(1),nvec(2),nvec(3)))
    call fftFreq(nvec,bvec,ku,kv,kw)
    allocate(k2(nvec(1),nvec(2),nvec(3)))
    k2   = ku*ku+kv*kv+kw*kw
    k2   = 0.5D0*k2
    
    select case (degree)
    case (0)
        allocate(precond(0,0,0))
    case (1)
        k2 = 1.0D0/(2.0D0*k2-2.0D0)
        allocate(precond(size(k2,2),size(k2,3),1))
        forall(ii=1:size(k2,2),jj=1:size(k2,3))
            precond(ii,jj,1) = minval(k2(:,ii,jj))
        end forall
    case (2)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 9.0D0+6.0D0*k2+4.0D0*k2**2.0D0
        precond = precond/(precond+8.0D0*k2**3.0D0)
    case (4)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 81.0D0+54.0D0*k2+36.0D0*k2**2.0D0+24.0D0*k2**3.0D0+16.0D0*k2**4.0D0
        precond = precond/(precond+32.0D0*k2**5.0D0)
    case (5)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 243.0D0+162.2D0*k2+108.0D0*k2**2.0D0+72.0D0*k2**3.0D0+48.0D0*k2**4.0D0+&
                32.0D0*k2**5.0D0
        precond = precond/(precond+64.0D0*k2**6.0D0)
    case (6)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 729.0D0+486.0D0*k2+324.0D0*k2**2.0D0+216.0D0*k2**3.0D0+144.0D0*k2**4.0D0+&
                96.0D0*k2**5.0D0+64.0D0*k2**6.0D0
        precond = precond/(precond+128.0D0*k2**7.0D0)
    case (7)
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 2187.0D0+1458.0D0*k2+972.0D0*k2**2.0D0+648.0D0*k2**3.0D0+432.0D0*k2**4+&
                288.0D0*k2**5.0D0+192.0D0*k2**6.0D0+128.0D0*k2**7.0D0
        precond = precond/(precond+256.0D0*k2**8.0D0)
    case default
        allocate(precond(nvec(1),nvec(2),nvec(3)))
        precond = 81.0D0+54.0D0*k2+36.0D0*k2**2.0D0+24.0D0*k2**3.0D0+16.0D0*k2**4.0D0
        precond = precond/(precond+32.0D0*k2**5.0D0)
    end select
    
    return
    end subroutine precondT
    end module precondT_module
