
!***********************************************************************
      !
      !
      !
!***********************************************************************

      module MPI_Reduce_sum_R1D_module
      contains
      subroutine MPI_Reduce_sum_R1D(vari,ranks,root,redvar)

      !include 'mpif.h'

      use MEX_ALLREDUCE_SUM_INTEG_module
      use MEX_REDUCE_SUM_REAL8_module

      include 'mpif.h'

      ! input variables
      integer              :: root
      real*8 , allocatable :: vari(:), redvar(:)
      integer, allocatable :: ranks(:)

      ! temporary variables
      integer :: mpirank, mpisize, ierr, rankn, ntmp, ii
      !integer :: sendinfo(16), recvinfo(16)
      integer :: nvar, grouproot
      !integer, allocatable :: grouproot(:)
      integer, allocatable :: sendinfo(:), recvinfo(:)
      real*8 , allocatable :: redvart(:), varitmp(:)
      ! output variables

      ! body of this function
      call MPI_COMM_RANK(MPI_COMM_WORLD,mpirank,ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD,mpisize,ierr)

      if (all(ranks /= mpirank)) then
              redvar = 0.0D0
              return
      end if

      rankn = size(ranks)

      ntmp  = 0
      do ii = 1,rankn,1
          if (ranks(ii) == root) then
                  ntmp = ntmp +1
          end if
      end do
      ntmp  = 0
      do ii = 1,rankn,1
          if (ranks(ii) == root) then
                  ntmp      = ntmp +1
                  grouproot = ii - 1
          end if
      end do

      allocate(sendinfo(16),recvinfo(16))
      sendinfo    = 0
      sendinfo(1) = 0
      sendinfo(2) = 0
      recvinfo    = 0

      call MEX_ALLREDUCE_SUM_INTEG(sendinfo,recvinfo,16,rankn,ranks)

      nvar = size(vari)

      if (nvar == 1) then
              allocate(redvart(2))
              allocate(varitmp(2))
              varitmp(1) = vari(1)
              varitmp(2) = 0.0D0
              call MEX_REDUCE_SUM_REAL8(varitmp,redvart,2,grouproot,rankn,ranks)
              redvar(1)  = redvart(1)
              deallocate(redvart,varitmp)
      else
              redvar = 0.0D0
              call MEX_REDUCE_SUM_REAL8(vari,redvar,nvar,grouproot,rankn,ranks)
      end if

      return
      end subroutine MPI_Reduce_sum_R1D
      end module MPI_Reduce_sum_R1D_module
