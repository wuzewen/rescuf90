! reshapeAndSortXandV.f90
    
!********************************************
    !
    !
    !
!********************************************
    
    module reshapeAndSortXandV_module
    contains
    subroutine reshapeAndSortXandV(X,Y,Xtmp,Ytmp)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: X(:), Y(:)
    
    ! temporary variables
    integer             :: nX, nY, ii, xloc(1), xlocl
    real*8              :: xmax
    real*8, allocatable :: XX(:)
    
    ! output variables
    real*8, allocatable :: Xtmp(:), Ytmp(:)
    
    ! body of this function
    nX = size(X)
    nY = size(Y)
    allocate(XX(nX))
    
    XX = X
    
    xmax = maxval(XX)
    do ii = 1,nX,1
        Xtmp(ii) = minval(XX)
        xloc     = minloc(XX)
        xlocl    = xloc(1)
        Ytmp(ii) = Y(xlocl)
        XX(xloc) = XX(xloc)+xmax
    end do
    
    return
    end subroutine reshapeAndSortXandV
    end module reshapeAndSortXandV_module
        
