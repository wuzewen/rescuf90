! readSummeryOfBasis.f90
!***************************************
    !
!***************************************
    
    module readSummeryOfBasis_module
    contains
    subroutine readSummeryOfBasis(ElementName,SOB)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use SummeryOfBas_module
    
    implicit none
    
    ! input variables
    character(len=3) :: ElementName
    
    ! temporary variables
    integer            :: ii
    character(len=8)   :: FileName
    character(len=100) :: nonsense
    ! output variables
    !type :: SummeryOfBas
    !    integer  :: SizeOfVlocal,    SizeOfVlocalrrData,    SizeOfVlocaldrData,    SizeOfVlocalvvData
    !    integer  :: SizeOfRlocal,    SizeOfRlocalrrData,    SizeOfRlocaldrData,    SizeOfRlocalrhoData
    !    integer  :: SizeOfVnl,       SizeOfVnlrrData,       SizeOfVnldrData,       SizeOfVnlvvData,                SizeOfVnlqqData,                  SizeOfVnlfqData,       SizeOfVnlqwData
    !    integer  :: SizeOfVna,       SizeOfVnarrData,       SizeOfVnadrData,       SizeOfVnavvData
    !    integer  :: SizeOfRna,       SizeOfRnarrData,       SizeOfRnadrData,       SizeOfRnarhoData
    !    integer  :: SizeOfRelPseudoP,SizeOfRelPseudoPrrData,SizeOfRelPseudoPdrData,SizeOfRelPseudoPvvData_screened,SizeOfRelPseudoPvvData_unscreened
    !    integer  :: SizeOfOrbitalSet,SizeOfOrbitalSetrrData,SizeOfOrbitalSetdrData,SizeOfOrbitalSetfrData,         SizeOfOrbitalSetqqData,           SizeOfOrbitalSetfqData,SizeOfOrbitalSetqwData
    !end type SummeryBas
    
    type(SummeryOfBas) :: SOB
    
    ! body of this function
    !write(*,*) "I'm here in readsummery of basis."
    fileName = trim(ElementName)//".bas"
    !fileName = 'C.bas'
    open(unit=10,file=fileName,status='old',access='sequential',action='read')
    read(10,*) nonsense   ! 001
    
    !write(*,*) nonsense
    
    
    read(10,*) nonsense   ! 002
    read(10,*) nonsense   ! 003
    read(10,*) nonsense   ! 004
    read(10,*) nonsense   ! 005
    read(10,*) nonsense   ! 006
    read(10,*) nonsense   ! 007
    read(10,*) nonsense   ! 008
    
    ! Vlocal
    read(10,*) SOB%Vloc   ! 009
    
    !write(*,*) SOB%SizeOfVlocal
    
    read(10,*) nonsense
    read(10,*) SOB%Vlocrr,SOB%Vlocdr,SOB%Vlocvv
    
    ! Rlocal
    read(10,*) nonsense
    read(10,*) SOB%Rloc   ! 013
    read(10,*) nonsense
    read(10,*) SOB%Rlocrr,SOB%Rlocdr,SOB%Rlocrho
    
    ! Vnl
    read(10,*) nonsense
    read(10,*) SOB%Vnl      ! 017
    read(10,*) nonsense
    read(10,*) SOB%Vnlrr,SOB%Vnldr,SOB%Vnlvv,SOB%Vnlqq,SOB%Vnlfq,SOB%Vnlqw
    
    ! Vna
    read(10,*) nonsense
    read(10,*) SOB%Vna      ! 021
    read(10,*) nonsense
    read(10,*) SOB%Vnarr,SOB%Vnadr,SOB%Vnavv
    
    ! Rna
    read(10,*) nonsense
    read(10,*) SOB%Rna      ! 025
    read(10,*) nonsense
    read(10,*) SOB%Rnarr,SOB%Rnadr,SOB%Rnarho
    
    ! RelPseudoP
    read(10,*) nonsense
    read(10,*) SOB%PseP  ! 029
    read(10,*) nonsense
    read(10,*) SOB%PsePrr,SOB%PsePdr,SOB%PsePvvS,SOB%PsePvvU
    
    ! OrbitalSet
    read(10,*) nonsense
    read(10,*) SOB%Orbit  ! 033
    read(10,*) nonsense
    allocate(SOB%orbi(SOB%Orbit))
    do ii = 1,SOB%Orbit,1
        read(10,*) SOB%orbi(ii)%rr,SOB%orbi(ii)%dr,SOB%orbi(ii)%fr, &
                   SOB%orbi(ii)%qq,SOB%orbi(ii)%fq,SOB%orbi(ii)%qw
    end do
    
    close(10)
    
    return
    end subroutine readSummeryOfBasis
    end module readSummeryOfBasis_module
    
