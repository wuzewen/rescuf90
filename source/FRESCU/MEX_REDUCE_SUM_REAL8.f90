! MEX_REDUCE_SUM_REAL8.f90

!**************************************************************************
      !
      !
      !
!**************************************************************************

      module MEX_REDUCE_SUM_REAL8_module
      contains
      subroutine MEX_REDUCE_SUM_REAL8(sendinfo,recvinfo,n,root,rankn,ranks)

      include 'mpif.h'

      ! input variables
      integer              :: n, rankn, root
      integer, allocatable :: ranks(:)
      real*8 , allocatable :: sendinfo(:), recvinfo(:)

      real*8 , allocatable :: test1(:), test2(:)
      ! temporary variables
      integer :: world_size, ierr, world_group, prime_group
      integer :: prime_comm

      ! output variables

      ! body of this function
      call MPI_COMM_SIZE(MPI_COMM_WORLD,world_size,ierr)

      !write(*,*) "world_size =", world_size
      !write(*,*) "I'm here in MEX_REDUCE_SUM_REAL8."
      if (rankn == world_size) then
              !allocate(test1(12),test2(12))
              !test1 = 0.0D0
              !test2 = 0.0D0
              !write(*,*) "size of send and recv.", size(sendinfo), size(recvinfo)
              !write(*,*)  n
              !call MPI_REDUCE(test1,test2,12,MPI_DOUBLE,MPI_SUM,root,MPI_COMM_WORLD,ierr)
              !write(*,*) "I'm here in MEX_REDUCE_SUM_REAL8. 1"
              call MPI_REDUCE(sendinfo,recvinfo,n,MPI_DOUBLE,MPI_SUM,root,MPI_COMM_WORLD,ierr)
      else
              !write(*,*) "I'm here in MEX_REDUCE_SUM_REAL8."
              call MPI_COMM_GROUP(MPI_COMM_WORLD,world_group,ierr)
              call MPI_GROUP_INCL(world_group,rankn,ranks,prime_group,ierr)
              call MPI_COMM_CREATE_GROUP(MPI_COMM_WORLD,prime_group,0,prime_comm)
              call MPI_REDUCE(sendinfo,recvinfo,n,MPI_DOUBLE,MPI_SUM,root,prime_comm,ierr)
              !write(*,*) "I'm here in MEX_REDUCE_SUM_REAL8. 2"
              call MPI_GROUP_FREE(world_group,ierr)
              call MPI_GROUP_FREE(prime_group,ierr)
              call MPI_COMM_FREE(prime_comm,ierr)
      end if

      return
      end subroutine MEX_REDUCE_SUM_REAL8
      end module MEX_REDUCE_SUM_REAL8_module
