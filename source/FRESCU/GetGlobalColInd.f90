! GetGlobalColInd.f90

!*************************************
      !
      !
      !
!******************************************

      module GetGlobalColIndMod
      contains
      subroutine GetGlobalColInd(mpirank,A,colind)

      implicit none

      n     = A%n
      nb    = A%nblock
      nprow = A%mproc
      npcol = A%nproc

      if (trim(A%order) .EQ. trim('C')) then
          mycol = floor(dble(mpirank)/dble(nprow))
      else
          mycol = mod(mpirank,npcol)
      end if

      forall(1:nb)
              colind(ii) = ii+mycol*nb
      end forall

      nbcol  = floor(dble(n)/dble(nb)/dble(npcol))
      istart = 1+mycol*nb+nbcol*nb*npcol
      iend   = min(nb+mycol*nb+nbcol*nb*npcol,n)
      forall(ii=1:iend-istart)
              tmp(ii) = ii+istart-1
      end forall
      call bsxfunPlus(colind,)
      colind = 

      return
      end subroutine GetGlobalColInd
      end module GetGlobalColIndMod
