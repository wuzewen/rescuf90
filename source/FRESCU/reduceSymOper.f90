! reduceSymOper.f90

!****************************************************************************************
      !
      !
      !
!************************************************************************************
      
      module reduceSymOperMod
      contains
      subroutine reduceSymOper(FRC,Acell,Av,kpt,spnnz)

      implicit none

      mpistat = FRC%mpi%status
      call find(Av,2,ind)
      oper = Acell(1)
      m = size(Acell%vata,1)
      n = size(Acell%vata,2)

      if (spnnz .GE. 0) then
          call spalloc(m,n,2*spnnz,poer%vata)
          do jj = 1,size(ind),1
              ii = ind(jj)
              oper%vata = oper%vata + Acell(ii)%vata*exp(2.0D0*im*pi*matmul(kpt,Av(ii,:))
          end do
      else
          allocate(oper%vata(m,n))
          oper%vata = 0.0D0
          do jj = 1,size(ind),1
              ii = ind(jj)
              oper%vata = oper%vata + Acell(ii)%vata*exp(2.0D0*im*pi*matmul(kpt,Av(ii,:))
          end do
      end if

      if (FRC%smi%status .AND. spnnz .LT. 0) then
          otmp = oper
          call smiZGEADD('c',1,otmp,1,oper,oper)
      else if (mpistat) then
          call ModBCDist(FRC,oper,oper%m,oper%n,oper%mproc,oper%nproc,Bcell)
          if (FRC%mpi%rank .EQ. 0) then
                  Bcell%vata = transpose(conjg(Bcell%vata))
          end if
          call ModBCDist(FRC,Bcell,oper%mblock,oper%nblock,oper%mproc,oper%nproc,Bcell)
          oper%vata = oper%vata + Bcell%vata
      else
          oper%vata = oper%vata + transpose(conjg(oper%vata))
      end if

      oper%vata = oper%vata + Acell()%vata

      return
      end subroutine reduceSymOper
      end module reduceSymOperMod
