! infoType.f90
    
!****************************
    !
    !
    !
!****************************
    
    module infoType_module
    
    type :: infoType
        logical              :: calcBSafterSCF
        character(len=20)    :: calculationType
        character(len=20)    :: mpiPath
        character(len=20)    :: outfile
        character(len=20)    :: rescuPath
        character(len=20)    :: savePath
        character(len=20)    :: saveDir
        character(len=20)    :: savePreFix
        character(len=20)    :: smiPath
    end type infoType
    
    end module infoType_module
