! fermiDirac.f90
    
!********************************************************************************
    !
    !
    !
!********************************************************************************
    
    module fermiDirac_module
    contains
    subroutine fermiDirac(W,EF,x,y)
    
    implicit none
    
    ! input variables
    real*8              :: W, EF
    real*8, allocatable :: x(:)
    
    ! temporary variables
    integer :: n, ii
    real*8  :: xtmp
    
    ! output variables
    real*8, allocatable :: y(:)
    
    ! body of this function
    n = size(x)
    allocate(y(n))
    do ii = 1,n,1
        xtmp  = (x(ii)-EF)/W
        y(ii) = 1.0D0/(exp(xtmp)+1.0D0)
    end do
    
    return
    end subroutine fermiDirac
    end module fermiDirac_module
