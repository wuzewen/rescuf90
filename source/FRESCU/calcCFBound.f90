! calcCFBound.f90
!**********************************************
    !
    ! This function is used to calculate the lower boundary and upper boudary
    ! for CFSI method which may be used to solve the KS equation within real
    ! space. 
    !
    ! Method may be used are as follows:
    !        Lanczos and another one.
    !
    ! If MPI is used, every process will have all the boudaray results.
    !
!**********************************************
    
    module calcCFBound_module
    contains
    subroutine calcCFBound(FRC,LB,UB)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use OptsType_module
    use updateVeff_module
    use distmat_allgather_1D_module
    use ind2sub_module
    use getkborb_module
    use GetKBorb_Sparse_module
    use genhamfun_module
    use lanczosUB_module
    use MPI_Bcast_variable_2D_module
    use MPI_Allreduce_sum_real8_2D_module

    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    logical :: mpistat
    integer :: mpirank, mpisize, iter, ispin, nspin, cn, nkpt, gridn, nham
    integer :: niter, ii, jj, kk, ss
    real*8  :: kpt(3)
    integer,allocatable :: ksp(:), ranks(:)
    real*8, allocatable :: kdir(:,:), LB(:,:), UB(:,:), ham(:,:)
    real*8, allocatable :: hamu(:,:), hamd(:,:), vloc(:)
    real*8, allocatable :: LBtmp(:,:), UBtmp(:,:)
    type(Opts_Type) :: opts
    type(VataMN_1D) :: veff
    type(RealVata_3D) :: ksnrg
    
    ! output variables
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    iter    = FRC%scloop
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    cn      = product(FRC%domain%cgridn)
    !kdir    = FRC%kpoint%ikdirect
    !nkpt    = size(kdir,1)
    nkpt    = size(FRC%kpoint%ikdirect,1)
    allocate(kdir(nkpt,3))
    kdir    = FRC%kpoint%ikdirect
    
    call updateVeff(FRC,FRC%potential%veffin(1),FRC%potential%vps,.FALSE.,veff)
    !write(*,*) "veff in calcCFbound"
    !write(*,*)  veff%vata
    !allocate(vloc(size(veff%vata)))
    !write(*,*) 'FRC%potential%vps in bound.', FRC%potential%vps%m, FRC%potential%vps%n,FRC%potential%vps%mblock,FRC%potential%vps%nblock, FRC%potential%vps%mproc, FRC%potential%vps%nproc
    !write(*,*) 'FRC%potential%veffin(1) in bound.', FRC%potential%veffin(1)%m, FRC%potential%veffin(1)%n, FRC%potential%veffin(1)%mblock,FRC%potential%veffin(1)%nblock, FRC%potential%veffin(1)%mproc, FRC%potential%veffin(1)%nproc
    !write(*,*) 'veff in bound.', veff%m, veff%n, veff%mblock, veff%nblock, veff%mproc, veff%nproc
    call distmat_allgather_1D(FRC,veff,vloc)
    FRC%potential%vloc(:,1) = vloc
    gridn = veff%m
    
    !write(*,*) 'vloc'
    !write(*,*)  vloc
    !if (vtauStatus) then
    !    call distmat_allgether(FRC,FRC%potential%vtau,FRC%potential%vtauloc)
    !end if
    allocate(ksnrg%vata(size(FRC%energy%ksnrg%vata,1),size(FRC%energy%ksnrg%vata,2),size(FRC%energy%ksnrg%vata,3)))
    ksnrg = FRC%energy%ksnrg
    !LB    = maxval(ksnrg%vata,1)
    allocate(LB(nkpt,nspin))
    forall(ii=1:nkpt,jj=1:nspin)
            LB(ii,jj) = maxval(ksnrg%vata(:,ii,jj))
    end forall

    !write(*,*) "in calcCFbound, LB = "
    !write(*,*)  LB
    !call permute(LB,(/2,3,1/),LB)
    !call squeeze(LB,LB)
    !write(*,*) 'ksnrg%vata'
    !write(*,*)  ksnrg%vata
    
    allocate(UB(nkpt,nspin))
    niter  = FRC%eigensolver%ubmaxit
    allocate(ksp(2))
    ksp(1) = nkpt
    ksp(2) = nspin
    !write(*,*) 'mpirank+1 =', mpirank+1
    !write(*,*) 'mpisize   =', mpisize
    !write(*,*) 'nkpt*nspin=', nkpt*nspin
    do ii = mpirank+1,nkpt*nspin,mpisize
        !write(*,*) 'in calcbound, ii =', ii
        !write(*,*) "I'm here before ind2sub"
        call ind2sub(ksp,ii,kk,ss)
        !write(*,*) "I'm here after ind2sub"
        kpt = kdir(kk,:)
        !deallocate(FRC%potential%vnl%KBorb)
        if (FRC%potential%vnlScheme) then
                if (FRC%potential%vnl%KBorbIsSparse) then
                        call GetKBorb_Sparse(FRC,kpt)
                else
                        call GetKBorb(FRC,kpt)
                end if
        end if
        !write(*,*) 'vnl'
        !do jj = 1,FRC%potential%vnl%sparseKBorb%N_nonzero,1
        !write(*,*) FRC%potential%vnl%sparseKBorb%Index1(jj), FRC%potential%vnl%sparseKBorb%Index2(jj),FRC%potential%vnl%sparseKBorb%ValueN(jj)
        !end do
        if (ispin == 1 .or. ispin == 2) then
            !call GenHamFun(FRC,kpt,ss,.false.,X,ham,hamu,hamd)
            nham = cn
        else if (ispin == 4) then
            !call GenHamFun(FRC,kpt,ss,.false.,X,ham,hamu,hamd)
            ! ham = xxx
            nham = 2*cn
        end if
        !write(*,*) "FRC%eigensolver%ubmethod ="
        !write(*,*)  FRC%eigensolver%ubmethod
        if (trim(FRC%eigensolver%ubmethod) == trim("lanczos")) then
            call LanczosUB(FRC,kpt,ispin,.FALSE.,cn,niter,UB(kk,ss))
        else
            write(*,*) "Error in CalcCFBound.f90. ubmethod should be lanczos."
            !opts%issym  = 1
            !opts%isreal = any(kpt /= 0)
            !opts%tol    = 0.1D0
            !opts%maxit  = niter
            !sigma       = "lr"
            !if (.not. any(kpt /= 0)) then
            !    sigma = "la"
            !end if
            !call eigs(ham,gridn,1,sigma,opts,V,UB(kk,ss),flag)
            !if (flag) then
            !    tmp = ham(V)
            !    UB(kk,ss) = matmul(transpose(V),tmp)
            !    call norm(tmp-matmul(V,UB(kk,ss)),tmp2)
            !    UB(kk,ss) = UB(kk,ss) + tmp2
            !else
            !    UB(kk,ss) = UB(kk,ss)+0.1D0
            !end if
        end if
    end do
    
    !write(*,*) "in calcCFBound, UB ="
    !write(*,*)  UB
    if (mpistat) then
            !write(*,*) 'in bound, marker1.'
            allocate(LBtmp(size(LB,1),size(LB,2)))
            allocate(UBtmp(size(UB,1),size(UB,2)))
            LBtmp = LB
            UBtmp = UB
            allocate(ranks(mpisize))
            forall(ii=1:mpisize)
                    ranks(ii) = ii-1
            end forall
            !write(*,*) 'in bound, marker2.'
            call MPI_Bcast_variable_2D(LBtmp,0,ranks,LB)
            !write(*,*) 'in bound, marker3.'
            call MPI_Allreduce_sum_real8_2D(UBtmp,ranks,UB)
            !write(*,*) 'in bound, marker4.'
    end if

    !stop
    
    return
    end subroutine calcCFBound
    end module calcCFBound_module
    
    
