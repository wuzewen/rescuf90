! DOStype.f90
    
!****************************
    !
    !
    !
!*****************************
    
    module DOStype_module
    
    use LDOStype_module
    
    type :: DOStype
        type(LDOStype)       :: LDOS
        integer, allocatable :: projL(:)
        integer, allocatable :: projM(:)
        integer, allocatable :: projZ(:)
        integer, allocatable :: projXYZ(:,:)
        integer, allocatable :: projAtom(:)
        integer, allocatable :: projSpecies(:)
        real*8               :: DOSrange(2)
        real*8               :: resolution
        logical              :: status
        logical              :: pdos
    end type DOStype
    
    end module DOStype_module