! genDenseVnlOp.f90

!*****************************************************************************
      !
      !
      !
!*****************************************************************************

      module genDenseVnlOp_module
      contains
      subroutine genDenseVnlOp(FRC,kpt,Psi,vnlPsiout)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use IntegerVata_1D_module
      use RealVata_1D_module
      use ComplexVata_2D_module
      use ndgrid_module
      use bsxfunTimes_CMPLX_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      real*8                          :: kpt(3)
      complex*16, allocatable         :: Psi(:,:)

      ! temporary variables
      integer          :: gridn(3), nc, AoMax
      integer          :: mtmp, ntmp, ncell
      integer          :: NG1IND, NG2IND, NG3IND
      integer          :: n1, n2, n3
      integer          :: ii, jj, ll, mm, nn
      integer          :: Modtmp, psin
      integer          :: m, n, k, lda, ldb, ldc
      real*8           :: avec(3,3), dr, pi
      complex*16       :: alpha, beta, imnu
      character(len=1) :: transa, transb

      integer, allocatable    :: aorb(:)
      integer, allocatable    :: g1tmp(:), g2tmp(:), g3tmp(:)
      real*8 , allocatable    :: kbedr(:) 
      real*8 , allocatable    :: g1Indtmp(:), g2Indtmp(:), g3Indtmp(:)
      real*8 , allocatable    :: gg1(:,:,:), gg2(:,:,:), gg3(:,:,:)
      complex*16, allocatable :: psitmp(:,:,:,:), vnlpsi(:,:,:,:)
      complex*16, allocatable :: kbDii(:,:)
      complex*16, allocatable :: eikr(:,:,:), eikrc(:,:), kbetmp(:,:)
      complex*16, allocatable :: ptmptmp(:,:,:,:)
      complex*16, allocatable :: kbpt(:,:), kbpt2(:,:), kbpt3(:,:)
      complex*16, allocatable :: ptmpii(:,:,:,:)

      type(IntegerVata_1D), allocatable :: g1Ind(:), g2Ind(:), g3Ind(:)
      type(IntegerVata_1D), allocatable :: g1Mod(:), g2Mod(:), g3Mod(:)
      type(RealVata_1D),    allocatable :: kbecell(:)
      type(ComplexVata_2D), allocatable :: kbDcell(:)
      type(ComplexVata_2D), allocatable :: ptmp(:)

      ! output variables
      complex*16, allocatable :: vnlPsiout(:,:)

      ! body of this function
      if (FRC%interpolation%vnl) then
              gridn = FRC%domain%fgridn
      else
              gridn = FRC%domain%cgridn
      end if
      pi      = 3.1415926535897932385D0
      imnu    = (0.0D0,1.0D0)
      avec    = FRC%domain%latvec
      dr      = avec(1,1)*avec(2,2)*avec(3,3) &
               +avec(1,2)*avec(2,3)*avec(3,1) &
               +avec(1,3)*avec(2,1)*avec(3,2) &
               -avec(1,3)*avec(2,2)*avec(3,1) &
               -avec(1,1)*avec(2,3)*avec(3,2) &
               -avec(1,2)*avec(2,1)*avec(3,3)
      nc      = product(gridn)
      dr      = dr/dble(nc)
      allocate(kbedr(size(FRC%potential%vnl%KBEnergy)))
      kbedr   = FRC%potential%vnl%KBEnergy*dr
      allocate(aorb(size(FRC%potential%vnl%Aorb)))
      aorb    = Int(FRC%potential%vnl%Aorb)
      AoMax   = maxval(aorb)
      allocate(kbecell(Aomax))
      mtmp    = 0
      do ii = 1,Aomax,1
          ntmp = count(aorb==ii)
          if (ntmp /= 0) then
                  allocate(kbecell(ii)%vata(ntmp))
                  forall(jj=1:ntmp)
                          kbecell(ii)%vata(jj) = kbedr(mtmp+jj)
                  end forall
                  mtmp = mtmp + ntmp
          end if
      end do

      ncell = size(FRC%potential%vnl%kbDcell)

      !********************** set g*Ind ***********************!
      NG1IND  = size(FRC%potential%vnl%g1Ind)
      NG2IND  = size(FRC%potential%vnl%g2Ind)
      NG3IND  = size(FRC%potential%vnl%g3Ind)
      allocate(g1Ind(NG1IND))
      allocate(g2Ind(NG2IND))
      allocate(g3Ind(NG3IND))
      allocate(g1Mod(NG1IND))
      allocate(g2Mod(NG2IND))
      allocate(g3Mod(NG3IND))
      do ii = 1,NG1IND,1
          ntmp = size(FRC%potential%vnl%g1Ind(ii)%vata)
          allocate(g1Ind(ii)%vata(ntmp))
          allocate(g1Mod(ii)%vata(ntmp))
      end do
      do ii = 1,NG2IND,1
          ntmp = size(FRC%potential%vnl%g2Ind(ii)%vata)
          allocate(g2Ind(ii)%vata(ntmp))
          allocate(g2Mod(ii)%vata(ntmp))
      end do
      do ii = 1,NG3IND,1
          ntmp = size(FRC%potential%vnl%g3Ind(ii)%vata)
          allocate(g3Ind(ii)%vata(ntmp))
          allocate(g3Mod(ii)%vata(ntmp))
      end do
      g1Ind   = FRC%potential%vnl%g1Ind
      g2Ind   = FRC%potential%vnl%g2Ind
      g3Ind   = FRC%potential%vnl%g3Ind
      do ii = 1,NG1IND,1
          ntmp = size(FRC%potential%vnl%g1Ind(ii)%vata)
          do jj = 1,ntmp,1
              ModTmp = mod(g1Ind(ii)%vata(jj),gridn(1))
              if (ModTmp <= 0) then
                      g1Mod(ii)%vata(jj) = gridn(1)+ModTmp
              else
                      g1Mod(ii)%vata(jj) = ModTmp
              end if
          end do
      end do
      do ii = 1,NG2IND,1
          ntmp = size(FRC%potential%vnl%g2Ind(ii)%vata)
          do jj = 1,ntmp,1
              ModTmp = mod(g2Ind(ii)%vata(jj),gridn(2))
              if (ModTmp <= 0) then
                      g2Mod(ii)%vata(jj) = gridn(2)+ModTmp
              else
                      g2Mod(ii)%vata(jj) = ModTmp
              end if
          end do
      end do
      do ii = 1,NG3IND,1
          ntmp = size(FRC%potential%vnl%g3Ind(ii)%vata)
          do jj = 1,ntmp,1
              ModTmp = mod(g3Ind(ii)%vata(jj),gridn(3))
              if (ModTmp <= 0) then
                      g3Mod(ii)%vata(jj) = gridn(3)+ModTmp
              else
                      g3Mod(ii)%vata(jj) = ModTmp
              end if
          end do
      end do
      !********************************************************!

      !write(*,*) "Mods"
      !do  ii = 1,NG1IND,1
      !write(*,*) g1mod(ii)%vata
      !end do
      !do  ii = 1,NG2IND,1
      !write(*,*) g2mod(ii)%vata
      !end do
      !do  ii = 1,NG3IND,1
      !write(*,*) g3mod(ii)%vata
      !end do
      psin    = size(psi,2)
      allocate(psitmp(gridn(1),gridn(2),gridn(3),psin))
      psitmp  = reshape(Psi,(/gridn(1),gridn(2),gridn(3),psin/))
      allocate(vnlpsi(gridn(1),gridn(2),gridn(3),psin))
      vnlpsi  = (0.0D0,0.0D0)
      allocate(ptmp(ncell))

      do ii = 1,ncell,1
          n1    = size(g1Mod(ii)%vata)
          n2    = size(g2Mod(ii)%vata)
          n3    = size(g3Mod(ii)%vata)
          allocate(g1tmp(n1))
          allocate(g2tmp(n2))
          allocate(g3tmp(n3))
          g1tmp = g1Mod(ii)%vata
          g2tmp = g2Mod(ii)%vata
          g3tmp = g3Mod(ii)%vata

          allocate(kbDii(size(FRC%potential%vnl%kbDcell(ii)%vata,1),size(FRC%potential%vnl%kbDcell(ii)%vata,2)))
          kbDii = FRC%potential%vnl%kbDcell(ii)%vata

          if (any(kpt /= 0.0D0)) then
                  allocate(g1Indtmp(n1))
                  allocate(g2Indtmp(n2))
                  allocate(g3Indtmp(n3))
                  g1Indtmp = dble(g1Ind(ii)%vata)/dble(gridn(1))
                  g2Indtmp = dble(g2Ind(ii)%vata)/dble(gridn(2))
                  g3Indtmp = dble(g3Ind(ii)%vata)/dble(gridn(3))
                  allocate(gg1(n1,n2,n3))
                  allocate(gg2(n1,n2,n3))
                  allocate(gg3(n1,n2,n3))
                  call ndgrid(g1Indtmp,g2Indtmp,g3Indtmp,gg1,gg2,gg3)
                  allocate(eikr(n1,n2,n3))
                  eikr     = dcmplx(gg1*kpt(1)+gg2*kpt(2)+gg3*kpt(3))
                  eikr     = exp(-2.0D0*imnu*pi*eikr)
                  allocate(eikrc(n1*n2*n3,1))
                  eikrc    = reshape(eikr,(/n1*n2*n3,1/))
                  call bsxfunTimes_CMPLX(eikrc,kbDii,kbDii)
                  deallocate(g1Indtmp,g2Indtmp,g3Indtmp)
                  deallocate(gg1,gg2,gg3)
                  deallocate(eikr,eikrc)
          end if

          !n1 = size(g1tmp)
          !n2 = size(g2tmp)
          !n3 = size(g3tmp)
          allocate(ptmp(ii)%vata(n1*n2*n3,psin))
          allocate(ptmptmp(n1,n2,n3,psin))
          forall(jj=1:n1,ll=1:n2,mm=1:n3,nn=1:psin)
                  ptmptmp(jj,ll,mm,nn) = psitmp(g1tmp(jj),g2tmp(ll),g3tmp(mm),nn)
          end forall
          ptmp(ii)%vata = reshape(ptmptmp,(/n1*n2*n3,psin/))
          !knDiitmp      = conjg(transpose(kbDii))
          transa        = 'C'
          transb        = 'N'
          m             =  size(kbDii,2)
          n             =  psin
          k             =  n1*n2*n3
          lda           =  k
          ldb           =  k
          ldc           =  m
          alpha         = (1.0D0,0.0D0)
          beta          = (0.0D0,0.0D0)
          allocate(kbpt(m,n))
          allocate(kbpt2(m,n))
          kbpt2         = (0.0D0,0.0D0)
          !write(*,*) "I'm here 1"
          call ZGEMM(transa,transb,m,n,k,alpha,kbDii,lda,ptmp(ii)%vata,ldb,beta,kbpt,ldc)
          allocate(kbetmp(size(kbecell(ii)%vata),1))
          kbetmp(:,1)   = dcmplx(kbecell(ii)%vata)
          call bsxfunTimes_CMPLX(kbetmp,kbpt,kbpt2)
          transa        = 'N'
          transb        = 'N'
          m             =  size(kbDii,1)
          n             =  n
          k             =  size(kbDii,2)
          lda           =  m
          ldb           =  k
          ldc           =  m
          alpha         = (1.0D0,0.0D0)
          beta          = (0.0D0,0.0D0)
          allocate(kbpt3(m,n))
          kbpt3         = (0.0D0,0.0D0)
          !write(*,*) "I'm here 2"
          call ZGEMM(transa,transb,m,n,k,alpha,kbDii,lda,kbpt2,ldb,beta,kbpt3,ldc)
          allocate(ptmpii(n1,n2,n3,psin))
          ptmpii        = reshape(kbpt3,(/n1,n2,n3,psin/))
          forall(jj=1:n1,ll=1:n2,mm=1:n3)
                  vnlPsi(g1tmp(jj),g2tmp(ll),g3tmp(mm),:) = &
                          vnlPsi(g1tmp(jj),g2tmp(ll),g3tmp(mm),:) + ptmpii(jj,ll,mm,:)
          end forall
          deallocate(g1tmp,g2tmp,g3tmp)
          deallocate(kbDii)
          deallocate(ptmptmp,kbpt,kbpt2,kbpt3,ptmpii,kbetmp)
          !stop
      end do

      !stop
      !allocate(vnlPsiout(gridn(1)*gridn(2)*gridn(3),psin))
      vnlPsiout = reshape(vnlPsi,(/gridn(1)*gridn(2)*gridn(3),psin/))

      !write(*,*) "size", gridn(1)*gridn(2)*gridn(3),psin
      !do jj = 1,psin,1
      !do ii = 1,gridn(1)*gridn(2)*gridn(3),1
      !write(*,*) vnlPsiout(ii,jj)
      !end do
      !end do
      !stop

      return
      end subroutine genDenseVnlOp
      end module genDenseVnlOp_module


