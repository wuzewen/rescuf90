! sph2real.f90

!********************************************************************
      !
      !
      !
!*******************************************************************

      module sph2realMod
      contains
      subroutine sph2real(A,Morb,cha)

      implicit none

      call sph2realMat(Morb,U)
      select case (cha)
      case ('l')
              A = matmul(transpose(conjg(U)),A)
      case ('r')
              A = matmul(A,U)
      case default
              A = matmul(matmul(transpose(conjg(U)),A),U)
      end select

      return
      end subroutine sph2real
      end module sph2realMod

