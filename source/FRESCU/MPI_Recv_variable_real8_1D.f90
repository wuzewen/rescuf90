! MPI_Recv_variable_real8_1D.f90

!*****************************************************************
      !
      !
      !
!*****************************************************************

      module MPI_Recv_variable_real8_1D_module
      contains
      subroutine MPI_Recv_variable_real8_1D(sor,tag,vari)

      include 'mpif.h'

      !implicit none

      ! input variables
      integer :: sor
      integer :: tag

      ! temporary variables
      integer :: varinfo(16), issp, isrl, bloc, nonzero, vardim, varsize
      integer :: varn, kk, ntmp, ii, ierr

      integer,allocatable :: inde(:)
      real*8, allocatable :: buf(:), buftmp(:)
      ! output variables
      real*8, allocatable :: vari(:)

      ! body of this function
      varinfo = 0
      call MPI_RECV(varinfo,16,MPI_INT,sor,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
      issp    = varinfo(1)
      isrl    = varinfo(2)
      bloc    = varinfo(3)
      nonzero = varinfo(4)
      vardim  = varinfo(5)
      varsize = varinfo(6)

      tag     = tag+1
      !allocate(vari(varsize))
      varn    = varsize

      !kk      = 0
      !do while (kk < varn)
          !ntmp = min(kk+bloc,varn)
          !forall(ii=1:ntmp-kk)
          !        inde(ii) = kk+ii
          !end forall
          !if (ntmp == 1) then
          !        allocate(buftmp(2))
          !        buftmp = 0.0D0
          !        call MPI_RECV(buftmp,2,MPI_DOUBLE,sor,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE) 
          !        tag = tag+1
          !        allocate(buf(1))
          !        buf(1) = buftmp(1)
          !else
                  !allocate(buf(ntmp))
                  call MPI_RECV(vari,varn,MPI_DOUBLE,sor,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
                  !tag = tag+1
          !end if

          !forall(ii=1:ntmp)
          !        vari(inde(ii)) = buf(ii)
          !end forall
      !end do

      !kk = kk + bloc

      return
      end subroutine MPI_Recv_variable_real8_1D
      end module MPI_Recv_variable_real8_1D_module

