! rescu_bs.f90

      module rescu_bs_module
      subroutine rescu_bs(FRC)

      implicit none

      ! input variables

      ! temporary variables

      ! output variables

      ! body of this function
      FRC%gpu%status = 0
      FRC%scfloop    = 1
      mpirank        = FRC%mpi%rank 
      if (mpirank == 0) then
              call displayInfos(FRC)
      end if
      call GenHamiltonian(FRC)
      if (mpirank == 0) then
              !call printBegin()
      end if
      if (FRC%functional%hybrid) then
              !call LoadExxData(FRC,'FockMatrix')
      end if
      if (FRC%CAO%status) then
              FRC%LCAO%dynSubRed = .TRUE.
              FRC%LCAO%veffRed   = .TRUE.
              call ks_main_lcao_nonscf(FRC)
      else
              call ks_main_real_nonscf(FRC)
      end if
      if (mpirank == 0) then
              call printFinish(outfile)
      end if
      call parseBandInfo(FRC)
      if (mpirank == 0) then
              call writeBandStructure(FRC%band)
      end if
      call saveMatRcal(FRC)

      return
      end subroutine rescu_bs
      end module rescu_bs_module
