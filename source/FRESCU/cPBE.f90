! cPBE.f90

! ***********************************************************************
      ! This subroutine will provide correlation potential
      ! with GGA_PBE method.
      ! All the inputs and outputs are describe as follows:
      ! rho     : input , two dimentional array (ngrid * ?), double precision
      ! sigma   : input , two dimentional array (ngrid * 6), double precision
      ! Ec1     : output, one dimentiinal array (ngrid), double precision
      ! DEcDP1  : output, 
      ! DEcDN1  : output,
      ! DEcDGP1 : output,
      ! DEcDGN1 : output,
! **********************************************************************

      module cPBE_module
      contains
      subroutine cPBE(rho,sigma,EC1,DEcDP1,DEcDN1,DEcDGP1,DEcDGN1)

      use XC_LDA_C_PW_module

      implicit none

      ! input variables
      real*8, allocatable :: rho(:,:), sigma(:,:)

      ! temporary variables
      integer :: npts, ntmp, nara, ii
      real*8  :: DENMIN, OneThird, TwoThird, SevenThird, ConstT2
      real*8  :: ConstPhi, Beta, GammaC, One, EPS
      real*8, allocatable :: RhoSP(:), GRhoSPX(:), GRhoSPY(:), GRhoSPZ(:)
      real*8, allocatable :: RhoSN(:), GRhoSNX(:), GRhoSNY(:), GRhoSNZ(:)
      real*8, allocatable :: PNtmp(:,:), Ec(:), DEcDP(:), DEcDP2(:,:)
      real*8, allocatable :: DEcDN(:), RhoTotal(:), Phi(:)
      real*8, allocatable :: Phi2(:), FT2(:), GX(:), GY(:), GZ(:), T2(:)
      real*8, allocatable :: GP3(:), A(:), AT2(:), TA1(:), TA2(:), H(:)
      real*8, allocatable :: DHDF1(:), DHDT2(:), DHDA(:), DHDPhi(:)
      real*8, allocatable :: RhoDHDGD(:), DENS(:), DEcDGP(:,:)
      real*8, allocatable :: DEcDGPX(:), DEcDGPY(:), DEcDGPZ(:)
      real*8, allocatable :: DEcDGNX(:), DEcDGNY(:), DEcDGNZ(:)
      real*8, allocatable :: rhotmp(:,:), sigmatmp(:,:), DEcDGN(:,:)

      ! output variables
      real*8, allocatable :: EC1(:), DEcDP1(:), DEcDN1(:), DEcDGP1(:,:)
      real*8, allocatable :: DEcDGN1(:,:)

      ! body of this function
      EPS    = 2.220446049250313D-16
      DENMIN = 50.0D0*2.220446049250313D-16
      npts   = size(rho,1)
      nara   = size(rho,2)
      allocate(EC1(npts))
      allocate(DEcDP1(npts))
      allocate(DEcDN1(npts))
      allocate(DEcDGP1(npts,3))
      allocate(DEcDGN1(npts,3))
      EC1     = 0.0D0
      DEcDP1  = 0.0D0
      DEcDN1  = 0.0D0
      DEcDGP1 = 0.0D0
      DEcDGN1 = 0.0D0

      !write(*,*) "npts = ", npts

      allocate(DENS(npts))
      DENS    = sum(rho,2)
      ntmp    = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                  ntmp = ntmp+1
          end if
      end do
      allocate(rhotmp(ntmp,nara))
      allocate(sigmatmp(ntmp,size(sigma,2)))
      ntmp = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                  ntmp             = ntmp+1
                  rhotmp(ntmp,:)   = rho(ii,:)
                  sigmatmp(ntmp,:) = sigma(ii,:)
          end if
      end do

      OneThird   = 0.333333333333333333D0
      TwoThird   = 0.666666666666666667D0
      SevenThird = 2.333333333333333333D0
      ConstT2    = 0.0634682060977037D0
      ConstPhi   = 0.793700525984100D0
      Beta       = 2.14614079435349144D0
      GammaC     = 0.031090690869654895D0

      !write(*,*) "after constants."
      allocate(RhoSP(ntmp))
      allocate(GRhoSPX(ntmp))
      allocate(GRhoSPY(ntmp))
      allocate(GRhoSPZ(ntmp))
      allocate(RhoSN(ntmp))
      allocate(GRhoSNX(ntmp))
      allocate(GRhoSNY(ntmp))
      allocate(GRhoSNZ(ntmp))

      RhoSP   = rhotmp(:,1)
      do ii = 1,ntmp,1
          if (RhoSP(ii) < EPS) then
                  RhoSP(ii) = EPS
          end if
      end do
      GRhoSPX = sigmatmp(:,1)
      GRhoSPY = sigmatmp(:,2)
      GRhoSPZ = sigmatmp(:,3)
      RhoSN   = rhotmp(:,2)
      do ii = 1,ntmp,1
          if (RhoSN(ii) < EPS) then
                  RhoSN(ii) = EPS
          end if
      end do
      GRhoSNX = sigmatmp(:,4)
      GRhoSNY = sigmatmp(:,5)
      GRhoSNZ = sigmatmp(:,6)

      allocate(PNtmp(ntmp,2))
      PNtmp(:,1)  =  RhoSP
      PNtmp(:,2)  =  RhoSN
      !write(*,*) "before call XC_LDA_C_PW."
      call XC_LDA_C_PW(2,PNtmp,EC,DEcDP2)


      !write(*,*) "EC ="
      !write(*,*)  EC
      !write(*,*) "DEcDP2 ="
      !write(*,*)  DEcDP2


      allocate(DEcDN(ntmp))
      allocate(DEcDP(ntmp))
      DEcDN       =  DEcDP2(:,2)
      DEcDP       =  DEcDP2(:,1)
      !write(*,*) "after call XC_LDA_C_PW."

      allocate(RhoTotal(ntmp))
      allocate(Phi(ntmp))
      allocate(Phi2(ntmp))
      allocate(FT2(ntmp))
      allocate(GX(ntmp))
      allocate(GY(ntmp))
      allocate(GZ(ntmp))
      allocate(T2(ntmp))
      allocate(GP3(ntmp))
      RhoTotal    =  RhoSP+RhoSN
      Phi         =  ConstPhi*((RhoSP/RhoTotal)**(TwoThird)+(RhoSN/RhoTotal)**(TwoThird))
      Phi2        =  Phi*Phi
      FT2         =  ConstT2*((RhoTotal)**(-SevenThird))/Phi2

      GX = GRhoSPX + GRhoSNX
      GY = GRhoSPY + GRhoSNY
      GZ = GRhoSPZ + GRhoSNZ
 
      T2 = (GX*GX + GY*GY + GZ*GZ)*FT2
      GP3 = GammaC*Phi*Phi2
      !write(*,*) "after GP3, in cPBE"
      !write(*,*) "T2 ="
      !write(*,*)  T2
      !write(*,*) "GP3 = "
      !write(*,*)  GP3

      allocate(A(ntmp))
      allocate(AT2(ntmp))
      allocate(TA1(ntmp))
      allocate(TA2(ntmp))
      allocate(H(ntmp))
      allocate(DHDF1(ntmp))
      allocate(DHDT2(ntmp))
      allocate(DHDA(ntmp))
      allocate(DHDPhi(ntmp))
      !write(*,*) "before A, in cPBE"
      A = Beta/(exp(-EC/GP3) - 1.0D0)
      AT2 = A*T2
      TA1 = T2*(1.0D0 + AT2)
      TA2 = 1.0D0 + A*TA1
      H = GP3*log(1.0D0 + Beta*TA1/TA2)
      DHDF1 = Beta*GP3/(TA2 + Beta*TA1)/TA2
      DHDT2 = DHDF1*(1.0D0 + 2.0D0*AT2)
      DHDA = -DHDF1*AT2*AT2*AT2*(2.0D0 + AT2)*exp(-Ec/GP3)/Beta/GP3
      DHDPhi = 2.0D0*(H - DHDA*Ec - TwoThird*DHDT2*T2)/Phi

      !allocate(EC(ntmp))
      !allocate(DEcDP(ntmp))
      !allocate(DEcDN(ntmp))
      allocate(RhoDHDGD(ntmp))
      allocate(DEcDGPX(ntmp))
      allocate(DEcDGPY(ntmp))
      allocate(DEcDGPZ(ntmp))
      allocate(DEcDGP(ntmp,3))
      allocate(DEcDGN(ntmp,3))
      !write(*,*) "before EC, in cPBE"
      Ec = Ec + H
      DEcDP = DEcDP + H
      DEcDN = DEcDN + H
      DEcDP = DEcDP + DHDA*(DEcDP - EC) - DHDT2*T2*SevenThird  &
            + DHDPhi*(ConstPhi*(RhoTotal/RhoSP)**(OneThird) - Phi)
      DEcDN = DEcDN + DHDA*(DEcDN - EC) - DHDT2*T2*SevenThird  &
            + DHDPhi*(ConstPhi*(RhoTotal/RhoSN)**(OneThird) - Phi)
      RhoDHDGD = 2.0D0*RhoTotal*DHDT2*FT2
      DEcDGPX = RhoDHDGD*GX
      !DEcDGNX = DEcDGPX       !RhoDHDGD*GX
      DEcDGPY = RhoDHDGD*GY
      !DEcDGNY = DEcDGPY       !RhoDHDGD*GY
      DEcDGPZ = RhoDHDGD*GZ
      !DEcDGNZ = DEcDGPZ       !RhoDHDGD*GZ;
      DEcDGP(:,1) = DEcDGPX
      DEcDGP(:,2) = DEcDGPY
      DEcDGP(:,3) = DEcDGPZ
      DEcDGN      = DEcDGP    ![DEcDGNX; DEcDGNY; DEcDGNZ];

      !Ec1(index) = Ec;
      !DEcDP1(:,index) = DEcDP;
      !DEcDN1(:,index) = DEcDN;
      !DEcDGP1(:,index) = DEcDGP;
      !DEcDGN1(:,index) = DEcDGN

      ntmp = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                  ntmp          = ntmp+1
                  Ec1(ii)       = Ec(ntmp)
                  DEcDP1(ii)    = DEcDP(ntmp)
                  DEcDN1(ii)    = DEcDN(ntmp)
                  DEcDGP1(ii,:) = DEcDGP(ntmp,:)
                  DEcDGN1(ii,:) = DEcDGN(ntmp,:)
          end if
      end do

      return
      end subroutine cPBE
      end module cPBE_module


