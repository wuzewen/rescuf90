! calcVnlMatDOI.f90

!***************************************************************************
      !
      !
      !
!****************************************************************************

      module calcVnlMatSOIMod
      contains
      subroutine calcVnlMatSOI(FRC)

      implicit none

      mpirank  = FRC%mpi%rank
      mpisize  = FRC%mpi%mpisize
      smimb    = FRC%smi%mb
      sminb    = FRC%smi%nb
      smimp    = FRC%smi%mp
      sminp    = FRC%smi%np
      kbSOcell = FRC%LCAOkbSOtv
      tkbSO    = FRC%LCAO%kbSOtv

      do ii = 1,2,1
          call sprucecell(FRC,kbSOcell(ii),idel)
          delet
      end do

      nnao = size(FRC%LCAO%parameters%Lorb)
      allocate()
      kbe  = FRC%potential%vnlSO%Eorb
      J    = FRC%potential%vnlSO%Jorb
      L    = FRC%potential%vnlSO%Morb
      M    = FRC%potential%vnlSO%Morb
      O    = FRC%potential%vnlSO%Oorb
      evec = FRC%potential%vnlSO%evec

      call calcKBSO(kbe,J,L,M,O,evec,kbso)

      call initDistArray(nnao,nnao,smimb,sminb,smimp,sminp,T)
      call GetGlobalInd(mpirank,T,ia,ja)

      do alpha = 1,2,1
          kbcell1 = kbSOcell(alpha)
          kbcell2 = kbSOcell(alpha)
          tkb1    = tkbSO(alpha)
          kbetmp  = kbso(alpha)%vata(3,:)
          do ii = 1,size(kbcell),1
              kbtmp1 = kbcell(ii)
              call ModBCDist(FRC,kbtmp1,smimb,sminb,mpisize,1)
              call circshift(kbtmp1%vata,(/0,1/))
              call ModBCDist(FRC,kbtmp1,smimb,sminb,smimp,sminp)
              kbcell(ii) = kbtmp1
          end do
          call mergeDispPairs(FRC,Tcell,tvtmp,kbcell1,tkb1,kbetmp,kbcell2,.FALSE.)
      end do
      call spruceCell(FRC,Tcell,idel)
      delet

      do alpha = 1,2,1
          kbcell1 = kbSOcell(alpha)
          kbcell2 = kbSOcell(alpha)
          tkb1    = tkbSO(alpha)
          tkb2    = tkbSO(alpha)
          kbetmp  = kbso(alpha)%vata(3,:)
          do ii = 1,size(kbcell)
              kbtmp1 = kbcell(ii)
              call ModBCDist(FRC,kbtmp1,smimb,sminb,mpisize,1)
              call circshift(kbtmp1%vata,(/0,1/))
              call ModBCDist(FRC,kbtmp1,smimb,sminb,smimp,sminp)
              kbcell(ii) = kbtmp1
          end do
          call mergeDispPairs(FRC,Tcell,tvtmp,kbcell1,tkb1,kbetmp,kbcell2,.FALSE.)
      end do
      call spruceCell(FRC,Tcell,idel)
      delet

      do alpha = 1,2,1
          kbcell1 = kbSOcell(alpha)
          kbcell2 = kbSOcell(alpha)
          tkb1    = tkbSO(alpha)
          tkb1    = tkbSO(alpha)
          tkb2    = tkbSO(alpha)
          kbetmp  = kbso(alpha)%vata(4,:)
          call mergeDispPairs(FRC,Tcell,tvtmp,kbcell1,tkb1,kbetmp,kbcell2,.FALSE.)
      end do
      call spruceCell(FRC,Tcell,idel)
      delet

      vnlSOcell = 
      tvSOcell  = 

      return
      end subroutine calcVnlMatSOI
      end module calcVnlMatSOIMod
