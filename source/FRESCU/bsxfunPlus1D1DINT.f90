! bsxfunPlus1D1DINT.f90

!***********************************************************************************
      !
      !
      !
!***********************************************************************************

      module bsxfunPlus1D1DINTMOD
      contains
      subroutine bsxfunPlus1D1DINT(A,B,C)

      implicit none

      integer, allocatable :: A(:), B(:), C(:,:)

      integer :: M, N, ii, jj

      ! body of this function

      M = size(A)
      N = size(B)
      forall(ii=1:M,jj=1:N)
              C(ii,jj) = A(ii)+B(jj)
      end forall

      return
      end subroutine bsxfunPlus1D1DINT
      end module bsxfunPlus1D1DINTMOD
