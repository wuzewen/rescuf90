!  UpdateVeff.f90 
!****************************************************************************
!
!  PROGRAM: UpdateVeff
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module UpdateVeff2D_module
    contains
    subroutine UpdateVeff2D(FRC,veff,vadd,anterpolate,veffout)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    use VataMN_2D_module
    use distmat_bsxfun_plus_module
    use ModBCDist_1D_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_1D)                 :: vadd
    type(VataMN_2D)                 :: veff
    logical                         :: anterpolate
    
    ! temporary variables
    integer :: mpisize, ispin
    
    ! output variables
    type(VataMN_2D) :: veffout

    ! Body of UpdateVeff
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    
    if (ispin==1) then
            !write(*,*) "In UpdateVeff. marker 1."
        !call distmat_bsxfun_Plus(FRC,vadd,veff,veffout)
        allocate(veffout%vata(size(veff%vata,1),size(veff%vata,2)))
        veffout        = veff
        !write(*,*) "sizes of veff and vadd.", size(veff%vata), size(vadd%vata)
        veffout%vata(:,1)   = veff%vata(:,1)+vadd%vata(:)
        !write(*,*) "In UpdateVeff. marker 2."
        !veffout%n      = 1
        !veffout%nblock = 1
    else if (ispin==2) then
        write(*,*) "Error in UpdateVeff.f90. Collinear spin is not available now."
        !call distmat_bsxfunPlus(FRC,vadd,veff,veff)
    else if (ispin==4) then
        write(*,*) "Error in UpdateVeff.f90. Non-collinear spin is not available now."
        stop
        !call ModBCDist(FRC,vadd,veff%mblock,veff%nblock,veff%mproc,veff%nproc,.FALSE.,.FALSE.,vadd)
        !veff%datav(:,1) = veff%datav(:,1)+vadd%datav
        !veff%datav(:,4) = veff%datav(:,4)+vadd%datav
    else
        write(*,*) 'Error'
    end if
    
    if (anterpolate) then
        !write(*,*) "Error in UpdateVeff.f90. anterpolate should be false now."
        !stop
        !call ModBCDist_1D(FRC,veff,1,1,1,mpisize,.FAlSE.,.FALSE.,veff)
        !if (size(veff%vata,2)>0) then
        !    veff%vata = interpolation%AntFun(veff%vata)
        !end if
        veff%m = product(FRC%domain%cgridn)
        !call ModBCDist_1D(FRC,veff,veff%m/mpisize,1,mpisize,1,.FALSE.,.FALSE.,veff)
    end if
    
    return
    end subroutine UpdateVeff2D
    end module UpdateVeff2D_module
