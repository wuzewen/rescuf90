! initMixer.f90
!*************************************************************************
    !
    ! This is a subroutine to init how to mix during ths calculation.
    !
!*************************************************************************
    
    module initMixer_module
    contains
    subroutine initMixer(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    integer                              :: ii, nline
    logical                              :: MixingTypeIsFind, MixingBetaIsFind, MixingBetaLinIsFind
    logical                              :: MixingMethodIsFind, MixingTolEnergyIsFind, MixingTolRhoIsFind
    logical                              :: MixingAlphaIsFind, MixingLambdaIsFind, MixingHistoryIsFind
    logical                              :: MixingInitLinIsFind
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%mixer) then
        MixingTypeIsFind      = .FALSE.
        MixingBetaIsFind      = .FALSE.
        MixingBetaLinIsFind   = .FALSE.
        MixingMethodIsFind    = .FALSE.
        MixingTolEnergyIsFind = .FALSE.
        MixingTolRhoIsFind    = .FALSE.
        MixingAlphaIsFind     = .FALSE.
        MixingLambdaIsFind    = .FALSE.
        MixingHistoryIsFind   = .FALSE.
        MixingInitLinIsFind   = .FALSE.
        nline                 =  inputFromFile%numberOfInput
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingType")) then
                MixingTypeIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingTypeIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%mixing%mixingType
        else
            FRC%mixing%mixingType = "potential"
        end if
        !write(*,*) "I'm here before MixingBeta in initMixer."
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingBeta")) then
                MixingBetaIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingBetaIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%mixing%Beta
        else
            FRC%mixing%Beta = 0.25D0
        end if
        !write(*,*) "I'm here after MixingBeta in initMixer."
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingBetaLin")) then
                MixingBetaLinIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingBetaLinIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%mixing%BetaLin
        else
            if (MixingBetaIsFind) then
                FRC%mixing%BetaLin = FRC%mixing%Beta
            else
                FRC%mixing%BetaLin = 0.1E0
            end if
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingMethod")) then
                MixingMethodIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingMethodIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%mixing%Method
        else
            FRC%mixing%Method = "broyden"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingTolEnergy")) then
                MixingTolEnergyIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingTolEnergyIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%mixing%Tol(1)
        else
            FRC%mixing%Tol(1) = 1.0D-5
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingTolRho")) then
                MixingTolRhoIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingTolRhoIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%mixing%Tol(2)
        else
            FRC%mixing%Tol(2) = 1.0D-5
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingAlpha")) then
                MixingAlphaIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingAlphaIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%mixing%Alpha
        else
            FRC%mixing%Alpha = 1.5D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingLambda")) then
                MixingLambdaIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingLambdaIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%mixing%Lambda
        else
            FRC%mixing%Lambda = 0.5D0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingHistory")) then
                MixingHistoryIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingHistoryIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%mixing%maxHistory
        else
            FRC%mixing%maxHistory = 20
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("MixingInitLin")) then
                MixingInitLinIsFind = .TRUE.
                exit
            end if
        end do
        if (MixingInitLinIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%mixing%InitLin
        else
            FRC%mixing%InitLin = 1
        end if
    end if
    
    FRC%init%mixer = .TRUE.
    
    return
    end subroutine initMixer
    end module initMixer_module
