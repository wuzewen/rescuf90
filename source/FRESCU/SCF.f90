!  SCF.f90 
!****************************************************************************
!
!  PROGRAM: SCF
!
!  PURPOSE: Main function of RESCU in FORTRAN version.
!       Including: SCF, DOS, force, total energy, mulliken populations.
!
!****************************************************************************

    module SCF_module
    contains
    subroutine SCF(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use GenHamiltonian_module
    use getHartreePotential_module
    use XCmain_module
    use VataMN_2D_module
    use solveKSrealDirectly_module
    use KS_main_real_scf_module
    use mixer_module
    use updateoccupancy_module
    use UpdateRHO_module
    use UpdateVeff_module
    use writeEverything_module
    use displayError_module
    !use cleanStruct_module
    use distmat_bsxfun_Minus_module
    use calcBS_module
    use GetInitialSubspace_module
    use, intrinsic :: iso_c_binding

    !include 'fftw3.f03'
    
    !implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    logical                         :: shortrange, isConverged
    integer                         :: iter, nkpt, ngrid, ii, jj, nspin, maxp
    integer                         :: nband
    type(VataMN_2D)                 :: rho
    type(VataMN_1D)                 :: VXCout, rhotmp
    real*8                          :: dV, dRho, dE
    integer                         :: time0 , time1 , time2, time3, time4
    integer                         :: crate0, crate1, crate2, crate3, crate4
    integer                         :: cmax0 , cmax1 , cmax2, cmax3, cmax4
    real                            :: dtime1
    integer :: mpisize, mpirank, nsize
    character(len=20)    :: flax, numb
    integer :: cou

    !integer(C_INT)                              :: L, M, N
    !complex(C_DOUBLE_COMPLEX), dimension(L,M,N) :: Arr, Brr

    ! output variables
    

    ! Body of SCF
    shortrange = FRC%option%shortRange
    ngrid      = product(FRC%domain%cgridn)
    nband      = FRC%eigenSolver%nband
    mpirank    = FRC%mpi%rank
    mpisize    = FRC%mpi%mpisize
    nspin      = FRC%spin%nspin
    maxp       = FRC%mixing%maxhistory
    nkpt       = size(FRC%kpoint%ikdirect,1)

    FRC%L      = FRC%domain%cgridn(1)
    FRC%M      = FRC%domain%cgridn(2)
    FRC%N      = FRC%domain%cgridn(3)

    !write(*,*) 'before allocate Arr Brr.'
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Create plans for doing FFT and IFFT calculations with FFWW3 lib. %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    allocate(FRC%Arr(FRC%L,FRC%M,FRC%N))
    !allocate(FRC%Arr(FRC%domain%cgridn(1),FRC%domain%cgridn(2),FRC%domain%cgridn(3)))
    !allocate(FRC%Brr(FRC%L,FRC%M,FRC%N))
    !write(*,*) 'after allocate Arr Brr.'
    call dfftw_plan_dft_3d(FRC%planfft ,FRC%L,FRC%M,FRC%N,FRC%Arr,FRC%Arr,FFTW_FORWARD ,FFTW_EXHAUSTIVE)
    call dfftw_plan_dft_3d(FRC%planifft,FRC%L,FRC%M,FRC%N,FRC%Arr,FRC%Arr,FFTW_BACKWARD,FFTW_EXHAUSTIVE)
    !write(*,*) 'after plan.'
    call calcsize(mpisize,mpirank,ngrid,nsize)
    !outfile    = FRC%info%outfile
    !write(*,*) 'nsize', nsize
!*********************************************  001 start
! generating Hamiltonian, neutral atom and prtial core density
    !******* allocate memories for allocatable variables.
    !******* Mixing *******

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Allocate memory for the variables.                              %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    if (trim(FRC%Mixing%Method) == trim("Broyden")) then
        allocate(FRC%mixing%u(nsize,maxp))
        allocate(FRC%mixing%vt(nsize,maxp))
        allocate(FRC%mixing%F_mml(nsize))
        allocate(FRC%mixing%mu_mml(nsize))
        FRC%mixing%u      = 0.0D0
        FRC%mixing%vt     = 0.0D0
        FRC%mixing%F_mml  = 0.0D0
        FRC%mixing%mu_mml = 0.0D0
    end if
    !******* Potential *******
    allocate(FRC%potential%VH%vata(nsize))
    allocate(FRC%potential%VHout%vata(nsize))
    allocate(FRC%potential%veffin(1)%vata(nsize))
    allocate(FRC%potential%veffin(2)%vata(nsize))
    allocate(FRC%potential%veffout%vata(nsize))
    allocate(FRC%potential%vloc(ngrid,1))
    allocate(FRC%potential%VXCout%vata(nsize,2))
    !******* Energy *******
    allocate(FRC%energy%ksnrg%vata(nband,nkpt,nspin))
    allocate(FRC%energy%XCdens%vata(nsize))
    allocate(FRC%energy%nocc(nband,nkpt,nspin))
    !******* Wave Function *******
    allocate(FRC%psi(nkpt,FRC%spin%nspin))
    do ii = 1,nkpt,1
        do jj = 1,nspin,1
            allocate(FRC%psi(ii,jj)%vata(nsize,nband))
        end do
    end do
    !******* Rho *******
    allocate(FRC%rho%output(1)%vata(nsize,nspin))
    allocate(FRC%rho%output(2)%vata(nsize,nspin))
    !******* Temporary variables *******
    allocate(rho%vata(nsize,nspin))
    allocate(rhotmp%vata(nsize))
    allocate(VXCout%vata(nsize))

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate initial Hamiltonian                                 %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    call GenHamiltonian(FRC)
    !call writeEverything(FRC)
    !write(*,*) "I'm here, in SCF after GenHamiltonian."
!*********************************************  001 end
    
    !if (shortRange) then
        !call calcEsrFourier(FRC)
    !else
    !    write(*,*) "Error in SCF.f90. ShortRange must be true now."
    !    stop
    !    if (FRC%energy%ewaldE) then
    !        call IonIonInt(FRC)
    !        call IonAlphaFactor(FRC)
    !    else
    !        FRC%energy%ionion = 0
    !        FRC%energy%alphaZ = 0
    !    end if
    !end if
    
!********************************************  002 start
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate subspace                                           %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    call system_clock(time1,crate1,cmax1)
    if (FRC%LCAO%status) then
        write(*,*) "Error in SCF.f90. LCAO is not."
        !call GetAtomicOrbitalSubspace(FRC)
    else
        call GetInitialSubspace(FRC)
    end if
    !write(numb,"(i4.4)") mpirank
    !flax = 'psi'//trim(numb)//'.txt'
    !cou  =  mpirank + 10
    !open(unit=cou,file=flax)

    !do jj = 1,size(FRC%psi,2),1
    !    do ii = 1,size(FRC%psi,1),1
    !    write(cou,*) ii, jj
    !    write(cou,*) FRC%psi(ii,jj)%vata
    !    end do
    !end do
    !close(cou)
    call system_clock(time2,crate2,cmax2)
    dtime1 = real(time2-time1)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating initial subspace:                                 ", dtime1
    end if

    !write(numb,"(i4.4)") mpirank
    !flax = 'rho'//trim(numb)//'.txt'
    !cou  =  mpirank + 10
    !open(unit=cou,file=flax)
    !write(cou,*)  mpirank
    !write(cou,*) 'FRC%rho%output(2) ='
    !write(cou,*)  FRC%rho%output(2)%vata
    !close(cou)
    !stop
    !write(*,*) "I,m here after GetInitialSubspace in scf."
!********************************************  002 end
    
    !if (FRC%functional%hybrid) then
    !    call GenGmask(FRC)
    !    call GenDMxx0(FRC)
    !    call DetYcondition(FRC)
    !    
    !    if (FRC%Exx%UseSMI) then
    !        call calcYmatrixSMI(FRC)
    !    else
    !        call calcYmatrix(FRC)
    !    end if
    !end if
    
    !call initTime(FRC)
   ! 
    !FRC%scloop     = 0
    isConverged    = .FALSE.
    dRho           =  0.0D0
    dE             =  0.0D0
   ! 
!****************************************************
!****************************************************
! scf loop start

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Start SCF loop.                                                %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    call system_clock(time0,crate0,cmax0)
    time1  = time0
    crate1 = crate0 
    cmax1  = cmax0
    if (mpirank == 0) then
    write(*,*) "************************************************************************"
    write(*,*) "Entering self-consistent loop at                   ", time0
    write(*,*) "************************************************************************"
    write(*,*) "#        ", "iter               ", "dRho                      ", "dE          ", "dt"
    end if
    FRC%scloop = 0
    do while(FRC%scloop < FRC%option%maxSCfIteration)
        FRC%scloop = FRC%scloop+1
        iter       = FRC%scloop
        !write(*,*) "Self-consistent loop", iter
    
        !    call initTime(FRC)
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        !%%%% solve Kohn-Sham equation, get new subspace.                      %%%%!
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        call system_clock(time3,crate3,cmax3)
        if (FRC%LCAO%status) then
            write(*,*) "Error in SCF.f90. LCAO is not."
    !        call ks_main_lcao_scf(FRC)
        else if (trim(FRC%eigenSolver%algo) == trim("CFSI")) then
            if (FRC%mpi%paraK) then
                write(*,*) "Error in SCF.f90. paraK is not available."    
    !            call ks_main_real_scf_paraK(FRC)
            else
                call ks_main_real_scf(FRC)
                !write(*,*) "I'm here after solve KS in SCF."
            end if
        else
            call solveKSrealDirectly(FRC)
        end if
        !call writeEverything(FRC)
        !write(*,*) "I'm here after solve KS in SCF."

        call system_clock(time4,crate4,cmax4)
        dtime1 = real(time4-time3)/real(crate3)
        if (mpirank == 0) then
                write(*,*) "               Solving Kohn-Sham equation:  ", dtime1
        end if
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        !%%%% Update occupation numbers and fermi energy.                     %%%%!
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        call UpdateOccupancy(FRC)
        !write(*,*) "I'm here after UpdateOccupancy."
        call system_clock(time3,crate3,cmax3)
        dtime1 = real(time3-time4)/real(crate4)
        if (mpirank == 0) then
                write(*,*) "               Update Occupancy:            ", dtime1
        end if

        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        !%%%% Calculate electronic density.                                  %%%%!
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !    if (FRC%LCAO%status .and. trim(FRC%mixing%mixingType) == trim("density-matrix") then
    !        call calcDensityMatrix(FRC)
    !        call Mixer(FRC)
    !        call updateRHO_LCAO_DM(FRC)
    !    else if (FRC%LCAO%status) then
    !        call updateRHO_LCAO(FRC)
    !    else
            call updateRHO(FRC,FRC%energy%nocc,FRC%rho%output(2))
         !end if
        call system_clock(time4,crate4,cmax4)
        dtime1 = real(time4-time3)/real(crate3)
        if (mpirank == 0) then
                write(*,*) "               Update RHO:                  ", dtime1
        end if
            !write(*,*) "I'm here after updateRHO in scf"
            !write(*,*) 'FRC%rho%output(2) ='
            !write(*,*)  FRC%rho%output(2)%vata
    !    end if
    !write(numb,"(i4.4)") mpirank
    !flax = 'rho'//trim(numb)//'.txt'
    !cou  =  mpirank + 10
    !open(unit=cou,file=flax)
    !write(cou,*)  mpirank
    !write(cou,*) 'FRC%rho%output(2) ='
    !write(cou,*)  FRC%rho%output(2)%vata
    !close(cou)
    !stop
     
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        !%%%%  Mix Density or Potential.                                     %%%%!
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !    if (trim(FRC%mixing%mixingType) == trim("density")) then
            call Mixer(FRC)
            !write(*,*) "I'm here after mixer in scf."
    !    end
        call system_clock(time3,crate3,cmax3)
        dtime1 = real(time3-time4)/real(crate4)
        if (mpirank == 0) then
                write(*,*) "               Mixing RHO or Potential:     ", dtime1
        end if
        FRC%rho%input(2)%m       = FRC%rho%input(1)%m
        FRC%rho%input(2)%n       = FRC%rho%input(1)%n
        FRC%rho%input(2)%mblock  = FRC%rho%input(1)%mblock
        FRC%rho%input(2)%nblock  = FRC%rho%input(1)%nblock
        FRC%rho%input(2)%mproc   = FRC%rho%input(1)%mproc
        FRC%rho%input(2)%nproc   = FRC%rho%input(1)%nproc
        if (trim(FRC%mixing%mixingType) == trim("density")) then
            rho = FRC%rho%input(2)
        else
            rho = FRC%rho%input(1)
        end if
        
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        !%%%% Generate Hartree Potential.                                    %%%%!
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        if (shortRange) then
            if (FRC%spin%ispin == 1) then
                !call distmat_bsxfun_Minus(FRC,rho,FRC%rho%atom,rho)
                rho%vata = rho%vata - FRC%rho%atom%vata
            else if (FRC%spin%ispin == 2) then
                write(*,*) "Error in SCF.f90. Collinear spin is not available."
    !            call distmat_bsxfun2(FRC)
            else if (FRC%spin%ispin == 4) then
                write(*,*) "Error in SCF.f90. General spin is not available."
    !            call distmat_bsxfunMinus(FRC)
            end if
        end if
        rhotmp%vata(:)   = rho%vata(:,1)
        rhotmp%m         = rho%m
        rhotmp%n         = rho%n
        rhotmp%mblock    = rho%mblock
        rhotmp%nblock    = rho%nblock
        rhotmp%mproc     = rho%mproc
        rhotmp%nproc     = rho%nproc
        call GetHartreePotential(FRC,rhotmp,FRC%domain%boundary,.TRUE.,FRC%potential%VHout)
        !write(*,*) "I'm here after GetHartreePotential in scf."
        call system_clock(time4,crate4,cmax4)
        dtime1 = real(time4-time3)/real(crate3)
        if (mpirank == 0) then
                write(*,*) "               Update Hartree potential:    ", dtime1
        end if

        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        !%%%% generate XC Functional.                                      %%%%!
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        call xcMain(FRC)
        !write(*,*) "stop here after xcMain in scf."

        call system_clock(time3,crate3,cmax3)
        dtime1 = real(time3-time4)/real(crate4)
        if (mpirank == 0) then
                write(*,*) "               Update XC potential:         ", dtime1
        end if
    !******* Update effective Potential *******
    !    if (trim(FRC%mixing%mixingType) == trim("potential")) then
    !        call updateVeff(FRC)
    !    else
            VXCout%vata(:) = FRC%potential%VXCout%vata(:,1)
            VXCout%m       = FRC%potential%VXCout%m
            VXCout%n       = FRC%potential%VXCout%n
            VXCout%mblock  = FRC%potential%VXCout%mblock
            VXCout%nblock  = FRC%potential%VXCout%nblock
            VXCout%mproc   = FRC%potential%VXCout%mproc
            VXCout%nproc   = FRC%potential%VXCout%nproc
            call updateVeff(FRC,VXCout,FRC%potential%VHout,.FALSE.,FRC%potential%Veffin(2))
            !write(*,*) "After updateVeff in SCF"
    !    end if
    !    
    !******* Calculate total energy *******
    !    if (shortRange) then
    !        call totalEnergy2(FRC)
    !    else
    !        call totalEnergy1(FRC)
    !    end if
        
    !    if (trim(FRC%mixing%mixingType) == trim("potential")) then
    !        call Mixer(FRC)
    !    end if
    !    
    !    call InterMediarySave(FRC)
    !    
    !******* calculate delta E and delta Rho *******
        FRC%potential%veffin(2)%nblock = 1
        FRC%potential%veffin(2)%n      = 1

        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        ! Calculate the difference between new and old rho or potential.   !
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        call displayError(FRC,dV,dRho,dE)

    !******* Renew potential and rho *******
        FRC%potential%veffin(1) = FRC%potential%veffin(2)
        FRC%rho%input(1)        = FRC%rho%input(2)
        FRC%rho%output(1)       = FRC%rho%output(2)
    !    
    !    call cleanStruct(FRC)
    !    
        !if (iter == 1) then
        !    call writeEverything(FRC)
        !end if
        !write(*,*) "Stop in SCF"
        !stop

        call system_clock(time2,crate2,cmax2)
        dtime1 = real(time2-time1)/real(crate2)
        time1  = time2
        crate1 = crate2
        cmax1  = cmax2
        if (mpirank == 0) then
                write(*,*) "#", iter, dRho, dE, dtime1
        end if
        !write(*,*) "dRho = ", dRho, "dE = ", dE
        !write(*,*) "Fermi-Energy=", FRC%energy%EFermi

        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        !%%%% Determine it's converged or not.                         %%%%!
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        if (iter>1 .and. dRHO<FRC%mixing%tol(2) .and. dE<FRC%mixing%tol(1)) then
            isConverged = .TRUE.
            call system_clock(time2,crate2,cmax2)
            dtime1 = dble(time2-time0)/dble(crate2)
            if (mpirank == 0) then
            write(*,*) "************************************************************************"
            write(*,*) "Self-consistent converged. Total time used: ", dtime1
            write(*,*) "************************************************************************"
            !write(*,*) "Starting calculate Band Structure."
            end if
            if (FRC%info%calcBSafterSCF) then
                    call calcBS(FRC)
            end if
            !open(unit=33,file='rho.out')
            !do ii = 1,size(FRC%rho%input(2)%vata,1),1
            !    write(33,*) FRC%rho%input(2)%vata(ii,:)
            !end do
            !close(33)
            exit
        end if
    end do

    !if (FRC%mpi%paraK .and. (FRC%option%saveWaveFunction .or. FRC%force%status .or. FRC%DOS%status .or. FRC%LCAO%mulliken)) then
    !    call distributeWaveFunction(FRC)
    !end if
    
!*********************************** force
    !if (FRC%force%ftatus) then
    !    call force_main(FRC)
    !end if
    
!*********************************** dos
    !if (FRC%DOS%status) then
    !    call dos_main(FRC)
    !end if
    
!*********************************** mulliken
    !if (FRC%LCAO%mulliken) then
    !    call mulliken_main(FRC)
    !end if
    
    !call saveMatRcal(FRC)
    if ((.not. isConverged) .and. mpirank == 0) then
        write(*,*) "FRESCU failed to converge the ground state density within the prescribed tolerance."
    end if

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Destroy the plans for FFT and IFFT with FFTW3 lib       %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    call dfftw_destroy_plan(FRC%planfft)
    call dfftw_destroy_plan(FRC%planifft)

    return
    end subroutine SCF
    end module SCF_module

