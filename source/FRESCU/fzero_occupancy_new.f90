! fzero_occupancy.f90
    
!***********************************************************************************************
    !
    !
    !
!***********************************************************************************************
    
    module fzero_occupancy_module
    contains
    subroutine fzero_occupancy(sigma,kweight,energy,ntot,mu0,mu)
    
    use fermiDirac_3D_module
    
    implicit none
    
    ! input variables
    real*8              :: sigma, ntot, mu0
    real*8, allocatable :: kweight(:), energy(:,:,:)
    
    ! temporary variables
    real*8, allocatable :: focc(:,:,:), foccT(:)
    real*8              :: init0, init1, init2, init3, tmp1, tmp2, mut, h
    real*8              :: ef0, ef02, ef1, ef2, ef3, ef4, initD, initM, initX
    real*8              :: initD2, initM2, initX2, h2, deltaF, deltaF2
    integer             :: ii
    
    ! output variables
    real*8 :: mu
    
    ! body of this function
    mulow = minval(energy)
    muhig = maxval(energy)
    call fermiDirac_3D(sigma,mulow,energy,focc)
    allocate(foccT(size(focc,2)))
    foccT = sum(sum(focc,3),1)
    deallocate(focc)
    init0 = sum(foccT*kweight)-ntot
    init1      = init0
    if (init1 > 0) then
        init2      = 1.0D0
        init3      = 1.0D0
    else if (init1 == 0) then
        mu = mu0
        return
    else
        init2      = -1.0D0
        init3      = -1.0D0
    end if
    h          = 0.10D0
    h2         = 0.000001D0
    mu         = mu0
    mut        = mu
    tmp1       = init0*init2
    tmp2       = init1*init3
    do while (tmp1 > 0.0 .and. tmp2 > 0.0)
        mu = mu+h
        call fermiDirac_3D(sigma,mu,energy,focc)
        foccT = sum(sum(focc,3),1)
        deallocate(focc)
        init2 = sum(foccT*kweight)-ntot
        tmp1  = init0*init2
        if (tmp1 <= 0.0) then
                ef1    = mu-h
                ef2    = mu
                initD  = init2
                initX  = init0
                deltaF = h
                do while (deltaF > h2)
                    ef0  = (ef1+ef2)/2.0
                    call fermiDirac_3D(sigma,ef0,energy,focc)
                    foccT = sum(sum(focc,3),1)
                    deallocate(focc)
                    initM = sum(foccT*kweight)-ntot
                    if (initX*initM <= 0.0) then
                            ef1   = ef1
                            ef2   = ef0
                            initD = initM
                    else if (initM*initD <= 0.0) then
                            ef1 = ef0
                            ef2 = ef2
                            initX = initM
                    end if
                    deltaF = deltaF/2.0D0
                end do
            mu = (ef1+ef2)/2.0D0
            exit
        end if
        init0      = init2
        
        mut = mut-h
        call fermiDirac_3D(sigma,mut,energy,focc)
        foccT = sum(sum(focc,3),1)
        deallocate(focc)
        init3 = sum(foccT*kweight)-ntot
        tmp2  = init1*init3
        if (tmp2 <= 0) then
                ef3 = mut
                ef4 = mut+h
                initX2 = init3
                initD2 = init1
                deltaF2 = h
                do while (deltaF2 > h2)
                    ef02 = (ef3+ef4)/2.0D0
                    call fermiDirac_3D(sigma,ef02,energy,focc)
                    foccT = sum(sum(focc,3),1)
                    deallocate(focc)
                    initM2 = sum(foccT*kweight)-ntot
                    if (initX2*initM2 <= 0.0) then
                            ef3    = ef3
                            ef4    = ef02
                            initD2 = initM2
                    else if (initM2*initD2 <= 0.0) then
                            ef3    = ef02
                            ef4    = ef4
                            initX2 = initM2
                    end if
                    deltaF2 = deltaF2/2.0D0
                end do
            mu = (ef3+ef4)/2.0D0
            exit
        end if
        init1      = init3
    end do
    
    return
    end subroutine fzero_occupancy
    end module fzero_occupancy_module
