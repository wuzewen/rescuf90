! rkronprod_cmplx.f90
    
!*****************************************************************
    !
    !
    !
!*****************************************************************
    
    module rkronprod_cmplx_module
    contains
    subroutine rkronprod_cmplx(A,B,KA)
    
    external ZGEMM
    !implicit none
    
    ! input variables
    complex*16, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    integer :: p, q, n, m
    complex*16, allocatable :: T(:,:), Atmp(:,:)
    character(LEN=1) :: transa, transb
    integer          :: ii, jj, kk, LDA, LDB, LDC
    complex*16       :: alpha, beta

    
    ! output variables
    complex*16, allocatable :: KA(:,:)
    
    ! body of this function
    p = size(A,1)
    q = size(A,2)
    n = size(B,1)/q
    m = size(B,2)
    
    allocate(T(q,m*n))
    T = reshape(B,(/q,m*n/))
    allocate(Atmp(p,m*n))

    transa = 'N'
    transb = 'N'
    ii     =  p
    jj     =  m*n
    kk     =  q
    LDA    =  ii
    LDB    =  kk
    LDC    =  ii
    alpha  = (1.0D0,0.0D0)
    beta   = (0.0D0,0.0D0)
    call ZGEMM(transa,transb,ii,jj,kk,alpha,A,LDA,T,LDB,beta,Atmp,LDC)
    !Atmp = matmul(A,T)
    !allocate(KA(p*n,m))
    KA = reshape(Atmp,(/p*n,m/))
    
    return
    end subroutine rkronprod_cmplx
    end module rkronprod_cmplx_module
