! reduceOper.f90
    
!*************************************************************************
    !
    ! In this function, kpt = [0 0 0] is used in GetNonLocalPotentialK.
    ! While if kpt is not [0 0 0], this function should be fixed, with
    ! complex numbers and arrays. When it is done, renew this notes.
    ! Another important thing is, sparse matrix is not used here.
    !
!*************************************************************************
    
    module reduceOper_module
    contains
    subroutine reduceOper(Acell,Av,kpt,spnnz,oper)
    
    use RealVata_2D_module
    
    implicit none
    
    ! input variables
    type(RealVata_2D), allocatable :: Acell(:)
    integer                      :: kpt(1,3), spnnz, ii
    integer, allocatable         :: Av(:,:)
    
    ! temporary variables
    real*8  :: pi
    integer :: datatmp(3,1), m, n, ncell, tmp
    complex*16 :: im
    
    ! output variables
    type(RealVata_2D)              :: oper
    
    ! body of this function
    im   = (0.0D0,1.0D0)
    pi   = 3.1415926535897932385D0
    m    = size(Acell(1)%vata,1)
    n    = size(Acell(1)%vata,2)
    
    allocate(oper%vata(m,n))
    oper%vata = 0.0D0
    
    !if (spnnz >= 0) then
    ncell = size(Acell)
    
    do ii = 1,ncell,1
        datatmp(1,:) = Av(ii,:)
        tmp          = kpt(1,1)*datatmp(1,1)+kpt(1,2)*datatmp(2,1)+kpt(1,3)*datatmp(3,1) !matmul(kpt,datatmp) !
        oper%vata    = oper%vata + Acell(ii)%vata*exp(2.0D0*dble(im)*pi*dble(tmp))
    end do
    
    return
    end subroutine reduceOper
    end module reduceOper_module
