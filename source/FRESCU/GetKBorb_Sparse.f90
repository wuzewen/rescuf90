!  GetKBorb_Sparse.f90 
!
!  FUNCTIONS:
!  GetKBorb - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: GetKBorb
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module GetKBorb_Sparse_module
    contains
    subroutine GetKBorb_Sparse(FRC,kpt)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use RealVata_2D_module
    use GetRSphase_module
    use reduceOper_real_module
    use bsxfunTimes_module
    use bsxfunTimes_CMPLX_module
    use reduceOper_Sparse_cmplx_module
    use reduceOper_BLAS_cmplx_module
    use reformKBorb_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8                          :: kpt(3)
    
    ! temporary variables
    integer :: fgridn(3), fn, ntmp, ii!, jj
    logical :: anterpvnl, kbsparse
    complex*16, allocatable         :: eikr(:,:), kborb(:,:)
    real*8,  allocatable         :: kbvec(:,:)!, eikrtmp(:,:)
    type(RealVata_2D), allocatable :: kbcell(:)

    
    ! output variables

    ! Body of GetKBorb
    !if (size(FRC%potential%vnl%KBorb,1) /= 0 .and. FRC%potential%vnl%kpt == kpt) then
    !    return
    !end if
    
    fgridn    = FRC%domain%fgridn
    fn        = product(fgridn)
    anterpvnl = (.not. FRC%interpolation%vnl)
    call getrsphase(FRC,kpt,fgridn,eikr)
    
    if (FRC%spin%SOI) then
        write(*,*) "Error in GetKBorb.f90. SOI is not avalaible now."
        stop
    end if
    !ntmp     = size(FRC%potential%vnl%kbcell)
    !allocate(kbcell(ntmp))

    !do ii = 1,ntmp,1
    !    allocate(kbcell(ii)%vata(size(FRC%potential%vnl%kbcell(ii)%vata,1),size(FRC%potential%vnl%kbcell(ii)%vata,2)))
    !end do
    
    !kbcell   = FRC%potential%vnl%kbcell

    !allocate(kbvec(size(FRC%potential%vnl%kbvec,1),size(FRC%potential%vnl%kbvec,2)))
    !kbvec    = FRC%potential%vnl%kbvec

    kbsparse = FRC%potential%vnl%kbsparse
    kbsparse = .FALSE.
    !if (FRC%potential%vnl%reduceKBcell) then
            call reduceOper_BLAS_cmplx(FRC%potential%vnl%sparsekbcell,FRC%potential%vnl%kbvec,kpt,FRC%potential%vnl%sparseKBorb)
            !call reduceOper_Sparse_cmplx(FRC%potential%vnl%sparsekbcell,FRC%potential%vnl%kbvec,kpt,FRC%potential%vnl%sparseKBorb)
    !else
    !        call reformKBorb(FRC%potential%vnl%sparsekbcell,FRC%potential%vnl%kbvec,kpt,FRC%potential%vnl%sparseKBorb)
    !end if
    !write(*,*) "after reduceOper_Sparse_cmplx in GetKBorb_Sparse_Cmplx"
    !write(*,*)  FRC%mpi%rank, FRC%potential%vnl%sparseKBorb%N_nonzero
    !if (kbsparse) then
        !call spdiags(eikr,0,fn,fn,kborbtmp)
        !kborb = kborbtmp*kborb%dataV
    !else
        !allocate(eikrtmp(size(eikr,1),size(eikr,2)))
        !eikrtmp = real(eikr)
    !    call bsxfunTimes_CMPLX(eikr,kborb,kborb)
    !end if
    !write(*,*) "size of FRC%potential%vnl%sparseKBorb."
    !write(*,*)  FRC%potential%vnl%sparseKBorb%N_nonzero
    !write(*,*)  size(FRC%potential%vnl%sparseKBorb%ValueN)
    !write(*,*) "FRC%potential%vnl%sparseKBorb%Index1"
    !write(*,*)  FRC%potential%vnl%sparseKBorb%Index1
    forall(ii=1:FRC%potential%vnl%sparseKBorb%N_nonzero)
            FRC%potential%vnl%sparseKBorb%ValueN(ii) = FRC%potential%vnl%sparseKBorb%ValueN(ii)* &
                                                  eikr(FRC%potential%vnl%sparseKBorb%Index1(ii),1)
    end forall
    if (anterpVnl) then
        !kborb = FRC%interpolation%AntFun(kborb)
    end if


    !write(*,*) "sparseKBorb ="
    !do ii = 1,FRC%potential%vnl%sparseKBorb%N_nonzero,1
    !    write(*,*) FRC%potential%vnl%sparseKBorb%Index1(ii), FRC%potential%vnl%sparseKBorb%Index2(ii), FRC%potential%vnl%sparseKBorb%ValueN(ii)
    !end do
    !allocate(FRC%potential%vnl%KBorb(size(kborb,1),size(kborb,2)))
    !FRC%potential%vnl%KBorb = kborb
    FRC%potential%vnl%kpt   = kpt

    return
    end subroutine GetKBorb_Sparse
    end module GetKBorb_Sparse_module

