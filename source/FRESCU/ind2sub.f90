! ind2sub.f90

!************************************************************
      !
      ! The same function as function ind2sub.m in matlab.
      !
      ! siz : input,  one dimentional integer Array, siz(siz) = 2
      ! ind : input,  integer
      ! kk  : output, integer
      ! ss  : output, integer
      !
!************************************************************

      module ind2sub_module
      contains
      subroutine ind2sub(siz,ind,kk,ss)

      implicit none

      ! input varibales
      integer :: siz(2), ind

      ! temporary variables

      ! output variables
      integer :: kk, ss

      ! body of this function
      kk = mod(ind,siz(1))
      if (kk <= 0) then
              kk = siz(1)+kk
      end if
      ss = (ind - kk)/siz(1)+1
      if (ss > siz(2)) then
              write(*,*) "Error in ind2sub.f90. Ind out of boundary."
              stop
      end if

      return
      end subroutine ind2sub
      end module ind2sub_module

