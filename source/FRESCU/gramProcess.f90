! gramProcess.f90
    
!**************************************************************
    !
    !
    !
!**************************************************************
    
    module gramProcess_module
    contains
    subroutine gramProcess(blockX,mpistat,gramXBX,cholFlag)
    
    use chol_module
    use inversion_module
    
    implicit none
    
    ! input varibales
    real*8, allocatable :: blockX(:,:), gramXBX(:,:)
    logical :: mpistat
    
    ! temporary variables
    integer :: mpirank, ii
    real*8, allocatable :: gramXBXout(:,:), eye(:,:)
    
    ! output variables
    integer :: cholFlag
    
    ! body of this function
    cholFlag = 0
    allocate(gramXBX(size(blockX,2),size(blockX,2)))
    gramXBX  = matmul(transpose(blockX),blockX)
    
    if (mpistat) then
        write(*,*) "Error in gramProcess.f90. MPI is not available now."
        !stop
        !call MPI_Reduce_sum(gramXBX,gramXBX)
        !mpirank = MPI_Comm_rank
    else
        mpirank = 0
    end if
    if (mpirank == 0) then
        gramXBX = 0.5D0*(transpose(gramXBX)+gramXBX)
        call chol(gramXBX,gramXBXout,cholFlag)
    end if
    
    if (mpistat) then
        write(*,*) "Error in gramProcess.f90. MPI is not available now."
        !stop
        !call MPI_Bcast_variable(gramXBX,0,gramXBX)
        !call MPI_Bcast_variable(cholFlag,0,cholFlag)
    end if
    
    if (cholFlag == 0) then
        call inversion(gramXBXout,gramXBX)
        allocate(eye(size(gramXBX,1),size(gramXBX,2)))
        eye = 0.0D0
        forall(ii=1:size(gramXBX,1))
            eye(ii,ii) = 1.0D0
        end forall
        blockX = matmul(blockX,matmul(eye,gramXBX))
    end if
    
    return
    end subroutine gramProcess
    end module gramProcess_module
    
