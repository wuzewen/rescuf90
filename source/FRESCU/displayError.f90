! displayError.f90
    
!**********************************************************************
    !
    !
    !
!**********************************************************************
    
    module displayError_module
    contains
    subroutine displayError(FRC,dV,dRho,dE)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use determineVinVout_displayError_module
    use distmat_feval_plus_1D_module
    use distmat_feval_minus_1D_module
    use distmat_feval_minus_2D_module
    use VataMN_2D_module
    use VataMN_1D_module
    use MPI_Allreduce_sum_real8_1D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical         :: mpistat
    integer         :: mpirank, iter, Nval, ii
    real*8          :: avec(3,3), vol, dr
    type(VataMN_2D) :: rin, rout, Rhod
    type(VataMN_1D) :: vin, vout, meanV, diffV
    real*8, allocatable :: Vmean(:), dRhod(:), Vdiff(:), dRhotmp(:)
    integer,allocatable :: ranks(:)
    ! output variables
    real*8 :: dV, dRho, dE
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    iter    = FRC%scloop
    Nval    = FRC%eigensolver%Nvalence
    avec    = FRC%domain%latvec
    vol     = avec(1,1)*avec(2,2)*avec(3,3)  &
             +avec(1,2)*avec(2,3)*avec(3,1)  &
             +avec(1,3)*avec(2,1)*avec(3,2)  &
             -avec(1,1)*avec(2,3)*avec(3,2)  &
             -avec(1,2)*avec(2,1)*avec(3,3)  &
             -avec(1,3)*avec(2,2)*avec(3,1)
    vol     = abs(vol)
    dr      = vol/dble(product(FRC%domain%cgridn))

    !write(*,*) "some parameters in displayError."
    !write(*,*) "mpistat=",mpistat,"mpirank=",mpirank
    !write(*,*) "iter=",iter,"Nval=",Nval
    !write(*,*) "vol=",vol,"dr=",dr



    call determineVinVout_displayError(FRC,vin,vout,rin,rout)
    !write(*,*) 'vin',vin%m,vin%n,vin%mblock,vin%nblock,vin%mproc,vin%nproc
    !write(*,*) 'vout',vout%m,vout%n,vout%mblock,vout%nblock,vout%mproc,vout%nproc
    allocate(diffV%vata(size(vin%vata)))
    call distmat_feval_minus_1D(FRC,vin,vout,diffV)
    allocate(Vdiff(1))
    !write(*,*) 'Vdiff =', Vdiff
    Vdiff = sum(abs(diffV%vata))
    !write(*,*) 'Vdiff =', Vdiff

    !write(*,*) 'vin',vin%m,vin%n,vin%mblock,vin%nblock,vin%mproc,vin%nproc
    !write(*,*) 'vout',vout%m,vout%n,vout%mblock,vout%nblock,vout%mproc,vout%nproc
    allocate(meanV%vata(size(vin%vata)))
    call distmat_feval_plus_1D(FRC,vin,vout,meanV)
    allocate(Vmean(1))
    Vmean = sum(abs(meanV%vata))/2.0
    !write(*,*) 'rin',rin%m,rin%n,rin%mblock,rin%nblock,rin%mproc,rin%nproc
    !write(*,*) 'rout',rout%m,rout%n,rout%mblock,rout%nblock,rout%mproc,rout%nproc
    allocate(Rhod%vata(size(rin%vata,1),size(rin%vata,2)))
    call distmat_feval_minus_2D(FRC,rin,rout,Rhod)
    allocate(dRhod(size(Rhod%vata,2)))
    dRhod = sum(abs(Rhod%vata),1)
    !write(*,*) "vdiff, Vmean and dRhod in displayError."
    !write(*,*)  Vdiff, Vmean, dRhod
    if (mpistat) then
        !write(*,*) "Error in displayError.f90. MPI is not available now."
        !stop
        allocate(dRhotmp(size(dRhod)))
        allocate(ranks(FRC%mpi%mpisize))
        forall(ii=1:FRC%mpi%mpisize)
                ranks(ii) = ii-1
        end forall
        call MPI_Allreduce_sum_real8_1D(dRhod,ranks,dRhotmp)
        dRhod = dRhotmp
    end if
    
    dV   = Vdiff(1)/Vmean(1)
    dRho = sum(dRhod)*dr/dble(Nval)
    !write(*,*) "dV and dRho in displayError."
    !write(*,*)  dV, dRho
    !if (trim(FRC%info%calculationType) == trim("self-consistent") .or. trim(FRC%info%calculationType) == trim("relaxation") .or. trim(FRC%info%calculationType) == trim("phonon")) then
    !    Etot = FRC%energy%Etot(iter)
    !    if (iter > 1) then
    !        dE = abs(FRC%energy%Etot(iter)-FRC%energy%Etot(iter-1))/Nval
    !    else
    !        dE = 1.0
    !    end if
    !    if (mpistat) then
    !        write(*,*) "Error in displayError.f90. MPI is not available now."
    !        stop
    !    end if
    !    
    !else
    !    Etot = FRC%energy%Etot(1)
    !    dE   = 0.0
    !end if
    
    return
    end subroutine displayError
    end module displayError_module
