! InitFromRand.f90

!**********************************
      !
      !
      !
!***********************************

      module InitFromRandMod
      contains
      subroutine InitFromRand(FRC,time)

      use FORTRAN_RESCU_CALCULATION_TYPE
      implicit none

      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize
      time    = 0.0D0
      if (mpirank .EQ. 0) then
              write(*,*) 'Generating random subspace:'
      end if

      ispin = FRC%spin%ispin
      nkpt  = size(FRC%kpoint%ikdirect,1)
      allocate(kdir(kdir(nkpt,3)))
      kdir  = FRC%kpoint%ikdirect
      nvec  = FRC%domain%cgridn
      nband = FRC%eigensolver%nband
      cn    = product(nvec)
      nx    = nvec(1)
      ny    = nvec(2)
      nz    = nvec(3)
      allocate(xtmp(nx))
      allocate(ytmp(ny))
      allocate(ztmp(nz))
      forall(ii=1:nx)
              xtmp(ii) = dble(ii)/dble(nx)
      end forall
      forall(ii=1:ny)
              ytmp(ii) = dble(ii)/dble(ny)
      end forall
      forall(ii=1:nz)
              ztmp(ii) = dble(ii)/dble(nz)
      end forall
      call ndgrid(xtmp,ytmp,ztmp,xt,yt,zt)
      call initdistArray(cn,nband,ceiling(dble(cn)/dble(mpisize)),1,mpisize,1,vs)
      call GetGlobalInd(mpirank,vs,iloc,jloc)
      forall()
              xtmp(ii) = xt(iloc(ii))
              ytmp(ii) = yt(iloc(ii))
              ztmp(ii) = zt(iloc(ii))
      end forall
      call UpdateVeff(FRC,FRC%potential%veffin(1)%vata,FRC%potential%vps,veff)
      call distmat_allgather(FRC,veff,FRC%potential%vloc)

      do kk = 1,nkpt,1
          kpt = kdir(kk,:)
          call GetBorb(FRC,kpt)
          do ss = 1,nspin,1
              call initDistArray(cn,nband,ceiling(dble(cn)/dble(mpisize)),1,mpisize,1,vs)
              call GetGlobalInd(mpirank,vs,iloc,jloc)
              allocate(vs%vata(size(iloc),size(jloc)))
              call system_clock(time1,crate1,)
              do ii = 
                  call random_number(tt)
              end do
              do jj = 1,size(jloc),1
              do ii = 1,size(iloc),1
                  call random_number(tt)
                  vs%vata(ii,jj) = tt
              end do
              end do
              call GenHamFun

              if (any(kpt .NE. 0.0D0)) then
                      phase = exp(-im*(xtmp*kpt(1)+ytmp*kpt(2)+ztmp*kpt(3)))
                      forall(ii=1:size(jloc))
                              vs%vata(:,ii) = vs%vata(:,ii)*phase
                      end forall
              end if
              if (trim(FRC%eigensolver%init) .EQ. 'rr') then
                  if (ispin .EQ. 1 .OR. ispin .EQ. 2) then
                      call projectHamiltonian(FRC,vs,XHX,XSX)
                  else if (ispin .EQ. 4) then
                      call projectHamiltonianSOC(FRC,vs,vs,XHX,XSX)
                  end if
                  call diag_proj(FRC,XHX,XSX,any(kpt.NE.0.0D0),ZZ,nrg)
                  call calcev(FRC,vs,ZZ)
              else
                  call ModBCDist(FRC,vs,1,1,1,mpisize,vstmp)
                  call getGlobalInd(mpirank,vstmp,iloc,jloc)
                  do ii = 1,size(jloc),1
                      wv(1,:) = vstmp%vata(:,ii)
                      call GenHamFun(vstmp%vata,)
                      vw(:,1) = vstmp%vata(:,ii)
                      ntgtmp  = matmul(wv,vw)
                  end do
                  if (mpistat) then
                          call MPI_Allreduce_sum(nrg)
                  end if
              end if
              FRC%energy%ksnrg(:,kk,ss) = nrg
              FRC%psi(kk,ss)            = vs
          end do
      end do

      if (mpirank .EQ. 0) then
              write(*,*) 'time'
      end if

      return
      end subroutine InitFromRand
      end module InitFromRandMod

