! setdiff.f90
    
!*******************************************************************
    !
    !
    !
!*******************************************************************
    
    module setdiff_module
    contains
    subroutine setdiff(A,B,inde)
    
    implicit none
    
    ! input variables
    integer, allocatable :: A(:), B(:)
    
    ! temporary variables
    integer, allocatable :: Atmp1(:)!, Atmp2(:), indetmp(:)
    integer              :: nA, nB, mtmp, ntmp, ii!, jj
    
    ! output variables
    integer, allocatable :: inde(:)
    
    ! body of this fucntion
    nA = size(A)
    nB = size(B)
    
    allocate(Atmp1(nA))
    Atmp1 = 0
    mtmp = count(A == 0)
    if (mtmp == 0) then
        ntmp  = 0
        do ii = 1,nA,1
            if (all(B /= A(ii))) then
                ntmp = ntmp+1
                Atmp1(ntmp) = A(ii)
            end if
        end do
    else
        ntmp = 0
        if (all(B /= 0)) then
            ntmp = ntmp+1
            Atmp1(1) = 0
        end if
        do ii = 1,nA,1
            if (all(B /= A(ii)) .and. A(ii) /= 0) then
                ntmp = ntmp+1
                Atmp1(ntmp) = A(ii)
            end if
        end do
    end if

    ntmp = count(Atmp1 /= 0)
    if (mtmp ==0) then
        allocate(inde(ntmp))
        forall(ii=1:ntmp)
            inde(ii) = Atmp1(ii)
        end forall
    else
        allocate(inde(ntmp+1))
        forall(ii=1:ntmp+1)
            inde(ii) = Atmp1(ii)
        end forall
    end if

    return
    end subroutine setdiff
    end module setdiff_module
