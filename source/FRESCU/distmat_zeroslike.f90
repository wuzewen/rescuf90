! distmat_zeroslike.f90

!***********************************************************************
      !
      ! Build an interface named distmat_zeroslike.
      ! Its function is generating an array B which has the same size
      ! with the input array A.
      ! 
      ! ######## case 01 ########
      ! In subroutine distmat_zeroslike_1D_real8,
      ! A : 1D, real
      ! B : 1D, real
      !
      ! ######## case 02 ########
      ! In subroutine distmat_zeroslike_1D_cmplx,
      ! A : 1D, complex
      ! B : 1D, complex
      !
      ! ######## case 03 ########
      ! In subroutine distmat_zeroslike_2D_real8,
      ! A : 2D, real
      ! B : 2D, real
      !
      ! ######## case 04 ########
      ! In subroutine distmat_zeroslike_2D_cmplx,
      ! A : 2D, complex
      ! B : 2D, complex
      !
      ! ######## case 05 ########
      ! In subroutine distmat_zeroslike_2D_cm2re,
      ! A : 2D, complex
      ! B : 2D, real
      !
!***********************************************************************

      module distmat_zeroslike_module

              use distmat_zeroslike_1D_real8_module
              use distmat_zeroslike_1D_cmplx_module
              use distmat_zeroslike_2D_real8_module
              use distmat_zeroslike_2D_cmplx_module
              use distmat_zeroslike_2D_cm2re_module

              implicit none

              interface distmat_zeroslike
                      module procedure distmat_zeroslike_1D_real8 
                      module procedure distmat_zeroslike_1D_cmplx
                      module procedure distmat_zeroslike_2D_real8
                      module procedure distmat_zeroslike_2D_cmplx
                      module procedure distmat_zeroslike_2D_cm2re
              end interface distmat_zeroslike

      end module distmat_zeroslike_module
