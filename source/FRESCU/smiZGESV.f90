! smiZGESV.f90

!*******************************************
      !
      !
      !
!**************************************************

      module smiZGESVMOD
      contains
      subroutine smiZGESV(N,NRHS,A,B,nprocs,MB,NB,nprow,npcol)

      implicit none

      integer :: N, NRHS, nprocs, MB, NB, nprow, npcol
      complex*16, allocatable :: A(), B()

      integer :: iam, ictxt, myrow, mycol

      iam = 0
      ictxt = 0
      myrow = 0
      mycol = 0
      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1

      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,B_np)
      B_np = 
      A_np = 
      
      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,info)
      call descinit(descB,N,N,MB,NB,izero,izero,ixtxt,B_np,info)

      call PZGESV(N,NRHS,A,ione,ione,descA,IPIV,B,ione,ione,descB,info)

      if (info .LT. 0 .AND. iam .EQ. 0) then
          write(*,*) 'Error'
      end if
      if (info .GT. 0 .AND. iam .EQ. 0) then
          write(*,*) 'Error'
      end if

      call blacs_gridexit(ictxt)

      return
      end subroutine smiZGESV
      end module smiZGESVMOD
