! uniqueRow.f90
    
!******************************************************************************
    !
    !
    !
!******************************************************************************
    
    module uniqueRow_module
    contains
    subroutine uniqueRow(Ain,Aout)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: Ain(:,:)
    
    ! temporary variables
    real*8, allocatable :: Atmp(:,:), Aintmp(:,:), Aouttmp(:,:), Attmp2(:,:)
    integer :: n, m, kk, ii, jj, ntmp
    logical :: issame
    
    ! output variables
    real*8, allocatable :: Aout(:,:)
    
    ! body of this function
    !write(*,*) "I'm here at the begining of uniqueRow."
    n   = size(Ain,1)
    m   = size(Ain,2)

    !allocate(Aintmp(m,n))
    !allocate(Aouttmp(m,n))
    !Aintmp = reshape(Ain,(/m,n/))
    !Aouttmp      = 0.0D0
    !Aouttmp(:,1) = Aintmp(:,1)
    !kk = 1
    !do ii = 2,n,1
    !    issame = .FALSE.
        !ntmp   =  1
    !    do jj = 1,kk,1
    !        if (all(Aintmp(:,ii) == Aouttmp(:,jj))) then
    !                issame = .TRUE.
    !                exit
    !        end if
    !    end do
    !    if (.not. issame) then
    !            kk = kk + 1
    !            Aouttmp(:,kk) = Aintmp(:,ii)
    !    end if
    !end do
    !allocate(Attmp2(m,kk))
    !forall(ii=1:kk)
    !        Attmp2(:,ii) = Aouttmp(:,ii)
    !end forall
    !allocate(Aout(kk,m))
    !Aout = reshape(Attmp2,(/kk,m/))
    
    allocate(Aout(1,m))
    Aout(1,:) = Ain(1,:)
    
    kk = 1
    do ii = 2,n,1
        ntmp = size(Aout,1)
        issame = .FALSE.
        do jj = 1,ntmp,1
            if (all(Ain(ii,:) == Aout(jj,:))) then
                issame = .TRUE.
            end if
        end do
        if (.not. issame) then
            kk = kk+1
            allocate(Atmp(ntmp,m))
            Atmp = Aout
            deallocate(Aout)
            allocate(Aout(kk,m))
            Aout(1:kk-1,:) = Atmp
            Aout(kk,:)     = Ain(ii,:)
            deallocate(Atmp)
        end if
    end do
    !write(*,*) "I'm here at the end of uniqueRow"
    
    return
    end subroutine uniqueRow
    end module uniqueRow_module
