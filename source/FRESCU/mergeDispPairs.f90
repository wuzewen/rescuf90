! mergeDispPairs.f90

!*************************************************************************************
      !
      !
      !
!*************************************************************************************

      module mergeDispPairsMod
      contains
      subroutine mergeDispPairs(FRC,CellT,vecT,cellA,vecA,wghtA,cellB,symSum)

      implicit none

      mpirank  = FRC%mpi%rank
      ncell    = size(cellA)
      nnao     = cellA(1)%m
      calcpara = nnao**2 .GT. 2**18 .OR. size(cellA) .EQ. 1
      smimp    = FRC%smi%mp
      sminp    = FRC%smi%np
      smimb    = FRC%smi%mb
      sminb    = FRC%smi%nb

      if (calcpara) then
          call InitDistArray(nnao,nnao,smimb,sminb,smimp,sminp,tmpT)
          call GetGlobalInd(mpirank,tmpT,ia,ja)
          allocate(tmpT%vata(size(ia),size(ja)))
          if (.NOT. allocated(cellB%vata)) then
              allocate(cellB%vata(size(cellA%vata,1),size(cellA%vata,2)))
              cellB = cellA
          end if
      else
          do ii = 1,size(cellA)
              call ModBCDist(FRC,cellA(ii),nnao,cellA(ii)%n,smimp,sminp)
          end do
          if (.NOT. allocated(cellB)) then
              allocate(cellB(size(cellA)))
              do ii = 1,size(cellA),1
                  allocate(cellB(ii)%vata(size(cellA(ii)%vata,1),size(cellA(ii)%vata,2)))
              end do
              cellB = cellA
          else
              do ii = 1,size(cellB),1
                  call ModBCDist(FRC,cellB(ii),nnao,cellB(ii)%n,smimp,sminp)
              end do
          end if

          do ii = 1,size(cellT),1
              if (allocated(cellT)) then
                  call ModBCDist(FRC,cellT(ii),nnao,nnao,smimp,sminp)
              end if
          end do
          call initDistArray(nnao,nnao,nnao,nnao,smimp,sminp,tmpT)
          call GetGlobalInd(mpirank,tmpT,ia,ja)
          allocate(tmpt%vata(size(ia),size(ja)))
      end if

      do ii = 1,ncell,1
          !if (symSum) then
          !    allocate(jrange(ii))
          !    forall(jj=1:ii)
          !            jrange(jj) = jj
          !    end forall
          !else
          !    allocate(jrange(ncell))
          !    forall(jj=1:ncell)
          !            jrange(jj) = jj
          !    end forall
          !end if
          if (symSum) then
                  ntmp = ii
          else
                  ntmp = ncell
          end if

          do jj = 1,ntmp,1
              dt = vecA(ii,:) - vecA(jj,:)
              call bsxfunEq(vecT, dt,ind1)
              call bsxfunEq(vecT,-dt,ind2)

              if (all(ind1 .EQ. 0) .OR. (symSum .AND. all(ind1 .EQ. ind2))) then
                  cellT(+1,1) = tmpT
                  vecT(+1,:)  = dt
                  ind1(+1)  = .TRUE.
              end if

              if (any(ind1) .NE. 0) then
                  do kk = 1,size(ind1),1
                      if (.NOT. allocated(cellT(ind1(kk)))) then
                          allocate(cellT(ind1(kk))%vata(size(tmpT%vata,1),size(tmpT%vata,2))
                          cellT(ind1(kk)) = tmpT
                      end if
                  end do
              
                  if (calcpara) then
                      call kbProj2Vnl(FRC,cellT,ind1,cellA(ii),cellB(jj),wghtA,)
                  else if (mpirank .EQ. 0) then
                      do kk = 1,size(ind1),1
                          call bsxfunTimes()
                          cellT(ind1(kk))%vata = cellT(ind1(kk))%vata+
                      end do
                  end if
              else if (any(ind2 .NE. 0) .AND. symSum) then
                  do kk = 1,size(ind2),1
                      if (.NOT. allocated(cellT(ind2(kk)))) then
                          allocate(cellT(ind2(kk))%vata(size(tmpT%vata,1),size(tmpT%vata,2))
                          cellT(ind2(kk)) = tmpT
                      end if
                  end do
                  if (calcpara) then
                      call kbProj2Vnl(FRC,cellT,ind2,cellA(ii),cellB(jj),wghtA,)
                  else if (mpirank .EQ. 0) then
                      do kk = 1,size(ind2),1
                          call bsxfunTimes()
                          cellT(ind2(kk))%vata = cellT(ind2(kk))%vata+
                      end do
                  end if
              end if
          end do
      end do

      if (.NOT. calcpara) then
          do ii = 1,size(cellT),1
              if (allocated(cellT(ii)%vata)) then
                  call ModBCDist(FRC,cellT(ii),smimb,sminb,smimp,sminp)
              end if
          end do
      end if

      return
      end subroutine mergeDispPairs
      end module mergeDispPairs


