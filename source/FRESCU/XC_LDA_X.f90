! XC_LDA_X.f90

! ********************************************************************
      ! This subroutine finds local exchange energy density and
      ! potential. It will be called in xPBE.f90.
      ! 
      ! Adapted by J.M.Soler from routine velect of Froyen's
      ! pseudopotential generation program. Madrid, Jan'97. Version 0.5.
      ! Modified by Lei Zhang using MATLAB, Novenber 2013. Version 0.1.
      ! Translated to FORTRAN version here by Zewen Wu, August 2018.
      !
      ! ###### INPUT #######
      ! IREL :: logical, relativistic-exchange switch (FALSE for no, TRUE
      ! for yes)
      ! NSP  :: integer, spin-polarizations (1 for unpolarized, 2
      ! for polarized)
      ! DS   :: double precision array (size(ngrids,NSP)), electron
      ! density
      ! ###### OUTPUT ######
      ! EX   :: double precision array (size(ngrids)), exchange energy
      ! density
      ! VX   :: double precision array (size(ngrids,NSP)), exchange
      ! potential
      ! 
      ! Units :: electrons/Bohr**3 for Densities, Hartrees for Energies

      module XC_LDA_X_module
      contains
      subroutine XC_LDA_X(IREL,NSP,DS,EX,VX)

      implicit none

      ! input variables
      logical :: IREL
      integer :: NSP
      real*8, allocatable :: DS(:,:)

      ! temporary variables
      integer :: npts, ntmp, nara, ii
      real*8  :: DENMIN, PFIVE, OPF, C014, pi, TRD, FTRD, TFTM, A0, ALP
      real*8, allocatable :: DENS(:), DStmp(:,:), D1(:), D2(:), D(:)
      real*8, allocatable :: Z(:), FZ(:), FZP(:), RS(:), VXP(:)
      real*8, allocatable :: EXP_VAR(:), BETA(:), SB(:), ALB(:)
      real*8, allocatable :: VXF(:), EXF(:)

      ! output variables
      real*8, allocatable :: EX(:), VX(:,:)

      ! body of this function

      !write(*,*) "in xc_lda_x, inputs ="
      !write(*,*) "ds ="
      !write(*,*)  ds


      DENMIN = 50.0D0*2.220446049250313D-16
      PFIVE  = 0.5D0
      OPF    = 1.5D0
      C014   = 0.014D0
      pi     = 3.1415926535897932385D0
      TRD    = 1.0D0/3.0D0
      FTRD   = 4.0D0*TRD
      TFTM   = 2.0D0**TRD - 2.0D0
      A0     = (4.0D0/(9.0D0*pi))**TRD
      ALP    = 2.0D0*TRD
      npts   = size(DS,1)
      nara   = size(DS,2)
      allocate(EX(npts))
      EX     = 0.0D0
      allocate(VX(npts,NSP))
      VX     = 0.0D0
      allocate(DENS(npts))
      DENS   = sum(DS,2)

      ntmp = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                  ntmp = ntmp+1
          end if
      end do

      allocate(DStmp(ntmp,nara))
      allocate(D(ntmp))
      allocate(Z(ntmp))
      allocate(FZ(ntmp))
      allocate(FZP(ntmp))
      
      ntmp = 0
      do ii = 1,npts,1
          if (DENS(ii) > DENMIN) then
                  ntmp          = ntmp+1
                  DStmp(ntmp,:) = DS(ii,:)
          end if
      end do

      if (NSP == 2) then
              allocate(D1(ntmp),D2(ntmp))
              D1  = DStmp(:,1)
              D2  = DStmp(:,2)
              do ii = 1,ntmp,1
                  if (D1(ii) < DENMIN) then
                          D1(ii) = DENMIN
                  end if
                  if (D2(ii) < DENMIN) then
                          D2(ii) = DENMIN
                  end if
              end do

              D   = D1+D2
              Z   = (D1-D2)/D
              FZ  = ((1.0D0+Z)**FTRD+(1.0D0-Z)**FTRD-2.0D0)/TFTM
              FZP = FTRD*((1.0D0+Z)**TRD - (1.0D0-Z)**TRD)/TFTM
      else
              D   = DStmp(:,1)
              do ii = 1,ntmp,1
                  if (D(ii) < DENMIN) then
                          D(ii) = DENMIN
                  end if
              end do
              Z   = 0.0D0
              FZ  = 0.0D0
              FZP = 0.0D0
      end if

      !write(*,*) "in xc_lda_x, part1"
      !write(*,*) "D = "
      !write(*,*)  D
      !write(*,*) "Z ="
      !write(*,*)  Z
      !write(*,*) "FZ = "
      !write(*,*)  FZ
      !write(*,*) "FZP ="
      !write(*,*)  FZP

      allocate(RS(ntmp))
      allocate(VXP(ntmp))
      allocate(EXP_VAR(ntmp))

      RS      =  (3.0D0/(4.0D0*pi*D))**TRD
      VXP     = -(3.0D0*ALP/(2.0D0*pi*A0*RS))
      EXP_VAR = 3.0D0*VXP/4.0D0


      !write(*,*) "in xc_lda_x, part2"
      !write(*,*) "RS ="
      !write(*,*)  RS
      !write(*,*) "VXP ="
      !write(*,*)  VXP
      !write(*,*) "EXP_VAR ="
      !write(*,*)  EXP_VAR

      allocate(BETA(ntmp))
      allocate(SB(ntmp))
      allocate(ALB(ntmp))

      if (IREL) then
              BETA = C014/RS
              SB   = sqrt(1.0D0+BETA*BETA)
              ALB  = log(BETA+SB)
              VXP  = VXP*(-PFIVE+OPF*ALB/(BETA*SB))
              EXP_VAR = EXP_VAR*(1.0D0-OPF*((BETA*SB-ALB)/(BETA**2))**2)
      end if

      allocate(VXF(ntmp))
      allocate(EXF(ntmp))

      VXF = (2.0D0**TRD)*VXP
      EXF = (2.0D0**TRD)*EXP_VAR

      !write(*,*) "in xc_lda_x, part3"
      !write(*,*) "VXF ="
      !write(*,*)  VXF
      !write(*,*) "EXF ="
      !write(*,*)  EXF

      if (NSP == 2) then
              ntmp = 0
              do ii = 1,npts,1
                  if (DENS(ii) > DENMIN) then
                          ntmp = ntmp+1
                          VX(ii,1) = VXP(ntmp)  &
                                   + FZ(ntmp)*(VXF(ntmp)-VXP(ntmp)) &
                  +(1.0D0-Z(ntmp))*FZP(ntmp)*(EXF(ntmp)-EXP_VAR(ntmp))
                          VX(ii,2) = VXP(ntmp)  &
                                   + FZ(ntmp)*(VXF(ntmp)-VXP(ntmp)) &
                  -(1.0D0+Z(ntmp))*FZP(ntmp)*(EXF(ntmp)-EXP_VAR(ntmp))
                          EX(ii)   = EXP_VAR(ntmp)+FZ(ntmp)*(EXF(ntmp)-EXP_VAR(ntmp))
                  end if
              end do
      else
              ntmp = 0
              do ii = 1,npts,1
                  if (DENS(ii) > DENMIN) then
                          ntmp = ntmp+1
                          VX(ii,1) = VXP(ntmp)
                          EX(ii)   = EXP_VAR(ntmp)
                  end if
              end do
      end if

      return
      end subroutine XC_LDA_X
      end module XC_LDA_X_module

