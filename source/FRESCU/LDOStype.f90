! LDOStype.f90
    
!*****************
    !
    !
    !
!**********************
    
    module LDOStype_module
    
    type :: LDOStype
        logical              :: status
        real*8               :: energy
        real*8, allocatable  :: energyI(:)
        real*8, allocatable  :: energyK(:)
    end type LDOStype
    
    end module LDOStype_module