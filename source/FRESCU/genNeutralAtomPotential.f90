!  genNeutralAtomPotential.f90 
!****************************************************************************
!
!  PROGRAM: genNeutralAtomPotential
!
!  PURPOSE: Get Neutral Atom Potential from $Element.bas file.
!  
!  Some Information:
!  1. There are two method to get Neutral Atom Potential, depending on 
!     whether FRC%option%initParaReal is true or not.
!  2. If FRC%option%initParaReal is true, Neutral Atom Potential on real
!     space grids will be interplated using function InterpSph2Cart.
!  3. If FRC%option%initParaReal is false, Neutral Atom Potential on real
!     space grids will be interplated using function InterpRdist2Cart.
!  4. All the source data are from LCAO basis.
!  5. If FRC%option%initParaReal is true, parallelezation will process on
!     real space grids.
!  6. If FRC%option%initParaReal is false, parallelezation will process
!     atom by atom.
!
!****************************************************************************

    module genNeutralAtomPotential_module
    contains
    subroutine genNeutralAtomPotential(FRC)

    !include 'mpif.h'

    use FORTRAN_RESCU_CALCULATION_TYPE
    use getGridPointCoord_module
    use interpSph2Cart_module
    use VataMN_1D_module
    use InterpRdist2Cart_module
    use MPI_Reduce_sum_R1D_module
    use ModBCDist_module
    use RealVata_2D_module
    
    include 'mpif.h'

    !implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    integer                     :: mpisize, mpirank, fgridn(3), ntmp, ii
    integer                     :: fn, natom, Etype, bc(3), root, ierr
    integer, allocatable        :: indep(:,:), ranks(:)
    logical                     :: mpistat
    real*8                      :: lvec(3,3), pos(1,3), pi
    real*8, allocatable         :: GridCoord(:,:), vnaDatatmp(:), vnaDataReal(:,:)
    real*8, allocatable         :: nonsense(:,:), vnarec(:)
    type(VataMN_1D)             :: vna
    complex*16                  :: im
    type(RealVata_2D),allocatable :: vnavatatmp(:,:)
    ! output variables
    

    ! Body of genNeutralAtomPotential
    pi                        = 3.1415926535897932385D0
    im                        = (0.0D0,1.0D0)
    mpistat                   = FRC%mpi%status
    mpirank                   = FRC%mpi%rank
    mpisize                   = FRC%mpi%mpisize
    fgridn                    = FRC%domain%fgridn
    natom                     = FRC%atom%numberOfAtom
    lvec                      = FRC%domain%latvec
    fn                        = product(fgridn)
    FRC%option%initParaReal   = .TRUE.
    FRC%potential%fourierinit = .FALSE.

    !call MPI_INIT(ierr)
    !call MPI_COMM_RANK(MPI_COMM_WORLD,mpirank,ierr)
    !call MPI_COMM_SIZE(MPI_COMM_WORLD,mpisize,ierr)
    !FRC%mpi%mpisize = mpisize
    !FRC%mpi%rank    = mpirank
    !FRC%mpi%status  = .TRUE.

    if (FRC%potential%fourierinit .or. FRC%option%initParaReal) then
        !allocate(GridCoord(fn,3))
        ntmp = ceiling(dble(fn)/dble(mpisize))
        call GetGridPointCoord(FRC,fgridn,ntmp,GridCoord,indep)
        
        !write(*,*) "in gvna. GridCoord =", size(GridCoord,1)
        !do ii = 1,size(GridCoord,1),1
        !write(*,*)  GridCoord(3373,:)
        !end do
        !write(*,*) "end data"
        !write(*,*) 'in genNeu, ntmp =', ntmp
        call initDistArray_1D(fn,1,ntmp,1,mpisize,1,vna)
        allocate(vna%vata(size(GridCoord,1)))
        allocate(vnaDataReal(size(vna%vata),1))
    end if
    
    !allocate(vna%vata(fn))
    !allocate(vnaDatatmp(fn))
    
    if (FRC%option%initParaReal) then
            !write(*,*) "Getting Neutral Atom Potential with ParaReal."
        vna%vata = 0.0D0
        do ii = 1,natom,1
            Etype    = FRC%atom%element(ii)
            pos(1,:) = FRC%atom%XYZ(ii,:)
            bc       = 1
            !allocate(vnaDataReal(size(vna%vata),1))
            
            call InterpSph2Cart(FRC%ElementData(Etype)%Vna%rrData,&
                                FRC%ElementData(Etype)%Vna%vvData,&
                                pos,-1,GridCoord,lvec,bc,vnaDataReal)
            !ntmp = size(vnaDatatmp,1)
            !allocate(vnatt(fn))
            !forall(jj=1:ntmp)
            !    vnatt(jj) = vnaDatatmp(jj)
            !end forall
            !write(*,*) "vnaDataReal = ", vnaDataReal(2745,1)
            
            vna%vata = vna%vata + vnaDataReal(:,1) !vnatt
            !deallocate(vnaDataReal)
        end do
        deallocate(vnaDataReal)
    else if (.not. FRC%potential%fourierinit) then
        allocate(vna%vata(fn))
        vna%vata = 0.0D0
        do ii = mpirank+1,natom,mpisize
        !do ii = 1,natom,1
            Etype    = FRC%atom%element(ii)
            pos(1,:) = FRC%atom%XYZ(ii,:)
            call InterpRdist2Cart(FRC%ElementData(Etype)%Vna%rrData,&
                                  FRC%ElementData(Etype)%Vna%vvData,&
                                  pos,-1,lvec,fgridn,.FALSE.,vnavatatmp,nonsense,vnaDatatmp)
            vna%vata = vna%vata + vnaDatatmp
            deallocate(vnaDatatmp,nonsense,vnavatatmp)
        end do
        !mpistat = .TRUE.
        if (mpistat) then
            root = 0
            allocate(ranks(mpisize))
            forall(ii=1:mpisize)
                ranks(ii) = ii-1
            end forall
            allocate(vnarec(size(vna%vata)))
            vnarec = 0.0D0
            call MPI_Reduce_sum_R1D(vna%vata,ranks,root,vnarec)
            vna%vata = vnarec
            deallocate(vnarec,ranks)
        end if
        
        !call MPI_Finalize(ierr)
        !if (mpirank /=0) then
        !    vna%dataA = 0
        !end if
        
        call InitDistArray_1D(fn,1,fn,1,1,mpisize,vna)
        !call ModBCDist_1D(FRC,fn/mpisize,1,mpisize,1,vna)
    else
        !####################################
        !
        ! This part will never be used.
        !
        !####################################
        
        write(*,*) "Error in genNeutralAtomPotential. FRC%potential%fourierinit should be true. This part may never be avalable."
        stop
        
        !fgridn = FRC%domain%fgridn
        !call inversion(lvec,bvec)
        !bvec = 2*pi*transpose(bvec)
        !call fftFreq(fgridn,bvec,ku,kv,kw)
        !allocate(inde())
        !call GetGlobalInd(mpirank,vna,inde)
        !ntmp = size(inde)
        !allocate(kuvw(ntmp,3))
        !forall(ii=1:ntmp)
        !    kuvw(ii,1)  = ku(inde(ii))
        !    kuvw(ii,2)  = kv(inde(ii))
        !    kuvw(ii,3)  = kw(inde(ii))
        !end forall
        
        !nelem      = FRC%atom%numberOfElement
        !vna%DataA  = 0
        !vnatmp     = vna
        
        !do ii = 1,nelem,1
        !    allocate(rrData(FRC%SOB(ii)%SizeOfVnarrData))
        !    allocate(vvData(FRC%SOB(ii)%SizeOfVnavvData))
        !    rrData = FRC%ElementData(ii)%Vna%rrData
        !    vvData = FRC%ElementData(ii)%Vna%vvData
        !    call InterpSph2Cart(rrData,vvData,(/0,0,0/),-1,GridCoord,lvec,vnaDatatmp)
        !    if (mpistat) then
        !        call ModBCDist(FRC,vnatmp,fn,1,mpisize,1,vnatmp)
        !    end if
            
        !    if (mpirank == 0) then
        !        allocate(tmp(fgridn(1),fgridn(2),fgridn(3)))
        !        tmp = reshape(vnatmp%dataA,(/fgridn(1),fgridn(2),fgridn(3)/))
        !        call fftn(tmp,vnatmp%dataA)
        !    end if
            
        !    if (mpistat) then
        !        call ModBCDist(FRC,vnatmp,fn/mpisize,1,mpisize,1,vnatmp)
        !    end if
        !    
        !    ntmp = 0
        !    do jj = 1,nelem,1
        !        if (FRC%atom%element(jj) == ii) then
        !            ntmp = ntmp+1
        !        end if
        !    end do
        !    allocate(itmp(ntmp))
        !    ntmp = 0
        !    do jj = 1,nelem,1
        !        if (FRC%atom%element(jj) == ii) then
        !            ntmp = ntmp+1
        !            itmp(ntmp) = jj
        !        end if
        !    end do
        !    
        !    do jj = 1,ntmp,1
        !        pos(1,:)  = FRC%atom%XYZ(itmp(jj),:)
        !        vna%dataA = vna%dataA + exp(-im*(matmul(kuvw,transpose(pos))))*vnatmp%dataA
        !    end do
        !    deallocate(rrData,vvData)
        !end do
        
        !call ModBCDist(FRC,vna,fn,1,1,mpisize)
        
        !if (mpirank == 0) then
        !    vna%dataA(1) = 0
        !    allocate(pptmp(fgridn(1),fgridn(2),fgridn(3)))
        !    call ifftn(reshape(vna%dataA,(/fgridn(1),fgridn(2),fgridn(3)/)),pptmp)
        !    vna%dataA    = real(pptmp)
        !end if
        
        !call ModBCDist(FRC,vna,fn/mpisize,1,mpisize,1,vna)
    end if
    FRC%potential%vna = vna
    deallocate(vna%vata)

    !write(*,*) "FRC%potential%vna%vata(777) ="
    !write(*,*)  FRC%potential%vna%vata(777)
    !call MPI_FINALIZE(ierr)
    !FRC%mpi%status = .FALSE.

    return
    end subroutine genNeutralAtomPotential
    end module genNeutralAtomPotential_module

