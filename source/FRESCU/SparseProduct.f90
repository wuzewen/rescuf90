! SparseProduct.f90

!*************************************************************************
      !
      !
      !
!*************************************************************************

      module SparseProduct_module
      contains
      subroutine SparseProduct(SparseA,B,conj,Cout)

      use SparseArray_2D_Cmplx_module

      implicit none

      ! input variable 
      type(SparseArray_2D_Cmplx) :: SparseA
      complex*16, allocatable    :: B(:,:)
      logical                    :: conj

      ! temporary variables
      integer :: nA(2), nB(2), nnz, ii, jj
      integer, allocatable :: ind1(:,:), ind2(:,:)
      complex*16, allocatable :: valu(:,:)

      ! output variables
      complex*16, allocatable    :: Cout(:,:)

      ! body of this function
      nA(1) = SparseA%SIZE1
      nA(2) = SparseA%SIZE2
      nB(1) = size(B,1)
      nB(2) = size(B,2)
      nnz   = SparseA%N_nonzero

      allocate(ind1(nnz,nB(2)))
      allocate(ind2(nnz,nB(2)))
      allocate(valu(nnz,nB(2)))

      if (.not. conj) then
              forall(ii=1:nnz,jj=1:nB(2))
                      ind1(ii,jj) = SparseA%Index1(ii)
                      ind2(ii,jj) = jj
                      valu(ii,jj) = SparseA%ValueN(ii)*B(SparseA%Index2(ii),jj)
              end forall
              allocate(Cout(SparseA%SIZE1,nB(2)))
              Cout = (0.0D0,0.0D0)
              forall(jj=1:nB(2),ii=1:nnz)
                      Cout(ind1(ii,jj),ind2(ii,jj)) = Cout(ind1(ii,jj),ind2(ii,jj))+ valu(ii,jj)
              end forall
      else
              forall(ii=1:nnz,jj=1:nB(2))
                      ind1(ii,jj) = SparseA%Index2(ii)
                      ind2(ii,jj) = jj
                      valu(ii,jj) = conjg(SparseA%ValueN(ii))*B(SparseA%Index1(ii),jj)
              end forall
              allocate(Cout(SparseA%SIZE2,nB(2)))
              Cout = (0.0D0,0.0D0)
              forall(jj=1:nB(2),ii=1:nnz)
                      Cout(ind1(ii,jj),ind2(ii,jj)) = Cout(ind1(ii,jj),ind2(ii,jj))+ valu(ii,jj)
              end forall
      end if

      return
      end subroutine SparseProduct
      end module SparseProduct_module
