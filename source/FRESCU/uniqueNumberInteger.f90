! uniqueNumberInteger.f90

!**********************************************************************
      !
      !
      !
!**********************************************************************

      module uniqueNumberInteger_module
      contains
      subroutine uniqueNumberInteger(inde,N)

      implicit none

      ! input variables
      integer, allocatable :: inde(:,:)

      ! temporary variables
      logical :: issame
      integer :: ss1, ss2, ii, jj
      integer, allocatable :: tmp1(:,:)

      ! output variables
      integer :: N

      ! body of this function
      ss1 = size(inde,1)
      ss2 = size(inde,2)

      allocate(tmp1(ss1,ss2))

      if (ss1 == 1) then
              N = 1
              return
      end if

      tmp1(1,:) = inde(1,:)

      N = 1
      do ii = 2,ss1,1
          issame = .FALSE.
          do jj = 1,ii-1,1
              if (all(inde(ii,:) == tmp1(jj,:))) then
                      issame = .TRUE.
                      exit
              end if
          end do
          if (.not. issame) then
                  N = N+1
          end if
      end do

      return
      end subroutine uniqueNumberInteger
      end module uniqueNumberInteger_module


