! mixingMetricWrapper.f90

!***************************************
      !
      !
      !
!******************************************

      module mixingMetricWrapperMod
      contains
      subroutine mixingMetricWrapper(fun,FRC,distTmp,dat)

      use

      implicit none

      mpisize = FRC%mpi%mpisize
      forall(ii=1:3)
              bc(ii) = FRC%domain%boundary(ii) .EQ. 1
      end forall
      invJ0 = FRC%mixing%invJ0
      ipScheme = FRC%mixing%ipScheme
      tmp      = distTmp
      tmp%vata = reshape(dat,(/size(distTmp%vata,1),size(distTmp%vata,2)/))
      call ModBCDist(FRC,tmp,1,1,1,mpisize,tmp)
      if (tmp%m .EQ. product(FRC%domain%cgridn)) then
              tn = product(FRC%domain%cgridn)
      else if (tmp%m .EQ. product(FRC%domain%fgridn)) then
              tn = product(FRC%domain%fgridn)
      else if (trim(invJ0) .EQ. 'dm' .OR. & 
                     trim(ipScheme).EQ. 'nonuniform') then
              nao = size(FRC%LCAO%parameters%evec)
              tn = nao
      else
              write(*,*) 'Error'
              stop
      end if

      do ii = 1,size(tmp%vata),1
          tmpdata = reshape(tmp%vata(:,ii),tn)
          if (trim(fun) .EQ. 'kerker') then
                  call kerkerPrecond(FRC,tmpdata,bc)
          else if (trim(fun) .EQ. 'nonuniform') then
                  call kresseMetric(FRC,tmpdata,bc)
          else if (trim(fun) .EQ. 'dm') then
                  call dmPrecond(FRC,tmpdata,bc)
          else if (trim(fun) .EQ. 'linlin') then
                  call linlinPrecond(FRC,tmpdata,bc)
          else if (trim(fun) .EQ. 'kerker2D') then
                  call kerker2DPrecond(FRC,tmpdata,bc)
          end if
          tmp%vata(:,ii) = reshape(tmpdata,(//))
      end do
      call ModBCDist(FRC,tmp,distTmp%mblock,distTmp%nblock,distTmp%mproc,&
              distTmp%nproc,tmp)
      dat = tmp%vata

      return
      end subroutine mixingMetricWrapper
      end module mixingMetricWrapperMod

