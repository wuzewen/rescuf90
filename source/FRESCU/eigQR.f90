! eigQR.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module eigQR_module
    contains
    subroutine eigQR(A,eps,U,V,Vec,JT)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: A(:,:)
    real*8              :: eps
    
    ! temporary variables
    integer :: M, N, L, IT, jj, kk, ii
    real*8  :: W, B, C, X, Y, XY, P, Q, R, E, F, Z, G, S
    
    ! output variables
    real*8, allocatable :: U(:), V(:), Vec(:,:)
    integer             :: JT
    
    ! body of this function
    JT  = 1
    M   = size(A,1)
    N   = size(A,2)
    !Vec = Vec
    allocate(Vec(M,N))
    do while(M /= 0)
        do while(L == M-1)
            if (M == 0) then
                return
            end if
            do while (L == M)
                if (M == 0) then
                    return
                end if
    
                L   = M+1
                L   = L-1
    
                do while(abs(Vec(L,L-1)) > eps*(abs(Vec(L-1,L-1))+abs(Vec(L,L))) .and. L > 1)
                    L = L-1
                end do
    
                !if (L == M) then
                U(M) = Vec(M,M)
                V(M) = 0.0
                M    = M-1
                IT   = 0
                !if (M == 0) then
                !    return
                !end if
            end do
    
            !if (L == M-1) then
            B = -(Vec(M,M)+Vec(M-1,M-1))
            C =   Vec(M,M)*Vec(M-1,M-1)-Vec(M,M-1)*Vec(M-1,M)
            W =   B*B-4*C
            Y =   sqrt(abs(W))
        
            if (W > 0.0) then
                XY = 1.0
            
                if (B < 0.0) then
                    XY = -1.0
                end if
            
                U(M)   = (-B-XY*Y)/2.0
                U(M-1) =   C/U(M)
                V(M)   =   0.0
                V(M-1) =   0.0
            else
                U(M)   =  -B/2.0
                U(M-1) =   U(M)
                V(M)   =   Y/2.0
                V(M-1) =  -V(M)
            end if
        
            M  = M-2
            IT = 0
        
            !if (M == 0) then
            !    return
            !end if
        end do
    
        if (IT >= 60) then
            write(*,*) "Error in eigQR.f90. FVeciled."
            JT = 0
            return
        end if
    
        IT = IT+1
    
        do jj = L+2,M,1
            Vec(jj,jj-2) = 0.0
        end do
        do jj = L+3,M,1
            Vec(jj,jj-3) = 0.0
        end do
    
        do kk = L,M-1,1
            if (KK == L) then
                P = Vec(kk,kk-1)
                Q = Vec(kk+1,kk-1)
                R = 0.0
                if (kk /= M-1) then
                    R = Vec(kk+2,kk-1)
                end if
            else
                X = Vec(M,M)+Vec(M-1,M-1)
                Y = Vec(M-1,M-1)*Vec(M,M)-Vec(M-1,M)*Vec(M,M-1)
	            P = Vec(L,L)*(Vec(L,L)-X)+Vec(L,L+1)*Vec(L+1,L)+Y
	            Q = Vec(L+1,L)*(Vec(L,L)+Vec(L+1,L+1)-X)
	            R = Vec(L+1,L)*Vec(L+2,L+1)
            end if
        
            if (abs(P)+abs(Q)+abs(R) /= 0.0) then
                XY = 1.0
                if (P < 0.0) then
                    XY = -1.0
                end if
                S = XY*SQRT(P*P+Q*Q+R*R)
                if (kk /= L) then
                    Vec(kk,kk-1) = -s
                end if
            
                E = -Q/S
	            F = -R/S
	            X = -P/S
	            Y = -X-F*R/(P+S)
	            G =  E*R/(P+S)
	            Z = -X-E*Q/(P+S)
            
                do jj = kk,M
                    P = X*Vec(kk,jj)+E*Vec(kk+1,jj)
	                Q = E*Vec(kk,jj)+Y*Vec(kk+1,jj)
	                R = F*Vec(kk,jj)+G*Vec(kk+1,jj)
                
                    if (kk /= M-1) then
                        P          = P+F*Vec(kk+2,jj)
	                    Q          = Q+G*Vec(kk+2,jj)
	                    R          = R+Z*Vec(kk+2,jj)
	                    Vec(kk+2,jj) = R
                    end if
                    Vec(kk+1,jj) = Q
	                Vec(kk,jj)   = P
                end do
            
                jj = kk+3
            
                if (jj >= M) then
                    jj = M
                end if
            
                do ii = L,jj,1
                    P = X*Vec(ii,kk)+E*Vec(ii,kk+1)
	                Q = E*Vec(ii,kk)+Y*Vec(ii,kk+1)
	                R = F*Vec(ii,kk)+G*Vec(ii,kk+1)
                
                    if (kk /= M-1) then
                        P          = P+F*Vec(ii,kk+2)
	                    Q          = Q+G*Vec(ii,kk+2)
	                    R          = R+Z*Vec(ii,kk+2)
	                    Vec(ii,kk+2) = R
                    end if
                
                    Vec(ii,kk+1) = Q
                    Vec(ii,kk)   = P
                end do
            end if
        end do
    end do
    
    return
    end subroutine eigQR
    end module eigQR_module