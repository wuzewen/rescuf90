! uniqueInteger.f90
    
!*********************************************
    !
    !
    !
!*********************************************
    
    module uniqueInteger_module
    contains
    subroutine uniqueInteger(tx,ut)
    
    implicit none
    
    ! input variables
    integer, allocatable :: tx(:)
    
    ! temporary variables
    integer              :: n, ii, jj, ntmp, maxv
    integer, allocatable :: txtmp(:), uttmp(:)
    integer, allocatable :: loca(:)
    
    ! output variables
    integer, allocatable :: ut(:)
    
    ! body of this function
    n = size(tx)
    if (n == 1) then
        allocate(ut(1))
        ut = tx
        return
    end if
    
    allocate(txtmp(n),uttmp(n))
    uttmp    = tx
    maxv     = maxval(tx)
    txtmp(1) = minval(tx)

    do ii = 2,n,1
        do jj = 1,n,1
            if (uttmp(jj) == txtmp(ii-1)) then
                    uttmp(jj) = 7 + maxv
            end if
        end do
        txtmp(ii) = minval(uttmp)
        if (txtmp(ii) == maxv) then
                exit
        end if
    end do

    allocate(ut(ii))
    forall(jj=1:ii)
            ut(jj) = txtmp(jj)
    end forall

    !ntmp = 1
    !do ii = 2,n,1
    !    allocate(txtmp(ii-1))
    !    forall(jj=1:ii-1)
    !        txtmp(jj) = tx(jj)
    !    end forall
    !    if (any(txtmp == tx(ii))) then
    !        ntmp = ntmp+0
    !    else
    !        ntmp = ntmp+1
    !    end if
    !    deallocate(txtmp)
    !end do
    !allocate(ut(ntmp),uttmp(ntmp))
   ! 
   ! ntmp  = 1
    !ut(1) = tx(1)
    !do ii = 2,n,1
    !    allocate(txtmp(ii-1))
    !    forall(jj=1:ii-1)
    !        txtmp(jj) = tx(jj)
    !    end forall
    !    if (any(txtmp == tx(ii))) then
    !        ntmp = ntmp+0
    !    else
    !        ntmp     = ntmp+1
    !        ut(ntmp) = tx(ii)
    !    end if
    !    deallocate(txtmp)
    !end do
    
    !uttmp = ut
    
    !maxv     = maxval(uttmp)
    !ut(ntmp) = maxv
    !allocate(loca(1))
    !do ii = 1,ntmp-1,1
    !    loca        = minloc(uttmp)
    !    ut(ii)      = minval(uttmp)
    !    uttmp(loca) = uttmp(loca)+maxv
    !end do
    
    return
    end subroutine uniqueInteger
    end module uniqueInteger_module
