! GETGLOBALIND.f90

!*****************************************************************************
      !
      !
      !
!**************************************************************************************

      module GETGLOBALINDMOD

      interface GETGLOBALIND
              module procedure GetGlobalInd_1D
              module procedure GetGlobalInd_2D
              module procedure GetGlobalInd_CMPLX_1D
              module procedure GetGlobalInd_CMPLX_2D
      end interface GETGLOBALIND

      end module GETGLOBALINDMOD
