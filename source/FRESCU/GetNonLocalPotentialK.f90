!  GetNonLocalPotentialK.f90 
!
!  FUNCTIONS:
!  GetNonLocalPotentialK - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: GetNonLocalPotentialK
!
!  PURPOSE: This function is used to get Non-local Potential.
!
!****************************************************************************

    module GetNonLocalPotentialK_module
    contains
    subroutine GetNonLocalPotentialK(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use RealVata_2D_module
    use getKBOrbitalinfo_module
    use cellInRange_module
    use flipud_module
    use interprDist2Cart_module
    use interprDist2Cart_Sparse_module
    use ismember_module
    use ismemberRow_module
    use cellfunnnz_module
    use cellfunnnz2_module
    use cat_module
    use reduceoper_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical :: mpistat, useCell, kbsparse
    integer :: mpirank, mpisize, fgridn(3), fn, natom, nproj, ntv, ntv2, kk, elem!, gg, bb
    integer :: lcut, ncut, ntmp, nlnu, ii, jj, itmp, ll, nindtmp, pp, nevec, ntmpxxx!, zz, ww
    integer :: spnnz, typeE, nkbcell, nnzLoc, qq, ntmp1, ntmp2, ntmp3, ntmp4, ntmp5!,nnz
    real*8  :: avec(3,3), pos(1,3), rad
    real*8, allocatable   :: evec(:), rorb(:), oorb(:), xyz(:,:), tvectmp(:,:)
    real*8, allocatable   :: KBEnergy(:), tvtmp(:,:) ! rrdata(:), vvdata(:)
    real*8, allocatable   :: avectmp(:,:), vatatmp1(:,:), vatatmp2(:,:), tvec(:,:), out3(:)
    integer, allocatable  :: lnu(:), nnu(:), inde(:), indtmp(:), indLoc(:)
    integer, allocatable  :: indLoctmp(:), tvecp(:,:)
    logical, allocatable  :: isLoc(:), isLoctmp(:)
    type(RealVata_2D), allocatable :: kbcell(:,:), kbcelltmp(:)
    type(RealVata_2D)              :: kbcell2!, kbcell3
    !real*8, allocatable :: kbtmp(:,:)
    !type(VnlType), allocatable   :: dataVnl(:)
    type(RealVata_2D), allocatable :: kbtmp(:,:)
    
    ! output variables

    ! Body of GetNonLocalPotentialK
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    
    call GetKBOrbitalInfo(FRC)
    fgridn  = FRC%domain%fgridn
    avec    = FRC%domain%latvec
    nproj   = size(FRC%potential%vnl%evec)
    allocate(evec(nproj),rorb(nproj),oorb(nproj))
    evec    = FRC%potential%vnl%evec
    nevec   = size(evec)
    rorb    = FRC%potential%vnl%Rorb
    oorb    = FRC%potential%vnl%Oorb
    natom   = FRC%atom%numberOfAtom
    fn      = product(fgridn)
    kbsparse = .FALSE.
    
    allocate(KBEnergy(nproj))
    KBEnergy = 0.0D0
    
    if (any(FRC%kpoint%kdirect /= 0) .or. trim(FRC%info%calculationType) == trim("dfpt")) then
        useCell = .TRUE.
    else
        useCell = .FALSE.
    end if
    
    rad  = maxval(rorb)
    allocate(xyz(natom,3))
    xyz  = FRC%atom%XYZ
    !allocate(tvec(ntv))
    allocate(avectmp(3,3))
    avectmp = avec
    call cellInRange(avectmp,xyz,rad,.FALSE.,tvec)
    
    ntv  = size(tvec,1)
    ntv2 = size(tvec,2)
    !allocate(tvectmp(ntv,ntv2))
    call flipud(tvec,tvectmp)
    tvec = -tvectmp
    FRC%option%initParaReal = .FALSE.
    if (FRC%option%initParaReal) then
        
        write(*,*) "Error in GetNonLocalPotential.f90. FRC%option%initParaReal = FALSE."
        stop
        
        !call circum_sphere(avec,rsph)
        !center = 0.5*matmul((/1.0D0,1.0D0,1.0D0/),avec)
        !npoint = 
        !allocate(GridCoord(fn,3))
        !ntmp = ceiling(dble(fn)/dble(mpisize))
        !call GetGridPointCoord(FRC,fgridn,ntmp,GridCoord,indep)
        !call InitDistArray(fn,nproj,ntmp,1,mpisize,1,kbsub)
        !call repmat(kbsub,ntv,1,kbcell)
        
        !do tt = 1,ntv,1
        !    kbtmp = kbsub
        !    do ii = 1,natom,1
        !        pos  = FRC%atom%XYZ(ii,:) + matmul(tvec(tt,:),avec)
        !        elem = FRC%atom%element(ii)
        !        lnu  = FRC%element(elem)%vnllnu
        !        lcut = FRC%element(elem)%vnlLcutoff
        !        nnu  = FRC%element(elem)%vnlnnu
        !        ncut = FRC%element(elem)%vnlNcutoff
        !        
        !        ntmp = 0
        !        do jj = 1,nlnu,1
        !            if (lnu <= lcut .and. nnu <= ncut) then
        !                ntmp = ntmp + 1
        !            end if
        !        end do
        !        
        !        allocate(inde(ntmp))
        !        ntmp = 0
        !        do jj = 1,nlnu,1
        !            if (lnu <= lcut .and. nnu <= ncut) then
        !                ntmp       = ntmp + 1
        !                inde(ntmp) = jj
        !            end if
        !        end do
        !        
        !        dataV = FRC%ElementData(elem)%Vnl
        !        
        !        do jj = 1,nlnu,1
        !            itmp = inde(jj)
        !            if (evev == ii .and. oorb == jj) then
        !                kbind = .TRUE.
        !            else
        !                kbind = .FALSE.
        !            end if
        !            
        !            call norm(pos-center,tmp)
        !            nnn = FRC%SOB(itmp)%rrdatasize
        !            if (tmp < rsph+dataV(itmp)%rrData(nnn)) then
        !                ll = dataV(jj)%L
        !                call InterpSph2Cart(dataV(itmp),"rrData","vvData",pos,ll,GridCoord,avec,(/.FALSE.,.FALSE.,.FALSE./),kbtmp%dataV(:,kbind))
        !            end if
        !            KBEnergy(kbind) = dataV(itmp)%KBEnergy
        !        end do
        !    end do
        !    
        !    call adjustSparsity(kbtmp%dataV,0.2,kbtmp%dataV)
        !    kbcell(tt) = kbtmp
        !end do
        
        !if (.not. useCell) then
        !    call cellFun(nnz())
        !    
        !    
        !    call adjustSparsity(kbcell(1)%dataV,0.2,kbcell(1)%dataV)
        !end if
        !
        !if (mpistat) then
        !    
        !    write(*,*) "Error in GetNonLocalPotentialK.f90. MPI is not avalable now."
        !    stop
        !    
        !    !do tt = 1,NumKbcell,1
        !    !    call distmat_allgather(FRC,kbcell(tt),kbcell(tt)%dataV)
        !    !    call adjustSparsity(kbcell(tt)%dataV,0.2,kbcell(tt)%dataV)
        !    !end do
        !end if
        !
    else
        !call repmat(ntv,1,kbcell)
        ntmp = 0
        do ii = 1,natom,1
            typeE = FRC%atom%element(ii)
            do jj = 1,FRC%SOB(typeE)%Vnl
                ntmp = ntmp +1
            end do
        end do
        allocate(kbcell(ntv,ntmp))
        nkbcell = ntv
        
        kk = 1
        do ii = 1,natom,1
            pos(1,:)  = FRC%atom%XYZ(ii,:)
            elem      = FRC%atom%element(ii)
            nlnu      = size(FRC%element(elem)%vnllnu)
            allocate(lnu(nlnu),nnu(nlnu))
            lnu       = FRC%element(elem)%vnllnu
            lcut      = FRC%element(elem)%vnlLcutoff
            nnu       = FRC%element(elem)%vnlnnu
            ncut      = FRC%element(elem)%vnlNcutoff
            
            ntmp = 0
            do jj = 1,nlnu,1
                if (lnu(jj)<=lcut .and. nnu(jj)<=ncut) then
                    ntmp = ntmp+1
                end if
            end do
            allocate(inde(ntmp))
            ntmp = 0
            do jj = 1,nlnu,1
                if (lnu(jj)<=lcut .and. nnu(jj)<=ncut) then
                    ntmp       = ntmp+1
                    inde(ntmp) = jj
                end if
            end do
            
            !allocate(dataVnl(FRC%SOB(elem)%SizeOfVnl))
            !dataVnl(:) = FRC%ElementData(elem)%Vnl(:)
            
            do jj = 1,ntmp,1
                itmp    = inde(jj)
                !ll      = dataVnl(itmp)%L
                ll      = FRC%ElementData(elem)%Vnl(itmp)%L
                nindtmp = 0
                do pp = 1,nevec,1
                    if (evec(pp) == ii .and. oorb(pp) == itmp) then
                        nindtmp = nindtmp + 1
                    end if
                end do
                
                allocate(indtmp(nindtmp))
                nindtmp = 0
                do pp = 1,nevec,1
                    if (evec(pp) == ii .and. oorb(pp) == itmp) then
                        nindtmp         = nindtmp + 1
                        indtmp(nindtmp) = pp
                    end if
                end do
                
                do pp = 1,nindtmp
                    ntmp           = indtmp(pp)
                    !KBenergy(ntmp) = dataVnl(itmp)%KBenergy
                    KBenergy(ntmp) = FRC%ElementData(elem)%Vnl(itmp)%KBenergy
                end do
                    
                !allocate(isLoc(ntv))
                !forall(pp=1:ntv)
                !    isLoc(pp) = .FALSE.
                !end forall
                
                if (mod(ii-1,mpisize) == mpirank) then
                    !allocate(rrData(FRC%SOB(elem)%sizeOfVnlrrData))
                    !allocate(vvData(FRC%SOB(elem)%sizeOfVnlvvData))
                    !rrData = dataVnl(jj)%rrData
                    !vvData = dataVnl(jj)%vvData
                    !rrdata = FRC%ElementData(elem)%Vnl(jj)%rrData
                    !vvdata = FRC%ElementData(elem)%Vnl(jj)%vvData
                    !call InterpRdist2Cart_Sparse(FRC%ElementData(elem)%Vnl(jj)%rrData,FRC%ElementData(elem)%Vnl(jj)%vvData,pos,ll,avec,fgridn,.TRUE.,kbtmp,tvtmp,out3)
                    call InterpRdist2Cart(FRC%ElementData(elem)%Vnl(jj)%rrData, &
                            FRC%ElementData(elem)%Vnl(jj)%vvData,pos,ll,avec,fgridn,.TRUE.,kbtmp,tvtmp,out3)
                    
                    call ismemberRow(tvec,tvtmp,isLoc,indLoctmp)
                    
                    call ismemberRow(tvtmp,tvec,isLoctmp,indLoc)
                    
                    ntmpxxx = size(indLoc)
                    if (any(isLoc .EQV. .TRUE.)) then
                        do pp = 1,ntmpxxx,1
                            qq = indLoc(pp)
                            allocate(kbcell(qq,kk)%vata(size(kbtmp(pp,1)%vata,1),size(kbtmp(pp,1)%vata,2)))
                            !kbcell(pp,kk)%vata = kbtmp(qq,1)%vata
                            kbcell(qq,kk)%vata = kbtmp(pp,1)%vata
                        end do
                    end if
                    ! edited on 10th August
                    ntmpxxx = size(isLoc)
                    do pp = 1,ntmpxxx,1
                        if (.not. isLoc(pp)) then
                            allocate(kbcell(pp,kk)%vata(fn,2*ll+1))
                            kbcell(pp,kk)%vata = 0.0D0
                        end if
                    end do
                    ! end edit
                    ! deallocate(rrData,vvdata)
                end if
                kk = kk +1
                deallocate(indtmp,kbtmp,tvtmp,isLoc,indLoctmp,isLoctmp,indLoc,out3)
            end do
            deallocate(lnu,nnu,inde)!,dataVnl)
        end do
        
        
        allocate(kbcelltmp(nkbcell))
        do ii = 1,nkbcell,1

            ! faster method
            ntmp2 = 0
            do jj = 1,kk-1,1
                ntmp2 = ntmp2 + size(kbcell(ii,jj)%vata,2)
            end do
            ntmp1 = size(kbcell(ii,1)%vata,1)
            allocate(kbcelltmp(ii)%vata(ntmp1,ntmp2))
            ntmp1 = 0
            ntmp2 = 0
            do jj = 1,kk-1,1
                ntmp1 = ntmp2+1
                ntmp2 = ntmp2+size(kbcell(ii,jj)%vata,2)
                kbcelltmp(ii)%vata(:,ntmp1:ntmp2) = kbcell(ii,jj)%vata
            end do


            !ntmp1 = size(kbcell(ii,1)%vata,1)
            !ntmp2 = size(kbcell(ii,1)%vata,2)
            !allocate(vatatmp1(ntmp1,ntmp2))
            !vatatmp1 = kbcell(ii,1)%vata
            
            !do jj = 1,kk-2,1
            !    ntmp1 = size(kbcell(ii,jj)%vata,1)
            !    ntmp2 = size(kbcell(ii,jj)%vata,2)
            !    ntmp3 = size(kbcell(ii,jj+1)%vata,1)
            !    ntmp4 = size(kbcell(ii,jj+1)%vata,2)
            !    ntmp5 = size(vatatmp1,2)
            !    allocate(vatatmp2(ntmp1,ntmp5+ntmp4))
            !    call cat(2,vatatmp1,kbcell(ii,jj+1)%vata,vatatmp2)
            !    deallocate(vatatmp1)
            !    allocate(vatatmp1(ntmp1,ntmp5+ntmp4))
            !    vatatmp1 = vatatmp2
            !    deallocate(vatatmp2)
            !end do
            !ntmp1 = size(vatatmp1,1)
            !ntmp2 = size(vatatmp1,2)
            !allocate(kbcelltmp(ii)%vata(ntmp1,ntmp2))
            !kbcelltmp(ii)%vata = vatatmp1
            !deallocate(vatatmp1)
            !do jj = 1,kk-2,1
            !    kbcelltmp(ii)%vata(ii,:) = kbcell(ii,jj)%vata
            !end do
            !call cat(2,kbcell(ii,jj)%vata,kbcell(ii,jj+1)%vata,kbcelltmp)    
            !call adjustSparsity(kbcelltmp,0.2,kbcell(ii,1))
        end do
                !kbcell = kbcell(:,1)
       !
        ntmp1 = size(tvec,1)
        ntmp2 = size(tvec,2)
        !tvecp = int(tvec)

        allocate(tvecp(ntmp1,ntmp2))
        tvecp = int(tvec)
        if (.not. useCell) then
            call cellfunnnz(kbcelltmp,spnnz)
            !call cellfunnnz(Acell,spnnz)
            
            call reduceOper(kbcelltmp,tvecp,(/0,0,0/),spnnz,kbcell2)
            FRC%potential%vnl%kbcell = kbcell2
        else
            ntmp1 = size(kbcelltmp)
            allocate(FRC%potential%vnl%kbcell(ntmp1))
            do ii = 1,ntmp1,1
                ntmp2 = size(kbcelltmp(ii)%vata,1)
                ntmp3 = size(kbcelltmp(ii)%vata,2)
                allocate(FRC%potential%vnl%kbcell(ii)%vata(ntmp2,ntmp3))
            end do
            
            FRC%potential%vnl%kbcell = kbcelltmp
            !tvec = 0
            !call adjustSparsity(kbcell(1,1)%vata,0.2,kbcell(1,1))
        end if
        
        if (mpistat) then
            
            write(*,*) "Error in GetNonLocalPotentialK.f90. MPI is not avalable now."
            !stop
            
            !do tt = 1,nkbcell
            !    do ii = 0,mpisize-1,1
            !        nindtmp = 0
            !        do pp = 1,nevec,1
            !           if (mod(evec(pp)-1,mpisize) == ii) then
            !                nindtmp = nindtmp + 1
            !            end if
            !        end do
            !        allocate(indtmp(nindtmp))
            !        nindtmp = 0
            !        do pp = 1,nevec,1
            !            if (mod(evec(pp)-1,mpisize) == ii) then
            !                nindtmp = nindtmp + 1
            !                indtmp(nindtmp) = pp
            !            end if
            !        end do
            !        
            !        if (sum(indtmp) /= 0) then
            !            call adjustSparsity(kbcell(tt)%dataV(:,indtmp),0.2,kbdata)
            !            call MPI_Bcast_variable(kbdata,ii,kbdata)
            !        end if
            !    end do
            !    call adjustSparsity(kbcell(tt)%dataV,0.2,kbcell(tt)%dataV)
            !end do
        end if
    end if
    
    !call cellfunnnz(kbcelltmp,spnnz)
    !call reduceOper(kbcelltmp,tvecp,(/0,0,0/),spnnz,kbcell3)
    !call adjustSparsity(kbtmp%dataV,0.2,kbsparse) !! sparse matrix not used.
    !nnzLoc = nnz(kbtmp%vata)
    !call cellfunnnz(kbtmp,spnnz)
    
    !call PickInterpVnl(FRC)
    !allocate()
    !FRC%potential%vnl%kbcell = kbcell
    allocate(FRC%potential%vnl%kbvec(size(tvec,1),size(tvec,2)))
    FRC%potential%vnl%kbvec  = tvec
    
    if (kbsparse) then
        FRC%potential%vnl%kbsparse = nnzLoc
    else
        FRC%potential%vnl%kbsparse = -1
    end if
    allocate(FRC%potential%vnl%Aorb(size(evec)))
    FRC%potential%vnl%Aorb     = evec
    allocate(FRC%potential%vnl%KBEnergy(size(KBEnergy)))
    FRC%potential%vnl%KBEnergy = KBEnergy
    
    return
    end subroutine GetNonLocalPotentialK
    end module GetNonLocalPotentialK_module

