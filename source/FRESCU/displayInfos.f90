! displayInfos.f90

!******************************************************************
      !
      !
      !
!******************************************************************

      module displayInfos_module

      contains

      subroutine displayInfos(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC

      ! temporary variales
      real*8 :: HA2EV, KB, realtmp(3)

      ! output variables

      ! body of this function
      HA2EV = 27.21138602
      KB    = 0.000086173303/HA2EV
      write(*,*) "Welcome to FRESCU."
      write(*,*) "If you have any problem, please send an E-mail to"
      write(*,*) "jovenwoods@hotmail.com"
      write(*,*) "Here is some summery information in the calculations."
      write(*,*) "For more information, please see output file."
      write(*,*) "*********************** INFO *******************************************"
      write(*,*) "RESCU Version                    = ", FRC%version
      write(*,*) "Calculation Type                 = ", FRC%info%calculationType
      write(*,*) "********************* PARALLEL *****************************************"
      write(*,*) "Number of Processes              = ", FRC%mpi%mpisize
      write(*,*) "Number of GPUs                   = ", 0
      write(*,*) "Use Scalapack                    = ", FRC%smi%status
      write(*,*) "Parallelize Over K-Points        = ", FRC%mpi%paraK
      if (FRC%smi%status) then
          write(*,*) "ORFAC                            = ", FRC%smi%orfac
          write(*,*) "LWORK                            = ", FRC%smi%lwork
      end if 
      write(*,*) "********************** Basis *******************************************"
      if (FRC%LCAO%status) then
          write(*,*) "Calculation Basis                = ", "LCAO"
          !call GetAtomOrbitalInfo(FRC)
          write(*,*) "Number of Basis Functions        = ", FRC%LCAO%numberOfOrbital
      else
          write(*,*) "Calculation Basis                = ", "Real"
      end if
      write(*,*) "********************* Domain *******************************************"
      write(*,*) "Lattice Vectors                  = "!, FRC%domain%latvec(1,:)
      !write(*,*) "Lattice Vector 2                 = ", FRC%domain%latvec(2,:)
      !write(*,*) "Lattice Vector 3                 = ", FRC%domain%latvec(3,:)
      realtmp = FRC%domain%latvec(1,:)
      write(*,*) "      ", realtmp
      realtmp = FRC%domain%latvec(2,:)
      write(*,*) "      ", realtmp
      realtmp = FRC%domain%latvec(3,:)
      write(*,*) "      ", realtmp
      write(*,*) "Low Resolution N                 = ", FRC%domain%cgridn
      write(*,*) "High Resolution N                = ", FRC%domain%fgridn
      if (FRC%force%status) then
          write(*,*) "Force Resolution N               = ", FRC%force%gridn
      end if
      write(*,*) "Differentiation Method           = ", FRC%diffop%method
      write(*,*) "Differentiation Accuracy         = ", FRC%diffop%accuracy
      write(*,*) "Interpolation Method             = ", FRC%interpolation%method
      write(*,*) "Interpolation Accuracy           = ", FRC%interpolation%order
      write(*,*) "Dynamic Interpolation (VNL)      = ", FRC%interpolation%vnl
      write(*,*) "********************* Atoms ********************************************"
      write(*,*) "Number of Elements               = ", FRC%atom%numberOfElement
      write(*,*) "Number of Atoms                  = ", FRC%atom%numberOfAtom
      write(*,*) "Number of Electrons              = ", FRC%eigensolver%Nvalence
      write(*,*) "Number of Bands                  = ", FRC%eigensolver%nband
      write(*,*) "******************* Symmetry *******************************************"
      write(*,*) "Spatial Symmetries               = ", FRC%symmetry%spacesymmetry
      write(*,*) "Point Symmetries                 = ", FRC%symmetry%pointsymmetry
      write(*,*) "Time-Reversal Symmetry           = ", FRC%symmetry%timereversal
      write(*,*) "Number of Symmetries             = ", size(FRC%symmetry%sym_rec,3)
      write(*,*) "******************** Kpoints *******************************************"
      if (trim(FRC%kpoint%Ktype) == trim("full")) then
          write(*,*) "Monkhorst-Pack Grid              = ", FRC%kpoint%gridn
          write(*,*) "K-point Grid Shift               = "
          write(*,*) "      ", FRC%kpoint%kshift
      else if (trim(FRC%kpoint%Ktype) == trim("line")) then
          write(*,*) "K-point Line Grid                = ", FRC%kpoint%gridn(1)
      end if
      write(*,*) "Reduced K-grid                   = ", size(FRC%kpoint%ikdirect,1)
      write(*,*) "Sampling Method                  = ", FRC%kpoint%sampling
      write(*,*) "******************** Smearing ******************************************"
      write(*,*) "Temperature                      = ", FRC%smearing%sigma/KB
      write(*,*) "Sigma (eV)                       = ", FRC%smearing%sigma*HA2EV
      write(*,*) "********************* Functional ***************************************"
      write(*,*) "LibXC                            = ", FRC%functional%libxc
      write(*,*) "XC functional 1                  = ", FRC%functional%list(1)
      write(*,*) "XC functional 2                  = ", FRC%functional%list(2)
      if (FRC%functional%hybrid .and. trim(FRC%info%calculationType) == trim("self-consistent")) then
          write(*,*) "Gcutoff                          = ", FRC%Exx%Gcutoff
          write(*,*) "Q-point Grid                     = ", FRC%Exx%qgridn
          write(*,*) "Use SMI to Calculate Exx         = ", FRC%Exx%usesmi
      end if
      write(*,*) "******************** Eigen Solver **************************************"
      !write(*,*) "Eigen Solver Method              = ", FRC%eigenSolver%algo
      if (FRC%LCAO%status) then
          write(*,*) "Eigen Solver                     = ", FRC%eigenSolver%algoproj
      else
          write(*,*) "Initial Subspace Method          = ", FRC%eigensolver%init
          write(*,*) "Eigen Solver                     = ", FRC%eigenSolver%algo
          if (trim(FRC%eigenSolver%algo) == trim("lobpcg")) then
              write(*,*) "Subspace block size              = ", FRC%eigensolver%nsym
          end if
          if (trim(FRC%eigenSolver%algo) == trim("cfsi")) then
              write(*,*) "Adaptive Filtering Degree        = ", FRC%eigensolver%adapCFD
              write(*,*) "Spectrum Bound Method            = ", FRC%eigensolver%ubmethod
              write(*,*) "Spectrum Bound Iteration         = ", FRC%eigenSolver%ubmaxit
          end if
          write(*,*) "Maximal Number if Iterations     = ", FRC%eigensolver%maxit
          write(*,*) "Preconditioner                   = ", FRC%eigenSolver%precond
          write(*,*) "Use Partial Rayleigh-Ritz        = ", FRC%partialRR%status
          if (FRC%partialRR%status) then
              write(*,*) "Partial Rayleigh-Ritz Solver     = ", FRC%partialRR%solver
              write(*,*) "Partial Rayleigh-Ritz buffer     = ", FRC%partialRR%peig
          end if
      end if
      if (trim(FRC%info%calculationType) == trim("self-consistent") .or. &
          trim(FRC%info%calculationType) == trim("relaxation") .or. &
          trim(FRC%info%calculationType) == trim("phonon")) then
          write(*,*) "Unoccupied Bands                 = ", FRC%eigenSolver%emptyBand
      end if
      write(*,*) "*********************** Mixing *****************************************"
      write(*,*) "Mixer Algorithm                  = ", FRC%mixing%method
      write(*,*) "Mixer Type                       = ", FRC%mixing%mixingtype
      write(*,*) "Mixer History                    = ", FRC%mixing%maxHistory
      write(*,*) "Mixing Fraction                  = ", FRC%mixing%beta
      write(*,*) "Tolerance (Rho)                  = ", FRC%mixing%tol(1)
      write(*,*) "Tolerance (Etot)                 = ", FRC%mixing%tol(2)
      write(*,*) "*********************** Options ****************************************"
      write(*,*) "Maximal Number of SCF Iterations = ", FRC%option%maxSCFiteration
      write(*,*) "Init Para Real                   = ", FRC%option%initParaReal
      write(*,*) "Save Wave Functions              = ", FRC%option%saveWaveFunction
      write(*,*) "Buffer Size                      = ", FRC%option%bufferSize
      write(*,*) "    "
      write(*,*) "************************************************************************"
      write(*,*) "    "
      return
      end subroutine displayInfos
      end module displayInfos_module

