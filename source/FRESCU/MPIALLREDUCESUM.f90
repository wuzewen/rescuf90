! MPIALLREDUCESUM.f90

!*******************************************************************
      !
      !
      !
!***********************************************************************************

      module MPIALLREDUCESUMMOD
      contains

      implicit none

      interface MPIALLREDUCESUM
              module procedure MPI_Allrecude_sum_real8_1D
              module procedure MPI_Allreduce_sum_real8_2D
              module procedure MPI_Allreduce_sum_cmplx_1D
              module procedure MPI_Allreduce_sum_cmplx_2D
      end interface MPIALLREDUCESUM

      end module MPIALLREDUCESUMMOD
