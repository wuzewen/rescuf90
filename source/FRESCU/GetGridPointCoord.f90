! GetGridPointCoord.f90
    
!********************************************
    !
    !
    !
!********************************************
    
    module GetGridPointCoord_module
    contains
    subroutine GetGridPointCoord(FRC,grid,blk,coord,inde)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_1D_module
    use VataMN_2D_module
    use GetGlobalInd_1D_module
    use ndgridA_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer                         :: grid(3), blk
    
    ! temporary variables
    logical :: mpistat
    integer :: mpirank, mpisize, n, nu, nv, nw, ii, ntmp
    real*8  :: avec(3,3)
    real*8, allocatable  :: u(:), v(:), w(:)
    integer, allocatable :: inde1(:,:), inde2(:,:)
    type(VataMN_1D)      :: tmpArray
    
    
    ! output variables
    real*8, allocatable  :: coord(:,:)
    integer, allocatable :: inde(:,:)
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    avec    = FRC%domain%latvec
    
    n       = product(grid)
    nu      = grid(1)
    nv      = grid(2)
    nw      = grid(3)
 
    !write(*,*) "in getcoord, mpi=", mpistat   
    if (mpistat) then
        call InitDistArray_1D(n,1,blk,1,mpisize,1,tmpArray)
        call GetGlobalInd_1D(mpirank,tmpArray,inde1,inde2)
        inde1 = inde1 - 1
        ntmp  = size(inde1)
        allocate(u(ntmp))
        !forall(ii = 1:ntmp)
        !    u(ii) = dble(mod(int(inde1(1,ii)),nu))
        !end forall
        !u = mod(inde1(1,:),nu)
        do ii = 1,ntmp,1
            u(ii) = dble(mod(inde1(1,ii),nu))
            if (u(ii) < 0) then
                u(ii) = u(ii) + dble(nu)
            end if
        end do
     
        allocate(inde(1,ntmp))
        inde      = dble(inde1)
        inde(1,:) = (inde(1,:)-u)/dble(nu)
        u         = (u+1.0D0)/dble(nu)
        
        allocate(v(ntmp))
        do ii = 1,ntmp,1
            v(ii) = dble(mod(int(inde(1,ii)),nv))
            if (v(ii) < 0) then
                v(ii) = v(ii) + dble(nv)
            end if
        end do

       ! v    = dble(mod(int(inde(1,:)),nv))
        inde(1,:) = (inde(1,:)-v)/dble(nv)
        v         = (v+1)/dble(nv)
        allocate(w(ntmp))
        do ii = 1,ntmp,1
            w(ii) = dble(mod(int(inde(1,ii)),nw))
            if (w(ii) < 0) then
                w(ii) = w(ii) + dble(nw)
            end if
        end do
        !w         = mod(inde(1,:),nw)
        !inde(1,:) = (inde(1,:)-w)/dble(nw)
        w         = (w+1)/dble(nw)
        
        allocate(coord(ntmp,3))
        coord(:,1) = u(:)
        coord(:,2) = v(:)
        coord(:,3) = w(:)
        
        coord = matmul(coord,avec)
    else
        allocate(inde(n,1))
        forall(ii = 1:n)
            inde(ii,1) = ii
        end forall
        allocate(u(nu))
        forall(ii=1:nu)
            u(ii) = dble(ii)/dble(nu)
        end forall
        allocate(v(nv))
        forall(ii=1:nv)
            v(ii) = dble(ii)/dble(nv)
        end forall
        allocate(w(nw))
        forall(ii=1:nw)
            w(ii) = dble(ii)/dble(nw)
        end forall
        
        !write(*,*) "u in GetGridPointCoord.f90"
        !write(*,*)  u
        !write(*,*) "v in GetGridPointCoord.f90"
        !write(*,*)  v
        !write(*,*) "w in GetGridPointCoord.f90"
        !write(*,*)  w
        
        allocate(coord(n,3))
        call ndgridA(u,v,w,coord)
        
        !write(*,*) "coord in GetGridPointCoord.f90 after ndgridA"
        !write(*,*)  coord
        
        coord = matmul(coord,avec)
        
        !write(*,*) "coord in GetGridPointCoord.f90 after matmul"
        !do ii = 1, size(coord,1),1 
        !write(*,*)  ii,coord(ii,:)
        !end do
        
    end if
    
    return
    end subroutine GetGridPointCoord
    end module GetGridPointCoord_module
        
