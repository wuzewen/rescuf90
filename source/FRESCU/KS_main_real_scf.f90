!  KS_main_real_scf.f90 
!****************************************************************************
!
!  PROGRAM: KS_main_real_scf
!
!  PURPOSE: This subroutine aims at solving the KS equation with real space.
!
!  Method may be used to solve KS equation:
!           CFSI, LOBPCG..
!
!  
!
!****************************************************************************

    module KS_main_real_scf_module
    contains
    subroutine KS_main_real_scf(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    !use lobpcgMain_module
    use calcCFBOUND_module
    use CFSI_module
    use compute_CFSI_residuals_module
    
    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporery variables
    integer :: iter, cn, nkpt, nband, nspin, maxRestart
    integer :: restartIT
    logical :: failureFlag
    real*8  :: vol, dr, toltmp(2), latvec(3,3), tol
    real*8, allocatable :: kdir(:,:)
    real*8, allocatable :: LB(:,:), UB(:,:), residualNorms(:,:,:)
    
    ! output variables
    

    ! Body of KSreal
    iter   = FRC%scloop
    cn     = product(FRC%domain%cgridn)
    latvec = FRC%domain%latvec
    vol    = latvec(1,1)*latvec(2,2)*latvec(3,3)  &
            +latvec(1,2)*latvec(2,3)*latvec(3,1)  &
            +latvec(1,3)*latvec(2,1)*latvec(3,2)  &
            -latvec(1,1)*latvec(2,3)*latvec(3,2)  &
            -latvec(1,2)*latvec(2,1)*latvec(3,3)  &
            -latvec(1,3)*latvec(2,2)*latvec(3,1)
    dr         = vol/dble(cn)
    !allocate(kdir(size(FRC%kpoint%ikdirect,1),3))
    !kdir       = FRC%kpoint%ikdirect
    !nkpt       = size(kdir,1)
    nkpt       = size(FRC%kpoint%ikdirect,1)
    allocate(kdir(nkpt,3))
    kdir       = FRC%kpoint%ikdirect
    nband      = FRC%eigensolver%nband
    nspin      = FRC%spin%nspin
    toltmp     = FRC%eigensolver%tol
    tol        = minval(toltmp)
    maxRestart = FRC%eigensolver%maxRestart
    !write(*,*) "In CFSI, maxRestart = "
    !write(*,*)  maxRestart
    if (trim(FRC%eigensolver%algo) == trim("CFSI")) then
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        !%%%% Solve KS equation with CFSI method.                         %%%%!
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
        !write(*,*) "Error in KS_main_real_scf.f90. CFSI is not available now."
        !stop

        ! Get lower and upper boundary for CFSI.
        ! LB is the lower boudary, UB is the upper boundary.
        call calcCFBound(FRC,LB,UB)

        !write(*,*) "In KS_main, LB ="
        !write(*,*)  LB
        !write(*,*) "In KS_main, UB ="
        !write(*,*)  UB

        !stop

        restartIT = 1;
        do while(restartIT <= maxRestart)
            call CFSI(FRC,LB,UB)
            !write(*,*) "In KS_main after CFSI, psi ="
            !write(*,*)  FRC%psi(1,1)%vata
            !write(*,*) "Stop here"
            !stop
            if (maxRestart>1 .and. restartIT<maxRestart) then
                tol = min(FRC%mixing%dRHO/dr/nband/10,tol)
                call compute_cfsi_residuals(FRC,tol,.false.,failureFlag,residualNorms)
            else
                failureFlag = .FALSE.
            end if
            if (.not. failureFlag) then
                exit
            else
                restartIT = restartIT+1
            end if
        end do
    else if (trim(FRC%eigensolver%algo) == trim("lobpcg")) then
        !call lobpcgMain(FRC)
    else
        write(*,*) "Error in KS_main_real_scf.f90. Invalid method for eigensolver algo."
    end if

    !write(*,*) "In KS_main_real_scf, psi ="
    !write(*,*)  FRC%psi(1,1)%vata
    
    return
    end subroutine KS_main_real_scf
    end module KS_main_real_scf_module

