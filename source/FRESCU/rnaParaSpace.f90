! rnaParaSpace.f90

!********************************************************************************************
      !
      ! This function is used to calculate neutral atom charge density. 
      !
      ! Parallel will be processed over real space traversing all the
      ! atoms. Firtly, the real space grids will be generated in
      ! accordance with mpi rank and mpi size. And the charge density on
      ! these grids will be calculated.
      !
!********************************************************************************************

      module rnaParaSpace_module
      contains
      subroutine rnaParaSpace(FRC,rho,rhodm)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use calcFilling_module
      use InterpSph2Cart_module
      use VataMN_2D_module
      use GetGridPointCoord_module

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      type(VataMN_2D) :: rho, rhodm

      ! temporary variables
      integer :: mpisize, ispin, fn, npoint, natom, ntmp
      integer :: ii, typeE, fgridn(3), bc(3)
      real*8  :: lvec(3,3), pos(1,3), fillFrac

      integer, allocatable :: indep(:,:)
      real*8 , allocatable :: GridCoord(:,:), rhotmp(:,:)

      character(len=20)    :: flax, numb
      integer :: dou

      ! output variables

      ! body of this function
      mpisize = FRC%mpi%mpisize
      ispin   = FRC%spin%ispin
      fgridn  = FRC%domain%fgridn
      fn      = product(fgridn)

      ntmp    = ceiling(dble(fn)/dble(mpisize))
      !allocate(GridCoord(fn,3))
      call GetGridPointCoord(FRC,fgridn,ntmp,GridCoord,indep)
      npoint  = size(GridCoord,1)
      natom   = FRC%Atom%NumberofAtom
      lvec    = FRC%domain%latvec

      !write(numb,"(i4.4)") FRC%mpi%rank
      !flax = 'GenRho'//trim(numb)//'.txt'
      !dou  = FRC%mpi%rank+10
      !open(unit=dou,file=flax)
      !write(dou,*) 'process', FRC%mpi%rank
      !write(dou,*) 'GridCoord'
      !write(dou,*)  GridCoord


      if (ispin == 4) then
              write(*,*) "Error in rnaParaSpace. SPIN."
      end if

      call calcFilling(FRC,fillFrac)

      !write(dou,*) 'fillFrac', fillFrac

      ntmp = ceiling(dble(fn)/dble(mpisize))
      call InitDistArray_2D(fn,1,ntmp,1,mpisize,1,rho)
      allocate(rho%vata(npoint,1))
      rho%vata = 0.0D0

      if (ispin == 1) then
              ntmp = ceiling(dble(fn)/dble(mpisize))
              call InitDistArray_2D(fn,1,ntmp,1,mpisize,1,rhodm)
              allocate(rhodm%vata(npoint,1))
              rhodm%vata = 0.0D0
      else if (ispin == 2) then
              write(*,*) "Error in rnaParaSpace. SPIN"
      else if (ispin == 4) then
              write(*,*) "Error in rnaParaSpace. SPIN"
      end if

      !write(*,*) "rmax =", maxval(FRC%ElementData(1)%rna%rrData)
      !write(*,*) "GridCoord ="
      !write(*,*)  GridCoord

      allocate(rhotmp(npoint,1))
      bc  =  1
      do ii = 1,natom,1

          !write(dou,*) 'loop', ii
          
          typeE    = FRC%Atom%element(ii)
          pos(1,:) = FRC%Atom%XYZ(ii,:)
          call InterpSph2Cart(FRC%ElementData(typeE)%rna%rrData, &
                              FRC%ElementData(typeE)%rna%rhoData,&
                              pos,-1,GridCoord,lvec,bc,rhotmp)
          rho%vata(:,1) = rho%vata(:,1) + rhotmp(:,1)
          !write(dou,*) 'rhotmp'
          !write(dou,*)  rhotmp
          if (ispin == 1) then
                  rhodm%vata(:,1) = rhodm%vata(:,1) + rhotmp(:,1)*fillFrac
          else
                  write(*,*) "Error in rnaParaSpace. SPIN"
          end if

      end do

      allocate(FRC%rho%atom%vata(npoint,1))
      FRC%rho%atom = rho
      !close(dou)

      return
      end subroutine
      end module
