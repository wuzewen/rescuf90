! distmat_submatrix.f90

!*********************************************
      !
      !
      !
!**********************************************

      module distmat_submatrixMod
      contains
      subroutine distmat_submatrix(FRC,dA,subi,subj,dC)

      implicit none

      mpisize = FRC%mpi%mpisize
      if ((.NOT. allocated(subi)) .AND. &
              (.NOT. allocated(subj)) then
          dC = dA
      else if (.NOT. allocated(subi)) then
          call ModBCDist(FRC,dA,dA%mblock,dA%nblock,mpisize,1,dC)
          dC%n = size(subj)
          dC%nblock = min(dA%block,dC%n)
          dC%vata = 
          call ModBCDist(FRC,dC,dA%mblock,dA%nblock,dA%mproc,dA%nproc)
      else if (.NOT. allocated(subj)) then
          call ModBCDsit(FRC,dA,dA%mblock,dA%nblock,1,mpisize,dC)
          dC%m = size(subi)
          dC%mblock = min(dA%mblock,dC%m)
          dC%vata = 
          call ModBCDist(FRC,dC,dC%mblock,dC%nblock,dA%mproc,dA%nproc)
      end if

      return
      end subroutine distmat_submatrix
      end module distmat_submatrixMod
