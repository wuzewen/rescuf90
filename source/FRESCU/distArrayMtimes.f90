! distArrayMtimes.f90
    
!************************************************************
    !
    !
    !
!************************************************************
    
    module distArrayMtimes_module
    contains
    subroutine distArrayMtimes(blockX1,blockX2,mpistat,gramXBX)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: blockX1(:,:), blockX2(:,:)
    logical             :: mpistat
    
    ! temporary variables
    
    ! output variables
    real*8, allocatable :: gramXBX(:,:)
    
    ! body of this function
    allocate(gramXBX(size(blockX1,2),size(blockX2,2)))
    gramXBX = matmul(transpose(blockX1),blockX2)
    
    if (mpistat) then
        write(*,*) "Error in distArrayMtimes.f90. MPI is not available now."
        !call MPI_Allreduce_sum(gramXBX,gramXBX)
    end if
    
    return
    end subroutine distArrayMtimes
    end module distArrayMtimes_module