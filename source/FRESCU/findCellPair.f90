! findCellPair.f90

!***********************************************************
      !
      !
      !
!***********************************************************

      module findCellPair_module
      contains
      subroutine findCellPair(FRC,aocell,pair,pairLoc)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC

      ! temporary variables

      ! output variables

      ! body of this function
      mpistat = FRC%mpi%status
      ncell   = size(aocell)
      do ii = 1,ncell,1
          do jj = 1,size(aocell(ii)%vata,1),1
              if (any(aocell(ii)%vata(jj,:) /= 0)) then
                      gridBoole(ii)%vata(jj) = .TRUE.
              else
                      gridBoole(ii)%vata(jj) = .FALSE.
              end if
          end do
      end do

      allocate(pair(ncell,ncell))
      allocate(pairLoc(ncell,ncell))

      do ii = 1,ncell,1
          do jj = 1,ncell,1
              if (any(gridBoole(ii)) .and. any(gridBoole(jj))) then
                      pairLoc(ii,jj) = .TRUE.
              else
                      pairLoc(ii,jj) = .FALSE.
              end if
              pair(ii,jj) = pairLoc(ii,jj)
          end do
      end do

      return
      end subroutine findCellPair
      end module findCellPair_module


