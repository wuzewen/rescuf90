! BSXFTIMES.f90

      module BSXFMINUS_module

              use bsxfunminus_2d_2d_real8_module
              use bsxfunminus_2d_2d_cmplx_module
              use bsxfunminus_2d_3d_real8_module
              use bsxfunminus_2d_3d_cmplx_module
              use bsxfunminus_3d_2d_real8_module
              use bsxfunminus_3d_2d_cmplx_module
              use bsxfunminus_3d_3d_real8_module
              use bsxfunminus_3d_3d_cmplx_module
              use bsxfunminus_4d_4d_real8_module
              use bsxfunminus_4d_4d_cmplx_module

              implicit none

              interface BSXFMINUS
                      module procedure bsxfunminus_2d_2d_real8
                      module procedure bsxfunminus_2d_2d_cmplx 
                      module procedure bsxfunminus_2d_3d_real8
                      module procedure bsxfunminus_2d_3d_cmplx
                      module procedure bsxfunminus_3d_2d_real8
                      module procedure bsxfunminus_3d_2d_cmplx
                      module procedure bsxfunminus_3d_3d_real8
                      module procedure bsxfunminus_3d_3d_cmplx
                      module procedure bsxfunminus_4d_4d_real8
                      module procedure bsxfunminus_4d_4d_cmplx
              end interface BSXFMINUS

      end module BSXFMINUS_module
