! bsxfunMod.f90
    
!*************************************************
    !
    ! This function has the same function with
    ! bsxfun(@Mod,A,B) in matlab
    !
!*************************************************
    
    module bsxfunMod_module
    contains
    subroutine bsxfunMod(MatrixA,MatrixB,MatrixC)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: MatrixA(:,:), MatrixB(:,:)
    
    ! temporary variables
    integer :: mA, nA, mB, nB
    integer :: ii, jj
    
    ! output variables
    real*8, allocatable :: MatrixC(:,:)
    
    ! body of this function
    mA = size(MatrixA,1)
    nA = size(MatrixA,2)
    mB = size(MatrixB,1)
    nB = size(MatrixB,2)
    
    if (mA == 1 .and. nB == 1) then
        do ii = 1,nA,1
            do jj = 1,mB,1
                MatrixC(jj,ii) = mod(MatrixA(1,ii),MatrixB(jj,1))
                if (MatrixC(jj,ii) < 0) then
                    MatrixC(jj,ii) = MatrixC(jj,ii)+MatrixB(jj,1)
                end if
            end do
        end do
    else if (nA == 1 .and. mB == 1) then
        do ii = 1,mA,1
            do jj = 1,nB,1
                MatrixC(ii,jj) = mod(MatrixA(ii,1),MatrixB(1,jj))
                if (MatrixC(ii,jj) < 0) then
                    MatrixC(ii,jj) = MatrixC(ii,jj)+MatrixB(1,jj)
                end if
            end do
        end do
    else if (mA == 1 .and. nA == nB) then
        do ii = 1,nA,1
            do jj = 1,mB,1
                MatrixC(jj,ii) = mod(MatrixA(1,ii),MatrixB(jj,ii))
                if (MatrixC(jj,ii) < 0) then
                    MatrixC(jj,ii) = MatrixC(jj,ii)+MatrixB(jj,ii)
                end if
            end do
        end do
    else if (nA == 1 .and. mA == mB) then
        do ii = 1,mA,1
            do jj = 1,nB
                MatrixC(ii,jj) = mod(MatrixA(ii,1),MatrixB(ii,jj))
                if (MatrixC(ii,jj) < 0) then
                    MatrixC(ii,jj) = MatrixC(ii,jj)+MatrixB(ii,jj)
                end if
            end do
        end do
    else if (mB == 1 .and. nA == nB) then
        do ii = 1,nB,1
            do jj = 1,mA,1
                MatrixC(jj,ii) = mod(MatrixA(jj,ii),MatrixB(1,ii))
                if (MatrixC(jj,ii) < 0) then
                    MatrixC(jj,ii) = MatrixC(jj,ii)+MatrixB(1,ii)
                end if
            end do
        end do
    else if (nB == 1 .and. mB == mA) then
        do ii = 1,mB,1
            do jj = 1,nA,1
                MatrixC(ii,jj) = mod(MatrixA(ii,jj),MatrixB(ii,1))
                if (MatrixC(ii,jj) < 0) then
                    MatrixC(ii,jj) = MatrixC(ii,jj)+MatrixB(ii,1)
                end if
            end do
        end do
    else if (mA == mB .and. nA == nB) then
        do ii = 1,mA,1
            do jj = 1,nA,1
                MatrixC(ii,jj) = mod(MatrixA(ii,jj),MatrixB(ii,jj))
                if (MatrixC(ii,jj) < 0) then
                    MatrixC(ii,jj) = MatrixC(ii,jj)+MatrixB(ii,jj)
                end if
            end do
        end do
    else
        write(*,*) "Error in bsxfunMod.f90."
        stop
    end if
    
    return
    end subroutine bsxfunMod
    end module bsxfunMod_module