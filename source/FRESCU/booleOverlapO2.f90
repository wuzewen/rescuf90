! booleOverlapO2.f90

!**********************************************************************************
      !
      !
      !
!**********************************************************************************

      module booleOverlapO2Mod
      contains
      subroutine booleOverlapO2(FRC,xyz,evec1,Rorb1,evec2,Rorb2,sbDisp,vcDisp)

      implicit none

      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize
      smimb   = FRC%smi%mb
      sminb   = FRC%smi%nb
      smimp   = FRC%smi%mp
      sminp   = FRC%smi%np
      avec    = FRC%domain%latvec
      sprs    = FRC%LCAO%sprs
      norb1   = size(Rorb1)
      norb2   = size(Rorb2)
      rad     = maxval(Rorb1)+maxval(Rorb2)
      call cellInrange(avec,xyz,rad,1,vcDisp)
      nt      = size(vcDisp,1)
      natom   = size(xyz,1)
      call InitDistArray(natom,natom,smimb,sminb,smimp,sminp,sboole)
      call GetGlobalInd(mpirank,sboole,iloc,jloc)
      allocate(sboole%vata(size(iloc),size(jloc)))
      sboole%vata = .FALSE.
      allocate(rmax(natom))

      ntmp = size(evec1)
      do ii = 1,evec1(ntmp),1
          do jj = 1,ntmp,1
              if (evec1(jj) .EQ. ii) then
                  rmax(ii) = max(rmax(ii),Rorb1(evec1(jj)))
              end if
              if (evec2(jj) .EQ. ii) then
                  rmax(ii) = max(rmax(ii),Rorb2(evec2(jj)))
              end if
          end do
      end do

      allocate(iRorb(size(iloc)))
      allocate(jRorb(size(jloc)))
      allocate(xyz1(size(iloc),3))
      allocate(xyz2(size(jloc),3))
      forall(ii=1:size(iloc))
              iRorb(ii)  = rmax(iloc(ii))
              xyz1(ii,:) = xyz(iloc(ii),:)
      end forall
      forall(ii=1:size(jloc))
              jRorb(ii)  = rmax(jloc(ii))
              xyz2(ii,:) = xyz(jloc(ii),:)
      end forall

      do ll = 1,nt,1
          if (size(sbDisp(ll)%vata) .GT. 0) then
              call bsxfunPlus(xyz2,matmul(vcDisp(ll,:),avec),xyz2)
              call calcLocalBooleanOverlap(xyz1,xyz2,iRorb,jRorb,tmp)
              call adjustSparsity()
          end if
      end do

      if (mpistat) then
          do ll = 2,nt,1
              sbDisp(1)%vata = sbDisp(1)%vata .OR. sbDisp(ll)%vata
          end do
          call distmat_nnz(FRC,sbDisp(1),nonzero)
          spfrac = nonzero/sbDisp(1)%m/sbDisp(1)%n
          issp   = spfrac .LT. 0.05D0 .AND. spDisp(1)%m*spDisp(1)%n &
                      .GT. 10**6
          if (issp) then
              sparse
          else
              full
          end if
          call ModBCDist(FRC,sbDisp(1),smimb,sminb,1,mpisize,.FALSE.,issp)
          **
          call ModBCDist(FRC,sbDisp(1),smimb,sminb,mpisize,1,.FALSE.,issp)
          **
          call ModBCDist(FRC,sbDisp(1),smimb,sminb,smimp,sminp,.FALSE.,issp)
          logical
          call adjustSparsity()

          do ll = 2,nt,1
              sbDisp(ll) = sbDisp(1)
          end do
      else
          do ll = 1,nt,1
              sbDisp(ll)%vata = 
              sbDisp(ll)%m    = norb1
              sbDisp(ll)%n    = norb2
              call adjustSparsity(sbDisp(ll)%vata,sprs)
          end do
      end if
      forall(ii=1:size(evec1))
              xyz1(ii,:) = xyz(evec1(ii),:)
      end forall
      forall(ii=1:size(evec2))
              xyz2(ii,:) = xyz(evec2(ii),:)
      end forall
      call GetGlobalInd(mpirank,sbDisp(1),iloc,jloc)
      forall(ii=1:size(iloc))
              xtmp1(ii,:) = xyz1(iloc(ii),:)
              iRorb(ii)   = Rorb1(iloc(ii))
      end forall
      forall(ii=1:size(jloc))
              xtmp2(ii,:) = xyz2(jloc(ii),:)
              jRorb(ii)   = Rorb2(jloc(ii))
      end forall

      do ll = 1,nt,1
          if (size(sbDisp(ll)%vata) .GT. 0) then
              call bscfunPlus(xtmp2,)
              call calcLocalBooleanOverlap(xtmp1,xtmp2,iRorb,jRorb,sbDisp(ll)%vata,tmp)
              call adjustSparsity(tmp,0.2,sbDisp(ll)%vata)
          end if
      end if

      call sprucecell(FRC,sbDisp,idel)
      
      delet

      return
      end subroutine booleOverlapO2
      end module booleOverlapO2Mod
