! ndgrid
    
    module ndgrid_module
    contains
    subroutine ndgrid(ku,kv,kw,ndgu,ndgv,ndgw)
    
    implicit none
    
    ! input valuables
    real*8,allocatable :: ku(:), kv(:), kw(:)
    
    ! temporery valuables
    integer          :: nu, nv, nw
    integer          :: i, j!, k
    
    ! output valuables
    real*8,allocatable :: ndgu(:,:,:), ndgv(:,:,:), ndgw(:,:,:)
    
    ! body of this function
    nu = size(ku)
    nv = size(kv)
    nw = size(kw)
    
    do i = 1,nu,1
        ndgu(i,:,:) = ku(i)
    end do
        
    do i = 1,nv,1
        ndgv(:,i,:) = kv(i)
    end do
    
    do i = 1,nw,1
        ndgw(:,:,i) = kw(i)
    end do
    
    
    return
    end subroutine ndgrid
    end module ndgrid_module
            
