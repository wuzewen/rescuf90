! smiDSYMM.f90

!*********************************************
      !
      !
      !
!*******************************************************

      module smiDSYMMMOD
      contains
      subroutine smiDSYMM(side,uplo,alpha,A,B,beta,C,nprocs, &
                      MA,NA,MBA,NBA,nprowA,npcolA,&
                      MB,NB,MBB,NBB,nprowB,npcolB,&
                      MC,NC,MBC,NBC,nprowC,npcolC)

      implicit none

      character(len=1) :: side, uplo
      integer :: MA,NA,MBA,NBA,nprowA,npcolA
      integer :: MB,NB,MBB,NBB,nprowB,npcolB
      integer :: MC,NC,MBC,NBC,nprowC,npcolC
      real*8 :: alpha, beta
      real*8, allocatable :: A(), B(), C()

      integer :: iam, ictxtA, ictxtB, ictxtC
      integer :: myrowA, mycolA, myrowB, mycolB, myrowC, mycolC

      iam    = 0
      ictxtA = 0
      ictxtB = 0
      ictxtC = 0
      myrowA = 0
      mycolA = 0
      myrowB = 0
      mycolB = 0
      myrowC = 0
      mycolC = 0
      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxtA)
      call blacs_gridinit(ictxtA,'Row-major',nprowA,npcolA)
      call blacs_gridinfo(ictxtA,nprowA,npcolA,myrowA,mycolA)
      call blacs_get(-1,0,ictxtB)
      call blacs_gridinit(ictxtB,'Row-major',nprowB,npcolB)
      call blacs_gridinfo(ictxtB,nprowB,npcolB,myrowB,mycolB)
      call blacs_get(-1,0,ictxtC)
      call blacs_gridinit(ictxtC,'Row-major',nprowC,npcolC)
      call blacs_gridinfo(ictxtC,nprowC,npcolC,myrowC,myrowC)

      izero = 0
      ione  = 1
      call numroc(MA,MBA,myrowA,izero,nprowA,A_np)
      call numroc(MB,MBB,myrowB,izero,nprowB,B_np)
      call numroc(MC,MBC,myrowC,izero,norowC,C_np)

      call PDSYMM(side,uplo,MC,NC,alpha,A,ione,ione,descA,&
              B,ione,ione,descB,beta,C,ione,ione,descC)

      call blacs_gridexit(ictxtA)
      call blacs_gridexit(ictxtB)
      call blacs_gridexit(ictxtC)

      return
      end subroutine smiDSYMM
      end module smiDSYMMMOD
