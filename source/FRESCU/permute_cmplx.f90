! permute.f90
    
!****************************************************************
    !
    ! This function has the same function as function permute
    ! in MATLAB.
    !
!****************************************************************
    
    module permute_cmplx_module
    contains
    subroutine permute_cmplx(A,b,C)
    
    ! input variables
    complex*16, allocatable :: A(:,:,:)
    integer              :: b(3)
    
    ! temporary variables
    integer :: nx, ny, nz, sA(3), sC(3), ss(3), sx(3), ii, jj, kk
    
    ! output variables
    complex*16, allocatable :: C(:,:,:)
    
    ! body of this function
    nx = b(1)
    ny = b(2)
    nz = b(3)
    
    sA(1) = size(A,1)
    sA(2) = size(A,2)
    sA(3) = size(A,3)
    
    sC(1) = sA(nx)
    sC(2) = sA(ny)
    sC(3) = sA(nz)
    
    allocate(C(sC(1),sC(2),sC(3)))
    !forall(ii=1:sC(1),jj=1:sC(2),kk=1:sC(3))
    !    C(ii,jj,kk) = A()
    do ii = 1,sA(1),1
        do jj = 1,sA(2),1
            do kk = 1,sA(3),1
                ss(1) = ii
                ss(2) = jj
                ss(3) = kk
                sx(1) = ss(nx)
                sx(2) = ss(ny)
                sx(3) = ss(nz)
                C(sx(1),sx(2),sx(3)) = A(ii,jj,kk)
            end do
        end do
    end do
    
    return
    end subroutine permute_cmplx
    end module permute_cmplx_module
