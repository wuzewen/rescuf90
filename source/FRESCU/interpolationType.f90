! interpolationType.f90
    
!***********************************************************************
    !
    ! This is to define a type to contain interpolation information
    !
!***********************************************************************
    
    module interpolationType_module
    
    type :: interpolationType
        integer              :: interpRho
        character(len=10)    :: method
        integer              :: order
        logical              :: Vnl
        logical              :: vloc
    end type interpolationType
    
    end module interpolationType_module