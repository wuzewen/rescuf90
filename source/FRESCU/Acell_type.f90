! Acell_type
    
!*****************************************************************
    !
    ! This type of data will be used in GetNonLocalPotential
    ! and reduceOper
    !
!*****************************************************************
    
    module Acell_type
    
    type :: AcellType
        real*8, allocatable :: vata(:,:)
    end type
    
    end module Acell_type