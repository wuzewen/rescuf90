! reCalcLcaoHam.f90

!*******************************************************************************************
      !
      !
      !
!*******************************************************************************************

      module reCalcLcaoHamMod
      contains
      subroutine reCalcLcaoHam(FRC,incVeff,calcVeff,sameVec, &
                                        calcDistpMat,HH,hv,SS,sv)

      use

      implicit none

      mpisize = FRC%mpi%mpisize
      ispin   = FRC%spin%ispin

      if (calcDispMat) then
              FRC%kpoint%kdirect = 0.5D0
      end if

      if (allocated(FRC%LCAO%Scell) .OR. allocated(FRC%LCAO%Tcell)) then
              call calcOverlapAndKineticMatrix(FRC)
              call calcNonlocalPotentialMatrix(FRC)
              call mergeVnlKineticMatrix(FRC)
      end if

      Scell = FRC%LCAO%Scell
      sv    = FRC%LCAO%Svec

      if (.NOT. samevec) then
              SS = Scell
      end if

      Tcell = FRC%LCAO%Tcell
      tv    = FRC%LCAO%Tvec

      call unionRow(sv,tv,hv)

      if (FRC%spin%SOI) then
          if ((.NOT. allocated(FRC%LCAO%vnlSOcell)) .OR. & 
                 (calcDispMat .AND. size(FRC%LCAO%vnlSOcell,1) .EQ. 1)) then
              call calcProjectorMatrixSOI(FRC)
              call calcVnlMatrixSOI(FRC)
          end if
          SOcell = FRC%VnlSOcell
          sov    = sv
          call unionRow(hv,sov(1)%vata,hv)
          call unionRow(hv,sov(2)%vata,hv)
          call unionRow(hv,sov(3)%vata,hv)
      end if

      if (FRC%functional%includeU) then
          Ucell = FRC%Hubbard%VhubbardMatrix
          suv   = sv
          if (spins .EQ. 4) then
              do tt = 1,size(Ucell,1),1
                  Ucell(tt,3)%vata = Ucell(tt,2)%vata-im*Ucell(tt,3)%vata
              end do
              delet
          end id
          call unionRow(hv,suv,hv)
      end if

      if (FRC%functional%hybrid) then
          VFcell = FRC%Exx%VFcell
          vfv    = FRC%EXX%VFvec
          do ii = 1,size(vfv),1
              call unionRow(hv,vfv(ss)%vata,hv)
          end do
      end if

      if (incVeff) then
          if (calcVeff) then
              call recalcPotential(FRC,'Veff',FRCtmp)
              vell = FRCtmp%potential%veff
          else
              !iter = FRC%scloop
              veff = FRC%potential%veffin(1)
              anterp = (product(FRC%domain%cgridn) .NE. veff%m)
              call UpdateVeff(FRC,FRC%potential%veffin(1),FRC%potential%vps,anterp,veff)
          end if
          call ModBCDist(FRC,veff,ceiling(veff%m/mpisize),1,mpisize,1,vefftmp)
          call calcVeffCell(FRC,veff,Vcell,vv)
          call union(hv,vv,hv)
      end if

      nhv = size(hv,1)
      dumdmat = Scell(1)
      dumdmat%vata = sprse

      do tt = 1,nhv,1
          if (samevec) then
              call ismemberRow(hv(tt,:),sv,stmp,itmp)
              if (stmp) then
                  SS(tt)%vata = SS(tt)%vata+ &
                    reshape(Scell(itmp)%vata,(/size(SS(tt)%vata,1),size(SS(tt)%vata,2)/))
              end if
          end if
          do ss = 1,size(HH,2),1
              if (inclVeff) then
                  call ismemberRow(hv(tt,:),vv,stmp,itmp)
                  if (stmp) then
                      HH(tt,ss)%vata = HH(tt,ss)%vata + &
                                         Vcell(itmp,ss)%vata
                  end if
              end if
              if (FRC%spin%SOI) then
                  call ismemberRow(hv(tt,:),sov(ss)%vata,stmp,itmp)
                  if (stmp) then
                      HH(tt,ss)%vata = HH(tt,ss)%vata + &
                              SOcell(ss,itmp)%vata
                  end if
              end if
              if (FRC%functional%hybrid) then
                  exxbeta = FRC%EXX%beta
                  call ismemberRow(hv(tt,:),vfv(ss)%vata,stmp,itmp)
                  if (stmp) then
                      HH(tt,ss)%vata= HH(tt,ss)%vata + &
                              exxbeta*VFcell(ss,itmp)%vata
                  end if
              end if
              if (FRC%functional%includeU) then
                  call ismemberRow(hv(tt,:),suv,stmp,itmp)
                  if (stmp) then
                      HH(tt,ss)%vata = HH(tt,ss)%vata + &
                              Ucell(itmp,ss)%vata
                  end if
              end if

              if (ispin .EQ. 4 .AND. ss .EQ. 2) then
                      exit
              end if
              call ismemberRow(hv(tt,:),tv,stmp,itmp)
              if (stmp) then
                  HH(tt,ss)%vata = HH(tt,ss)%vata + &
                          Tcell(itmp)%vata
              end if
          end do
      end do
      if (samevec) then
              sv = hv
      end if

      return
      end subroutine reCalcLcaoHam
      end module reCalcLcaoHamMod

