! ifft3.f90
    
    module ifft3_module
    contains
    subroutine ifft3(A,dim,ifftout)
    
    implicit none
    
    ! input valuables
    integer               ::  dim
    complex*16, allocatable  ::  A(:,:,:)
    
    ! temporery valuables
    integer               ::  i, j
    integer               ::  N
    complex*16, parameter    ::  imnu = (0.0D0,1.0D0), pi = (3.1415926535897932385D0,0.0D0)
    
    ! output valuables
    complex*16, allocatable  ::  ifftout(:,:,:)
    
    ! body of this function
    N    = size(A,dim)
    ifftout = (0.0D0,0.0D0)
    do i = 1,N,1
        do j = 1,N,1
        if (dim == 1) then
            ifftout(i,:,:) = ifftout(i,:,:)+A(j,:,:)*exp(imnu*dcmplx(2.0D0)*pi*dcmplx(i-1)*dcmplx(j-1)/dcmplx(N))
        else if (dim == 2) then
            ifftout(:,i,:) = ifftout(:,i,:)+A(:,j,:)*exp(imnu*dcmplx(2.0D0)*pi*dcmplx(i-1)*dcmplx(j-1)/dcmplx(N))
        else if (dim == 3) then
            ifftout(:,:,i) = ifftout(:,:,i)+A(:,:,j)*exp(imnu*dcmplx(2.0D0)*pi*dcmplx(i-1)*dcmplx(j-1)/dcmplx(N))
        end if
        end do
    end do
    ifftout = ifftout/dcmplx(N)
    
    end subroutine ifft3
    end module ifft3_module
    
