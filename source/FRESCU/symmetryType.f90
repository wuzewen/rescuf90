! symmetryType.f90
    
!******
    !
    !
    !
!*********************************************************
    
    module symmetryType_module
    
    type :: symmetryType
        logical              :: spacesymmetry
        logical              :: pointsymmetry
        logical              :: timereversal
        integer, allocatable :: sym_rec(:,:,:)
        integer, allocatable :: sym_dir(:,:,:)
        real*8, allocatable  :: sym_t(:,:)
        integer, allocatable :: sym_perm(:,:)
    end type symmetryType
    
    end module symmetryType_module