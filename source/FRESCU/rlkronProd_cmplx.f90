! rkronprod.f90
    
!*****************************************************************
    !
    !
    !
!*****************************************************************
    
    module rlkronprod_cmplx_module
    contains
    subroutine rlkronprod_cmplx(A,B,nx,KA)
    
    use lkronprod_cmplx_module
    
    implicit none
    
    ! input variables
    integer             :: nx
    complex*16, allocatable :: A(:,:), B(:,:)
    
    ! temporary variables
    integer :: p, q, n, m
    complex*16, allocatable :: T(:,:), Atmp(:,:)
    
    ! output variables
    complex*16, allocatable :: KA(:,:)
    
    ! body of this function
    p = nx*size(A,1)
    q = Nx*size(A,2)
    n = size(B,1)/q
    m = size(B,2)
    
    allocate(T(q,m*n))
    T = reshape(B,(/q,m*n/))
    call lkronprod_cmplx(A,T,Atmp)
    allocate(KA(p*n,m))
    KA = reshape(Atmp,(/p*n,m/))
    
    return
    end subroutine rlkronprod_cmplx
    end module rlkronprod_cmplx_module
