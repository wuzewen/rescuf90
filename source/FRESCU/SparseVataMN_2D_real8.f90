! SparseVataMN_2D_Real8.f90

!***********************************************************************
      !
      !
      !
!***********************************************************************

      module SparseVataMN_2D_Real8_module

              type :: SparseVataMN_2D_Real8
                      integer :: SIZE1, SIZE2
                      integer :: N_nonzero
                      integer :: m, n
                      integer :: mblock, nblock
                      integer :: mproc, nproc
                      integer, allocatable :: Index1(:)
                      integer, allocatable :: Index2(:)
                      real*8 , allocatable :: ValueN(:)
              end type SparseVataMN_2D_Real8

      end module SparseVataMN_2D_Real8_module
