! kbProj2Vnl.f90

!*********************************************************************
      !
      !
      !
!*********************************************************************

      module kbProj2VnlMod
      contains
      subroutine kbProj2Vnl(FRC,T,V1,V2,kbe,isrl)

      implicit none

      if ((.NOT. allocated(V1)) .OR. (.NOT. allocate(V2))) then
              return
      end if

      mpistat = FRC%mpi%status
      mpisize = FRC%mpi%mpisize
      mpirank = FRC%mpi%rank
      smistat = FRC%smi%status

      call distmat_nnz(FRC,V1,nnz1)
      call distmat_nnz(FRC,V2,nnz2)
      nn      = V1%m*V1%n

      if (.NOT. mpistat) then
          call bsxfunTimes(V1%vata,kbe,tmp)
          T%vata = T%vata + matmul(tmp,transpose(conjg(V2%vata)))
      else if (smistat .AND. (nn .LE. 2**20 .OR. &
              (dble(nnz1)/dble(nn) .GT. 0.01 .AND. dble(nnz2)/dble(nn) .GT. 0.01))) then
          call ModBCDist(FRC,V1,T%mblock,T%nblock,T%mproc,T%nproc)
          call ModBCDist(FRC,V2,T%mblock,T%nblock,T%mproc,T%nproc)
          GetGlobalInd(mpirank,V1,jloc)
          call bsxfunTimes(V1%vata,kbe,V1%vata)
          Tnew = T
          if (isrl) then
                  call smiDGEMM('n','t',1,V1,V2,1,Tnew)
          else
                  call smiZGEMM('n','c',1,V1,V2,1,Tnew)
          end if
      else
          call ModBCDist(FRC,V1,T%mblock,T%nblock,1,mpisize)
          call GetGlobalInd(mpirank,V1,jloc)
          call adjustSparsity(V1%vata,0.1D0)
          forall(ii=1:size(jloc))
                  V1%vata(ii,ii) = V1%vata(ii,ii)*kbe(jloc(ii))
          end forall
          call ModBCDist(FRC,V2,T%mblock,T%nblock,1,mpisize)
          call adjustSparsity(V2%vata,0.1D0)

          do ii = 0,mpisize-1,1
              call GetGlobalInd(ii,T,iloc,jloc)
              tmp = matmul(V1%vata(iloc,:),transpose(conjg(V2%vata(jloc,:))))
              if (mpistat) then
                  call MPI_Reduce_sum(tmp,ii)
              end if
              if (mpirank .EQ. ii) then
                  T%vata = T%vata + tmp
              end if
          end do
      end if

      call adjustSparsity(T%vata,0.25D0)

      return
      end subroutine kbProj2Vnl
      end module kbProj2VnlMod
