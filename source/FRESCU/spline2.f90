! spline2.f90

!**************************************************************
      ! This function provide spline intepolation method.
      ! It is based on spline.m in MATLAB.
      ! Only one dimention array is available in this function.
!**************************************************************

      module spline2_module
      contains
      subroutine spline2(x,y,xx,yy)

      use spdiags_4in_module
      use inversion_module

      external dgetrf, dgetri

      !implicit none

      ! input variables
      real*8, allocatable :: x(:), y(:), xx(:)

      ! temporary variables
      integer :: n, m, ii, jj, LDA, LWORK, INFO
      integer, allocatable :: IPIV(:)
      real*8  :: x31, xnn
      real*8, allocatable :: dx(:), divdif(:), b(:,:), s(:,:)
      real*8, allocatable :: c(:,:), dxt(:,:), dzzdx(:), dzdxdx(:)
      real*8, allocatable :: pc(:,:), xs(:), WORK(:)
      integer,allocatable :: ind(:), tmp(:)

      ! output variables
      real*8, allocatable :: yy(:)

      ! body of this function
      n   = size(x)
      if (n < 4) then
              write(*,*) "Error in spline2. Not enough points data."
              stop
      end if
      allocate(dx(n-1))
      allocate(divdif(n-1))
      forall(ii=1:n-1)
              dx(ii)     = x(ii+1)-x(ii)
              divdif(ii) = (y(ii+1)-y(ii))/dx(ii)
      end forall
      !write(*,*) "dx = "
      !write(*,*)  dx
      !write(*,*) "divdif = "
      !write(*,*)  divdif

      allocate(b(1,n))
      b = 0.0D0

      forall(ii=2:n-1)
              b(1,ii) = 3.0D0*(dx(ii)*divdif(ii-1)+dx(ii-1)*divdif(ii))
      end forall
      !write(*,*) "b = "
      !write(*,*)  b


      x31 = x(3)-x(1)
      xnn = x(n)-x(n-2)

      b(1,1) = ((dx(1)+2.0D0*x31)*dx(2)*divdif(1)+(dx(1)**2.0D0)*divdif(2))/x31
      b(1,n) = ((dx(n-1)**2.0D0)*divdif(n-2)+(2.0D0*xnn+dx(n-1))*dx(n-2)*divdif(n-1))/xnn

      !write(*,*) "b = "
      !write(*,*)  b
      allocate(dxt(n,3))

      dxt(1,1) = x31
      dxt(n,1) = 0.0D0
      dxt(1,2) = dx(2)
      dxt(n,2) = dx(n-2)
      dxt(1,3) = 0.0D0
      dxt(n,3) = xnn
      forall(ii=2:n-1)
              dxt(ii,1) = dx(ii-1)
              dxt(ii,2) = 2.0D0*(dx(ii)+dx(ii-1))
              dxt(ii,3) = dx(ii)
      end forall

      !write(*,*) "dxt = "
      !do ii = 1,size(dxt,1),1
      !write(*,*) dxt(ii,:)
      !end do
      allocate(tmp(3))
      tmp(1) = -1
      tmp(2) =  0
      tmp(3) =  1
      call spdiags_4in(dxt,tmp,n,n,c)

      !write(*,*) "c = "
      !do jj = 1,size(c,2),1
      !do ii = 1,size(c,1),1
      !if (c(ii,jj) /=0) then
      !write(*,*) ii,jj,c(ii,jj)
      !end if
      !end do
      !end do

      LDA   = n
      LWORK = n
      allocate(WORK(LWORK))
      allocate(IPIV(n))
      call DGETRF(n,n,c,LDA,IPIV,INFO)
      call DGETRI(n,c,LDA,IPIV,WORK,LWORK,INFO)
      !call inversion(c,c)
      !write(*,*) "c = "
      !do jj = 1,size(c,2),1
      !do ii = 1,size(c,1),1
      !if (c(ii,jj) /=0) then
      !      write(*,*) ii,jj,c(ii,jj)
      !end if
      !end do
      !end do

      allocate(s(1,n))
      s = matmul(b,c)

      !write(*,*) "s = "
      !write(*,*)  s

      allocate(dzzdx(n-1))
      allocate(dzdxdx(n-1))
      dzzdx = (divdif-s(1,1:n-1))/dx

      !write(*,*) "dzzdx ="
      !write(*,*)  dzzdx

      dzdxdx = (s(1,2:n)-divdif)/dx

      !write(*,*) "dzdxdx ="
      !write(*,*)  dzdxdx

      allocate(pc(n-1,4))
      pc(:,1) = (dzdxdx-dzzdx)/dx
      pc(:,2) = 2.0D0*dzzdx-dzdxdx
      pc(:,3) = s(1,1:n-1)
      pc(:,4) = y(1:n-1)

      !write(*,*) "pc ="
      !do ii = 1,size(pc,1),1
      !write(*,*) pc(ii,:)
      !end do

      m  = size(xx)
      allocate(xs(m))
      allocate(ind(m))
      xs = xx
      
      do ii = 1,m,1
          if (xs(ii) < x(2)) then
              xs(ii)  = xs(ii) - x(1)
              ind(ii) = 1
          else if (xs(ii) >= x(n-1)) then
              xs(ii)  = xs(ii) - x(n-1)
              ind(ii) = n-1
          else
              do jj = 2,n-2,1
                  if (xs(ii) >= x(jj) .and. xs(ii) < x(jj+1)) then
                          xs(ii)  = xs(ii) - x(jj)
                          ind(ii) = jj
                          exit
                  end if
              end do
          end if
      end do

      !write(*,*) "xs ="
      !write(*,*)  xs
      !write(*,*) "ind"
      !write(*,*)  ind

      forall(ii=1:m)
              yy(ii) = pc(ind(ii),1)
      end forall

      do jj = 2,4,1
          forall(ii=1:m)
                  yy(ii) = xs(ii)*yy(ii)+pc(ind(ii),jj)
          end forall
      end do


      return
      end subroutine spline2
      end module spline2_module
              

      
