! potentialType.f90
    
!********************************************************************
    !
    ! This is to define a type to contain potential information.
    !
!********************************************************************
    
    module potentialType_module
    
    use VataMN_2D_module
    use VataMN_1D_module
    use RealVata_2D_module
    use IntegerVata_1D_module
    use ComplexVata_2D_module
    use SparseArray_2D_Real8_module
    use SparseArray_2D_Cmplx_module
    use SparseVataMN_2D_Real8_module
    
    type :: Potvnltype
        logical                      :: kbsparse
        logical                      :: KBorbIsSparse
        logical                      :: separable
        logical                      :: reduceKBcell
        integer, allocatable         :: evec(:), Lorb(:), Morb(:), Norb(:), Oorb(:), Sorb(:)
        real*8 , allocatable         :: Rorb(:), Eorb(:), kbvec(:,:), Aorb(:), KBEnergy(:)
        complex*16, allocatable      :: KBorb(:,:)
        real*8                       :: kpt(3)
        type(RealVata_2D), allocatable :: kbcell(:) 
        integer, allocatable         :: vnlsize(:)
        type(IntegerVata_1D), allocatable :: g1Ind(:), g2Ind(:), g3Ind(:)
        type(ComplexVata_2D), allocatable :: kbDcell(:)
        !type(SparseArray_2D_Real8), allocatable :: sparsekbcell(:)
        type(SparseVataMN_2D_Real8), allocatable :: sparsekbcell(:)
        type(SparseArray_2D_Cmplx) :: sparsekborb
    end type Potvnltype
    
    type :: potentialType
        logical                      :: fourierinit
        logical                      :: separable
        logical                      :: vnlScheme
        real*8, allocatable          :: vloc(:,:)
        real*8, allocatable          :: vloc1(:)
        type(VataMN_2D)              :: vtau
        real*8, allocatable          :: vtauloc(:,:)
        type(VataMN_2D)              :: VXCout
        type(VataMN_1D)              :: vna
        type(VataMN_1D)              :: vps
        type(VataMN_1D)              :: VH
        type(VataMN_1D)              :: VHout
        type(VataMN_1D)              :: veffin(2)
        type(VataMN_1D)              :: veffout
        type(VataMN_1D)              :: deltain
        type(VataMN_1D)              :: deltaout(2)
        type(Potvnltype)             :: vnl
    end type potentialType
    
    end module potentialType_module
