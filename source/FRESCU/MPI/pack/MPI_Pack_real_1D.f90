! MPI_Pack_real_1D.f90

      module MPI_Pack_real_1D_module
      contains
      subroutine MPI_Pack_real_1D(A,header,vata)

      implicit none

      ! input variables
      real*8, allocatable :: A(:)

      ! temporary variables
      logical :: isSparse
      integer :: typeSPARSE, typrMATRIX, headersize, datalength
      integer :: s, hashcode, nd

      ! output variables
      integer             :: header(10)
      real*8, allocatable :: vata(:)

      ! body of this function
      typeSPARSE =  1
      typrMATRIX =  2
      headersize =  10
      isSparse   = .FALSE.

      if (isSparse) then
              write(*,*) "Error in MPI_Pack. sparse matrix is unable."
      else
              datalength = size(A)
              s          = size(A)
              nd         = 1
              allocate(vata(datalength))
              !do jj = 1,s(2),1
              !    do ii = 1,s(1),1
              !        ntmp = ntmp + 1
              !        vata(ntmp) = A(ii,jj)
              !    end do
              !end do
              vata       = A
              hashcode   = 0
              header(1)  = typeMatrix
              header(2)  = datalength
              header(3)  = 1
              header(4)  = hashcode
              header(5)  = nd
              header(6)  = s
              header(7)  = 0
              header(8)  = 0
              header(9)  = 0
              header(10) = 0
      end if

      return
      end subroutine MPI_Pack_real_1D
      end module MPI_Pack_real_1D_module
