! MPI_Reduce.f90

!**************************************************************
      !
      !
      !
!**************************************************************

      module MPI_Reduce_module
      contains
      subroutine MPI_Reduce(vari,oper,ranks,root,variout)

      include 'mpif.h'

      call MPI_Comm_rank()
      call MPI_Comm_size()

      rank_tmp = ranks

      do while (size(rank_tmp) > 1)
      forall(ii = 1:)
              destranks(ii) = rank_tmp(ii)
              sourceranks(ii) = rank_tmp(ii+1)
      end forall
      tag = size(destranks)
      if (any(sourceranks == mpirank)) then
              mtmp = 0
              do ii = 1,
                  if (sourceranks(ii) == mpirank) then
                          mtmp = mtmp + 1
                  end if
              end do
              allocate(dest(mtmp))
              mtmp = 0
              do ii = 1,
                  if (sourceranks(ii) == mpirank) then
                          mtmp = mtmp + 1
                          dest(mtmp) = destranks(ii)
                  end if
              end do
              call MPI_Send_variable(vari,dest,tag)
      else if (any(destranks == mpirank)) then
              mtmp = 0
              do ii = 1,
                  if (destranks(ii) == mpirank) then
                          mtmp = mtmp + 1
                  end if
              end do
              allocate(sour(mtmp))
              mtmp = 0
              do ii = 1,
                  if (destranks(ii) == mpirank) then
                          mtmp = mtmp + 1
                          sour(mtmp) = sourceranks(ii)
                  end if
              end do
              call MPI_Recv_variable(sour,tag)
              if (trim(oper) == trim('PLUS')) then
                      vari = vari + sour
              else if (trim(oper) == trim('MINUS')) then
                      vari = vari - sour
              end if
      end if
      ranktmp = ranktmp(1:2:)
      end do

      if (root /= rank_tmp) then
              if (rank_tmp == mpirank) then
                      call MPI_Send_variable(vari,root,0)
              else if (root == mpirank) then
                      call MPI_Recv_variable(rank_tmp,0)
              end if
      end if
      if (root /= mpirank) then
              variout = 0
      end if

      return
      end subroutine MPI_Reduce
      end module MPI_Reduce_module
