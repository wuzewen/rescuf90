! MPI_Send_variable.f90

!*******************************************************************
      !
      !
      !
!*******************************************************************

      module MPI_Send_variable_module
              use MPI_Send_variable_integ_1D_module
              use MPI_Send_variable_integ_2D_module
              use MPI_Send_variable_integ_3D_module
              use MPI_Send_variable_real8_1D_module
              use MPI_Send_variable_real8_2D_module
              use MPI_Send_variable_real8_3D_module
              use MPI_Send_variable_cmplx_1D_module
              use MPI_Send_variable_cmplx_2D_module
              use MPI_Send_variable_cmplx_3D_moudle
              use MPI_Send_variable_relsp_2D_moudle
              use MPI_Send_variable_cmpsp_2D_moudle

              implicit none

              interface MPI_Send_variable
                      module procedure MPI_Send_variable_integ_1D
                      module procedure MPI_Send_variable_integ_2D
                      module procedure MPI_Send_variable_integ_3D
                      module procedure MPI_Send_variable_real8_1D
                      module procedure MPI_Send_variable_real8_2D
                      module procedure MPI_Send_variable_real8_3D
                      module procedure MPI_Send_variable_cmplx_1D
                      module procedure MPI_Send_variable_cmplx_2D
                      module procedure MPI_Send_variable_cmplx_3D
                      module procedure MPI_Send_variable_relsp_2D
                      module procedure MPI_Send_variable_cmpsp_2D
              end interface

      end module MPI_Send_variable_module

