! MPI_Send_variable_real8_1D.f90

!**********************************************************
      !
      !
      !
!**********************************************************

      module MPI_Send_variable_real8_1D_module
      contains
      subroutine MPI_Send_variable_real8_1D(vari,dest,tag,bloc)

      include 'mpif.h'

      ! input variables
      integer              :: tag, bloc
      real*8, allocatable  :: vari(:)
      integer, allocatable :: dest(:)

      ! temporary variables

      ! output variables


      ! body of this funtion
      varinfo     = 0
      varinfo(1)  = 0
      issp        = varinfo(1)
      varinfo(2)  = 1
      isrl        = varinfo(2)
      varinfo(3)  = bloc
      varinfo(4)  = count(vari /= 0.0)
      vaeinfo(5)  = 2
      varinfo(6)  = size(vari,1)
      varisize    = varinfo(6)

      call mex_send(varinfo,16,dest,tag)
      tag         = tag + 1
      kk          = 0

      varn = size(vari)

      do while (kk < varn)
          ntmp = minval(kk+bloc,varn)
          forall(ii = kk+1:ntmp)
                  inde(ii-kk) = ii
          end forall
          allocate(buf(ntmp-kk))
          forall(ii=1:ntmp)
                  buf(ii) = vari(inde(ii))
          end forall
          if (ntmp == 1) then
                  buftmp(1) = buf(1)
                  buftmp(2) = 0
                  call mex_send(buf,2,dest,tag)
                  tag = tag+1
          else
                  call mex_send(buf,2,dest,tag)
                  tag = tag+1
          end if
          kk = kk + bloc

      end do

      return
      end subroutine MPI_Send_variable_real8_1D
      end module MPI_Send_variable_real8_1D_module
