! MEX_ALLREDUCE_SUM.f90

!**************************************************************************
      !
      !
      !
!**************************************************************************

      module MEX_ALLREDUCE_SUM_module
      contains
      subroutine MEX_ALLREDUCE_SUM(sendinfo,recvinfo,n,rankn,ranks)

      include 'mpif.h'

      ! input variables
      integer              :: n, rankn
      integer, allocatable :: ranks(:)
      real*8 , allocatable :: sendinfo(:), recvinfo(:)

      ! temporary variables
      integer :: world_size, ierr, world_group, prime_group
      integer :: prime_comm

      ! output variables

      ! body of this function
      call MPI_COMM_SIZE(MPI_COMM_WORLD,world_size,ierr)

      if (rankn == world_size) then
              call MPI_ALLREDUCE(sendinfo,recvinfo,n,&
                                 MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD,ierr)
      else
              call MPI_COMM_GROUP(MPI_COMM_WORLD,world_group,ierr)
              call MPI_GROUP_INCL(world_group,rankn,ranks,prime_group,ierr)
              call MPI_COMM_CREATE_GROUP(MPI_COMM_WORLD,prime_group,0,prime_comm)
              call MPI_ALLREDUCE(sendinfo,recvinfo,n,&
                                 MPI_DOUBLE,MPI_SUM,prime_comm,ierr)
              call MPI_GROUP_FREE(world_group,ierr)
              call MPI_GROUP_FREE(prime_group,ierr)
              call MPI_COMM_FREE(prime_comm,ierr)
      end if

      return
      end subroutine MEX_ALLREDUCE_SUM
      end module MEX_ALLREDUCE_SUM_module
