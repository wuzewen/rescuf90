! calcProjMatSOI.f90

!***************************************************************************************
      !
      !
      !
!***************************************************************************************

      module calcProjMatSOIMod
      contains
      subroutine calcProjMatSOI(FRC)

      implicit none

      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpi%mpisize
      sprslim = FRC%LCAO%sprs
      buff    = min(2**26,FRC%option%buuferSize)
      avec    = FRC%domain%latvec
      smimb   = FRC%smi%mb
      sminb   = FRC%smi%nb
      smimp   = FRC%smi%mp
      sminp   = FRC%smi%np
      xyz     = FRC%smi%xyz

      call GetAtomicOrbitalInfo(FRC)
      evec1   = FRC%LCAO%parameters%evec
      Lorb1   = FRC%LCAO%parameters%Lorb
      Morb1   = FRC%LCAO%parameters%Morb
      Oorb1   = FRC%LCAO%parameters%Oorb
      Rorb1   = FRC%LCAO%parameters%Rorb
      Sorb1   = FRC%LCAO%parameters%Sorb
      norb1   = size(Lorb1)

      call GetKBSOIOrbiInfo(FRC)

      Lorb2   = FRC%potential%vnlSO%Lorb
      Jorb2   = FRC%potential%vnlSO%Jorb
      Porb    = Jorb2 - Lorb2

      do ii = 1,size(FRC%ElementData),1
      do jj = 1,size(FRC%ElementData(ii)%OrbitalSet),1
      do kk = 1,size(FRC%ElementData),1
      do ll = 1,size(FRC%ElementData(kk)%VnlSO)
      if (allocated(FRC%ElementData(kk)%Vnl(ll)%fqData)) then
              fqmat2(:,ii,jj,kk,ll) = &
                      FRC%ElementData(ii)%OrbitalSet(jj)%fqData* &
                      FRC%ElementData(kk)%VnlSO(ll)%fqData
      end if
      end do
      end do
      end do
      end do

      ntmp = size(Porb)
      allocate(ind(ntmp))
      !allocate(evec2(ntmp),Lorb2(ntmp),Morb2(ntmp))
      !allocate(Oorb2(ntmp),Rorb2(ntmp),Sorb2(ntmp))
      !norb2 = size(Lorb2)

      do ss = 1,2,1
          !allocate(ind(size(Porb)))

          do ii = 1,ntmp,1
              if (dble(ss) -1.5D0 .EQ. Porb(ii)) then
                  ind(ii) = .TRUE.
              else
                  ind(ii) = .FALSE.
              end if
          end do
          nrmp = count(ind .EQV. .TRUE.)

          allocate(dex(nrmp))
          nrmp = 0
          do ii = 1,ntmp,1
              if (dble(ss) -1.5D0 .EQ. Porb(ii)) then
                      nrmp      = nrmp + 1
                      dex(nrmp) = ii
              end if
          end do
          allocate(evec2(nrmp),Lorb2(nrmp),Morb2(nrmp))
          allocate(Oorb2(nrmp),Rorb2(nrmp),Sorb2(nrmp))
          norb2 = ntmp
          forall(ii=nrmp)
                   evec2(ii) = FRC%potential%vnlSO%evec(dex(ii))
                   Lorb2(ii) = FRC%potential%vnlSO%Lorb(dex(ii))
                   Morb2(ii) = FRC%potential%vnlSO%Morb(dex(ii))
                   Oorb2(ii) = FRC%potential%vnlSO%Oorb(dex(ii))
                   Rorb2(ii) = FRC%potential%vnlSO%Rorb(dex(ii))
                   Sorb2(ii) = FRC%potential%vnlSO%Sorb(dex(ii))
           end forall
           call booleOverlapO2(FRC,xyz,evec1,Rorb1,evec2,Rorb2,sbDisp,txyz)
           nt = size(txyz,1)
           call GetGlobalInd(mpirank,sbDisp(1),iloc,jloc)
           allocate(iRorb(size(iloc)))
           allocate(jRorb(size(jloc)))
           forall(ii=1:size(iloc))
                   iRorb(ii) = Rorb1(iloc(ii))
           end forall
           forall(ii=1:size(jloc))
                   jRorb(ii) = Rorb2(jloc(ii))
           end forall
           lmax = maxval()
           call GenGauntMatrix(lmax,gauntMat)
           q = FRC%ElementData%OrbitalSet(1)%qqData
           w = FRC%ElementData%OrbitalSet(1)%qwData
           maxrad = (maxval(Rorb1)+maxval(Rorb2))*maxval(q)*1.05D0
           call linspace(0,maxrad,ceiling(maxrad/0.05D0),juvrad)
           allocate(juvval(2*lmax+1,size(juvrad)))
           juvval = 0.0D0
           do ii = 0,2*lmax,1
               call calcSphericalBessel(Luv,juvrad,juvval)
           end do
           sprs = norb1*norb2*16*2/mpisize .GT. buff
           allocate(xyz1(size(iloc)))
           allocate(xyz2(size(jloc)))
           forall(ll=1:size(iloc))
                   xyz1(ll,:) = xyz(evec1(ll),:)
           end forall
           forall(ll=1:size(jloc))
                   xyz2t(ll.:) = xyz(evec2(ll),:)
           end forall
           allocate(xyz2t(size(jloc)))
           do ll = 1,nt,1
               kb = sbDisp(ll)
               call bsxfunPlus(xyz2t,matmul(txyz(ll,:),avec))
               call calcLocalBooleanOverlap(xyz1,xyz2,iRorb,jRorb,sbDisp(ll)%vata,slog)
               if (sprs) then
                       complex
               else
                       complex
               end if
               if (nnz(slog)) then
                   call find(slog,fslogi,fslogj)
                   !blks = ceiling(buff/size(q))
                   kk   = 1
                   do while (kk .LE. size(slog))
                       if (kk+blks .GT. size(fslogi)) then
                               blks = size(fslogi) - kk
                       end if
                       allocate(slogi(blks),slogj(blks))
                       forall(ii=1:blks)
                               slogi(ii) = fslogi(kk+ii-1)
                               slogj(ii) = fslogj(kk+ii-1)
                               rijmat(ii,:) = xyz1(slogi(ii),:)
                               dijmat(ii)   = sqrt(sum(rijmat(ii,:)**2))
                       end forall
                       do Luv = 0,2*lmax,1
                           forall(ii=blks)
                                   lu(ii) = Lorb1(lioc(slogi(ii)))
                                   lv(ii) = Lorb2(jloc(slogj(ii)))
                                   mu(ii) = Morb1(iloc(slogi(ii)))
                                   mv(ii) = Morb2(jloc(slogj(ii)))
                                   slogtmp(ii) = Luv .GE. abs(lu(ii)-lv(ii))&
                                           .AND. Luv .LE. lv(ii)+lu(ii)&
                                           .AND. Luv .GE. abs(mu(ii)-mv(ii))
                           end forall
                           if (nnz(slogtmp)) then
                               call sub2ind()
                               gaunt = 
                               call GetsphHarm(Luv,mv-mu,rij,sphHarm)
                               gaunt = im**(lu-lv+Luv) * gaunt*sphHarm
                               allocate(rij(size(slogtmp)))
                               forall(ii=1:size(slogtmp))
                                   rij(ii) = dijmat(slogtmp(ii))
                               end forall
                               call interp1()
                               forall(ii=1:size(slogtmp))
                                   slogtmpi(ii) = slogi(slogtmp(ii))
                                   slogtmpj(ii) = slogj(slogtmp(ii))
                               end forall
                               call sub2ind()
                               temp = fqmat2(Rq12,:)*juv
                               call sub2ind()
                               if (sprs) then
                                   forall(ii=1:blks)
                                       lind(ii) = kk+ii-1
                                       kb%vata(lind(ii)) = &
                                               kb%vata(lind(ii)) + &
                                               gaunt(ii)*(temp(ii)*q(ii)**2*w(ii))
                                   end forall
                               else
                                   forall(ii=1:blks)
                                       ztemp(lind(ii)) = gaunt(ii)*temp(ii)*(q(ii)**2 * w(ii))
                                   end forall
                                   kb%vata = kb%vata+ztemp
                                   ztemp(lind) = 0
                               end if
                           end if
                       end do
                       kk = kk + blks + 1
                   end do
               end if
               if (sprs) then
                   if (.NOT. nnz(slog)) then
                           call find(slog,fslogi,fslogj)
                   end if
                   call sparse
               end if
               call adjustsparsity(kb%vata,sprslim)
               kbcell(ll) = kb
           end do

           if (mpistat) then
                   call MPI_Barrier()
           end if

           if (all(FRC%kpoint%kdirect .EQ. 0.0D0)) then
               call cellfunsparse(kbcell,issp)
               if (issp) then
                       call cellfunnnz(kbcell,isspn)
               else
                       isspn = -1
               end if
               call reduceOper(kbcell,txyz,(//0,0,0),issp,tmp)
               kbcell = tmp
               txyz   = (/0,0,0/)
           end if

           do ii = 1,size(kbcell),1
               call distmat_nnz(FRC,kbcell(ii),nonzero)
               if (nonzero .GT. 0) then
                   tmp = kbcell(ii)
                   spfrac = nonzero/tmp%m/tmp%n
                   issp   = spfrac .LT. 0.05D0 .AND. &
                           tmp%m*tmp%n .GT. 10**6
                   if (issp) then
                           sparse
                   else
                           full
                   end if

                   call ModBCDist(FRC,tmp,smimb,sminb,1,mpisize,issp,tmp)
                   call sph2real(tmp%vata,Morb1,'l')
                   tmp%vata = 4.0D0*pi*tmp%vata
                   call croparray(tmp%vata,eps)
                   call ModBCDist(FRC,tmp,smimb,sminb,smimp,sminp,issp,tmp)
                   call adjustSparsity(tmp%vata,sprslim)
                   kbcell(ii) = tmp
               else
                   delet
               end if
           end do
           call spricecell(FRC,kbcell)
           txyz() = 
           FRC%LCAO%kbSOcell(ss) = kbcell
           FRC%LCAO%kbSOtv(ss)   = txyz
       end do

       return
       end subroutine calcProjMatSOI
       end module calcProjMatSOIMod


