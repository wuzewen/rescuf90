!  genLaplacian.f90 
!****************************************************************************
!
!  PROGRAM: genLaplacian
!
!  PURPOSE: This function is used to generate Laplacian Operator.
!  
!  One of the following method will be used: FD or FFT
!
!****************************************************************************

    module genLaplacian_module
    contains
    subroutine Genlaplacian(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ftLaplMat_module
    use fdLaplMat_module
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    integer                         :: acc
    integer                         :: bc(3)
    character(len=20)               :: method
    integer                         :: cgridn(3), fgridn(3)
    
    ! temporery valuables

    ! output valuables

    ! Body of genLaplacian
    method = FRC%diffop%method
    acc    = FRC%diffop%accuracy
    bc     = FRC%domain%boundary
    cgridn = FRC%domain%cgridn
    fgridn = FRC%domain%fgridn
    allocate(FRC%diffop%Duu(cgridn(1),cgridn(1)))
    allocate(FRC%diffop%Dvv(cgridn(2),cgridn(2)))
    allocate(FRC%diffop%Dww(cgridn(3),cgridn(3)))
    allocate(FRC%diffop%fDuu(fgridn(1),fgridn(1)))
    allocate(FRC%diffop%fDvv(fgridn(2),fgridn(2)))
    allocate(FRC%diffop%fDww(fgridn(3),fgridn(3)))
    if (trim(method) == trim("fd")) then
        call fdLaplMat(cgridn,acc,bc,FRC%diffop%Duu,FRC%diffop%Dvv,FRC%diffop%Dww)
        call fdLaplMat(fgridn,acc,bc,FRC%diffop%fDuu,FRC%diffop%fDvv,FRC%diffop%fDww)
    else if (trim(method) == trim("fft")) then
        call ftLaplMat(cgridn,FRC%diffop%Duu,FRC%diffop%Dvv,FRC%diffop%Dww)
        call ftLaplMat(fgridn,FRC%diffop%fDuu,FRC%diffop%fDvv,FRC%diffop%fDww)
    end if
    
    return
    end subroutine genLaplacian
    end module genLaplacian_module

