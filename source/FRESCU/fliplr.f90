! fliplr.f90

!*********************************************************************
      !
      ! A is an input 2D array. B is an output 2D array.
      ! B returns A with with the order of elements flipped left to
      ! right along the second dimension. For example,
      ! A = 1 2 3  becomes B = 3 2 1
      !     4 5 6              6 5 4
      !
!*********************************************************************

      module fliplr_module
      contains
      subroutine fliplr(A,B)

      implicit none

      ! input variables
      real*8, allocatable :: A(:,:)

      ! temporary variables
      integer :: m, n, ii

      ! output variables
      real*8, allocatable :: B(:,:)

      ! body of this function
      m = size(A,1)
      n = size(A,2)

      forall(ii=1:n)
              B(:,ii) = A(:,n-ii+1)
      end forall

      return
      end subroutine fliplr
      end module fliplr_module
