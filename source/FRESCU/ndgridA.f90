! ndgrid
    
    module ndgridA_module
    contains
    subroutine ndgridA(ku,kv,kw,uvw)
    
    implicit none
    
    ! input valuables
    real*8,allocatable :: ku(:), kv(:), kw(:)
    
    ! temporery valuables
    integer            :: nu, nv, nw, nn
    real*8,allocatable :: ndgu(:,:,:), ndgv(:,:,:), ndgw(:,:,:), tmp(:,:)
    integer            :: i, j, k
    
    ! output valuables
    real*8,allocatable :: uvw(:,:)
    
    ! body of this function
    nu = size(ku)
    nv = size(kv)
    nw = size(kw)
    nn = nu*nv*nw
    
    allocate(ndgu(nu,nv,nw))
    do i = 1,nu,1
        ndgu(i,:,:) = ku(i)
    end do
    
    allocate(ndgv(nu,nv,nw))
    do i = 1,nv,1
        ndgv(:,i,:) = kv(i)
    end do
    
    allocate(ndgw(nu,nv,nw))
    do i = 1,nw,1
        ndgw(:,:,i) = kw(i)
    end do
    
    allocate(tmp(nn,1))
    tmp = reshape(ndgu,(/nn,1/))
    
    !write(*,*) "tmp in ndgridA."
    !write(*,*)  tmp
    !write(*,*)  size(tmp)
    !allocate(uvw(nn,3))
    forall(i=1:nn)
        uvw(i,1) = tmp(i,1)
    end forall
    
    tmp = reshape(ndgv,(/nn,1/))
    forall(i=1:nn)
        uvw(i,2) = tmp(i,1)
    end forall
    
    tmp = reshape(ndgw,(/nn,1/))
    forall(i=1:nn)
        uvw(i,3) = tmp(i,1)
    end forall
    
    return
    end subroutine ndgridA
    end module ndgridA_module
            