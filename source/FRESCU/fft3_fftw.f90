! fft3_fftw.f90

      module fft3_fftw_module
      contains
      !subroutine fft3_fftw(FRC,A)
      subroutine fft3_fftw(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use, intrinsic :: iso_c_binding
      !include 'fftw3.f03'

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      !type(C_PTR) :: plan
      !integer(C_INT)                              :: L, M, N
      !complex(C_DOUBLE_COMPLEX), dimension(L,M,N) :: Arr, Brr
      !complex*16, allocatable         :: A(:,:,:)!, B(:,:,:)

      !FRC%Arr = A
      !call dfftw_plan_dft_3d(plan,L,M,N,Arr,Brr,FFTW_FORWARD,FFTW_ESTIMATE)
      call dfftw_execute_dft(FRC%planfft,FRC%Arr,FRC%Arr)
      !call dfftw_destroy_plan(plan)
      !B   = Brr
      !A       = FRC%Arr

      return
      end subroutine fft3_fftw
      end module fft3_fftw_module
