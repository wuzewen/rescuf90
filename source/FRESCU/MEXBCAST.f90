! MEXBCAST.f90

!************************************************************************************************
      !
      !
      !
!************************************************************************************************

      module MEXBCASTMOD

      interface MEXBCAST
              module procedure Mex_Bcast_1D
              module procedure Mex_Bcast_2D
              module procedure Mex_Bcast_3D
              module procedure Mex_Bcast_cmplx_2D
              module procedure Mex_Bcast_int_2D
      end interface MEXBCAST

      end module MEXBCASTMOD
