! fft.f90
    
    module fft4_module
    contains
    subroutine fft4(A,dim,B)
    
    implicit none
    
    ! input valuables
    integer               ::  dim
    complex*16, allocatable  ::  A(:,:,:,:)
    
    ! temporery valuables
    integer               ::  i, j
    integer               ::  N
    complex*16, parameter    ::  imnu = (0.0D0,1.0D0)!, pi = (3.14159265354D0,0.0D0)
    complex*16 :: Atmp
    real*8     :: nmt, pi
    ! output valuables
    complex*16, allocatable  ::  B(:,:,:,:)
    
    ! body of this function
    pi = 3.1415926535897932385D0
    N  = size(A,dim)
    B  = (0.0D0,0.0D0)
    do i = 1,N,1
        do j = 1,N,1
            if (dim==1) then
                nmt        = 2.0D0*pi*dble(i-1)*dble(j-1)/dble(N)
                Atmp       = exp(-imnu*dcmplx(nmt))
                B(i,:,:,:) = B(i,:,:,:)+A(j,:,:,:)*Atmp           
                ! & exp(-imnu*cmplx(2.0D0)*cmplx(pi)*cmplx(real(i-1))*cmplx(real(j-1))/cmplx(real(N)))
            else if (dim==2) then
                nmt        = 2.0D0*pi*dble(i-1)*dble(j-1)/dble(N)
                Atmp       = exp(-imnu*dcmplx(nmt))
                B(:,i,:,:) = B(:,i,:,:)+A(:,j,:,:)*Atmp 
                ! & exp(-imnu*cmplx(2.0D0)*cmplx(pi)*cmplx(real(i-1))*cmplx(real(j-1))/cmplx(real(N)))
            else if (dim==3) then
                nmt        = 2.0D0*pi*dble(i-1)*dble(j-1)/dble(N)
                Atmp       = exp(-imnu*dcmplx(nmt))
                B(:,:,i,:) = B(:,:,i,:)+A(:,:,j,:)*Atmp 
                ! & exp(-imnu*cmplx(2.0D0)*cmplx(pi)*cmplx(real(i-1))*cmplx(real(j-1))/cmplx(real(N)))
            else if (dim==4) then
                nmt        = 2.0D0*pi*dble(i-1)*dble(j-1)/dble(N)
                Atmp       = exp(-imnu*dcmplx(nmt))
                B(:,:,:,i) = B(:,:,:,i)+A(:,:,:,j)*Atmp 
                ! & exp(-imnu*cmplx(2.0D0)*cmplx(pi)*cmplx(real(i-1))*cmplx(real(j-1))/cmplx(real(N)))
            end if
        end do
    end do
    
    end subroutine fft4
    end module fft4_module
    
