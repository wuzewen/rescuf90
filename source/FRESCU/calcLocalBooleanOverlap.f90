! calcLocalBooleanOverlap.f90

!***********************************************************************
      !
      !
      !
!***********************************************************************

      module calcLocalBooleanOverlapMod
      contains
      subroutine calcLocalBooleanOverlap(xyz1,xyz2,rorb1,rorb2,sbloc)

      implicit none

      blks = 2**20
      nrow = size(rorb1)
      ncol = size(rorb2)

      ! case 1
      sbloc = .FALSE.
      kk = 1
      do while (kk .LE. ncol)
          if (kk+blks .GT. ncol) then
                  blks = ncol - kk
          end if
          call gendistmat(xyz1,xyz2(),sbtmp)
          call bsxfunPlus(rorb1,rorb2(),maxdist)
          sbloc() = sbtmp .LT. maxdist
          kk = kk + blks +1
      end do

      ! case 2
      call find(sbloc,fslogi,fslogj,sbloc)
      forall(ii=1:size(fslogi))
              colvec = floor((dble(ii)-10.0D0**-2)/blks) + 1
      end forall
      do kk = 1,maxval(colvec),1
          ntmp = 0
          do ii = 1,size(colvec),1
          if (colvec(ii) .EQ. kk) then
              ntmp = ntmp + 1
          end if
          end do
          allocate(slogi(ntmp),slogj(ntmp))
          ntmp = 0
          do ii = 1,size(colvec),1
          if (colvec(ii) .EQ. kk) then
              ntmp = ntmp + 1
              slogi(ntmp) = fslogi(colvec(ii))
              slogj(ntmp) = fslogj(colvec(ii))
          end if
          end do
          allocate(dijmat(size(slogi)))
          forall(ii=1:size(slogi))
                  dijmat(ii,:) = xyz1(slogi(ii),:) - xyz2(slogj(ii),:)
          end forall
          dijmat = dijmat**2
          dijmat = sum(dijmat)
          ntmp = 0
          do ii = 1,size(colvec),1
          if (colvec(ii) .EQ. kk) then
              ntmp = ntmp + 1
              sbloc(ntmp) = dijmat(ii) .LT. rorb1(ii) + rorb2(ii)
          end if
      end do
      
      return
      end subroutine calcLocalBooleanOverlap
      end module calcLocalBooleanOverlapMod
