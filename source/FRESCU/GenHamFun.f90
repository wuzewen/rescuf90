! GenHamFun.f90
    
!***************************************************
    !
    !
    !
!***************************************************
    
    module GenHamFun_module
    contains
    subroutine GenHamFun(FRC,kpt,spin,gpu,X,ham,hamu,hamd)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use getVtauloc_module
    use genVnlop_module
    use genVnlOp_Sparse_module
    use genVnlOp_BLAS_module
    use genDenseVnlOp_module
    use genVlocop_module
    use genLaplFun_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8                          :: kpt(3)
    logical                         :: gpu
    integer                         :: spin
    complex*16, allocatable         :: X(:,:)
    
    ! temporary variables
    integer             :: ispin, ntmp, mtmp, cgridn(3)
    integer             :: n
    complex*16          :: a
    real*8, allocatable :: vtauloc(:), vnlPvloc(:,:), vnld(:,:), vnlu(:,:)
    real*8, allocatable :: vnlju(:,:), vnljd(:,:), vlocu(:,:), vlocd(:,:), cLkfunu(:,:), cLkfund(:,:)
    complex*16, allocatable :: vlocfun(:,:), out2(:,:), fLkfun(:,:)
    real*8                  :: avec(3,3)
    complex*16, allocatable :: vloc(:,:), xu(:,:), xd(:,:), cLkfun(:,:)
    complex*16, allocatable :: vnl(:,:)
    
    ! output variables
    complex*16, allocatable             :: ham(:,:), hamu(:,:), hamd(:,:)
    
    ! body of this function
    !ham    = 0.0D0
    ispin  = FRC%spin%ispin
    ntmp   = size(FRC%potential%vloc,1)
    mtmp   = size(FRC%potential%vloc,1)
    if (ispin == 1 .or. ispin == 2) then
        !allocate(vloc(ntmp,1))

        !write(*,*) "In genHamFun, size of vloc"
        !write(*,*)  size(vloc,1), size(vloc,2)
        !write(*,*) "size of pot%vloc"
        !write(*,*)  size(FRC%potential%vloc,1),size(FRC%potential%vloc,2)
        !vloc(:,1) = FRC%potential%vloc(:,spin)
    else
        write(*,*) "Error in GenHamFun.f90. Ispin should not be greater than 2."
        stop
        !allocate(vloc(ntmp,mtmp))
        !vloc    = FRC%potential%vloc
    end if
    
    if (size(FRC%potential%vtau%vata) /= 0) then
        !call getVtauloc(FRC,spin,vtauloc)
        !avec    = FRC%domain%latvec
        !cgridn  = FRC%domain%cgridn
        !vtauloc = vtauloc+1
    else
        !call GenLaplFun(FRC,kpt,gpu,cLkfun,nonsense)
    end if
    
    if (FRC%interpolation%vloc) then
        write(*,*) "Error in GenHamFun.f90. FRC%interpolation%vloc should be flase now."
        stop
        !if (ispin == 1 .or. ispin == 2) then
        !    call genVnlVloc(FRC,vloc,gpu,X,vnlPvloc)
        !    if (size(FRC%potential%vtau%vata) /= 0) then
        !        call genTauKinOp(FRC,avec,cgridn,kpt,gpu,clkfun)
        !    else
        !        call GenLaplFun(FRC,kpt,gpu,X,clkFun)
        !    end if
        !    ham = -0.5*cLkfun+vnlPvloc
        !end if
    else
        if (ispin == 1 .or. ispin == 2) then
            if (FRC%potential%vnlScheme) then
                    if (FRC%potential%vnl%KBorbIsSparse) then
                            !call genVnlOp_Sparse(FRC,gpu,X,vnl)
                            call genVnlOp_BLAS(FRC,gpu,X,vnl)
                            !call genVnlOp_BLAS(FRC,gpu,X,ham)
                    else
                            !call genVnlOp(FRC,gpu,X,ham)
                            call genVnlOp(FRC,gpu,X,vnl)
                    end if
            else
                !call genDenseVnlOp(FRC,kpt,X,ham)
                call genDenseVnlOp(FRC,kpt,X,Vnl)
            end if
            !write(*,*) 'in genHF. after vnl'
            !call genVlocOp(FRC,vloc,gpu,X,Xu,Xd,vlocfun,out2)
            !call genVlocOp(FRC,vloc,gpu,X,Xu,Xd,vlocfun)
            call genVlocOp(FRC,FRC%potential%vloc,gpu,X,Xu,Xd,vlocfun)
            !call genVlocOp(FRC,FRC%potential%vloc,gpu,X,Xu,Xd,ham)
            !write(*,*) 'in genHF. after vloc'
            if (size(FRC%potential%vtau%vata) /= 0) then
                write(*,*) "Error in GenHamFun.f90. FRC%potential%vtau should not exist for now."
                stop
                !call genTauKinOp(FRC,avec,cgridn,kpt,gpu,clkfun)
            else
                !call GenLaplFun(FRC,kpt,gpu,X,ham)
                call GenLaplFun(FRC,kpt,gpu,X,clkFun)
                    !call GenLaplFun(FRC,kpt,gpu,X,clkFun,fLkfun)
            end if
            !write(*,*) 'in genHF. after lapl'

            !write(*,*) "I'm here in GenHamFun. rank", FRC%mpi%rank
            !if (FRC%mpi%rank == 1) then
                !write(*,*) "X ="
                !write(*,*)  X
                !write(*,*) "vnl ="
                !write(*,*)  vnl
                
                !write(*,*) "vlocfun ="
                !write(*,*)  vlocfun
                !write(*,*) "clkFun ="
                !write(*,*)  clkFun
            !end if

            !stop
            !a = (-0.5D0,0.0D0)
            !n = size(ham)
            !ham = vlocfun
            !call zaxpy(n,a,cLkFun,1,ham,1)
            !a = (1.0D0,0.0D0)
            !call zaxpy(n,a,vnl,1,ham,1)
            ham = Vnl-0.5D0*cLkFun+vlocfun
            deallocate(vnl,cLkFun,vlocfun)
            !ham = -0.5D0*cLkFun+vnl+vlocfun
        else if (ispin == 4) then
            write(*,*) "Error in GenHamFun.f90. Non-collinear spin is not available now."
            stop
            !call genVnlOp(FRC,gpu,X,vnlu)
            !call genVnlOp(FRC,gpu,X,vnld)
            !call genVlocOp(FRC,vloc,gpu,xu,xd,vlocu,vlocd)
            !if (FRC%spin%SOI) then
            !    call genVnlSO(FRC,xu,xd,vnlju,vnljd)
            !    vlocu = vnlju+vlocu
            !    vlocd = vnljd+vlocd
            !end if
            !if (size(FRC%potential%vtau%vata) /= 0) then
            !    call genTauKinOp(FRC,avec,cgridn,kpt,gpu,clkfunu)
            !    call genTauKinOp(FRC,avec,cgridn,kpt,gpu,clkfund)
            !else
            !    call GenLaplFun(FRC,kpt,gpu,X,clkFunu)
            !    call GenLaplFun(FRC,kpt,gpu,X,clkFund)
            !end if
            !hamu = -0.5*clkFunu+vnlu+vlocu
            !hamd = -0.5*clkfund+vnld+vlocd
        end if
    end if
    
    return
    end subroutine GenHamFun
    end module GenHamFun_module
            
