! FRC.f90
    
!******************************************************************************
    !
    ! This file contains all the types of data used during the calculation
    !
!******************************************************************************
    
    module FORTRAN_RESCU_CALCULATION_TYPE
    
    use infoType_module
    use mpiType_module
    use smiType_module
    use initType_module
    use optionType_module
    use atomType_module
    use domainType_module
    use unitsType_module
    use elementType_module
    use SummeryOfBas_module
    use ElementDataType_module
    use spinType_module
    use LCAOType_module
    use diffOpType_module
    use symmetryType_module
    use kpointType_module
    use smearingType_module
    use DOStype_module
    use eigenSolverType_module
    use partialRRtype_module
    use functionalType_module
    use potentialType_module
    use interpolationType_module
    use FRC_RHO_TYPE
    use EnergyType_module
    use GPUtype_module
    use mixingType_module
    use forceType_module
    use VataMN_2D_module
    use VataMN_CMPLX_2D_module
    use ExxType_module
    use constants_module

    use, intrinsic :: iso_c_binding
    include 'fftw3.f03'
    
    type :: inteType
        integer, allocatable :: vata(:)
    end type inteType
     
    type :: realType
        real*8,  allocatable :: vata(:)
    end type realType
    
    type :: FORTRAN_RESCU_CALCULATION
        type(C_PTR)             :: planfft, planifft
        integer(C_INT)          :: L, M, N
        !complex(C_DOUBLE_COMPLEX), dimension(L,M,N) :: Arr, Brr
        complex*16, allocatable :: Arr(:,:,:), Brr(:,:,:)

        character(len=20)       :: version
        integer                 :: scloop
        type(constantsType)     :: constants
        type(infoType)          :: info
        type(mpiType)           :: mpi
        type(smiType)           :: smi
        type(initType)          :: init
        type(optionType)        :: option
        type(AtomType)          :: Atom
        type(DomainType)        :: Domain
        type(unitsType)         :: units
        type(spinType)          :: spin
        type(LCAOtype)          :: LCAO
        type(diffOpType)        :: diffOp
        type(symmetryType)      :: symmetry
        type(kpointType)        :: kpoint
        type(smearingType)      :: smearing
        type(DOStype)           :: DOS
        type(eigenSolverType)   :: eigenSolver
        type(partialRRtype)     :: partialRR
        type(functionalType)    :: functional
        type(potentialType)     :: potential
        type(interPolationType) :: interpolation
        type(FRCrhoType)        :: rho
        type(EnergyType)        :: Energy
        type(GPUtype)           :: GPU
        type(mixingType)        :: mixing
        type(forceType)         :: force
        type(ExxType)           :: Exx
        type(VataMN_CMPLX_2D)   :: tmp3D
        type(VataMN_CMPLX_2D), allocatable :: psi(:,:)
        type(elementType),     allocatable :: element(:)
        type(SummeryOfBas),    allocatable :: SOB(:)
        type(ElementDataType), allocatable :: ElementData(:)
    end type FORTRAN_RESCU_CALCULATION
    
    end module FORTRAN_RESCU_CALCULATION_TYPE
