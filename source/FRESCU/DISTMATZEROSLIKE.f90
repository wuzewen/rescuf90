! DISTMATZEROSLIKE.f90

!***************************************************************************
      !
      !
      !
!*******************************************************************************

      module DISTMATZEROSLIKEMOD

      interface DISTMATZEROSLIKE
              module procedure distmat_zeroslike_real_1D
              module procedure distmat_zeroslike_real_2D
              module procedure distmat_zeroslike_real_3D
              module procedure distmat_zeroslike_cmplx_1D
              module procedure distmat_zeroslike_cmplx_2D
              module procedure distmat_zeroslike_cmplx_3D
      end interface DISTMATZEROSLIKE

      end module DISTMATZEROSLIKEMOD
