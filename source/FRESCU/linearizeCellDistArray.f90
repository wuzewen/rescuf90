! linearizeCellDistArray.f90

!**************************************************************************************
      !
      !
      !
!**************************************************************************************

      module linearizeCellDistArrayMod
      contains
      subroutine linearizeDistArray(CellArray,buffersize,spthres,XXdat)

      use

      implicit none

      call cellfunNNZ(cellArray,nnzDatatmp)
      nnzData = sum(nnzDatatmp)

      if (size(CellArray) .LT. nnzData*24+1) then
              return
      end if

      call CellfunNum(CellArray,numDatatmp)
      numData = sum(numDatatmp)
      spfrac  = nnzData/numData

      if (spfrac .LE. spthres) then
              ncall  = size(CellArray)
              XXdat%m = cellArray%m
              XXdat%n = cellArray%n
              XXdat%mblock = CellArray%mblock
              XXdat%nblock = CellArray%nblock
              XXdat%mproc  = CellArray%mproc
              XXdat%nproc  = CellArray%nproc
              sparse
      else
              call Cellreshape(cellArray,1,XXdat)
      end if

      call cat(XXdat)

      return
      end subroutine linearizeDistArray
      end module linearizeCellDistArrayMod
