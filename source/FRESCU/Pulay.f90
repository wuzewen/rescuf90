! pulay.f90

!*******************************************
      !
      !
      !
!**************************************************

      module PulayMod
      contains
      subroutine Pulay(FRC,mu_m,vu_m,mu_mp1)

      use

      implicit none

      mpistat  = FRC%mpi%status
      iter     = FRC%scloop
      betamix  = FRC%mixing%beta
      initlin  = FRC%mixing%initlin
      invJ0    = FRC%mixing%invJ0
      ipScheme = FRC%mixing%ipScheme
      maxh     = FRC%mixing%history
      WI       = FRC%mixing%WI

      mu_0     = mu_m
      F_m      = vu_m-mu_m
      DF       = FRC%mixing%DF
      u        = FRC%mixing%u

      if (iter .GT. maxh) then
              forall(ii=1:size(DF,2)-1)
                      DFT(:,ii) = DF(:,ii)
                      ut(:,ii)  = u(:,ii)
              end forall
              forall(ii=1:size(FINF,1),jj=1:size(FINF,2))
                      FINF(ii,jj) = FRC%mixing%FINF(ii+1,jj+1)
              end forall
              FRC%
      end if

      if (iter .LE. initlin) then
              mu_mp1 = batalin*F_m
              if (strim(ijvJ0) .EQ. 'kerker') then
                      call mixingMetricWrapper(FRC,mu_0,mu_mp1)
              else if (strim(ijvJ0) .EQ. 'kerker2D') then
                      call mixingMetricWrapper(FRC,mu_0,mu_mp1)
              else if (strim(ijvJ0) .EQ. 'linlin') then
                      call mixingMetricWrapper(FRC,mu_0,mu_mp1)
              else if (strim(ijvJ0) .EQ. 'dm') then
                      call mixingMetricWrapper(FRC,mu_0,mu_mp1)
              end if
              mu_mp1 = mu_M + MU_MP1
              MF_m   = F_m
              if (trim(ipSheme) 'EQ' 'nonuniform') then
                      call mixingMetricWrapper(FRC,mu_0,F_m,MF_m)
              else (trim(ipSheme) 'EQ' 'nonuniformdm') then
                      call mixingMetricWrapper(FRC,mu_0,F_m,MF_m)
              end if
      else
              mu_mm1 = FRC%mixing%mu_mm1
              F_mm1  = FRC%mixing%F_mm1
              MF_mm1 = FRC%mixing%MF_mm1
              MF_m   = F_m
              if (trim(ipSheme) 'EQ' 'nonuniform') then
                      call mixingMetricWrapper(FRC,mu_0,F_m,MF_m)
              else (trim(ipSheme) 'EQ' 'nonuniformdm') then
                      call mixingMetricWrapper(FRC,mu_0,F_m,MF_m)
              end if
              MY  = MF_m - MF_mm1
              Y   = F_m  - F_mm1
              yty = sum(conjg(MY)*Y)
              yty = 1.0D0/sqrt(yty)
              MY  = yty*MY
              Y   = yty*Y
              DF  = 
              U   = 
              tmp = betamix*y
              if (trim(invJ0) .EQ. 'kerker') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              else if (trim(invJ0) .EQ. 'kerker2D') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              else if (trim(invJ0) .EQ. 'linlin') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              else if (trim(invJ0) .EQ. 'dm') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              end if

              u = 

              if (iter-initlin .EQ. 1) then
                      FINF = 0.0D0
              else
                      FINF = 0.0D0
                      forall(ii=1:,jj=1:)
                              FINF(ii,jj) = FRC%mixingFINF(ii,jj)
                      end forall
              end if
              tmp = matmul(transpose(conjg(DF)),MY)

              if (mpistat) then
                      call MPI_Allreduce_sum(tmp)
              end if
              FINF(:,*) = tmp
              FINF(*,:) = tmp
              FINF(*,*) = 1.0D0
              forall(ii=1:)
                      AMAT(ii,ii) = 1.0D0
              end forall
              AMAT = AMAT + FINF*WI**2
              mu_mp1  = mu_m
              tmp = matmul(transpose(conjg(DF)),MF_m)

              if (mpistat) then
                      call MPI_Allreduce_sum(tmp)
              end if
              call inversion()
              tmp = matmul(,tmp)
              if (mpistat) then
                      call MPI_Bcast_variable(tmp)
              end if
              mu_mp1 = mu_mp1 -matmul(u,WI**2*tmp)
              tmp = betamix*F_m

              if (trim(invJ0) .EQ. 'kerker') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              else if (trim(invJ0) .EQ. 'kerker2D') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              else if (trim(invJ0) .EQ. 'linlin') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              else if (trim(invJ0) .EQ. 'dm') then
                      call mixingMetricWrapper(FRC,mu_0,tmp)
              end if

              mu_mp1 = mu_mp1 + tmp
              FRC%mixing%DF   = DF
              FRC%mixing%FINF = FINF
              FRC%mixing%u    = u
      end if
      FRC%mixing%mu_mm1 = mu_m
      FRC%mixing%F_mm1  = F_m
      FRC%mixing%MF_mm1 = MF_m
      mu_mp1            = reshape(mu_mp1,)

      return
      end subroutine Pulay
      end subroutine PulayMod

