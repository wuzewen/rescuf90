! bsxfunTimes_3D.f90
!******************************************************************************
    ! This function has the same function with function bsxfun(@times,*,*)
    ! in matlab. While, here a should be two dimentional matrix, and b sh-
    ! ould be an array.
!******************************************************************************
    
    module bsxfunTimes_3D_module
    contains
    subroutine bsxfunTimes_3D(MatrixA,MatrixB,MatrixC)
    
    use bsxfunTimes_module
    
    implicit none
    
    ! input valuables
    real*8,allocatable :: MatrixA(:,:,:), MatrixB(:,:,:)
    
    ! temporery valuables
    real*8,allocatable :: tmpA(:,:), tmpB(:,:), tmpC(:,:)
    integer            :: mA, nA, lA, mB, nB, lB
    integer            :: ii, jj, kk

    ! output valuables
    real*8,allocatable :: MatrixC(:,:,:)
    
    ! body of this function
    mA = size(MatrixA,1)
    nA = size(MatrixA,2)
    lA = size(MatrixA,3)
    mB = size(MatrixB,1)
    nB = size(MatrixB,2)
    lB = size(MatrixB,3)
    
    if (mA == 1 .and. mB == 1) then
        allocate(tmpA(nA,lA))
        allocate(tmpB(nB,lB))
        allocate(tmpC(max(nA,nB),max(lA,lB)))
        call bsxfunTimes(tmpA,tmpB,tmpC)
        MatrixC(1,:,:) = tmpC(:,:)
    else if (nA == 1 .and. nB == 1) then
        allocate(tmpA(mA,lA))
        allocate(tmpB(mB,lB))
        allocate(tmpC(max(mA,mB),max(lA,lB)))
        call bsxfunTimes(tmpA,tmpB,tmpC)
        MatrixC(:,1,:) = tmpC(:,:)
    else if (lA == 1 .and. lB == 1) then
        allocate(tmpA(mA,nA))
        allocate(tmpB(mB,nB))
        allocate(tmpC(max(mA,mB),max(nA,nB)))
        call bsxfunTimes(tmpA,tmpB,tmpC)
        MatrixC(:,:,1) = tmpC(:,:)
    else if (mA == mB) then
        do ii = 1,mA,1
            MatrixC(ii,:,:) = MatrixA(ii,:,:)+MatrixB(ii,:,:)
            allocate(tmpA(nA,lA))
            allocate(tmpB(nB,lB))
            allocate(tmpC(max(nA,nB),max(lA,lB)))
            call bsxfunTimes(tmpA,tmpB,tmpC)
            MatrixC(ii,:,:) = tmpC(:,:)
        end do
    else if (nA == nB) then
        do ii = 1,nA,1
            MatrixC(:,ii,:) = MatrixA(:,ii,:)+MatrixB(:,ii,:)
            allocate(tmpA(mA,lA))
            allocate(tmpB(mB,lB))
            allocate(tmpC(max(mA,mB),max(lA,lB)))
            call bsxfunTimes(tmpA,tmpB,tmpC)
            MatrixC(:,ii,:) = tmpC(:,:)
        end do
    else if (lA == lB) then
        do ii = 1,lA,1
            MatrixC(:,:,ii) = MatrixA(:,:,ii)+MatrixB(:,:,ii)
            allocate(tmpA(mA,nA))
            allocate(tmpB(mB,nB))
            allocate(tmpC(max(mA,mB),max(nA,nB)))
            call bsxfunTimes(tmpA,tmpB,tmpC)
            MatrixC(:,:,ii) = tmpC(:,:)
        end do
    else
        write(*,*) "Error in bsxfunTimes_3D.f90. The sizes of the inputs is not suitable."
    end if
    
    
    
    !if (mA == 1 .and. nB == 1) then
    !    do ii = 1,nA,1
    !        do jj = 1,mB,1
    !            MatrixC(jj,ii) = MatrixA(1,ii)*MatrixB(jj,1)
    !        end do
    !    end do
    !else if (nA == 1 .and. mB == 1) then
    !    do ii = 1,mA,1
    !        do jj = 1,nB,1
    !            MatrixC(ii,jj) = MatrixA(ii,1)*MatrixB(1,jj)
    !        end do
    !    end do
    !else if (mA == 1 .and. nA == nB) then
    !    do ii = 1,nA,1
    !        do jj = 1,mB,1
    !            MatrixC(jj,ii) = MatrixA(1,ii)*MatrixB(jj,ii)
    !        end do
    !    end do
    !else if (nA == 1 .and. mA == mB) then
    !    do ii = 1,mA,1
    !        do jj = 1,nB
    !            MatrixC(ii,jj) = MatrixA(ii,1)*MatrixB(ii,jj)
    !        end do
    !    end do
    !else if (mB == 1 .and. nA == nB) then
    !    do ii = 1,nB,1
    !        do jj = 1,mA,1
    !            MatrixC(jj,ii) = MatrixB(1,ii)*MatrixA(jj,ii)
    !        end do
    !    end do
    !else if (nB == 1 .and. mB == mA) then
    !    do ii = 1,mB,1
    !        do jj = 1,nA,1
    !            MatrixC(ii,jj) = MatrixB(ii,1)*MatrixA(ii,jj)
    !        end do
    !    end do
    !else if (mA == mB .and. nA == nB) then
    !    MatrixC = MatrixA*MatrixB
    !else
    !    write(*,*) "Error in bsxfunTimes.f90."
    !    stop
    !end if
    
    return
    end subroutine bsxfunTimes_3D
    end module bsxfunTimes_3D_module
        