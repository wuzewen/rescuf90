! greatGfunction.f90
    
!************************************************************
    !
    !
    !
!************************************************************
    
    module greatGfunction_module
    contains
    subroutine greatGfunction(order,k,rs,f,dfdrs,d2fdrs2)
    
    implicit none
    
    ! input variables
    integer             :: order, k
    real*8, allocatable :: rs(:,:)
    
    ! temporary variables
    integer :: k2, ntmp
    real*8 :: pp(3), a(3), alpha(3), beta(3,4), q10, dq0
    real*8, allocatable :: q0(:), q1(:), q2(:), auxl(:), dq1(:)
    
    ! output variables
    real*8, allocatable :: f(:), dfdrs(:), d2fdrs2(:)
    
    ! body of this function
    k2        = k+1
    pp        = 1.000000D0
    a(1)      = 0.031091D0
    a(2)      = 0.015545D0
    a(3)      = 0.016887D0
    alpha(1)  = 0.213700D0
    alpha(2)  = 0.205480D0
    alpha(3)  = 0.111250D0
    beta(1,1) = 7.595700D0
    beta(1,2) = 3.587600D0
    beta(1,3) = 1.638200D0
    beta(1,4) = 0.492940D0
    beta(2,1) = 14.11890D0
    beta(2,2) = 6.197700D0
    beta(2,3) = 3.366200D0
    beta(2,4) = 0.625170D0
    beta(3,1) = 10.35700D0
    beta(3,2) = 3.623100D0
    beta(3,3) = 0.880260D0
    beta(3,4) = 0.496710D0
    ntmp      = size(rs,1)
    allocate(q0(ntmp))
    q0        = -2.0D0*a(k2)*(1.0D0+alpha(k2)*rs(:,2))
    q10       =  2.0D0*a(k2)
    allocate(q1(ntmp))
    q1        =  q10*(beta(k2,1)*rs(:,1)+beta(k2,2)*rs(:,2)+beta(k2,3)*rs(:,1)*rs(:,2)+beta(k2,4)*(rs(:,2)**(1.0D0+pp(k2))))
    allocate(q2(ntmp))
    q2        =  log(1.0D0+1.0D0/q1)
    allocate(f(ntmp))
    f         =  q0*q2
    
    if (order<1) then
        return
    end if
    allocate(auxl(ntmp))
    auxl  =  q1*(1.0D0+q1)
    !allocate(dq0(ntmp))
    dq0   = -2.0D0*a(k2)*alpha(k2)
    allocate(dq1(ntmp))
    dq1   =  a(k2)*(beta(k2,1)/rs(:,1)+2.0D0*beta(k2,2)+3.0D0*beta(k2,3)*rs(:,1)+2.0D0*(1.0D0+pp(k2))*beta(k2,4)*rs(:,2)**pp(k2))
    allocate(dfdrs(ntmp))
    dfdrs =  dq0*q2-q0*dq1/auxl
    
    if (order<2) then
        return
    end if
    
    write(*,*) "Error in greatGfunction.f90. order should be 1 now."
    stop
    !d2q2    =  a(k)*(-beta(k,1)/(2*rs(1,:)*rs(2,:))+3.0*beta(k,3)/rs(1,:)/2+2.0*(1.0+pp(k))*beta(k,4)*rs(2,:)**(pp(k)-1))
    !d2fdrs2 =  1/aux1*(-2*dq0*dq1-q0*d2q1+q0*(2*q1+1)*dq1*dq1/aux1)
    
    !if (order < 3) then
    !    return
    !end if
    
    return
    end subroutine greatGfunction
    end module greatGfunction_module
