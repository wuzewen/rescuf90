!  GetPartialCoreDensity.f90 
!
!  FUNCTIONS:
!  GetPartialCoreDensity - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: GetPartialCoreDensity
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module GetPartialCoreDensity_module
    contains
    subroutine GetPartialCoreDensity(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use parseFuncName_module
    use ModBCDist_module
    use ModBCDist_2D_module
    use VataMN_2D_module
    use getgridpointcoord_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    logical              :: mpistat, calcGrad!, isgga, ismgga, ishyb
    logical, allocatable :: isgga(:), ismgga(:), ishyb(:)
    integer              :: mpirank, mpisize, natom, fn, fgridn(3), n, ii
    integer :: ntmp, npoint
    real*8  :: lvec(3,3)
    integer, allocatable :: indep(:,:)
    character(len=20), allocatable   :: faprx(:)
    real*8, allocatable  :: laplRho(:,:), GridCoord(:,:)
    type(VataMN_2D)      :: rho, rhotmp, gradRhotmp, gradRho
    
    ! output variables

    ! Body of GetPartialCoreDensity
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    natom   = FRC%atom%numberOfAtom
    lvec    = FRC%domain%latvec
    fgridn  = FRC%domain%fgridn
    fn      = product(fgridn)
    allocate(isgga(2),ismgga(2),ishyb(2))
    isgga   = .FALSE.
    ismgga  = .FALSE.
    ishyb   = .FALSE.
    FRC%option%initParaReal = .TRUE.
    call parseFuncName(FRC%functional%list,faprx)
    n = size(faprx)
    do ii = 1,n,1
        if (trim(faprx(ii)) == "GGA") then
            isgga = .TRUE.
        else if (trim(faprx(ii)) == "mGGA") then
            ismgga = .TRUE.
            write(*,*) "Error in GetPartialCoreDensity.f90, mGGA is not available."
            stop
        else if (trim(faprx(ii)) == "HYB") then
            ishyb = .TRUE.
            write(*,*) "Error in GetPartialCoreDensity.f90, Hybrid functional is not available."
            stop
        end if
    end do
    
    if (any(isgga .EQV. .TRUE.) .or. any(ismgga .EQV. .TRUE.) .or. & 
            any(ishyb .EQV. .TRUE.) .or. FRC%force%status) then
        calcGrad = .TRUE.
    end if
    
    if (calcGrad) then
        
        write(*,*) "WARNING. calcGrad is true. But calcGradRad will not be processed"
        !call calcGradRad(FRC%ElementData)
    end if
    
    if (any(ismgga .EQV. .TRUE.)) then
        
        write(*,*) "Error in GetPartialCoreDensity.f90. mGGA is not available now."
        
        !call calcGradRad(FRC%elementData)
        !call calcLaplRad(FRC%elementData)
    end if
    
    if (FRC%option%initParaReal) then
        
        !write(*,*) "Error in GetPartialCoreDensity.f90. FRC%option%initParaReal should be false."
        !stop
        
        !npoint = 
        !allocate(GridCoord(npoint))
        ntmp = ceiling(dble(fn)/dble(mpisize))
        call GetGridPointCoord(FRC,fgridn,ntmp,GridCoord,indep)
        npoint = size(GridCoord,1)
        call InitDistArray_2D(fn,1,ntmp,1,mpisize,1,rho)
        allocate(rho%vata(npoint,1))
        rho%vata = 0.0D0
        !if (calcGrad) then
        !    call initDistArray(fn,3,fn/mpisize,1,mpisize,1,gradRho)
        !    allocate(gradRho%datav(npoint,3))
        !    gradRho%dataA = 0
        !end if
        !if (any(ismgga)) then
        !    call initDistArray(fn,1,ceiling(fn,mpisize),1,mpisize,1,laplRho)
        !    allocate(laplRho%dataA(npoint,1))
        !    laplRho%dataA = 0
        !end if
        
        !do ii = 1,natom,1
        !    thpeE   = FRC%atom%element(ii)
        !    pos     = FRC%atom%xyz(ii,:)
        !    datav   = FRC%elementData(typeE)%Rpc
        !    rrData  = datav%rrData
        !    thoData = datav%rhoData
        !    call interoSph2Cart(rrData,rhoData,pos,-1,GridCoord,lvec,datavtmp)
        !    rho%datav = rho%datav + datavtmp
        !    if (calcGrad) then
        !        gradData = datav%gradData
        !        call interoSph2Cart(rrData,gradData,pos,-1,GridCoord,lvec,datavtmp)
        !        gradRho%datav = gradRho%datav + datavtmp
        !    end if
        !    if (ismgga) then
        !        laplData = datav%laplData
        !        call interpSph2Cart(rrData,gradData,pos,-1,GridCoord,lvec,datavtmp)
        !        glaplRho%datav = laplRho%datav + datavtmp
        !    end if
        !end do
    else
        npoint = fn
        allocate(rhotmp%vata(fn,1))
        rhotmp%vata = 0.0D0
        if (calcGrad) then
            allocate(gradRho%vata(fn,3))
            gradRho%vata = 0.0D0
        end if
        if (any(ismgga)) then
            
            write(*,*) "Error in GetPartialCoreDensity.f90. mGGA is not available."
            stop
            
            !allocate(laplRho(fn,1))
            !laplRho = 0
        end if
        !do ii = mpirank+1,natom,mpisize
        !    typeE    = FRC%atom%element(ii)
        !    if (size(FRC%ElementData(typeE)%Rpc)>0) then
        !        pos(1,:) = FRC%atom%xyz(ii,:)
        !        datav    = FRC%ElementData(typeE)%Rpc
        !        rrData   = 
        !        call InterpRdist2Cart(datav,"rrData","rhoData",pos,-1,lvec,fgridn,rhotmp)
        !        rho = rho+rhotmp
        !        if (calcGrad) then
        !            call InterpRdist2Cart(datav,"rrData","gradData",pos,-1,lvec,fgridn,gradRhotmp)
        !            gradRho = gradRho+gradRhotmp
        !        end if
        !        if (ismgga) then
        !            call InterpRdist2Cart(datav,"rrData","gradData",pos,-1,lvec,fgridn,laplRhotmp)
        !            laplRho = laplRho+laplRhotmp
        !        end if
        !    end if
        !end do
        
        if (mpistat) then
            
            write(*,*) "Error in GetPartialCoreDensity.f90. MPI is not available now."
            !stop
            
            !call MPI_Allreduce_sum(rho)
        end if
        !rhotmp(:) = rho(:,1)
        call initDistArray_2D(fn,1,fn,1,1,mpisize,rhotmp)
        ntmp = ceiling(dble(fn)/dble(mpisize))
        call ModBCDist_2D(FRC,rhotmp,ntmp,1,mpisize,1,.FALSE.,.FALSE.,rho)
        !rho(:,1) = rhotmp(:)
        
        if (calcGrad) then
            
            if (mpistat) then
                
                write(*,*) "Error in GetPartialCoreDensity.f90. MPI is not available."
                !stop
                
                !call MPI_Allreduce_sum(gradRho)
            end if
            
            call initDistArray_2D(fn,3,fn,3,1,mpisize,gradRhotmp)
            ntmp = ceiling(dble(fn)/dble(mpisize))
            call ModBCDist_2D(FRC,gradRhotmp,ntmp,1,mpisize,1,.FALSE.,.FALSE.,gradRho)
        end if
        
        if (any(ismgga)) then
            
            write(*,*) "Error in GetPartialCoreDensity.f90. mGGA is not avalaible now."
            stop
            
            !if (mpistat) then
            !    
            !    write(*,*) "Error in GetPartialCoreDensity.f90. MPI is not available now."
            !    stop
            !    
            !    !call MPI_Allreduce_sum(laplRho)
            !end if
            !call initDistArray(fn,3,fn,3,1,mpisize,laplRho)
            !call ModBCDist(FRC,gradRho,fn/mpisize,1,mpisize,1,laplRho)
        end if
    end if
    
    allocate(FRC%rho%pc%vata(npoint,1))
    FRC%rho%pc = rho
    if (calcGrad) then
        allocate(FRC%rho%gradRpc%vata(fn,3))
        FRC%rho%gradRpc = gradRho
    end if
    if (any(ismgga .EQV. .TRUE.)) then
        
        write(*,*) "Error in GetPartialCoreDensity.f90. mGGA is not avalaible."
        stop
        
        !FRC%rho%lapaRpc = laplRho
    end if

    return
    end subroutine GetPartialCoreDensity
    end module GetPartialCoreDensity_module

