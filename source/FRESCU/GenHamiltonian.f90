!  GenHamiltonian.f90 
!****************************************************************************
!
!  PROGRAM: GenHamiltonian
!
!  PURPOSE: Generate Hamiltonian for the first SCF loop.
!
!****************************************************************************

    module GenHamiltonian_module
    contains
    subroutine GenHamiltonian(FRC)
    
    ! data modules
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ElementDataType_module
    use GetKBorb_module
    use GetKBorb_Sparse_Init_module
    
    ! function modules
    use GenGradient_module
    use Genlaplacian_module
    use genNeutralAtomPotential_module
    use GetNeutralAtomDensity_module
    use ModBCDist_1D_module
    use distmat_bsxfun_Minus_module
    use GetNonLocalPotentialK_module
    use GetNonLocalPotentialK_Sparse_module
    use genKBProjDense_module
    use getPartialCoreDensity_module
    use distmat_gather_module
    use getHartreePotential_module
    use xcMain_module
    use UpdateVeff_module
    use UpdateVeff2D_module
    use uniqueNumberInteger_module

    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    !type(ElemetDataType)            :: 
    
    ! temporery variables
    integer             :: bc(3), ntmp1, ntmp2!, ii, jj
    integer             :: clock1, clock2
    integer             :: crate1, crate2
    integer             :: cmax1,  cmax2
    integer             :: ii
    logical             :: shortRange, antloc
    real*8              :: tmp(3)
    real                :: Dtime
    integer             :: mpirank, mpisize, veffn
    integer, allocatable :: inde(:,:)
    !real*8, allocatable :: vmean(:,:,:)
    type(VataMN_2D)     :: rho, veffout
    type(VataMN_1D)     :: vps, rhotmp
    character(len=20)    :: flax, numb
    integer :: dou
    !real*8, allocatable :: vps2(:)
    
    ! output variables
    

    ! Body of GenHamiltonian
    shortRange = FRC%option%shortRange
    mpirank    = FRC%mpi%rank
    mpisize    = FRC%mpi%mpisize
    bc         = FRC%domain%boundary

    !write(numb,"(i4.4)") mpirank
    !flax = 'GenH'//trim(numb)//'.txt'
    !dou  = mpirank+10
    !open(unit=dou,file=flax)
    !write(dou,*) 'process', mpirank

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate gradient operateors.                                          %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    call system_clock(clock1,crate1,cmax1)
    call GenGradient(FRC)               ! generate gradiant operator
    call system_clock(clock2,crate2,cmax2)
    Dtime      = real(clock2-clock1)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating gradient operators:                               ", Dtime
    end if

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate Laplacian operarors.                                          %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    call Genlaplacian(FRC)              ! generate laplacian operator
    call system_clock(clock1,crate1,cmax1)
    Dtime      = real(clock1-clock2)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating laplacian operators:                              ", Dtime
    end if
    !call GenInterpolator(FRC)           ! generate interpolation operator
    !call GenIntFun(FRC)
    !write(*,*) "I'm here, after GenGradient and GenLaplacian."   
    !write(*,*) "Generating finite difference operators:                         **.****"
    !open(unit=11,file='Gradient.txt')
    !write(dou,*) 'FRC%diffop%Du ='
    !write(dou,*)  FRC%diffop%Du
    !write(dou,*) 'FRC%diffop%Dv ='
    !write(dou,*)  FRC%diffop%Dv
    !write(dou,*) 'FRC%diffop%Dw ='
    !write(dou,*)  FRC%diffop%Dw
    !write(dou,*) 'FRC%diffop%fDu ='
    !write(dou,*)  FRC%diffop%fDu
    !write(dou,*) 'FRC%diffop%fDv ='
    !write(dou,*)  FRC%diffop%fDv
    !write(dou,*) 'FRC%diffop%fDw ='
    !write(dou,*)  FRC%diffop%fDw
    !close(11)
    !open(unit=11,file='Laplacian.txt')
    !write(dou,*) 'FRC%diffop%Duu ='
    !write(dou,*)  FRC%diffop%Duu
    !write(dou,*) 'FRC%diffop%Dvv ='
    !write(dou,*)  FRC%diffop%Dvv
    !write(dou,*) 'FRC%diffop%Dww ='
    !write(dou,*)  FRC%diffop%Dww

    !write(dou,*) 'FRC%diffop%fDuu ='
    !write(dou,*)  FRC%diffop%fDuu
    !write(dou,*) 'FRC%diffop%fDvv ='
    !write(dou,*)  FRC%diffop%fDvv
    !write(dou,*) 'FRC%diffop%fDww ='
    !write(dou,*)  FRC%diffop%fDww
    !close(11)

    if (FRC%LCAO%status .or. trim(FRC%eigensolver%Init) == trim("LCAO")) then
        
        write(*,*) "Error in GenHamiltonian.f90. LCAO is not available now."
        stop
        
        !call calcBooleOverlapMatrix(FRC)
        !call calcOverlapAndKineticMatrix(FRC)
        !call calcNonLocalPotentialMatrix(FRC)
        !call mergeVnlKineticMatrix(FRC)
        !if (FRC%spin%SOI) then
        !    call calcProjectorMatrixSOI(FRC)
        !    call calcVnlMatrixSOI(FRC)
        !end if
    end if
    
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate neutral atom potential.                                   %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    call GenNeutralAtomPotential(FRC)   ! generate neutral atom potential
    !write(*,*) "I'm here after NeutralAtomPotential in SCF."
    !open(unit=11,file='Vna.txt')
    !write(*,*) 'FRC%potential%vna%vata'
    !write(*,*)  FRC%potential%vna%vata
    !close(11)

    call system_clock(clock2,crate2,cmax2)
    Dtime      = real(clock2-clock1)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating neutral atom potential:                           ", Dtime
    end if
    if (.not. shortRange) then
        write(*,*) "Error in GenHamiltonian.f90. shortRange should be true now."
        stop
        !vmean = sum(FRC%potential%Vna%vnaData)
        !if (FRC%mpi%status) then
        !    call MPI_ALLreduce_sum(vmean)
        !end if
        !FRC%potential%Vna%vnaData = FRC%potential%Vna%vnaData-vmean/FRC%potential%Vna%M
    end if
    !write(*,*) "I'm here before NonLocalPotential in GenHamiltonian."

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate nonlocal potential projectors.                           %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    if ((.not. FRC%LCAO%status) .or. FRC%LCAO%cfsi) then
        if (FRC%potential%vnlScheme) then
                if (.not. FRC%potential%vnl%KBorbIsSparse) then
                        call GetNonLocalPotentialK(FRC)     ! generate non-local potential (RS)
                else
                        call GetNonLocalPotentialK_Sparse(FRC)
                end if
        else
            call genKBProjDense(FRC)
        end if
        if (FRC%spin%SOI) then
            write(*,*) "Error in GenHamiltonian.f90. SOI is not available now."
            stop
            !call GetNonLocalPotentialSO(FRC)
        end if
        tmp   = 0
        !write(*,*) 'after GetNonLocalPotentialK.'
        if (.not. FRC%potential%vnl%KBorbIsSparse) then
                ntmp1 = size(FRC%potential%vnl%kbcell(1)%vata,1)
                ntmp2 = size(FRC%potential%vnl%kbcell(1)%vata,2)
                allocate(FRC%potential%vnl%KBorb(ntmp1,ntmp2))
        else
                !write(*,*) "size(FRC%potential%vnl%sparsekbcell)"
                !write(*,*)  size(FRC%potential%vnl%sparsekbcell)
                !ntmp1 = 0
                !do ii = 1,size(FRC%potential%vnl%sparsekbcell),1
                !    ntmp1 = ntmp1 + FRC%potential%vnl%sparsekbcell(ii)%N_nonzero
                !end do
                !allocate(inde(ntmp1,2))
                !ntmp1 = 0
                !ntmp2 = 0
                !do ii = 1,size(FRC%potential%vnl%sparsekbcell),1
                !    ntmp1               = ntmp2+1
                !    ntmp2               = ntmp2+FRC%potential%vnl%sparsekbcell(ii)%N_nonzero
                !    inde(ntmp1:ntmp2,1) = FRC%potential%vnl%sparsekbcell(ii)%Index1
                !    inde(ntmp1:ntmp2,2) = FRC%potential%vnl%sparsekbcell(ii)%Index2
                !end do
                !ntmp1 = 0
                !do ii = 1,size(FRC%potential%vnl%sparsekbcell),1
                !    ntmp1 = ntmp1 + FRC%potential%vnl%sparsekbcell(ii)%SIZE2
                !end do
                !call uniqueNumberInteger(inde,ntmp2)
                !if (dble(ntmp2)/dble(size(inde,1)) < 0.333333D0) then
                !        !ntmp2 = size(inde,1)
                !        FRC%potential%vnl%reduceKBcell = .TRUE.
                !else
                !        !ntmp2                          =  size(inde,1)
                !        FRC%potential%vnl%reduceKBcell = .FALSE.
                !end if
                !write(*,*) "ntmp2 =", ntmp2 
                !FRC%potential%vnl%sparseKBorb%N_nonzero = ntmp2
                !FRC%potential%vnl%sparseKBorb%SIZE1     = ntmp1
                !FRC%potential%vnl%sparseKBorb%SIZE2     = FRC%potential%vnl%sparsekbcell(1)%SIZE1
                !allocate(FRC%potential%vnl%sparseKBorb%Index1(ntmp2))
                !allocate(FRC%potential%vnl%sparseKBorb%Index2(ntmp2))
                !allocate(FRC%potential%vnl%sparseKBorb%ValueN(ntmp2))
        end if

        !write(*,*) "after GetNonlocalPotentialK.f90."

        if (FRC%potential%vnlScheme) then
                if (FRC%potential%vnl%KBorbIsSparse) then
                        !write(*,*) 'before GetKBorb_Sparse_Init.'
                        call GetKBorb_Sparse_Init(FRC,tmp)
                else
                        call GetKBorb(FRC,tmp)
                end if
        end if
    end if

    !open(unit=11,file='Vnl.txt')
    !write(*,*) 'FRC%potential%vnl'
    !do ii = 1,FRC%potential%vnl%sparseKBorb%N_nonzero,1
    !write(dou,*) FRC%potential%vnl%sparseKBorb%Index1(ii),FRC%potential%vnl%sparseKBorb%Index2(ii),FRC%potential%vnl%sparseKBorb%ValueN(ii)
    !end do
    !close(11)
    
    call system_clock(clock1,crate1,cmax1)
    Dtime      = real(clock1-clock2)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating non-local potential (RS):                         ", Dtime
    end if

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate neutral atom density.                                         %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    call GetNeutralAtomDensity(FRC)     ! generate isolated atom density

    !open(unit=11,file='Rna.txt')
    !write(*,*) 'FRC%rho%atom'
    !write(*,*)  FRC%rho%atom%vata
    !write(dou,*) 'FRC%rho%input'
    !write(dou,*)  FRC%rho%input(1)%vata
    !close(11)

    call system_clock(clock2,crate2,cmax2)
    Dtime      = real(clock2-clock1)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating isolated atom density:                            ", Dtime
    end if
    !call GetInitialDensity(FRC)         ! generate initial density
    call GetPartialCoreDensity(FRC)     ! generate partial core density
    call system_clock(clock1,crate1,cmax1)
    Dtime      = real(clock1-clock2)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating partial core density:                             ", Dtime
    end if

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate local pseudopotential.                                        %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    if (shortRange) then
        allocate(FRC%potential%vps%vata(size(FRC%potential%Vna%vata)))
        FRC%potential%vps = FRC%potential%Vna
        !allocate(vps2(size(vps%vata)))
        !call distmat_gather(FRC,vps,vps2)
        !if (mpirank == 0 .and. FRC%interpolation%vloc) then
            !vps = FRC%interPolation%AntFun(vps)
        !end if
        !veffn = size(vps%vata)
        !if (FRC%mpi%status) then
            !write(*,*) "Error in GenHamiltonian.f90. MPI is not available now."
            !stop
            !call MPI_bcast_variables(veffn,0)
        !end if
        !call InitDistArray_1D(veffn,1,veffn,1,1,mpisize,vps)
        !call modBCDist_1D(FRC,vps,vps%m/mpisize,1,mpisize,1,.FALSE.,.FALSE.,FRC%potential%vps)
    else
        
        write(*,*) "Error in GenHamiltonian.f90. ShortRange should be true now."
        stop
        
        !call GetHartreePotential(FRC)       ! Calculate Hartree potential of the neutral atom density
        !call distmat_fevalMinus(FRC)
        !call distmat_gather(FRC,vps)
        !if (mpiRank == 0) then
        !    vps = InterPolationAntFun(vps)
        !end if
        !veffn = size(vps)
        !if (FRC%mpi%status) then
        !    call MPI_Bcast_variable(veffn,0)
        !end if
        !call InitDistArray(veffn,1,veffn,1,1,mpisize,vps)
        !call ModBCDist(FRC,vps,ceiling(vps%m/mpisize),1,mpisize,1)
    end if
    call system_clock(clock2,crate2,cmax2)
    Dtime      = real(clock2-clock1)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating local pseudopotential:                            ", Dtime
    end if

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate Hartree potential.                                            %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    if (shortRange) then
        allocate(rho%vata(size(FRC%rho%input(1)%vata,1),size(FRC%rho%input(1)%vata,2)))
        rho = FRC%rho%input(1)
        if (FRC%spin%ispin == 1) then
            call distmat_bsxfun_Minus(FRC,rho,FRC%rho%atom,rho)
        else if (FRC%spin%ispin == 2) then
            write(*,*) "Error in GenHamiltonian.f90. Collinear spin is not available now."
            stop
            !call distmat_bsxfun_XY(FRC,rho,FRC%rho%atom)
        else if (FRC%spin%ispin == 4) then
            write(*,*) "Error in GenHamiltonian.f90. general spin(non-collinear) is not alvailable now."
            stop
            !call distmat_bsxfun_Minus(FRC,rho,FRC%rho%atom)
        end if
        allocate(rhotmp%vata(size(rho%vata)))
        rhotmp%m      = rho%m
        rhotmp%n      = rho%n
        rhotmp%mblock = rho%mblock
        rhotmp%nblock = rho%nblock
        rhotmp%mproc  = rho%mproc
        rhotmp%nproc  = rho%nproc
        rhotmp%vata   = rho%vata(:,1)
        !write(*,*) "maker 1"
        call GetHartreePotential(FRC,rhotmp,bc,.TRUE.,FRC%potential%VH)
    else
        
        write(*,*) "Error in GenHamiltonian.f90. ShortRange should be true now."
        stop
        
        !if (FRC%rho%input(1) /= FRC%rho%Atom%atomData) then
        !    call GetHartreePotential(FRC,FRC%rho%input(1),FRC%potential%VH(1))
        !else
        !    FRC%potential%VH(1) = FRC%potential%Vatom
        !end if
    end if
    !open(unit=11,file='VH.txt')
    !open(unit=dou,file=flax)
    !write(dou,*) 'process', mpirank
    !write(*,*) 'FRC%potential%VH%vata', size(FRC%potential%VH%vata)
    !write(*,*)  FRC%potential%VH%vata
    !close(11)

    call system_clock(clock1,crate1,cmax1)
    Dtime      = real(clock1-clock2)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating Hartree potential:                                ", Dtime
    end if

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% Generate exchange correlation potential.                               %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    if (trim(FRC%mixing%mixingType) == trim("density")) then
        FRC%rho%input(2)  = FRC%rho%input(1)
    else
        allocate(FRC%rho%output(1)%vata(size(FRC%rho%input(1)%vata,1),size(FRC%rho%input(1)%vata,2)))
        FRC%rho%output(1) = FRC%rho%input(1)
    end if

    !write(*,*) 'here in GenH, before xcMain.'
    call xcMain(FRC)                   ! Calculate the XC potential of the neutral atom density
    
    !open(unit=11,file='XC.input')
    !open(unit=dou,file=flax)
    !write(dou,*) 'process', mpirank
    !write(dou,*) 'FRC%potential%VXCout%vata',size(FRC%potential%VXCout%vata,1)
    !write(*,*)  FRC%potential%VXCout%vata(:,1)
    !close(11)
    
    call system_clock(clock2,crate2,cmax2)
    Dtime      = real(clock2-clock1)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating exchange correlation potential:                   ", Dtime
    end if
    antloc = .not.(FRC%interpolation%vloc)
 
    call updateVeff2D(FRC,FRC%potential%VXCout,FRC%potential%VH,antloc,veffout)
    FRC%potential%veffin(1)%vata   = veffout%vata(:,1)
    FRC%potential%veffin(1)%m      = veffout%m
    FRC%potential%veffin(1)%n      = 1!veffout%n
    FRC%potential%veffin(1)%mblock = veffout%mblock
    FRC%potential%veffin(1)%nblock = veffout%nblock
    FRC%potential%veffin(1)%mproc  = veffout%mproc
    FRC%potential%veffin(1)%nproc  = veffout%nproc
    call system_clock(clock1,crate1,cmax1)
    Dtime      = real(clock1-clock2)/real(crate1)
    if (mpirank == 0) then
    write(*,*) "Generating effective poential:                               ", Dtime
    end if
    !deallocate memories
    deallocate(rho%vata,veffout%vata,rhotmp%vata)
    !open(unit=dou,file=flax)
    !write(dou,*) 'process', mpirank
    !write(*,*) 'FRC%potential%veffin(1)%vata', size(FRC%potential%veffin(1)%vata)
    !write(*,*)  FRC%potential%veffin(1)%vata
    !close(dou)
    !stop
    return
    end subroutine GenHamiltonian
    end module GenHamiltonian_module

