! elementType.f90
    
!******************************************************************
    !
    ! this is to define a type to contain element information.
    !
!******************************************************************
    
    module elementType_module
    
    use rnuType_module
    
    type :: elementType
        integer                    :: Z
        integer                    :: aoLcutoff
        integer                    :: aoZcutoff
        character(len=99)          :: path
        character(len=03)          :: species
        integer                    :: NumberOfAtom
        integer                    :: valence
        integer                    :: vnlNcutoff
        integer                    :: vnlLcutoff
        integer, allocatable       :: aolnu(:)
        integer, allocatable       :: aopnu(:)
        integer, allocatable       :: aoznu(:)
        real*8,  allocatable       :: aornu(:)
        !type(rnuType), allocatable :: aornu(:)
        integer, allocatable       :: vnllnu(:)
        real*8, allocatable        :: vnlenu(:)
        integer, allocatable       :: vnlnnu(:)
        real*8, allocatable        :: vnlrnu(:)
    end type elementType
    
    end module elementType_module