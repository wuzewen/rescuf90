! calcDOS.f90

      module calcDOS_module
      contains
      subroutine calcDOS(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC

      ! temporary variables
      logical :: mpistat
      integer :: mpirank, mpisize

      ! body of this function
      mpistat = FRC%mpi%status
      mpirank = FRC%mpi%rank
      mpisize = FRC%mpisize
      iter    = FRC%scloop
      nband   = size(FRC%energy%ksnrg%vata,1)
      nkpt    = size(FRC%energy%ksnrg%vata,2)
      ispin   = FRC%spin%ispin
      nspin   = FRC%spin%nspin
      energy  = FRC%energy%ksnrg%vata
      call calcOccupancy(FRC,energy,eFermi,nonsense1,nonsense2)
      kweight = FRC%kpoint%kweight
      nvec    = FRC%kpoint%%gridn
      avec    = FRC%domain%latvec
      call inversion(avec,bvec)
      bvec    = transpose(bvec)
      bvec    = 2.0*pi*bvec
      FRC%energy%EFermi = eFermi
      DosRange = FRC%dos%dosrange

      eres     = FRC%dos%resolution

      !forall(ii=1:size(DosRange)-1)
      nnrg     = ceiling((DosRange(2)-DosRange(1))/eres)
      !end forall
      deltaE   = (DosRange(2)-DosRange(1))/dble(nnrg-1)
      forall(ii=1:nnrg)
              nrgvec(ii) = eFermi+DosRange(1)+dble(ii-1)*deltaE
      end forall
      allocate(DOS(nnrg,nspin))

      ! if (all(pweight == 0.0)) then
              !allocate(pdos(nnrg,nspin,1))
      ! else
              !allocate(pdos(nnrg,nspin,size(pweight)))
      ! end if

      if (trim(FRC%kpoint%sampling) == trim("fermi-dirac")) then
              sigma = mrc


