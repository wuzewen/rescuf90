! initType.f90
    
!************************************************************
    !
    ! This is to define a type to contian init information
    !
!************************************************************
    
    module initType_module
    
    type :: initType
        logical              ::  addpath
        logical              ::  atominfo
        logical              ::  density
        logical              ::  domaininfo
        logical              ::  dos
        logical              ::  dfpt
        logical              ::  eigensolver
        logical              ::  element
        logical              ::  energy
        logical              ::  force
        logical              ::  gpu
        logical              ::  initsub
        logical              ::  interpolation
        logical              ::  kpoint
        logical              ::  lcao
        logical              ::  mixer
        logical              ::  mpi
        logical              ::  option
        logical              ::  poisson
        logical              ::  potential
        logical              ::  prr
        logical              ::  repxyz
        logical              ::  smearing
        logical              ::  smi
        logical              ::  spin
        logical              ::  sr
        logical              ::  symmetry
        logical              ::  ub
        logical              ::  xc
    end type initType
    
    end module initType_module