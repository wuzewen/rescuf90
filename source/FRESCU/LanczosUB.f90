! LanczosUB.f90

!*************************************************************************
      !
      !
      !
!*************************************************************************

      module LanczosUB_module
      contains
      subroutine LanczosUB(FRC,kpt,spin,gpu,gridn,niter,uout)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use GenHamFun_module
      external DGEEV

      !implicit none

      ! input variables
      type(FORTRAN_RESCU_CALCULATION) :: FRC
      integer :: gridn, niter, spin
      logical :: gpu
      real*8  :: kpt(3)

      ! temporary variables
      integer :: ii
      real*8  :: beta, tt, mval
      complex*16 :: imnu
      real*8, allocatable :: T(:,:), alpha(:,:)
      !real*8, allocatable :: s(:,:), u(:)
      complex*16, allocatable :: v(:,:), v0(:,:)
      complex*16, allocatable :: f(:,:), hamu(:,:), hamd(:,:)

      character(LEN=1) :: JOBVL, JOBVR
      integer          :: LDVL, LDVR, LWORK, INFO
      real*8, allocatable :: WR(:), WI(:), VL(:,:), VR(:,:), WORK(:)

      ! output variables
      real*8  :: uout

      ! body of this function
      imnu = (0.0D0,1.0D0)
      allocate(v(gridn,1))
      call random_seed()
      do ii = 1,gridn,1
          !call random_seed()
          call random_number(tt)
          v(ii,1) = dcmplx(tt)
      end do
      !call random_seed()
      do ii = 1,gridn,1
          !call random_seed()
          call random_number(tt)
          v(ii,1) = v(ii,1)+tt*imnu
      end do

      !write(*,*) "in Lanczos, v ="
      !write(*,*)  v

      tt = dble(sum(v*conjg(v)))

      !write(*,*) "tt ="
      !write(*,*)  tt

      !write(*,*) "niter"
      !write(*,*)  niter

      v  = v/(tt**0.5D0)
      allocate(T(niter,niter))
      T = 0.0D0
      allocate(f(gridn,1))
      call GenHamFun(FRC,kpt,spin,gpu,v,f,hamu,hamd)
      allocate(alpha(1,1))
      alpha = dble(matmul(conjg(transpose(f)),v))
      !write(*,*) "alpha ="
      !write(*,*)  v
      !stop
      f     = f - alpha(1,1)*v
      T(1,1) = alpha(1,1)
      allocate(v0(gridn,1))
      do ii = 2,niter,1
          tt         = dble(sum(f*conjg(f)))
          beta       = tt**0.5D0
          v0         = v
          v          = f/beta
          call GenHamFun(FRC,kpt,spin,gpu,v,f,hamu,hamd)

          !write(*,*) 'in Lanzos. f =',ii,niter
          !write(*,*)  v

          !stop


          f          = f-beta*v0
          alpha      = dble(matmul(conjg(transpose(f)),v))
          f          = f-alpha(1,1)*v
          T(ii,ii-1) = beta
          T(ii-1,ii) = beta
          T(ii,ii)   = alpha(1,1)
      end do

      tt   = dble(sum(f*conjg(f)))
      beta = tt**0.5D0

      JOBVL = 'N'
      JOBVR = 'V'
      LDVL  =  1
      LDVR  =  niter
      LWORK =  4*niter
      allocate(WR(niter),WI(niter),VL(LDVL,niter),VR(LDVR,niter))
      allocate(WORK(LWORK))

      !write(*,*) 'size of T' , size(T,1), size(T,2)
      !write(*,*) T
      call DGEEV(JOBVL,JOBVR,niter,T,niter,WR,WI,VL,LDVL,VR,LDVR,WORK,LWORK,INFO)
      !write(*,*) 'here'
      mval  = maxval(WR)
      !call diag(u,utmp)
      uout = mval + beta*maxval(abs(VR(niter,niter-2:niter)))

      return
      end subroutine LanczosUB
      end module LanczosUB_module
