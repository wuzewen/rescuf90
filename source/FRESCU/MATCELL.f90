! MATCELL

      module MATCELLMOD

      interface MATCELL
              module procedure mat2cell_1D
              module procedure mat2cell_2D
              module procedure mat2cell_3D
      end interface MATCELL

      end module MATCELLMOD
