! InterpRdist2Cart_sparse.f90
    
!********************************************
    !
    !
    !
!********************************************
    
    module InterpRdist2Cart_sparse_module
    contains
    subroutine InterpRdist2Cart_sparse(rrdata,frdata,pos,lqn,avec,nvec,outcell,ftout,out2)
    
    use RealVata_2D_module
    use RealVata_3D_module
    use inversion_module
    use cellInRange_module
    use bsxfunMinus_module
    use bsxfunTimes_module
    use interp1_module
    use genRealSH_module
    use getLocalIndex_module
    use accumarray1_module
    use getTrans_module
    use splitMat2Cell_module
    use ndgridA_module
    use repmat_module
    use repmat2_module
    use mat2cell3D_module
    use mat2cell4D_module
    use ismemberRow_module
    use Cellrepmat_module
    use ind2sub_3D_module
    !use Cell_4D_real
    use RealVata_4D_module
    use SparseArray_2D_Real8_module
    use SparseVataMN_2D_Real8_module

    implicit none
    
    ! input variables
    real*8, allocatable  :: rrdata(:), frdata(:)
    real*8               :: pos(1,3), avec(3,3)
    logical              :: outcell
    integer              :: lqn, nvec(3)
    
    ! temporary variables
    real*8               :: eps, rmax
    real*8, allocatable  :: ffgrid(:,:), xtmp(:,:), dist(:), post(:,:), frgrix(:,:)
    real*8, allocatable  :: xtmp1(:), xtmp2(:), xtmp3(:), Ylm(:,:), tv1(:), tv2(:), tv3(:)
    real*8, allocatable  :: frgrid(:), disttmp(:), ffgridtmp(:,:), txlin(:), tylin(:), tzlin(:)
    real*8, allocatable  :: utxyz(:,:), ftmpt(:,:), vtmp(:,:), ffgtmp(:)!, fxgrid(:,:,:)   !ttmp(:), 
    real*8, allocatable  :: rrdata2(:), frdata2(:)
    real*8, allocatable  :: ffgridnn(:,:,:,:), ltmp(:,:), ltmpt(:,:), txyz(:,:), indR(:,:,:)
    real*8, allocatable  :: postmp(:,:), utxr(:), utyr(:), utzr(:)
    integer              :: inde, ii, jj, nf, nptvec(3), npoint, tlpl, ntmp, nut
    !integer              :: itmp, itmp1, itmp2, itmp3, nxtmp, lo1, lo2, lo3, icx, icy, icz
    integer, allocatable :: ind(:,:,:), indtmp(:,:,:), tx(:,:,:), ty(:,:,:), tz(:,:,:), sx(:), sy(:), sz(:)
    integer, allocatable :: utx(:), uty(:), utz(:), txtmp(:,:,:), txtmptmp(:)
    character(len=20)    :: method
    logical              :: issym
    !type(RealVata_2D), allocatable :: ftmppt(:,:)
    type(RealVata_3D), allocatable :: icell(:,:,:) !fcell(:,:,:), 
    type(RealVata_4D), allocatable :: fcell(:,:,:)!, icell(:,:,:)

    integer, allocatable :: NonzeroInd(:)

    !type(SparseArray_2D_Real8), allocatable :: ftmp(:), ftout(:)
    type(SparseVataMN_2D_Real8), allocatable :: ftmp(:), ftout(:)
    integer :: nnn(3), ss, mm, nn
    
    ! output variables
    ! type(RealVata_2D), allocatable :: out1(:,:)
    real*8, allocatable          :: out2(:,:)!, out3(:)
    
    
    ! body of this function
    eps  = 1D-14
    inde = 0
    nf   = size(frdata)
    do ii = 1,nf,1
        if (abs(frdata(ii))>eps) then ! .and. abs(frdata(ii)) == 1.0) then
            inde = ii
        end if
    end do
    inde = inde + 1
    
    allocate(rrdata2(inde))
    allocate(frdata2(inde))
    forall(ii=1:inde)
            rrdata2(ii) = rrdata(ii)
            frdata2(ii) = frdata(ii)
    end forall
    if (inde<size(rrdata)) then
        frdata2(inde) = 0.0D0
    end if
    rmax = maxval(rrdata2)
    
    allocate(ltmp(3,3),ltmpt(3,3))
    ltmp = 0.0D0
    forall(ii=1:3)
        ltmp(ii,ii) =  dble(nvec(ii))
    end forall
    call inversion(ltmp,ltmpt)
    ltmp  = matmul(ltmpt,avec)
    issym = .FALSE.
    
    allocate(post(1,3))
    post = pos
    call cellInRange(ltmp,post,rmax,issym,txyz)
    
    do ii = 1,3,1
        nptvec(ii) = maxval(txyz(:,ii))-minval(txyz(:,ii))+1
    end do
    
    allocate(tx(nptvec(1),nptvec(2),nptvec(3)), ty(nptvec(1),nptvec(2),nptvec(3)),tz(nptvec(1),nptvec(2),nptvec(3)))
    tx     = reshape(txyz(:,1),nptvec)
    ty     = reshape(txyz(:,2),nptvec)
    tz     = reshape(txyz(:,3),nptvec)
    txyz   = matmul(txyz,ltmp)
    npoint = size(txyz,1)
    if (lqn<0) then
        tlpl = 1
        allocate(ffgrid(npoint,1))
        ffgrid = 0.0D0
    else
        tlpl = 2*lqn+1
        allocate(ffgrid(npoint,tlpl))
        ffgrid = 0.0D0
    end if
    
    allocate(postmp(1,3))
    postmp = pos
    allocate(xtmp(npoint,3))
    call bsxfunMinus(txyz,postmp,xtmp)
    allocate(dist(npoint))
    dist(:) = xtmp(:,1)**2+xtmp(:,2)**2+xtmp(:,3)**2
    dist    = dist**0.5D0
    
    if (minval(dist)<=rmax) then
        ntmp = 0
        do ii = 1,npoint,1
            if (dist(ii)<=rmax) then
                ntmp = ntmp+1
            end if
        end do
        allocate(NonzeroInd(ntmp))
        allocate(disttmp(ntmp))
        ntmp = 0
        do ii = 1,npoint,1
            if (dist(ii)<=rmax) then
                ntmp             = ntmp+1
                NonzeroInd(ntmp) = ii
                disttmp(ntmp)    = dist(ii)
            end if
        end do
        allocate(frgrid(ntmp))
        method = "spline"
        call interp1(rrdata2,frdata2,disttmp,method,0,frgrid)
        
        if (lqn<0) then
            !ntmp = 0
            do ii = 1,ntmp,1
                !if (dist(ii)<=rmax) then
                !ntmp = ntmp+1
                ffgrid(NonzeroInd(ii),1) = ffgrid(NonzeroInd(ii),1)+frgrid(ii)
                !end if
            end do
        else
            allocate(xtmp1(ntmp),xtmp2(ntmp),xtmp3(ntmp))
            xtmp1 = 0.0D0
            xtmp2 = 0.0D0
            xtmp3 = 0.0D0
            !ntmp = 0
            do ii = 1,ntmp,1
                !if (dist(ii)<=rmax) then
                !ntmp          = ntmp+1
                xtmp1(ii) = xtmp(NonzeroInd(ii),1)
                xtmp2(ii) = xtmp(NonzeroInd(ii),2)
                xtmp3(ii) = xtmp(NonzeroInd(ii),3)
                !end if
            end do
            
            call GenRealSH(xtmp1,xtmp2,xtmp3,lqn,1,Ylm)
            
            allocate(frgrix(size(frgrid),1),ffgridtmp(size(Ylm,1),size(Ylm,2)))
            frgrix(:,1) = frgrid(:)
            
            call bsxfunTimes(frgrix,Ylm,ffgridtmp)
            
            !ntmp = 0
            do ii = 1,ntmp,1
                !if (dist(ii)<=rmax) then
                !ntmp          = ntmp+1
                ffgrid(NonzeroInd(ii),:)  = ffgrid(NonzeroInd(ii),:)+ffgridtmp(ii,:)
                !end if
            end do
            
        end if
    end if

    if (outCell) then
        
        call getLocalIndex(tx,ty,tz,nvec,ind)
        
        ntmp = maxval(tx)-minval(tx)+1
        allocate(txlin(ntmp))
        forall(ii=1:ntmp)
            txlin(ii) = minval(tx)+ii-1
        end forall
        
        ntmp = maxval(ty)-minval(ty)+1
        allocate(tylin(ntmp))
        forall(ii=1:ntmp)
            tylin(ii) = minval(ty)+ii-1
        end forall
        
        ntmp = maxval(tz)-minval(tz)+1
        allocate(tzlin(ntmp))
        forall(ii=1:ntmp)
            tzlin(ii) = minval(tz)+ii-1
        end forall
        
        call getTrans(nvec,txlin,tylin,tzlin,tv1,tv2,tv3)
        
        call splitMat2Cell(tv1,sx,utx)
        call splitMat2Cell(tv2,sy,uty)
        call splitMat2Cell(tv3,sz,utz)
        
        allocate(utxr(size(utx)))
        allocate(utyr(size(uty)))
        allocate(utzr(size(utz)))
        utxr = dble(utx)
        utyr = dble(uty)
        utzr = dble(utz)
        nut = size(utx)*size(uty)*size(utz)
        allocate(utxyz(nut,3))
        
        call ndgridA(utxr,utyr,utzr,utxyz)
        
        ntmp = product(nvec)
        
        allocate(ftmpt(ntmp,tlpl))
        !ftmpt = 0
        !call Cellrepmat(ftmpt,1,nut,ftmppt)
        
        allocate(ffgridnn(nptvec(1),nptvec(2),nptvec(3),tlpl))
        ffgridnn = reshape(ffgrid,(/nptvec(1),nptvec(2),nptvec(3),tlpl/))
        
        call mat2cell4D(ffgridnn,sx,sy,sz,tlpl,fcell)
        
        allocate(indtmp(nptvec(1),nptvec(2),nptvec(3)))
        indtmp = reshape(ind,nptvec)
        allocate(indR(size(ind,1),size(ind,2),size(ind,3)))
        indR = dble(ind)
        !write(*,*) "sx ="
        !write(*,*)  sx
        !write(*,*) "sy ="
        !write(*,*)  sy
        !write(*,*) "sz ="
        !write(*,*)  sz
        call mat2cell3D(indR,sx,sy,sz,icell)
        nnn(1)  = size(icell,1)
        nnn(2)  = size(icell,2)
        nnn(3)  = size(icell,3)
        !write(*,*) "nnn =", nnn
        !ntmp                 = product(nptvec)
        !ftmp(ii)%N_nonzero = ntmp*tlpl
        !ntmp = size(ind,1)*size(ind,2)*size(ind,3)
        allocate(ftmp(nut))
        do ii = 1,nut,1
            call ind2sub_3D(nnn,ii,nn,ss,mm)
            ntmp               = size(icell(nn,ss,mm)%vata)
            ftmp(ii)%N_nonzero = ntmp*tlpl
            ftmp(ii)%SIZE1     = product(nvec)
            ftmp(ii)%SIZE2     = tlpl
            !write(*,*) "ntmp =", ntmp
            !call ind2sub_3D(nnn,ii,nn,ss,mm)

            allocate(ftmp(ii)%Index1(ftmp(ii)%N_nonzero))
            allocate(ftmp(ii)%Index2(ftmp(ii)%N_nonzero))
            allocate(ftmp(ii)%ValueN(ftmp(ii)%N_nonzero))
            do jj = 1,tlpl,1
                !allocate(ftmp(ii)%Index1(ftmp(ii)%N_nonzero))
                !allocate(ftmp(ii)%Index2(ftmp(ii)%N_nonzero))
                !allocate(ftmp(ii)%ValueN(ftmp(ii)%N_nonzero))
                !call ind2sub_3D(nnn,ii,nn,ss,mm)
                !write(*,*) "nn,ss,mm=",nn,ss,mm
                !write(*,*) "icell(nn,ss,mm)%vata"
                !write(*,*)  icell(nn,ss,mm)%vata
                ftmp(ii)%Index2((jj-1)*ntmp+1:jj*ntmp) = jj
                ftmp(ii)%Index1((jj-1)*ntmp+1:jj*ntmp) = reshape(icell(nn,ss,mm)%vata,(/product(nptvec)/))
                ftmp(ii)%ValueN((jj-1)*ntmp+1:jj*ntmp) = reshape(fcell(nn,ss,mm)%vata(:,:,:,jj),(/product(nptvec)/))
            end do
        end do

        allocate(ftout(nut))
        do ii = 1,nut,1
            ntmp = count(ftmp(ii)%ValueN /= 0.0D0)
            ftout(ii)%N_nonzero = ntmp
            ftout(ii)%SIZE1     = ftmp(ii)%SIZE1
            ftout(ii)%SIZE2     = ftmp(ii)%SIZE2
            allocate(ftout(ii)%Index1(ntmp))
            allocate(ftout(ii)%Index2(ntmp))
            allocate(ftout(ii)%ValueN(ntmp))
            ftout(ii)%Index1 = 0
            ftout(ii)%Index2 = 0
            ftout(ii)%ValueN = 0.0D0
            ntmp = 0
            do jj = 1,size(ftmp(ii)%ValueN),1
                if (ftmp(ii)%ValueN(jj) /= 0.0D0) then
                        ntmp = ntmp + 1
                        ftout(ii)%Index1(ntmp) = ftmp(ii)%Index1(jj)
                        ftout(ii)%Index2(ntmp) = ftmp(ii)%Index2(jj)
                        ftout(ii)%ValueN(ntmp) = ftmp(ii)%ValueN(jj)
                end if
            end do
        end do
        !write(*,*) "N_nonzero =", ftout(2)%N_nonzero
        !write(*,*) "Index1 ="
        !write(*,*)  ftout(2)%Index1
        !write(*,*) "Index2 ="
        !write(*,*)  ftout(2)%Index2
        !write(*,*) "ValueN ="
        !write(*,*)  ftout(2)%ValueN

        !write(*,*) "stop in sparse."
        !stop
        !allocate(out1(nut,1))
        allocate(out2(size(utxyz,1),size(utxyz,2)))
        !allocate(out3(0))
        !out1(:,1) =  ftmppt(1,:)
        out2      = -utxyz
    else
            write(*,*) "Error in InterpRdist2Cart_sparse."
            write(*,*) "outCell should be true."
        !call getLocalIndex(tx,ty,tz,nvec,txtmp)
        !ii = product(nvec)
        !jj = size(ffgrid,2)
        !allocate(vtmp(ii,jj))
        !do ii = 1,size(ffgrid,2)
        !    allocate(ffgtmp(size(ffgrid,1)))
        !    ffgtmp(:) = ffgrid(:,ii)
        !    ntmp = size(txtmp,1)*size(txtmp,2)*size(txtmp,3)
        !    allocate(txtmptmp(ntmp))
        !    txtmptmp = int(reshape(txtmp,(/ntmp/)))
        !    ntmp = product(nvec)
        !    call accumarray1(txtmptmp,ffgtmp,ntmp,out3)
        !end do
        !allocate(out1(0,0))
        !allocate(out2(0,0))
    end if
    
    return
    end subroutine InterpRdist2Cart_sparse
    end module InterpRdist2Cart_sparse_module
