! MPI_Pack_cmplx_2D.f90

      module MPI_Pack_cmplx_2D_module
      contains
      subroutine MPI_Pack_cmplx_2D(A,header,vata)

      implicit none

      ! input variables
      complex*16, allocatable :: A(:,:)

      ! temporary variables
      logical :: isSparse
      integer :: typeSPARSE, typrMATRIX, headersize, datalength
      integer :: s(2), nd, ntmp, ii, jj, hashcode

      ! output variables
      integer :: header(10)
      complex*16, allocatable :: vata(:)

      ! body of this function
      typeSPARSE =  1
      typrMATRIX =  2
      headersize =  10
      isSparse   = .FALSE.

      if (isSparse) then
              write(*,*) "Error in MPI_Pack. sparse matrix is unable."
      else
              datalength = size(A)
              s(1)       = size(A,1)
              s(2)       = size(A,2)
              nd         = 2
              allocate(vata(datalength))
              ntmp  = 0
              do jj = 1,s(2),1
                  do ii = 1,s(1),1
                      ntmp       = ntmp + 1
                      vata(ntmp) = A(ii,jj)
                  end do
              end do
              hashcode   = 0
              header(1)  = typeMatrix
              header(2)  = datalength
              header(3)  = 1
              header(4)  = hashcode
              header(5)  = nd
              header(6)  = s(1)
              header(7)  = s(2)
              header(8)  = 0
              header(9)  = 0
              header(10) = 0
      end if

      return
      end subroutine MPI_Pack_cmplx_2D
      end module MPI_Pack_cmplx_2D_module
