! fftplan.f90

      module fftplan_module
      contains
      subroutine fftplan(FRC)

      call dfftw_plan_dft_3d(FRC%planfft,FRC%L,FRC%M,FRC%N, & 
                             FRC%Arr,FRC%Brr,FFTW_FORWARD,FFTW_ESTIMATE)

      return 
      end subroutine fftplan
      end mocule fftplan_module
