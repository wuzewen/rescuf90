! projectHamiltonian.f90
    
!******************************************************************
    !
    ! Projected Hamiltonian will be generated in this subroutine.
    ! 
    !
!******************************************************************
    
    module projectHamiltonian_module
    contains
    subroutine projectHamiltonian(FRC,kpt,vs,XHXout,XSXout)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use distmat_isreal_module
    use distmat_isreal_CMPLX_module
    use modBCDist_2D_module
    use modBCDist_CMPLX_2D_module
    use getGlobalInd_2D_module
    use getGlobalInd_CMPLX_2D_module
    use triu_module
    use triu_cmplx_module
    use GenHamFun_module
    
    implicit none
    
    ! input variables
    real*8                          :: kpt(3)
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_CMPLX_2D)           :: vs
    real*8, allocatable             :: oper(:,:)
    
    ! temporary variables
    integer                         :: mpirank, mpisize, cn, ii, jj, norb, nblock, maxind, buff, sizeof
    integer                         :: nx, mbpsi, nbpsi, blks, ntmp, crealIN, spin, kk, mtmp
    logical                         :: smistat, mpistat, gpustat, creal
    real*8                          :: sep, dr, avec(3,3)
    logical, allocatable            :: colind(:)
    integer, allocatable            :: jb(:), ia(:,:), ja(:,:), iaHX(:,:), jaHX(:,:)
    complex*16, allocatable         :: tmp2(:,:), XSXtmp(:,:)
    complex*16, allocatable         :: nonsense1(:,:), nonsense2(:,:)
    complex*16, allocatable         :: tmp1(:,:), XHXtmp(:,:)
    type(VataMN_CMPLX_2D)           :: HX, XH
    integer                         :: m, n, k, lda, ldb, ldc
    complex*16                      :: alpha, beta
    character(len=1)                :: transa, transb
    
    ! output variables
    type(VataMN_CMPLX_2D)           :: XHX, XSX, XHXout, XSXout
    
    ! body of this function
    spin    = 1
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    smistat = FRC%smi%status
    gpustat = .FALSE.
    sep     = 1.0D0*10.0D0**(-14.0D0)
    cn      = product(FRC%domain%cgridn)
    avec    = FRC%domain%latvec
    dr      = avec(1,1)*avec(2,2)*avec(3,3)+avec(1,2)*avec(2,3)*avec(3,1)+avec(1,3)*avec(2,1)*avec(3,2) &
             -avec(1,3)*avec(2,2)*avec(3,1)-avec(1,1)*avec(2,3)*avec(3,2)-avec(1,2)*avec(2,1)*avec(3,3)
    dr      = dr/dble(cn)
    norb    = vs%n
    buff    = FRC%option%bufferSize
    !call distmat_isreal_CMPLX(FRC,vs,creal)
    creal   = .FALSE.
    if (creal) then
        crealIN = 1
    else
        crealIN = 0
    end if
    sizeof  = 16-8*crealIN
    mbpsi   = vs%mblock
    nbpsi   = vs%nblock
    blks    = ceiling(dble(buff)/dble(sizeof)/dble(cn))*mpisize
    ntmp    = ceiling(dble(norb)/768.0D0)
    ntmp    = ceiling(dble(norb)/dble(ntmp))
    blks    = min(blks,ntmp)
    allocate(jb(norb))
    forall(ii=1:norb)
        jb(ii) = ceiling((dble(ii)-sep)/dble(blks))
    end forall
    !write(*,*) 'marker 1 in projH.'
    nblock  = jb(norb)
    call initDistArray_CMPLX_2D(norb,norb,norb,blks,1,mpisize,XHX)
    call GetGlobalInd_CMPLX_2D(mpirank,XHX,ia,ja)
    !write(*,*) 'XHX',XHX%m,XHX%n,XHX%mblock,XHX%nblock,XHX%mproc,XHX%nproc
    !write(*,*) "ja =", ja
    allocate(XHX%vata(size(ia),size(ja)))
    allocate(XSX%vata(size(ia),size(ja)))
    XHX%vata = (0.0D0,0.0D0)
    XSX      = XHX
    
    allocate(colind(size(jb)))
    !write(*,*) 'nblock =', nblock
    !write(*,*) 'marker 2 in projH.'

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!
    !%%%% If the wave function is too large(too many bands), The wave function       %%%%!
    !%%%% will be divided to nblock(integer) blocks. And The calculation will go     %%%%!
    !%%%% over one block by one block. In this way,  there may be less memory        %%%%!
    !%%%% problem.                                                                   %%%%!
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!

    do ii = 1,nblock,1
        do jj = 1,size(jb),1
            if (jb(jj) == ii) then
                colind(jj) = .TRUE.
                maxind     = jj
            else
                colind(jj) = .FALSE.
            end if
        end do
        nX = count(colind .EQV. .TRUE.)
        !write(*,*) "nX ="
        !write(*,*)  nX
        if (nX > 0) then
            !write(*,*) 'vs%vata size', size(vs%vata,1), size(vs%vata,2)
            call initDistArray_CMPLX_2D(cn,nX,vs%mblock,1,mpisize,1,HX)
            allocate(HX%vata(size(vs%vata,1),nX))
            ntmp = 0
            do jj = 1,size(jb),1
                if (colind(jj)) then
                    ntmp = ntmp+1
                    HX%vata(:,ntmp) = vs%vata(:,jj)
                end if
            end do
            !write(*,*) "in projectH, before GenH, HX%vata ="
            !write(*,*)  HX%vata
            call InitDistArray_CMPLX_2D(HX%m,HX%n,1,1,1,mpisize,XH)
            !allocate(HX%vata(XH%m,))
            call GetGlobalInd_CMPLX_2D(mpirank,XH,iaHX,jaHX)
            allocate(XH%vata(size(iaHX),size(jaHX)))
            deallocate(iaHX,jaHX)
            call modBCDist_CMPLX_2D(FRC,HX,1,1,1,mpisize,.FALSE.,.FALSE.,XH)
            !write(*,*) "in projectH, before GenH, after mod"
            if (size(HX%vata,2) > 0) then
                !write(*,*) "size of HX%vata"
                !write(*,*)  size(HX%vata,1), size(HX%vata,2)
                call GenHamFun(FRC,kpt,spin,gpustat,XH%vata,XH%vata,nonsense1,nonsense2)
                !call oper(HX%vata)
            end if

            !write(*,*) "in projectH, HX%vata ="
            !write(*,*)  HX%vata
            
            call ModBCDist_CMPLX_2D(FRC,XH,vs%mblock,1,mpisize,1,.FALSE.,.FALSE.,HX)
            if (gpustat) then
                write(*,*) "Error in projectHamiltonian.f90. GPU is not available now."
                stop
            else
                allocate(tmp1(maxind,size(vs%vata,1)))
                forall (jj=1:maxind)
                    tmp1(jj,:) = conjg(vs%vata(:,jj))
                end forall
                allocate(XHXtmp(size(tmp1,1),size(HX%vata,2)))
                XHXtmp =  0.0D0
                transa = 'N'
                transb = 'N'
                m      =  maxind
                n      =  size(HX%vata,2)
                k      =  size(vs%vata,1)
                lda    =  m
                ldb    =  k
                ldc    =  m
                alpha  = (1.0D0,0.0D0)
                beta   = (0.0D0,0.0D0)
                call ZGEMM(transa,transb,m,n,k,alpha,tmp1,lda,HX%vata,ldb,beta,XHXtmp,ldc)
                !XHXtmp = matmul(tmp1,HX%vata)
                allocate(tmp2(size(vs%vata,1),nX))
                ntmp = 0
                do jj=1,size(jb),1
                    if (colind(jj)) then
                        ntmp = ntmp+1
                        tmp2(:,ntmp) = vs%vata(:,jj)
                    end if
                end do
                allocate(XSXtmp(size(tmp1,1),size(tmp2,2)))
                XSXtmp =  0.0D0
                transa = 'N'
                transb = 'N'
                m      =  maxind
                n      =  size(tmp2,2)
                k      =  size(vs%vata,1)
                lda    =  m
                ldb    =  k
                ldc    =  m
                call ZGEMM(transa,transb,m,n,k,alpha,tmp1,lda,tmp2,ldb,beta,XSXtmp,ldc)
                !XSXtmp = matmul(tmp1,tmp2)
            end if
            
            !if (mpistat) then
                !write(*,*) "Error in projectHamiltonian.f90. MPI is not available now."
                !stop
            !end if
            
            !write(*,*) "maxind =",maxind,"size(ja)",size(ja),"true", count(colind == .TRUE.)
            !write(*,*) "size1",size(XHXtmp,1),"size2",size(XHXtmp,2)
            !write(*,*) "max(ja)=", maxval(ja)
            !write(*,*) "size(XHX%vata,2)", size(XHX%vata,2)
            if (mpirank == mod(ii-1,mpisize)) then
                !mtmp = 0
                !do jj = 1,maxind,1
                !    do kk = 1,size(ja)
                !        if (colind(ja(1,kk))) then
                !            !mtmp                  = mtmp+1
                !            XHX%vata(jj,ja(1,kk)) = XHXtmp(jj,kk)
                !            XSX%vata(jj,ja(1,kk)) = XSXtmp(jj,kk)
                            !XHX%vata(jj,ja(1,kk)) = XHXtmp(jj,mtmp)
                            !XSX%vata(jj,ja(1,kk)) = XSXtmp(jj,mtmp)
                            !XHX%vata(jj,ja(1,kk)) = XHXtmp(jj,ja(1,kk))
                !        end if
                !    end do
                !end do
                !write(*,*) "colind =", colind
                !write(*,*) "ja = ", ja
                 mtmp  = 0
                 do kk = 1,size(ja),1
                     !write(*,*) "kk = ", kk
                     if (colind(kk)) then
                             mtmp = mtmp+1
                             forall(jj=1:maxind)
                                     XHX%vata(jj,ja(1,kk)) = XHXtmp(jj,mtmp)
                                     XSX%vata(jj,ja(1,kk)) = XSXtmp(jj,mtmp)
                             end forall
                             !write(*,*) "kk =", kk, "mtmp =", mtmp

                     end if
                 end do
            end if
            deallocate(XH%vata,HX%vata,tmp1,XHXtmp,tmp2,XSXtmp)
        end if
    end do

    if (smistat) then
        write(*,*) "Error in projectHamiltonian.f90. SMI is not available now."
        stop
    else
            !write(*,*) "here here"
        allocate(XHXout%vata(XHX%m,XHX%n))
        allocate(XSXout%vata(XSX%m,XSX%n))
        call ModBCDist_CMPLX_2D(FRC,XHX,XHX%m,XHX%n,mpisize,1,.FALSE.,.FALSE.,XHXout)
        call modBCDist_CMPLX_2D(FRC,XSX,XSX%m,XSX%n,mpisize,1,.FALSE.,.FALSE.,XSXout)
        if (mpirank == 0) then
            allocate(tmp1(size(XHXout%vata,1),size(XHXout%vata,2)))
            allocate(tmp2(size(XHXout%vata,1),size(XHXout%vata,2)))
            call triu_cmplx(XHXout%vata,0,tmp1)
            call triu_cmplx(XHXout%vata,1,tmp2)
            XHXout%vata = tmp1+conjg(transpose(tmp2))
            ntmp = max(size(XHXout%vata,1),size(XHXout%vata,2))
            forall(ii=1:ntmp)
                    XHXout%vata(ii,ii) = dcmplx(dble(XHXout%vata(ii,ii)))
            end forall
            !write(*,*) "triutmp1 ="
            !write(*,*)  tmp1
            !write(*,*) "triutmp2 ="
            !write(*,*)  tmp2

            call triu_cmplx(XSXout%vata,0,tmp1)
            call triu_cmplx(XSXout%vata,1,tmp2)
            XSXout%vata = tmp1+conjg(transpose(tmp2))
            ntmp = max(size(XSXout%vata,1),size(XSXout%vata,2))
            forall(ii=1:ntmp)
                    XSXout%vata(ii,ii) = dcmplx(dble(XSXout%vata(ii,ii)))
            end forall
            !write(*,*) "triutmp1 ="
            !write(*,*)  tmp1
            !write(*,*) "triutmp2 ="
            !write(*,*)  tmp2
        end if
        XHXout%vata = XHXout%vata*dr
        XSXout%vata = XSXout%vata*dr
    end if

    !write(*,*) "XHXF ="
    !write(*,*)  XHX%vata
    !write(*,*) "XSXF ="
    !write(*,*)  XSX%vata
    
    return
    end subroutine projectHamiltonian
    end module projectHamiltonian_module
