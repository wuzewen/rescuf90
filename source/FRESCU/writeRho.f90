! writeRho.f90

      module writeRho_module
      contains
      subroutine writeRho(FRC,filnam,iter)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam
      integer                         :: ii, jj, iter
      
      open(unit=11,file=filnam)

      write(11,*) "input"
      write(11,*) "input%m, input%n, input%mblock, input%nblock, input%mproc, input%nproc"
      write(11,*)  FRC%rho%input(iter)%m,      FRC%rho%input(iter)%n
      write(11,*)  FRC%rho%input(iter)%mblock, FRC%rho%input(iter)%nblock
      write(11,*)  FRC%rho%input(iter)%mproc,  FRC%rho%input(iter)%nproc
      write(11,*) "vata"
      do ii = 1,size(FRC%rho%input(iter)%vata,1),1
          write(11,*)  FRC%rho%input(iter)%vata(ii,:)
      end do

      write(11,*) "output"
      write(11,*) "output%m, output%n, output%mblock, output%nblock, output%mproc, output%nproc"
      write(11,*)  FRC%rho%output(2)%m,      FRC%rho%output(2)%n
      write(11,*)  FRC%rho%output(2)%mblock, FRC%rho%output(2)%nblock
      write(11,*)  FRC%rho%output(2)%mproc,  FRC%rho%output(2)%nproc
      write(11,*) "vata"
      do ii = 1,size(FRC%rho%output(2)%vata,1),1
          write(11,*)  FRC%rho%output(2)%vata(ii,:)
      end do

      write(11,*) "deltain"
      write(11,*) "deltain%m, deltain%n, deltain%mblock, deltain%nblock, deltain%mproc, deltain%nproc"
      write(11,*)  FRC%rho%deltain%m,      FRC%rho%deltain%n
      write(11,*)  FRC%rho%deltain%mblock, FRC%rho%deltain%nblock
      write(11,*)  FRC%rho%deltain%mproc,  FRC%rho%deltain%nproc
      write(11,*) "vata"
      do ii = 1,size(FRC%rho%deltain%vata,1),1
          write(11,*)  FRC%rho%deltain%vata(ii,:)
      end do

      write(11,*) "deltaout"
      write(11,*) "deltaout%m, deltaout%n, deltaout%mblock, deltaout%nblock, deltaout%mproc, deltaout%nproc"
      write(11,*)  FRC%rho%deltaout(3)%m,      FRC%rho%deltaout(3)%n
      write(11,*)  FRC%rho%deltaout(3)%mblock, FRC%rho%deltaout(3)%nblock
      write(11,*)  FRC%rho%deltaout(3)%mproc,  FRC%rho%deltaout(3)%nproc
      write(11,*) "vata"
      do ii = 1,size(FRC%rho%deltaout(3)%vata,1),1
          write(11,*)  FRC%rho%deltaout(3)%vata(ii,:)
      end do

      write(11,*) "atom"
      write(11,*) "atom%m, atom%n, atom%mblock, atom%nblock, atom%mproc, atom%nproc"
      write(11,*)  FRC%rho%atom%m,      FRC%rho%atom%n
      write(11,*)  FRC%rho%atom%mblock, FRC%rho%atom%nblock
      write(11,*)  FRC%rho%atom%mproc,  FRC%rho%atom%nproc
      write(11,*) "vata"
      do ii = 1,size(FRC%rho%atom%vata,1),1
          write(11,*)  FRC%rho%atom%vata(ii,:)
      end do

      write(11,*) "gradRpc"
      write(11,*) "gradRpc%m, gradRpc%n, gradRpc%mblock, gradRpc%nblock, gradRpc%mproc, gradRpc%nproc"
      write(11,*)  FRC%rho%gradRpc%m,      FRC%rho%gradRpc%n
      write(11,*)  FRC%rho%gradRpc%mblock, FRC%rho%gradRpc%nblock
      write(11,*)  FRC%rho%gradRpc%mproc,  FRC%rho%gradRpc%nproc
      write(11,*) "vata", size(FRC%rho%gradRpc%vata,1), size(FRC%rho%gradRpc%vata,2)
      !do ii = 1,size(FRC%rho%gradRpc%vata,1),1
          !write(11,*) "to"
      !    write(11,*)  FRC%rho%gradRpc%vata(ii,:)
      !end do
      !write(11,*)  FRC%rho%gradRpc%vata(1745,2)

      write(11,*) "pc"
      write(11,*) "pc%m, pc%n, pc%mblock, pc%nblock, pc%mproc, pc%nproc"
      write(11,*)  FRC%rho%pc%m,      FRC%rho%pc%n
      write(11,*)  FRC%rho%pc%mblock, FRC%rho%pc%nblock
      write(11,*)  FRC%rho%pc%mproc,  FRC%rho%pc%nproc
      write(11,*) "vata"
      do ii = 1,size(FRC%rho%pc%vata,1),1
          write(11,*)  FRC%rho%pc%vata(ii,:)
      end do

      close(11)
      return
      end subroutine writeRho
      end module writeRho_module
