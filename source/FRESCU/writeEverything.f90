! writeEverything.f90

      module writeEverything_module
      contains
      subroutine writeEverything(FRC)

      use FORTRAN_RESCU_CALCULATION_TYPE
      use writeAtom_module
      use writeDomain_module
      use writeDiffop_module
      use writeeigenSolver_module
      use writeElement_module
      use writeInfo_module
      use writeKpoint_module
      use writePotential_module
      use writeRho_module
      use writeEnergy_module

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam

      !write(*,*) "I'm here in writeEverything.f90."
      filnam = 'atom.txt'
      call writeAtom(FRC,filnam)

      filnam = 'domain.txt'
      call writeDomain(FRC,filnam)

      filnam = 'diffop.txt'
      call writeDiffop(FRC,filnam)

      filnam = 'eigenSolver.txt'
      call writeEigenSolver(FRC,filnam)

      filnam = 'element.txt'
      call writeElement(FRC,filnam)

      filnam = 'info.txt'
      call writeInfo(FRC,filnam)

      filnam = 'kpoint.txt'
      call writeKpoint(FRC,filnam)

      filnam = 'potential.txt'
      call writePotential(FRC,filnam,1)

      filnam = 'rho.txt'
      call writeRho(FRC,filnam,1)

      filnam = 'energy.txt'
      call writeEnergy(FRC,filnam,1)

      return
      end subroutine writeEverything
      end module writeEverything_module
