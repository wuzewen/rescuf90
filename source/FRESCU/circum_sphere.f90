! circum_sphere.f90

!***********************************************************************
      !
      !
      !
!***********************************************************************

      subroutine circum_sphere(avec,rad)

      use ndgridA_module

      implicit none

      ! input variables
      real*8  :: avec(3,3)

      ! temporary varaibles
      real*8 , allocatable :: X(:), radtmp(:,:), radarr(:)

      ! output variables
      real*8  :: rad
      
      ! body of this function
      allocate(X(6))
      allocate(radtmp(216,3))

      X(1) = -0.5D0
      X(2) =  0.5D0
      X(3) = -0.5D0
      X(4) =  0.5D0
      X(5) = -0.5D0
      X(6) =  0.5D0

      call ndgridA(X,X,X,radtmp)
      radtmp = matmul(radtmp,avec)
      radtmp = radtmp*radtmp

      allocate(radarr(216))
      radarr = sum(radtmp,2)
      radarr = sqrt(radarr)
      rad    = maxval(radarr)

      return
      end subroutine circum_sphere
