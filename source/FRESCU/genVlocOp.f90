! genVlocOp.f90
    
!*****************************************************
    !
    !
    !
!*****************************************************
    
    module genVlocOp_module
    contains
    !subroutine genVlocOp(FRC,vloc,gpu,X,Xu,Xd,out1,out2)
    subroutine genVlocOp(FRC,vloc,gpu,X,Xu,Xd,out1)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use bsxfunTimes_CMPLX_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    logical :: gpu
    real*8    , allocatable :: vloc(:,:)
    complex*16, allocatable :: X(:,:), Xu(:,:), Xd(:,:)!, vloc(:,:)
    
    ! temporary variables
    integer             :: ispin, ii
    real*8, allocatable :: out1tmp(:,:), out2tmp(:,:)
    
    ! output variables
    complex*16, allocatable :: out1(:,:)!, out2(:,:)
    
    ! body of this function
    ispin = FRC%spin%ispin
    
    if (gpu) then
        write(*,*) "Error in genVlocOp.f90. gpu is not avalaible now."
        stop
        !call gpuArray(vloc)
    end if
    
    if (ispin == 1 .or. ispin == 2) then
        allocate(out1(max(size(vloc,1),size(X,1)),max(size(vloc,2),size(X,2))))
        !write(*,*) 'sizes in genVlocOp'
        !write(*,*) 'X',size(X,1),size(X,2)
        !write(*,*) 'vloc', size(vloc,1),size(vloc,2)
        !write(*,*)  vloc
        !call bsxfunTimes_CMPLX(X,vloc,out1)
        do ii = 1,size(X,2),1
            out1(:,ii) = X(:,ii)*vloc(:,1)
            !out1(:,ii) = out1(:,ii) + X(:,ii)*vloc(:,1)
        end do
        !write(*,*) 'marker in genVlocOp.'
    else if (ispin == 4) then
        write(*,*) "Error in genVlocOp.f90. Ispin can't be 4, but should be 1 or 2."
        stop
        !call bsxfunTimes(xu,vloc(:,1),out1tmp)
        !call bsxfunTimes(xd,vloc(:,3),out1)
        !out1 = out1tmp+out1
        !call bsxfunTimes(xu,vloc(:,2),out2tmp)
        !call bsxfunTimes(xd,vloc(:,4),out2)
        !out2 = out2tmp+out2
    end if
    
    return
    end subroutine genVlocOp
    end module genVlocOp_module
