! isqrt.f90

!************************************************************
      !
      !
      !
!************************************************************

      subroutine isqrt(X,m,n)

      implicit none

      ! input variables
      integer :: X

      ! temporary variables
      integer :: tmp, ii, nn
      real*8  :: rtmp, mtmp

      ! output variables
      integer :: m, n

      ! body of this function
      rtmp = sqrt(dble(X))

      if (rtmp == dble(int(rtmp))) then
              m = int(rtmp)
              n = int(rtmp)
      else
              nn = int(rtmp)
              do ii = 1,nn,1
                      mtmp = dble(X)/dble(ii)
                      if (mtmp == dble(int(mtmp))) then
                              m = ii
                              n = int(mtmp)
                      end if
              end do
      end if

      return
      end subroutine isqrt

