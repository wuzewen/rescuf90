!  UpdateRHO.f90 
!****************************************************************************
!
!  PROGRAM: UpdateRHO
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module UpdateRHO_module
    contains
    subroutine UpdateRHO(FRC,nocc,rho)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use GetGlobalInd_2D_module
    use calcRho_module
    use modBCDist_2D_module
    use symmetrize_2D_module

    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable             :: nocc(:,:,:)
    
    ! temporery variables
    logical :: mpistat
    integer :: nband, ntmp
    integer :: mpirank, mpisize, ispin, nspin, ngrid, nkpt, kk, ss, ii, jj
    type(VataMN_1D) :: rhotmp
    type(VataMN_2D) :: rhosym
    type(VataMN_CMPLX_2D) :: tmppsi
    real*8 , allocatable :: tmpnocc(:), symrec(:,:,:)
    integer, allocatable :: ia(:,:), ja(:,:)
    
    ! output variables
    type(VataMN_2D) :: rho

    ! Body of UpdateRHO
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    ngrid   = product(FRC%domain%fgridn)
    nkpt    = size(FRC%kpoint%ikdirect,1)
    nband   = FRC%eigenSolver%nband
    allocate(tmpnocc(size(nocc,1)))
    !write(*,*) "I'm here at the begining of UpdateRHO."    
    !write(*,*) "size of nocc in UpdateRho."
    !write(*,*)  size(nocc,1), size(nocc,2), size(nocc,3)
    !write(*,*)  nocc
    allocate(tmppsi%vata(ngrid,nband))
    if (FRC%mpi%paraK) then
        !allocate(RHO(ngrid,ispin))
        !do
        write(*,*) "Error in UpdateRHO.f90. FRC%mpi%paraK is not available now."
        stop
    else
        ntmp = ceiling(dble(ngrid)/dble(mpisize))
        call InitDistArray_2D(ngrid,ispin,ntmp,1,mpisize,1,rho)
        !write(*,*) "I'm here after initDistArray_2D in UpdateRHO."
        call GetGlobalInd_2D(mpirank,rho,ia,ja)
        !write(*,*) "I'm here after GetGlobalInd_2D in UpdateRHO."
        !allocate(rho%vata(size(ia),size(ja)))
        !allocate(rho%vata(ngrid,nspin))
        allocate(rhotmp%vata(size(ia)))
        rho%vata = 0.0D0
        do kk = 1,nkpt,1
            do ss = 1,nspin,1
                if (ispin == 1 .or. ispin == 2) then
                    !write(*,*) "I'm here in the loop in UpdateRHO."
                    tmpnocc = nocc(:,kk,ss)
                    !write(*,*) "I'm here after tmpnocc in the loop in UpdateRHO."
                    tmppsi  = FRC%psi(kk,ss)
                    !write(*,*) "I'm here before calcRho, in UpdateRHO."
                    !write(*,*) "input wavefunction of calcRho in updateRho."
                    !do ii = 1,size(tmppsi%vata,2),1
                    !    do jj = 1,size(tmppsi%vata,1),1
                    !        write(*,*) jj,ii,tmppsi%vata(jj,ii)
                    !    end do
                    !end do
                    call calcRho(FRC,tmpnocc,tmppsi,rhotmp)
                    !write(*,*) "size of rhotmp%vata"
                    !write(*,*)  size(rhotmp%vata)

                    !write(*,*) "in updateRho, rhotmp%vata ="
                    !write(*,*)  rhotmp%vata
                    !write(*,*) "size of rho%vata", mpirank, &
                    !size(rho%vata,1), size(rho%vata,2), &
                    !size(rhotmp%vata)!, size(rhotmp%vata,2)
                    !write(*,*) "I'm here after calcRho, in UpdateRHO."
                    rho%vata(:,ss) = rho%vata(:,ss)+rhotmp%vata
                else if (ispin == 4) then
                    write(*,*) "Error in UpdateRHO.f90. FRC%spin%ispin should be 1 now."
                    stop
                end if
                !write(*,*) "stop in updateRho."
                !stop
            end do
        end do
    end if
    !write(*,*) "I'm here after loop in UpdateRHO and stop."
    !stop
    if (FRC%symmetry%pointsymmetry) then
        allocate(rhosym%vata(rho%m,rho%n))
        call ModBCDist_2D(FRC,rho,rho%m,rho%n,mpisize,1,.FALSE.,.FALSE.,rhosym)
        if (mpirank == 0) then
            allocate(symrec(size(FRC%symmetry%sym_rec,1),size(FRC%symmetry%sym_rec,2),size(FRC%symmetry%sym_rec,3)))
            symrec = dble(FRC%symmetry%sym_rec)
            call symmetrize_2D(FRC,rhosym%vata,FRC%domain%fgridn,symrec,FRC%symmetry%sym_t)
        end if
        !write(*,*) "rhosym after symmetrize in UpdateRho."
        !write(*,*)  rhosym%vata
        call ModBCDist_2D(FRC,rhosym,rho%mblock,1,mpisize,1,.FALSE.,.FALSE.,rho)
    end if
    !write(*,*) "rhosym after ModBCDist_2D in UpdateRho."
    !write(*,*)  rhosym%vata
    !write(*,*) "stop at the end of UpdateRho."
    !stop
    return
    end subroutine UpdateRHO
    end module UpdateRHO_module

