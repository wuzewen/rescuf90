! smiDSYGST.f90

!************************************************
      !
      !
      !
!****************************************************

      module smiDSYGSTMOD
      contains
      subroutine smiDSYGST(N,A,B,nprocs,MB,NB,nprow,npcol)

      implicit none 

      integer :: N, nprocs, MB, NB, nprow, npcol
      complex*16, allocatable :: A(), B()

      integer :: IBTYPE, INFO
      integer :: iam, ictxt, myrow, mycol
      character(len=1) :: UPLO

      IBTYPE =  1
      UPLO   = 'U'
      iam    =  0
      ictxt  =  0
      myrow  =  0
      mycol  =  0

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Rpw-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      call numroc(N,MB,myrow,izero,nprow,B_np)
      A_np = 
      B_np = 

      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call descinit(descB,N,N,MB,NB,izero,izero,ictxt,B_np,descinfo)

      call PZHEGST(IBTYPE,UPLO,N,A,ione,ione,descA,B,ione,ione,&
              descB,cale,INFO)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiZHEGST
      end module smiZHEGSTMOD

      end subroutine smiDSYGST
      end module smiDSYGSTMOD
