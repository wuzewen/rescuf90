! distmat_gather.f90
    
!**************************************
    !
    ! 
    !
!**************************************
    
    module distmat_allgather_1D_module
    contains
    subroutine distmat_allgather_1D(FRC,dA,A)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use GetGlobalInd_1D_module
    use findnonzero_module
    use VataMN_1D_module
    use ModBCDist_1D_module
    use MPI_Bcast_variable_1D_module
    
    implicit none
    
    ! input variables
    integer                         :: rank
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_1D)                 :: dA
    
    ! temporary variables
    integer         :: mpirank, mpisize
    integer         :: sparseS, ii
    type(VataMN_1D) :: Atmp
    integer, allocatable :: ranks(:)
    
    ! output variables
    real*8, allocatable :: A(:)
    
    ! body of this function
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    sparseS = 0
    !call distmat_issparse(FRC,dA,sparseS)
    
    if (mpisize == 1) then
        A = dA%vata
    else if (sparseS < 1) then
        
        !write(*,*) "Error in distmat_gather.f90. mpi is not available now."
        !stop
        allocate(Atmp%vata(dA%m),A(dA%m))
        call ModBCDist_1D(FRC,dA,dA%m,dA%n,dA%mproc,dA%nproc,.FALSE.,.FALSE.,Atmp)
        rank = 0
        allocate(ranks(mpisize))
        forall(ii=1:mpisize)
                ranks(ii) = ii-1
        end forall
        call MPI_Bcast_variable_1D(Atmp%vata,rank,ranks,A)
        !A = Atmp%vata
        !if (rank /= 0) then
        !    if (mpirank == 0) then
        !        call MPI_Send_variables(A,rank,0)
        !        deallocate(A)
        !    else if (mpirank == rank) then
        !        call MPI_Recv_variable(0,0,A)
        !    end if
        !end if
    else
        
        write(*,*) "Error in distmat_gather.f90. Sparse Matrix is not available now."
        stop
        
        !if (mpirank == 0) then
        !    call GetGlobalInd(mpirank,dA,iloc,jloc)
        !    call findnonzero(dA%dataA,iA,jA,valA)
        !    Atmp(1,1)%vata(:,1) = iloc(iA)
        !    Atmp(1,1)%vata(:,2) = jloc(iA)
        !    Atmp(1,1)%vata(:,3) = valA
        !end if
        
        !do ii = 1,mpisize-1,1
        !    if (mpirank == ii) then
        !        call GetGlobalInd(mpirank,dA,iloc,jloc)
        !        call findnonzero(dA%dataA,iA,jA,valA)
        !        valA(:,1) = iloc(iA)
        !        valA(:,2) = jloc(jA)
        !        valA(:,3) = valA
        !        call MPI_Send_variable(valA,0,0)
        !    end if
        !    if (mpirank == 0) then
        !        call MPI_Recv_variable(ii,0,Atmp(ii+1,1))
        !    end if
        !end do
        
        !if (mpirank == 0) then
        !    call cellfun(Atmp,isnul)
        !    Atmp(isnul) = 0
        !else
        !    deallocate(A)
        !end if
        
        !if (rank /= 0) then
        !    if (mpirank == 0) then
        !        
        !        
        !        
        !        !call MPI_Send_variable(A,rank,0)
        !        !deallocate(A)
        !    else if (mpirank == rank) then
        !        call MPI_Recv_variable(0,0)
        !    end if
        !end if
    end if
    
    return
    end subroutine distmat_allgather_1D
    end module distmat_allgather_1D_module
            
