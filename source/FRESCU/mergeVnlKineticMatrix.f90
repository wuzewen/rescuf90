! mergeVnlKineticMatrix.f90

!***************************************************************
      !
      !
      !
!***************************************************************************

      module mergeVnlKineticMatrixMod
      contains
      subroutine mergeVnlKineticMatrix(FRC)

      implicit none

      Scell = FRC%LCAO%Scell
      Tcell = FRC%LCAO%Tcell
      Svec  = FRC%LCAO%Svec
      stv   = FRC%LCAO%Svec
      kbcell = FRC%LCAO%kbcell
      tkb    = FRC%LCAO%kbtv
      kbe    = FRC%potential%vnl%Eorb

      call mergeDispPairs(FRC,Tcell,stv,kbcell,tkb,kbe,Tcell,stv)
      call ismember(stv,Svec,ivec,jvec)

      do ii =1,size(jvec),1
          if (jvec(ii) .NE. 0) then
              tmp(ii) = Scell
          end if
      end do
      Scell = tmp

      call sprucecell(FRC,Scell,idel)
      sv = 
      call symmetrizecell(FRC,Scell,sv)

      call sprucecell(FRC,Tcell,Tcell)
      tv = 
      call symmetrizeCell(FRC,Tcell,tv)

      FRC%LCAO%Scell = Scell
      FRC%LCAO%Tcell = Tcell
      FRC%LCAO%Svec  = sv
      FRC%LCAO%Tvec  = tv

      return
      end subroutine mergeVnlKineticMatrix
      end module mergeVnlKineticMatrixMod
