! upGradeGrid.f90
    
    module upGradeGrid_module
    contains
    subroutine upGradeGrid(nv, symop, upG)
    
    implicit none
    
    ! input valuables
    integer              :: nv(3)
    real*8, allocatable  :: symop(:,:,:)
    
    ! temporery valuables
    
    ! output valuables
    integer              :: upG(3)
    
    ! body of this function
    upG = nv
    if ((any(symop(1,2,:) /=0)) .or. (any(symop(2,1,:) /=0))) then
        upG(1) = max(upG(1),upG(2))
        upG(2) = max(upG(1),upG(2))
    end if
    
    if ((all(symop(2,3,:) /=0)) .or. (any(symop(3,2,:) /=0))) then
        upG(2) = max(upG(2),upG(3))
        upG(3) = max(upG(2),upG(3))
    end if
    
    if ((any(symop(1,3,:) /=0)) .or. (any(symop(3,1,:) /=0))) then
        upG(1) = max(upG(1),upG(3))
        upG(3) = max(upG(1),upG(3))
    end if
    
    if ((any(symop(2,3,:) /=0)) .or. (any(symop(3,2,:) /=0))) then
        upG(2) = max(upG(2),upG(3))
        upG(3) = max(upG(2),upG(3))
    end if
    
    if ((any(symop(1,2,:) /=0)) .or. (any(symop(2,1,:) /=0))) then
        upG(1) = max(upG(1),upG(2))
        upG(2) = max(upG(1),upG(2))
    end if
    
    
    return
    end subroutine upGradeGrid
    end module upGradeGrid_module
    
