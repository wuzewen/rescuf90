! DISTMATISSAME.f90

!*************************************************************************************
      !
      !
      !
!*************************************************************************************

      module DISTMATISSAMEMOD

      interface DISTMATISSAME
              module procedure distmat_issame_1D
              module procedure distmat_issame_2D
      end interface DISTMATISSAME

      end module DISTMATISSAMEMOD
