! writeEnergy.f90

      module writeEnergy_module
      contains
      subroutine writeEnergy(FRC,filnam,iter)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam
      integer                         :: iter


      open(unit=11,file=filnam)
      write(11,*) "Fermi Energy = ", FRC%energy%Efermi
      write(11,*) "Ewald Energy = ", FRC%energy%ewaldE

      write(11,*) "Eks"
      write(11,*)  FRC%energy%Eks
      write(11,*) "Etot"
      write(11,*)  FRC%energy%Etot
      write(11,*) "EdH"
      write(11,*)  FRC%energy%EdH
      write(11,*) "nocc"
      write(11,*)  size(FRC%energy%nocc,1), size(FRC%energy%nocc,2), size(FRC%energy%nocc,3)
      write(11,*)  FRC%energy%nocc
      write(11,*) "entropy"
      write(11,*)  size(FRC%energy%entropy,1), size(FRC%energy%entropy,2)

      write(11,*) "XCdens"
      write(11,*)  FRC%energy%XCdens%m, FRC%energy%XCdens%n, &
                   FRC%energy%XCdens%mblock, FRC%energy%XCdens%nblock, &
                   FRC%energy%XCdens%mproc, FRC%energy%XCdens%nproc
      write(11,*)  FRC%energy%XCdens%vata

      close(11)

      return
      end subroutine writeEnergy
      end module writeEnergy_module
