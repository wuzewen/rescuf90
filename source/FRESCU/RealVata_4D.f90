! RealVata_4D.f90
    
!***********************************************************
    !
    ! This module is used to define a type of data.
    ! The data is 4 dimentional allocatable real array.
    !
!***********************************************************
    
    module RealVata_4D_module
    
    type :: RealVata_4D
        real*8, allocatable :: vata(:,:,:,:)
    end type RealVata_4D
    
    end module RealVata_4D_module
