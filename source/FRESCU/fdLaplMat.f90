! fdGradMat.f90
    
    module fdLaplMat_module
    contains
    subroutine fdLaplMat(n,acc,bc,Lu,Lv,Lw)
    
    use stencil_module
    use galleryCircul_module
    use repMat_module
    use spdiags_module
    
    implicit none
    
    ! input valuables
    integer              :: n(3), bc(3)
    integer              :: acc
    
    ! temporery valuables
    integer              :: i, j, k, itemp
    real*8, allocatable  :: stcl(:), stc2(:), stc3(:), stc4(:)
    real*8, allocatable :: stctmp(:,:)
    integer              :: acctmp, npoint
    
    ! output valuables
    real*8, allocatable    :: Lu(:,:), Lv(:,:), Lw(:,:)
    
    ! body of this function
    acctmp = 2*(minval(n-1)/2)
    acc    = min(acc,acctmp)
    
    !write(*,*) "acc"
    !write(*,*)  acc
    !write(*,*) "bc"
    !write(*,*)  bc
    
    npoint = acc+1
    allocate(stcl(npoint))
    call stencil(2,acc,stcl)
    
    !write(*,*) "stcl"
    !write(*,*)  stcl

    if (bc(1)/=0) then
        allocate(stc2(n(1)))
        do i=1,(npoint+1)/2,1
            itemp   = i+(npoint+1)/2-1
            stc2(i) = stcl(itemp)
        end do
        
        forall(i=((npoint+3)/2):(n(1)-(npoint+1)/2+1))
            stc2(i) = 0.0D0
        end forall
        forall(i=(n(1)-(npoint+1)/2+2):n(1))
            stc2(i) = stcl(i-(n(1)-(npoint+1)/2+1))
        end forall

        !write(*,*) "stc2"
        !write(*,*)  stc2
        
        
        call galleryCircul(stc2,Lu)
        
        !write(*,*) "Lu"
        !write(*,*)  Lu
        
    else
        allocate(stctmp(npoint,n(1)))
        call repMat(stcl,n(1),1,stctmp)
        allocate(stc2(2*((npoint-1)/2)+1))
        
        forall(i=1:npoint)
            stc2(i) = dble(i-1-(npoint-1)/2)
        end forall
        
        !call arrayN(-(npoint-1)/2,(npoint-1)/2,stc2)
        
        call spdiags(stctmp,stc2,n(1),n(1),Lu)
    end if
    
    Lu = Lu*dble(n(1))*dble(n(1))
    Lu = (Lu+transpose(Lu))/2.0D0
    
    if (bc(2) /= 0) then
        allocate(stc3(n(2)))
        forall(i=1:(npoint+1)/2)
            stc3(i) = stcl(i+(npoint+1)/2-1)
        end forall
        
        forall(i=(npoint+3)/2:(n(2)-(npoint+1)/2+1))
            stc3(i) = 0.0D0
        end forall
        forall(i=(n(2)-(npoint+1)/2+2):n(2))
            stc3(i) = stcl(i-(n(2)-(npoint+1)/2+1))
        end forall
!        allocate(Lv(n(2),n(2)))
        call galleryCircul(stc3,Lv)
    else
        allocate(stctmp(npoint,n(2)))
        call repMat(stcl,n(2),1,stctmp)
        allocate(stc2(2*((npoint-1)/2)+1))
        
        forall(i=1:npoint)
            stc2(i) = dble(i-1-(npoint-1)/2)
        end forall
        !call arrayN(-(npoint-1)/2,(npoint-1)/2,stc2)
        allocate(Lv(n(2),n(2)))
        call spdiags(stctmp,stc2,n(2),n(2),Lv)
    end if
    Lv = Lv*dble(n(2))*dble(n(2))
    Lv = 0.5D0*(Lv+transpose(Lv))
    
    if (bc(3)/=0) then
        allocate(stc4(n(3)))
        forall(i=1:(npoint+1)/2)
            stc4(i) = stcl(i+(npoint+1)/2-1)
        end forall
        
        forall(i=((npoint+3)/2):(n(3)-(npoint+1)/2+1))
            stc4(i) = 0.0D0
        end forall
        forall(i=(n(3)-(npoint+1)/2+2):n(3))
            stc4(i) = stcl(i-(n(3)-(npoint+1)/2+1))
        end forall
!        allocate(Lw(n(3),n(3)))
        call galleryCircul(stc4,Lw)
    else
        allocate(stctmp(npoint,n(3)))
        call repMat(stcl,n(3),1,stctmp)
        allocate(stc2(2*((npoint-1)/2)+1))
        
        forall(i=1:npoint)
            stc2(i) = dble(i-1-(npoint-1)/2)
        end forall
        !call arrayN(-(npoint-1)/2,(npoint-1)/2,stc2)
        allocate(Lw(n(3),n(3)))
        call spdiags(stctmp,stc2,n(3),n(3),Lw)
    end if
    Lw = Lw*dble(n(3))*dble(n(3))
    Lw = (Lw+transpose(Lw))/2.0D0
    
    return
    end subroutine fdLaplMat
    end module fdLaplMat_module
