! setBoundaryValue.f90
    
!********************************************************
    !
    ! set boudary value for solving poisson equation
    !
!********************************************************
    
    module setBoundaryValue_module
    contains
    subroutine setBoundaryValue(FRC,rho,bc,VH)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use triu_module
    use tril_module
    use inversion_module
    
    implicit none
    
    ! input valuables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable :: rho(:,:,:)
    
    ! temporery valuables
    real*8, allocatable  :: bval(:,:,:)
    integer              :: nx, ny, nz
    integer              :: bc(3), szrho(3)
    real*8, allocatable  :: avec(:,:), j1(:,:), j2(:,:)
    !real, allocatable    :: VH(:,:,:)
    real, allocatable    :: bvalx(:,:,:), bvaly(:,:,:), bvalz(:,:,:)
    
    ! output valuables
    complex*16, allocatable  :: VH(:,:,:)
    
    
    ! body of this function
    allocate(avec(3,3),j2(3,3),j1(3,3))
    avec  = FRC%domain%latvec
    j2    = transpose(avec)
    j2    = matmul(avec,j2)
    call inversion(j2,avec)
    call triu(avec,0,j2)
    call tril(avec,-1,j1)
    j2 = transpose(j1)+j2
    szrho(1) = size(rho,1)
    szrho(2) = size(rho,2)
    szrho(3) = size(rho,3)
    !allocate(VH(szrho(1),szrho(2),szrho(3)))
    VH       = dcmplx(rho)
    !! ************************************
    !! set boudary value in x direction 
    
    if (bc(1)==0) then
        
        write(*,*) "Error in setBoundaryValue.f90. bc(1) should set to 1 now, others are not avalaible."
        stop
        
        !bval = FRC%domain%bvalx(1)
        !if () then
        !    bval
        !else if () then
        !    bval
        !end if
        !VH(1,:,:) = VH(1,:,:)+bval*((real(nx))**2)*j2(1,1)*0.25/pi
        
        !bval = bvalx{2}
        !if () then
        !    bval
        !else if () 
        !    bval
        !end if
        !setBoundaryValue(nx,:,:) = VH(nx,:,:)+bval*((real(nx))**2)*j2(1,1)*0.25/pi
    end if
    
    !! ************************************
    !! set boudary value in y direction
    
    if (bc(2)==0) then
        
        write(*,*) "Error in setBoundaryValue.f90. bc(2) should set to 1 now, others are not avalaible."
        stop
        
        !bval = bvaly{1}
        !if () then
        !    bval = 
        !else if () then
        !    bval = 
        !end if
        !VH(:,1,:) = VH(:,1,:)+bval*((real(ny))**2)*j2(2,2)*0.25/pi
       ! 
        !bval = bvaly{2}
        !if () then
        !    bval = 
        !else if () then
        !    bval = 
        !end if
        !setBoundaryValue(:,ny,:) = VH(:,ny,:)+bval*((real(ny))**2)*j2(2,2)*0.25/pi
    end if
    
    !! ************************************
    !! set boudary value in z direction
    
    if (bc(3)==0) then
        
        write(*,*) "Error in setBoundaryValue.f90. bc(3) should set to 1 now, others are not avalaible."
        stop
        
        !bval = bvalz{1}
        !if () then
        !    bval = 
        !else if () then
        !    bval = 
        !end if
        !VH(:,:,1) = VH(:,:,1)+bval*((real(nz))**2)*j2(2,2)*0.25/pi
        
        !bval = bvalz{2}
        !if () then
        !    bval = 
        !else if () then
        !    bval = 
        !end if
        !setBoundaryValue(:,:,nz) = VH(:,:,nz)+bval*((real(nz))**2)*j2(2,2)*0.25/pi
    end if
    
    return
    end subroutine setBoundaryValue
    end module setBoundaryValue_module
