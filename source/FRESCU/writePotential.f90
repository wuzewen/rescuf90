! writePotential.f90

      module writePotential_module
      contains
      subroutine writePotential(FRC,filnam,iter)

      use FORTRAN_RESCU_CALCULATION_TYPE

      implicit none

      type(FORTRAN_RESCU_CALCULATION) :: FRC
      character(len=30)               :: filnam, newfile
      integer                         :: ii, jj, iter
      
      open(unit=11,file=filnam)
      write(11,*) "fourierinit"
      write(11,*)  FRC%potential%fourierinit
      write(11,*) "vloc1"
      write(11,*)  FRC%potential%vloc1
      write(11,*) "vloc"
      do ii = 1,size(FRC%potential%vloc,1),1
          write(11,*)  FRC%potential%vloc(ii,:)
      end do

      write(11,*) "vtau"
      write(11,*) "vtau%m, vtau%n, vtau%mblock, vtau%nblock, vtau%mproc, vtau%nproc"
      write(11,*)  FRC%potential%vtau%m,      FRC%potential%vtau%n
      write(11,*)  FRC%potential%vtau%mblock, FRC%potential%vtau%nblock
      write(11,*)  FRC%potential%vtau%mproc,  FRC%potential%vtau%nproc
      write(11,*) "vata"
      do ii = 1,size(FRC%potential%vtau%vata,1),1
          write(11,*)  FRC%potential%vtau%vata(ii,:)
      end do

      write(11,*) "vtauloc"
      do ii = 1,size(FRC%potential%vtauloc,1),1
          write(11,*)  FRC%potential%vtauloc(ii,:)
      end do

      write(11,*) "vna"
      write(11,*) "vna%m, vna%n, vna%mblock, vna%nblock, vna%mproc, vna%nproc"
      write(11,*)  FRC%potential%vna%m,      FRC%potential%vna%n
      write(11,*)  FRC%potential%vna%mblock, FRC%potential%vna%nblock
      write(11,*)  FRC%potential%vna%mproc,  FRC%potential%vna%nproc
      write(11,*) "vata"
      !do ii = 1,size(FRC%potential%vna%vata),1
      write(11,*)  FRC%potential%vna%vata
      !end do

      write(11,*) "vps"
      write(11,*) "vps%m, vps%n, vps%mblock, vps%nblock, vps%mproc, vps%nproc"
      write(11,*)  FRC%potential%vps%m,      FRC%potential%vps%n
      write(11,*)  FRC%potential%vps%mblock, FRC%potential%vps%nblock
      write(11,*)  FRC%potential%vps%mproc,  FRC%potential%vps%nproc
      write(11,*) "vata"
      !do ii = 1,size(FRC%potential%vps%vata),1
      write(11,*)  FRC%potential%vps%vata
      !end do

      newfile = "Vnl"//filnam
      open(unit=12,file=newfile)
      write(12,*) "vnl"
      write(12,*) "kbsparse, separable"
      write(12,*)  FRC%potential%vnl%kbsparse, FRC%potential%vnl%separable
      write(12,*) "evec"
      write(12,*)  FRC%potential%vnl%evec
      write(12,*) "Lorb"
      write(12,*)  FRC%potential%vnl%Lorb
      write(12,*) "Morb"
      write(12,*)  FRC%potential%vnl%Morb
      write(12,*) "Norb"
      write(12,*)  FRC%potential%vnl%Norb
      write(12,*) "Oorb"
      write(12,*)  FRC%potential%vnl%Oorb
      write(12,*) "Sorb"
      write(12,*)  FRC%potential%vnl%Sorb
      write(12,*) "Rorb"
      write(12,*)  FRC%potential%vnl%Rorb
      write(12,*) "Eorb"
      write(12,*)  FRC%potential%vnl%Eorb
      write(12,*) "Aorb"
      write(12,*)  FRC%potential%vnl%Aorb
      write(12,*) "KBenergy"
      write(12,*)  FRC%potential%vnl%KBenergy
      write(12,*) "vnlsize"
      write(12,*)  FRC%potential%vnl%vnlsize

      write(12,*) "kbvec"
      do ii = 1,size(FRC%potential%vnl%kbvec,1),1
          write(12,*)  FRC%potential%vnl%kbvec(ii,:)
      end do

      write(12,*) "kborb"
      do ii = 1,size(FRC%potential%vnl%kborb,1),1
          write(12,*)  FRC%potential%vnl%kborb(ii,:)
      end do
      !write(*,*) "I'm here before KBcell in writePotential."
      do ii = 1,size(FRC%potential%vnl%kbcell),1
          write(12,*) "kbcell", ii
          do jj = 1,size(FRC%potential%vnl%kbcell(ii)%vata,1),1
              write(12,*)  jj
              write(12,*)  FRC%potential%vnl%kbcell(ii)%vata(jj,:)
          end do
      end do
      close(12)
      !write(*,*) "I'm here before VXCout in writePotential."
      write(11,*) "VXCout"
      write(11,*) "VXCout%m, VXCout%n, VXCout%mblock, VXCout%nblock, VXCout%mproc, VXCout%nproc"
      write(11,*)  FRC%potential%VXCout%m,      FRC%potential%VXCout%n
      write(11,*)  FRC%potential%VXCout%mblock, FRC%potential%VXCout%nblock
      write(11,*)  FRC%potential%VXCout%mproc,  FRC%potential%VXCout%nproc
      write(11,*) "vata"
      do ii = 1,size(FRC%potential%VXCout%vata,1),1
          write(11,*)  FRC%potential%VXCout%vata(ii,:)
      end do
      !write(*,*) "I'm here before VH in writePotential."
      write(11,*) "VH"
      write(11,*) "VH%m, VH%n, VH%mblock, VH%nblock, VH%mproc, VH%nproc"
      write(11,*)  FRC%potential%VH%m,      FRC%potential%VH%n
      write(11,*)  FRC%potential%VH%mblock, FRC%potential%VH%nblock
      write(11,*)  FRC%potential%VH%mproc,  FRC%potential%VH%nproc
      write(11,*) "vata"
      write(11,*)  FRC%potential%VH%vata
      !write(*,*) "I'm here before VHout in writePotential."
      write(11,*) "VHout"
      write(11,*) "VHout%m, VHout%n, VHout%mblock, VHout%nblock, VHout%mproc, VHout%nproc"
      write(11,*)  FRC%potential%VHout%m,      FRC%potential%VHout%n
      write(11,*)  FRC%potential%VHout%mblock, FRC%potential%VHout%nblock
      write(11,*)  FRC%potential%VHout%mproc,  FRC%potential%VHout%nproc
      write(11,*) "vata"
      write(11,*)  FRC%potential%VHout%vata
      !write(*,*) "I'm here before veffin in writePotential."
      write(11,*) "veffin"
      write(11,*) "veffin%m, veffin%n, veffin%mblock, veffin%nblock, veffin%mproc, veffin%nproc"
      write(11,*)  FRC%potential%veffin(1)%m,FRC%potential%veffin(1)%n
      write(11,*)  FRC%potential%veffin(1)%mblock,FRC%potential%veffin(1)%nblock
      write(11,*)  FRC%potential%veffin(1)%mproc, FRC%potential%veffin(1)%nproc
      write(11,*) "vata", size(FRC%potential%veffin(1)%vata)
      write(11,*)  FRC%potential%veffin(1)%vata
      !write(*,*) "I'm here before veffout in writePotential."
      write(11,*) "veffout"
      write(11,*) "veffout%m, veffout%n, veffout%mblock, veffout%nblock, veffout%mproc, veffout%nproc"
      write(11,*)  FRC%potential%veffout%m,      FRC%potential%veffout%n
      write(11,*)  FRC%potential%veffout%mblock, FRC%potential%veffout%nblock
      write(11,*)  FRC%potential%veffout%mproc,  FRC%potential%veffout%nproc
      write(11,*) "vata"
      write(11,*)  FRC%potential%veffout%vata
      !write(*,*) "I'm here before deltain in writePotential."
      write(11,*) "deltain"
      write(11,*) "deltain%m, deltain%n, deltain%mblock, deltain%nblock, deltain%mproc, deltain%nproc"
      write(11,*)  FRC%potential%deltain%m,      FRC%potential%deltain%n
      write(11,*)  FRC%potential%deltain%mblock, FRC%potential%deltain%nblock
      write(11,*)  FRC%potential%deltain%mproc,  FRC%potential%deltain%nproc
      write(11,*) "vata"
      write(11,*)  FRC%potential%deltain%vata
      !write(*,*) "I'm here before deltaout in writePotential."
      write(11,*) "deltaout"
      write(11,*) "deltaout%m, deltaout%n, deltaout%mblock, deltaout%nblock, deltaout%mproc, deltaout%nproc"
      write(11,*)  FRC%potential%deltaout(2)%m, FRC%potential%deltaout(2)%n
      write(11,*)  FRC%potential%deltaout(2)%mblock,FRC%potential%deltaout(2)%nblock
      write(11,*)  FRC%potential%deltaout(2)%mproc,FRC%potential%deltaout(2)%nproc
      write(11,*) "vata"
      write(11,*)  FRC%potential%deltaout(2)%vata

      close(11)
      return
      end subroutine writePotential
      end module writePotential_module
