! union.f90
    
!********************************************************************
    ! 
    ! 
    ! 
!********************************************************************
    
    module union2_module
    contains
    subroutine union2(A,B,lint)
    
    implicit none
    
    ! input variables
    integer, allocatable :: A(:), B(:)
    
    ! temporary variables
    integer, allocatable :: Atmp(:), linttmp(:)
    integer              :: nA, nB, xx, ntmp, ii, jj
    
    ! output variables
    integer, allocatable :: lint(:)
    
    ! body of this function
    nA = size(A)
    nB = size(B)
    
    allocate(Atmp(1))
    Atmp = A(1)
    
    ntmp = 1
    if (nA == 1) then
        allocate(lint(1))
        lint = A
    else
        allocate(lint(1))
        lint = A(1)
        do ii = 2,nA,1
            xx = A(ii)
            !allocate(linttmp(ii))
            !linttmp = 
            if (all(Atmp /= xx) .and. any(B == xx)) then
                ntmp = ntmp+1
                allocate(linttmp(ntmp))
                forall(jj=1:ntmp-1)
                    linttmp(jj) = lint(jj)
                end forall
                linttmp(ntmp) = xx
                deallocate(lint)
                allocate(lint(ntmp))
                lint = linttmp
            deallocate(linttmp)
            end if
            
            
        end do
    end if
    
    return
    end subroutine union2
    end module union2_module