! InterpRdist2Cart_NoCell.f90
    
!********************************************
    !
    !
    !
!********************************************
    
    module InterpRdist2Cart_NoCell_module
    contains
    subroutine InterpRdist2Cart_NoCell(rrdata,frdata,pos,lqn,avec,nvec,out3)
    
    use RealVata_2D_module
    use RealVata_3D_module
    use inversion_module
    use cellInRange_module
    use bsxfunMinus_module
    use bsxfunTimes_module
    use interp1_module
    use genRealSH_module
    use getLocalIndex_module
    use accumarray1_module
    use getTrans_module
    use splitMat2Cell_module
    use ndgridA_module
    use repmat_module
    use repmat2_module
    use mat2cell3D_module
    use mat2cell4D_module
    use ismemberRow_module
    use Cellrepmat_module
    !use Cell_4D_real
    use RealVata_4D_module
    
    implicit none
    
    ! input variables
    real*8, allocatable  :: rrdata(:), frdata(:)
    real*8               :: pos(1,3), avec(3,3)
    logical              :: outcell
    integer              :: lqn, nvec(3)
    
    ! temporary variables
    real*8               :: eps, rmax
    real*8, allocatable  :: ffgrid(:,:), xtmp(:,:), dist(:), post(:,:), frgrix(:,:)
    real*8, allocatable  :: xtmp1(:), xtmp2(:), xtmp3(:), Ylm(:,:), tv1(:), tv2(:), tv3(:)
    real*8, allocatable  :: frgrid(:), disttmp(:), ffgridtmp(:,:), txlin(:), tylin(:), tzlin(:)
    real*8, allocatable  :: utxyz(:,:), ftmpt(:,:), vtmp(:,:), ffgtmp(:), fxgrid(:,:,:)   !ttmp(:), 
    real*8, allocatable  :: fxgridtmp(:), rrdata2(:), frdata2(:)
    real*8, allocatable  :: ffgridnn(:,:,:,:), ltmp(:,:), ltmpt(:,:), txyz(:,:), indR(:,:,:)
    real*8, allocatable  :: postmp(:,:), utxr(:), utyr(:), utzr(:)
    integer              :: inde, ii, jj, kk, pp, nf, nptvec(3), npoint, tlpl, ntmp, nut, tllpl
    integer              :: itmp, itmp1, itmp2, itmp3, nxtmp, lo1, lo2, lo3, icx, icy, icz
    integer, allocatable :: ind(:,:,:), indtmp(:,:,:), tx(:,:,:), ty(:,:,:), tz(:,:,:), sx(:), sy(:), sz(:)
    integer, allocatable :: utx(:), uty(:), utz(:), txtmp(:,:,:), txtmptmp(:)
    character(len=20)    :: method
    logical              :: issym
    type(RealVata_2D), allocatable  :: ftmp(:), ftmppt(:,:)
    type(RealVata_3D), allocatable :: icell(:,:,:) !fcell(:,:,:), 
    type(RealVata_4D), allocatable :: fcell(:,:,:)!, icell(:,:,:)
    
    
    ! output variables
    real*8, allocatable          :: out3(:)
    
    
    ! body of this function
    eps  = 1D-14
    inde = 0
    nf   = size(frdata)
    do ii = 1,nf,1
        if (abs(frdata(ii))>eps) then ! .and. abs(frdata(ii)) == 1.0) then
            inde = ii
        end if
    end do
    inde = inde + 1
    
    allocate(rrdata2(inde))
    allocate(frdata2(inde))
    forall(ii=1:inde)
            rrdata2(ii) = rrdata(ii)
            frdata2(ii) = frdata(ii)
    end forall
    if (inde<size(rrdata)) then
        frdata2(inde) = 0.0D0
    end if
    rmax = maxval(rrdata2)
    
    allocate(ltmp(3,3),ltmpt(3,3))
    ltmp = 0.0D0
    forall(ii=1:3)
        ltmp(ii,ii) =  dble(nvec(ii))
    end forall
    call inversion(ltmp,ltmpt)
    ltmp  = matmul(ltmpt,avec)
    issym = .FALSE.
    
    allocate(post(1,3))
    post = pos
    call cellInRange(ltmp,post,rmax,issym,txyz)
    
    do ii = 1,3,1
        nptvec(ii) = maxval(txyz(:,ii))-minval(txyz(:,ii))+1
    end do
    
    allocate(tx(nptvec(1),nptvec(2),nptvec(3)), ty(nptvec(1),nptvec(2),nptvec(3)),tz(nptvec(1),nptvec(2),nptvec(3)))
    tx     = reshape(txyz(:,1),nptvec)
    ty     = reshape(txyz(:,2),nptvec)
    tz     = reshape(txyz(:,3),nptvec)
    txyz   = matmul(txyz,ltmp)
    npoint = size(txyz,1)
    if (lqn<0) then
        tlpl = 1
        allocate(ffgrid(npoint,1))
        ffgrid = 0.0D0
    else
        tlpl = 2*lqn+1
        allocate(ffgrid(npoint,tlpl))
        ffgrid = 0.0D0
    end if
    
    allocate(postmp(1,3))
    postmp = pos
    allocate(xtmp(npoint,3))
    call bsxfunMinus(txyz,postmp,xtmp)
    allocate(dist(npoint))
    dist(:) = xtmp(:,1)**2+xtmp(:,2)**2+xtmp(:,3)**2
    dist    = dist**0.5D0
    
    if (minval(dist)<=rmax) then
        ntmp = 0
        do ii = 1,npoint,1
            if (dist(ii)<=rmax) then
                ntmp = ntmp+1
            end if
        end do
        allocate(disttmp(ntmp))
        ntmp = 0
        do ii = 1,npoint,1
            if (dist(ii)<=rmax) then
                ntmp          = ntmp+1
                disttmp(ntmp) = dist(ii)
            end if
        end do
        allocate(frgrid(ntmp))
        method = "spline"
        call interp1(rrdata2,frdata2,disttmp,method,0,frgrid)
        
        if (lqn<0) then
            ntmp = 0
            do ii = 1,npoint,1
                if (dist(ii)<=rmax) then
                    ntmp = ntmp+1
                    ffgrid(ii,1) = ffgrid(ii,1)+frgrid(ntmp)
                end if
            end do
        else
            allocate(xtmp1(ntmp),xtmp2(ntmp),xtmp3(ntmp))
            ntmp = 0
            do ii = 1,npoint,1
                if (dist(ii)<=rmax) then
                    ntmp          = ntmp+1
                    xtmp1(ntmp) = xtmp(ii,1)
                    xtmp2(ntmp) = xtmp(ii,2)
                    xtmp3(ntmp) = xtmp(ii,3)
                end if
            end do
            
            call GenRealSH(xtmp1,xtmp2,xtmp3,lqn,1,Ylm)
            
            allocate(frgrix(size(frgrid),1),ffgridtmp(size(Ylm,1),size(Ylm,2)))
            frgrix(:,1) = frgrid(:)
            
            call bsxfunTimes(frgrix,Ylm,ffgridtmp)
            
            ntmp = 0
            do ii = 1,npoint,1
                if (dist(ii)<=rmax) then
                    ntmp          = ntmp+1
                    ffgrid(ii,:)  = ffgrid(ii,:)+ffgridtmp(ntmp,:)
                end if
            end do
            
        end if
    end if

    call getLocalIndex(tx,ty,tz,nvec,txtmp)
    ii = product(nvec)
    jj = size(ffgrid,2)
    allocate(vtmp(ii,jj))
    do ii = 1,size(ffgrid,2)
        allocate(ffgtmp(size(ffgrid,1)))
        ffgtmp(:) = ffgrid(:,ii)
        ntmp = size(txtmp,1)*size(txtmp,2)*size(txtmp,3)
        allocate(txtmptmp(ntmp))
        txtmptmp = int(reshape(txtmp,(/ntmp/)))
        ntmp = product(nvec)
        call accumarray1(txtmptmp,ffgtmp,ntmp,out3)
    end do
    
    return
    end subroutine InterpRdist2Cart_NoCell
    end module InterpRdist2Cart_NoCell_module
