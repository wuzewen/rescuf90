! smiZTRTRI.f90

!**************************************************
      !
      !
      !
!****************************************************

      module smiZTRTRIMOD
      contains
      subroutine smiZTRTRI(N,A,nprocs,MB,NB,nprow,npcol)

      implicit none

      integer :: N, MB, NB, nprow, npcol, nprocs
      complex*16, allocatable :: A()

      character(len=1) :: UPLO,diag
      integer :: iam, INFO, ictxt, myrow, mycol

      UPLO = 'U'
      diag = 'N'
      iam  =  0
      ictxt = 0
      myrow = 0
      mycol = 0

      call blacs_pinfo(iam,nprocs)
      call blacs_get(-1,0,ictxt)
      call blacs_gridinit(ictxt,'Row-major',nprow,npcol)
      call blacs_gridinfo(ictxt,nprow,npcol,myrow,mycol)

      izero = 0
      ione  = 1
      call numroc(N,MB,myrow,izero,nprow,A_np)
      A_np  =  
      call descinit(descA,N,N,MB,NB,izero,izero,ictxt,A_np,descinfo)
      call PZTRTRI(UPLO,diag,N,A,ione,ione,descA,INFO)
      call blacs_gridexit(ictxt)

      return
      end subroutine smiZPOTRF
      end module smiZPOTRFMOD
