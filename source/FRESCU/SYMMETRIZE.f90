! SYMMETRIZE.f90

!***********************************************************************************************************
      !
      !
      !
!***********************************************************************************************************

      module SYMMETRIZEMOD

      use symmetrize_1D_module
      use symmetrize_2D_module

      interface SYMMETRIZE
              module procedure symmetrize_1D
              module procedure symmetrize_2D
      end interface SYMMETRIZE

      end module SYMMETRIZEMOD
