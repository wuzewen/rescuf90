!  genGradient.f90 
!
!***********************************************************************
   !
   ! This function is used to generate Momentum Operator.
   ! 
   ! One of the following method may be used: FD or FFT.
   !
!***********************************************************************

    module genGradient_module
    contains
    subroutine genGradient(FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use fdGradMat_module
    use ftGradMat_module
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    character(len=20)               :: method
    integer                         :: cgridn(3), fgridn(3), acc, bc(3)
    
    ! temporery valuables

    ! output variables 

    ! Body of genGradient
    method = FRC%diffop%method
    acc    = FRC%diffop%accuracy
    bc     = FRC%domain%boundary
    cgridn = FRC%domain%cgridn
    fgridn = FRC%domain%fgridn
    allocate(FRC%diffop%Du(cgridn(1),cgridn(1)))
    allocate(FRC%diffop%Dv(cgridn(2),cgridn(2)))
    allocate(FRC%diffop%Dw(cgridn(3),cgridn(3)))
    allocate(FRC%diffop%fDu(fgridn(1),fgridn(1)))
    allocate(FRC%diffop%fDv(fgridn(2),fgridn(2)))
    allocate(FRC%diffop%fDw(fgridn(3),fgridn(3)))
    if (trim(method) == trim("fd")) then
        call fdGradMat(cgridn,acc,bc,FRC%diffop%Du,FRC%diffop%Dv,FRC%diffop%Dw)
        call fdGradMat(fgridn,acc,bc,FRC%diffop%fDu,FRC%diffop%fDv,FRC%diffop%fDw)
    else if (trim(method) == trim("fft")) then
        call ftGradMat(cgridn,FRC%diffop%Du,FRC%diffop%Dv,FRC%diffop%Dw)
        call ftGradMat(fgridn,FRC%diffop%fDu,FRC%diffop%fDv,FRC%diffop%fDw)
    end if
    
    return
    end subroutine genGradient
    end module genGradient_module
