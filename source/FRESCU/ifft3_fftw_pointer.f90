! ifft3_fftw.f90

      module ifft3_fftw_pointer_module
      contains
      !subroutine ifft3_fftw(FRC,A)
      subroutine ifft3_fftw_pointer(planfft,Arr,Brr)

      !use FORTRAN_RESCU_CALCULATION_TYPE
      use, intrinsic :: iso_c_binding
      !include 'fftw3.f03'

      type(C_PTR)         :: planfft
      complex*16, pointer :: Arr(:), Brr(:)

      !type(FORTRAN_RESCU_CALCULATION) :: FRC
      real*8  :: a
      integer :: n
      !type(C_PTR) :: plan
      !integer(C_INT)                              :: L, M, N
      !complex(C_DOUBLE_COMPLEX), dimension(L,M,N) :: Arr, Brr
      !complex*16, allocatable                     :: A(:,:,:)!, B(:,:,:)

      !FRC%Arr = A
      !call dfftw_plan_dft_3d(plan,L,M,N,Arr,Brr,FFTW_BACKWARD,FFTW_ESTIMATE)
      call dfftw_execute_dft(planfft,Arr,Brr)
      !call dfftw_destroy_plan(plan)
      !n = FRC%L*FRC%M*FRC%N
      !a = 1.0D0/dble(n)
      
      Brr = Brr/dble(size(Arr))
      !call zdscal(n,a,FRC%Arr,1)

      return
      end subroutine ifft3_fftw_pointer
      end module ifft3_fftw_pointer_module
