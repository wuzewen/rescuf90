! distmat_zeroslike_1D.f90
    
!****************************************
    !
    !
    !
!****************************************
    
    module distmat_zeroslike_1D_module
    contains
    subroutine distmat_zeroslike_1D(FRC,dA,n,dB)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use GetGlobalInd_1D_module
    use VataMN_1D_module
    !use VataMN_2D_module
    !use InitDistArray_1D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_1D)   :: dA
    integer           :: n(2)
    
    ! temporary variables
    integer              :: mpirank, ntmp
    integer, allocatable :: ia(:,:), ja(:,:)
    
    ! output variables
    type(VataMN_1D)      :: dB
    
    ! body of this function
    if (n(2) /= 1) then
        write(*,*) "Error in distmat_zeroslike_1D.f90. n(2) should equal to 1 if you call this function."
        write(*,*) "If you want to vata with more than one dimention, please change the function."
        stop
    end if
    
    mpirank = FRC%mpi%rank
    
    call InitDistArray_1D(n(1),n(2),dA%mblock,dA%nblock,dA%mproc,dA%nproc,dB)
    
    if (dB%mblock > n(1)) then
        dB%mblock = max(n(1),1)
    end if
    
    if (dB%nblock > n(2)) then
        dB%nblock = max(n(2),1)
    end if
    
    call GetGlobalInd_1D(mpirank,dB,ia,ja)
    
    !allocate(dB%vata(size(ia),size(ja),1))
    ntmp = size(dA%vata)
    allocate(dB%vata(ntmp))
    dB%vata = 0.0D0
    
    return
    end subroutine distmat_zeroslike_1D
    end module distmat_zeroslike_1D_module
