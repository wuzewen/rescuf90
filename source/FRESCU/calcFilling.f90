! calcFilling.f90
!*******************************************************
    !
!*******************************************************
    
    module calcFilling_module
    contains
    !subroutine calcFilling(Nvalence,elementZ,nspec,Aelement,fillFrac)
    subroutine calcFilling(FRC,fillFrac)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    !real, allocatable    :: Nvalence(:)
    !integer, allocatable :: elementZ(:)
    !integer              :: nspec
    !integer, allocatable :: Aelement(:)
    
    ! temporery variables
    integer              :: i, nAtom, Ztot, valTot, nspec
    integer, allocatable :: nz(:)
    
    ! output variables
    real*8  :: fillFrac
    
    ! body of this function
    valTot = FRC%eigensolver%Nvalence
    nspec  = FRC%Atom%numberOfElement
    natom  = FRC%Atom%numberOfAtom
    
    allocate(nz(nspec))
    !nz(:) = 0
    do i = 1,nspec,1
        nz(i) = FRC%element(i)%Z
    end do
    
    !nAe  = size(Aelement)
    Ztot = 0
    do i = 1,natom,1
        Ztot = Ztot+nz(FRC%atom%element(i))
    end do
    
    fillFrac = dble(valTot)/dble(Ztot)
    
    return
    end subroutine calcFilling
    end module calcFilling_module
