!  UpdateRHO.f90 
!****************************************************************************
!
!  PROGRAM: UpdateRHO
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module UpdateRHO_module
    contains
    subroutine UpdateRHO(FRC,nocc,rho)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use GetGlobalInd_2D_module
    use calcRho_module
    use modBCDist_2D_module
    use symmetrize_2D_module

    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable             :: nocc(:,:,:)
    
    ! temporery variables
    logical :: mpistat
    integer :: mpirank, mpisize, ispin, nspin, ngrid, nkpt, kk, ss
    type(VataMN_1D) :: rhotmp
    type(VataMN_2D) :: tmppsi, rhosym
    real*8 , allocatable :: tmpnocc(:), symrec(:,:,:)
    integer, allocatable :: ia(:,:), ja(:,:)
    
    ! output variables
    type(VataMN_2D) :: rho

    ! Body of UpdateRHO
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ispin   = FRC%spin%ispin
    nspin   = FRC%spin%nspin
    ngrid   = product(FRC%domain%fgridn)
    nkpt    = size(FRC%kpoint%ikdirect,1)
    
    if (FRC%mpi%paraK) then
        !allocate(RHO(ngrid,ispin))
        !do
        write(*,*) "Error in UpdateRHO.f90. FRC%mpi%paraK is not available now."
        stop
    else
        call InitDistArray_2D(ngrid,ispin,ngrid/mpisize,1,mpisize,1,rho)
        call GetGlobalInd_2D(mpirank,rho,ia,ja)
        allocate(rho%vata(size(ia),size(ja)))
        rho%vata = 0
        do kk = 1,nkpt,1
            do ss = 1,nspin,1
                if (ispin == 1 .or. ispin == 2) then
                    tmpnocc = nocc(:,kk,ss)
                    tmppsi  = FRC%psi(kk,ss)
                    call calcRho(FRC,tmpnocc,tmppsi,rhotmp)
                    rho%vata(:,ss) = rho%vata(:,ss)+rhotmp%vata
                else if (ispin == 4) then
                    write(*,*) "Error in UpdateRHO.f90. FRC%spin%ispin should be 1 now."
                    stop
                end if
            end do
        end do
    end if
    
    if (FRC%symmetry%pointsymmetry) then
        call ModBCDist_2D(FRC,rho,rho%m,rho%n,mpisize,1,.FALSE.,.FALSE.,rhosym)
        if (mpirank == 0) then
            symrec = real(FRC%symmetry%sym_rec)
            call symmetrize_2D(rhosym%vata,FRC%domain%fgridn,symrec,FRC%symmetry%sym_t,rhosym%vata)
        end if
        call ModBCDist_2D(FRC,rhosym,rho%mblock,1,mpisize,1,.FALSE.,.FALSE.,rho)
    end if
    
    return
    end subroutine UpdateRHO
    end module UpdateRHO_module

