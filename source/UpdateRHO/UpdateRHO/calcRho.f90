! calcRho.f90
    
!*************************************************************************
    !
    !
    !
!*************************************************************************
    
    module calcRho_module
    contains
    subroutine calcRho(FRC,nocc,evec,rho)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ModBCDist_2D_module
    use ModBCDist_1D_module
    use GetGlobalInd_2D_module
    use distmat_zeroslike_2D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    real*8, allocatable             :: nocc(:)
    type(VataMN_2D)                 :: evec
    
    ! temporary variables
    logical              :: mpistat, dg
    integer              :: mpirank, mpisize, ngrid, tmp(2), ii
    type(VataMN_2D)      :: psi, rho2
    integer, allocatable :: ia(:,:), ja(:,:)
    real*8 , allocatable :: nocct(:), rhotmp(:), rhoVata(:,:)
    
    ! output variables
    type(VataMN_1D)                 :: rho
    
    ! body of this function
    mpistat = FRC%mpi%status
    mpirank = FRC%mpi%rank
    mpisize = FRC%mpi%mpisize
    ngrid   = product(FRC%domain%fgridn)
    
    if (all(FRC%domain%fgridn == FRC%domain%cgridn)) then
        dg = .FALSE.
    else
        dg = .TRUE.
    end if
    
    !intFun = FRC%interpolation%IntFun
    
    if (dg .and. (.not. FRC%interpolation%interpRho)) then
        call ModBCDist_2D(FRC,evec,1,1,1,mpisize,.FALSE.,.FALSE.,psi)
        call GetGlobalInd_2D(mpirank,psi,ia,ja)
        allocate(nocct(size(ja)))
        forall(ii=1:size(ja))
            nocct(ii) = nocc(ja(ii,1))
        end forall
        
        allocate(rhoVata(ngrid,1))
        rhoVata   = 0
        do ii = 1,size(psi%vata,2),1
            !call intFun(psi%vata(:,ii))
            rhoVata(:,1) = rhoVata(:,1)+nocct(ii)*rhotmp*rhotmp
        end do
        
        call InitDistArray_1D(ngrid,1,ngrid,1,1,mpisize,rho)
    else
        tmp(1) = ngrid
        tmp(2) = 1
        call distmat_zeroslike_2D(FRC,evec,tmp,rho2)
        do ii = 1,size(nocct),1
            rhotmp   = evec%vata(:,ii)
            rho%vata = rho%vata+nocct(ii)*rhotmp*rhotmp
        end do
        if (dg) then
            call ModBCDist_1D(FRC,rho,1,1,1,mpisize,.FALSE.,.FALSE.,rho)
            !if (size(rho%vata,2) > 0) then
            !    rho%vata = FRC%interpolation%intFun(rho%vata)
            !end if
            rho%m = ngrid
        end if
    end if
    call ModBCDist_1D(FRC,rho,rho%m/mpisize,1,mpisize,1,.FALSE.,.FALSE.,rho)
    
    return
    end subroutine calcRho
    end module calcRho_module