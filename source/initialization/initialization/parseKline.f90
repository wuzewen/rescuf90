    
    module parsekline_module
    contains
    subroutine parsekline(sympoint0,sympoint1)
    
    implicit none
    
    ! input variables
    real*8, allocatable :: sympoint0(:,:)
    
    ! temporary variables
    integer :: nline, ii
    
    ! output variables
    real*8, allocatable :: sympoint1(:,:)
    
    ! body of this function
    nline = size(sympoint0,1)
    do ii = 1,nline,1
        line = sympoint0(ii)
        nsympt = size(line)
        do jj = 1,nsympt-1,1
            sympoint1(jj,1) = line(jj:jj+1)
        end do
    end do
    
    return
    end subroutine parsekline
    end module parsekline_module
    
    
