    module label2line_module
    contains
    subroutine label2line(crystalstructure, sympoint, nkpt, avec, kvec)
    
    implicit none
    
    ! input variables
    character(len=10)   :: crystalstructure
    real*8, allocatable :: sympoint(:,:)
    integer :: nkpt
    real*8 :: avec(3,3)
    
    ! temporary variables
    
    ! output variables
    real*8, allocatable :: kvec(:,:)
    
    ! body of this function
    call sym2kpt(crystalstructure,sympoint(1),avec,ksym1)
    call sym2kpt(crystalstructure,sympoint(2),avec,ksym2)
    call linspace(0,1,nkpt,w)
    call bsxfunTimes(ksym1,1-transpose(w),kvec1)
    call bsxfunTimes(ksym2,transpose(w),kvec)
    kvec = kvec+kvec1
    
    return
    end subroutine label2line
    end module label2line_module
    
    
