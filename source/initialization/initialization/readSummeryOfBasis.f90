! readSummeryOfBasis.f90
!***************************************
    !
!***************************************
    
    module readSummeryOfBasis_module
    contains
    subroutine readSummeryOfBasis(ElementName,SOB)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use SummeryOfBas_module
    
    implicit none
    
    ! input variables
    character(len=3) :: ElementName
    
    ! temporary variables
    integer            :: ii
    character(len=8)   :: FileName
    character(len=100) :: nonsense
    ! output variables
    !type :: SummeryOfBas
    !    integer  :: SizeOfVlocal,    SizeOfVlocalrrData,    SizeOfVlocaldrData,    SizeOfVlocalvvData
    !    integer  :: SizeOfRlocal,    SizeOfRlocalrrData,    SizeOfRlocaldrData,    SizeOfRlocalrhoData
    !    integer  :: SizeOfVnl,       SizeOfVnlrrData,       SizeOfVnldrData,       SizeOfVnlvvData,                SizeOfVnlqqData,                  SizeOfVnlfqData,       SizeOfVnlqwData
    !    integer  :: SizeOfVna,       SizeOfVnarrData,       SizeOfVnadrData,       SizeOfVnavvData
    !    integer  :: SizeOfRna,       SizeOfRnarrData,       SizeOfRnadrData,       SizeOfRnarhoData
    !    integer  :: SizeOfRelPseudoP,SizeOfRelPseudoPrrData,SizeOfRelPseudoPdrData,SizeOfRelPseudoPvvData_screened,SizeOfRelPseudoPvvData_unscreened
    !    integer  :: SizeOfOrbitalSet,SizeOfOrbitalSetrrData,SizeOfOrbitalSetdrData,SizeOfOrbitalSetfrData,         SizeOfOrbitalSetqqData,           SizeOfOrbitalSetfqData,SizeOfOrbitalSetqwData
    !end type SummeryBas
    
    type(SummeryOfBas) :: SOB
    
    ! body of this function
    fileName = trim(ElementName)//".bas"
    !fileName = 'Si.bas'
    open(unit=10,file=fileName,status='old',access='sequential',action='read')
    read(10,*) nonsense   ! 001
    
    !write(*,*) nonsense
    
    
    read(10,*) nonsense   ! 002
    read(10,*) nonsense   ! 003
    read(10,*) nonsense   ! 004
    read(10,*) nonsense   ! 005
    read(10,*) nonsense   ! 006
    read(10,*) nonsense   ! 007
    read(10,*) nonsense   ! 008
    
    ! Vlocal
    read(10,*) SOB%SizeOfVlocal   ! 009
    
    !write(*,*) SOB%SizeOfVlocal
    
    read(10,*) nonsense
    read(10,*) SOB%SizeOfVlocalrrData,    SOB%SizeOfVlocaldrData,    SOB%SizeOfVlocalvvData
    
    ! Rlocal
    read(10,*) nonsense
    read(10,*) SOB%SizeOfRlocal   ! 013
    read(10,*) nonsense
    read(10,*) SOB%SizeOfRlocalrrData,    SOB%SizeOfRlocaldrData,    SOB%SizeOfRlocalrhoData
    
    ! Vnl
    read(10,*) nonsense
    read(10,*) SOB%SizeOfVnl      ! 017
    read(10,*) nonsense
    read(10,*) SOB%SizeOfVnlrrData,       SOB%SizeOfVnldrData,       SOB%SizeOfVnlvvData,                SOB%SizeOfVnlqqData,                  SOB%SizeOfVnlfqData,       SOB%SizeOfVnlqwData
    
    ! Vna
    read(10,*) nonsense
    read(10,*) SOB%SizeOfVna      ! 021
    read(10,*) nonsense
    read(10,*) SOB%SizeOfVnarrData,       SOB%SizeOfVnadrData,       SOB%SizeOfVnavvData
    
    ! Rna
    read(10,*) nonsense
    read(10,*) SOB%SizeOfRna      ! 025
    read(10,*) nonsense
    read(10,*) SOB%SizeOfRnarrData,       SOB%SizeOfRnadrData,       SOB%SizeOfRnarhoData
    
    ! RelPseudoP
    read(10,*) nonsense
    read(10,*) SOB%SizeOfRelPseudoP  ! 029
    read(10,*) nonsense
    read(10,*) SOB%SizeOfRelPseudoPrrData,SOB%SizeOfRelPseudoPdrData,SOB%SizeOfRelPseudoPvvData_screened,SOB%SizeOfRelPseudoPvvData_unscreened
    
    ! OrbitalSet
    read(10,*) nonsense
    read(10,*) SOB%SizeOfOrbitalSet  ! 033
    read(10,*) nonsense
    allocate(SOB%orbitalsize(SOB%SizeOfOrbitalSet))
    do ii = 1,SOB%SizeOfOrbitalSet,1
        read(10,*) SOB%orbitalsize(ii)%SizeOfOrbitalSetrrData,SOB%orbitalsize(ii)%SizeOfOrbitalSetdrData,SOB%orbitalsize(ii)%SizeOfOrbitalSetfrData,         SOB%orbitalsize(ii)%SizeOfOrbitalSetqqData,           SOB%orbitalsize(ii)%SizeOfOrbitalSetfqData,SOB%orbitalsize(ii)%SizeOfOrbitalSetqwData
    end do
    
    close(10)
    
    return
    end subroutine readSummeryOfBasis
    end module readSummeryOfBasis_module
    