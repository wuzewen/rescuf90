! parseKBinfo.f90
    
!*************************************************************
    !
    !
    !
!*************************************************************
    
    
    module parseKBinfo_module
    contains
    subroutine parseKBinfo(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    real*8, allocatable        :: rnu(:)
    integer, allocatable       :: lnu(:)
    real*8, allocatable        :: enu(:)
    integer, allocatable       :: nnu(:)
    integer                    :: norb, jj, ii, nelem, nset
    
    ! out put variables

    ! body of this function
    nelem = FRC%Atom%numberOfElement
    do ii = 1,nelem,1
        norb = FRC%SOB(ii)%SizeOfVnl
        allocate(enu(norb),lnu(norb),nnu(norb),rnu(norb))
        lnu         = 0
        enu         = 0
        rnu         = 0
        nnu         = 0
        do jj = 1,norb,1
            enu(jj) = FRC%ElementData(ii)%Vnl(jj)%KBenergy
            lnu(jj) = FRC%ElementData(ii)%Vnl(jj)%L
            nnu(jj) = FRC%ElementData(ii)%Vnl(jj)%N
            !allocate(rnu(jj)%rnuD(FRC%SOB(ii)%SizeOfVnlrrData))
            rnu(jj) = maxval(FRC%ElementData(ii)%Vnl(jj)%rrData)
        end do
        allocate(FRC%element(ii)%vnllnu(norb))
        allocate(FRC%element(ii)%vnlenu(norb))
        allocate(FRC%element(ii)%vnlrnu(norb))
        allocate(FRC%element(ii)%vnlnnu(norb))
        FRC%element(ii)%vnllnu = lnu
        FRC%element(ii)%vnlenu = enu
        FRC%element(ii)%vnlrnu = rnu
        FRC%element(ii)%vnlnnu = nnu
    end do
    
    end subroutine parseKBinfo
    end module parseKBinfo_module