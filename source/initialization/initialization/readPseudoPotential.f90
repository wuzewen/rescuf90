! readPseudoPotential.f90
!***************************************
    !
!***************************************
    
    module readPseudoPotential_module
    contains
    subroutine readPseudoPotential(species,SOB,ElementData)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    !type :: SummeryOfBas
    !    integer  :: SizeOfVlocal,    SizeOfVlocalrrData,    SizeOfVlocaldrData,    SizeOfVlocalvvData
    !    integer  :: SizeOfRlocal,    SizeOfRlocalrrData,    SizeOfRlocaldrData,    SizeOfRlocalrhoData
    !    integer  :: SizeOfVnl,       SizeOfVnlrrData,       SizeOfVnldrData,       SizeOfVnlvvData,                SizeOfVnlqqData,                  SizeOfVnlfqData,       SizeOfVnlqwData
    !    integer  :: SizeOfVna,       SizeOfVnarrData,       SizeOfVnadrData,       SizeOfVnavvData
    !    integer  :: SizeOfRna,       SizeOfRnarrData,       SizeOfRnadrData,       SizeOfRnarhoData
    !    integer  :: SizeOfRelPseudoP,SizeOfRelPseudoPrrData,SizeOfRelPseudoPdrData,SizeOfRelPseudoPvvData_screened,SizeOfRelPseudoPvvData_unscreened
    !    integer  :: SizeOfOrbitalSet,SizeOfOrbitalSetrrData,SizeOfOrbitalSetdrData,SizeOfOrbitalSetfrData,         SizeOfOrbitalSetqqData,           SizeOfOrbitalSetfqData,SizeOfOrbitalSetqwData
    !end type SummeryBas
    type(SummeryOfBas) :: SOB
    
    ! temporary variables
    character(len=100) :: nonsense, filename
    character(len=4)   :: species
    integer            :: ii, jj
    
    ! output variables
    !type :: ElementType
    !    type :: RelPseudoPtype
    !        integer              :: N, L
    !        real*8               :: J, Rc
    !        real*8, allocatable  :: rrData(:), drData(:), vvData_screened(:), vvData_unscreened(:)
    !    end type RelPseudoPtype
    !    type(RelPseudoPtype), allocatable :: RelPseudoP(:)
    !    
    !    type :: VlocalType
    !        real*8               :: Rcore, Ncore
    !        real*8, allocatable  :: rrData(:), drData(:), vvData(:)
    !    end type VlocalType
    !    type(VlocalType)  :: Vlocal
    !    
    !    type :: RlocalType
    !        real*8, allocatable  :: rrData(:), drData(:), rhoData(:)
    !    end type RlocalType
    !    type(RlocalType)  :: Rlocal
    !    
    !    type :: OrbitalSetType
    !        real*8, allocatable :: rrData(:), drData(:), frData(:), qqData(:), fqData(:), qwData(:)
    !        integer             :: N, L
    !        real*8              :: E, Population, CoulombEnergyU, ExchangeEnergyJ
    !        character(len=20)   :: OrbitalType
    !    end type OrbitalSetType
    !    type(OrbitalSetType), allocatable :: OrbitalSet(:)
    !    
    !    type :: VnlType
    !        real*8, allocatable :: rrData(:), drData(:), vvData(:), qqData(:), fqData(:), qwData(:)
    !        integer :: L
    !        real*8  :: KBenergy, KBcosine, E
    !        logical :: isGhost
    !    end type VnlType
    !    type(VnlType), allocatable :: Vnl(:)
    !    
    !    type :: VnaType
    !        real*8, allocatable :: rrData(:), drData(:), vvData(:)
    !    end type VnaType
    !    type(VnaType) :: Vna
   ! 
    !    type :: RnaType
    !        real*8, allocatable :: rrData(:), drData(:), rhoData(:)
    !    end type RnaType
    !    type(RnaType) :: Rna
    !    
    !    type :: AtomType
    !        integer           :: Z, N
    !        character(len=4)  :: Symbol
    !        character(len=20) :: shell
    !        character(len=20) :: name
    !        real*8            :: mass
    !    end type AtomType
    !    type(atomType) :: Atom
    !    
    !end type ElementType
    
    type(ElementDataType) :: ElementData
    
    integer :: NumOfLine, Remainder
    
    ! body of this function
    
    
    filename = trim(species)//".bas"
    !fileName = 'Si.bas'
    open(unit=10,file=fileName,status='old',access='sequential',action='read')
    do ii = 1,37,1
        read(10,*) nonsense
    end do
    
    ! Vlocal
    read(10,*) nonsense  ! 037
    read(10,*) nonsense
    read(10,*) ElementData%Vlocal%Rcore, ElementData%Vlocal%Ncore
    
    read(10,*) nonsense     ! rrData
    NumOfLine = SOB%SizeOfVlocalrrData/10
    Remainder = SOB%SizeOfVlocalrrData-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) ElementData%Vlocal%rrData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Vlocal%rrData(NumOfLine*10+1:SOB%SizeOfVlocalrrData)
    end if
    
    read(10,*) nonsense     ! drData
    NumOfLine = SOB%SizeOfVlocaldrData/10
    Remainder = SOB%SizeOfVlocaldrData-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) ElementData%Vlocal%drData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Vlocal%drData(NumOfLine*10+1:SOB%SizeOfVlocaldrData)
    end if
    
    read(10,*) nonsense     ! vvData
    NumOfLine = SOB%SizeOfVlocalvvData/10
    Remainder = SOB%SizeOfVlocalvvData-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) ElementData%Vlocal%vvData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Vlocal%vvData(NumOfLine*10+1:SOB%SizeOfVlocalvvData)
    end if
    
    ! Rlocal
    read(10,*) nonsense
    
    read(10,*) nonsense  ! rrData
    NumOfLine = SOB%SizeOfRlocalrrData/10
    Remainder = SOB%SizeOfRlocalrrData-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) ElementData%Rlocal%rrData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Rlocal%rrData(NumOfLine*10+1:SOB%SizeOfRlocalrrData)
    end if
    
    read(10,*) nonsense  ! drData
    NumOfLine = SOB%SizeOfRlocaldrData/10
    Remainder = SOB%SizeOfRlocaldrData-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) ElementData%Rlocal%drData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Rlocal%drData(NumOfLine*10+1:SOB%SizeOfRlocaldrData)
    end if
    
    read(10,*) nonsense  ! rhoData
    NumOfLine = SOB%SizeOfRlocalrhoData/10
    Remainder = SOB%SizeOfRlocalrhoData-10*NumOfLine
    do ii = 1,NumOfLine,1   
        read(10,*) ElementData%Rlocal%rhoData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Rlocal%rhoData(NumOfLine*10+1:SOB%SizeOfRlocalrhoData)
    end if
    
    ! Vnl
    read(10,*) nonsense
    read(10,*) nonsense
    
    do jj = 1,SOB%SizeOfVnl
        read(10,*) nonsense
        
        !write(*,*) nonsense
        
        read(10,*) nonsense
        !read(10,*) nonsense
        !write(*,*) jj
        !write(*,*) nonsense
        !write(*,*) SOB%SizeOfVnl
        
        read(10,*) ElementData%Vnl(jj)%L,ElementData%Vnl(jj)%KBenergy,ElementData%Vnl(jj)%KBcosine,ElementData%Vnl(jj)%E,ElementData%Vnl(jj)%isGhost
        
        !write(*,*) ElementData%Vnl(jj)%L,ElementData%Vnl(jj)%KBenergy,ElementData%Vnl(jj)%KBcosine,ElementData%Vnl(jj)%E,ElementData%Vnl(jj)%isGhost
        
        read(10,*) nonsense
        
        !write(*,*) nonsense
        
        NumOfLine = SOB%SizeOfVnlrrData/10
        Remainder = SOB%SizeOfVnlrrData-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) ElementData%Vnl(jj)%rrData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%Vnl(jj)%rrData(NumOfLine*10+1:SOB%SizeOfVnlrrData)
        end if
        
        read(10,*) nonsense ! drData
        NumOfLine = SOB%SizeOfVnldrData/10
        Remainder = SOB%SizeOfVnldrData-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) ElementData%Vnl(jj)%drData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%Vnl(jj)%drData(NumOfLine*10+1:SOB%SizeOfVnldrData)
        end if
        
        read(10,*) nonsense ! vvData
        NumOfLine = SOB%SizeOfVnlvvData/10
        Remainder = SOB%SizeOfVnlvvData-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) ElementData%Vnl(jj)%vvData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%Vnl(jj)%vvData(NumOfLine*10+1:SOB%SizeOfVnlvvData)
        end if
        
        read(10,*) nonsense ! qqData
        NumOfLine = SOB%SizeOfVnlqqData/10
        Remainder = SOB%SizeOfVnlqqData-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) ElementData%Vnl(jj)%qqData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%Vnl(jj)%qqData(NumOfLine*10+1:SOB%SizeOfVnlqqData)
        end if
        
        read(10,*) nonsense ! fqData
        NumOfLine = SOB%SizeOfVnlfqData/10
        Remainder = SOB%SizeOfVnlfqData-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) ElementData%Vnl(jj)%fqData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%Vnl(jj)%fqData(NumOfLine*10+1:SOB%SizeOfVnlfqData)
        end if
        
        read(10,*) nonsense ! qqData
        NumOfLine = SOB%SizeOfVnlqwData/10
        Remainder = SOB%SizeOfVnlqwData-10*NumOfLine
        do ii = 1,NumOfLine,1   
            read(10,*) ElementData%Vnl(jj)%qwData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%Vnl(jj)%qwData(NumOfLine*10+1:SOB%SizeOfVnlqwData)
        end if
    end do
    
    !write(*,*) "end of Vnl"
    
    ! Vna
    read(10,*) nonsense
    
    read(10,*) nonsense  ! rrData
    NumOfLine = SOB%SizeOfVnarrData/10
    Remainder = SOB%SizeOfVnarrData-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) ElementData%Vna%rrData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Vna%rrData(NumOfLine*10+1:SOB%SizeOfVnarrData)
    end if
    
    read(10,*) nonsense  ! drData
    NumOfLine = SOB%SizeOfVnadrData/10
    Remainder = SOB%SizeOfVnadrData-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) ElementData%Vna%drData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Vna%drData(NumOfLine*10+1:SOB%SizeOfVnadrData)
    end if
    
    read(10,*) nonsense  ! vvData
    NumOfLine = SOB%SizeOfVnavvData/10
    Remainder = SOB%SizeOfVnavvData-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) ElementData%Vna%vvData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Vna%vvData(NumOfLine*10+1:SOB%SizeOfVnavvData)
    end if
    
    ! Rna
    read(10,*) nonsense
    
    read(10,*) nonsense  ! rrData
    NumOfLine = SOB%SizeOfRnarrData/10
    Remainder = SOB%SizeOfRnarrData-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) ElementData%Rna%rrData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Rna%rrData(NumOfLine*10+1:SOB%SizeOfRnarrData)
    end if
    
    read(10,*) nonsense  ! drData
    NumOfLine = SOB%SizeOfRnadrData/10
    Remainder = SOB%SizeOfRnadrData-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) ElementData%Rna%drData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Rna%drData(NumOfLine*10+1:SOB%SizeOfRnadrData)
    end if
    
    read(10,*) nonsense  ! rhoData
    NumOfLine = SOB%SizeOfRnarhoData/10
    Remainder = SOB%SizeOfRnarhoData-10*NumOfLine
    do ii = 1,NumOfLine,1
        read(10,*) ElementData%Rna%rhoData((ii-1)*10+1:ii*10)
    end do
    if (Remainder/=0) then
        read(10,*) ElementData%Rna%rhoData(NumOfLine*10+1:SOB%SizeOfRnarhoData)
    end if
    
    !write(*,*) ElementData%Rna%rhoData(NumOfLine*10+1:SOB%SizeOfRnarhoData)
    
    ! RelPseudoP
    read(10,*) nonsense
    read(10,*) nonsense
    
    do jj = 1,SOB%SizeOfRelPseudoP,1
        read(10,*) nonsense
        read(10,*) nonsense
        
        !write(*,*) jj
        !write(*,*) nonsense
        
        !read(10,*) nonsense
        
        !write(*,*) nonsense
        
        read(10,*) ElementData%RelPseudoP(jj)%N, ElementData%RelPseudoP(jj)%L, ElementData%RelPseudoP(jj)%J, ElementData%RelPseudoP(jj)%Rc
        
        read(10,*) nonsense  ! rrData
        
        !write(*,*) nonsense
        
        NumOfLine = SOB%SizeOfRelPseudoPrrData/10
        Remainder = SOB%SizeOfRelPseudoPrrData-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%RelPseudoP(jj)%rrData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%RelPseudoP(jj)%rrData(NumOfLine*10+1:SOB%SizeOfRelPseudoPrrData)
        end if
        
        read(10,*) nonsense  ! drData
        NumOfLine = SOB%SizeOfRelPseudoPdrData/10
        Remainder = SOB%SizeOfRelPseudoPdrData-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%RelPseudoP(jj)%drData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%RelPseudoP(jj)%drData(NumOfLine*10+1:SOB%SizeOfRelPseudoPdrData)
        end if
        
        read(10,*) nonsense  ! vvData_screened
        NumOfLine = SOB%SizeOfRelPseudoPvvData_screened/10
        Remainder = SOB%SizeOfRelPseudoPvvData_screened-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%RelPseudoP(jj)%vvData_screened((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%RelPseudoP(jj)%vvData_screened(NumOfLine*10+1:SOB%SizeOfRelPseudoPvvData_screened)
        end if
        
        read(10,*) nonsense  ! vvData_unscreened
        NumOfLine = SOB%SizeOfRelPseudoPvvData_unscreened/10
        Remainder = SOB%SizeOfRelPseudoPvvData_unscreened-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%RelPseudoP(jj)%vvData_unscreened((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%RelPseudoP(jj)%vvData_unscreened(NumOfLine*10+1:SOB%SizeOfRelPseudoPvvData_unscreened)
        end if
    end do
    
    ! OrbitalSet
    read(10,*) nonsense
    read(10,*) nonsense
    do jj = 1,SOB%SizeOfOrbitalSet,1
        read(10,*) nonsense
        read(10,*) nonsense
        read(10,*) ElementData%OrbitalSet(jj)%N,ElementData%OrbitalSet(jj)%L,ElementData%OrbitalSet(jj)%OrbitalType,ElementData%OrbitalSet(jj)%E,ElementData%OrbitalSet(jj)%population,ElementData%OrbitalSet(jj)%CoulombEnergyU,ElementData%OrbitalSet(jj)%ExchangeEnergyJ
        
        read(10,*) nonsense  ! rrData
        
        !write(*,*) jj
        !write(*,*) nonsense
        
        NumOfLine = SOB%orbitalsize(jj)%SizeOfOrbitalSetrrData/10
        Remainder = SOB%orbitalsize(jj)%SizeOfOrbitalSetrrData-10*NumOfLine
        !write(*,*) "start of data"
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%OrbitalSet(jj)%rrData((ii-1)*10+1:ii*10)
            !write(*,*) ElementData%OrbitalSet(jj)%rrData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%OrbitalSet(jj)%rrData(NumOfLine*10+1:SOB%orbitalsize(jj)%SizeOfOrbitalSetrrData)
        end if
        
        read(10,*) nonsense  ! drData
        NumOfLine = SOB%orbitalsize(jj)%SizeOfOrbitalSetdrData/10
        Remainder = SOB%orbitalsize(jj)%SizeOfOrbitalSetdrData-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%OrbitalSet(jj)%drData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%OrbitalSet(jj)%drData(NumOfLine*10+1:SOB%orbitalsize(jj)%SizeOfOrbitalSetdrData)
        end if
        
        read(10,*) nonsense  ! frData
        NumOfLine = SOB%orbitalsize(jj)%SizeOfOrbitalSetfrData/10
        Remainder = SOB%orbitalsize(jj)%SizeOfOrbitalSetfrData-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%OrbitalSet(jj)%frData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%OrbitalSet(jj)%frData(NumOfLine*10+1:SOB%orbitalsize(jj)%SizeOfOrbitalSetfrData)
        end if
        
        read(10,*) nonsense  ! qqData
        NumOfLine = SOB%orbitalsize(jj)%SizeOfOrbitalSetqqData/10
        Remainder = SOB%orbitalsize(jj)%SizeOfOrbitalSetqqData-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%OrbitalSet(jj)%qqData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%OrbitalSet(jj)%qqData(NumOfLine*10+1:SOB%orbitalsize(jj)%SizeOfOrbitalSetqqData)
        end if
        
        read(10,*) nonsense  ! fqData
        NumOfLine = SOB%orbitalsize(jj)%SizeOfOrbitalSetfqData/10
        Remainder = SOB%orbitalsize(jj)%SizeOfOrbitalSetfqData-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%OrbitalSet(jj)%fqData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%OrbitalSet(jj)%fqData(NumOfLine*10+1:SOB%orbitalsize(jj)%SizeOfOrbitalSetfqData)
        end if
        
        read(10,*) nonsense  ! qwData
        NumOfLine = SOB%orbitalsize(jj)%SizeOfOrbitalSetqwData/10
        Remainder = SOB%orbitalsize(jj)%SizeOfOrbitalSetqwData-10*NumOfLine
        do ii = 1,NumOfLine,1
            read(10,*) ElementData%OrbitalSet(jj)%qwData((ii-1)*10+1:ii*10)
        end do
        if (Remainder/=0) then
            read(10,*) ElementData%OrbitalSet(jj)%qwData(NumOfLine*10+1:SOB%orbitalsize(jj)%SizeOfOrbitalSetqwData)
        end if
    end do
    
    close(10)
    
    return
    end subroutine readPseudoPotential
    end module readPseudoPotential_module