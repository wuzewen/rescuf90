! initXC.f90
!**************************************
    !
!**************************************
    
    module initXC_module
    contains
    subroutine initXC(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    use parseFuncName_module
    use findFuncApprox_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    integer                        :: mpirank, ii, nn, jj, gridn(3), nqpt, nline, status
    real*8                         :: shift(3)
    real*8, allocatable            :: qdirect(:,:)
    logical                        :: libxcStatusIsFind, libxcListXIsFind, libxcListCIsFind, libxcisrelativisticIsFind, libxcouputIsFind
    logical                        :: XC_listIsFind, functionalHYBisFind, VFinputIsFind, RHOinIsFind, EXXqgridnIsFind, mGGA
    logical                        :: EXXusesmiIsFind, EXXGcutoffIsFind, EXXYprecisionIsFind, saveYGIsFind, isFind, MBJlimIsFind
    logical, allocatable           :: isMgga(:), isHYB(:)
    character(len=20)              :: datinput, aprx
    character(len=20), allocatable :: xclist(:), flist(:), faprx(:)
    type(kpointType)               :: kpoint
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%XC) then
        libxcStatusIsFind         = .FALSE.
        libxcListXIsFind          = .FALSE.
        libxcListCIsFind          = .FALSE.
        libxcisrelativisticIsFind = .FALSE.
        libxcouputIsFind          = .FALSE.
        XC_listIsFind             = .FALSE.
        functionalHYBisFind       = .FALSE.
        VFinputIsFind             = .FALSE.
        RHOinIsFind               = .FALSE.
        EXXqgridnIsFind           = .FALSE.
        EXXusesmiIsFind           = .FALSE.
        EXXGcutoffIsFind          = .FALSE.
        EXXYprecisionIsFind       = .FALSE.
        saveYGIsFind              = .FALSE.
        isFind                    = .FALSE.
        MBJlimIsFind              = .FALSE.
        mpirank                   =  FRC%mpi%rank
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("libxcStatus")) then
                libxcStatusIsFind = .TRUE.
                exit
            end if
        end do
        if (libxcStatusIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%functional%libxc = .TRUE.
            else
                FRC%functional%libxc = .FALSE.
            end if
        else
            FRC%functional%libxc = .FALSE.
        end if
        
        allocate(FRC%functional%list(2))
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("libxcListX")) then
                libxcListXIsFind = .TRUE.
                exit
            end if
        end do
        if (libxcListXIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%functional%list(1)
        else
            FRC%functional%list(1) = "none"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("libxcListC")) then
                libxcListCIsFind = .TRUE.
                exit
            end if
        end do
        if (libxcListCIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%functional%list(2)
        else
            FRC%functional%list(2) = "none"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("libxcisrelativistic")) then
                libxcisrelativisticIsFind = .TRUE.
                exit
            end if
        end do
        if (libxcisrelativisticIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%functional%isrelativistic = .TRUE.
            else
                FRC%functional%isrelativistic = .FALSE.
            end if
        else
            FRC%functional%isrelativistic = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("libxcouput")) then
                libxcouputIsFind = .TRUE.
                exit
            end if
        end do
        if (libxcouputIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%functional%ouput
        else
            FRC%functional%ouput = "EV"
        end if
        
        !ii = 0
        !do while(ii < nline)
        !    ii     = ii+1
        !    if (trim(inputFromFile%NAV(ii)%name) == trim("XC_list")) then
        !        XC_listIsFind = .TRUE.
        !        exit
        !    end if
        !end do
        !if (XC_listIsFind) then
        !    xclist = inputFromFile%NAV(ii)%value
        !    nn     = size(xclist)
        !    forall(ii=1:nn)
        !        FRC%functional%list(ii) = xclist(ii)
        !    end forall
        if (trim(FRC%functional%list(1)) == trim("none") .or. trim(FRC%functional%list(2)) == trim("none")) then
            datinput = FRC%ElementData(1)%XCfunctional
            call findFuncApprox(datinput,aprx)
            if (trim(aprx) == "LDA") then
                FRC%functional%list(1) = "XC_LDA_X"
                FRC%functional%list(2) = "XC_LDA_C_PW"
            else if (trim(aprx) == "PBE") then
                FRC%functional%list(1) = "XC_GGA_X_PBE"
                FRC%functional%list(2) = "XC_GGA_C_PBE"
            else
                FRC%functional%list(1) = "XC_LDA_X"
                FRC%functional%list(2) = "XC_LDA_C_PW"
            end if
        end if
        
        nn = size(FRC%functional%list)
        allocate(flist(nn))
        flist = FRC%functional%list
        call parseFuncName(flist,faprx)
        !call strcmp(faprx,"MGGA",isMgga)
        mGGA  = .FALSE.
        do ii = 1,size(faprx),1
            if (trim(faprx(ii)) == trim("mGGA")) then
                mGGA = .TRUE.
            end if
        end do
        
        if (any(isMgga) .and. trim(FRC%spin%spinType) == trim("non-collinear")) then
            write(*,*) "Error. Meta-GGA functionals are not supported in non-collinear spin calculations."
            stop
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("functionalHybrid")) then
                functionalHYBisFind = .TRUE.
                exit
            end if
        end do
        !if (.not. functionalHYBisFind) then
        !    flist = FRC%functional%list
        !    call parseFuncName(flist,faprx)
        !    call strcmp(faprx,"HYB",isHYB)
        !    if (count(isHYB == .TRUE.)>1) then
        !        nn = size(flist)
        !        allocate(ftmp(2*nn))
        !        do jj = 1,2*nn,1
        !            if (mod(jj,2) == 1) then
        !                ftmp(jj) = flist((jj+1)/2)
        !            else
        !                ftmp(jj) = "','"
        !            end if
        !        end do
        !        call cat(2,flist,flist)
        !        write(*,*) "Error. There connot be more than one hybrid functional."
        !    end if
        !    FRC%functional%hybrid = any(isHYB == .TRUE.)
        !else
        !    FRC%functional%hybrid = inputFromFile%NAV(ii)%value
        !end if
        
        !if (FRC%functional%hybrid) then
        !    if (trim(FRC%info%calculationType) == trim("band-structure")) then
        !        ii = 0
        !        do while(ii < nline)
        !            ii     = ii+1
        !            if (trim(inputFromFile%NAV(ii)%name) == trim("VFinput")) then
        !                VFinputIsFind = .TRUE.
        !                exit
        !            end if
        !        end do
        !        
        !        jj = 0
        !        do while(jj < nline)
        !            jj     = jj+1
        !            if (trim(inputFromFile(jj)%name) == trim("RHOin")) then
        !                RHOinIsFind = .TRUE.
        !                exit
        !            end if
        !        end do
        !        
        !        if (VFinputIsFind) then
        !            FRC%EXX%VFinput  = inputFromFile%NAV(ii)%value
        !        else if ((.not. VFinputIsFind) .and. RHOinIsFind) then
        !            FRC%rho(1)%input = inputFromFile(jj)%value
        !            FRC%EXX%VFinput  = inputFromFile(jj)%value
        !        else
        !            write(*,*) "error"
        !        end if
        !    end if
        !    
        !    if (trim(FRC%info%calculationType) == trim("self-consistent")) then
        !        FRC%symmetry%spaceSymmetry = .FALSE.
        !        FRC%symmetry%pointSymmetry = .FALSE.
        !        FRC%symmetry%timeReversal  = .FALSE.
        !        FRC%init%symmetry          = .FALSE.
        !        FRC%init%kpoint            = .FALSE.
        !        call initSymmetry(nline,inputFromFile,FRC)
        !        call initKpoint(nline,inputFromFile,FRC)
        !        
        !        kpoint = FRC%kpoint
        !        
        !        ii = 0
        !        do while(ii < nline)
        !            ii     = ii+1
        !            if (trim(inputFromFile%NAV(ii)%name) == trim("EXXqgridn")) then
        !                EXXqgridnIsFind = .TRUE.
        !                exit
        !            end if
        !        end do
        !        if (EXXqgridnIsFind) then
        !            FRC%EXX%qgridn     = inputFromFile%NAV(ii)%value
        !        else
        !            FRC%EXX%qgridn     = kpoint%gridn
        !        end if
        !        
        !        ii = 0
        !        do while(ii < nline)
        !            ii     = ii+1
        !            if (trim(inputFromFile%NAV(ii)%name) == trim("EXXusesmi")) then
        !                EXXusesmiIsFind = .TRUE.
        !                exit
        !            end if
        !        end do
        !        if (EXXusesmiIsFind) then
        !            FRC%EXX%usesmi = inputFromFile%NAV(ii)%value
        !        else
        !            FRC%EXX%usesmi = .FALSE.
        !        end if
        !        
        !        ii = 0
        !        do while(ii < nline)
        !            ii     = ii+1
        !            if (trim(inputFromFile%NAV(ii)%name) == trim("EXXGcutoff")) then
        !                EXXGcutoffIsFind = .TRUE.
        !                exit
        !            end if
        !        end do
        !        if (EXXGcutoffIsFind) then
        !            FRC%EXX%Gcutoff = inputFromFile%NAV(ii)%value
        !        else
        !            FRC%EXX%Gcutoff = 5
        !        end if
        !        
        !        ii = 0
        !        do while(ii < nline)
        !            ii     = ii+1
        !            if (trim(inputFromFile%NAV(ii)%name) == trim("EXXYprecision")) then
        !                EXXYprecisionIsFind = .TRUE.
        !                exit
        !            end if
        !        end do
        !        if (EXXYprecisionIsFind) then
        !            FRC%EXX%Yprecision = inputFromFile%NAV(ii)%value
        !        else
        !            FRC%EXX%Yprecision = "double"
        !        end if
        !        
        !        gridn = FRC%EXX%qgridn
        !        nqpt  = product(gridn)
        !        shift = kpoint%shift
        !        call mpmesh(gridn,shift,qdirect)
        !        FRC%EXX%qdirect    = qdirect
        !        FRC%EXX%qcartesian = matmul(qdirect,FRC%domain%recvec)
        !        FRC%EXX%qweight(:) = 1/nqpt
        !        
        !        if (any(mod(kpoint%gridn,FRC%EXX%qgridn)) .and. mpirank == 0) then
        !            write(*,*) "warning"
        !            FRC%EXX%qgridn = kpoint%gridn
        !        end if
        !        
        !        if (FRC%EXX%usesmi .and. product(kpoint%gridn)/=1 .and. mpirank == 0) then
        !            write(*,*) "warning"
        !            FRC%EXX%usesmi = .FALSE.
        !        end if
                
        !        ii = 0
        !        do while(ii < nline)
        !            ii     = ii+1
        !            if (trim(inputFromFile%NAV(ii)%name) == trim("saveYG")) then
        !                saveYGIsFind = .TRUE.
        !                exit
        !            end if
        !        end do
        !        if (saveYGIsFind) then
        !            FRC%functional%saveYG = inputFromFile%NAV(ii)%value
        !        else
        !            FRC%functional%saveYG = .FALSE.
        !        end if
        !        
        !        if (FRC%functional%hybrid .and. (.not. saveYGIsFind)) then
        !            FRC%functional%saveYG = .FALSE.
        !        end if
        !    end if
        !end if
        
        !nn = size(FRC%functional%list,1)
        !do ii = 1,nn,1
        !    if (trim(FRC%functional%list(ii)) == trim("XC_MGGA_X_TB09")) then
        !        isFInd = .TRUE.
        !        exit
        !    end if
        !end do
        
        !ii = 0
        !do while(ii < nline)
        !    ii     = ii+1
        !    if (trim(inputFromFile%NAV(ii)%name) == trim("MBJlim")) then
        !        MBJlimIsFind = .TRUE.
        !        exit
        !    end if
        !end do
        !if (MBJlimIsFind) then
        !    FRC%functional%MBJlim = inputFromFile%NAV(ii)%value
        !else if (.not. isFind) then
        !    FRC%functional%MBJlim = 0
        !else
        !    FRC%functional%MBJlim = inputFromFile%NAV(ii)%value
        !end if
        
        FRC%init%XC = .TRUE.
    end if
    
    return
    end subroutine initXC
    end module initXC_module
    
    
