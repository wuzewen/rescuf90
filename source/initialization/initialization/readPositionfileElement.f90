! readPositionfileElement.f90
!********************************
    !
!********************************
    
    module readPositionfileElement_module
    contains
    subroutine readPositionfileElement()
    
    implicit none
    
    ! input variables
    character(len=20) :: positionfile
    
    ! temporary variables
    character(len=20) :: nonsense
    real*8            :: const
    
    ! output variables
    type :: AtomSymbolType
        character(len=4) :: species
        integer          :: NumberOfAtoms
    end type AtomSymbolType
    
    type(AtomSymbolType),allocatable :: AtomSymbol(:)
    
    ! body of this function
    open(unit=10,file=positionfile,status='old',access='sequential',action='read')
    do ii = 1,5,1
        read(10,*) nonsense
    end do
    
    read(10,*) AtomSymbol(:)%species
    read(10,*) AtomSymbol(:)%NumberOfAtoms
    close(10)
    
    return
    end subroutine readPositionfileElement
    end module readPositionfileElement_module
    
    
    