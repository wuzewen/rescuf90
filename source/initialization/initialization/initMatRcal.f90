!  initMatRcal.f90 
!
!  FUNCTIONS:
!  initMatRcal - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: initMatRcal
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module initMatRcal_module
    contains
    subroutine initMatRcal(inputFromFile,FRC)

    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    use initMPI_module
    use initInfo_module
    !use AddPaths_module
    use InitOption_module
    !use repxyz_module
    use initElement_module
    use GetAtomInfo_module
    use initLCAO_module
    use GetDomainInfo_module
    use initSpin_module
    use initSymmetry_module
    use initKpoint_module
    use initTempSmear_module
    use initDOS_module
    use initEigensolver_module
    use initPRR_module
    use initXC_module
    use initPotential_module
    use initInterPolation_module
    use initDensity_module
    use initEnergy_module
    use initSMI_module
    use initGPU_module
    use initMixer_module
    use initSR_module
    use initForce_module
    use initDFPT_module
    
    implicit none

    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    
    ! output variables

    ! Body of initMatRcal
    call initMPI(inputFromFile,FRC)
    call initInfo(inputFromFile,FRC)
    !call AddPaths(nline,inputFromFile,FRC)
    call InitOption(inputFromFile,FRC)
    !call repxyz(FRC)
    call initElement(inputFromFile,FRC)
    call GetAtomInfo(inputFromFile,FRC)
    call initLCAO(inputFromFile,FRC)
    call GetDomainInfo(inputFromFile,FRC)
    call initSpin(inputFromFile,FRC)
    call initSymmetry(inputFromFile,FRC)
    call initKpoint(inputFromFile,FRC)
    call initTempSmear(inputFromFile,FRC)
    call initDOS(inputFromFile,FRC)
    call initEigensolver(inputFromFile,FRC)
    call initPRR(inputFromFile,FRC)
    call initXC(inputFromFile,FRC)
    call initPotential(inputFromFile,FRC)
    call initInterPolation(inputFromFile,FRC)
    call initDensity(inputFromFile,FRC)
    call initEnergy(inputFromFile,FRC)
    call initSMI(inputFromFile,FRC)
    call initGPU(inputFromFile,FRC)
    call initMixer(inputFromFile,FRC)
    call initSR(inputFromFile,FRC)
    call initForce(inputFromFile,FRC)
    call initDFPT(inputFromFile,FRC)
    
    !call rng(FRC%mpi%rank)
    
    !if (FRC%diffop%method == "FFT") then
    !    call fftw("planner","exhaustive")
    !    call fftw("dwisdom","none")
    !    gridn = FRC%domain%cgridn
    !    call initfftw(gridn)
    !    gridn = FRC%domain%fgridn
    !    call initfftw(gridn)
    !    call fftw("dwisdom",fftinfo)
    !    call fftw("dwisdom",fftinfo)
    !end if
    
    !if (FRC%option%plorder) then
    !    call plorder(FRC)
    !end if

    return
    end subroutine initMatRcal
    end module initMatRcal_module

