! initLCAO.f90
!***********************************
    !
!***********************************
    
    module initLCAO_module
    contains
    subroutine initLCAO(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    !input variables
    integer                         :: nline, status
    real*8                          :: statusReal
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    integer :: ii
    logical :: LCAOstatusIsFind, LCAOCFSIIsFind, LCAOsprsIsFind, LCAOmullikenIsFind
    
    ! output variables
    
    ! body of this function
    nline = inputFromFile%NumberofInput
    
    if (.not. FRC%init%LCAO) then
        LCAOstatusIsFind   = .FALSE.
        LCAOCFSIIsFind     = .FALSE.
        LCAOsprsIsFind     = .FALSE.
        LCAOmullikenIsFind = .FALSE.
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("LCAOstatus")) then
                LCAOstatusIsFind = .TRUE.
                exit
            end if
        end do
        if (LCAOstatusIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%LCAO%status = .TRUE.
            else
                FRC%LCAO%status = .FALSE.
            end if
        else
            FRC%LCAO%status = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("LCAOCFSI")) then
                LCAOCFSIIsFind = .TRUE.
                exit
            end if
        end do
        if (LCAOCFSIIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%LCAO%CFSI = .TRUE.
            else
                FRC%LCAO%CFSI = .FALSE.
            end if
        else
            FRC%LCAO%CFSI = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("LCAOsprs")) then
                LCAOsprsIsFind = .TRUE.
                exit
            end if
        end do
        if (LCAOsprsIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") statusReal
            FRC%LCAO%sprs = statusReal
        else
            FRC%LCAO%sprs = 0.15
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("LCAOmulliken")) then
                LCAOmullikenIsFind = .TRUE.
                exit
            end if
        end do
        if (LCAOsprsIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%LCAO%mulliken = .TRUE.
            else
                FRC%LCAO%mulliken = .FALSE.
            end if
        else
            FRC%LCAO%mulliken = .FALSE.
        end if
        
        FRC%init%LCAO = .TRUE.
    end if
    
    return
    end subroutine initLCAO
    end module initLCAO_module