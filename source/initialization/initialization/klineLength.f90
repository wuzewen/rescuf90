    module klinelength_module
    contains
    subroutine klinelength(crystalstructure,sympoint,avec,klen)
    
    implicit none
    
    ! input variables
    character :: crystalstructure
    real*8, allocatable :: sympoint(:,:), avec(:,:)
    
    ! temporary variables
    
    ! output variables
    real*8, allocatable :: klen(:,:)
    
    ! body of this function
    call inversion(avec,bvec)
    bvec = 2*pi*transpose(bvec)
    nline = size(sympoint,1)
    klen(:) = 0
    do ii = 1,nline,1
        line = sympoint(ii)
        call norm(matmul((line(2)-line(1)),bvec),klen(ii))
    end do
    
    return
    end subroutine klinelength
    end module klinelength_module
    
    