! rmSymNonPeriodic.f90
    
!*************************************************
    !
    !
    !
!*************************************************
    
    module rmSymNonPeriodic_module
    contains
    subroutine rmSymNonPeriodic(bc,symd,symt,idel)
    
    implicit none
    
    ! input variables
    integer              :: bc(3)
    integer, allocatable :: symd(:,:,:)
    real*8,  allocatable :: symt(:,:)
    
    ! temporary variables
    integer :: ii, jj, kk, ntmp, mtmp, nx, ny, nz, maskbcIn(3,3)
    logical :: bctmp(3), maskbc(3,3)
    integer, allocatable :: tmpIn(:,:,:)
    logical, allocatable :: ideltmp(:), tmp(:)
    
    ! output variables
    integer, allocatable :: idel(:)
    
    ! body of this function
    ntmp  = size(symt,1)
    bctmp = .FALSE.
    do ii = 1,3,1
        if (bc(ii) == 0) then
            bctmp(ii) = .TRUE.
        end if
    end do
    
    !allocate(ideltmp(ntmp))
    !mtmp = count(bctmp == .TRUE.)
    !allocate(symtmp(ntmp,mtnp))
    !mtmp = 0
    !do
    
    allocate(ideltmp(ntmp))
    !allocate(idel(ntmp))
    do ii = 1,ntmp,1
        ideltmp(ii) = .FALSE.
        do jj = 1,3,1
            if (bctmp(jj) .and. symt(ii,jj) /= 0) then
                ideltmp(ii) = .TRUE.
            end if
        end do
    end do
    
    maskbc = .FALSE.
    do ii = 1,3,1
        do jj = 1,3,1
            if (bctmp(ii) .or. bctmp(jj)) then
                maskbc(ii,jj) = .TRUE.
            end if
        end do
    end do
    
    maskbc(1,1) = .FALSE.
    maskbc(2,2) = .FALSE.
    maskbc(3,3) = .FALSE.
    
    !allocate(tmp(nz))
    
    nx  = size(symd,1)
    ny  = size(symd,2)
    nz  = size(symd,3)
    allocate(tmp(nz))
    tmp = .FALSE.
    do ii = 1,nx,1
        do jj = 1,ny,1
            do kk = 1,nz,1
                if ((symd(ii,jj,kk) /= 0) .and. maskbc(ii,jj)) then
                    tmp(kk) = .TRUE.
                end if
            end do
        end do
    end do
    
    do ii = 1,ntmp,1
        if (ideltmp(ii) .or. tmp(ii)) then
            ideltmp(ii) = .TRUE.
        else
            ideltmp(ii) = .FALSE.
        end if
    end do
    
    maskbcIn = 0
    do ii = 1,3,1
        do jj = 1,3,1
            if (bctmp(ii) .and. bctmp(jj)) then
                maskbcIn(ii,jj) = 1
            end if
        end do
    end do
    
    allocate(tmpIn(nx,ny,nz))
    do ii = 1,nx,1
        do jj = 1,ny,1
            do kk = 1,nz,1
                tmpIn(ii,jj,kk) = symd(ii,jj,kk)*maskbc(ii,jj)
            end do
        end do
    end do
    tmp = .FALSE.
    do kk = 1,nz,1
        if (any(tmpIn(:,:,kk) == -1)) then
            tmp(kk) = .TRUE.
        end if
    end do
    
    do ii = 1,ntmp,1
        if (ideltmp(ii) .or. tmp(ii)) then
            ideltmp(ii) = .TRUE.
        else
            ideltmp(ii) = .FALSE.
        end if
    end do
    
    ntmp = count(ideltmp == .TRUE.)
    allocate(idel(ntmp))
    ntmp = 0
    do kk = 1,nz,1
        if (ideltmp(kk)) then
            ntmp       = ntmp+1
            idel(ntmp) = kk
        end if
    end do
    
    return
    end subroutine rmSymNonPeriodic
    end module rmSymNonPeriodic_module
            