! readinputfile.f90
    
!*****************************************************************************************
    !
    ! This subroutine reads all the input in Control.input file.
    !
!*****************************************************************************************
    
    module readinputfile_module
    contains
    subroutine readinputfile(inputFromFile)
    
    use inputtype
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    character(len=100)      :: nonsense
    integer                 :: ii
    integer                 :: nline, nvalue, val
    
    ! output variables
    type(inputFromFileType) :: inputFromFile
    
    ! body of this function
    open(unit=10,file='control.input',status='old',access='sequential',action='read')
    val    = 0
    nline  = 0
    nvalue = 0
    do while(val==0)
        read(10,fmt="(A79)",iostat=val) nonsense
        if (val==0) then
            nline = nline+1
            if (len_trim(nonsense)/=0) then
                nvalue = nvalue+1
            end if
        else if (val>0) then
            write(*,*) "Wrong Input File"
        else
            exit
        end if
    end do
    close(10)
    
    inputFromFile%NumberofInput = nline
    allocate(inputFromFile%NAV(nvalue))
    
    open(unit=10,file='control.input',status='old',access='sequential',action='read')
    do ii = 1,nline,1
        read(10,*) inputFromFile%NAV(ii)%name,nonsense,inputFromFile%NAV(ii)%value
    end do
    close(10)
    
    return
    end subroutine readinputfile
    end module readinputfile_module