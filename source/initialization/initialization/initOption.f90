! initOption.f90
!******************************************
    !
!******************************************
    
    module initOption_module
    contains
    subroutine initOption(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputtype
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    integer :: ii, nline, status
    logical :: bufferSizeIsFind, centerMoleculeIsFind, initParaRealIsFind, maxSCFiterationIsFind
    logical :: plorderIsFind, replicateCellIsFind, shortRangeIsFind, saveDensityIsFind
    logical :: saveDFPTIsFind, saveEigensolverIsFind, saveInterpolationIsFind, saveLCAOIsFind
    logical :: saveMiscIsFind, saveMixingIsFind, savePotentialIsFind, saveTauIsFind, saveWaveFunctionIsFind
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%option) then
        FRC%scloop = 1
        nline = inputFromFile%NumberOfInput
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("bufferSize")) then
                bufferSizeIsFind = .TRUE.
                exit
            end if
        end do
        if (bufferSizeIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            FRC%option%bufferSize = status
        else
            FRC%option%bufferSize = 2**27
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("centerMolecule")) then
                centerMoleculeIsFind = .TRUE.
                exit
            end if
        end do
        if (centerMoleculeIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%centerMolecule = .TRUE.
            else
                FRC%option%centerMolecule = .FALSE.
            end if
        else
            FRC%option%centerMolecule = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("initParaReal")) then
                initParaRealIsFind = .TRUE.
                exit
            end if
        end do
        if (initParaRealIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%option%initParaReal = .FALSE.
            else
                FRC%option%initParaReal = .TRUE.
            end if
        else
            FRC%option%initParaReal     = .TRUE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("maxSCFiteration")) then
                maxSCFiterationIsFind = .TRUE.
                exit
            end if
        end do
        if (maxSCFiterationIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            FRC%option%maxSCFiteration = status
        else
            FRC%option%maxSCFiteration = 100
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("plorder")) then
                plorderIsFind = .TRUE.
                exit
            end if
        end do
        if (plorderIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%plorder = .TRUE.
            else
                FRC%option%plorder = .FALSE.
            end if
        else
            FRC%option%plorder = .FALSE.
        end if
    
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("replicateCell")) then
                replicateCellIsFind = .TRUE.
                exit
            end if
        end do
        if (replicateCellIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            FRC%option%replicateCell = status
        else
            FRC%option%replicateCell = 1
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("shortRange")) then
                shortRangeIsFind = .TRUE.
                exit
            end if
        end do
        if (shortRangeIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%option%shortRange = .FALSE.
            else
                FRC%option%shortRange = .TRUE.
            end if
        else
            FRC%option%shortRange = .TRUE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("saveDensity")) then
                saveDensityIsFind = .TRUE.
                exit
            end if
        end do
        if (saveDensityIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%option%saveDensity = .FALSE.
            else
                FRC%option%saveDensity = .TRUE.
            end if
        else
            FRC%option%saveDensity = .TRUE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("saveDFPT")) then
                saveDFPTIsFind = .TRUE.
                exit
            end if
        end do
        if (saveDFPTIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%saveDFPT = .TRUE.
            else
                FRC%option%saveDFPT = .FALSE.
            end if
        else
            FRC%option%saveDFPT = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("saveEigensolver")) then
                saveEigensolverIsFind = .TRUE.
                exit
            end if
        end do
        if (saveEigensolverIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%saveEigensolver = .TRUE.
            else
                FRC%option%saveEigensolver = .FALSE.
            end if
        else
            FRC%option%saveEigensolver = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("saveInterpolation")) then
                saveInterpolationIsFind = .TRUE.
                exit
            end if
        end do
        if (saveInterpolationIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%saveInterpolation = .TRUE.
            else
                FRC%option%saveInterpolation = .FALSE.
            end if
        else
            FRC%option%saveInterpolation = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("saveLCAO")) then
                saveLCAOIsFind = .TRUE.
                exit
            end if
        end do
        if (saveLCAOIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%saveLCAO = .TRUE.
            else
                FRC%option%saveLCAO = .FALSE.
            end if
        else
            FRC%option%saveLCAO = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("saveMisc")) then
                saveMiscIsFind = .TRUE.
                exit
            end if
        end do
        if (saveMiscIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%saveMisc = .TRUE.
            else
                FRC%option%saveMisc = .FALSE.
            end if
        else
            FRC%option%saveMisc = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("saveMixing")) then
                saveMixingIsFind = .TRUE.
                exit
            end if
        end do
        if (saveMixingIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%saveMixing = .TRUE.
            else
                FRC%option%saveMixing = .FALSE.
            end if
        else
            FRC%option%saveMixing = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("savePotential")) then
                savePotentialIsFind = .TRUE.
                exit
            end if
        end do
        if (savePotentialIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%savePotential = .TRUE.
            else
                FRC%option%savePotential = .FALSE.
            end if
        else
            FRC%option%savePotential = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("saveTau")) then
                saveTauIsFind = .TRUE.
                exit
            end if
        end do
        if (saveTauIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%saveTau = .TRUE.
            else
                FRC%option%saveTau = .FALSE.
            end if
        else
            FRC%option%saveTau = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("saveWaveFunction")) then
                saveWaveFunctionIsFind = .TRUE.
                exit
            end if
        end do
        if (saveWaveFunctionIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%option%saveWaveFunction = .TRUE.
            else
                FRC%option%saveWaveFunction = .FALSE.
            end if
        else
            FRC%option%saveWaveFunction = .FALSE.
        end if
        FRC%init%option = .TRUE.
    end if
    
    return
    end subroutine initOption
    end module initOption_module