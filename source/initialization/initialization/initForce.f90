! initForce.f90
!************************
    !
!************************
    
    module initForce_module
    contains
    subroutine initForce(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType)              :: inputFromFile
    
    ! temporary variables
    logical                              :: forcedef, ForceStatusIsFind, ForceSamplingIsFind, ForceLebedevIsFind
    logical                              :: ForceGridnXIsFind, ForceGridnYIsFind, ForceGridnZIsFind, ForceResolutionIsFind
    integer                              :: ii, jj, nline, status,nrman(3), ntmp
    real*8                               :: nrma(3), avec(3,3), tmp
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%force) then
        
        nline                 =  inputFromFile%numberOfinput
        ForceStatusIsFind     = .FALSE. 
        ForceSamplingIsFind   = .FALSE. 
        ForceLebedevIsFind    = .FALSE.
        ForceGridnXIsFind     = .FALSE. 
        ForceGridnYIsFind     = .FALSE. 
        ForceGridnZIsFind     = .FALSE. 
        ForceResolutionIsFind = .FALSE.
        
        if (trim(FRC%info%calculationType) == trim("DOS") .or. trim(FRC%info%calculationType) == trim("band-structure")) then
            forcedef = .FALSE.
        else
            forcedef = .TRUE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("ForceStatus")) then
                ForceStatusIsFind = .TRUE.
                exit
            end if
        end do
        if (ForceStatusIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%force%status = .TRUE.
            else
                FRC%force%status = .FALSE.
            end if
        else
            FRC%force%status = forcedef
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("ForceSamplingX")) then
                ForceSamplingIsFind = .TRUE.
                exit
            end if
        end do
        if (ForceSamplingIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%force%Sampling(1)
        else
            FRC%force%Sampling(1) = 1
        end if
        
        ForceSamplingIsFind = .FALSE.
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("ForceSamplingY")) then
                ForceSamplingIsFind = .TRUE.
                exit
            end if
        end do
        if (ForceSamplingIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%force%Sampling(2)
        else
            FRC%force%Sampling(2) = 1
        end if
        
        ForceSamplingIsFind = .FALSE.
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("ForceSamplingZ")) then
                ForceSamplingIsFind = .TRUE.
                exit
            end if
        end do
        if (ForceSamplingIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%force%Sampling(3)
        else
            FRC%force%Sampling(3) = 1
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("ForceLebedev")) then
                ForceLebedevIsFind = .TRUE.
                exit
            end if
        end do
        if (ForceLebedevIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%force%Lebedev
        else
            FRC%force%Lebedev = 770
        end if
        
        if (FRC%LCAO%status .or. FRC%spin%ispin == 4) then
            FRC%force%status = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("ForceGridnX")) then
                ForceGridnXIsFind = .TRUE.
                exit
            end if
        end do
        if (ForceGridnXIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%force%Gridn(1)
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("ForceGridnY")) then
                ForceGridnYIsFind = .TRUE.
                exit
            end if
        end do
        if (ForceGridnYIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%force%Gridn(2)
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("ForceGridnZ")) then
                ForceGridnZIsFind = .TRUE.
                exit
            end if
        end do
        if (ForceGridnZIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%force%Gridn(3)
        end if
        
        if (.not. (ForceGridnZIsFind .and. ForceGridnZIsFind .and. ForceGridnZIsFind)) then
            jj = 0
            do while(jj < nline)
                jj     = jj+1
                if (trim(inputFromFile%NAV(jj)%name) == trim("ForceResolution")) then
                    ForceResolutionIsFind = .TRUE.
                    exit
                end if
            end do
         
            if (ForceResolutionIsFind) then
                read(inputFromFile%NAV(jj)%value,"(i2)") FRC%force%Resolution
                avec            = FRC%domain%latvec
                avec            = avec*avec
                nrma(1)         = sqrt(sum(avec(1,:)))
                nrma(2)         = sqrt(sum(avec(2,:)))
                nrma(3)         = sqrt(sum(avec(3,:)))
                nrman(1)        = ceiling(nrma(1)/FRC%force%Resolution)
                nrman(2)        = ceiling(nrma(2)/FRC%force%Resolution)
                nrman(3)        = ceiling(nrma(3)/FRC%force%Resolution)
                forall(ii=1:3)
                    FRC%force%Gridn(ii) = nrman(ii) + mod(nrman(ii)+1,2)
                end forall
            else
                FRC%force%Resolution = 1
                do ii = 1,3,1
                    tmp  = real(FRC%domain%cgridn(ii))
                    tmp  = tmp*2.5
                    ntmp = ceiling(tmp)
                    FRC%force%Gridn(ii) = max(ntmp,FRC%domain%fgridn(ii))
                end do
            end if
        end if
        
        FRC%init%force = .TRUE.
    end if
    
    return
    end subroutine initForce
    end module initForce_module