! initMPI.f90
    
!***************************************************************************************
    !
    ! This subroutine gets all the control parameter in FRC%mpi.
    !
!***************************************************************************************
    
    module initMPI_module
    contains
    subroutine initMPI(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    integer  :: ii, nline, status
    logical  :: mpiStatusIsFind, smiStatusIsFind, paraKisFind
    
    ! output variables
    
    ! body of this function
    mpiStatusIsFind = .FALSE.
    smiStatusIsFind = .FALSE.
    paraKisFind     = .FALSE.
    nline           = inputFromFile%NumberofInput
    ii = 0
    do while(ii < nline)
        ii     = ii+1
        if (trim(inputFromFile%NAV(ii)%name) == trim("mpiStatus")) then
            mpiStatusIsFind = .TRUE.
            exit
        end if
    end do
    if (mpiStatusIsFind) then
        read(inputFromFile%NAV(ii)%value,"(i2)") status
        if (status /= 0) then
            FRC%mpi%status = .TRUE.
        else 
            FRC%mpi%status = .FALSE.
        end if
    else
        FRC%mpi%status = .FALSE.
    end if
    
    ii = 0
    do while(ii < nline)
        ii     = ii+1
        if (trim(inputFromFile%NAV(ii)%name) == trim("smiStatus")) then
            smiStatusIsFind = .TRUE.
            exit
        end if
    end do
    if (smiStatusIsFind) then
        read(inputFromFile%NAV(ii)%value,"(i2)") status
        if (status /= 0) then
            FRC%smi%status = .TRUE.
        else
            FRC%smi%status = .FALSE.
        end if
    else
        FRC%smi%status = .FALSE.
    end if
    
    ii = 0
    do while(ii < nline)
        ii     = ii+1
        if (trim(inputFromFile%NAV(ii)%name) == trim("mpiparaK")) then
            paraKisFind = .TRUE.
            exit
        end if
    end do
    
    if (FRC%smi%status) then
        FRC%mpi%status = .TRUE.
    end if
    
    !call MPI_initialize(FRC)
    
    if (FRC%mpi%status) then
        write(*,*) "Error in initMPI.f90. MPI is not available now."
        stop
        !FRC%mpi%rank = MPI_Comm_rank
        !FRC%mpi%size = MPI_Comm_size
    else
        FRC%mpi%rank    = 0
        FRC%mpi%mpisize = 1
    end if
    
    if (.not. paraKisFind) then
        if (trim(FRC%info%calculationType) == trim("DFPT")) then
            FRC%mpi%paraK = .TRUE.
        else
            FRC%mpi%paraK = .FALSE.
        end if
    else
        read(inputFromFile%NAV(ii)%value,"(i2)") status
        if (status /= 0) then
            FRC%mpi%paraK = .TRUE.
        else 
            FRC%mpi%paraK = .FALSE.
        end if
    end if
    
    FRC%init%mpi = .TRUE.
    
    return
    end subroutine initMPI
    end module initMPI_module