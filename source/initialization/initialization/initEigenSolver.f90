! initEigenSolver.f90
    
!*************************************************************************************************
    !
    !
    !
!*************************************************************************************************
    
    module initEigenSolver_module
    contains
    subroutine initEigenSolver(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    use precondT_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    logical :: algoIsFind, eigenSolverAdapCFDIsFind, eigenSolveralgoprojIsFind, eigenSolverbandiIsFind
    logical :: eigenSolveremptybandIsFind, eigenSolverinitIsFind, eigenSolvermaxitIsFind, eigenSolvernsymIsFind
    logical :: eigenSolverorthogonalizeIsFind, eigenSolverprecondIsFind, eigenSolverpwmodeIsFind
    logical :: eigenSolvertolIsFind, eigenSolverubmaxitIsFind, eigenSolverubmethodIsFind, maxRestartIsFind
    logical :: sinIsFind, cosIsFind, rrIsFind, sincosIsFind, sincosrrIsFind
    integer :: xband, precond, maxit, ii, nspec, ispin, nband, nn, nline, status, lcut, zcut, jj, mpirank
    integer, allocatable :: nval(:), norb(:), lnu(:), znu(:)
    real*8  :: tol
    character(len=20) :: solver
    
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%eigensolver) then
        algoIsFind                     = .FALSE.
        eigenSolverAdapCFDIsFind       = .FALSE.
        eigenSolveralgoprojIsFind      = .FALSE.
        eigenSolverbandiIsFind         = .FALSE.
        eigenSolveremptybandIsFind     = .FALSE.
        eigenSolverinitIsFind          = .FALSE.
        eigenSolvermaxitIsFind         = .FALSE.
        eigenSolvernsymIsFind          = .FALSE.
        eigenSolverorthogonalizeIsFind = .FALSE.
        eigenSolverprecondIsFind       = .FALSE.
        eigenSolverpwmodeIsFind        = .FALSE.
        eigenSolvertolIsFind           = .FALSE.
        eigenSolverubmaxitIsFind       = .FALSE.
        eigenSolverubmethodIsFind      = .FALSE.
        maxRestartIsFind               = .FALSE.
        mpirank                        = FRC%mpi%rank
        nline                          = inputFromFile%numberOfInput
        
        if (trim(FRC%info%calculationType) == trim("band-structure") .or. trim(FRC%info%calculationType) == trim("DOS")) then
            solver  = "lobpcg"
            precond = 4
            maxit   = 1E2
            tol     = 1E-4
            xband   = 4
        else
            if (FRC%atom%NumberOfAtom > 64) then
                solver  = "cfsi"
                precond = -1
            else
                solver  = "lobpcg"
                precond = 4
            end if
            maxit   = 15
            tol     = 0.01
            xband   = max(16,FRC%atom%NumberOfAtom/4)
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("algo")) then
                algoIsFind = .TRUE.
                exit
            end if
        end do
        if (algoIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%eigenSolver%algo
        else
            FRC%eigenSolver%algo = solver
        end if
        if (algoIsFind .and. FRC%eigenSolver%algo == "cfsi") then
            precond = -1
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolverAdapCFD")) then
                eigenSolverAdapCFDIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolverAdapCFDIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%eigenSolver%adapCFD = .TRUE.
            else
                FRC%eigenSolver%adapCFD = .FALSE.
            end if
        else
            FRC%eigenSolver%adapCFD = .FALSE.
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolveralgoproj")) then
                eigenSolveralgoprojIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolveralgoprojIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%eigenSolver%algoproj
        else
            FRC%eigenSolver%algoproj = "DSYGVX"
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolverbandi")) then
                eigenSolverbandiIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolverbandiIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%eigenSolver%bandi
        else
            FRC%eigenSolver%bandi= 0
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolveremptyband")) then
                eigenSolveremptybandIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolveremptybandIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%eigenSolver%emptyband
        else
            FRC%eigenSolver%emptyband = xband
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolverinit")) then
                eigenSolverinitIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolverinitIsFind) then
            read(inputFromFile%NAV(ii)%value,*) FRC%eigenSolver%init
        else
            FRC%eigenSolver%init = "LCAO"
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolvermaxit")) then
                eigenSolvermaxitIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolvermaxitIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%eigenSolver%maxit
        else
            FRC%eigenSolver%maxit = maxit
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolvernsym")) then
                eigenSolvernsymIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolvernsymIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%eigenSolver%nsym
        else
            FRC%eigenSolver%nsym = 0
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolverorthogonalize")) then
                eigenSolverorthogonalizeIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolverorthogonalizeIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%eigenSolver%orthogonalize = .TRUE.
            else
                FRC%eigenSolver%orthogonalize = .FALSE.
            end if
        else
            FRC%eigenSolver%orthogonalize = .FALSE.
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolverprecond")) then
                eigenSolverprecondIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolverprecondIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%eigenSolver%precond
        else
            FRC%eigenSolver%precond = precond
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolverpwmode")) then
                eigenSolverpwmodeIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolverpwmodeIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%eigenSolver%pwmode = .TRUE.
            else
                FRC%eigenSolver%pwmode = .FALSE.
            end if
        else
            FRC%eigenSolver%pwmode = .FALSE.
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolvertol")) then
                eigenSolvertolIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolvertolIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%eigenSolver%tol
        else
            FRC%eigenSolver%tol = tol
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolverubmaxit")) then
                eigenSolverubmaxitIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolverubmaxitIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%eigenSolver%ubmaxit
        else
            FRC%eigenSolver%ubmaxit = 10
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("eigenSolverubmethod")) then
                eigenSolverubmethodIsFind = .TRUE.
                exit
            end if
        end do
        if (eigenSolverubmethodIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%eigenSolver%ubmethod
        else
            FRC%eigenSolver%ubmethod = "lanczos"
        end if
        
        if (.not. (trim(FRC%eigenSolver%algo) == trim("cfsi") .or. trim(FRC%eigenSolver%algo) == trim("eigs") .or. trim(FRC%eigenSolver%algo) == trim("lobpcg") .or. trim(FRC%eigenSolver%algo) == trim("lobpcg_soc"))) then
            write(*,*) "Error. Invalid value ", FRC%eigenSolver%algo, " for FRC%eigenSolver%algo."
        end if
        
        if (trim(FRC%eigenSolver%algo) == trim("eigs")) then
            FRC%mpi%paraK = .TRUE.
        end if
        
        if (trim(FRC%eigenSolver%algo) == trim("lobpcg") .or. trim(FRC%info%calculationType) == trim("dfpt")) then
            call precondT(FRC,FRC%eigenSolver%precond,FRC%eigenSolver%prekin)
        end if
        
        ii = 0
        do while(ii< nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("maxRestart")) then
                maxRestartIsFind = .TRUE.
                exit
            end if
        end do
        
        if (maxRestartIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%eigenSolver%maxRestart
        else
            if (trim(FRC%info%calculationType) == trim("band-structure") .or. trim(FRC%info%calculationType) == trim("dos")) then
                FRC%eigenSolver%maxRestart = 10
            else
                FRC%eigenSolver%maxRestart = 1
            end if
        end if
        
        sinIsFind      = .FALSE.
        cosIsFind      = .FALSE.
        rrIsFind       = .FALSE.
        sincosIsFind   = .FALSE.
        sincosrrIsFind = .FALSE.
        if (trim(FRC%eigenSolver%init) == trim("sin")) then
            sinIsFind = .TRUE.
        end if
        if (trim(FRC%eigenSolver%init) == trim("cos")) then
            cosIsFind = .TRUE.
        end if
        if (trim(FRC%eigenSolver%init) == trim("rr")) then
            rrIsFind = .TRUE.
        end if
        if (trim(FRC%eigenSolver%init) == trim("sin/cos")) then
            sincosIsFind = .TRUE.
        end if
        if (trim(FRC%eigenSolver%init) == trim("sin/cos/rr")) then
            sincosrrIsFind = .TRUE.
        end if
        
        if ((sinIsFind .or. cosIsFind .or. sincosIsFind .or. sincosrrIsFind) .and. (.not. rrIsFind)) then
            FRC%eigenSolver%init = "sin/cos"
        else if ((sinIsFind .or. cosIsFind .or. sincosIsFind .or. sincosrrIsFind) .and. rrIsFind) then
            FRC%eigenSolver%init = "sin/cos/rr"
        end if
        
        if (trim(FRC%eigenSolver%init) /= trim("LCAO") .and. trim(FRC%eigenSolver%init) /= trim("sin/cos") .and. trim(FRC%eigenSolver%init) /= trim("sin/cos/rr") .and. trim(FRC%eigenSolver%init) /= trim("rand")) then
            write(*,*) "Error. Invalid FRC%eigenSolver%init value."
        end if
        
        nspec = FRC%atom%NumberOfElement
        allocate(nval(nspec),norb(nspec))
        nval = 0
        norb = 0
        do ii = 1,nspec,1
            nval(ii) = FRC%element(ii)%valence
            allocate(lnu(size(FRC%element(ii)%aolnu)))
            lnu      = FRC%element(ii)%aolnu
            lcut     = FRC%element(ii)%aoLcutoff
            allocate(znu(size(FRC%element(ii)%aoznu)))
            znu      = FRC%element(ii)%aoznu
            zcut     = FRC%element(ii)%aoZcutoff
            nn       = size(lnu)
            !norb(ii) = 0
            do jj = 1,nn,1
                if (lnu(jj)<lcut .and. znu(jj)<zcut) then
                    norb(ii) = norb(ii)+2*FRC%element(ii)%aolnu(jj)+1
                end if
            end do
            deallocate(lnu,znu)
        end do
        FRC%eigenSolver%Nvalence = 0
        FRC%eigenSolver%norb     = 0
        do ii = 1,FRC%atom%NumberOfAtom,1
            FRC%eigenSolver%Nvalence = FRC%eigenSolver%Nvalence+nval(FRC%atom%element(ii))
            FRC%eigenSolver%norb     = FRC%eigenSolver%norb+norb(FRC%atom%element(ii))
        end do
        deallocate(nval)
        allocate(nval(1))
        deallocate(norb)
        allocate(norb(1))
        nval  = FRC%eigenSolver%Nvalence
        xband = FRC%eigenSolver%emptyBand
        norb  = FRC%eigenSolver%norb
        if ((xband > norb(1)-(nval(1)/2)) .and. trim(FRC%eigenSolver%init) == trim("LCAO")) then
            xband = norb(1)-(nval(1)/2)
            if (mpirank == 0) then
                write(*,*) "Warning. The number of bands exceeds the number of atomic orbitals. The value of mrc.eigensolver.emptyBand has been reset."
            end if
            FRC%eigenSolver%emptyBand = xband
        end if
        
        xband = FRC%eigenSolver%emptyBand
        ispin = FRC%spin%ispin
        
        if (all(FRC%eigenSolver%bandi == 0)) then
            if (ispin == 1 .or. ispin == 2) then
                nband = (nval(1)/2) + xband
            else if (ispin == 4) then
                nband = nval(1)+xband
            end if
            !allocate(FRC%eigenSolver%bandi(2))
            FRC%eigenSolver%bandi(1) = 1
            FRC%eigenSolver%bandi(2) = nband
        end if
        
        FRC%eigenSolver%nband = nband
        
        if (FRC%eigenSolver%nsym == 0) then
            FRC%eigenSolver%nsym = nband
        end if
        
        FRC%init%eigensolver = .TRUE.
    end if
    
    return
    end subroutine initEigenSolver
    end module initEigenSolver_module
        