! initInfo.f90
!************************************
    !
!************************************
    
    module initInfo_module
    contains
    subroutine initInfo(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    
    ! output variables

    ! body of this function
    FRC%init%addpath       = .FALSE.
    FRC%init%atominfo      = .FALSE.
    FRC%init%density       = .FALSE.
    FRC%init%domaininfo    = .FALSE.
    FRC%init%dos           = .FALSE.
    FRC%init%dfpt          = .FALSE.
    FRC%init%eigensolver   = .FALSE.
    FRC%init%element       = .FALSE.
    FRC%init%energy        = .FALSE.
    FRC%init%force         = .FALSE.
    FRC%init%gpu           = .FALSE.
    FRC%init%initsub       = .FALSE.
    FRC%init%interpolation = .FALSE.
    FRC%init%kpoint        = .FALSE.
    FRC%init%lcao          = .FALSE.
    FRC%init%mixer         = .FALSE.
    FRC%init%option        = .FALSE.
    FRC%init%poisson       = .FALSE.
    FRC%init%potential     = .FALSE.
    FRC%init%prr           = .FALSE.
    FRC%init%repxyz        = .FALSE.
    FRC%init%smearing      = .FALSE.
    FRC%init%smi           = .FALSE.
    FRC%init%spin          = .FALSE.
    FRC%init%sr            = .FALSE.
    FRC%init%symmetry      = .FALSE.
    FRC%init%ub            = .FALSE.
    FRC%init%xc            = .FALSE.
    
    return
    end subroutine initInfo
    end module initInfo_module