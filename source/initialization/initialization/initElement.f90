! initElement.f90
!*********************************
    !
!*********************************
    
    module initElement_module
    contains
    subroutine initElement(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    use readSummeryOfBasis_module
    use readPseudopotential_module
    use parseKBinfo_module
    use parseAOinfo_module
    !use AtomSymbolType_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType) :: inputFromFile
    !type(AtomSymbolType),    allocatable :: AtomSymbol(:)
    
    ! temperary variables
    integer                              :: mpirank, ii, jj, nspec
    real*8                               :: bohr2ang
    character(len=4)                     :: species
    
    ! output variables
    
    ! body of this function
    mpirank  = FRC%mpi%rank
    bohr2ang = 0.52917721067
    
    if (.not. FRC%init%atominfo) then
        nspec = FRC%atom%NumberOfElement
        allocate(FRC%SOB(nspec))
        allocate(FRC%ElementData(nspec))
        do ii = 1,nspec,1
            species = FRC%element(ii)%species
            call readSummeryOfBasis(species,FRC%SOB(ii))
            
            ! allocate Vlocal
            allocate(FRC%ElementData(ii)%Vlocal%rrData(FRC%SOB(ii)%SizeOfVlocalrrData))
            allocate(FRC%ElementData(ii)%Vlocal%drData(FRC%SOB(ii)%SizeOfVlocaldrData))
            allocate(FRC%ElementData(ii)%Vlocal%vvData(FRC%SOB(ii)%SizeOfVlocalvvData))
            
            ! allocate Rlocal
            allocate(FRC%ElementData(ii)%Rlocal%rrData(FRC%SOB(ii)%SizeOfRlocalrrData))
            allocate(FRC%ElementData(ii)%Rlocal%drData(FRC%SOB(ii)%SizeOfRlocaldrData))
            allocate(FRC%ElementData(ii)%Rlocal%rhoData(FRC%SOB(ii)%SizeOfRlocalrhoData))
            
            ! allocate vnl
            allocate(FRC%ElementData(ii)%Vnl(FRC%SOB(ii)%SizeOfVnl))
            do jj = 1,FRC%SOB(ii)%SizeOfVnl,1
                allocate(FRC%ElementData(ii)%Vnl(jj)%rrData(FRC%SOB(ii)%SizeOfVnlrrData))
                allocate(FRC%ElementData(ii)%Vnl(jj)%drData(FRC%SOB(ii)%SizeOfVnldrData))
                allocate(FRC%ElementData(ii)%Vnl(jj)%vvData(FRC%SOB(ii)%SizeOfVnlvvData))
                allocate(FRC%ElementData(ii)%Vnl(jj)%qqData(FRC%SOB(ii)%SizeOfVnlqqData))
                allocate(FRC%ElementData(ii)%Vnl(jj)%fqData(FRC%SOB(ii)%SizeOfVnlfqData))
                allocate(FRC%ElementData(ii)%Vnl(jj)%qwData(FRC%SOB(ii)%SizeOfVnlqwData))
            end do
            
            ! allocate Vna
            allocate(FRC%ElementData(ii)%Vna%rrData(FRC%SOB(ii)%SizeOfVnarrData))
            allocate(FRC%ElementData(ii)%Vna%drData(FRC%SOB(ii)%SizeOfVnadrData))
            allocate(FRC%ElementData(ii)%Vna%vvData(FRC%SOB(ii)%SizeOfVnavvData))
            
            ! allocate Rna
            allocate(FRC%ElementData(ii)%Rna%rrData(FRC%SOB(ii)%SizeOfRnarrData))
            allocate(FRC%ElementData(ii)%Rna%drData(FRC%SOB(ii)%SizeOfRnadrData))
            allocate(FRC%ElementData(ii)%Rna%rhoData(FRC%SOB(ii)%SizeOfRnarhoData))
            
            ! allocate RelpseudoP
            allocate(FRC%ElementData(ii)%RelPseudoP(FRC%SOB(ii)%SizeOfRelPseudoP))
            do jj = 1,FRC%SOB(ii)%SizeOfRelPseudoP,1
                allocate(FRC%ElementData(ii)%RelPseudoP(jj)%rrData(FRC%SOB(ii)%SizeOfRelPseudoPrrData))
                allocate(FRC%ElementData(ii)%RelPseudoP(jj)%drData(FRC%SOB(ii)%SizeOfRelPseudoPdrData))
                allocate(FRC%ElementData(ii)%RelPseudoP(jj)%vvData_screened(FRC%SOB(ii)%SizeOfRelPseudoPvvData_screened))
                allocate(FRC%ElementData(ii)%RelPseudoP(jj)%vvData_unscreened(FRC%SOB(ii)%SizeOfRelPseudoPvvData_unscreened))
            end do
            
            ! allocate OrbitalSet
            allocate(FRC%ElementData(ii)%OrbitalSet(FRC%SOB(ii)%SizeOfOrbitalSet))
            do jj = 1,FRC%SOB(ii)%SizeOfOrbitalSet,1
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%rrData(FRC%SOB(ii)%orbitalsize(jj)%SizeOfOrbitalSetrrData))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%drData(FRC%SOB(ii)%orbitalsize(jj)%SizeOfOrbitalSetdrData))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%frData(FRC%SOB(ii)%orbitalsize(jj)%SizeOfOrbitalSetfrData))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%qqData(FRC%SOB(ii)%orbitalsize(jj)%SizeOfOrbitalSetqqData))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%fqData(FRC%SOB(ii)%orbitalsize(jj)%SizeOfOrbitalSetfqData))
                allocate(FRC%ElementData(ii)%OrbitalSet(jj)%qwData(FRC%SOB(ii)%orbitalsize(jj)%SizeOfOrbitalSetqwData))
            end do
            
            call readPseudoPotential(species,FRC%SOB(ii),FRC%ElementData(ii))
            
            if (FRC%spin%SOI) then
                write(*,*) "Error in initElement.f90. SOI is not available now."
                stop
                !call AddSOI(Element)
            end if
            
            !if (FRC%element(ii)%aoprec == "SZ") then
            !    FRC%element(ii)%aoZcutoff =1
            !else if (FRC%element(ii)%aoprec == "DZ") then
            !    FRC%element(ii)%aoZcutoff =2
            !end if
            do jj = 1,FRC%SOB(ii)%SizeOfVnl,1
                FRC%ElementData(ii)%Vnl(jj)%N = 1
            end do
        end do
        call parseAOinfo(FRC)
        call parseKBinfo(FRC)
        
        do ii = 1,nspec,1
            FRC%element(ii)%Z          = sum(FRC%element(ii)%aopnu)
            FRC%element(ii)%valence    = sum(FRC%element(ii)%aopnu)
            FRC%element(ii)%vnlLcutoff = maxval(FRC%element(ii)%aolnu)+1
            FRC%element(ii)%aoLcutoff  = maxval(FRC%element(ii)%aolnu)+1
            FRC%element(ii)%aoZcutoff  = 3
            FRC%element(ii)%vnlNcutoff = 2
        end do
        
        FRC%init%Element = .TRUE.
    end if
    
    return
    end subroutine initElement
    end module initElement_module
    
    
