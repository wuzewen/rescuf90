! find_sym_op.f90
    
!**********************************************************
    !
    !
    !
!**********************************************************
    
    module find_sym_op_module
    contains
    subroutine find_sym_op(element,pos,avec,spaceSymmetry,tol,ph,magmom,allSym,allTrans,allPerm,allSym_rec)
    
    use bsxfunMinus_module
    use inversion_module
    use calcpermSym_module
    use uniqueRow_module
    use intersectRow_module
    
    implicit none
    
    ! input variables
    integer, allocatable :: element(:)
    real*8,  allocatable :: pos(:,:), avec(:,:), magmom(:,:)
    logical              :: spaceSymmetry, ph
    real*8               :: tol
    
    ! temporary variables
    integer :: natom, nelem, ii, jj, kk, ll, x(3), N, mtmp, ntmp, ttmp, nsym, ind, ss, nt1
    real*8  :: met(3,3), met_tmp(3,3), stmp(3,3), tvt(1,3), bvec(3,3), pi, alltn(3,3), allout(3,3)
    logical :: failflag
    integer,allocatable :: deter(:), perm(:), tmptrans(:,:), tmpperm(:,:), symop(:,:), symopt(:,:,:)
    integer,allocatable :: symopr(:,:,:), allSymTmp(:,:,:)
    real*8, allocatable :: magmomTMP(:,:), posx(:,:), posN(:,:), allTransTmp(:,:), allPermTmp(:,:)
    real*8, allocatable :: postmp(:,:), tvecss(:,:), tvecs(:,:), tvecsT(:,:), tvectmp(:,:), tvectmp1(:,:)
    real*8, allocatable :: tvectmp2(:,:), avtmp(:,:), a2b(:,:), b2a(:,:)
    
    ! output variables
    integer, allocatable :: allSym(:,:,:), allSym_rec(:,:,:)
    real*8,  allocatable :: allTrans(:,:), allPerm(:,:)
    
    ! body of this function
    pi    = 3.14159265354
    natom = size(element)
    nelem = maxval(element)
    if (size(magmom) == 0) then
        allocate(magmomTMP(natom,1))
        magmomTMP = 0
    else
        allocate(magmomTMP(natom,size(magmom,2)))
        magmomTMP = magmom
    end if
    
    met = matmul(avec,transpose(avec))
    
    forall(ii=1:3)
        x(ii) = ii-2
    end forall
    
    N = 3**9
    allocate(symop(N,9))
    do ii = 1,9,1
        ntmp = 3**ii
        mtmp = ntmp/3
        ttmp = 0
        do jj = 1,N/ntmp,1
            do kk = 1,3,1
                do ll = 1,mtmp,1
                    ttmp = ttmp+1
                    symop(ttmp,ii) = x(kk)
                end do
            end do
        end do
    end do
    
    allocate(deter(N))
    
    deter  = symop(:,1)*symop(:,5)*symop(:,9)+symop(:,2)*symop(:,6)*symop(:,7) &
           + symop(:,3)*symop(:,4)*symop(:,8)-symop(:,7)*symop(:,5)*symop(:,3) &
           - symop(:,8)*symop(:,6)*symop(:,1)-symop(:,9)*symop(:,4)*symop(:,2)
    allocate(symopt(3,3,N))
    symopt = reshape(transpose(symop),(/3,3,N/))
    
    nsym = 0
    do ii = 1,N,1
        if (abs(deter(ii)) == 1) then
            nsym = nsym+1
        end if
    end do
    
    !deallocate(symop)
    allocate(symopr(3,3,nsym))
    ttmp = 0
    do ii = 1,N,1
        if (abs(deter(ii)) == 1) then
            ttmp = ttmp+1
            symopr(:,:,ttmp) = symopt(:,:,ii)
        end if
    end do
    
    deallocate(deter)
    allocate(deter(nsym))
    deter = 0
    
    do ii = 1,nsym,1
        stmp    = real(symopr(:,:,ii))
        met_tmp = matmul(stmp,avec)
        met_tmp = matmul(met_tmp,transpose(met_tmp))
        met_tmp = abs(met_tmp-met)
        if (maxval(met_tmp) < tol) then
            deter(ii) = 1
        end if
    end do
    
    ntmp = count(deter == 1)
    deallocate(symopt)
    allocate(symopt(3,3,ntmp))
    
    
    ttmp = 0
    do ii = 1,nsym,1
        if (deter(ii) == 1) then
            ttmp = ttmp+1
            symopt(:,:,ttmp) = symopr(:,:,ii)
        end if
    end do
    
    nsym = ttmp
    allocate(postmp(1,3))
    
    ntmp = 0
    do ii = 1,nsym,1
        stmp = symopt(:,:,ii)
        allocate(posx(natom,3))
        posx = matmul(pos,stmp)
        allocate(posN(natom,3))
        forall(jj=1:size(pos,1),kk=1:3)
            posN(jj,kk) = anint(posx(jj,kk))
        end forall
        posx = posx - posN
        
        if (spaceSymmetry) then
            ind = 1
            
            postmp(1,:) = pos(ind,:)
            allocate(tvecss(natom,3))
            call bsxfunMinus(postmp,posx,tvecss)
            tvecss = anint(tvecss/tol)*tol
            tvecss = tvecss+0.0000001
            tvecss = tvecss-anint(tvecss)
            tvecss = anint(tvecss/tol)*tol
            call uniqueRow(tvecss,tvecst)
            allocate(tvectmp(size(tvecst,1)+1,3))
            tvectmp(1,:) = 0
            forall(jj=1:size(tvecst,1),kk=1:3)
                tvectmp(jj+1,kk) = tvecst(jj,kk)
            end forall
            
            do ss = 2,nelem
                ind = 0
                do jj = 1,natom,1
                    if (element(jj) /= ss) then
                        ind = ind+1
                    end if
                end do
                ind = ind+1
                postmp(1,:) = pos(ind,:)
                call bsxfunMinus(postmp,posx,tvecss)
                tvecss = anint(tvecss/tol)*tol
                tvecss = tvecss-anint(tvecss)
                tvecs  = anint(tvecss/tol)*tol
                call uniqueRow(tvecs,tvecst)
                tvectmp1(1,:) = 0
                tvectmp1(2:size(tvecst,1),:) = tvecst(:,:)
                call intersectRow(tvectmp,tvectmp1,tvectmp2)
                deallocate(tvectmp)
                allocate(tvectmp(size(tvectmp2,1),size(tvectmp2,2)))
                tvectmp = tvectmp2
                deallocate(tvectmp2)
            end do
            deallocate(tvecss,tvecst)
        else
            allocate(tvectmp(1,3))
            tvectmp = 0
        end if
        
        tvectmp = anint(tvectmp/tol)*tol
        call uniqueRow(tvectmp,tvectmp2)
        nt1 = size(tvectmp2,1)
        
        do jj = 1,nt1,1
            !stmp = symopt(:,:,i)
            tvt(1,:) = tvectmp2(jj,:)
            call calcPermSym(stmp,tvt,pos,tol,perm,failflag)
            if (.not. failflag) then
                if (.not. all(element(perm) == element)) then
                    failflag = .TRUE.
                else
                    failflag = .FALSE.
                end if
                if (.not. all(magmom(perm,:) == magmom)) then
                    failflag = .TRUE.
                else if (failflag) then
                    failflag = .TRUE.
                else
                    failflag = .FALSE.
                end if
            end if
            
            if ((.not. failflag) .and. (.not. ph)) then
                ntmp = ntmp+1
                allocate(allTransTmp(ntmp,3))
                allocate(allSymTmp(3,3,ntmp))
                allocate(allPermTmp(size(perm,1),ntmp))
                !allocate(allTransTmp(ntmp,3))
                if (ntmp == 1) then
                    allTransTmp(ntmp,:)    = tvectmp(jj,:)
                    allSymTmp(:,:,ntmp)    = symopt(:,:,ii)
                    allPermTmp(:,ntmp)     = perm(:)
                    !allTransTmp(ntmp,:) = tvectmp(jj,:)
                else if (ntmp > 1) then
                    forall(kk=1:ntmp-1)
                        allTransTmp(kk,:) = allTrans(kk,:)
                        allSymTmp(:,:,kk) = allSym(:,:,kk)
                        allPermTmp(:,kk)  = allPerm(:,kk)
                    end forall
                    allTransTmp(ntmp,:) = tvectmp(jj,:)
                    allSymTmp(:,:,ntmp) = symopt(:,:,ii)
                    allPermTmp(:,ntmp)  = perm(:)
                    deallocate(allTrans)
                    deallocate(allSym)
                    deallocate(allPerm)
                end if
                allocate(allTrans(ntmp,3))
                allTrans = allTransTmp
                deallocate(allTransTmp)
                allocate(allSym(3,3,ntmp))
                allSym   = allSymTmp
                deallocate(allSymTmp)
                allocate(allPerm(size(perm),ntmp))
                allPerm  = allPermTmp
                deallocate(allPermTmp)
                
                !allTrans((ii-1)*nt1+jj,:) = tvectmp(jj,:)
                !allSym(:,:,(ii-1)*nt1+jj) = symopt(:,:,ii)
                !allPerm(:,(ii-1)*nt1+jj)  = perm
                deallocate(perm)
                exit
            else if (.not. failflag) then
                write(*,*) "Error in find_sym_op.f90. ph is not available now."
                stop
                !tmpTrans(jj,:) = tvectmp(jj,:)
                !tmpPerm(:,jj)  = perm
            end if
            
        end do
        
        if (ph) then
            write(*,*) "Error in find_sym_op.f90. ph is not available now."
            stop
            !nt = size(tmpTrans,1)
            !allTrans(ii,:) = tmpTrans
            !allPerm(:,ii)  = tmpPerm
            !allSym(:,:,ii) = symopt 
        end if
        deallocate(posx,posN,tvectmp,tvectmp2)
    end do
    
    nsym = size(allSym,3)
    allocate(allSym_rec(size(allSym,1),size(allSym,2),nsym))
    allSym_rec = 0
    allocate(avtmp(3,3))
    call inversion(avec,avtmp)
    bvec = 2.0*pi*avtmp
    allocate(a2b(3,3),b2a(3,3))
    a2b  = matmul(bvec,avtmp)
    call inversion(a2b,b2a)
    do ii = 1,nsym,1
        alltn = real(allSym(:,:,ii))
        allout = matmul(a2b,matmul(alltn,b2a))
        do jj = 1,3,1
            do kk = 1,3,1
                allSym_rec(jj,kk,ii) = anint(allout(jj,kk))
            end do
        end do
    end do
    allSym_rec = anint(real(allSym_rec))
    
    return
    end subroutine find_sym_op
    end module find_sym_op_module