! initSR.f90
    
!************************************************************************
    !
    !
    !
!************************************************************************
    
    module initSR_module
    contains
    subroutine initSR(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType) :: inputFromFile
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    if (trim(FRC%info%calculationType) == trim("relaxation")) then
        write(*,*) "Error in initSR.f90. Structure relaxation is not available now."
        stop
    end if
    
    FRC%init%sr = .TRUE.    
    
    return
    end subroutine initSR
    end module initSR_module