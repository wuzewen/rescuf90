! NineDgrid.f90
    
!***********************************************************
    !
    ! This function can only be used in find_sym_op.f90.
    !
!***********************************************************
    
    module NineDgrid_module
    contains
    subroutine NineDgrid(symop)
    
    implicit none
    
    ! inout variables
    
    ! temporary variables
    integer :: x(3)
    integer, allocatable :: 
    
    ! output variables
    
    ! body of this function
    forall(ii=1:3)
        x(ii) = ii-2
    end forall
    
    N = 3**9
    
    do ii = 1,9,1
        ntmp = 3**ii
        mtmp = ntmp/3
        
        ttmp = 0
        do jj = 1,N/ntmp,1
            do kk = 1,3,1
                do ll = 1,mtmp,1
                    ttmp = ttmp+1
                    symop(ttmp,ii) = x(kk)
                end do
            end do
        end do
    end do
    
    return
    end subroutine NineDgrid
    end module NineDgrid_module