! initStructureAndLattice.f90
!*********************************
    !
!*********************************
    
    module initStructureAndLattice_module
    contains
    subroutine initStructureAndLattice(filename,NumberOfElement,NumberOfAtoms,AtomicStructure)
    
    implicit none
    
    ! input variables
    integer                         :: NumberOfElement, NumberOfAtoms
    character(len=20)               :: filename
    
    ! temporary variables
    character(len=20), allocatable  :: species(:)
    character(len=20)               :: nonsense
    real*8                          :: const
    integer, allocatable            :: numbers(:)
    integer                         :: ii,jj,nn
    
    ! output variables
    type :: AtomType
        character(len=4)            :: symbol
        real*8                      :: position(3)
    end type AtomType
    
    type :: AtomicStructureType
        character(len=10)           :: coordinateType
        real*8                      :: Latvec(3,3)
        type(AtomType), allocatable :: Atom(:)
    end type AtomicStructureType
    type(AtomicStructureType) AtomicStructure
    
    ! body of this function
    open(unit=10,file=fileName,status='old',access='direct',action='read')
    read(10,*) nonsense
    read(10,*) const
    read(10,*) AtomicStructure%Latvec(1,:)
    AtomicStructure%Latvec(1,:) = AtomicStructure%Latvec(1,:)*const
    read(10,*) AtomicStructure%Latvec(2,:)
    AtomicStructure%Latvec(2,:) = AtomicStructure%Latvec(2,:)*const
    read(10,*) AtomicStructure%Latvec(3,:)
    AtomicStructure%Latvec(3,:) = AtomicStructure%Latvec(3,:)*const
    allocate(species(NumberOfElement))
    read(10,*) species(:)
    allocate(numbers(NumberOfElement))
    read(10,*) numbers(:)
    allocate(AtomicStructure%Atom(NumberOfAtoms))
    nn = 0
    do ii = 1,NumberOfElement,1
        do jj = 1,numbers(ii),1
            nn = nn+1
            AtomicStructure%Atom(nn)%symbol = species(ii)
            read(10,*) AtomicStructure%Atom(nn)%position(:)
        end do
    end do
    
    close(10)
    return
    end subroutine initStructureAndLattice
    end module initStructureAndLattice_module