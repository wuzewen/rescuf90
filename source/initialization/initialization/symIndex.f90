! symIndex.f90
    
!************************************************************
    !
    !
    !
!************************************************************
    
    module symIndex_module
    contains
    subroutine symIndex(symArray,sym,inde)
    
    implicit none
    
    ! input variables
    integer, allocatable :: symArray(:,:,:)
    real*8,  allocatable :: sym(:,:)
    
    ! temporary variables
    real*8  :: tmp(3,3)
    integer :: ntmp, nsym, ii
    
    ! output variables
    integer, allocatable :: inde(:)
    
    ! body of this function
    nsym = size(symArray,3)
    ntmp = 0
    do ii = 1,nsym,1
        tmp(:,:) = symArray(:,:,ii)
        if (all(tmp == sym)) then
            ntmp = ntmp+1
        end if
    end do
    allocate(inde(ntmp))
    ntmp = 0
    do ii = 1,nsym,1
        tmp(:,:) = symArray(:,:,ii)
        if (all(tmp == sym)) then
            ntmp       = ntmp+1
            inde(ntmp) = ii
        end if
    end do
    
    return
    end subroutine symIndex
    end module symIndex_module