! findFuncApprox.f90
    
!*******************************************************************************
    !
    ! 
    !
!*******************************************************************************
    
    module findFuncApprox_module
    contains
    subroutine findFuncApprox(inputString,aprx)
    
    implicit none
    
    ! input variables
    character(len=20)              :: inputString
    !character(len=20),allocatable :: inputString(:)
    
    ! temperary variables
    logical                        :: xcind, LSDAisFind, LDAisFind, GGAisFind
    integer                        :: locate, ii
    character(len=100),allocatable :: stringtmp(:)
    character(len=100)             :: stringtmp2
    
    ! output variables
    character(len=20)              :: aprx
    
    ! body of this function
    !call strsplit(inputString,"\n",inputString)
    !do ii = 1,size(inputString),1
    !    xcind  = .FALSE.
    !    locate = 0
    !    locate = scan(inputString(ii),"XC")
    !    if (locate>0) then
    !        xcind = .TRUE.
    !        exit
    !    end if
    !end do
    
    !stringtmp = inputString(ii)
    !call strsplit(stringtmp," ",stringtmp)
    !stringtmp2 = stringtmp(size(stringtmp))
    
    LSDAisFind = .FALSE.
    LDAisFind  = .FALSE.
    GGAisFind  = .FALSE.
    
    locate     = 0
    locate     = scan(stringtmp2,"LSDA")
    if (locate>0) then
        LSDAisFind = .TRUE.
    end if
    
    locate     = 0
    locate     = scan(stringtmp2,"LDA")
    if (locate>0) then
        LDAisFind = .TRUE.
    end if
    
    locate     = 0
    locate     = scan(stringtmp2,"GGA")
    if (locate>0) then
        GGAisFind = .TRUE.
    end if
    
    if (LSDAisFind .or. LDAisFind) then
        aprx = "LDA"
    else if (GGAisFind) then
        aprx = "GGA"
    else
        aprx = "Unknown"
    end if
    
    return
    end subroutine findFuncApprox
    end module findFuncApprox_module

