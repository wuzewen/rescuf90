! initPRR.f90
!***********************************
    !
!***********************************
    
    module initPRR_module
    contains
    subroutine initPRR(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    integer :: ii, pdef, nline, status
    character(len=20) :: sol
    logical :: partialRRstatusisFind, partialRRpeigisFind, partialRRRitzVectorsisFind
    logical :: partialRRsolverisFind, partialRRissymisFind, partialRRisrealisFind
    logical :: partialRRtolisFind, partialRRmaxitisFind
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%PRR) then
        nline                      = inputFromFile%NumberOfInput
        partialRRstatusisFind      = .FALSE.
        partialRRpeigisFind        = .FALSE.
        partialRRRitzVectorsisFind = .FALSE.
        partialRRsolverisFind      = .FALSE.
        partialRRissymisFind       = .FALSE.
        partialRRisrealisFind      = .FALSE.
        partialRRtolisFind         = .FALSE.
        partialRRmaxitisFind       = .FALSE.
        
        pdef = FRC%eigenSolver%emptyBand +4
        if (FRC%smi%status) then
            sol = "SMI"
        else
            sol = "arpack"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("partialRRstatus")) then
                partialRRstatusisFind = .TRUE.
                exit
            end if
        end do
        if (partialRRstatusisFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%partialRR%status = .TRUE.
            else
                FRC%partialRR%status = .FALSE.
            end if
        else
            FRC%partialRR%status = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("partialRRpeig")) then
                partialRRpeigisFind = .TRUE.
                exit
            end if
        end do
        if (partialRRpeigisFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%partialRR%peig
        else
            FRC%partialRR%peig = pdef
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("partialRRRitzVectors")) then
                partialRRRitzVectorsisFind = .TRUE.
                exit
            end if
        end do
        if (partialRRRitzVectorsisFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%partialRR%RitzVectors
        else
            FRC%partialRR%RitzVectors = 0
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("partialRRsolver")) then
                partialRRsolverisFind = .TRUE.
                exit
            end if
        end do
        if (partialRRsolverisFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%partialRR%solver
        else
            FRC%partialRR%solver = sol
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("partialRRissym")) then
                partialRRissymisFind = .TRUE.
                exit
            end if
        end do
        if (partialRRissymisFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%partialRR%opts%issym = .FALSE.
            else
                FRC%partialRR%opts%issym  = .TRUE.
            end if
        else
            FRC%partialRR%opts%issym  = .TRUE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("partialRRisreal")) then
                partialRRisrealisFind = .TRUE.
                exit
            end if
        end do
        if (partialRRisrealisFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%partialRR%opts%isreal = .FALSE.
            else
                FRC%partialRR%opts%isreal = .TRUE.
            end if
        else
            FRC%partialRR%opts%isreal = .TRUE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("partialRRtol")) then
                partialRRtolisFind = .TRUE.
                exit
            end if
        end do
        if (partialRRtolisFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%partialRR%opts%tol
        else
            FRC%partialRR%opts%tol = 1E-8
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("partialRRmaxit")) then
                partialRRmaxitisFind = .TRUE.
                exit
            end if
        end do
        if (partialRRmaxitisFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%partialRR%opts%maxit
        else
            FRC%partialRR%opts%maxit = 50
        end if
        
        FRC%init%PRR = .TRUE.
    end if
    
    return
    end subroutine initPRR
    end module initPRR_module