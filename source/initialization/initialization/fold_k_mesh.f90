! fold_k_mesh.f90
    
!*********************************************************
    !
    !
    !
!*********************************************************
    
    module fold_k_mesh_module
    contains
    subroutine fold_k_mesh(kpt,sym_rec,timereversal,tol,kred,ikpt,ired,isop)
    
    use intersectRow_3output_module
    use union2_module
    use setdiff_module
    
    implicit none
    
    ! input variables
    real*8,  allocatable :: kpt(:,:)
    integer, allocatable :: sym_rec(:,:,:)
    logical              :: timereversal
    real*8               :: tol
    
    ! temporary variables
    integer :: nkpt, nsym, ii, jj, kk, ntmp
    real*8,  allocatable :: kptmp(:,:), kxx(:,:), kxt(:,:), kredtmp(:,:), kx(:,:), mkx(:,:)
    real*8,  allocatable :: input1(:,:), input2(:,:), nonsense(:,:)
    integer, allocatable :: symtmp(:,:), inde(:), miint(:), mjint(:), niint(:), njint(:), miintmp(:)
    integer, allocatable :: lint(:), indetmp(:)
    
    ! output variables
    real*8,  allocatable :: kred(:,:)
    integer, allocatable :: ikpt(:), ired(:), isop(:)
    
    !body of this funtion
    nkpt   = size(kpt,1)
    allocate(kptmp(nkpt,3))
    kptmp  = kpt-real(int(kpt))
    kptmp  = kptmp-real(anint(kptmp))
    nsym   = size(sym_rec,3)
    allocate(symtmp(3,3*nsym))
    symtmp = reshape(sym_rec,(/3,3*nsym/))
    
    allocate(ikpt(nkpt))
    ikpt  = 0
    allocate(ired(nkpt))
    ired  = 0
    allocate(isop(nkpt))
    isop  = 0
    allocate(inde(nkpt))
    
    forall(ii=1:nkpt)
        inde(ii) = ii
    end forall
    
    
    allocate(kxx(3,3*nsym))
    allocate(kxt(1,3))
    allocate(mkx(nsym,3))
    
    allocate(input2(nsym,3))
    ntmp = 0
    do ii = 1,nkpt,1
        if (ired(ii) == 0) then
            ntmp = ntmp+1
            allocate(kredtmp(ntmp,3))
            if (ntmp == 1) then
                kredtmp(ntmp,:) = kpt(ii,:)
            else
                forall(jj=1:ntmp-1)
                    kredtmp(jj,:) = kred(jj,:)
                end forall
                deallocate(kred)
                kredtmp(ntmp,:) = kpt(ii,:)
            end if
            allocate(kred(ntmp,3))
            kred = kredtmp
            deallocate(kredtmp)
            
            kxt(1,:) = kpt(ii,:)
            allocate(kx(1,3*nsym))
            kx  = matmul(kxt,symtmp)
            kxx = reshape(kx,(/3,nsym/))
            deallocate(kx)
            allocate(kx(nsym,3))
            kx = transpose(kxx)
            
            if (timereversal) then
                
                mkx = -kx
                forall(jj=1:3,kk=1:nsym)
                    mkx(kk,jj) = mod(mkx(kk,jj),1.0)
                    mkx(kk,jj) = mkx(kk,jj)-anint(mkx(kk,jj))
                end forall
                allocate(input1(size(inde),3))
                forall(kk=1:size(inde),jj=1:3)
                    input1(kk,jj) = real(anint(kpt(inde(kk),jj)/tol))*tol
                end forall
                
                forall(kk=1:nsym,jj=1:3)
                    input2(kk,jj) = real(anint(mkx(kk,jj)/tol))*tol
                end forall
                call intersectRow_3output(input1,input2,nonsense,miint,mjint)
                
                !forall(ii=1:size(miint))
                !    miint(ii)       = inde(miint(ii))
                !end forall
                
                forall(jj=1:size(miint))
                    miint(jj)       = inde(miint(jj))
                    ikpt(miint(jj)) = -ii
                    ired(miint(jj)) = -size(kred,1)
                    isop(miint(jj)) = -mjint(jj)
                end forall
                deallocate(nonsense,input1)
            else
                allocate(miint(0))
            end if
            
            forall(kk=1:nsym,jj=1:3)
                kx(kk,jj) = mod(kx(kk,jj),1.0)
                kx(kk,jj) = kx(kk,jj)-real(anint(kx(kk,jj)))
            end forall
            
            allocate(input1(size(inde),3))
            !allocate(input2(nsym,3))
            forall(kk=1:size(inde),jj=1:3)
                input1(kk,jj) = real(anint(kpt(inde(kk),jj)/tol))*tol
            end forall
            forall(kk=1:3,jj=1:nsym)
                input2(jj,kk) = real(anint(kx(jj,kk)/tol))*tol
            end forall
            call intersectRow_3output(input1,input2,nonsense,niint,njint)
            
            forall(kk=1:size(niint))
                niint(kk)       = inde(niint(kk))
                ikpt(niint(kk)) = ii
                ired(niint(kk)) = size(kred,1)
                isop(niint(kk)) = njint(kk)
            end forall
            
            if (size(miint) /= 0 .or. size(niint) /= 0) then
                call union2(miint,niint,lint)
                !if (.not. all(inde == lint)) then
                call setdiff(inde,lint,indetmp)
                !end if
                jj = size(indetmp)
                if (size(indetmp) > 0) then
                    deallocate(inde)
                    allocate(inde(size(indetmp)))
                    inde = indetmp
                    deallocate(indetmp)
                end if
            end if
            
            deallocate(input1,nonsense,niint,njint,miint,mjint,lint)
            deallocate(kx)
        end if
    end do
    
    return
    end subroutine fold_k_mesh
    end module fold_k_mesh_module