! sparseAOinfo.f90
    
!*************************************************************************
    !
    !
    !
!*************************************************************************
    
    module parseAOinfo_module
    contains
    subroutine parseAOinfo(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    real*8,  allocatable       :: rnu(:)
    integer, allocatable       :: lnu(:)
    integer, allocatable       :: pnu(:)
    integer, allocatable       :: znu(:)
    integer                    :: nset, ii, jj, nelem
    
    ! out put variables

    ! body of this function
    nelem = FRC%Atom%numberOfElement
    do ii = 1,nelem,1
        nset = FRC%SOB(ii)%SizeOfOrbitalSet
        allocate(lnu(nset),pnu(nset),rnu(nset),znu(nset))
        lnu         = 0
        pnu         = 0
        rnu         = 0
        znu         = 0
        do jj = 1,nset,1
            lnu(jj) = FRC%ElementData(ii)%OrbitalSet(jj)%L
            pnu(jj) = FRC%ElementData(ii)%OrbitalSet(jj)%Population
            !allocate(rnu(jj)%rnuD(FRC%SOB(ii)%SizeOfOrbitalSetrrData))
            rnu(jj) = maxval(FRC%ElementData(ii)%OrbitalSet(jj)%rrData)
            if (trim(FRC%ElementData(ii)%OrbitalSet(jj)%OrbitalType) == trim("zeta1-confined")) then
                znu(jj) = 1
            else if (trim(FRC%ElementData(ii)%OrbitalSet(jj)%OrbitalType) == trim("zeta2-confined")) then
                znu(jj) = 2
            else
                znu(jj) = 0
            end if
        end do
        allocate(FRC%element(ii)%aolnu(nset))
        allocate(FRC%element(ii)%aopnu(nset))
        allocate(FRC%element(ii)%aornu(nset))
        allocate(FRC%element(ii)%aoznu(nset))
        FRC%element(ii)%aolnu = lnu
        FRC%element(ii)%aopnu = pnu
        FRC%element(ii)%aornu = rnu
        FRC%element(ii)%aoznu = znu
    end do
    
    return
    end subroutine parseAOinfo
    end module parseAOinfo_module
            
    
    