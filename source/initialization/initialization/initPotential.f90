! initPotential.f90
!*********************************
    !
!*********************************
    
    module initPotential_module
    contains
    subroutine initPotential(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType)              :: inputFromFile
    
    ! temporary variables
    integer              :: ii, nspec, nline, status
    logical              :: fourieinitIsFind, separableIsFind
    integer, allocatable :: vnlsize(:)
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%potential) then
        nline = inputFromFile%numberOfInput
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("fourieinit")) then
                fourieinitIsFind = .TRUE.
                exit
            end if
        end do
        if (fourieinitIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status == 0) then
                FRC%potential%fourierinit = .FALSE.
            else
                FRC%potential%fourierinit = .TRUE.
            end if
        else
            FRC%potential%fourierinit = .TRUE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("separable")) then
                separableIsFind = .TRUE.
                exit
            end if
        end do
        if (separableIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%potential%vnl%separable = .TRUE.
            else
                FRC%potential%vnl%separable = .FALSE.
            end if
        else
            FRC%potential%vnl%separable = .FALSE.
        end if
        
        nspec = FRC%atom%numberOfElement
        allocate(vnlsize(nspec))
        vnlsize(:) = 0
        
        do ii = 1,nspec,1
            vnlsize(ii) = (FRC%element(ii)%vnlLcutoff+1)**2
        end do
        allocate(FRC%potential%vnl%vnlsize(nspec))
        FRC%potential%vnl%vnlsize = vnlsize
        
        FRC%init%potential = .TRUE.
    end if
    
    return
    end subroutine initPotential
    end module initPotential_module
    
    