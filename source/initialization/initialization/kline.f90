    module kline_module
    contains
    subroutine kline(crystalstructure,sympoint,nkpt,avec,sympoint,kvec,indexk,jkvec)
    
    implicit none
    
    ! input variables
    character(len=10) :: crystalstructure
    sympoint
    integer :: nkpt
    real*8  :: avec(3,3)
    
    ! temporary variables
    
    ! output variables
    sympoint2
    kvec
    index
    jkvec
    
    ! body of this function
    call parsekline(sympoint)
    call klinelength(crystalstructure,sympoint,avec)
    tlen = sum(klen)
    nline = size(klen,1)
    indexk = sympoint
    do ii = 1,nline,1
        ntmp = ceiling(klen(ii)/tlen*nkpt)
        call label2line(crystalstructure,sympoint(ii),ntmp,avec,ktmp)
        indexk(ii) =(/size(kvec,1)+1,size(kvec,1)+size(ktmp,1)/)
        kvectmp(1:3,:) = kvec
        kvectmp(4:6,:) = ktmp
    end
    call unique(kvec,"rows","stable")
    
    return
    end subroutine kline
    end module kline_module
    

