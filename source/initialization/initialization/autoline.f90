    module autoline_module
    contains
    subroutine autoline(crystalstructure,kline)
    
    implicit none
    
    ! input variables
    character(len=10)      :: crystalstructure
    
    ! temporary variables
    
    ! output variables
    character, allocatable :: kline(:)
    
    ! body of this function
    select case (crystalstructure)
    case("CUB")
        kline = (/"G","X","M","G","R","X","M","R"/)
    case("FCC")
        kline = (/"G","X","W","K","G","L","U","W","L","K","U","X"/)
    case("BCC")
        kline = (/"G","H","N","G","P","H","P","N"/)
    case("TET")
        kline = (/"G","X","M","G","Z","R","A","Z","X","R","M","A"/)
    case("BCT1")
        kline = (/"G","X","M","G","Z","P","N","Z1","M","X","P"/)
    case("BCT2")
        kline = (/"G","X","Y","SIG","G","Z","SIG1","N","P","Y1","Z","X","P"/)
    case("ORC")
        kline = (/"G","X","S","Y","G","Z","U","R","T","Z","Y","T","U","X","S","R"/)
    case("ORCF1")
        kline = (/"G","Y","T","Z","G","X","A1","Y","T","X1","X","A","Z","L","G"/)
    case("ORCF2")
        kline = (/"G","Y","C","D","X","G","Z","D1","H","C","C1","Z","X","H1","H","Y","L","G"/)
    case("ORCF3")
        kline = (/"G","Y","T","Z","G","X","A1","Y","X","A","Z","L","G"/)
    case("ORCI")
        kline = (/"G","X","L","T","W","R","X1","Z","G","Y","S","W","L1","Y","Y1","Z"/)
    case("ORCC")
        kline = (/"G","X","S","R","A","Z","G","Y","X1","A1","T","Y","Z","T"/)
    case("HEX")
        kline = (/"G","M","K","G","A","L","H","A","L","M","K","H"/)
    case("RHL1")
        kline = (/"G","L","B1","B","Z","G","X","Q","F","P1","Z","L","P"/)
    case("RHL2")
        kline = (/"G","P","Z","Q","G","F","P1","Q1","L","Z"/)
    case("MCL")
        kline = (/"G","Y","H","C","E","M1","A","X","G","Z","D","M","Z","A","D","Y","X","H1"/)
    case("MCLC1")
        kline = (/"G","Y","F","L","I","I1","Z","G","X","X1","Y","M","G","N","Z","F1"/)
    case("MCLC2")
        kline = (/"G","Y","F","L","I","I1","Z","F1","N","G","M"/)
    case("MCLC3")
        kline = (/"G","Y","F","H","Z","I","X","G","Z","M","G","N","X","Y1","H1","I","F1"/)
    case("MCLC4")
        kline = (/"G","Y","F","H","Z","I","H1","Y1","X","G","N","M","G"/)
    case("MCLC5")
        kline = (/"G","Y","F","L","I","I1","Z","G","X","Y1","H1","H","F1","F2","X","M","G","N","H","Z"/)
    case default
        write(*,*) "error. Wrong crystal structure."
    end select

    return
    end subroutine autoline
    end module autoline_module
    