! repXYZ.f90
!*******************************************
    !
!*******************************************
    
    module repXYZ_module
    contains
    subroutine repXYZ(FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    atomXYZisUsed = .FALSE.
    FracXYZisUsed = .FALSE.
    if (size(FRC%atom%XYZ)>1) then
        atomXYZisUsed = .TRUE.
    end if
    
    if (size(FRC%atom%FracXYZ)>1) then
        FracXYZisUsed = .TRUE.
    end if
    
    if (.not. FRC%init%repXYZ) then
        if (.not. (all(FRC%option%replicateCell==1))) then
            natom = size(FRC%atom%element,1)
            acev  = FRC%domain%latvec
            lx    = FRC%option%replicateCell(1)
            ly    = FRC%option%replicateCell(2)
            lz    = FRC%option%replicateCell(3)
            ll    = lx*ly*lz
            
            if (atomXYZisUsed) then
                xyz = FRC%atom%xyz
                allocate(ax(lx),ay(ly),az(lz))
                forall(ii=1:lx)
                    ax(ii) = ii-1
                end forall
                forall(ii=1:ly)
                    ay(ii) = ii-1
                end forall
                forall(ii=1:lz)
                    az(ii) = ii-1
                end forall
                
                call ndgrid(ax,ay,az,nx,ny,nz)
                xyzrep(:,1) = reshape(nx,/ll/)
                xyzrep(:,2) = reshape(ny,/ll/)
                xyzrep(:,3) = reshape(nz,/ll/)
                xyzrep      = matmul(xyzrep,avec)
                aa = (/1,2,3/)
                call permute(xyz,aa,xyz)
                call bsxfunPlus(xyz,xyzrep,xyzrep)
                aa = (/1,3,2/)
                call permute(xyzrep,aa,xyzrep)
                xyzrep = reshape(xyzrep,(/natom*size(nx),3/))
                FRC%atom%xyz = xyzrep
            else if (FracXYZisUsed) then
                xyz = FRC%atom%fracXYZ
                allocate(ax(lx),ay(ly),az(lz))
                forall(ii=1:lx)
                    ax(ii) = ii-1
                end forall
                forall(ii=1:ly)
                    ay(ii) = ii-1
                end forall
                forall(ii=1:lz)
                    az(ii) = ii-1
                end forall
                
                call ndgrid(ax,ay,az,nx,ny,nz)
                xyzrep(:,1) = reshape(nx,/ll/)
                xyzrep(:,2) = reshape(ny,/ll/)
                xyzrep(:,3) = reshape(nz,/ll/)
                xyzrep      = matmul(xyzrep,avec)
                aa = (/3,2,1/)
                call permute(xyz,aa,xyz)
                call bsxfunPlus(xyz,xyzrep,xyzrep)
                aa = (/1,3,2/)
                call permute(xyzrep,aa,xyzrep)
                xyzrep = reshape(xyzrep,(/natom*size(nx),3/))
                FRC%atom%fracXYZ = xyzrep
            else
                write(*,*) "error"
            end if
            
            call repmat(reshape(FRC%atom%element,(/natom/)),n,1,elerep)
            FRC%atom%element  = elerep
            call diag(FRC%option%replicateCell,tmp)
            FRC%domain%latvec = matmul(tmp,avec)
        end if
        FRC%init%repxyz = .TRUE.
    end if
    