!  initialization.f90 
!
!  FUNCTIONS:
!  initialization - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: initialization
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program initialization

    implicit none

    ! Variables

    ! Body of initialization
    print *, 'Hello World'

    end program initialization

