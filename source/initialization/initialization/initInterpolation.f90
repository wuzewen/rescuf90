! initInterPolation.f90
!****************************************
    !
!****************************************
    
    module initInterPolation_module
    contains
    subroutine initInterPolation(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    integer                              :: nline
    type(FORTRAN_RESCU_CALCULATION)      :: FRC
    type(inputFromFileType)              :: inputFromFile
    
    ! temporary variables
    integer :: ii, deforder, cmin, status
    logical :: interpRhoIsFind, interpolationMethodIsFind, interpolationOrderIsFind
    logical :: interpolationVnlIsFind, interpolationVlocIsFind
    
    ! output variables
    
    ! body of this function
    if (.not. FRC%init%interpolation) then
        nline                     =  inputFromFile%numberOfInput
        interpRhoIsFind           = .FALSE.
        interpolationMethodIsFind = .FALSE.
        interpolationOrderIsFind  = .FALSE.
        interpolationVnlIsFind    = .FALSE.
        interpolationVlocIsFind   = .FALSE.
        
        deforder = minval(FRC%domain%cgridn)
        deforder = min(deforder,16)
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("interpRho")) then
                interpRhoIsFind = .TRUE.
                exit
            end if
        end do
        if (interpRhoIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%interPolation%interPrho = .TRUE.
            else
                FRC%interPolation%interPrho = .FALSE.
            end if
        else
            FRC%interPolation%interPrho = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("interpolationMethod")) then
                interpolationMethodIsFind = .TRUE.
                exit
            end if
        end do
        if (interpolationMethodIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%interPolation%method
        else
            FRC%interPolation%method = "lagrange"
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("interpolationOrder")) then
                interpolationOrderIsFind = .TRUE.
                exit
            end if
        end do
        if (interpolationOrderIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") FRC%interPolation%order
        else
            FRC%interPolation%order = deforder
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("interpolationVloc")) then
                interpolationVlocIsFind = .TRUE.
                exit
            end if
        end do
        if (interpolationVlocIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%interPolation%vloc = .TRUE.
            else
                FRC%interPolation%vloc = .FALSE.
            end if
        else
            FRC%interPolation%vloc = .FALSE.
        end if
        
        ii = 0
        do while(ii < nline)
            ii     = ii+1
            if (trim(inputFromFile%NAV(ii)%name) == trim("interpolationVnl")) then
                interpolationVnlIsFind = .TRUE.
                exit
            end if
        end do
        if (interpolationVnlIsFind) then
            read(inputFromFile%NAV(ii)%value,"(i2)") status
            if (status /= 0) then
                FRC%interPolation%vnl = .TRUE.
            else
                FRC%interPolation%vnl = .FALSE.
            end if
        else
            FRC%interPolation%vnl = .FALSE.
        end if
        
        FRC%interPolation%order = FRC%interPolation%order+mod(FRC%interPolation%order,2)
        cmin                    = minval(FRC%domain%cgridn-1)
        cmin                    = cmin-mod(cmin,2)
        FRC%interPolation%order = min(FRC%interPolation%order,cmin)
        
        if (all(FRC%domain%cgridn == FRC%domain%fgridn)) then
            FRC%interPolation%vloc = .FALSE.
            FRC%interPolation%vnl  = .FALSE.
        end if
        
        if (FRC%interPolation%vloc) then
            FRC%interPolation%vnl = FRC%interPolation%vloc
        end if
        
        FRC%init%interPolation = .TRUE.
    end if
    
    return
    end subroutine initInterPolation
    end module initInterPolation_module