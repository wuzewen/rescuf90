! GetAtomInfo.f90
!**********************************
    !
!**********************************
    
    module GetAtomInfo_module
    contains
    subroutine GetAtomInfo(inputFromFile,FRC)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use inputType
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(inputFromFileType)         :: inputFromFile
    
    ! temporary variables
    real*8  :: bohr2ang, ang2bohr, nm2bohr
    integer :: ii, jj, ntmp
    logical :: unitsIsUsed, uniteXYZisUsed
    
    ! output variables
    
    ! body of this function
    
    if (.not. FRC%init%atominfo) then
        bohr2ang        = 0.52917721067
        ang2bohr        = 1/bohr2ang
        nm2bohr         = ang2bohr*10
        unitsIsUsed     = .FALSE.
        uniteXYZisUsed  = .FALSE.
        
        !ii = 0
        !do while(ii<=nline)
        !    ii     = ii+1
        !    if (trim(inputFromFile(ii)%name) == trim("unit")) then
        !        unitsIsUsed = .TRUE.
        !        exit
        !    end if
        !end do
        
        !ii = 0
        !do while(jj<=nline)
        !    ii     = ii+1
        !    if (trim(inputFromFile(ii)%name) == trim("unitXYZ")) then
        !        uniteXYZisUsed = .TRUE.
        !        exit
        !    end if
        !end do
        
        !if (.not. unitsIsUsed) then
        !    FRC%units%xyz = "Bohr"
        !else if (.not. uniteXYZisUsed) then
        !    FRC%units%xyz = "Bohr"
        !else
        !    FRC%units%xyz = inputFromFile(ii)%value
        !end if
        
        !if (FRC%spin%ispin == 1) then
        !    call readxyz(atomxyz,species)
        !else if (FRC%spin%ispin == 2) then
        !    call readxyzcollinear(atomxyz,magmom,species)
        !else if (FRC%spin%ispin == 4) then
        !    call readxyznoncollinear(atomxyz,magmom,species)
        !end if
        
        !if (FRC%sr%status) then
        !    call readxyzrelax(atomxyz,list,species)
        !end if
        
        allocate(FRC%atom%element(FRC%atom%NumberOfAtom))
        ntmp = 0
        do ii = 1,FRC%Atom%NumberOfElement,1
            do jj = 1,FRC%element(ii)%NumberOfAtom
                ntmp = ntmp +1
                FRC%atom%element(ntmp) = ii
            end do
        end do
        
        !if (trim(FRC%atom%coordinate) == trim("cartesian")) then
        !    FRC%atom%xyz = atomxyz
        !    if (trim(FRC%units%xyz) == trim("Angstrom")) then
        !        FRC%atom%xyz = FRC%atom%xyz*angtobohr
        !    else if (trim(FRC%units%xyz) == trim("nm")) then
        !        FRC%atom%xyz = FRC%atom%xyz*nmtobohr
        !    end if
        !else  if (trim(FRC%atom%coordinate) == trim("direct") .or. trim(FRC%atom%coordinate) == trim("fractional")) then
        !    FRC%atom%fracxyz = atomxyz
        !end if
        
        if (trim(FRC%atom%coordinateType) == trim("Cartesian")) then
            if (trim(FRC%units%XYZ) == trim("Angstrom")) then
                FRC%atom%XYZ = FRC%atom%xyz*ang2bohr
            else if (trim(FRC%units%XYZ) == trim("nm")) then
                FRC%atom%xyz = FRC%atom%xyz*nm2bohr
            end if
        end if
        
        FRC%units%xyz     = "Bohr"
        FRC%init%atominfo = .TRUE.
    end if
    
    return
    end subroutine GetAtomInfo
    end module GetAtomInfo_module
        
        
    