! parseFuncName.f90
    
!*************************************
    !
    !
    !
!*************************************
    
    module parseFuncName_module
    contains
    subroutine parseFuncName(list,faprx)
    
    implicit none
    
    ! input variables
    character(len=20), allocatable :: list(:)
    
    ! temporary variables
    integer                        :: nList, nchar, ntmp, ntmp1, ntmp2, ii, jj
    character(len=20)              :: chartmp, chartmp2
    character(len=1)               :: cpx
    logical                        :: setrue
    
    ! output variables
    character(len=20), allocatable :: faprx(:)
    
    ! body of this function
    nList = size(list)
    nchar = 20
    allocate(faprx(nList))
    
    do ii = 1,nList,1
        chartmp = list(ii)
        ntmp  = 0
        ntmp1 = 0
        ntmp2 = 0
        do jj = 1,20,1
            setrue = .FALSE.
            cpx = chartmp(jj:jj)
            if (cpx == '_') then
                setrue = .TRUE.
                ntmp   = ntmp+1
                if (ntmp == 1) then
                    ntmp1  = jj
                else if (ntmp == 2) then
                    ntmp2  = jj
                end if
            end if
        end do
        ntmp = ntmp2 - ntmp1 - 1
        chartmp2(1:ntmp) = chartmp(ntmp1+1:ntmp2-1)
        faprx(ii) = chartmp2
    end do
    
    return
    end subroutine parseFuncName
    end module parseFuncName_module
 