! calcGradRad.f90
    
!*****************************************
    !
    !
    !
!*****************************************
    
    module calcGradRad_module
    contains
    subroutine calcGradRad(dataStruct)
    
    use ElementData_type
    
    implicit none
    
    ! input variables
    type(ElementDataType), allocatable :: dataStruct(:)
    
    ! temporary variables
    integer :: n, ii
    
    ! output variables
    
    ! body of this function
    n = size(dataStruct)
    
    write(*,*) "Error in calcGradRad.f90. not available now."
    stop
    
    do ii = 1,n,1
        n = n+0
        !if (size(dataStruct(ii)%Rpc)>0 .and. size(dataStruct(ii)%gradData)>0) then
        !    rr  = dataStruct(ii)%Rpc%rrData
        !    rpc = dataStruct(ii)%Rpc%rhoData
        !    call radialGradient(rr,rpc,dataStruct(ii)%Rpc%gradData)
        !end if
    end do
    
    return
    end subroutine calcGradRad
    end module calcGradRad_module
    