! radialLaplacian.f90
    
!*******************************************
    !
    !
    !
!*******************************************
    
    module radialLaplacian_module
    contains
    subroutine radialLaplacian(r,fr,rLapl)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    call radialGradient(f,fr,rGrad)
    call radialGradient(r,rGrad,rLapl)
    do ii = 2,size(rLapl),1
        rlapl(ii) = rlapl(ii) + 2*rGrad(ii)/r(ii)
    end do
    
    call diff(r,dr)
    call cumsum(dr(1:6),drtmp)
    allocate(tmp(6))
    forall(ii=1:6)
        tmp(ii) = ii-1
    end forall
    
    call bsxfunPower(drtmp,tmp,V)
    allocate(b(6,1))
    b    = 0
    b(1) = 1
    call leftdivision(V,b,btmp)
    rLapl(1) = matmul(rlapl(2:7),btmp)
    
    return
    end subroutine radialLaplacian
    end module radialLaplacian_module