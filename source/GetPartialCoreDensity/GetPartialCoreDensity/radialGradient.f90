! radialGradient.f90
    
!*******************************************
    !
    !
    !
!*******************************************
    
    module radialGradient_module
    contains
    subroutine radialGradient(r,fr,rGrad)
    
    implicit none
    
    ! input variabes
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    npt  = size(r)
    npt1 = size(r,1)
    npt2 = size(r,2)
    allocate(rGrad(npt1,npt2))
    rGrad = 0
    allocate(b(5))
    b    = 0
    b(2) = 1
    
    do ii = 1,npt,1
        allocate(inde(5))
        if (ii == 1) then
            forall(jj=1:5)
                inde(jj) = jj
            end forall
            ipt = 1
        else if (ii == 2) then
            forall(jj=1:5)
                inde(jj) = jj
            end forall
            ipt = 2
        else if (ii == npt-1) then
            forall(jj=1:5)
                inde(jj) = jj+npt-5
            end forall
            ipt = npt-1
        else if(ii = npt) then
            forall(jj=1:5)
                inde(jj) = jj+npt-5
            end forall
            ipt = npt
        else
            forall(jj=1:5)
                inde(jj) = jj+ii-3
            end forall
            ipt = ii
        end if
        
        drtmp = r(inde) - r(ipt)
        call norm(drtmp,"inf",cdr)
        allocate(tmp(5,1))
        forall (jj=1:5)
            tmp(jj,1) = jj-1
        end forall
        call bsxfunPower(drtmp/cdr,tmp,V)
        call leftdivision(V,b,btmp)
        rGrad(ii) = matmul(fr(inde),btmp)/cdr
    end do
    
    return
    end subroutine radialGradient
    end module radialGradient_module