! calcGradRad.f90
    
!*****************************************
    !
    !
    !
!*****************************************
    
    module calcLaplRad_module
    contains
    subroutine calcLaplRad(dataStruct,n)
    
    implicit none
    
    ! input variables
    
    ! temporary variables
    
    ! output variables
    
    ! body of this function
    do ii = 1,n,1
        rr  = dataStruct(ii)%Rpc%rrData
        rpc = dataStruct(ii)%Rpc%rhoData
        call radialLaplcian(rr,rpc,dataStruct(ii)%Rpc%laplData)
    end do
    
    return
    end subroutine calcLaplRad
    end module calcLaplRad_module
    