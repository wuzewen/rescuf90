!**********************************************************
!   Data format **** Input variables **** Type **** size **** default **** component **** level **** description 
    integer          ::  SystemAtomElement  !  array  [inf,1] [] text Point every atoms to an element defined in element. Generated automatically if atom.xyz is a string pointing to an xyz file.
    real*8           ::  SystemAtomXYZ
    real*8           ::  SystemAtomFracXYZ
    real*8           ::  SystemAtomMagmom
    real*8           ::  SystemAtomMagmomD
    real*8           ::  SystemAtomMagmomCart
    
    character(len=5) :: SystemDomainBravaisLattice
    integer          ::  SystemDomainBoundary(3)
    real*8           ::  SystemDomainBvalX
    real*8           ::  SystemDomainBvalY
    real*8           ::  SystemDomainBvalZ
    real*8           ::  SystemDomainLatticeVectors(3,3)
    
    real*8           ::  RealSpaceControlCgridN(3)
    real*8           ::  RealSpaceControlFgridN(3)
    real*8           ::  RealSpaceControlHighRes
    real*8           ::  RealSpaceControlLowRes
    
    integer          ::  MethodEigenSolverAlgo
    integer          ::  MethodEigenSolverAlgoProj
    
    integer          ::  ControlMPIstatus
    integer          ::  ControlMPIparaK
    integer          ::  ControlSMIstatus
    integer          ::  ControlLCAOstatus
    integer          ::  ControlHSEstatus
    integer          ::  ControlMixDensity
    integer          ::  ControlMixDensityMatrix
    integer          ::  ControlMixPotential
    integer          ::  ControlShortRange
    integer          ::  