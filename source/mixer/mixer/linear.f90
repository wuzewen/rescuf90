! linear.f90
    
!***************************************************
    !
    !
    !
!***************************************************
    
    module linear_module
    contains
    subroutine linear(FRC,mu_m,vu_m,mu_mpl)
    
    use FORTRAN_RESCU_CALCULATION_TYPE
    use VataMN_2D_module
    
    implicit none
    
    ! input variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    type(VataMN_2D)                 :: vu_m, mu_m
    
    ! temporary variables
    real*8 :: beta
    
    ! output variables
    type(VataMN_2D)                 :: mu_mpl
    
    ! body of this funtion
    beta        = FRC%mixing%beta
    mu_mpl      = mu_m
    mu_mpl%vata = mu_m%vata+beta*(vu_m%vata-mu_m%vata)
    
    return
    end subroutine linear
    end module linear_module