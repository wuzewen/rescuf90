! extratDM.f90
!************************
    !
!************************
    
    module extratDM_module
    contains
    subroutine extratDM()
    
    implicit none
    
    ! input variables
    
    ! temporery variables
    
    ! output variables
    
    ! body of this function
    call GetGlobalInd()
    VnewData = reshape(VnewData)
    do i = 1,nkpt*nspin
        call ind2sub
        DMcellData(:,:) = VnewData(:,:,i)
    end do
    
    end subroutine extratDM
    end module extratDM_module