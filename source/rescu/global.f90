! global valuables
    module global
    implicit none
    
    real              :: lattice(3,3)
    integer           :: nx, ny, nz, nn
!    real, allocatable :: grids(:,:)
    common               lattice, nx, ny, nz, nn
    end module global