!   initRealSpaceGrids.f90
!   this is a function from which real space grids' coordinates can be obtained
    
    
    module     initRealSpaceGrid
    contains
    subroutine initRealSpaceGrids(lattice,nx,ny,nz,grids)
    !use global
    !use constant
    implicit none
    
    integer           :: i, j, k
    integer           :: nx,ny,nz,nn,nt
    real              :: lattice(3,3)
    real              :: dx(1,3), dy(1,3), dz(1,3)
    real, allocatable :: grids(:,:)
    
    nn      = nx*ny*nz
    dx(1,:) = lattice(1,:)/real((nx-1))
    dy(1,:) = lattice(2,:)/real((ny-1))
    dz(1,:) = lattice(3,:)/real((nz-1))
    
    open(unit=11, file="grids.txt",status='replace',action='write')
    do i = 1, nx, 1
        do j = 1, ny, 1
            do k = 1, nz, 1
                nt          = (i-1)*ny*nz+(j-1)*nz+k
                grids(nt,:) = dx(1,:)*real(i-1)+dy(1,:)*real(j-1)+dz(1,:)*real(k-1)
                write(11,*) grids(nt,:)
            end do
        end do
    end do
    close(11)
    
    return
    end  subroutine initRealSpaceGrids
    end  module     initRealSpaceGrid