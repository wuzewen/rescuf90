! constant file
    module constant
    implicit none
    
    real, parameter  ::  hbar      =  1
    real, parameter  ::  pi        =  3.141592653
    real, parameter  ::  bohr2angs = 27.211383411
    real, parameter  ::  electron  =  1
    
    end module constant