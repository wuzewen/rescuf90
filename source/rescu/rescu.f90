!   rescu.f90 
!   this is the main program in rescu project
    
    
    program rescu
    !use global
    !use constant
    use initRealSpaceGrid
    implicit none
    
    real               :: l
    integer            :: nx,ny,nz,nn
    real               :: lattice(3,3)
    real, allocatable  :: grids(:,:)
    character(len=20)  :: nonsense
    
    
    open(unit=10,file="INPUT",action='read')
    read(10,*) nonsense
    do l = 1,3,1
        read(10,*) lattice(l,:)
    end do
    read(10,*) nonsense
    read(10,*) nx, ny, nz
    close(10)
    
    nn = nx*ny*nz
    allocate(grids(nn,3))
    call initRealSpaceGrids(lattice,nx,ny,nz,grids)
    
    
    end program rescu

