!  status.f90 
!
!  FUNCTIONS:
!  status - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: status
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module status_module
    contains

    implicit none

    ! variables
    type :: status
        logical :: mpi
        logical :: smi

    ! Body of status
    

    end module status

