! GenRealSH.f90
!*********************************************
    !
!*********************************************
    
    module GenRealSH_module
    contains
    subroutine GenRealSH(x,y,z,LL,YLM)
    
    implicit none
    
    ! input variables
    real*8, allocatable  :: x(:), y(:), z(:)
    integer              :: LL
    
    ! temporery variables
    real*8, allocatable  :: r(:)
    integer, allocatable :: inde(:)
    integer              :: m, co, ct, i
    real*8               :: pi
    
    ! output variables
    real*8, allocatable  :: YLM(:)
    
    ! body of this function
    pi = 3.14159265354
    m  = size(x)
    allocate(r(m))
    r  = sqrt(x**2+y**2+z**2)
    co = count(r==0)
    allocate(inde(co))
    ct = 0
    do i = 1,m,1
        if (r(i)==0) then
            ct       = ct+1
            inde(ct) = i
        end if
    end do
    
    allocate(YLM(m))
    select case(LL)
    case(0)
        YLM(:) = 1
        YLM    = YLM/sqrt(pi)/2
    case(1)
        YLM    = sqrt(3/4/pi)
    