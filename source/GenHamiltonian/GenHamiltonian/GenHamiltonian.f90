!  GenHamiltonian.f90 
!****************************************************************************
!
!  PROGRAM: GenHamiltonian
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    module GenHamiltonian_module
    contains
    subroutine GenHamiltonian(FRC)
    
    ! data modules
    use FORTRAN_RESCU_CALCULATION_TYPE
    use ElementDataType_module
    use GetKBorb_module
    
    ! function modules
    use GenGradient_module
    use Genlaplacian_module
    use genNeutralAtomPotential_module
    use GetNeutralAtomDensity_module
    use ModBCDist_1D_module
    use GetNonLocalPotentialK_module
    use getPartialCoreDensity_module
    use distmat_gather_module
    use getHartreePotential_module
    use xcMain_module
    use UpdateVeff_module
    
    implicit none

    ! input Variables
    type(FORTRAN_RESCU_CALCULATION) :: FRC
    !type(ElemetDataType)            :: 
    
    ! temporery variables
    integer             :: bc(3)
    logical             :: shortRange, antloc
    real*8              :: tmp(3)
    integer             :: mpirank, mpisize, veffn
    real*8, allocatable :: vmean(:,:,:)
    type(VataMN_2D)     :: rho, veffout
    type(VataMN_1D)     :: vps, rhotmp
    real*8, allocatable :: vps2(:)
    
    ! output variables
    

    ! Body of GenHamiltonian
    shortRange = FRC%option%shortRange
    mpirank    = FRC%mpi%rank
    mpisize    = FRC%mpi%mpisize
    bc         = FRC%domain%boundary
    call GenGradient(FRC)               ! generate gradiant operator
    call Genlaplacian(FRC)              ! generate laplacian operator
    !call GenInterpolator(FRC)           ! generate interpolation operator
    !call GenIntFun(FRC)
    
    if (FRC%LCAO%status .or. trim(FRC%eigensolver%Init) == trim("LCAO")) then
        
        write(*,*) "Error in GenHamiltonian.f90. LCAO is not available now."
        stop
        
        !call calcBooleOverlapMatrix(FRC)
        !call calcOverlapAndKineticMatrix(FRC)
        !call calcNonLocalPotentialMatrix(FRC)
        !call mergeVnlKineticMatrix(FRC)
        !if (FRC%spin%SOI) then
        !    call calcProjectorMatrixSOI(FRC)
        !    call calcVnlMatrixSOI(FRC)
        !end if
    end if
    
    call GenNeutralAtomPotential(FRC)   ! generate neutral atom potential
    if (.not. shortRange) then
        write(*,*) "Error in GenHamiltonian.f90. shortRange should be true now."
        stop
        !vmean = sum(FRC%potential%Vna%vnaData)
        !if (FRC%mpi%status) then
        !    call MPI_ALLreduce_sum(vmean)
        !end if
        !FRC%potential%Vna%vnaData = FRC%potential%Vna%vnaData-vmean/FRC%potential%Vna%M
    end if
    
    if ((.not. FRC%LCAO%status) .or. FRC%LCAO%cfsi) then
        call GetNonLocalPotentialK(FRC)     ! generate non-local potential (RS)
        if (FRC%spin%SOI) then
            
            write(*,*) "Error in GenHamiltonian.f90. SOI is not available now."
            stop
            
            !call GetNonLocalPotentialSO(FRC)
        end if
        tmp = 0
        call GetKBorb(FRC,tmp)
    end if
    
    call GetNeutralAtomDensity(FRC)     ! generate isolated atom density
    !call GetInitialDensity(FRC)         ! generate initial density
    call GetPartialCoreDensity(FRC)     ! generate partial core density
    
    if (shortRange) then
        vps = FRC%potential%Vna
        call distmat_gather(FRC,vps,vps2)
        !if (mpirank == 0 .and. FRC%interpolation%vloc) then
            !vps = FRC%interPolation%AntFun(vps)
        !end if
        veffn = size(vps%vata)
        if (FRC%mpi%status) then
            write(*,*) "Error in GenHamiltonian.f90. MPI is not available now."
            stop
            !call MPI_bcast_variables(veffn,0)
        end if
        call InitDistArray_1D(veffn,1,veffn,1,1,mpisize,vps)
        call modBCDist_1D(FRC,vps,vps%m/mpisize,1,mpisize,1,.FALSE.,.FALSE.,FRC%potential%vps)
    else
        
        write(*,*) "Error in GenHamiltonian.f90. ShortRange should be true now."
        stop
        
        !call GetHartreePotential(FRC)       ! Calculate Hartree potential of the neutral atom density
        !call distmat_fevalMinus(FRC)
        !call distmat_gather(FRC,vps)
        !if (mpiRank == 0) then
        !    vps = InterPolationAntFun(vps)
        !end if
        !veffn = size(vps)
        !if (FRC%mpi%status) then
        !    call MPI_Bcast_variable(veffn,0)
        !end if
        !call InitDistArray(veffn,1,veffn,1,1,mpisize,vps)
        !call ModBCDist(FRC,vps,ceiling(vps%m/mpisize),1,mpisize,1)
    end if
    
    if (shortRange) then
        allocate(rho%vata(size(FRC%rho%input(1)%vata,1),size(FRC%rho%input(1)%vata,2)))
        rho = FRC%rho%input(1)
        if (FRC%spin%ispin == 1) then
            !call distmat_bsxfun_Minus(FRC,rho,FRC%rho%atom)
        else if (FRC%spin%ispin == 2) then
            write(*,*) "Error in GenHamiltonian.f90. Collinear spin is not available now."
            stop
            !call distmat_bsxfun_XY(FRC,rho,FRC%rho%atom)
        else if (FRC%spin%ispin == 4) then
            write(*,*) "Error in GenHamiltonian.f90. general spin(non-collinear) is not alvailable now."
            stop
            !call distmat_bsxfun_Minus(FRC,rho,FRC%rho%atom)
        end if
        allocate(rhotmp%vata(size(rho%vata)))
        rhotmp%m      = rho%m
        rhotmp%n      = rho%n
        rhotmp%mblock = rho%mblock
        rhotmp%nblock = rho%nblock
        rhotmp%mproc  = rho%mproc
        rhotmp%nproc  = rho%nproc
        rhotmp%vata   = rho%vata(:,1)
        call GetHartreePotential(FRC,rhotmp,bc,.TRUE.,FRC%potential%VH(1))
    else
        
        write(*,*) "Error in GenHamiltonian.f90. ShortRange should be true now."
        stop
        
        !if (FRC%rho%input(1) /= FRC%rho%Atom%atomData) then
        !    call GetHartreePotential(FRC,FRC%rho%input(1),FRC%potential%VH(1))
        !else
        !    FRC%potential%VH(1) = FRC%potential%Vatom
        !end if
    end if
    
    if (trim(FRC%mixing%mixingType) == trim("density")) then
        allocate(FRC%rho%input(2)%vata(size(FRC%rho%input(1)%vata),1))
        FRC%rho%input(2)  = FRC%rho%input(1)
    else
        allocate(FRC%rho%output(1))
        allocate(FRC%rho%output(1)%vata(size(FRC%rho%input(1)%vata,1),size(FRC%rho%input(1)%vata,2)))
        FRC%rho%output(1) = FRC%rho%input(1)
    end if
    
    call xcMain(FRC)                   ! Calculate the XC potential of the neutral atom density
    antloc = .not.(FRC%interpolation%vloc)
    call updateVeff(FRC,FRC%potential%VXCout(1),FRC%potential%VH(1),antloc,veffout)
    allocate(FRC%potential%veffin(1)%vata(size(veffout%vata,1)))
    FRC%potential%veffin(1)%vata   = veffout%vata(:,1)
    FRC%potential%veffin(1)%m      = veffout%m
    FRC%potential%veffin(1)%n      = veffout%n
    FRC%potential%veffin(1)%mblock = veffout%mblock
    FRC%potential%veffin(1)%nblock = veffout%nblock
    FRC%potential%veffin(1)%mproc  = veffout%mproc
    FRC%potential%veffin(1)%nproc  = veffout%nproc
    
    return
    end subroutine GenHamiltonian
    end module GenHamiltonian_module

