! applyStencil.f90
!**************************************
    !
!**************************************
    
    module applyStencil_module
    contains
    subroutine applyStencil(stencil,dim,ngrid,X,sX)
    
    implicit none
    
    ! input variables
    real, allocatable    :: stencil(:)
    integer              :: dim
    integer              :: ngrid(3)
    integer              :: ix, iy, ii
    real, allocatable    :: X(:,:)
    
    ! temporery variables
    integer              :: nst, nst2, m, n
    integer              :: tmp(3), step(3)
    
    ! output variables
    
    
    ! body of this function
    nst  = size(stencil)
    nst2 = nst/2
    m    = size(X,1)
    n    = size(X,2)
    allocate(sX(m,n))
    sX(:,:) = 0
    
    tmp(1)    = 1
    tmp(2)    = ngrid(1)
    tmp(3)    = ngrid(1)*ngrid(2)
    step(:)   = 0
    step(dim) = 1
    
    do ix = 1,ngrid(1),1
        nx = ix
        do iy = 1,ngrid(2),1
            ny = (iy-1)*tmp(2)
            do iz = 1,ngrid(3),1
                nz = (iz-1)*tmp(3)
                nn = nx+ny+nz
                do ii = -nst2,nst2,1
                    ns    = 1+(mod(ix-1+ii*step(1),ngrid(1)))*tmp(1)+ &
                              (mod(iy-1+ii*step(2),ngrid(2)))*tmp(2)+ &
                              (mod(iz-1+ii*step(3),ngrid(3)))*tmp(3)
                    sX(n) = sX(n)+stencil(ii+nst2+1)*X(ns)
                end do
            end do
        end do
    end do
    
    return
    end subroutine applyStencil
    end module applyStencil_module