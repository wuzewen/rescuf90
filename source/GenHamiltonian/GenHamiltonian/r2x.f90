! r2x.f90
!*****************************************
    !
!*****************************************
    
    module r2x_module
    contains
    subroutine r2x(r,t,p,x,y,z)
    
    implicit none
    
    ! input variables
    real, allocatable :: r(:), t(:), p(:)
    
    ! temporery variables
    
    ! output variables
    real, allocatable :: x(:), y(:), z(:)
    
    ! body of this function
    x = r*sin(t)*cos(p)
    y = r*sin(t)*sin(p)
    z = r*cos(t)
    
    end subroutine r2x
    end module r2x_module