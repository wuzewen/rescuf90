function H = hamOper2Mat(mrc,kpt)

iter = mrc.scloop;
% kine
avec = mrc.domain.latvec; bvec = 2*pi*inv(avec)';
kptcart = kpt*bvec;
J1 = inv(avec)'; J2 = J1*J1';
k2 = norm(kptcart,2)^2;
kptcart = J1*kptcart(:);
Du = mrc.diffop.Du; Dv = mrc.diffop.Dv; Dw = mrc.diffop.Dw;
Duu = mrc.diffop.Duu; Dvv = mrc.diffop.Dvv; Dww = mrc.diffop.Dww;
cgridn = mrc.domain.cgridn; n = cgridn;
G = {kron(speye(n(3)),kron(speye(n(2)),Du)),...
kron(speye(n(3)),kron(Dv,speye(n(1)))),...
kron(Dw,kron(speye(n(2)),speye(n(1))))};
L = {kron(speye(n(3)),kron(speye(n(2)),Duu)),...
kron(speye(n(3)),kron(Dvv,speye(n(1)))),...
kron(Dww,kron(speye(n(2)),speye(n(1))))};
lapl =        L{1}*J2(1,1);
lapl = lapl + L{2}*J2(2,2);
lapl = lapl + L{3}*J2(3,3);
if J2(1,2) || J2(1,3) || kptcart(1)
   if kptcart(1)
      lapl = lapl + 2j*kptcart(1)*G{1};
   end
   if J2(1,2)
      lapl = lapl + J2(1,2)*(G{1}*G{2}+G{2}*G{1});
   end
   if J2(1,3)
      lapl = lapl + J2(1,3)*(G{1}*G{3}+G{3}*G{1});
   end
end
if J2(2,3) || kptcart(2)
   if kptcart(2)
      lapl = lapl + 2j*kptcart(2)*G{2};
   end
   if J2(2,3)
      lapl = lapl + J2(2,3)*(G{2}*G{3}+G{3}*G{2});
   end
end
if kptcart(3)
   lapl = lapl + 2j*kptcart(3)*G{3};
end
if any(k2)
   lapl = lapl - k2*speye(size(lapl));
end
lapl = full(lapl);
% veff
mrc = gatherLocalPotential(mrc,iter);
veff = diag(mrc.potential.vloc);
% vnl
mrc = GetKBorb(mrc,kpt);
dr = abs(det(mrc.domain.latvec))/prod(cgridn);
kbedr = mrc.potential.vnl.KBE(:)*dr;
chilm = mrc.potential.vnl.KBorb;
vnl = full(chilm*diag(kbedr)*chilm');
% ham
H = -0.5*lapl + veff + vnl;
H = 0.5*(H+H');